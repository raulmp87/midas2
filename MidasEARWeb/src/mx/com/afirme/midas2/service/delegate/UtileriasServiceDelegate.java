package mx.com.afirme.midas2.service.delegate;
import java.math.BigDecimal;
import java.util.Map;

import mx.com.afirme.midas2.dto.siniestros.valuacion.ordencompra.ImportesOrdenCompraDTO;
import mx.com.afirme.midas2.service.UtileriasService;
import org.directwebremoting.annotations.RemoteProxy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

@RemoteProxy(name = "utileriasService")
public class UtileriasServiceDelegate implements UtileriasService {

	private UtileriasService utileriasService;
	public UtileriasService getUtileriasService() {
		return utileriasService;
	}
	@Autowired
	@Qualifier("utileriasServiceEJB")
	public void setUtileriasService(UtileriasService utileriasService) {
		this.utileriasService = utileriasService;
	}
	@Override
	public int diferenciaDiasFechas(String fechaInicial, String fechaFinal) {
		return utileriasService.diferenciaDiasFechas(fechaInicial, fechaFinal);
	}
	@Override
	public String obtenerRFCbeneficiario(Integer idPrestador) {
		return utileriasService.obtenerRFCbeneficiario(idPrestador);
	}
	@Override
	public String obtenerCURPbeneficiario(Integer idPrestador) {
		return utileriasService.obtenerCURPbeneficiario(idPrestador);
	}
	@Override
	public BigDecimal obtenerTotalesOrdenCompra(Long idEstimacionCobRepCab,
			Long idReporteCabina, Long idcoberturaReporteCabina) {
		return utileriasService.obtenerTotalesOrdenCompra(idEstimacionCobRepCab, idReporteCabina, idcoberturaReporteCabina);
	}
	@Override
	public BigDecimal obtenerReserva(Long idToCobertura,
			Long idEstimacionCobertura) {
		return utileriasService.obtenerReserva(idToCobertura, idEstimacionCobertura);
	}
	@Override
	public BigDecimal obtenerReservaDisponibleOrdenCompra(
			Long idEstimacionCobRepCab, Long idReporteCabina,
			Long idcoberturaReporteCabina) {
		return utileriasService.obtenerReservaDisponibleOrdenCompra(idEstimacionCobRepCab, idReporteCabina, idcoberturaReporteCabina);
	}
	@Override
	public boolean ordenCompraAplicaBeneficiario(Long idcoberturaReporteCabina,String cveSubTipoCalculo) {
		return utileriasService.ordenCompraAplicaBeneficiario(idcoberturaReporteCabina,cveSubTipoCalculo);
	}
	@Override
	public Map<Long, String> getListasTercerosAfectadorPorCoberturas(
			String cadenaCoberturas) {
		return utileriasService.getListasTercerosAfectadorPorCoberturas(cadenaCoberturas);
	}
	@Override
	public ImportesOrdenCompraDTO calcularImportes(Long idOrdenCompra,
			Long idOrdenCompraDetalle, boolean calcularPorConcepto) {
		return this.utileriasService.calcularImportes(idOrdenCompra, idOrdenCompraDetalle, calcularPorConcepto);
	}
	
	

}