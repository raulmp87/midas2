package mx.com.afirme.midas2.service.delegate;

import java.util.LinkedHashMap;

import mx.com.afirme.midas2.service.componente.ComponenteService;

import org.directwebremoting.annotations.RemoteMethod;
import org.directwebremoting.annotations.RemoteProxy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

@RemoteProxy(name="componenteService")
public class ComponenteServiceDelegate {

	ComponenteService componenteService;
	
	/**
	 * Consulta el listado de registros dependientes de un registro cuyo id se recibe en la clave.
	 * @param clave. La clave contiene el tipo de catálogos a consultar y el identificador del registro padre.
	 * @return
	 */
	@RemoteMethod
	public LinkedHashMap<String, String> getListadoRegistros(String clave){
		return componenteService.getMapaRegistros(clave);
	}
	

	@Autowired
	@Qualifier("componenteEJB")
	public void setComponenteService(ComponenteService componenteService){
		this.componenteService = componenteService;
	}
}
