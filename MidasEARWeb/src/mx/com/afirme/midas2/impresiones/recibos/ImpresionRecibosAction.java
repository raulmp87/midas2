package mx.com.afirme.midas2.impresiones.recibos;


import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas.solicitud.SolicitudDTO;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.emision.ppct.ReciboSeycos;
import mx.com.afirme.midas2.domain.negocio.Negocio;
import mx.com.afirme.midas2.domain.negocio.producto.NegocioProducto;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.NegocioSeccion;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.recibos.MovimientoReciboDTO;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.fuerzaventa.CentroEmisorService;
import mx.com.afirme.midas2.service.negocio.NegocioService;
import mx.com.afirme.midas2.service.poliza.EnvioEmailPolizaService.EnviarEmailPorIncisoParameters;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.recibos.ImpresionRecibosService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
public class ImpresionRecibosAction extends BaseAction  implements Preparable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8630881146731199451L;
	
	private PolizaDTO polizaDTO;
	private List<Agente> agenteList = new ArrayList<Agente>(1);
	private List<Negocio> negocioList = new ArrayList<Negocio>(1);
	private List<NegocioProducto> productoList = new ArrayList<NegocioProducto>(1);
	private Integer idTcAgente;
	private Boolean filtrar= false;
	private String accion;
	private Map<Long, String> centrosEmisores;
	private Map<Long, String> gerencias;
	private BigDecimal idToNegSeccion;
	private BigDecimal idToNegTipoPoliza;
	private BigDecimal idToNegProducto;
	private BigDecimal idToNegocio;
	private NegocioSeccion negocioSeccion;
	
	private EnviarEmailPorIncisoParameters enviarEmailPorIncisoParameters = new EnviarEmailPorIncisoParameters();
	
	private ListadoService listadoService;
	private NegocioService negocioService;
	private CentroEmisorService centroEmisorService;
	private ImpresionRecibosService impresionRecibosService;
	private EntidadService entidadService;
		
	
	private BigDecimal id;
	private Agente agente;

    private String descripcionBusquedaAgente = null;
    private String descripcionBusquedaNegocio = null;
    
	private InputStream plantillaInputStream;
	private String contentType;
	private String fileName;    

    
    private Locale locale = getLocale();
    private boolean agenteControlDeshabilitado;
    private boolean promotoriaControlDeshabilitado;
    
	private Boolean    chkcaratula = false;
	private Boolean    chkrecibo = false;
	private Boolean    chkinciso = false;
	private Boolean    chkcertificado = false;
	private Boolean    chkanexos = false;
	private Boolean    chkreferencias = false;
	private int        tipoImpresion = 4;
	private int        totalIncisos = 0;
    private Integer esImpresionPoliza = 1;
    private Date validOn;
    private Long validOnMillis;
    
    private List<PolizaDTO> polizaList = new ArrayList<PolizaDTO>(1);
    private String nombreArchivo;
    
    //Recibos
    private List<ReciboSeycos> reciboList = new ArrayList<ReciboSeycos>(1);
    private String incisosTxt = ""; 
    private InputStream reciboInputStream;
    private BigDecimal idRecibo;
    private BigDecimal numeroEndoso;
    private BigDecimal idProgPago;
    private BigDecimal idInciso;
    
    private Map<BigDecimal, String> endososList;
    private Map<BigDecimal, String> proPagoList;
    private Map<BigDecimal, String> incisoList;
    private List<MovimientoReciboDTO> movimientoReciboList = new ArrayList<MovimientoReciboDTO>(1);
    
    private static final int REGISTROS_A_MOSTRAR_REN = 300;

    @Autowired
	@Qualifier("entidadEJB")
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}
    
    @Autowired
	@Qualifier("impresionRecibosServiceEJB")
	public void setImpresionRecibosService(ImpresionRecibosService impresionRecibosService) {
		this.impresionRecibosService = impresionRecibosService;
	}
    
	@Autowired
	@Qualifier("listadoServiceEJB")
	public void setListadoService(ListadoService listadoService) {
		this.listadoService = listadoService;
	}
	
	@Autowired
	@Qualifier("negocioServiceEJB")
	public void setNegocioService(NegocioService negocioService) {
		this.negocioService = negocioService;
	}
	
	@Autowired
	@Qualifier("centroEmisorServiceEJB")
	public void setCentroEmisorService(CentroEmisorService centroEmisorService) {
		this.centroEmisorService = centroEmisorService;
	}
	
	public boolean isAgenteControlDeshabilitado() {
		return agenteControlDeshabilitado;
	}
	
	public boolean isPromotoriaControlDeshabilitado() {
		return promotoriaControlDeshabilitado;
	}

	@Override
	public void prepare() throws Exception {
		
		if(polizaDTO == null){
			polizaDTO = new PolizaDTO();
			polizaDTO.setCotizacionDTO(new CotizacionDTO());
			polizaDTO.getCotizacionDTO().setSolicitudDTO(new SolicitudDTO());
		}		
	}
	
	public void prepareMostrar(){
		//negocios activos
		Negocio negocio = new Negocio();
		negocio.setClaveNegocio("A");
		negocio.setClaveEstatus(Negocio.EstatusNegocio.ACTIVO.obtenerEstatus());
		negocioList = negocioService.findByFilters(negocio);
		
		if (usuarioService.tienePermisoUsuarioActual("FN_M2_General_Visibilidad_Agente_Agente_Usuario_Actual")) {
			agenteControlDeshabilitado = true;
			promotoriaControlDeshabilitado = true;
			Agente agenteUsuarioActual = usuarioService.getAgenteUsuarioActual();
			asignarAgente(agenteUsuarioActual);
			
			negocioList = negocioService.listarNegociosPorAgenteUnionNegociosLibres(agenteUsuarioActual.getId().intValue(), "A");
		} else if (usuarioService.tienePermisoUsuarioActual("FN_M2_General_Visibilidad_Agente_Agentes_Promotoria_Usuario_Actual")) {
			promotoriaControlDeshabilitado = true;
		}
		
		setCentrosEmisores(centroEmisorService.listarCentrosEmisoresMap());	
		this.setGerencias(listadoService.getMapGerencias());
	}
	
	public String mostrar(){
		return SUCCESS;
	}

		
	public String buscarPolizasPaginado() {
		if (polizaDTO != null && filtrar) {
			if(getTotalCount() == null){
				setTotalCount(impresionRecibosService.obtenerTotalPolizasRecibos(polizaDTO, filtrar, true));
				setListadoEnSession(null);
			}
			setPosPaginado(REGISTROS_A_MOSTRAR_REN);
		}
		return SUCCESS;
	}
	
	@SuppressWarnings("unchecked")
	public String buscarPolizas() {
		if (polizaDTO != null && filtrar) {
			if(!listadoDeCache()){
				polizaDTO.setPrimerRegistroACargar(getPosStart());
				polizaDTO.setNumeroMaximoRegistrosACargar(REGISTROS_A_MOSTRAR_REN);
				polizaList = impresionRecibosService.listarPolizasRecibos(polizaDTO, filtrar, false);
				setListadoEnSession(polizaList);
			}else{
				polizaList = (List<PolizaDTO>) getListadoPaginado();
				setListadoEnSession(polizaList);
			}
			
		}
		return SUCCESS;
	}
	
	public String mostrarContenedorImpresion(){		
		Date today = new Date();
		setValidOn(today);
		setValidOnMillis(today.getTime());
	    return SUCCESS;		
	}
	
	public String seleccionarNombre(){
	    return SUCCESS;		
	}

	public String generarImpresion(){
		if(polizaList != null && polizaList.size()>0){
			try{
				InputStream is = impresionRecibosService.imprimirPoliza(polizaList, locale,chkcaratula,chkrecibo,chkinciso,chkcertificado,chkanexos,chkreferencias);
				if(is == null){
					return ERROR;
				}
				setPlantillaInputStream(is);
				contentType = "application/zip";
				setFileName("Polizas.zip");
				return SUCCESS;
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		return ERROR;
	}
	
	public String enviarProveedor(){
		if(polizaList != null && polizaList.size()>0){
			try{
				String mensaje = impresionRecibosService.enviaPolizasProveedor(polizaList, locale,nombreArchivo);
				if(mensaje != null){
					this.setMensajeError(mensaje);
				}else{
					this.setMensajeExito();
				}
				return SUCCESS;
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		return ERROR;
	}
	
	public String mostrarEnviarEmailMasivo(){
		return SUCCESS;
	}
	
	public String enviarCorreoMasivo(){
		if(polizaList != null && polizaList.size()>0){
			
			impresionRecibosService.envioMasivoCorreos(polizaList, enviarEmailPorIncisoParameters);
			
			setTipoMensaje(BaseAction.TIPO_MENSAJE_EXITO);
			setMensaje("Se han enviado los correos con éxito.");
			
			return SUCCESS;
		}
		return ERROR;
	}
	
	public String mostrarContenedorRecibos(){
		if(polizaDTO.getIdToPoliza() != null){
			polizaDTO = entidadService.findById(PolizaDTO.class, polizaDTO.getIdToPoliza());
			//Muestra listado de endosos con recibos
			endososList = impresionRecibosService.getMapEndososRecibos(polizaDTO.getIdToPoliza());
		    numeroEndoso = BigDecimal.ZERO;
			//Muestra listado de programa de pagos
			proPagoList = impresionRecibosService.getMapProgramaPagos(polizaDTO.getIdToPoliza(), numeroEndoso);
		    idProgPago = BigDecimal.ONE;
			//Muestra incisos Afectado
			incisoList = impresionRecibosService.getMapIncisosEndoso(polizaDTO.getIdToPoliza(), idProgPago);
		}
		return SUCCESS;
	}

	
	public String mostrarRecibosPoliza(){
		reciboList = impresionRecibosService.listarRecibosPolizaInciso(polizaDTO.getIdToPoliza(), idProgPago, idInciso);
		return SUCCESS;
	}
	
	public String imprimirReciboLlaveFiscal(){
		if(polizaDTO.getIdToPoliza() != null && idRecibo != null){
			try{
				String llaveFiscal = impresionRecibosService.getLlaveFiscalRecibo(polizaDTO.getIdToPoliza(), idRecibo);
				if(llaveFiscal == null){
					return ERROR;
				}
				InputStream is = impresionRecibosService.imprimirLlaveFiscal(llaveFiscal);
				if(is == null){
					return ERROR;
				}
				setReciboInputStream(is);
				contentType = "application/pdf";
				setFileName(llaveFiscal+".pdf");
				return SUCCESS;
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		return ERROR;
	}
	
	public String mostrarMovimientosRecibo(){
		if(polizaDTO.getIdToPoliza() != null && idRecibo != null){
			setMovimientoReciboList(impresionRecibosService.getMovimientosReciboInciso(polizaDTO.getIdToPoliza(), idRecibo, idInciso));
		}
		return SUCCESS;
	}
	
	public String regenerarRecibo(){
		if(polizaDTO.getIdToPoliza() != null && idRecibo != null){
			impresionRecibosService.regenerarFiscalRecibo(polizaDTO.getIdToPoliza(), idRecibo);
		}
		return SUCCESS;
	}
	
	private void asignarAgente(Agente agente) {
		if (polizaDTO == null) {
			polizaDTO = new PolizaDTO();
		}
		
		if (polizaDTO.getCotizacionDTO() == null) {
			polizaDTO.setCotizacionDTO(new CotizacionDTO());
		}
		
		if (polizaDTO.getCotizacionDTO().getSolicitudDTO() == null) {
			polizaDTO.getCotizacionDTO().setSolicitudDTO(new SolicitudDTO());
		}
		
		SolicitudDTO solicitud = polizaDTO.getCotizacionDTO().getSolicitudDTO();
		solicitud.setCodigoAgente(BigDecimal.valueOf(agente.getId()));
		solicitud.setAgente(agente);
	}

	public void setPolizaDTO(PolizaDTO polizaDTO) {
		this.polizaDTO = polizaDTO;
	}

	public PolizaDTO getPolizaDTO() {
		return polizaDTO;
	}

	public void setAgenteList(List<Agente> agenteList) {
		this.agenteList = agenteList;
	}

	public List<Agente> getAgenteList() {
		return agenteList;
	}

	public void setNegocioList(List<Negocio> negocioList) {
		this.negocioList = negocioList;
	}

	public List<Negocio> getNegocioList() {
		return negocioList;
	}

	public void setProductoList(List<NegocioProducto> productoList) {
		this.productoList = productoList;
	}

	public List<NegocioProducto> getProductoList() {
		return productoList;
	}

	public void setIdTcAgente(Integer idTcAgente) {
		this.idTcAgente = idTcAgente;
	}

	public Integer getIdTcAgente() {
		return idTcAgente;
	}

	public void setFiltrar(Boolean filtrar) {
		this.filtrar = filtrar;
	}

	public Boolean getFiltrar() {
		return filtrar;
	}

	public void setAccion(String accion) {
		this.accion = accion;
	}

	public String getAccion() {
		return accion;
	}

	public void setCentrosEmisores(Map<Long, String> centrosEmisores) {
		this.centrosEmisores = centrosEmisores;
	}

	public Map<Long, String> getCentrosEmisores() {
		return centrosEmisores;
	}

	public void setGerencias(Map<Long, String> gerencias) {
		this.gerencias = gerencias;
	}

	public Map<Long, String> getGerencias() {
		return gerencias;
	}

	public void setIdToNegSeccion(BigDecimal idToNegSeccion) {
		this.idToNegSeccion = idToNegSeccion;
	}

	public BigDecimal getIdToNegSeccion() {
		return idToNegSeccion;
	}

	public void setIdToNegTipoPoliza(BigDecimal idToNegTipoPoliza) {
		this.idToNegTipoPoliza = idToNegTipoPoliza;
	}

	public BigDecimal getIdToNegTipoPoliza() {
		return idToNegTipoPoliza;
	}

	public void setIdToNegProducto(BigDecimal idToNegProducto) {
		this.idToNegProducto = idToNegProducto;
	}

	public BigDecimal getIdToNegProducto() {
		return idToNegProducto;
	}

	public void setIdToNegocio(BigDecimal idToNegocio) {
		this.idToNegocio = idToNegocio;
	}

	public BigDecimal getIdToNegocio() {
		return idToNegocio;
	}

	public void setNegocioSeccion(NegocioSeccion negocioSeccion) {
		this.negocioSeccion = negocioSeccion;
	}

	public NegocioSeccion getNegocioSeccion() {
		return negocioSeccion;
	}

	public void setId(BigDecimal id) {
		this.id = id;
	}

	public BigDecimal getId() {
		return id;
	}

	public void setAgente(Agente agente) {
		this.agente = agente;
	}

	public Agente getAgente() {
		return agente;
	}

	public void setDescripcionBusquedaAgente(String descripcionBusquedaAgente) {
		this.descripcionBusquedaAgente = descripcionBusquedaAgente;
	}

	public String getDescripcionBusquedaAgente() {
		return descripcionBusquedaAgente;
	}

	public void setDescripcionBusquedaNegocio(String descripcionBusquedaNegocio) {
		this.descripcionBusquedaNegocio = descripcionBusquedaNegocio;
	}

	public String getDescripcionBusquedaNegocio() {
		return descripcionBusquedaNegocio;
	}

	public void setPlantillaInputStream(InputStream plantillaInputStream) {
		this.plantillaInputStream = plantillaInputStream;
	}

	public InputStream getPlantillaInputStream() {
		return plantillaInputStream;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public String getContentType() {
		return contentType;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFileName() {
		return fileName;
	}

	public void setPolizaList(List<PolizaDTO> polizaList) {
		this.polizaList = polizaList;
	}

	public List<PolizaDTO> getPolizaList() {
		return polizaList;
	}

	public void setEsImpresionPoliza(Integer esImpresionPoliza) {
		this.esImpresionPoliza = esImpresionPoliza;
	}

	public Integer getEsImpresionPoliza() {
		return esImpresionPoliza;
	}

	public void setTipoImpresion(int tipoImpresion) {
		this.tipoImpresion = tipoImpresion;
	}

	public int getTipoImpresion() {
		return tipoImpresion;
	}

	public void setTotalIncisos(int totalIncisos) {
		this.totalIncisos = totalIncisos;
	}

	public int getTotalIncisos() {
		return totalIncisos;
	}

	public void setValidOn(Date validOn) {
		this.validOn = validOn;
	}

	public Date getValidOn() {
		return validOn;
	}

	public void setValidOnMillis(Long validOnMillis) {
		this.validOnMillis = validOnMillis;
	}

	public Long getValidOnMillis() {
		return validOnMillis;
	}

	public void setChkcaratula(Boolean chkcaratula) {
		this.chkcaratula = chkcaratula;
	}

	public Boolean getChkcaratula() {
		return chkcaratula;
	}

	public void setChkrecibo(Boolean chkrecibo) {
		this.chkrecibo = chkrecibo;
	}

	public Boolean getChkrecibo() {
		return chkrecibo;
	}

	public void setChkinciso(Boolean chkinciso) {
		this.chkinciso = chkinciso;
	}

	public Boolean getChkinciso() {
		return chkinciso;
	}

	/**
	 * @return the chkreferencias
	 */
	public Boolean getChkreferencias() {
		return chkreferencias;
	}

	/**
	 * @param chkreferencias the chkreferencias to set
	 */
	public void setChkreferencias(Boolean chkreferencias) {
		this.chkreferencias = chkreferencias;
	}

	public void setChkcertificado(Boolean chkcertificado) {
		this.chkcertificado = chkcertificado;
	}

	public Boolean getChkcertificado() {
		return chkcertificado;
	}

	public void setChkanexos(Boolean chkanexos) {
		this.chkanexos = chkanexos;
	}

	public Boolean getChkanexos() {
		return chkanexos;
	}

	public void setNombreArchivo(String nombreArchivo) {
		this.nombreArchivo = nombreArchivo;
	}

	public String getNombreArchivo() {
		return nombreArchivo;
	}

	public void setEnviarEmailPorIncisoParameters(
			EnviarEmailPorIncisoParameters enviarEmailPorIncisoParameters) {
		this.enviarEmailPorIncisoParameters = enviarEmailPorIncisoParameters;
	}

	public EnviarEmailPorIncisoParameters getEnviarEmailPorIncisoParameters() {
		return enviarEmailPorIncisoParameters;
	}

	public void setReciboList(List<ReciboSeycos> reciboList) {
		this.reciboList = reciboList;
	}

	public List<ReciboSeycos> getReciboList() {
		return reciboList;
	}

	public void setIncisosTxt(String incisosTxt) {
		this.incisosTxt = incisosTxt;
	}

	public String getIncisosTxt() {
		return incisosTxt;
	}

	public void setReciboInputStream(InputStream reciboInputStream) {
		this.reciboInputStream = reciboInputStream;
	}

	public InputStream getReciboInputStream() {
		return reciboInputStream;
	}

	public void setIdRecibo(BigDecimal idRecibo) {
		this.idRecibo = idRecibo;
	}

	public BigDecimal getIdRecibo() {
		return idRecibo;
	}

	public void setEndososList(Map<BigDecimal, String> endososList) {
		this.endososList = endososList;
	}

	public Map<BigDecimal, String> getEndososList() {
		return endososList;
	}

	public void setProPagoList(Map<BigDecimal, String> proPagoList) {
		this.proPagoList = proPagoList;
	}

	public Map<BigDecimal, String> getProPagoList() {
		return proPagoList;
	}

	public void setIncisoList(Map<BigDecimal, String> incisoList) {
		this.incisoList = incisoList;
	}

	public Map<BigDecimal, String> getIncisoList() {
		return incisoList;
	}

	public void setNumeroEndoso(BigDecimal numeroEndoso) {
		this.numeroEndoso = numeroEndoso;
	}

	public BigDecimal getNumeroEndoso() {
		return numeroEndoso;
	}

	public void setIdProgPago(BigDecimal idProgPago) {
		this.idProgPago = idProgPago;
	}

	public BigDecimal getIdProgPago() {
		return idProgPago;
	}

	public void setMovimientoReciboList(List<MovimientoReciboDTO> movimientoReciboList) {
		this.movimientoReciboList = movimientoReciboList;
	}

	public List<MovimientoReciboDTO> getMovimientoReciboList() {
		return movimientoReciboList;
	}

	public BigDecimal getIdInciso() {
		return idInciso;
	}

	public void setIdInciso(BigDecimal idInciso) {
		this.idInciso = idInciso;
	}
}
