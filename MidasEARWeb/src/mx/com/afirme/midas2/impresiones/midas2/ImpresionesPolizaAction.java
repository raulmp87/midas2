package mx.com.afirme.midas2.impresiones.midas2;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.Locale;

import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.dto.impresiones.DatosCaratulaPoliza;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.impresionesM2.GenerarReportes;
import mx.com.afirme.midas2.service.reportes.GenerarPlantillaReporte;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.emision.ImpresionesService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
public class ImpresionesPolizaAction extends BaseAction  implements Preparable {

	private static final long serialVersionUID = -7863646405931535462L;
	private BigDecimal idToCotizacion;
	private BigDecimal idToPoliza;
	private InputStream polizaInputStream;
	private String contentType;
	private String fileName;	
	private GenerarReportes generarReportes = new GenerarReportes();
	private Locale locale = getLocale();
	private DatosCaratulaPoliza datosCaratulaPoliza = new DatosCaratulaPoliza();
	
	private ImpresionesService impresionesService;
	
	private GenerarPlantillaReporte generarPlantillaReporteService;
	
	@SuppressWarnings("unused")
	public String imprimirPoliza() {	
		try{
			TransporteImpresionDTO transporte = generarPlantillaReporteService.imprimirPoliza(idToCotizacion, idToPoliza, locale);
			polizaInputStream = new ByteArrayInputStream(transporte.getByteArray());
			contentType = "application/pdf";
			fileName = "poliza"+idToPoliza+".pdf";			
		}
		catch(Exception e){
			e.printStackTrace();
			setMensaje("Error al imprimir favor de intentarlo mas tarde");
			setTipoMensaje(TIPO_MENSAJE_ERROR);
			return ERROR;
		}			
		return SUCCESS;		
	}
	
	public BigDecimal getIdToCotizacion() {
		return idToCotizacion;
	}

	public void setIdToCotizacion(BigDecimal idToCotizacion) {
		this.idToCotizacion = idToCotizacion;
	}

	public BigDecimal getIdToPoliza() {
		return idToPoliza;
	}

	public void setIdToPoliza(BigDecimal idToPoliza) {
		this.idToPoliza = idToPoliza;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public InputStream getPolizaInputStream() {
		return polizaInputStream;
	}

	public void setPolizaInputStream(InputStream polizaInputStream) {
		this.polizaInputStream = polizaInputStream;
	}
	
	public DatosCaratulaPoliza getDatosCaratulaPoliza() {
		return datosCaratulaPoliza;
	}

	public void setDatosCaratulaPoliza(DatosCaratulaPoliza datosCaratulaPoliza) {
		this.datosCaratulaPoliza = datosCaratulaPoliza;
	}

	@Autowired
	@Qualifier("impresionesServiceEJB")
	public void setImpresionesService(ImpresionesService impresionesService) {
		this.impresionesService = impresionesService;
	}

	@Override
	public void prepare() throws Exception {
		// TODO Auto-generated method stub
		
	}
	
	@Autowired
	@Qualifier("generarPlantillaReporteEJB")
	public void setGenerarPlantillaReporteService(
			GenerarPlantillaReporte generarPlantillaReporteService) {
		this.generarPlantillaReporteService = generarPlantillaReporteService;
	}
	
	
}
