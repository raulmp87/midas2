package mx.com.afirme.midas2.impresiones.midas2;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.validation.ConstraintViolationException;

import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas.sistema.mail.ByteArrayAttachment;
import mx.com.afirme.midas.sistema.mail.ByteArrayAttachment.TipoArchivo;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.dto.MensajeDTO;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.poliza.EnvioEmailPolizaService;
import mx.com.afirme.midas2.service.poliza.EnvioEmailPolizaService.EnviarEmailPorIncisoParameters;
import mx.com.afirme.midas2.service.poliza.seguroobligatorio.SeguroObligatorioService;
import mx.com.afirme.midas2.service.reportes.GenerarPlantillaReporte;
import mx.com.afirme.midas2.service.suscripcion.endoso.GeneraPlantillaReporteBitemporalService;
import mx.com.afirme.midas2.util.MailService;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;


@Component
@Scope("prototype")
public class ImpresionesAction extends BaseAction  implements Preparable {

	private static final long serialVersionUID = -5671403887413749714L;
	private BigDecimal idToCotizacion;
	private BigDecimal idToPoliza;
	private InputStream cotizacionInputStream;
	private InputStream incisoInputStream;
	private String contentType;
	private String fileName;
	private Locale locale = getLocale();
	private String correo;
	private String nombreDestinatario;
	private Long idToNegocio;
	private String cveNegocio;
	private InputStream genericInputStream;
	private String tituloImpresion;
	private String urlSendRequest;
	private GenerarPlantillaReporte generarPlantillaReporteService;
	private GeneraPlantillaReporteBitemporalService generaPlantillaReporteBitemporalService;
	private MailService mailService;
	
	private EntidadService entidadService;
	private SeguroObligatorioService seguroObligatorioService;
	
	@Autowired
	@Qualifier("entidadEJB")
	public void setEntidadService(EntidadService entidadService){
		this.entidadService = entidadService;
	}
	
	@Autowired
	@Qualifier("seguroObligatorioServiceEJB")
	public void setSeguroObligatorioService(SeguroObligatorioService seguroObligatorioService) {
		this.seguroObligatorioService = seguroObligatorioService;
	}	
	
	/** Fecha que sirve para filtrar la información con que se llenaran los formatos de impresion */
	private Date validOn;
	/** Representación de una sola cifra o cantidad numérica para la fecha de filtrado de la información,
	 * este formato es útil para pasar la información entre la GUI y el servidor de aplicaciones, sobre todo
	 * cuando la información contiene datos precisos (hasta millisegundos) con respecto a la fecha */
	private Long validOnMillis;
	private Long recordFromMillis;
	private Date recordFrom;
	private Short claveTipoEndoso;
	private String mensajeCorreoExito;
	private EnvioEmailPolizaService envioEmailPolizaService;
	private EnviarEmailPorIncisoParameters enviarEmailPorIncisoParameters = new EnviarEmailPorIncisoParameters();
	
	public String imprimirListadoExcepciones(){					
		try{
			TransporteImpresionDTO transporte = generarPlantillaReporteService.imprimirListadoExcepciones(cveNegocio, locale);
			genericInputStream = new ByteArrayInputStream(transporte.getByteArray());
			contentType = "application/pdf";
			fileName = "listadoExcepciones.pdf";			
		}
		catch(Exception e){
			e.printStackTrace();
			setMensaje("Error al imprimir favor de intentarlo mas tarde");
			setTipoMensaje(TIPO_MENSAJE_ERROR);
			return ERROR;
		}			
		return SUCCESS;
	}	
	
	
	public String imprimirCotizacion(){
		try{
			TransporteImpresionDTO transporte = generarPlantillaReporteService.imprimirCotizacion(idToCotizacion, locale, true);
			genericInputStream = new ByteArrayInputStream(transporte.getByteArray());
			contentType = "application/pdf";
			fileName = "cotizacion_"+idToCotizacion+".pdf";			
		}
		catch(Exception e){
			e.printStackTrace();
			setMensaje("Error al imprimir favor de intentarlo mas tarde");
			setTipoMensaje(TIPO_MENSAJE_ERROR);
			return ERROR;
		}			
		return SUCCESS;		
	}	
	 
	public String enviarCotizacionPorCorreo(){		
		try{
			TransporteImpresionDTO transporte = generarPlantillaReporteService.imprimirCotizacion(idToCotizacion, locale, true);	
			//Datos para envio de correo
			List<String> destinatarios = new ArrayList<String>();
			List<ByteArrayAttachment> adjuntos = new ArrayList<ByteArrayAttachment>();
			String [] correosDestinatarios = correo.split(";");
			for(int i=0;i<=correosDestinatarios.length-1;i++){
				destinatarios.add(correosDestinatarios[i]);
			}		
			adjuntos.add(new ByteArrayAttachment("Cotizacion_"+idToCotizacion+".pdf",TipoArchivo.PDF, transporte.getByteArray()));
			mailService.sendMail(destinatarios, getText("midas.correo.cotizacion.titulo"), 
					getText("midas.correo.cotizacion.cuerpo"), adjuntos, getText("midas.correo.cotizacion.titulo"), 
					getText("midas.correo.cotizacion.saludo", new String[]{nombreDestinatario}));
			this.setMensajeCorreoExito("La cotizaci\u00F3n ha sido enviada exitosamente");
			return SUCCESS;
		}
		catch(Exception e){
			e.printStackTrace();
			setMensaje("Error al enviar el correo favor de intentarlo mas tarde");
			setTipoMensaje(TIPO_MENSAJE_ERROR);
			return ERROR;
		}			
	}	
	
	@SuppressWarnings("deprecation")
	public String enviarPolizaPorCorreo(){
		
		 //Validacion Impresion de Seguro Obligatorio
		 try{
			 PolizaDTO polizaDTO = entidadService.findById(PolizaDTO.class, idToPoliza);
			 if(polizaDTO.getClaveAnexa() != null && polizaDTO.getClaveAnexa().equals(PolizaDTO.CLAVE_POLIZA_ANEXA)){
				//Valida Seguro Obligatorio Pagado
			 	MensajeDTO mensaje = seguroObligatorioService.validaPagoSO(idToPoliza);
			 	if(mensaje != null && mensaje.getMensaje() != null && !mensaje.getMensaje().isEmpty()){
			 		setMensajeCorreoExito("No es posible imprimir la Poliza de Seguro Obligatorio hasta el pago de la Voluntaria.");
					setTipoMensaje(TIPO_MENSAJE_ERROR);
					return SUCCESS;
			 	}		
			 }
		 }catch(Exception e){
			 e.printStackTrace();
		 }
		 
		TransporteImpresionDTO transporte = null;
		try{
			
			Date today = new Date();
			if(getValidOnMillis() == null || getValidOnMillis() == 0){
				//Date today = new Date(); 
				setValidOn(today);
				setValidOnMillis(today.getTime());
			}else{
				Date accurateDate = new Date(getValidOnMillis().longValue());
				setValidOn(accurateDate);
			}
			
			if(getRecordFromMillis() == null || getRecordFromMillis() == 0){
				//Date today = new Date();
				setRecordFrom(today);
				setRecordFromMillis(today.getTime());
			}else{
				Date accurateRecordFromDate = new Date(getRecordFromMillis());
				setRecordFrom(accurateRecordFromDate);
			}
			Date accurateDate = new Date(getValidOnMillis());
			setValidOn(accurateDate);
				try{
					 DateTime validOnDT = new DateTime(getValidOnMillis());
					 DateTime recordFromDT = new DateTime(getRecordFromMillis());

					transporte = 
							 generaPlantillaReporteBitemporalService.imprimirPoliza(
								 this.getIdToPoliza(), locale, validOnDT, recordFromDT, 
								 true, false, true,  
								 false, false, getClaveTipoEndoso(), false);

				}catch(Exception e){
					e.printStackTrace();
					setMensaje("Error al imprimir favor de intentarlo mas tarde");
					setTipoMensaje(TIPO_MENSAJE_ERROR);
					return ERROR;
				}	
			//Datos para envio de correo
			List<String> destinatarios = new ArrayList<String>();
			List<ByteArrayAttachment> adjuntos = new ArrayList<ByteArrayAttachment>();
			String [] correosDestinatarios = correo.split(";");
			for(int i=0;i<=correosDestinatarios.length-1;i++){
				destinatarios.add(correosDestinatarios[i]);
			}		
			adjuntos.add(new ByteArrayAttachment("Poliza_"+idToPoliza+".pdf",TipoArchivo.PDF, transporte.getByteArray()));
			mailService.sendMail(destinatarios, getText("midas.correo.poliza.titulo"), 
					getText("midas.correo.poliza.cuerpo"), adjuntos, getText("midas.correo.poliza.titulo"), 
					getText("midas.correo.cotizacion.saludo", new String[]{nombreDestinatario}));
			this.setMensajeCorreoExito("La p\u00F3liza ha sido enviada exitosamente");
			return SUCCESS;
		}
		catch(Exception e){
			e.printStackTrace();
			setMensaje("Error al enviar el correo favor de intentarlo mas tarde");
			setTipoMensaje(TIPO_MENSAJE_ERROR);
			return ERROR;
		}			
	}
	
	public String mostrarImpresiones() {
		super.setNextFunction("sendRequestJQ(null,"+urlSendRequest+",null,null);");
		return SUCCESS;
	}
	public String imprimirConfiguracionNegocio() {
		try {
			TransporteImpresionDTO transporte = generarPlantillaReporteService.imprimirConfiguracionNegocio(idToNegocio, locale);
			genericInputStream = new ByteArrayInputStream(transporte.getByteArray());
			contentType = "application/pdf";
			fileName = "negocio_"+idToNegocio+".pdf";			
		} catch (Exception e) {
			e.printStackTrace();
			super.setMensajeError(BaseAction.MENSAJE_ERROR_GENERAL);
			return ERROR;
		}
		return SUCCESS;
	} 
	
	public String errorImpresiones(){
		return SUCCESS;
	}
	
	public String correoEnviado(){
		return SUCCESS;
	}
	
	@Override
	public void prepare() throws Exception {
		// TODO Auto-generated method stub
		
	}
	
	public String mostrarEnviarEmailPorInciso() {
		return SUCCESS;
	}
	
	public String enviarEmailPorInciso() {		
		try {
			envioEmailPolizaService.enviarEmailPorInciso(enviarEmailPorIncisoParameters);		
		} catch(Exception ex) {			
			if (ex.getCause() instanceof ConstraintViolationException) {
				ConstraintViolationException constraintViolationException = (ConstraintViolationException)ex.getCause();
				addErrors(constraintViolationException.getConstraintViolations());
			}else{
				setTipoMensaje(BaseAction.TIPO_MENSAJE_ERROR);
				setMensaje(ex.getMessage());				
			}
			return INPUT;
		}
		setTipoMensaje(BaseAction.TIPO_MENSAJE_EXITO);
		setMensaje("Se han enviado los correos con éxito.");
		return SUCCESS;
	}

	public BigDecimal getIdToCotizacion() {
		return idToCotizacion;
	}

	public void setIdToCotizacion(BigDecimal idToCotizacion) {
		this.idToCotizacion = idToCotizacion;
	}

	public BigDecimal getIdToPoliza() {
		return idToPoliza;
	}

	public void setIdToPoliza(BigDecimal idToPoliza) {
		this.idToPoliza = idToPoliza;
	}

	public InputStream getCotizacionInputStream() {
		return cotizacionInputStream;
	}

	public void setCotizacionInputStream(InputStream cotizacionInputStream) {
		this.cotizacionInputStream = cotizacionInputStream;
	}

	public InputStream getIncisoInputStream() {
		return incisoInputStream;
	}

	public void setIncisoInputStream(InputStream incisoInputStream) {
		this.incisoInputStream = incisoInputStream;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	public String getNombreDestinatario() {
		return nombreDestinatario;
	}

	public void setNombreDestinatario(String nombreDestinatario) {
		this.nombreDestinatario = nombreDestinatario;
	}
	
	public String getCveNegocio() {
		return cveNegocio;
	}

	public void setCveNegocio(String cveNegocio) {
		this.cveNegocio = cveNegocio;
	}

	public InputStream getGenericInputStream() {
		return genericInputStream;
	}

	public void setGenericInputStream(InputStream genericInputStream) {
		this.genericInputStream = genericInputStream;
	}
	
	@Autowired
	@Qualifier("generarPlantillaReporteEJB")
	public void setGenerarPlantillaReporteService(
			GenerarPlantillaReporte generarPlantillaReporteService) {
		this.generarPlantillaReporteService = generarPlantillaReporteService;
	}

	@Autowired
	@Qualifier("mailServiceEJB")
	public void setMailService(MailService mailService) {
		this.mailService = mailService;
	}
	
	@Autowired
	@Qualifier("generarPlantillaBitemporalServiceEJB")
    public void setGeneraPlantillaReporteBitemporalService(GeneraPlantillaReporteBitemporalService generaPlantillaReporteBitemporalService){
    	this.generaPlantillaReporteBitemporalService = generaPlantillaReporteBitemporalService; 
    }
	
	@Autowired
	public void setEnvioEmailPolizaService(
			EnvioEmailPolizaService envioEmailPolizaService) {
		this.envioEmailPolizaService = envioEmailPolizaService;
	}

	/**
	 * @return the idToNegocio
	 */
	public Long getIdToNegocio() {
		return idToNegocio;
	}

	/**
	 * @param idToNegocio the idToNegocio to set
	 */
	public void setIdToNegocio(Long idToNegocio) {
		this.idToNegocio = idToNegocio;
	}


	public void setValidOn(Date validOn) {
		this.validOn = validOn;
	}


	public Date getValidOn() {
		return validOn;
	}


	public void setValidOnMillis(Long validOnMillis) {
		this.validOnMillis = validOnMillis;
	}


	public Long getValidOnMillis() {
		return validOnMillis;
	}


	public void setRecordFromMillis(Long recordFromMillis) {
		this.recordFromMillis = recordFromMillis;
	}


	public Long getRecordFromMillis() {
		return recordFromMillis;
	}


	public void setRecordFrom(Date recordFrom) {
		this.recordFrom = recordFrom;
	}


	public Date getRecordFrom() {
		return recordFrom;
	}


	public void setClaveTipoEndoso(Short claveTipoEndoso) {
		this.claveTipoEndoso = claveTipoEndoso;
	}


	public Short getClaveTipoEndoso() {
		return claveTipoEndoso;
	}


	public void setMensajeCorreoExito(String mensajeCorreoExito) {
		this.mensajeCorreoExito = mensajeCorreoExito;
	}


	public String getMensajeCorreoExito() {
		return mensajeCorreoExito;
	}


	/**
	 * @return the tituloImpresion
	 */
	public String getTituloImpresion() {
		return tituloImpresion;
	}


	/**
	 * @param tituloImpresion the tituloImpresion to set
	 */
	public void setTituloImpresion(String tituloImpresion) {
		this.tituloImpresion = tituloImpresion;
	}


	/**
	 * @return the urlSendRequest
	 */
	public String getUrlSendRequest() {
		return urlSendRequest;
	}


	/**
	 * @param urlSendRequest the urlSendRequest to set
	 */
	public void setUrlSendRequest(String urlSendRequest) {
		this.urlSendRequest = urlSendRequest;
	}
	
	public EnviarEmailPorIncisoParameters getEnviarEmailPorIncisoParameters() {
		return enviarEmailPorIncisoParameters;
	}
	
	public void setEnviarEmailPorIncisoParameters(
			EnviarEmailPorIncisoParameters enviarEmailPorIncisoParameters) {
		this.enviarEmailPorIncisoParameters = enviarEmailPorIncisoParameters;
	}	
	
}
