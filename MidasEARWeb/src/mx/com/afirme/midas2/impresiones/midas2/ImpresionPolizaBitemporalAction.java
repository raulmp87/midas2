/**
 * 
 */
package mx.com.afirme.midas2.impresiones.midas2;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Locale;
import java.util.logging.Level;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.dto.impresiones.ImpresionEndosoDTO;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.service.suscripcion.endoso.GeneraPlantillaReporteBitemporalService;
import mx.com.afirme.midas2.service.suscripcion.endoso.ImpresionEndosoService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

/**
 * @author admin
 *
 */
@Component
@Scope("prototype")
public class ImpresionPolizaBitemporalAction extends BaseAction implements
		Preparable {

	private static final long serialVersionUID = 5512435458426839913L; 
	private BigDecimal idToPoliza;
	private Date validOn;
	private Boolean incluirCaratula;
	private Boolean incluirIncisos;
	private int incisoInicial;
	private int incisoFinal;
	private Locale locale = getLocale();
	private InputStream polizaInputStream = null;
	private String contentType;
	private String fileName;

	// Para impresion de endoso
	private Short numeroEndoso;
	private BigDecimal idToCotizacion;
	private Date recordFrom;
	private Short claveTipoEndoso;
	private Long recordFromMillis;
	
	// Servicios usados
	
	@Autowired
	@Qualifier("generarPlantillaBitemporalServiceEJB")
	private GeneraPlantillaReporteBitemporalService generaPlantillaRepoBitempService;
	
	@Autowired
	@Qualifier("impresionEndosoService")
	private ImpresionEndosoService impresionEndosoService; 
	
	/**
	 * Genera un archivo PDF de una poliza para poder imprimirla.
	 * @return Devuelve un archivo PDF que el usuario puede descargar desde su navegador
	 */
	public String imprimirPoliza() {
		
		LogDeMidasWeb.log("Entrando a imprimirPoliza", Level.INFO, null);
		
		LogDeMidasWeb.log("idToPoliza => " + this.idToPoliza, Level.INFO, null);
		LogDeMidasWeb.log("validOn => " + this.validOn, Level.INFO, null);
		
		try{
			//Se solicita la generación del archivo PDF
			TransporteImpresionDTO transporte = null;
			if(incluirIncisos){
//				transporte = generaPlantillaRepoBitempService.imprimirPoliza(
//				    idToPoliza, locale, TimeUtils.getDateTime(validOn), incluirCaratula, incisoInicial, incisoFinal);
			}else{
//				transporte = generaPlantillaRepoBitempService.imprimirPoliza(
//					    idToPoliza, locale, TimeUtils.getDateTime(validOn), incluirCaratula, incisoInicial, incisoFinal);
			}
			LogDeMidasWeb.log("transporte => " + transporte, Level.INFO, null);
			
			// Se establecen los valores que hacen que el navegador web presente al usuario
			// la opción de descargar el archivo para imprimirlo
			polizaInputStream = new ByteArrayInputStream(transporte.getByteArray());
			contentType = "application/pdf";
			fileName = "poliza"+idToPoliza+".pdf";			
		}catch(Exception e){
			e.printStackTrace();
			setMensaje("Error al imprimir favor de intentarlo mas tarde");
			setTipoMensaje(TIPO_MENSAJE_ERROR);
			return ERROR;
		}			
		
		LogDeMidasWeb.log("Saliendo de imprimirPoliza", Level.INFO, null);
		
		return SUCCESS;		
	}

	public String imprimirEndosoM2(){
		LogDeMidasWeb.log("Entrando a imprimirEndosoM2", Level.INFO, null);

		LogDeMidasWeb.log("idToCotizacion => " + idToCotizacion, Level.INFO, null);
		LogDeMidasWeb.log("idToPoliza => " + idToPoliza, Level.INFO, null);
		LogDeMidasWeb.log("recordFrom => " + recordFrom, Level.INFO, null);
		LogDeMidasWeb.log("claveTipoEndoso => " + claveTipoEndoso, Level.INFO, null);
		LogDeMidasWeb.log("recordFromMillis => " + recordFromMillis, Level.INFO, null);
		
		if(recordFromMillis != null){
			Date accurateRecordFromTime = new Date(recordFromMillis);
			setRecordFrom(accurateRecordFromTime);
			LogDeMidasWeb.log("recordFrom => " + recordFrom, Level.INFO, null);
		}
		
		ImpresionEndosoDTO impresionEndosoDTO = impresionEndosoService.imprimirEndosoM2(
				idToCotizacion, recordFrom, claveTipoEndoso); 

		polizaInputStream = new ByteArrayInputStream(impresionEndosoDTO.getImpresionEndoso());
		contentType = "application/pdf";
		fileName = "endoso_" + impresionEndosoDTO.getEndoso().getPolizaDTO().getNumeroPolizaFormateada() 
					+ "_" + String.format("%06d", impresionEndosoDTO.getEndoso().getId().getNumeroEndoso()) +".pdf";
		
		LogDeMidasWeb.log("Saliendo de imprimirEndosoM2", Level.INFO, null);
		return SUCCESS;
	}
	
	public String imprimirEndosoEditado(){
		LogDeMidasWeb.log("Entrando a imprimirEndosoEditado", Level.INFO, null);

		LogDeMidasWeb.log("idToCotizacion => " + idToCotizacion, Level.INFO, null);
		LogDeMidasWeb.log("idToPoliza => " + idToPoliza, Level.INFO, null);
		LogDeMidasWeb.log("recordFrom => " + recordFrom, Level.INFO, null);
		LogDeMidasWeb.log("claveTipoEndoso => " + claveTipoEndoso, Level.INFO, null);
		LogDeMidasWeb.log("recordFromMillis => " + recordFromMillis, Level.INFO, null);
		
		if(recordFromMillis != null){
			Date accurateRecordFromTime = new Date(recordFromMillis);
			setRecordFrom(accurateRecordFromTime);
			LogDeMidasWeb.log("recordFrom => " + recordFrom, Level.INFO, null);
		}
		boolean imprimirEdicionEndoso = true;
		ImpresionEndosoDTO impresionEndosoDTO = impresionEndosoService.imprimirEndosoM2(
				idToCotizacion, recordFrom, claveTipoEndoso, imprimirEdicionEndoso); 

		polizaInputStream = new ByteArrayInputStream(impresionEndosoDTO.getImpresionEndoso());
		contentType = "application/pdf";
		fileName = "endoso_" + impresionEndosoDTO.getEndoso().getPolizaDTO().getNumeroPolizaFormateada() 
					+ "_" + String.format("%06d", impresionEndosoDTO.getEndoso().getId().getNumeroEndoso()) +".pdf";	
//		if(1==1)
//		    throw new RuntimeException("Kaboom!!!");
		
		LogDeMidasWeb.log("Saliendo de imprimirEndosoM2", Level.INFO, null);
		return SUCCESS;
	}
	
	/* (non-Javadoc)
	 * @see com.opensymphony.xwork2.Preparable#prepare()
	 */
	@Override
	public void prepare() throws Exception {
		// TODO Auto-generated method stub

	}

	public BigDecimal getIdToPoliza() {
		return idToPoliza;
	}

	public void setIdToPoliza(BigDecimal idToPoliza) {
		this.idToPoliza = idToPoliza;
	}
	
	public void setValidOn(Date validOn) {
		this.validOn = validOn;
	}
	
	public Date getValidOn() {
		return validOn;
	}
	
	public Boolean getIncluirCaratula() {
		return incluirCaratula;
	}

	public void setIncluirCaratula(Boolean incluirCaratula) {
		this.incluirCaratula = incluirCaratula;
	}

	public Boolean getIncluirIncisos() {
		return incluirIncisos;
	}

	public void setIncluirIncisos(Boolean incluirIncisos) {
		this.incluirIncisos = incluirIncisos;
	}

	public int getIncisoInicial() {
		return incisoInicial;
	}

	public void setIncisoInicial(int incisoInicial) {
		this.incisoInicial = incisoInicial;
	}

	public int getIncisoFinal() {
		return incisoFinal;
	}

	public void setIncisoFinal(int incisoFinal) {
		this.incisoFinal = incisoFinal;
	}

	public Short getNumeroEndoso() {
		return numeroEndoso;
	}

	public void setNumeroEndoso(Short numeroEndoso) {
		this.numeroEndoso = numeroEndoso;
	}

	public BigDecimal getIdToCotizacion() {
		return idToCotizacion;
	}

	public void setIdToCotizacion(BigDecimal idToCotizacion) {
		this.idToCotizacion = idToCotizacion;
	}

	public Date getRecordFrom() {
		return recordFrom;
	}

	public void setRecordFrom(Date recordFrom) {
		this.recordFrom = recordFrom;
	}

	public void setClaveTipoEndoso(Short claveTipoEndoso) {
		this.claveTipoEndoso = claveTipoEndoso;
	}

	public Short getClaveTipoEndoso() {
		return claveTipoEndoso;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public InputStream getPolizaInputStream() {
		return polizaInputStream;
	}

	public void setPolizaInputStream(InputStream polizaInputStream) {
		this.polizaInputStream = polizaInputStream;
	}

	public void setGeneraPlantillaRepoBitempService(
			GeneraPlantillaReporteBitemporalService generaPlantillaRepoBitempService) {
		this.generaPlantillaRepoBitempService = generaPlantillaRepoBitempService;
	}

	public void setImpresionEndosoService(
			ImpresionEndosoService impresionEndosoService){
		this.impresionEndosoService = impresionEndosoService;
	}
	
	public void setRecordFromMillis(Long recordFromMillis){
		this.recordFromMillis = recordFromMillis;
	}
	
	public Long getRecordFromMillis(){
		return this.recordFromMillis;
	}
}

