package mx.com.afirme.midas2.impresiones.midas2;

import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import mx.com.afirme.midas.catalogos.marcavehiculo.MarcaVehiculoDTO;
import mx.com.afirme.midas.catalogos.marcavehiculo.MarcaVehiculoFacadeRemote;
import mx.com.afirme.midas.catalogos.moneda.MonedaDTO;
import mx.com.afirme.midas.catalogos.moneda.MonedaFacadeRemote;
import mx.com.afirme.midas.consultas.formapago.FormaPagoDTO;
import mx.com.afirme.midas.consultas.formapago.FormaPagoFacadeRemote;
import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDTO;
import mx.com.afirme.midas.interfaz.cliente.ClienteDTO;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.dto.impresiones.DatosCoberturasDTO;
import mx.com.afirme.midas2.dto.impresiones.DatosIncisoDTO;
import mx.com.afirme.midas2.service.negocio.cliente.NegocioClienteService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.cobertura.CoberturaService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.inciso.IncisoService;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.JasperRunManager;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;


@Component
@Scope("prototype")
public class GenerarImpresionesAction extends BaseAction  implements Preparable {

	private static final long serialVersionUID = -5671403887413749714L;

	private BigDecimal idToCotizacion;
	
	private MarcaVehiculoFacadeRemote marcaVehiculoFacadeRemote;
	private IncisoService incisoService;
	private FormaPagoFacadeRemote formaPagoFacadeRemote;
	private MonedaFacadeRemote monedaFacadeRemote;
	private NegocioClienteService negocioClienteService;
	private CoberturaService coberturaService;
	
	public byte[] imprimirInciso(CotizacionDTO cotizacionDTO, Locale locale){
		DatosIncisoDTO datosIncisoDTO=llenarDatosGenerales(cotizacionDTO);
		datosIncisoDTO=llenarDatosCliente(cotizacionDTO, datosIncisoDTO);
		List<IncisoCotizacionDTO> incisosCotizacionList=incisoService.getIncisos(cotizacionDTO.getIdToCotizacion());
		byte[] pdfPoliza = null;
		for(IncisoCotizacionDTO incisoCotizacionDTO:incisosCotizacionList){
			datosIncisoDTO=llenarDatosVehiculo(cotizacionDTO, datosIncisoDTO, incisoCotizacionDTO);			
			
		List<DatosIncisoDTO> datasourceInciso= new ArrayList<DatosIncisoDTO>();
		datasourceInciso.add(datosIncisoDTO);
		List<DatosCoberturasDTO> datasourceCoberturas = llenarDataSourceCoberturas(incisoCotizacionDTO);		 
		
		try {
			InputStream caratulaIncisoStream = this.getClass().getResourceAsStream("/mx/com/afirme/midas2/impresiones/jrxml/CaratulaIncisoM2.jrxml");
			JasperReport reporteInciso = JasperCompileManager.compileReport(caratulaIncisoStream);
			InputStream desgloseCoberturasStream = this.getClass().getResourceAsStream("/mx/com/afirme/midas2/impresiones/jrxml/DesgloseCoberturasM2.jrxml");
			JasperReport reporteCoberturas = JasperCompileManager.compileReport(desgloseCoberturasStream);
					
			Map<String, Object> parameters = new HashMap<String, Object>();
			if(locale != null)
				parameters.put(JRParameter.REPORT_LOCALE, locale);
			 	parameters.put("P_IMAGE_LOGO", Sistema.LOGO_SEGUROS);
			 	parameters.put("P_SIGNATURES", Sistema.FIRMA_FUNCIONARIO_AUTOS);
			 	parameters.put("P_DUPLICATE", Sistema.DUPLICADO);
			 	parameters.put("coberturasDataSource", datasourceCoberturas);
				parameters.put("coberturasReport", reporteCoberturas);		
				
				datasourceInciso.add(datosIncisoDTO);
			JRBeanCollectionDataSource dsPoliza = new JRBeanCollectionDataSource(datasourceInciso);
			
			pdfPoliza = JasperRunManager.runReportToPdf(reporteInciso, parameters, dsPoliza);
		} catch(JRException ex) {
			throw new RuntimeException(ex);
		}		
		}
		return pdfPoliza;
		}
	
	private DatosIncisoDTO llenarDatosGenerales(CotizacionDTO cotizacionDTO){
		DatosIncisoDTO datosIncisoDTO = new DatosIncisoDTO();		
		datosIncisoDTO.setNumeroCotizacion(cotizacionDTO.getNumeroCotizacion());
		datosIncisoDTO.setFechaInicioVigencia(cotizacionDTO.getFechaInicioVigencia());
		datosIncisoDTO.setFechaFinVigencia(cotizacionDTO.getFechaFinVigencia());
		datosIncisoDTO.setPrimaNetaCotizacion(cotizacionDTO.getPrimaNetaCotizacion());
		datosIncisoDTO.setDerechosPoliza(cotizacionDTO.getDerechosPoliza());
		datosIncisoDTO.setIdToPersonaContratante(cotizacionDTO.getIdToPersonaContratante());
		datosIncisoDTO.setPrimaNetaTotal(cotizacionDTO.getPrimaNetaTotal());
		datosIncisoDTO.setMontoRecargoPagoFraccionado(cotizacionDTO.getMontoRecargoPagoFraccionado());
		datosIncisoDTO.setMontoIVA(cotizacionDTO.getMontoIVA());		
		datosIncisoDTO.setNombreContratante(cotizacionDTO.getNombreContratante().toUpperCase());	
		return datosIncisoDTO;
	}
	
	private DatosIncisoDTO llenarDatosCliente(CotizacionDTO cotizacionDTO, DatosIncisoDTO datosIncisoDTO){		
		ClienteDTO clienteDTO = new ClienteDTO();				
		clienteDTO = negocioClienteService.getClientePorId(cotizacionDTO.getIdToPersonaContratante(), "midas");
		if(clienteDTO != null){
			datosIncisoDTO.setCalleYNumero(clienteDTO.getNombreCalle());
			datosIncisoDTO.setCodigoPostal(clienteDTO.getNombreColonia());
			datosIncisoDTO.setCiudad(clienteDTO.getNombreDelegacion());
			datosIncisoDTO.setEstado(clienteDTO.getDescripcionEstado());
			datosIncisoDTO.setCodigoPostal(clienteDTO.getCodigoPostal());
			datosIncisoDTO.setRfcContratante(clienteDTO.getCodigoRFC());
		}
		return datosIncisoDTO; 
	}
	
	private DatosIncisoDTO llenarDatosVehiculo(CotizacionDTO cotizacionDTO, DatosIncisoDTO datosIncisoDTO, IncisoCotizacionDTO incisoCotizacionDTO){		
		Long idInciso=incisoCotizacionDTO.getIncisoAutoCot().getId();
		datosIncisoDTO.setIdInciso(idInciso.toString());
		datosIncisoDTO.setDescripcionClase(incisoCotizacionDTO.getIncisoAutoCot().getDescripcionFinal());
		MarcaVehiculoDTO marcaVehiculoDTO=marcaVehiculoFacadeRemote.findById(incisoCotizacionDTO.getIncisoAutoCot().getMarcaId());
		datosIncisoDTO.setMarca(marcaVehiculoDTO.getDescripcionMarcaVehiculo());
		FormaPagoDTO formaPagoDTO = new FormaPagoDTO();
		formaPagoDTO.setIdFormaPago(Integer.valueOf(cotizacionDTO.getIdFormaPago().toString()));
		formaPagoDTO=formaPagoFacadeRemote.findById(formaPagoDTO);
		datosIncisoDTO.setDescripcionFormaPago(formaPagoDTO.getDescripcion());
		MonedaDTO monedaDTO = new MonedaDTO();
		monedaDTO = monedaFacadeRemote.findById(cotizacionDTO.getIdMoneda().shortValue());
		datosIncisoDTO.setDescripcionMoneda(monedaDTO.getDescripcion());
//		datosIncisoDTO.setIdClase(incisoCotizacionDTO.getIncisoAutoCot().getClaveEstilo());
		datosIncisoDTO.setModelo(incisoCotizacionDTO.getIncisoAutoCot().getModeloVehiculo().toString());				
		datosIncisoDTO.setNumeroMotor(incisoCotizacionDTO.getIncisoAutoCot().getNumeroMotor());
		datosIncisoDTO.setNumeroSerie(incisoCotizacionDTO.getIncisoAutoCot().getNumeroSerie());	
		datosIncisoDTO.setNumeroAsientos(marcaVehiculoDTO.getEstiloVehiculoDTOs().get(0).getNumeroAsientos().toString());
		
		return datosIncisoDTO; 
	}
	
	private List<DatosCoberturasDTO> llenarDataSourceCoberturas(IncisoCotizacionDTO incisoCotizacionDTO){
		List<DatosCoberturasDTO> coberturaDataSource = new ArrayList<DatosCoberturasDTO>();
		List<CoberturaCotizacionDTO> coberturasList=coberturaService.getCoberturasContratadas(incisoCotizacionDTO);
		for(CoberturaCotizacionDTO coberturaCotizacionDTO:coberturasList){
			DatosCoberturasDTO datosCoberturasDTO = new DatosCoberturasDTO();
			datosCoberturasDTO.setRiesgo(Integer.valueOf(coberturaCotizacionDTO.getRiesgoCotizacionLista().get(0).getId().getIdToRiesgo().toString()));
			datosCoberturasDTO.setNombreCobertura(coberturaCotizacionDTO.getCoberturaSeccionDTO().getCoberturaDTO().getDescripcion());
			datosCoberturasDTO.setSumaAsegurada(coberturaCotizacionDTO.getValorSumaAsegurada().toString());
			datosCoberturasDTO.setDeducible(coberturaCotizacionDTO.getValorDeducible().toString());
			datosCoberturasDTO.setPrimaCobertura(coberturaCotizacionDTO.getValorPrimaNeta());			
			coberturaDataSource.add(datosCoberturasDTO);
		}
		return coberturaDataSource; 
	}
	
	public BigDecimal getIdToCotizacion() {
		return idToCotizacion;
	}

	public void setIdToCotizacion(BigDecimal idToCotizacion) {
		this.idToCotizacion = idToCotizacion;
	}

	@Autowired
	@Qualifier("incisoServiceEJB")
	public void setIncisoService(IncisoService incisoService) {
		this.incisoService = incisoService;
	}
	
	@Autowired
	@Qualifier("formaPagoEJB")
	public void setFormaPagoFacadeRemote(FormaPagoFacadeRemote formaPagoFacadeRemote) {
		this.formaPagoFacadeRemote = formaPagoFacadeRemote;
	}

	@Autowired
	@Qualifier("monedaEJB")
	public void setMonedaFacadeRemote(MonedaFacadeRemote monedaFacadeRemote) {
		this.monedaFacadeRemote = monedaFacadeRemote;
	}
	
	@Autowired
	@Qualifier("negocioClienteEJB")
	public void setNegocioClienteService(NegocioClienteService negocioClienteService) {
		this.negocioClienteService = negocioClienteService;
	}
	
	@Autowired
	@Qualifier("coberturaServiceEJB")
	public void setCoberturaService(CoberturaService coberturaService) {
		this.coberturaService = coberturaService;
	}
	
	@Autowired
	@Qualifier("marcaVehiculoFacadeRemoteEJB")
	public void setMarcaVehiculoFacadeRemote(
			MarcaVehiculoFacadeRemote marcaVehiculoFacadeRemote) {
		this.marcaVehiculoFacadeRemote = marcaVehiculoFacadeRemote;
	}

	@Override
	public void prepare() throws Exception {
		// TODO Auto-generated method stub
		
	}
	
	
}
