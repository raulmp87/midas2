package mx.com.afirme.midas2.impresiones.componente;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.logging.Level;

import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionFacadeRemote;
import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.StringUtil;
import mx.com.afirme.midas.sistema.constantes.ConstantesReporte;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.clientesapi.dto.ClientesApiService;
import mx.com.afirme.midas2.domain.catalogos.CatGrupoFijo.TIPO_CATALOGO;
import mx.com.afirme.midas2.domain.movil.cliente.EnvioCaratulaParameter;
import mx.com.afirme.midas2.domain.negocio.canalventa.NegocioCanalVenta;
import mx.com.afirme.midas2.domain.suscripcion.impresion.EdicionEndoso;
import mx.com.afirme.midas2.domain.suscripcion.impresion.EdicionInciso;
import mx.com.afirme.midas2.domain.suscripcion.impresion.EdicionPoliza;
import mx.com.afirme.midas2.service.suscripcion.endoso.GeneraPlantillaReporteBitemporalService;
import mx.com.afirme.midas2.dto.MensajeDTO;
import mx.com.afirme.midas2.dto.impresiones.DatosCoberturasDTO;
import mx.com.afirme.midas2.dto.impresiones.DatosLineasDeNegocioDTO;
import mx.com.afirme.midas2.dto.impresiones.DatosPolizaDTO;
import mx.com.afirme.midas2.dto.impresiones.ImpresionEndosoDTO;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.service.bitemporal.endoso.cotizacion.auto.EndosoService;
import mx.com.afirme.midas2.service.bitemporal.poliza.inciso.IncisoViewService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas2.clientesapi.dto.EntrevistaDTO;

import org.springframework.http.ResponseEntity;


import mx.com.afirme.midas2.service.movil.cliente.ClientePolizasService;
import mx.com.afirme.midas2.service.negocio.canalventa.CanalVentaNegocioService;
import mx.com.afirme.midas2.service.negocio.impresion.NegocioUsuarioEdicionImpresionService;
import mx.com.afirme.midas2.service.poliza.seguroobligatorio.SeguroObligatorioService;
import mx.com.afirme.midas2.service.reportes.GenerarPlantillaReporte;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;
import mx.com.afirme.midas2.service.suscripcion.endoso.ImpresionEndosoService;
import mx.com.afirme.midas2.util.UtileriasWeb;

import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

import mx.com.afirme.midas2.util.FileUtils;
import mx.com.afirme.midas2.domain.sistema.seguridad.SeguridadContext;
import mx.com.afirme.midas.sistema.seguridad.Usuario;

@Component
@Scope("prototype")
public class ComponenteImpresionAction extends BaseAction  implements Preparable{
	/**
	 * @ERV
	 */	
	private static final long serialVersionUID = -5671403887413749714L;
	private static final Logger LOG = Logger.getLogger(ComponenteImpresionAction.class);
	private BigDecimal id;
	private BigDecimal idToPoliza;
	private Boolean    chkcaratula;
	private Boolean    chkrecibo;
	private Boolean    chkinciso;
	private String     txtIncios;
	private Boolean    chkcertificado;
	private Boolean    chkanexos;
	private Boolean    chkreferencias = false;
	// @etrejo
	private Boolean	   chkcondiciones;
	private int        tipoImpresion;
	private int        totalIncisos;
    private int incisoI ;
    private int incisoF ;
    private int indicador;
	private            IncisoCotizacionFacadeRemote incisoCotizacionFacadeRemote;
	private            GenerarPlantillaReporte generarPlantillaReporte;
	private InputStream polizaInputStream;
	private String     fileName;
	private String     incisoInicial=null;
	private String     incisoFinal=null;
	private String      contentType;
	private Locale locale = getLocale();
	public static final int COTIZACION = 1;
	public static final int POLIZA = 2;
	public static final int ENDOSO = 3;
	
	private Boolean    chkAviso = false;
	private Boolean    chkDerechos = false;
	private String     txtNoOrder;
	
	public static final String IDIMPRESIONPOLIZA = "idImpresionPoliza";
	public static final String CHECKAVISOPRIV = "checkAvisoPriv";
	public static final String CHECKDERECHOASEG = "checkDerechoAseg";
	public static final String ORDENTRABAJO = "ordenTrabajo";

	/** Fecha que sirve para filtrar la información con que se llenaran los formatos de impresion */
	private Date validOn;
	/** Representación de una sola cifra o cantidad numérica para la fecha de filtrado de la información,
	 * este formato es útil para pasar la información entre la GUI y el servidor de aplicaciones, sobre todo
	 * cuando la información contiene datos precisos (hasta millisegundos) con respecto a la fecha */
	private Long validOnMillis;
	private Long recordFromMillis;
	private Date recordFrom;
	private Short claveTipoEndoso;
	private Boolean esSituacionActual;
	
	protected EntidadService entidadService;
	private GeneraPlantillaReporteBitemporalService generaPlantillaReporteBitemporalService;
	private IncisoViewService incisoViewService;
	
	private SeguroObligatorioService seguroObligatorioService;
	
	private String correo;
	private boolean correoObligatorio;
	private String numeroFolio;
	private String contratante;
	
	private DatosPolizaDTO datosPoliza;
	private EdicionPoliza edicionPoliza;
	private EdicionInciso edicionInciso;
	private EdicionEndoso edicionEndoso;
	private List<DatosLineasDeNegocioDTO> datosLineasDeNegocio;
	private List<DatosCoberturasDTO> datosCoberturasPoliza;
	private String nombreVentana;
	private boolean tienePermisosModificarEdicion;
	private boolean tienePermisosImprimirEdicion;
	private boolean incluirEdicionCaratulaPoliza;
	private boolean imprimirEdicionInciso;
	private boolean existeEdicionPoliza;
	private boolean existeEdicionInciso;
	private boolean existeEdicionEndoso;
	
	private boolean tienePermisosNoBrocker;
	private boolean tienePermisosAviso;
	private boolean tienePermisosDerechos;
	
	private ClientesApiService clienteRest;
	
	private String nombreUsuario;
    private String idSesionUsuario;
    private boolean isExterno;
    
    private UsuarioService usuarioService;
	
	@Autowired
	@Qualifier("clientesApiServiceEJB")
	public void setClienteRest(ClientesApiService clienteRest) {
		this.clienteRest = clienteRest;
	}
		
	@Autowired
	private ClientePolizasService clientePolizaService;
	
	@Autowired
	private NegocioUsuarioEdicionImpresionService negocioUsuarioEdicionImpresionService;
	
	@Autowired
	private ImpresionEndosoService impresionEndosoService;
	
	@Autowired
	private EndosoService endosoService;
	
	@Autowired
	@Qualifier("canalVentaNegocioServiceEJB")
	private CanalVentaNegocioService canalVentaNegocioService;
	
	@Override
	public void prepare() throws Exception {
		if (getChkcaratula() == null)
			chkcaratula=false;
	}
	
	public UsuarioService getUsuarioService() {
		return usuarioService;
	}
	
	@Autowired
	@Qualifier("usuarioServiceEJB")
	public void setUsuarioService(UsuarioService usuarioService) {
		this.usuarioService = usuarioService;
	}
	
	public void prepareMostrarContenedorImpresion() {
		LogDeMidasWeb.log("Entrando a prepareMostrarContenedorImpresion", Level.INFO, null);
		
		LogDeMidasWeb.log("IdToPoliza => " + getIdToPoliza(), Level.INFO, null);
		LogDeMidasWeb.log("ValidOn => " + getValidOn(), Level.INFO, null);
		LogDeMidasWeb.log("ValidOnMillis => " + getValidOnMillis(), Level.INFO, null);
		LogDeMidasWeb.log("RecordFromMillis => " + getRecordFromMillis(), Level.INFO, null);
		LogDeMidasWeb.log("RecordFrom => " + getRecordFrom(), Level.INFO, null);
		LogDeMidasWeb.log("ClaveTipoEndoso => " + getClaveTipoEndoso(), Level.INFO, null);
		
		PolizaDTO polizaDTO = null;
		// Se valida si se recibió o no un valor valido o conocido para la cotizacion que es el Id
		// que se está utilizando para el cálculo de las cotizaciones
		if(getId() == null || getId().equals(new BigDecimal(-1))){
			polizaDTO = entidadService.findById(PolizaDTO.class, getIdToPoliza());
			//LogDeMidasWeb.log("polizaDTO => " + polizaDTO, Level.INFO, null);
		}
		
		if(polizaDTO == null && getId()!=null && getId().compareTo(BigDecimal.ZERO)>0) {
			List<PolizaDTO> polizaDTOList = entidadService.findByProperty(PolizaDTO.class, "cotizacionDTO.idToCotizacion", getId());
			if((polizaDTOList!=null) && !polizaDTOList.isEmpty()){
				polizaDTO = polizaDTOList.get(0);
			}
		}
		
		if(polizaDTO != null){
			
			if(getIdToPoliza()==null || getIdToPoliza().compareTo(BigDecimal.ZERO)<=0){
				setIdToPoliza(polizaDTO.getIdToPoliza());
			}
			
			// busca el No. de orden del brocker
			this.setId(polizaDTO.getCotizacionDTO().getIdToCotizacion());
			setTxtNoOrder(endosoService.obtenerNoOrderBroker(polizaDTO.getIdToPoliza()));
			
			// Obtener el ID del Negocio
			BigDecimal idToNegocio = BigDecimal.valueOf(polizaDTO.getCotizacionDTO().getSolicitudDTO().getNegocio().getIdToNegocio());
			
			// Se inicializan en false
			setChkAviso(false);
			setChkDerechos(false);
			setTienePermisosDerechos(false);
			setChkDerechos(false);
			setTienePermisosNoBrocker(false);
			
			// Obtiener los canales de venta 
			List<NegocioCanalVenta> canalVentas = canalVentaNegocioService.getCamposPorTipoYNegocio(TIPO_CATALOGO.GPO_GPOS,IDIMPRESIONPOLIZA, idToNegocio);
			
			// Evaluar los canales de venta y condicionar si estan activos
			for (NegocioCanalVenta negocioCanalVenta : canalVentas) {
				
				if(CHECKAVISOPRIV.equals(negocioCanalVenta.getCatalogo().getCodigo())){
					setTienePermisosAviso(negocioCanalVenta.getStatus());
					setChkAviso(negocioCanalVenta.getStatus());
				} else if(CHECKDERECHOASEG.equals(negocioCanalVenta.getCatalogo().getCodigo())) {
					setTienePermisosDerechos(negocioCanalVenta.getStatus());
					setChkDerechos(negocioCanalVenta.getStatus());
				} else if(ORDENTRABAJO.equals(negocioCanalVenta.getCatalogo().getCodigo())) {
					setTienePermisosNoBrocker(negocioCanalVenta.getStatus());
				}
			}
		}
		
		// Esto se implemento porque había registros que tenian fecha y hora asociados al pasarseles
		// solamente la fecha no se producía el resultado esperado.
		Date today = new Date();
		if(getValidOnMillis() == null || getValidOnMillis() == 0){
			//Date today = new Date(); 
			setValidOn(today);
			setValidOnMillis(today.getTime());
		}else{
			Date accurateDate = new Date(getValidOnMillis().longValue());
			setValidOn(accurateDate);
		}
		
		if(getRecordFromMillis() == null || getRecordFromMillis() == 0){
			//Date today = new Date();
			setRecordFrom(today);
			setRecordFromMillis(today.getTime());
		}else{
			Date accurateRecordFromDate = new Date(getRecordFromMillis());
			setRecordFrom(accurateRecordFromDate);
		}
		
		LogDeMidasWeb.log("RecordFrom => " + getRecordFrom(), Level.INFO, null);
		LogDeMidasWeb.log("ValidOn => " + getValidOn(), Level.INFO, null);
		
		// TODO: Hacer que se cuenten los incisos usando la estructra de bitemporalidad
		// y hay que tener en cuenta si se va a imprimir una poliza o una cotización para saber por medio
		// de que estructura navegar para obtener la información correspondiente a la hora de mandar a imprimir
		
		if (tipoImpresion == COTIZACION) {
			totalIncisos = incisoCotizacionFacadeRemote.contarIncisosPorCotizacion(getId());
		} else {
			if(polizaDTO != null){
			totalIncisos = incisoViewService.getNumeroIncisos(polizaDTO.getCotizacionDTO().getIdToCotizacion().longValue(),
					new DateTime(validOn), new DateTime(recordFrom)).intValue();}
		}																
		
		existeEdicionPoliza = generaPlantillaReporteBitemporalService.existeEdicionPoliza(idToPoliza, new DateTime(validOn), 
			new DateTime(recordFrom), claveTipoEndoso, esSituacionActual);
		existeEdicionInciso = generaPlantillaReporteBitemporalService.existeEdicionInciso(idToPoliza, new DateTime(validOn), 
				new DateTime(recordFrom), claveTipoEndoso);
		existeEdicionEndoso = generaPlantillaReporteBitemporalService.existeEdicionEndoso(id, new Date(recordFromMillis), claveTipoEndoso);
		tienePermisosImprimirEdicion = 
			negocioUsuarioEdicionImpresionService.tienePermisoImprimir(
					usuarioService.getUsuarioActual().getNombreUsuario(), idToPoliza);
		tienePermisosModificarEdicion = 
			negocioUsuarioEdicionImpresionService.tienePermisoEditar(
					usuarioService.getUsuarioActual().getNombreUsuario(), idToPoliza);
		
		LogDeMidasWeb.log("totalIncisos => " + totalIncisos, Level.INFO, null);
		
		LogDeMidasWeb.log("Saliendo de prepareMostrarContenedorImpresion", Level.INFO, null);
	}
	
	public String mostrarContenedorImpresion(){
		
		setNombreUsuario(SeguridadContext.getContext().getUsuario().getNombreUsuario());
		setIdSesionUsuario(SeguridadContext.getContext().getUsuario().getIdSesionUsuario());
		setEsExterno(SeguridadContext.getContext().getUsuario().isOutsider());
		System.out.println("eS eXTERNO : " + SeguridadContext.getContext().getUsuario().isOutsider());
	    return SUCCESS;		
	}
	
	public void prepareGenerarImpresion(){

	}
	
	public String errorImpresiones(){
		
		return SUCCESS;
	}
	
	
	/**
	 * 
	 * @return
	 */
	public String generarImpresion(){
		long start = System.currentTimeMillis();
		
		if(getTipoImpresion()==POLIZA)	{
			// @etrejo
			if(getChkreferencias()==false && getChkinciso()==false && getChkcaratula()==false && getChkrecibo()==false &&
					getChkinciso() ==false  && getChkanexos()==false && txtIncios.length()==0 &&
					getChkcertificado()==false && getChkcondiciones() == false 
					&&  incluirEdicionCaratulaPoliza == false && imprimirEdicionInciso == false ){
				setMensajeError("Se debe de Capturar al menos un Campo");
				return INPUT;
			}
			if(getChkcertificado()==true && getChkinciso() ==false        && 
				txtIncios.length()==0){
				setMensajeError("Se deben de Capturar Incisos al seleccionar Certificado NU");
				return INPUT;
			}
			if(tienePermisosNoBrocker && StringUtil.isEmpty(getTxtNoOrder())) { 
				setMensajeError("Se debe de Capturar el número de Orden de Trabajo Broker");
				return INPUT;
			}
		}else if (getTipoImpresion()==COTIZACION){
			if(getChkinciso()==false && getChkcaratula()==false  && txtIncios.length()==0 ){
				setMensajeError("Se debe de Capturar al menos un Campo");
				return INPUT;		
			}
			LOG.info("Iniciando impresion de cotizacion : " + getId());  
		}

		if(getChkinciso() != null && getChkinciso() == false){				
			if(getTxtIncios().length()>0){
				if(getTxtIncios().indexOf("-")>=0){
					String incisos =getTxtIncios();
					int incisoIni=incisos.toString().indexOf("-");
			        incisoInicial= (!incisos.toString().substring(0,incisos.toString().indexOf(UtileriasWeb.SEPARADOR_MEDIO)).isEmpty())?
			    		            incisos.toString().substring(0,incisos.toString().indexOf(UtileriasWeb.SEPARADOR_MEDIO)):null;

			        incisoFinal  = (!incisos.toString().substring(incisos.toString().indexOf(UtileriasWeb.SEPARADOR_MEDIO)+1,
			        											 incisos.toString().length()).isEmpty())?
					                                             incisos.toString().substring(incisos.toString().indexOf(UtileriasWeb.SEPARADOR_MEDIO)+1,
					        		                             incisos.toString().length()):null;	

				    if (incisoInicial == null || !incisos.toString().substring(0,incisoIni).matches("[0-9]*")){
						 setMensajeError("El valor antes del guion debe ser Numerico");
						 return INPUT;
				    }
					if (incisoFinal == null || !incisos.toString().substring(incisoIni+1).matches("[0-9]*")){
						 setMensajeError("El valor despues del guion debe ser Numerico");
					     return INPUT;
					}      
				
					try{
					       incisoI =Integer.parseInt(incisoInicial);
					       incisoF =Integer.parseInt(incisoFinal);
					       if(incisoI==0)incisoI=1;
					}catch(Exception e){
						 setMensajeError("Formato de Rango de Numeros Incorrecto");
						 return INPUT;					
					}
				
					if(incisoI > incisoF){
						setMensajeError("El Inciso Inicial no puede ser mayor que el Inciso Final");
						return INPUT;
					}
				}else{
			        incisoInicial=getTxtIncios();		           
	
					if (!incisoInicial.matches("[0-9]*")){
						setMensajeError("El valor debe ser Numerico");
						return INPUT;
					} 
					
					incisoI =Integer.parseInt(incisoInicial);
			        if(incisoI==0)incisoI=1;
			        incisoF = incisoI;
				}
			}
		}		
		switch (getTipoImpresion()){
		case COTIZACION:
			    try{	
			    	
			    	
			    	LOG.info("JECL is nulo " + isEsExterno(), null);
			    	
			      if(getChkinciso()==false){
			    	  
				    TransporteImpresionDTO transporte = generarPlantillaReporte.imprimirCotizacion(getId(),locale,getChkcaratula(),incisoI,incisoF,isEsExterno());
				    polizaInputStream = new ByteArrayInputStream(transporte.getByteArray());
					contentType = ConstantesReporte.TIPO_PDF;
					fileName = "cotizacion_"+getId()+".pdf";
			      }else{
				    TransporteImpresionDTO transporte = generarPlantillaReporte.imprimirCotizacion(getId(),locale,getChkcaratula(),getChkinciso(),isEsExterno());
				    polizaInputStream = new ByteArrayInputStream(transporte.getByteArray());
					contentType = ConstantesReporte.TIPO_PDF;
					fileName = "cotizacion_"+getId()+".pdf";
			      }
			
				}catch(Exception e){
					LOG.error("Error al imprimir la cotizacion", e);
					setMensaje("Error al imprimir favor de intentarlo mas tarde");
					setTipoMensaje(TIPO_MENSAJE_ERROR);
					return ERROR;
				}
		break;
		case POLIZA:
			Date accurateDate = new Date(getValidOnMillis());
			setValidOn(accurateDate);
				try{
					
					// Guardar el no. de orden del brocker
					if(getTxtNoOrder()!=null) {
						endosoService.guardaNoOrderBroker(idToPoliza, getTxtNoOrder());
					}
					
					 DateTime validOnDT = new DateTime(getValidOnMillis());
					 DateTime recordFromDT = new DateTime(getRecordFromMillis());
					 PolizaDTO polizaDTO = entidadService.findById(PolizaDTO.class, idToPoliza);	
					 
					 //Validacion Impresion de Seguro Obligatorio
					 try{
						 if(polizaDTO.getClaveAnexa() != null && polizaDTO.getClaveAnexa().equals(PolizaDTO.CLAVE_POLIZA_ANEXA)){
							//Valida Seguro Obligatorio Pagado en caso de imprecion a parte de recibo
							if(getChkcaratula() || getChkcertificado() || getChkanexos() || getChkinciso() || getChkcondiciones() || 
									(!getChkinciso() &&  incisoI > 0)){								
							 	MensajeDTO mensaje = seguroObligatorioService.validaPagoSO(idToPoliza);
							 	if(mensaje != null && mensaje.getMensaje() != null && !mensaje.getMensaje().isEmpty()){
							 		setMensajeError("No es posible imprimir la Poliza de Seguro Obligatorio hasta el pago de la Voluntaria.");
							 		return INPUT;
							 	}		
							}
						 }
					 }catch(Exception e){
						 LOG.error("Error al validar seguro obligatorio", e);
					 }
						
					 if(getChkinciso()==false){
						 TransporteImpresionDTO transporte = null;
						// @etrejo
						 if(incluirEdicionCaratulaPoliza || imprimirEdicionInciso){
							 transporte = 
								 generaPlantillaReporteBitemporalService.imprimirPoliza(
									 this.getIdToPoliza(), locale,  validOnDT,  recordFromDT,
									 getChkcaratula(),  getChkrecibo(),
									 false,  getChkcertificado(),
									 getChkcertificado(),getChkreferencias(),  incisoI,  incisoF,
									 getClaveTipoEndoso(),  esSituacionActual,
									 getChkcondiciones(),  incluirEdicionCaratulaPoliza, imprimirEdicionInciso,true, getChkAviso(), getChkDerechos());
						 }else{
							 transporte = 
								 generaPlantillaReporteBitemporalService.imprimirPoliza(
									 this.getIdToPoliza(), locale, validOnDT, recordFromDT, 
									 getChkcaratula(), getChkrecibo(), 
									 getChkcertificado(),getChkreferencias(), getChkanexos(),
									 incisoI, incisoF, getClaveTipoEndoso(), esSituacionActual,
									 getChkcondiciones(), getChkAviso(), getChkDerechos());
						 }
						 
						 addFormatoEntrevista(transporte);					 
						 
						 polizaInputStream = new ByteArrayInputStream(transporte.getByteArray());
						 contentType = ConstantesReporte.TIPO_PDF;
						 fileName = "poliza_" + polizaDTO.getNumeroPolizaFormateada() + ".pdf";
					 }else{
						// @etrejo
						 TransporteImpresionDTO transporte = null;
						 if(incluirEdicionCaratulaPoliza || imprimirEdicionInciso){
							 transporte = 
							 generaPlantillaReporteBitemporalService.imprimirPoliza(
									 this.getIdToPoliza(), locale,  validOnDT,  recordFromDT,
									 getChkcaratula(),  getChkrecibo(), true,  getChkcertificado(),
									 getChkanexos(),getChkreferencias(),  0, 0, getClaveTipoEndoso(),  false,
									 getChkcondiciones(),  incluirEdicionCaratulaPoliza, imprimirEdicionInciso,true, getChkAviso(), getChkDerechos());
						 }else{
						 	transporte= 
							 generaPlantillaReporteBitemporalService.imprimirPoliza(
								 this.getIdToPoliza(), locale, validOnDT, recordFromDT, 
								 getChkcaratula(), getChkrecibo(), true,  
								 getChkcertificado(), getChkanexos(), 
								 getClaveTipoEndoso(), getChkcondiciones(), getChkreferencias(), getChkAviso(), getChkDerechos());
						 }
						 
						 addFormatoEntrevista(transporte);
						 
						 polizaInputStream = new ByteArrayInputStream(transporte.getByteArray());
						 contentType = ConstantesReporte.TIPO_PDF;
						 fileName = "poliza_" + polizaDTO.getNumeroPolizaFormateada() + ".pdf";
						 
						 if(correoObligatorio && (correo != null && correo.trim().length() > 0)){
							 StringBuilder greeting = new StringBuilder();
							 greeting.append("Estimado(a): ");
							 greeting.append(contratante);
							 EnvioCaratulaParameter params = new EnvioCaratulaParameter();
							 params.setTitle("P\u00f3liza " + getIdToPoliza());
							 params.setMessage("En este correo se adjunta la p\00f3liza que usted di\u00f3 de alta con el n\u00famero de folio: " + numeroFolio);
							 params.setSubject("P\u00f3liza");
							 clientePolizaService.enviarCaratulaPolizaPDF(transporte, correo, params);
						 }
					 }
				}catch(Exception e){
					 LOG.error("Error al imprimir la poliza", e);
					setMensaje("Error al imprimir favor de intentarlo mas tarde");
					setTipoMensaje(TIPO_MENSAJE_ERROR);
					return ERROR;
				}
		 break;
		 case ENDOSO:
//			 try{
//				 boolean incluirCaratula = true;
//				 boolean incluirIncisos = false;
//					 TransporteImpresionDTO transporte = generaPlantillaReporteBitemporalService.imprimirPoliza(
//							 this.getIdToPoliza(), locale, TimeUtils.getDateTime(fechaValidez),
//							 incluirCaratula, incluirIncisos);
//					 polizaInputStream = new ByteArrayInputStream(transporte.getByteArray());
//					 contentType = "application/pdf";
//					 fileName = "poliza_" + getId() + ".pdf";
//			 }catch(Exception e){
//				 e.printStackTrace();
//					setMensaje("Error al imprimir favor de intentarlo mas tarde");
//					setTipoMensaje(TIPO_MENSAJE_ERROR);
//					return ERROR;
//			 }
		 default:
				setMensaje("Error al definir el tipo de Impresion: "+getTipoImpresion()+" cotizacion "+id);
				setTipoMensaje(TIPO_MENSAJE_ERROR);
		    }
			   setMensajeExito();
			
		  if (getTipoImpresion()==COTIZACION
				  || getTipoImpresion() == POLIZA)
			  LOG.info("Terminando impresion de cotizacion : " + getId() + " : " + ((System.currentTimeMillis() - start) / 1000.0));
		   
		   return SUCCESS;
	}
	
	/**
	 * Agrega el formato de entrevista al formato de impresión de póliza (en caso de que aplique).
	 * @param TransporteImpresionDTO, objeto que contiene el PDF de la póliza.
	 */
	public void addFormatoEntrevista(TransporteImpresionDTO transporte){
		
		recuperarUsuarioDeContexto();		
		
		byte[] pdfEntrevista = getEntrevistaPDF();
		 
		 if(pdfEntrevista != null){
			 
			 List<byte[]> inputByteArray = new ArrayList<byte[]>();
			 ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
			 
			 inputByteArray.add(transporte.getByteArray());
			 inputByteArray.add(pdfEntrevista);
			 FileUtils.pdfConcantenate(inputByteArray, outputStream);
			 transporte.setByteArray(outputStream.toByteArray());
		 }	
	}
	
	/**
	 * Recupera el nombreUsuario y idSesionUsuario del contexto, que se pierden en la modal de impresión
	 * de entrevista 
	 */
	private void recuperarUsuarioDeContexto(){
		
		Usuario newUsuario = new Usuario();
		newUsuario.setNombreUsuario(nombreUsuario);
		newUsuario.setIdSesionUsuario(idSesionUsuario);
		SeguridadContext.getContext().setUsuario(newUsuario);
	}

	
	public void prepareMostrarEditarImpresionPoliza(){
		prepareMostrarContenedorImpresion();
	}
	
	public String mostrarEditarImpresionPoliza(){
		try{
			 DateTime validOnDT = new DateTime(getValidOnMillis());
			 DateTime recordFromDT = new DateTime(getRecordFromMillis());
			 edicionPoliza = 
				 generaPlantillaReporteBitemporalService.obtenerEdicionPoliza(idToPoliza, validOnDT, recordFromDT, claveTipoEndoso, esSituacionActual);
			 datosPoliza = edicionPoliza.getDatosPoliza();
			 datosLineasDeNegocio = edicionPoliza.getDatosLineasDeNegocio();
			 
		}catch(Exception e){
			LOG.error("Error al mostrar la edici\u00F3n de la p\u00F3liza", e);
			setMensaje("Error al mostrar la informaci\u00F3n. Favor de intentarlo mas tarde");
			setTipoMensaje(TIPO_MENSAJE_ERROR);
			return ERROR;
		}
		return SUCCESS;
	}
	
	public String guardarImpresionPolizaEditada(){
		try{
			DateTime validOnDT = new DateTime(getValidOnMillis());
			DateTime recordFromDT = new DateTime(getRecordFromMillis());
			decodificarInformacionEdicionPoliza();
			edicionPoliza = generaPlantillaReporteBitemporalService.guardarEdicionPoliza(
					edicionPoliza, idToPoliza, validOnDT, recordFromDT, claveTipoEndoso, esSituacionActual);
		}catch(Exception e){
			LOG.error("Error al guardar la edici\u00F3n de la p\u00F3liza", e);
			setMensaje("Error al guardar la informacion favor de intentarlo mas tarde");
			setTipoMensaje(TIPO_MENSAJE_ERROR);
			return ERROR;
		}
		setMensaje("Guardado realizado correctamente");
		setTipoMensaje(TIPO_MENSAJE_EXITO);
		return SUCCESS;
	}
	
	private void decodificarInformacionEdicionPoliza(){
		if(edicionPoliza != null){
			edicionPoliza.setNombreContratante(StringUtil.decodeUriModal(edicionPoliza.getNombreContratante()));
			edicionPoliza.setTxDomicilio( StringUtil.decodeUriModal(edicionPoliza.getTxDomicilio()));
			edicionPoliza.setRfcContratante(StringUtil.decodeUriModal(edicionPoliza.getRfcContratante()));
			edicionPoliza.setObservaciones(StringUtil.decodeUriModal(edicionPoliza.getObservaciones()));
			edicionPoliza.setDatosAgenteCaratula(StringUtil.decodeUriModal(edicionPoliza.getDatosAgenteCaratula()));
		}
	}
	
	public String restaurarImpresionPolizaEditada(){
		try{
			DateTime validOnDT = new DateTime(getValidOnMillis());
			DateTime recordFromDT = new DateTime(getRecordFromMillis());
			edicionPoliza = generaPlantillaReporteBitemporalService.restaurarEdicionPoliza(
					idToPoliza, validOnDT, recordFromDT, claveTipoEndoso, esSituacionActual);
		}catch(Exception e){
			LOG.error("Error al guardar la edici\u00F3n de la p\u00F3liza", e);
			setMensaje("Error al guardar la informacion favor de intentarlo mas tarde");
			setTipoMensaje(TIPO_MENSAJE_ERROR);
			return ERROR;
		}
		setMensaje("Se ha restaurado correctamente");
		setTipoMensaje(TIPO_MENSAJE_EXITO);
		return SUCCESS;
	}
	
	public String imprimirPolizaEditada(){
		try{
			nombreVentana = "mostrarEditarImpresionPoliza";
			PolizaDTO polizaDTO = entidadService.findById(PolizaDTO.class, idToPoliza);
			DateTime validOnDT = new DateTime(getValidOnMillis());
			DateTime recordFromDT = new DateTime(getRecordFromMillis());
			TransporteImpresionDTO transporte = generaPlantillaReporteBitemporalService.imprimirEdicionPoliza(
					idToPoliza, validOnDT, recordFromDT, claveTipoEndoso, esSituacionActual,locale);
			polizaInputStream = new ByteArrayInputStream(transporte.getByteArray());
			contentType = ConstantesReporte.TIPO_PDF;
			fileName = "poliza_" + polizaDTO.getNumeroPolizaFormateada() + ".pdf";
		}catch(Exception e){
			 LOG.error("Error al imprimir", e);
				setMensaje("Error al imprimir favor de intentarlo mas tarde");
				setTipoMensaje(TIPO_MENSAJE_ERROR);
			return ERROR;
		}
		setMensaje("Se ha restaurado correctamente");
		setTipoMensaje(TIPO_MENSAJE_EXITO);
		return SUCCESS;
	}

	public String mostrarLineasNegocio(){
		try{
			 DateTime validOnDT = new DateTime(getValidOnMillis());
			 DateTime recordFromDT = new DateTime(getRecordFromMillis());
			 edicionPoliza = 
				 generaPlantillaReporteBitemporalService.obtenerEdicionPoliza(idToPoliza, validOnDT, recordFromDT, claveTipoEndoso, esSituacionActual);
			 datosPoliza = edicionPoliza.getDatosPoliza();
			 datosLineasDeNegocio = edicionPoliza.getDatosLineasDeNegocio();
			 
		}catch(Exception e){
			LOG.error("Error al mostrar las lineas de negocio", e);
			setMensaje("Error al mostrar la informaci\u00F3n. Favor de intentarlo mas tarde");
			setTipoMensaje(TIPO_MENSAJE_ERROR);
			return ERROR;
		}
		return SUCCESS;
	}
	
	public void prepareMostrarEditarImpresionInciso(){
		prepareMostrarContenedorImpresion();
	}
	
	public String mostrarEditarImpresionInciso(){
		try{
			 DateTime validOnDT = new DateTime(getValidOnMillis());
			 DateTime recordFromDT = new DateTime(getRecordFromMillis());
			 edicionInciso = 
				 generaPlantillaReporteBitemporalService.obtenerEdicionInciso(idToPoliza, validOnDT, recordFromDT, claveTipoEndoso);
			 if(edicionInciso == null){
				 edicionInciso = new EdicionInciso();
			 }
		}catch(Exception e){
			LOG.error("Error al mostrar la edici\u00F3n de la p\u00F3liza", e);
			setMensaje("Error al mostrar la informaci\u00F3n. Favor de intentarlo mas tarde");
			setTipoMensaje(TIPO_MENSAJE_ERROR);
			return ERROR;
		}
		return SUCCESS;
	}
	
	public String mostrarCoberturasPoliza(){
		try{
			 DateTime validOnDT = new DateTime(getValidOnMillis());
			 DateTime recordFromDT = new DateTime(getRecordFromMillis());    
			 datosCoberturasPoliza = generaPlantillaReporteBitemporalService.obtenerCoberturasEdicionInciso(
					 validOnDT, recordFromDT, idToPoliza, claveTipoEndoso);
			 
		}catch(Exception e){
			LOG.error("Error al mostrar las coberturas de la p\u00F3liza", e);
			setMensaje("Error al mostrar la informaci\u00F3n. Favor de intentarlo mas tarde");
			setTipoMensaje(TIPO_MENSAJE_ERROR);
			return ERROR;
		}
		return SUCCESS;
	}
	
	public String guardarImpresionIncisoEditado(){
		try{
			DateTime validOnDT = new DateTime(getValidOnMillis());
			DateTime recordFromDT = new DateTime(getRecordFromMillis());
			decodificarInformacionEdicionInciso();
			edicionInciso = generaPlantillaReporteBitemporalService.guardarEdicionInciso(
					edicionInciso, idToPoliza, validOnDT, recordFromDT, claveTipoEndoso, datosCoberturasPoliza);
		}catch(Exception e){
			LOG.error("Error al guardar la edici\u00F3n del inciso", e);
			setMensaje("Error al guardar la informacion favor de intentarlo mas tarde");
			setTipoMensaje(TIPO_MENSAJE_ERROR);
			return ERROR;
		}
		setMensaje("Guardado realizado correctamente");
		setTipoMensaje(TIPO_MENSAJE_EXITO);
		return SUCCESS;
	}
		
	private void decodificarInformacionEdicionInciso(){
		if(edicionInciso != null){
			edicionInciso.setNombreContratante(StringUtil.decodeUriModal(edicionInciso.getNombreContratante()));
			edicionInciso.setCalleNumero(StringUtil.decodeUriModal(edicionInciso.getCalleNumero()));
			edicionInciso.setRfcContratante(StringUtil.decodeUriModal(edicionInciso.getRfcContratante()));
			edicionInciso.setObservaciones(StringUtil.decodeUriModal(edicionInciso.getObservaciones()));
			edicionInciso.setDatosAgenteCaratula(StringUtil.decodeUriModal(edicionInciso.getDatosAgenteCaratula()));
			edicionInciso.setNombreContratanteInciso(StringUtil.decodeUriModal(edicionInciso.getNombreContratanteInciso()));
			edicionInciso.setColonia(StringUtil.decodeUriModal(edicionInciso.getColonia()));
			edicionInciso.setEstado(StringUtil.decodeUriModal(edicionInciso.getEstado()));
			edicionInciso.setTipoUso(StringUtil.decodeUriModal(edicionInciso.getTipoUso()));
			edicionInciso.setTipoServicio(StringUtil.decodeUriModal(edicionInciso.getTipoServicio()));
			edicionInciso.setTelefono1(StringUtil.decodeUriModal(edicionInciso.getTelefono1()));
			edicionInciso.setDescTelefono1(StringUtil.decodeUriModal(edicionInciso.getDescTelefono1()));
			edicionInciso.setTelefono2(StringUtil.decodeUriModal(edicionInciso.getTelefono2()));
			edicionInciso.setDescTelefono2(StringUtil.decodeUriModal(edicionInciso.getDescTelefono2()));
			edicionInciso.setTelefono3(StringUtil.decodeUriModal(edicionInciso.getTelefono3()));
			edicionInciso.setDescTelefono3(StringUtil.decodeUriModal(edicionInciso.getDescTelefono3()));
			if(datosCoberturasPoliza != null
					&& !datosCoberturasPoliza.isEmpty()){
				for(DatosCoberturasDTO cobertura : datosCoberturasPoliza){
					cobertura.setSumaAsegurada(StringUtil.decodeUriModal(cobertura.getSumaAsegurada()));
					cobertura.setDeducible(StringUtil.decodeUriModal(cobertura.getDeducible()));
				}
			}
		}
	}
	
	public String restaurarImpresionIncisoEditado(){
		try{
			DateTime validOnDT = new DateTime(getValidOnMillis());
			DateTime recordFromDT = new DateTime(getRecordFromMillis());
			edicionInciso = generaPlantillaReporteBitemporalService.restaurarEdicionInciso(
					idToPoliza, validOnDT, recordFromDT, claveTipoEndoso);
		}catch(Exception e){
			LOG.error("Error al guardar la edici\u00F3n del inciso", e);
			setMensaje("Error al guardar la informacion favor de intentarlo mas tarde");
			setTipoMensaje(TIPO_MENSAJE_ERROR);
			return ERROR;
		}
		setMensaje("Se ha restaurado correctamente");
		setTipoMensaje(TIPO_MENSAJE_EXITO);
		return SUCCESS;
	}
	
	public String imprimirIncisosEditados(){
		try{
			nombreVentana = "mostrarEditarImpresionInciso";
			PolizaDTO polizaDTO = entidadService.findById(PolizaDTO.class, idToPoliza);
			DateTime validOnDT = new DateTime(getValidOnMillis());
			DateTime recordFromDT = new DateTime(getRecordFromMillis());
			TransporteImpresionDTO transporte = generaPlantillaReporteBitemporalService.imprimirPoliza(
					 this.getIdToPoliza(), locale,  validOnDT,  recordFromDT,
					 false,  false, true,  false,
					 false,getChkreferencias(),  0, 0, getClaveTipoEndoso(),  false,
					 false,  false, true, true);
			polizaInputStream = new ByteArrayInputStream(transporte.getByteArray());
			contentType = ConstantesReporte.TIPO_PDF;
			fileName = "incisosPoliza_" + polizaDTO.getNumeroPolizaFormateada() + ".pdf";
		}catch(Exception e){
			 LOG.error("Error al imprimir", e);
				setMensaje("Error al imprimir favor de intentarlo mas tarde");
				setTipoMensaje(TIPO_MENSAJE_ERROR);
			return ERROR;
		}
		setMensaje("Impresion de incisos editados correcta");
		setTipoMensaje(TIPO_MENSAJE_EXITO);
		return SUCCESS;
	}
	
	public void prepareMostrarEditarImpresionEndoso(){
		this.prepareMostrarContenedorImpresion();
	}
	
	public String mostrarEditarImpresionEndoso(){
		try{
			edicionEndoso = generaPlantillaReporteBitemporalService.obtenerEdicionEndoso(
					id, new Date(recordFromMillis), claveTipoEndoso);
		}catch(Exception e){
			LOG.error("Error al mostrar la edici\u00F3n del endoso", e);
			setMensaje("Error al mostrar la informacion. Favor de intentarlo mas tarde");
			setTipoMensaje(TIPO_MENSAJE_ERROR);
			return ERROR;
		}
		return SUCCESS;
	}
	
	public String guardarImpresionEndosoEditado(){
		try{
			
			edicionEndoso = generaPlantillaReporteBitemporalService.guardarEdicionEndoso(
					edicionEndoso, id, new Date(recordFromMillis), claveTipoEndoso);
			existeEdicionEndoso = generaPlantillaReporteBitemporalService.existeEdicionEndoso(id, new Date(recordFromMillis), claveTipoEndoso);
		}catch(Exception e){
			LOG.error("Error al guardar la edici\u00F3n del inciso", e);
			setMensaje("Error al guardar la informacion favor de intentarlo mas tarde");
			setTipoMensaje(TIPO_MENSAJE_ERROR);
			return ERROR;
		}
		setMensaje("Guardado realizado correctamente");
		setTipoMensaje(TIPO_MENSAJE_EXITO);
		return SUCCESS;
	}
	
	public String restaurarImpresionEndosoEditado(){
		try{
			edicionEndoso = generaPlantillaReporteBitemporalService.restaurarEdicionEndoso(
					id, new Date(recordFromMillis), claveTipoEndoso);
			existeEdicionEndoso = generaPlantillaReporteBitemporalService.existeEdicionEndoso(id, new Date(recordFromMillis), claveTipoEndoso);
		}catch(Exception e){
			LOG.error("Error al restaurar la edici\u00F3n del endoso", e);
			setMensaje("Error al restaurar la informacion favor de intentarlo mas tarde");
			setTipoMensaje(TIPO_MENSAJE_ERROR);
			return ERROR;
		}
		setMensaje("Se ha restaurado correctamente");
		setTipoMensaje(TIPO_MENSAJE_EXITO);
		return SUCCESS;
	}
	
	public String imprimirEndosoEditado(){
		try{
			nombreVentana = "mostrarEditarImpresionEndoso";
			ImpresionEndosoDTO impresionEndoso = impresionEndosoService.imprimirEndosoM2(
					 id, new Date(recordFromMillis), claveTipoEndoso, true);
			TransporteImpresionDTO transporte = new TransporteImpresionDTO();
			transporte.setByteArray(impresionEndoso.getImpresionEndoso());
			polizaInputStream = new ByteArrayInputStream(transporte.getByteArray());
			contentType = ConstantesReporte.TIPO_PDF;
			fileName = "endoso_" + impresionEndoso.getEndoso().getPolizaDTO().getNumeroPolizaFormateada() 
			+ "_" + String.format("%06d", impresionEndoso.getEndoso().getId().getNumeroEndoso()) +".pdf";
		}catch(Exception e){
			 LOG.error("Error al imprimir", e);
				setMensaje("Error al imprimir favor de intentarlo mas tarde");
				setTipoMensaje(TIPO_MENSAJE_ERROR);
			return ERROR;
		}
		setMensaje("Impresion de Endoso editado correcta");
		setTipoMensaje(TIPO_MENSAJE_EXITO);
		return SUCCESS;
	}
	
	/**
	 * Obtiene el formato de entrevista, en caso de que no aplique la impresión, retorna null.
	 * @return byte[], formato de entrevista.
	 */
	
	private byte[] getEntrevistaPDF(){
		
		LOG.info("Consultar impresion de entrevista ");
		byte[] temp = null;
		try {
			LOG.info("Consultar poliza " + idToPoliza);
			PolizaDTO poliza = entidadService.findById(PolizaDTO.class,
					idToPoliza);
			
			LOG.info("Consultar cotizacion " + poliza.getCotizacionDTO().getIdToCotizacion());
			CotizacionDTO cotizacion = entidadService.findById(CotizacionDTO.class,
					poliza.getCotizacionDTO().getIdToCotizacion());
			LOG.info("Consultar cliente de IdToPersonaContratante " + cotizacion.getIdToPersonaContratante().longValue());
			
			EntrevistaDTO entrevistaDTO = clienteRest.findInterViews(cotizacion
					.getIdToPersonaContratante().longValue());
			
			if(cotizacion.getIdToCotizacion().equals(entrevistaDTO.getIdToCotizacion())){
				
				ResponseEntity<Long> resp = clienteRest.clienteUnificado(null, Long
						.valueOf(cotizacion.getIdToPersonaContratante().longValue()));
				
				LOG.info("Consultar entrevista de " + resp.getBody());
				
				temp = clienteRest.createPDFInterviewApp(
						resp.getBody(),
						"A",
						"484",
						String.valueOf(cotizacion.getValorTotalPrimas().setScale(2,
								BigDecimal.ROUND_HALF_UP)), null);
				LOG.info("Cliente " + temp);
			}
			else
			{
				LOG.info("La entrevista no corresponde a la póliza poliza.cotizacion: '" + cotizacion.getIdToCotizacion()
						+ "'; entrevista.cotizacion: '" + entrevistaDTO.getIdToCotizacion() + "';");
			}
		} catch (Exception e) {

			LOG.error("Entrevista error : " + e.getMessage(), e);

		}
		
		return temp;
	}
	
	public BigDecimal getId() {
	 return id;
	}

	public void setId(BigDecimal id) {
	 this.id = id;
	}
	
	public BigDecimal getIdToPoliza() {
		return idToPoliza;
	}

	public void setIdToPoliza(BigDecimal idToPoliza) {
		this.idToPoliza = idToPoliza;
	}

	public Boolean getChkcaratula() {
		return chkcaratula;
	}

	public void setChkcaratula(Boolean chkcaratula) {
		this.chkcaratula = chkcaratula;
	}

	public Boolean getChkrecibo() {
		return chkrecibo;
	}

	public void setChkrecibo(Boolean chkrecibo) {
		this.chkrecibo = chkrecibo;
	}

	public Boolean getChkinciso() {
		return chkinciso;
	}

	public void setChkinciso(Boolean chkinciso) {
		this.chkinciso = chkinciso;
	}

	public Boolean getChkcertificado() {
		return chkcertificado;
	}

	public void setChkcertificado(Boolean chkcertificado) {
		this.chkcertificado = chkcertificado;
	}

	/**
	 * @return the chkreferencias
	 */
	public Boolean getChkreferencias() {
		return chkreferencias;
	}

	/**
	 * @param chkreferencias the chkreferencias to set
	 */
	public void setChkreferencias(Boolean chkreferencias) {
		this.chkreferencias = chkreferencias;
	}

	public Boolean getChkanexos() {
		return chkanexos;
	}

	public void setChkanexos(Boolean chkanexos) {
		this.chkanexos = chkanexos;
	}

	public String getTxtIncios() {
		return txtIncios;
	}

	public void setTxtIncios(String txtIncios) {
		this.txtIncios = txtIncios;
	}

	public int getTipoImpresion() {
		return tipoImpresion;
	}

	public void setTipoImpresion(int tipoImpresion) {
		this.tipoImpresion = tipoImpresion;
	}

	public int getTotalIncisos() {
		return totalIncisos;
	}

	public void setTotalIncisos(int totalIncisos) {
		this.totalIncisos = totalIncisos;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getIncisoInicial() {
		return incisoInicial;
	}

	public void setIncisoInicial(String incisoInicial) {
		this.incisoInicial = incisoInicial;
	}

	public int getIncisoI() {
		return incisoI;
	}

	public void setIncisoI(int incisoI) {
		this.incisoI = incisoI;
	}

	public int getIncisoF() {
		return incisoF;
	}

	public void setIncisoF(int incisoF) {
		this.incisoF = incisoF;
	}

	public String getIncisoFinal() {
		return incisoFinal;
	}

	public void setIncisoFinal(String incisoFinal) {
		this.incisoFinal = incisoFinal;
	}

	public int getIndicador() {
		return indicador;
	}

	public void setIndicador(int indicador) {
		this.indicador = indicador;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public InputStream getPolizaInputStream() {
		return polizaInputStream;
	}

	public void setPolizaInputStream(InputStream polizaInputStream) {
		this.polizaInputStream = polizaInputStream;
	}

	public void setValidOn(Date validOn){
		this.validOn = validOn;
	}
	
	public Date getValidOn(){
		return this.validOn;
	}
	
	public void setValidOnMillis(Long validOnMillis) {
		this.validOnMillis = validOnMillis;
	}

	public Long getValidOnMillis() {
		return validOnMillis;
	}

	public void setRecordFromMillis(Long recordFromMillis) {
		this.recordFromMillis = recordFromMillis;
	}

	public Long getRecordFromMillis() {
		return recordFromMillis;
	}

	public Date getRecordFrom(){
		return this.recordFrom;
	}
	
	public void setRecordFrom(Date recordFrom){
		this.recordFrom = recordFrom;
	}
	
	public Short getClaveTipoEndoso(){
		return this.claveTipoEndoso;
	}
	
	public void setClaveTipoEndoso(Short claveTipoEndoso){
		this.claveTipoEndoso = claveTipoEndoso;
	}
	
	public Boolean getEsSituacionActual() {
		return esSituacionActual;
	}

	public void setEsSituacionActual(Boolean esSituacionActual) {
		this.esSituacionActual = esSituacionActual;
	}

	@Autowired
	@Qualifier("entidadEJB")
	public void setEntidadService(EntidadService entidadService){
		this.entidadService = entidadService;
	}
	
	@Autowired
	@Qualifier("incisoCotizacionFacadeRemoteEJB")
	public void setIncisoCotizacionFacadeRemote(
			IncisoCotizacionFacadeRemote incisoCotizacionFacadeRemote) {
		this.incisoCotizacionFacadeRemote = incisoCotizacionFacadeRemote;
	}


	@Autowired
	@Qualifier("generarPlantillaReporteEJB")
	public void setGenerarPlantillaReporte(
			GenerarPlantillaReporte generarPlantillaReporte) {
		this.generarPlantillaReporte = generarPlantillaReporte;
	}
	
	@Autowired
	@Qualifier("generarPlantillaBitemporalServiceEJB")
    public void setGeneraPlantillaReporteBitemporalService(GeneraPlantillaReporteBitemporalService generaPlantillaReporteBitemporalService){
    	this.generaPlantillaReporteBitemporalService = generaPlantillaReporteBitemporalService; 
    }

	@Autowired
	@Qualifier("incisoAutoServiceEJB")
	public void setIncisoViewService(IncisoViewService incisoViewService) {
		this.incisoViewService = incisoViewService;
	}

	public Boolean getChkcondiciones() {
		return (chkcondiciones == null ? false : chkcondiciones);
	}

	public void setChkcondiciones(Boolean chkcondiciones) {
		this.chkcondiciones = chkcondiciones;
	}
	
	@Autowired
	@Qualifier("seguroObligatorioServiceEJB")
	public void setSeguroObligatorioService(SeguroObligatorioService seguroObligatorioService) {
		this.seguroObligatorioService = seguroObligatorioService;
	}

	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	public boolean getCorreoObligatorio() {
		return correoObligatorio;
	}

	public void setCorreoObligatorio(boolean correoObligatorio) {
		this.correoObligatorio = correoObligatorio;
	}

	public ClientePolizasService getClientePolizaService() {
		return clientePolizaService;
	}

	public void setClientePolizaService(ClientePolizasService clientePolizaService) {
		this.clientePolizaService = clientePolizaService;
	}

	public String getNumeroFolio() {
		return numeroFolio;
	}

	public void setNumeroFolio(String numeroFolio) {
		this.numeroFolio = numeroFolio;
	}

	public String getContratante() {
		return contratante;
	}

	public void setContratante(String contratante) {
		this.contratante = contratante;
	}

	public EdicionPoliza getEdicionPoliza() {
		return edicionPoliza;
	}

	public void setEdicionPoliza(EdicionPoliza edicionPoliza) {
		this.edicionPoliza = edicionPoliza;
	}

	public EdicionInciso getEdicionInciso() {
		return edicionInciso;
	}

	public void setEdicionInciso(EdicionInciso edicionInciso) {
		this.edicionInciso = edicionInciso;
	}

	public DatosPolizaDTO getDatosPoliza() {
		return datosPoliza;
	}

	public void setDatosPoliza(DatosPolizaDTO datosPoliza) {
		this.datosPoliza = datosPoliza;
	}

	public List<DatosLineasDeNegocioDTO> getDatosLineasDeNegocio() {
		return datosLineasDeNegocio;
	}

	public void setDatosLineasDeNegocio(
			List<DatosLineasDeNegocioDTO> datosLineasDeNegocio) {
		this.datosLineasDeNegocio = datosLineasDeNegocio;
	}

	public String getNombreVentana() {
		return nombreVentana;
	}

	public void setNombreVentana(String nombreVentana) {
		this.nombreVentana = nombreVentana;
	}


	public boolean isTienePermisosModificarEdicion() {
		return tienePermisosModificarEdicion;
	}

	public void setTienePermisosModificarEdicion(
			boolean tienePermisosModificarEdicion) {
		this.tienePermisosModificarEdicion = tienePermisosModificarEdicion;
	}

	public boolean isTienePermisosImprimirEdicion() {
		return tienePermisosImprimirEdicion;
	}

	public void setTienePermisosImprimirEdicion(boolean tienePermisosImprimirEdicion) {
		this.tienePermisosImprimirEdicion = tienePermisosImprimirEdicion;
	}
	

	public boolean isIncluirEdicionCaratulaPoliza() {
		return incluirEdicionCaratulaPoliza;
	}

	public void setIncluirEdicionCaratulaPoliza(boolean incluirEdicionCaratulaPoliza) {
		this.incluirEdicionCaratulaPoliza = incluirEdicionCaratulaPoliza;
	}

	public boolean isImprimirEdicionInciso() {
		return imprimirEdicionInciso;
	}

	public void setImprimirEdicionInciso(boolean imprimirEdicionInciso) {
		this.imprimirEdicionInciso = imprimirEdicionInciso;
	}

	public EdicionEndoso getEdicionEndoso() {
		return edicionEndoso;
	}

	public void setEdicionEndoso(EdicionEndoso edicionEndoso) {
		this.edicionEndoso = edicionEndoso;
	}

	public List<DatosCoberturasDTO> getDatosCoberturasPoliza() {
		return datosCoberturasPoliza;
	}

	public void setDatosCoberturasPoliza(
			List<DatosCoberturasDTO> datosCoberturasPoliza) {
		this.datosCoberturasPoliza = datosCoberturasPoliza;
	}

	public boolean isExisteEdicionPoliza() {
		return existeEdicionPoliza;
	}

	public void setExisteEdicionPoliza(boolean existeEdicionPoliza) {
		this.existeEdicionPoliza = existeEdicionPoliza;
	}

	public boolean isExisteEdicionInciso() {
		return existeEdicionInciso;
	}

	public void setExisteEdicionInciso(boolean existeEdicionInciso) {
		this.existeEdicionInciso = existeEdicionInciso;
	}

	public boolean isExisteEdicionEndoso() {
		return existeEdicionEndoso;
	}

	public void setExisteEdicionEndoso(boolean existeEdicionEndoso) {
		this.existeEdicionEndoso = existeEdicionEndoso;
	}

	public Boolean getChkAviso() {
		return chkAviso;
	}

	public void setChkAviso(Boolean chkAviso) {
		this.chkAviso = chkAviso;
	}

	public Boolean getChkDerechos() {
		return chkDerechos;
	}

	public void setChkDerechos(Boolean chkDerechos) {
		this.chkDerechos = chkDerechos;
	}

	public String getTxtNoOrder() {
		return txtNoOrder;
	}

	public void setTxtNoOrder(String txtNoOrder) {
		this.txtNoOrder = txtNoOrder;
	}

	public boolean isTienePermisosNoBrocker() {
		return tienePermisosNoBrocker;
	}

	public void setTienePermisosNoBrocker(boolean tienePermisosNoBrocker) {
		this.tienePermisosNoBrocker = tienePermisosNoBrocker;
	}

	public boolean isTienePermisosAviso() {
		return tienePermisosAviso;
	}

	public void setTienePermisosAviso(boolean tienePermisosAviso) {
		this.tienePermisosAviso = tienePermisosAviso;
	}

	public boolean isTienePermisosDerechos() {
		return tienePermisosDerechos;
	}

	public void setTienePermisosDerechos(boolean tienePermisosDerechos) {
		this.tienePermisosDerechos = tienePermisosDerechos;
	}
	
	public String getNombreUsuario() {
		return nombreUsuario;
	}

	public void setNombreUsuario(String nombreUsuario) {
		this.nombreUsuario = nombreUsuario;
	}

	public String getIdSesionUsuario() {
		return idSesionUsuario;
	}

	public void setIdSesionUsuario(String idSesionUsuario) {
		this.idSesionUsuario = idSesionUsuario;
	}

	public boolean isExterno() {
		return isExterno;
	}

	public void setExterno(boolean isExterno) {
		this.isExterno = isExterno;
	}
	
	
}
