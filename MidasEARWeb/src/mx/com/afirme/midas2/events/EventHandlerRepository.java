package mx.com.afirme.midas2.events;

import java.util.HashMap;
import java.util.Map;

import mx.com.afirme.midas2.events.fuerzaventa.NuevoElementoEmisionHandler;

public class EventHandlerRepository {

	private Map<String, EventHandler> eventHandlerMap = new HashMap<String, EventHandler>();
	
	
	private EventHandlerRepository() {
		
		initMap();
		
	}
	
	
	private static class EventHandlerRepositoryHolder { 
		
		private static final EventHandlerRepository INSTANCE = new EventHandlerRepository();
		
		private EventHandlerRepositoryHolder() {
			
		}
		
	}

	
	public static EventHandlerRepository getInstance() {
		
		return EventHandlerRepositoryHolder.INSTANCE;
		
	}
	
	
	public EventHandler getEventHandler(String eventName) {
		
		return eventHandlerMap.get(eventName);
		
	}
	
	
	private void initMap() {
		
		eventHandlerMap.put("NuevoElementoEmision", new NuevoElementoEmisionHandler());
		
		//You can add other event handlers here 
		
	}
	
	
}
