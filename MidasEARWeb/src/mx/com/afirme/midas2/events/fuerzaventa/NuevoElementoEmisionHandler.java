package mx.com.afirme.midas2.events.fuerzaventa;

import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas2.domain.sistema.Evento;
import mx.com.afirme.midas2.events.EventHandler;
import mx.com.afirme.midas2.service.notificaciones.NotificacionesService;

public class NuevoElementoEmisionHandler implements EventHandler {
	
	@Override
	public void execute(Evento evento) {

		notificacionesService.notificarNuevoElementoEmision(evento);
				
	}
	
	public NuevoElementoEmisionHandler() {
		
		try {
			
			notificacionesService = ServiceLocator.getInstance().getEJB(NotificacionesService.class);
			
		} catch (Exception e) {
			
			throw new RuntimeException(e);
			
		}
		
	}
	
	private NotificacionesService notificacionesService;
	
}
