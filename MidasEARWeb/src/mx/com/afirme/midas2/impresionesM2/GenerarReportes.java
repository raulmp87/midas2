package mx.com.afirme.midas2.impresionesM2;

import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas2.domain.catalogos.Paquete;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.movil.cotizador.CotizacionMovilDTO;
import mx.com.afirme.midas2.domain.movil.cotizador.DatosDesglosePagosMovil;
import mx.com.afirme.midas2.domain.movil.cotizador.SolicitudCotizacionMovil;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.CoberturaCotizacionExpress;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.CotizacionExpress;
import mx.com.afirme.midas2.dto.impresiones.DatosCaratulaCotizacion;
import mx.com.afirme.midas2.dto.impresiones.DatosCaratulaInciso;
import mx.com.afirme.midas2.dto.impresiones.DatosCaratulaPoliza;
import mx.com.afirme.midas2.dto.impresiones.DatosCotizacionDTO;
import mx.com.afirme.midas2.dto.impresiones.DatosCotizacionExpressM2DTO;
import mx.com.afirme.midas2.dto.impresiones.DatosCotizacionMovilM2DTO;
import mx.com.afirme.midas2.dto.impresiones.DatosCrossTabDTO;
import mx.com.afirme.midas2.dto.impresiones.DatosDesglosePagosDTO;
import mx.com.afirme.midas2.dto.impresiones.DatosIncisoDTO;
import mx.com.afirme.midas2.dto.impresiones.DatosLineasDeNegocioDTO;
import mx.com.afirme.midas2.dto.impresiones.DatosPolizaDTO;
import mx.com.afirme.midas2.dto.impresiones.ResumenCotExpressDTO;
import mx.com.afirme.midas2.dto.impresiones.ResumenCotMovilDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.EsquemaPagoCotizacionDTO;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.JasperRunManager;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.lowagie.text.Document;
import com.lowagie.text.pdf.PRAcroForm;
import com.lowagie.text.pdf.PdfCopy;
import com.lowagie.text.pdf.PdfImportedPage;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.SimpleBookmark;

@Component
@Scope("prototype")
public class GenerarReportes {

	private EntidadService entidadService;

	public byte[] imprimirPoliza(DatosCaratulaPoliza datosCaratulaPoliza,
			Locale locale) {

		List<DatosPolizaDTO> datasourcePoliza = new ArrayList<DatosPolizaDTO>();
		List<DatosLineasDeNegocioDTO> datasourceLineasNegocio = datosCaratulaPoliza
				.getDatasourceLineasNegocio();
		byte[] pdfPoliza = null;

		try {
			InputStream caratulaPolizaStream = this
					.getClass()
					.getResourceAsStream(
							"/mx/com/afirme/midas2/impresiones/jrxml/CaratulaPolizaM2.jrxml");
			JasperReport reportePoliza = JasperCompileManager
					.compileReport(caratulaPolizaStream);
			InputStream desgloseLineasStream = this
					.getClass()
					.getResourceAsStream(
							"/mx/com/afirme/midas2/impresiones/jrxml/DesgloseLineasM2.jrxml");
			JasperReport reporteLineas = JasperCompileManager
					.compileReport(desgloseLineasStream);

			Map<String, Object> parameters = new HashMap<String, Object>();
			if (locale != null)
				parameters.put(JRParameter.REPORT_LOCALE, locale);
			parameters.put("P_IMAGE_LOGO", Sistema.LOGO_SEGUROS);
			parameters.put("P_SIGNATURES", Sistema.FIRMA_FUNCIONARIO_AUTOS);
			parameters.put("lineasDataSource", datasourceLineasNegocio);
			parameters.put("lineasReport", reporteLineas);

			datasourcePoliza.add(datosCaratulaPoliza.getDatosPolizaDTO());
			JRBeanCollectionDataSource dsPoliza = new JRBeanCollectionDataSource(
					datasourcePoliza);

			pdfPoliza = JasperRunManager.runReportToPdf(reportePoliza,
					parameters, dsPoliza);
		} catch (JRException ex) {
			throw new RuntimeException(ex);
		}
		return pdfPoliza;
	}

	public byte[] imprimirCotizacion(
			DatosCaratulaCotizacion datosCaratulaCotizacion, Locale locale) {
		List<DatosCotizacionDTO> datasourceCotizacion = new ArrayList<DatosCotizacionDTO>();
		List<DatosLineasDeNegocioDTO> datasourceLineasNegocio = datosCaratulaCotizacion
				.getDatasourceLineasNegocio();
		List<DatosDesglosePagosDTO> datasourceDesglosePagos = datosCaratulaCotizacion
				.getDatasourceDesglosePagos();
		byte[] pdfCotizacion = null;

		try {
			InputStream caratulaCotizacionStream = this
					.getClass()
					.getResourceAsStream(
							"/mx/com/afirme/midas2/impresiones/jrxml/CaratulaCotizacionM2.jrxml");
			JasperReport reporteCotizacion = JasperCompileManager
					.compileReport(caratulaCotizacionStream);
			InputStream desgloseLineasStream = this
					.getClass()
					.getResourceAsStream(
							"/mx/com/afirme/midas2/impresiones/jrxml/DesgloseLineasM2.jrxml");
			JasperReport reporteLineas = JasperCompileManager
					.compileReport(desgloseLineasStream);
			InputStream desgloseEsquemaPagoStream = this
					.getClass()
					.getResourceAsStream(
							"/mx/com/afirme/midas2/impresiones/jrxml/DesglosePagosCotizacionM2.jrxml");
			JasperReport reporteEsquemaPago = JasperCompileManager
					.compileReport(desgloseEsquemaPagoStream);

			Map<String, Object> parameters = new HashMap<String, Object>();
			if (locale != null)
				parameters.put(JRParameter.REPORT_LOCALE, locale);
			parameters.put("P_IMAGE_LOGO", Sistema.LOGO_SEGUROS);
			parameters.put("P_SIGNATURE", Sistema.FIRMA_FUNCIONARIO_AUTOS);
			parameters.put("lineasDataSource", datasourceLineasNegocio);
			parameters.put("lineasReport", reporteLineas);
			parameters.put("dataSourceEsquemaPago", datasourceDesglosePagos);
			parameters.put("reporteEsquemaPago", reporteEsquemaPago);
			datasourceCotizacion.add(datosCaratulaCotizacion
					.getDatosCotizacionDTO());
			JRBeanCollectionDataSource dsCotizacion = new JRBeanCollectionDataSource(
					datasourceCotizacion);

			pdfCotizacion = JasperRunManager.runReportToPdf(reporteCotizacion,
					parameters, dsCotizacion);
		} catch (JRException ex) {
			throw new RuntimeException(ex);
		}
		return pdfCotizacion;
	}

	public byte[] imprimirInciso(List<DatosCaratulaInciso> incisosList,
			Locale locale) {
		byte[] pdfInciso = null;

		try {
			InputStream caratulaIncisoStream = this
					.getClass()
					.getResourceAsStream(
							"/mx/com/afirme/midas2/impresiones/jrxml/CaratulaIncisoM2.jrxml");
			JasperReport reporteInciso = JasperCompileManager
					.compileReport(caratulaIncisoStream);
			InputStream desgloseCoberturasStream = this
					.getClass()
					.getResourceAsStream(
							"/mx/com/afirme/midas2/impresiones/jrxml/DesgloseCoberturasM2.jrxml");
			JasperReport reporteCoberturas = JasperCompileManager
					.compileReport(desgloseCoberturasStream);
			Map<String, Object> parameters = new HashMap<String, Object>();
			List<DatosIncisoDTO> reporteDatasourceInciso = new ArrayList<DatosIncisoDTO>();
			for (DatosCaratulaInciso dataSourceInciso : incisosList) {
				dataSourceInciso.getDataSourceIncisos().setCoberturas(
						dataSourceInciso.getDataSourceCoberturas());
				reporteDatasourceInciso.add(dataSourceInciso
						.getDataSourceIncisos());
				if (locale != null)
					parameters.put(JRParameter.REPORT_LOCALE, locale);
				parameters.put("P_IMAGE_LOGO", Sistema.LOGO_SEGUROS);
				parameters.put("P_SIGNATURES", Sistema.FIRMA_FUNCIONARIO_AUTOS);
				parameters.put("coberturasDataSource",
						dataSourceInciso.getDataSourceCoberturas());
				parameters.put("coberturasReport", reporteCoberturas);
			}
			JRBeanCollectionDataSource dsInciso = new JRBeanCollectionDataSource(
					reporteDatasourceInciso);

			pdfInciso = JasperRunManager.runReportToPdf(reporteInciso,
					parameters, dsInciso);
		} catch (JRException ex) {
			throw new RuntimeException(ex);
		}
		return pdfInciso;
	}

	public byte[] imprimirCotizacionExpress(
			List<ResumenCotExpressDTO> cotizacionExpressList, Locale locale,
			List<BigDecimal> formaPagoMatriz, List<BigDecimal> paquetesMatriz) {
		List<DatosCotizacionExpressM2DTO> datasourceCotizacionExpress = new ArrayList<DatosCotizacionExpressM2DTO>();
		datasourceCotizacionExpress
				.add(llenarDatosCotizacionExpress(cotizacionExpressList.get(0)
						.getCotizacionExpress()));
		List<DatosCrossTabDTO> dataSourcePaquetes = llenarDatosCoberturasExpress(
				cotizacionExpressList, formaPagoMatriz, paquetesMatriz);
		List<DatosCrossTabDTO> dataSourceCostos = llenarPrimasPaquetes(
				cotizacionExpressList.get(0).getEsquemaPagoExpress(),
				formaPagoMatriz, paquetesMatriz);

		byte[] pdfPoliza = null;
		try {
			InputStream cotizacionExpressStream = this
					.getClass()
					.getResourceAsStream(
							"/mx/com/afirme/midas2/impresiones/jrxml/CotizacionExpresM3.jrxml");
			JasperReport reporteCotizacionExpress = JasperCompileManager
					.compileReport(cotizacionExpressStream);
			InputStream paquetesStream = this
					.getClass()
					.getResourceAsStream(
							"/mx/com/afirme/midas2/impresiones/jrxml/crosstabtest.jrxml");
			JasperReport reportePaquetes = JasperCompileManager
					.compileReport(paquetesStream);
			InputStream costosStream = this
					.getClass()
					.getResourceAsStream(
							"/mx/com/afirme/midas2/impresiones/jrxml/reporteCostos.jrxml");
			JasperReport reporteCostos = JasperCompileManager
					.compileReport(costosStream);

			Map<String, Object> parameters = new HashMap<String, Object>();
			if (locale != null)
				parameters.put(JRParameter.REPORT_LOCALE, locale);
			parameters.put("ENCABEZADO_EXPRESS", Sistema.ENCABEZADO_EXPRESS);
			parameters.put("reportePaquetes", reportePaquetes);
			parameters.put("dataSourcePaquetes", dataSourcePaquetes);
			parameters.put("reporteCostos", reporteCostos);
			parameters.put("dataSourceCostos", dataSourceCostos);

			JRBeanCollectionDataSource dsCotizacionExpress = new JRBeanCollectionDataSource(
					datasourceCotizacionExpress);
			pdfPoliza = JasperRunManager.runReportToPdf(
					reporteCotizacionExpress, parameters, dsCotizacionExpress);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RuntimeException(ex);
		}
		return pdfPoliza;
	}

	private DatosCotizacionExpressM2DTO llenarDatosCotizacionExpress(
			CotizacionExpress cotizacionExpress) {
		DatosCotizacionExpressM2DTO datosCotizacionExpressM2DTO = new DatosCotizacionExpressM2DTO();
		datosCotizacionExpressM2DTO.setIdCotizacion("COTIZACION EXPRESS");
		datosCotizacionExpressM2DTO.setDescripcion(cotizacionExpress
				.getModeloVehiculo().getEstiloVehiculoDTO().getDescription());
		datosCotizacionExpressM2DTO.setEstado(cotizacionExpress.getEstado()
				.getDescription());
		datosCotizacionExpressM2DTO.setMarca(cotizacionExpress
				.getModeloVehiculo().getEstiloVehiculoDTO()
				.getMarcaVehiculoDTO().getDescripcionMarcaVehiculo());
		datosCotizacionExpressM2DTO.setNombreCliente("Cliente");
		datosCotizacionExpressM2DTO.setModelo(cotizacionExpress
				.getModeloVehiculo().getId().getModeloVehiculo().toString());
		datosCotizacionExpressM2DTO.setFecha(new Date());
		return datosCotizacionExpressM2DTO;
	}

	private List<DatosCrossTabDTO> llenarDatosCoberturasExpress(
			List<ResumenCotExpressDTO> cotizacionExpressList,
			List<BigDecimal> formaPagoMatriz, List<BigDecimal> paquetesMatriz) {

		List<DatosCrossTabDTO> list = new ArrayList<DatosCrossTabDTO>(1);
		for (ResumenCotExpressDTO item : cotizacionExpressList) {
			if (paquetesMatriz.contains(new BigDecimal(item.getPaquete()
					.getId()))
					&& formaPagoMatriz.contains(new BigDecimal(item
							.getFormaPago().getIdFormaPago()))) {
				List<CoberturaCotizacionExpress> listaCoberturas = item
						.getCotizacionExpress()
						.getCoberturaCotizacionExpresses();
				for (CoberturaCotizacionExpress item2 : listaCoberturas) {
					String deducible = "";
					if (item2.getCoberturaPrimaCotExpress().getPctDeducible() != null
							&& !item2.getCoberturaPrimaCotExpress()
									.getPctDeducible().equals(0)) {
						deducible = item2.getCoberturaPrimaCotExpress()
								.getPctDeducible().toString()
								+ " %";
					} else {
						deducible = "";
					}
					DatosCrossTabDTO datos = new DatosCrossTabDTO(item
							.getPaquete().getDescripcion().trim(), item2
							.getCoberturaPrimaCotExpress().getCobertura()
							.getNombreComercial(), item2.getSumaAseguradaStr(),
							deducible);
					datos.setIdHeader(new BigDecimal(item.getPaquete().getId()));
					datos.setIdRow(item2.getCoberturaPrimaCotExpress()
							.getCobertura().getIdToCobertura());
					list.add(datos);
				}
			}
		}

		return ordenarDatosCoberturasExpress(list);
	}

	private List<DatosCrossTabDTO> ordenarDatosCoberturasExpress(
			List<DatosCrossTabDTO> list) {
		Collections.sort(list, new Comparator<DatosCrossTabDTO>() {
			public int compare(DatosCrossTabDTO n1, DatosCrossTabDTO n2) {
				int valor = 0;
				if (n1.getIdRow().compareTo(n2.getIdRow()) < 0) {
					valor = -1;
				}
				if (n1.getIdRow().compareTo(n2.getIdRow()) > 0) {
					valor = 1;
				}
				if (n1.getIdRow().compareTo(n2.getIdRow()) == 0
						&& n1.getHeader().compareTo(n2.getHeader()) < 0) {
					valor = -1;
				}
				if (n1.getIdRow().compareTo(n2.getIdRow()) == 0
						&& n1.getHeader().compareTo(n2.getHeader()) > 0) {
					valor = 1;
				}
				return valor;
			}
		});

		BigDecimal idHeaderAnt = new BigDecimal(-1);
		BigDecimal idRowAnt = new BigDecimal(-1);
		for (int i = 0; i < list.size(); i++) {
			DatosCrossTabDTO datos = list.get(i);
			if (datos.getIdHeader().equals(idHeaderAnt)
					&& datos.getIdRow().equals(idRowAnt)) {
				list.remove(i);
				i--;
			} else {
				idHeaderAnt = datos.getIdHeader();
				idRowAnt = datos.getIdRow();
			}
		}

		return list;
	}

	private List<DatosCrossTabDTO> llenarPrimasPaquetes(
			Map<String, EsquemaPagoCotizacionDTO> esquemaPagoExpress,
			List<BigDecimal> formaPagoMatriz, List<BigDecimal> paquetesMatriz) {
		List<DatosCrossTabDTO> primaPaquetes = new ArrayList<DatosCrossTabDTO>(
				1);
		for (BigDecimal formaPago : formaPagoMatriz) {
			for (BigDecimal paquete : paquetesMatriz) {
				String llaveCompuesta = paquete + "-" + formaPago;
				EsquemaPagoCotizacionDTO esquemaPagoCotizacionDTO = esquemaPagoExpress
						.get(llaveCompuesta);
				if (esquemaPagoCotizacionDTO != null) {

					String pagoInicial = String.format("%.2f",
							esquemaPagoCotizacionDTO.getPagoInicial()
									.getImporte());

					String pagoSubsecuente = new String("0");
					if (esquemaPagoCotizacionDTO.getPagoSubsecuente() != null) {
						pagoSubsecuente = String.format("%.2f",
								esquemaPagoCotizacionDTO.getPagoSubsecuente()
										.getImporte());
					}
					String descrPaquete = paquete.toString();
					if (this.getEntidadService() != null) {
						Paquete paqueteDTO = entidadService.findById(
								Paquete.class, paquete.longValue());
						descrPaquete = paqueteDTO.getDescripcion();
					}
					DatosCrossTabDTO datos = new DatosCrossTabDTO(descrPaquete,
							esquemaPagoCotizacionDTO.getFormaPagoDTO()
									.getDescripcion(), pagoInicial.toString(),
							pagoSubsecuente.toString());
					datos.setIdHeader(paquete);
					datos.setIdRow(formaPago);
					datos.setRow2(esquemaPagoCotizacionDTO.getCantidad()
							.toString());
					primaPaquetes.add(datos);
				}
			}
		}
		return ordenarDatosCoberturasExpress(primaPaquetes);
	}

	private List<DatosCrossTabDTO> llenarPrimasPaquetesMovil(
			List<ResumenCotMovilDTO> cotizacionMovilList) {
		List<DatosCrossTabDTO> primaPaquetes = new ArrayList<DatosCrossTabDTO>(
				1);
		for (ResumenCotMovilDTO resumenCotMovilDTO : cotizacionMovilList) {
			String descrPaquete = resumenCotMovilDTO.getPaquete()
					.getDescripcion();
			DatosCrossTabDTO datos = new DatosCrossTabDTO(descrPaquete,
					resumenCotMovilDTO.getFormaPago().getDescripcion(),
					UtileriasWeb.formatoMoneda(
							resumenCotMovilDTO.getDatosDesglosePagosMovil()
									.getPrimerPago()).toString(), UtileriasWeb
							.formatoMoneda(
									resumenCotMovilDTO
											.getDatosDesglosePagosMovil()
											.getPagosSubsecuentes()).toString());
			datos.setIdHeader(new BigDecimal(resumenCotMovilDTO.getPaquete()
					.getId()));
			datos.setIdRow(new BigDecimal(resumenCotMovilDTO.getFormaPago()
					.getIdFormaPago()));
			datos.setRow2(resumenCotMovilDTO.getCotizacionMovil()
					.getTarifaamplia().toString());
			primaPaquetes.add(datos);
		}
		return ordenarDatosCoberturasExpress(primaPaquetes);
	}

	public void pdfConcantenate(List inputByteArray, OutputStream outputStream) {
		try {
			int pageOffset = 0;
			int f = 0;
			ArrayList master = new ArrayList();
			Document document = null;
			PdfCopy writer = null;
			// we create a reader for a certain document
			Iterator iterator = inputByteArray.iterator();
			while (iterator.hasNext()) {
				byte[] data = (byte[]) iterator.next();
				if (data == null || data.length == 0) {
					f++;
					continue;
				}
				PdfReader reader = new PdfReader(data);
				reader.consolidateNamedDestinations();
				// we retrieve the total number of pages
				int n = reader.getNumberOfPages();
				List bookmarks = SimpleBookmark.getBookmark(reader);
				if (bookmarks != null) {
					if (pageOffset != 0)
						SimpleBookmark.shiftPageNumbers(bookmarks, pageOffset,
								null);
					master.addAll(bookmarks);
				}
				pageOffset += n;
				if (f == 0) {
					// step 1: creation of a document-object
					document = new Document(reader.getPageSizeWithRotation(1));
					// step 2: we create a writer that listens to the document
					writer = new PdfCopy(document, outputStream);
					// step 3: we open the document
					document.open();
				}
				// step 4: we add content
				PdfImportedPage page;
				for (int i = 0; i < n;) {
					++i;
					page = writer.getImportedPage(reader, i);
					writer.addPage(page);
				}
				PRAcroForm form = reader.getAcroForm();
				if (form != null) {
					writer.copyAcroForm(reader);
				}
				f++;
			}
			if (!master.isEmpty()) {
				writer.setOutlines(master);
			}
			// step 5: we close the document
			if (document != null) {
				document.close();
			}
		} catch (Exception e) {
		}
	}

	public byte[] imprimirCotizacionMovil(
			List<ResumenCotMovilDTO> cotizacionMovilList, Locale locale,
			Usuario usuario, SolicitudCotizacionMovil solicitud) {
		List<DatosCotizacionMovilM2DTO> datasourceCotizacionMovil = new ArrayList<DatosCotizacionMovilM2DTO>();
		datasourceCotizacionMovil.add(llenarDatosCotizacionMovil(
				cotizacionMovilList.get(0).getCotizacionMovil(), usuario));
		// List<DatosCrossTabDTO> dataSourcePaquetes =
		// llenarDatosCoberturasMovil(cotizacionMovilList, formaPagoMatriz,
		// paquetesMatriz);
		List<DatosCrossTabDTO> dataSourceCostos = llenarPrimasPaquetesMovil(cotizacionMovilList);
		byte[] pdfPoliza = null;
		try {
			InputStream cotizacionExpressStream = this
					.getClass()
					.getResourceAsStream(
							!SolicitudCotizacionMovil.TIPO_COTIZACION_PORTAL
									.equals(solicitud.getTipoCotizacion()) ? "/mx/com/afirme/midas2/impresiones/jrxml/CotizacionMovil.jrxml"
									: "/mx/com/afirme/midas2/impresiones/jrxml/CotizacionPortal.jrxml");
			JasperReport reporteCotizacionExpress = JasperCompileManager
					.compileReport(cotizacionExpressStream);
			InputStream paquetesStream = this
					.getClass()
					.getResourceAsStream(
							"/mx/com/afirme/midas2/impresiones/jrxml/crosstabtest.jrxml");
			JasperReport reportePaquetes = JasperCompileManager
					.compileReport(paquetesStream);
			InputStream costosStream = this
					.getClass()
					.getResourceAsStream(
							!SolicitudCotizacionMovil.TIPO_COTIZACION_PORTAL
									.equals(solicitud.getTipoCotizacion()) ? "/mx/com/afirme/midas2/impresiones/jrxml/reporteCostosMovil.jrxml"
									: "/mx/com/afirme/midas2/impresiones/jrxml/reporteCostos.jrxml");
			JasperReport reporteCostos = JasperCompileManager
					.compileReport(costosStream);

			Map<String, Object> parameters = new HashMap<String, Object>();
			if (locale != null)
				parameters.put(JRParameter.REPORT_LOCALE, locale);
			parameters.put("ENCABEZADO_EXPRESS", Sistema.ENCABEZADO_EXPRESS);
			parameters.put("COBERTURAS", Sistema.COBERTURAS);
			parameters.put("reportePaquetes", reportePaquetes);
			// parameters.put("dataSourcePaquetes", dataSourcePaquetes);
			parameters.put("reporteCostos", reporteCostos);
			parameters.put("dataSourceCostos", dataSourceCostos);

			JRBeanCollectionDataSource dsCotizacionMovil = new JRBeanCollectionDataSource(
					datasourceCotizacionMovil);
			pdfPoliza = JasperRunManager.runReportToPdf(
					reporteCotizacionExpress, parameters, dsCotizacionMovil);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RuntimeException(ex);
		}
		return pdfPoliza;
	}

	private DatosCotizacionMovilM2DTO llenarDatosCotizacionMovil(
			CotizacionMovilDTO cotizacionMovil, Usuario usuario) {
		DatosCotizacionMovilM2DTO datosCotizacionMovilM2DTO = new DatosCotizacionMovilM2DTO();
		datosCotizacionMovilM2DTO.setIdCotizacion(cotizacionMovil
				.getIdtocotizacionmidas().toString());
		datosCotizacionMovilM2DTO.setDescripcion(cotizacionMovil
				.getDescripciontarifa());
		datosCotizacionMovilM2DTO.setEstado(cotizacionMovil.getEstado());
		datosCotizacionMovilM2DTO.setMarca(cotizacionMovil.getMarcavehiculo());
		datosCotizacionMovilM2DTO.setNombreCliente(UtileriasWeb
				.regresaObjectEsNuloParametro(usuario.getNombre(), "Cliente")
				.toString()
				.concat(" ")
				.concat(UtileriasWeb
						.regresaObjectEsNuloParametro(
								usuario.getApellidoPaterno(), "")
						.toString()
						.concat(" ")
						.concat(UtileriasWeb.regresaObjectEsNuloParametro(
								usuario.getApellidoMaterno(), "").toString())));
		datosCotizacionMovilM2DTO.setModelo(cotizacionMovil.getModelovehiculo()
				.toString());
		datosCotizacionMovilM2DTO.setNumeroTelefonoAgente(cotizacionMovil
				.getDescuentoAgente().getNumerotelefono().toString());
		datosCotizacionMovilM2DTO.setNumeroTelefonoCliente(UtileriasWeb
				.regresaObjectEsNuloParametro(usuario.getTelefonoCelular(),
						"S/N").toString());
		datosCotizacionMovilM2DTO.setFecha(new Date());
		datosCotizacionMovilM2DTO.setEmailAgente(cotizacionMovil
				.getDescuentoAgente().getEmail().toString());
		datosCotizacionMovilM2DTO.setEmailCliente(UtileriasWeb
				.regresaObjectEsNuloParametro(usuario.getEmail(), "")
				.toString());
		datosCotizacionMovilM2DTO.setClavePromo(UtileriasWeb
				.regresaObjectEsNuloParametro(cotizacionMovil.getClavepromo(), "")
				.toString());
		datosCotizacionMovilM2DTO.setClaveAgente(UtileriasWeb
				.regresaObjectEsNuloParametro(cotizacionMovil.getClaveagente(), "")
				.toString());
		
		return datosCotizacionMovilM2DTO;
	}

	private static ApplicationContext ctx;

	public static void setApplicationContext(
			ApplicationContext applicationContext) {
		ctx = applicationContext;
	}

	public static ApplicationContext getApplicationContext() {
		return ctx;
	}

	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}

	public EntidadService getEntidadService() {
		return entidadService;
	}
}
