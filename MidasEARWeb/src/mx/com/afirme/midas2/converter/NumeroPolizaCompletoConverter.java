package mx.com.afirme.midas2.converter;

import java.util.Map;

import mx.com.afirme.midas.poliza.NumeroPolizaCompleto;

import org.apache.struts2.util.StrutsTypeConverter;

import com.opensymphony.xwork2.conversion.TypeConversionException;

public class NumeroPolizaCompletoConverter extends StrutsTypeConverter {

	@Override
	public Object convertFromString(Map context, String[] values, Class toClass) {
		if (values != null && values.length > 0 && values[0] != null && values[0].length() > 0) {
            try {
                return NumeroPolizaCompleto.parseNumeroPolizaCompleto(values[0]);
            }
            catch(IllegalArgumentException e) {
                throw new TypeConversionException(e);
            }
        }
        return null;
	}

	@Override
	public String convertToString(Map context, Object o) {
		if (o instanceof NumeroPolizaCompleto) {
			return ((NumeroPolizaCompleto) o).toString();
		}
		return "";
	}

}
