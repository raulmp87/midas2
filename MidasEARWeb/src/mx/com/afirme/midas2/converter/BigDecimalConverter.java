package mx.com.afirme.midas2.converter;

import java.math.BigDecimal;
import java.util.Map;

import org.apache.struts2.util.StrutsTypeConverter;

public class BigDecimalConverter extends StrutsTypeConverter {

    @SuppressWarnings("rawtypes")
	public Object convertFromString(Map context, String[] values, Class toClass) {
        if (values != null && values.length > 0 && values[0] != null && values[0].length() > 0) {
            String strValue = values[0];
            return new BigDecimal(strValue);
        }
        return null;
    }
    @SuppressWarnings("rawtypes")
	public String convertToString(Map context, Object o) {
        if (o instanceof BigDecimal && o != null) {
            return String.valueOf(o);
        }
        return "";
    }
    
}
