package mx.com.afirme.midas2.converter;

import java.util.Map;

import mx.com.afirme.midas2.domain.siniestro.TipoSiniestroMovil;
import mx.com.afirme.midas2.service.catalogos.EntidadService;

import org.apache.struts2.util.StrutsTypeConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.opensymphony.xwork2.conversion.TypeConversionException;

public class TipoSiniestroMovilConverter extends StrutsTypeConverter {

	@Autowired
	@Qualifier("entidadEJB")
	private EntidadService entidadService;
	
	@Override
	public Object convertFromString(Map context, String[] values, Class toClass) {
		if (values != null && values.length > 0 && values[0] != null && values[0].length() > 0) {
        	Long id = Long.parseLong(values[0]);
        	TipoSiniestroMovil tipoSiniestroMovil = entidadService.findById(TipoSiniestroMovil.class, id);
        	if (tipoSiniestroMovil == null) {
        		throw new TypeConversionException("No existe la entidad con el id: " + id);
        	}            	
        	return tipoSiniestroMovil;
        }
        return null;
	}

	@Override
	public String convertToString(Map context, Object o) {
		if (o instanceof TipoSiniestroMovil) {
			TipoSiniestroMovil tipoSiniestroMovil = (TipoSiniestroMovil) o;
			if (tipoSiniestroMovil.getId() != null) {
				return tipoSiniestroMovil.getId().toString();
			}
		}
		return "";
	}
	
}
