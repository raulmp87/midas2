package mx.com.afirme.midas2.converter;

import java.util.Map;

import mx.com.afirme.midas2.domain.siniestro.ProductoReporteSiniestroMovil;
import mx.com.afirme.midas2.service.catalogos.EntidadService;

import org.apache.struts2.util.StrutsTypeConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.opensymphony.xwork2.conversion.TypeConversionException;

public class ProductoReporteSiniestroMovilConverter extends StrutsTypeConverter {

	@Autowired
	@Qualifier("entidadEJB")
	private EntidadService entidadService;
	
	@Override
	public Object convertFromString(Map context, String[] values, Class toClass) {
		if (values != null && values.length > 0 && values[0] != null && values[0].length() > 0) {
        	Long id = Long.parseLong(values[0]);
        	ProductoReporteSiniestroMovil entity = entidadService.findById(ProductoReporteSiniestroMovil.class, id);
        	if (entity == null) {
        		throw new TypeConversionException("No existe la entidad con el id: " + id);
        	}            	
        	return entity;
        }
        return null;
	}

	@Override
	public String convertToString(Map context, Object o) {
		if (o instanceof ProductoReporteSiniestroMovil) {
			ProductoReporteSiniestroMovil entity = (ProductoReporteSiniestroMovil) o;
			if (entity.getId() != null) {
				return entity.getId().toString();
			}
		}
		return "";
	}
	
}
