package mx.com.afirme.midas2.converter;

import java.util.Map;

import org.apache.struts2.util.StrutsTypeConverter;

import com.opensymphony.xwork2.conversion.TypeConversionException;

public class TelefonoConverter extends StrutsTypeConverter {
	
	@Override
	public Object convertFromString(Map context, String[] values, Class toClass) {
		if (values != null && values.length > 0 && values[0] != null && values[0].length() > 0) {
			String strTelefono = values[0]
			                            .trim()
			                            .replace("-", "")
			                            .replace("(", "")
			                            .replace(")", "")
			                            .replace(" ", "");
			
			if (!strTelefono.matches("[\\d]{10}")) {
				throw new TypeConversionException("El número debe ser de 10 dígitos");				
			}
			
			return strTelefono;
        }
        return null;
	}

	@Override
	public String convertToString(Map context, Object o) {
		if (o != null) {
			return o.toString();
		}
		return "";
	}
	
}
