package mx.com.afirme.midas2.converter;

import java.util.Map;

import org.apache.struts2.util.StrutsTypeConverter;

public class StringConverter extends StrutsTypeConverter {

	@Override
	@SuppressWarnings("rawtypes")
	public Object convertFromString(Map context, String[] strings, Class toClass) {
		if (strings == null || strings.length == 0) {
			return null;
		}

		String result = strings[0];
		if (result == null) {
			return null;
		}

		if (result.isEmpty()) {
			return null;
		}
		
		return result;
	}

	@Override
	@SuppressWarnings("rawtypes")
	public String convertToString(Map context, Object object) {
		if (object != null && object instanceof String) {
			return object.toString();
		}
		return null;
	}

}
