package mx.com.afirme.midas2.converter;

import java.util.Map;

import org.apache.commons.lang.math.NumberUtils;
import org.apache.struts2.util.StrutsTypeConverter;

import com.opensymphony.xwork2.conversion.TypeConversionException;
/**
 * Convertidor para BigDecimals
 * @author vmhersil
 *
 */
public class ShortCoverter extends StrutsTypeConverter {

	@SuppressWarnings("rawtypes")
	@Override
	public Object convertFromString(Map context, String[] values, Class toClas) {
		if (values != null && values.length > 0 && values[0] != null && values[0].length() > 0) {
            try {
            	String number=values[0];
            	boolean isNumber=NumberUtils.isNumber(number);
            	if(!isNumber){
            		throw new TypeConversionException("Invalid value to Short!");
            	}
                return new Short(number);
            }
            catch(Exception e) {
                throw new TypeConversionException(e);
            }			
		}
		return null;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public String convertToString(Map context, Object o) {
		 if (o instanceof Short) {
			 return (o!=null)?((Short) o).toString():"";
		 }
		return "";
	}

}
