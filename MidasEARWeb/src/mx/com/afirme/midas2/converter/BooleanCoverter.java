package mx.com.afirme.midas2.converter;

import java.util.Map;

import org.apache.struts2.util.StrutsTypeConverter;

import com.opensymphony.xwork2.conversion.TypeConversionException;

public class BooleanCoverter extends StrutsTypeConverter {

	@SuppressWarnings("rawtypes")
	@Override
	public Object convertFromString(Map context, String[] values, Class toClas) {
		if (values != null && values.length > 0 && values[0] != null && values[0].length() > 0) {
            try {
                return "1".equals(values[0])? Boolean.TRUE:Boolean.FALSE;
            }
            catch(Exception e) {
                throw new TypeConversionException(e);
            }			
		}
		return null;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public String convertToString(Map context, Object o) {
		 if (o instanceof Boolean) {
			 return ((Boolean) o).booleanValue() ? "1":"0";
		 }
		return null;
	}

}
