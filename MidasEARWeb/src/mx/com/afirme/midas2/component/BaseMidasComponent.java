package mx.com.afirme.midas2.component;

import java.io.IOException;
import java.io.Writer;
import java.util.Map;

import org.apache.struts2.components.Component;

import com.opensymphony.xwork2.util.ValueStack;

/**
 * 
 * @author Lizeth De La Garza
 *
 */
public abstract class BaseMidasComponent extends Component {

	protected String property;
	protected String value;
	protected String disabled;
	protected String readOnly = "false";
	protected String onchange; 
	protected String onblur;
	protected String onfocus;
	protected String length;
	protected String size;
	protected String styleId;
	protected String styleClass;

	public enum COMPONENT_TYPE {
		TEXT((short) 1),
		DATE((short) 2),
		DATE_RANGE((short) 3),
		TIME((short) 4),
		TIME_RANGE((short) 5),
		RADIOBUTTON((short) 6),
		COMBOLIST((short) 7),
		CHECKBOX((short) 8),
		NUMERIC((short) 9),
		NUMERIC_RANGE((short) 10);

		private short value;

		COMPONENT_TYPE(short value) {
			this.value = value;
		}

		public short getValue() {
			return value;
		}

		public void setValue(short value) {
			this.value = value;
		}		
	}	
	
	public BaseMidasComponent() {
		super(null);
	}

	public BaseMidasComponent(ValueStack stack) {
		super(stack);
	}

	protected static void renderAttribute(Writer writer, String name, Object value) throws IOException {
		writer.write(" ");
		writer.write(name);
		writer.write("=\"");
		writer.write(String.valueOf(value));
		writer.write("\"");
	}


	protected void renderText(Writer writer, Map<String, Object> map) throws IOException {
		writer.write("<input ");
		renderAttribute(writer, "type", "text");
		renderReadOnly(writer);

		for (Map.Entry<String, Object> entry : map.entrySet()) {
			renderAttribute(writer, entry.getKey(), entry.getValue());			 
		}		 
		writer.write(" />");     

	}
	
	protected void renderLabel(Writer writer, String label) throws IOException {
		writer.write(" <label " );
		renderAttribute(writer, "class", "label");
		writer.write(">");  
		writer.write(label);
		writer.write("</label> ");
	}
	
	protected void renderNumeric(Writer writer, Map<String, Object> map) throws IOException {
		map.put("class", "txtfield jQnumeric jQrestrict");
		map.put("type", "text");		
		
		writer.write("<input ");
		
		renderReadOnly(writer);	

		for (Map.Entry<String, Object> entry : map.entrySet()) {
			renderAttribute(writer, entry.getKey(), entry.getValue());			 
		}		 
		writer.write(" />");     

	}
	
	protected void renderTime(Writer writer, Map<String, Object> map) throws IOException {
		map.put("type", "text");		
		
		writer.write("<input ");
		
		renderReadOnly(writer);	

		for (Map.Entry<String, Object> entry : map.entrySet()) {
			renderAttribute(writer, entry.getKey(), entry.getValue());			 
		}		 
		writer.write(" />");     

	}
	
	protected void renderDate(Writer writer, Map<String, Object> attributesInput, Map<String, Object> attributesBtn) throws IOException {		
		writer.write("<input ");
		for (Map.Entry<String, Object> entry : attributesInput.entrySet()) {
			renderAttribute(writer, entry.getKey(), entry.getValue());			 
		}	
		
		renderReadOnly(writer);	
		
		writer.write(" />");  
		
		if (readOnly != null && readOnly.equals("false")) {
			writer.write("<a ");
			
			for (Map.Entry<String, Object> entry : attributesBtn.entrySet()) {
				renderAttribute(writer, entry.getKey(), entry.getValue());			 
			}			
			writer.write(">"); 
			
			writer.write("<image "); 
			renderAttribute(writer, "src",  "/MidasWeb/img/b_calendario.gif");
			writer.write("/>"); 
			
			writer.write("<a/>");
		}
		
	}
	
	protected void renderCombo(Writer writer, Map<String, Object> attributes, Map<Object, Object> options) throws IOException {
		writer.write("<select ");
		renderReadOnly(writer);

		for (Map.Entry<String, Object> entry : attributes.entrySet()) {
			renderAttribute(writer, entry.getKey(), entry.getValue());			 
		}		
		writer.write(">\n"); 
		
		for (Map.Entry<Object, Object> entry : options.entrySet()) {
			
			  writer.write("<option value=\"");
			  writer.write(String.valueOf(entry.getKey()));
		      writer.write("\"");
		      
		      if (value != null && value.equals( entry.getKey().toString())) {
		    	  writer.write(" selected=true "); 		    	 
		      }
		      
		      writer.write(">");
		      
		      writer.write((String) entry.getValue());
		      writer.write("</option>");
		}
		
		
		writer.write("</select>");    

	}
	
	private void renderReadOnly(Writer writer) throws IOException{
		if (readOnly != null && readOnly.equals("true")) {
			renderAttribute(writer, "readonly", readOnly);
			renderAttribute(writer, "disabled", readOnly);
		}
	}

	public String getProperty() {
		return property;
	}

	public void setProperty(String property) {
		this.property = property;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getDisabled() {
		return disabled;
	}

	public void setDisabled(String disabled) {
		this.disabled = disabled;
	}

	public String getReadOnly() {
		return readOnly;
	}

	public void setReadOnly(String readOnly) {
		this.readOnly = readOnly;
	}

	public String getOnchange() {
		return onchange;
	}

	public void setOnchange(String onchange) {
		this.onchange = onchange;
	}

	public String getOnblur() {
		return onblur;
	}

	public void setOnblur(String onblur) {
		this.onblur = onblur;
	}

	public String getOnfocus() {
		return onfocus;
	}

	public void setOnfocus(String onfocus) {
		this.onfocus = onfocus;
	}

	public String getLength() {
		return length;
	}

	public void setLength(String length) {
		this.length = length;
	}

	public String getSize() {
		return size;
	}

	public void setSize(String size) {
		this.size = size;
	}

	public String getStyleId() {
		return styleId;
	}

	public void setStyleId(String styleId) {
		this.styleId = styleId;
	}

	public String getStyleClass() {
		return styleClass;
	}

	public void setStyleClass(String styleClass) {
		this.styleClass = styleClass;
	}
	
	

}
