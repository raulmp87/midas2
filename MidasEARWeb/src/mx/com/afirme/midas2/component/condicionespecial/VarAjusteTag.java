package mx.com.afirme.midas2.component.condicionespecial;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

 
import org.apache.struts2.components.Component;
import org.apache.struts2.views.jsp.*;
 
import com.opensymphony.xwork2.util.ValueStack;
 
/**
 * 
 * @author Lizeth De La Garza
 *
 */
public class VarAjusteTag extends ComponentTagSupport 
{
    
    private static final long serialVersionUID = 6901008022773665285L;
    protected String idCondicionEsp;
    protected String idVarAjuste;
    protected String readOnly;
    
 
    public Component getBean(ValueStack stack, HttpServletRequest req, HttpServletResponse res) {
        return new VarAjusteComponent(stack);
    }
 
    protected void populateParams() {
        super.populateParams();
        VarAjusteComponent obj = (VarAjusteComponent) component;
        obj.setReadOnly(readOnly);
        
        if (idCondicionEsp != null && !idCondicionEsp.equals("")) {
        	obj.setIdCondicionEspecial(new Long(idCondicionEsp));
        }
        
        if (idVarAjuste != null && !idVarAjuste.equals("")) {
        	obj.setIdVariableAjuste(new Long(idVarAjuste));
        }
        
    }
    
    
    
	public void setIdCondicionEsp(String idCondicionEsp) {
		this.idCondicionEsp = idCondicionEsp;
	}

	public void setIdVarAjuste(String idVarAjuste) {
		this.idVarAjuste = idVarAjuste;
	}

	public void setReadOnly(String readOnly) {
		this.readOnly = readOnly;
	}
	
    
}