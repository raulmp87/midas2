package mx.com.afirme.midas2.component.condicionespecial;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

 
import org.apache.struts2.components.Component;
import org.apache.struts2.views.jsp.*;
 
import com.opensymphony.xwork2.util.ValueStack;
 
/**
 * 
 * @author Lizeth De La Garza
 *
 */
public class FactorTag extends ComponentTagSupport 
{
    
    private static final long serialVersionUID = 6901008022773665285L;
    protected String idCondicionEsp;
    protected String idAreaImpacto;
    protected String idFactor;
    protected String readOnly;
    
 
    public Component getBean(ValueStack stack, HttpServletRequest req, HttpServletResponse res) {
        return new FactorComponent(stack);
    }
 
    protected void populateParams() {
        super.populateParams();
        FactorComponent obj = (FactorComponent) component;
        obj.setReadOnly(readOnly);
        
        if (idCondicionEsp != null && !idCondicionEsp.equals("")) {
        	obj.setIdCondicionEspecial(new Long(idCondicionEsp));
        }
        
        if (idAreaImpacto != null && !idAreaImpacto.equals("")) {
        	obj.setIdAreaImpacto(new Long(idAreaImpacto));
        }
        
        if (idFactor != null && !idFactor.equals("")) {
        	obj.setIdFactor(new Long(idFactor));
        }        
    }   
    
    
	public void setIdCondicionEsp(String idCondicionEsp) {
		this.idCondicionEsp = idCondicionEsp;
	}

	public void setIdAreaImpacto(String idAreaImpacto) {
		this.idAreaImpacto = idAreaImpacto;
	}

	public void setIdFactor(String idFactor) {
		this.idFactor = idFactor;
	}

	public void setReadOnly(String readOnly) {
		this.readOnly = readOnly;
	}
	
    
}