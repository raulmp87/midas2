package mx.com.afirme.midas2.component.condicionespecial;

import java.io.IOException;
import java.io.Writer;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import mx.com.afirme.midas2.component.BaseMidasComponent;
import mx.com.afirme.midas2.domain.condicionesespeciales.ValorVarAjusteCondicionEsp;
import mx.com.afirme.midas2.domain.condicionesespeciales.ValorVariableAjuste;
import mx.com.afirme.midas2.domain.condicionesespeciales.VarAjusteCondicionEsp;
import mx.com.afirme.midas2.domain.condicionesespeciales.VariableAjuste;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.catalogos.condicionesespeciales.CondicionEspecialService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import com.opensymphony.xwork2.util.ValueStack;

/**
 * 
 * @author Lizeth De La Garza
 *
 */

@Component
@Scope("prototype")
public class VarAjusteComponent extends BaseMidasComponent {

	protected Long idVariableAjuste;

	protected Long idCondicionEspecial;

	@Autowired
	@Qualifier("condicionEspecialServiceEJB")
	CondicionEspecialService condicionEspecialService;
	
	@Autowired
	@Qualifier("listadoServiceEJB")
	ListadoService listadoService;


	public VarAjusteComponent(ValueStack stack) {
		super(stack);
	}

	public boolean start(Writer writer) {
		SpringBeanAutowiringSupport.processInjectionBasedOnCurrentContext(this);
		Map<String, Object> attributes = new HashMap<String, Object>();
		Map<Object, Object> options = new LinkedHashMap<Object, Object>();		

		try {

			if (idVariableAjuste != null) {				

				VariableAjuste variable = condicionEspecialService.obtenerVariableAjuste(idVariableAjuste);

				List<ValorVariableAjuste> valores = condicionEspecialService.obtenerValoresVariable(idVariableAjuste);

				VarAjusteCondicionEsp variableCondicion = condicionEspecialService.obtenerVariableAjusteCondicion(idVariableAjuste, idCondicionEspecial);

				List<ValorVarAjusteCondicionEsp> valoresCondicion = null;				

				if (variableCondicion != null) {
					valoresCondicion = condicionEspecialService.obtenerValoresVariableCondicion(variableCondicion.getId());				
				}				


				if (variable.getTipo() == COMPONENT_TYPE.TEXT.getValue()) {
					ValorVariableAjuste valor = valores.get(0);
					attributes.put("name", idVariableAjuste);
					attributes.put("id", valor.getId());
					attributes.put("onblur", "actualizaValorVariable("+ idVariableAjuste + ","  + valores.get(0).getId() + ",this.value)");
					if (valoresCondicion != null && !valoresCondicion.isEmpty()) {
						attributes.put("value", valoresCondicion.get(0).getValor());
					}

					renderLabel(writer, valor.getNombre());
					renderText(writer, attributes);

				} else if (variable.getTipo() == COMPONENT_TYPE.NUMERIC.getValue()) {
					ValorVariableAjuste valor = valores.get(0);

					attributes.put("name", idVariableAjuste);
					attributes.put("id", valor.getId());
					attributes.put("onblur", "actualizaValorVariable("+ idVariableAjuste + ","  + valores.get(0).getId() + ",this.value)");
					attributes.put("size", "3");

					if (valoresCondicion != null && !valoresCondicion.isEmpty()) {
						attributes.put("value", valoresCondicion.get(0).getValor());
					}

					renderLabel(writer, valor.getNombre());
					renderNumeric(writer, attributes);

				} else if (variable.getTipo() == COMPONENT_TYPE.NUMERIC_RANGE.getValue()) {

					for (ValorVariableAjuste valor : valores) {

						attributes.clear();
						attributes.put("name", idVariableAjuste);
						attributes.put("id", valor.getId());
						attributes.put("onblur", "actualizaValorVariable("+ idVariableAjuste + ","  + valor.getId() + ",this.value)");
						attributes.put("size", "3");

						if (valoresCondicion != null && !valoresCondicion.isEmpty()) {

							for(ValorVarAjusteCondicionEsp valorCondicion: valoresCondicion) {
								if (valorCondicion.getOrden() == valor.getOrden()) {
									attributes.put("value", valorCondicion.getValor());
								}
							}							
						}						 
						renderLabel(writer, valor.getNombre());
						renderNumeric(writer, attributes);
					}		 


				} else if (variable.getTipo() == COMPONENT_TYPE.DATE.getValue()) {
					attributes.put("id", "varajuste_" +  idVariableAjuste + "_" + valores.get(0).getId());
					attributes.put("class", "varajustedate");
					attributes.put("onchange",  "actualizaValorVariable("+ idVariableAjuste + ", 'varajuste_"  + valores.get(0).getId() + "',this.value)");
					attributes.put("onclick", "javascript: showCalendar(" + valores.get(0).getId() + ");");		
					attributes.put("size", "10");
					attributes.put("eventInput", "actualizaValorVariable(" + idVariableAjuste + ", 'varajuste_" + valores.get(0).getId() + "',this.value)");
					attributes.put("eventCalendarClick", "actualizaValorVariable(" + idVariableAjuste + ", 'varajuste_" + valores.get(0).getId() + "',fecha)");

					if (valoresCondicion != null && !valoresCondicion.isEmpty()) {
						attributes.put("value", valoresCondicion.get(0).getValor());
					}

					Map<String, Object> attributesBtn = new HashMap<String, Object>();

					attributesBtn.put("id",  "calendar");
					attributesBtn.put("href",  "javascript: void(0);");
					attributesBtn.put("onclick", "javascript: showCalendar(" + valores.get(0).getId() + ");");	

					renderLabel(writer, valores.get(0).getNombre());

					renderDate(writer, attributes, attributesBtn);

				} else if (variable.getTipo() == COMPONENT_TYPE.DATE_RANGE.getValue()) {
					for (ValorVariableAjuste valor : valores) {
						attributes = new HashMap<String, Object>();
						attributes.put("id", "varajuste_" +  idVariableAjuste + "_" + valor.getId());
						attributes.put("class", "varajustedate");
						attributes.put("onchange",  "actualizaValorVariable("+ idVariableAjuste + ", 'varajuste_" + valor.getId() + "',this.value)");
						attributes.put("onclick", "javascript: showCalendar(" + valor.getId() + ");");
						attributes.put("size", "10");
						attributes.put("eventInput", "actualizaValorVariable("+ idVariableAjuste + ", 'varajuste_"  + valor.getId() + "',this.value)");
						attributes.put("eventCalendarClick", "actualizaValorVariable("+ idVariableAjuste + ", 'varajuste_"  + valor.getId() + "',fecha)");

						if (valoresCondicion != null && !valoresCondicion.isEmpty()) {
							for (ValorVarAjusteCondicionEsp valorCondicion: valoresCondicion) {
								if (valorCondicion.getOrden() == valor.getOrden()) {
									attributes.put("value", valorCondicion.getValor());
								}
							}	
						}

						Map<String, Object> attributesBtn = new HashMap<String, Object>();

						attributesBtn.put("id",  "calendar_" +  valor.getId());
						attributesBtn.put("href",  "javascript: void(0);");
						attributesBtn.put("onclick", "javascript: showCalendar(" + valor.getId() + ");");

						renderLabel(writer, valor.getNombre());
						renderDate(writer, attributes, attributesBtn);	
					}

				} else if (variable.getTipo() == COMPONENT_TYPE.TIME.getValue()) {					
					ValorVariableAjuste valor = valores.get(0);

					attributes.put("name", idVariableAjuste);
					attributes.put("id", "varajuste_" + valor.getId());
					attributes.put("onblur", "actualizaValorVariableTime(" + idVariableAjuste + ", 'varajuste_" + valor.getId() + "', this.value)");
					attributes.put("size", "5");
					attributes.put("class", "varajustetime");

					if (valoresCondicion != null && !valoresCondicion.isEmpty()) {
						attributes.put("value", valoresCondicion.get(0).getValor());
					}

					renderLabel(writer, valor.getNombre());
					renderTime(writer, attributes);

				} else if (variable.getTipo() == COMPONENT_TYPE.TIME_RANGE.getValue()) {
					for (ValorVariableAjuste valor : valores) {
						attributes = new HashMap<String, Object>();
						attributes.put("name", idVariableAjuste);
						attributes.put("id", "varajuste_" + valor.getId());
						attributes.put("onblur", "actualizaValorVariableTime(" + idVariableAjuste + ", 'varajuste_" + valor.getId() + "', this.value)");
						attributes.put("size", "5");
						attributes.put("class", "varajustetime");

						if (valoresCondicion != null && !valoresCondicion.isEmpty()) {
							for (ValorVarAjusteCondicionEsp valorCondicion: valoresCondicion) {
								if (valorCondicion.getOrden() == valor.getOrden()) {
									attributes.put("value", valorCondicion.getValor());
								}
							}	
						}

						renderLabel(writer, valor.getNombre());
						renderTime(writer, attributes);

					}

				} else if (variable.getTipo() == COMPONENT_TYPE.COMBOLIST.getValue()) {

					attributes.put("id", idVariableAjuste);
					attributes.put("name", idVariableAjuste);	
					attributes.put("onchange", "actualizaValorVariable(" + idVariableAjuste + ",0, this.value)" );	

					if (valoresCondicion != null && !valoresCondicion.isEmpty()) {
						value = valoresCondicion.get(0).getValor();
					} else {
						options.put(0L, "Seleccione...");
					}

					if (variable.getTipoListado().equals(VariableAjuste.TIPO_LISTADO.VALOR_VARIABLE.getValor())) {
						for (ValorVariableAjuste valor: valores) {

							options.put(valor.getId(), valor.getNombre());
						}

					} else if (variable.getTipoListado().equals(VariableAjuste.TIPO_LISTADO.ESTADO.getValor())) {
						Map<String, String> estados = listadoService.getMapEstadosPorPaisMidas(null);
						
						for (Map.Entry<String, String> estado : estados.entrySet()) {
							options.put(estado.getKey(), estado.getValue());
						}
						

					} else if (variable.getTipoListado().equals(VariableAjuste.TIPO_LISTADO.CAT_VALOR_FIJO.getValor())) {
						Map<String, String> catalogo = listadoService.obtenerCatalogoValorFijoByStr(variable.getGrupoCatalogo(), null);
						
						for (Map.Entry<String, String> valor : catalogo.entrySet()) {
							options.put(valor.getKey(), valor.getValue());
						}
					}					

					renderLabel(writer, variable.getNombre());

					renderCombo(writer, attributes, options);
				}
			}
			else {
				writer.write("<b/>");
			}
		}
		catch (IOException e) {
			System.out.println("IOException error: " + e.getMessage());
		}
		return true;
	}

	public boolean end(Writer writer) {
		return true;
	}

	@Override
	public boolean usesBody() {
		return false;
	}

	public void setIdVariableAjuste(Long idVariableAjuste) {
		this.idVariableAjuste = idVariableAjuste;
	}

	public void setIdCondicionEspecial(Long idCondicionEspecial) {
		this.idCondicionEspecial = idCondicionEspecial;
	}

}