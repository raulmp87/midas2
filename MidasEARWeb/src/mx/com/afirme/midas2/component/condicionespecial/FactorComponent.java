package mx.com.afirme.midas2.component.condicionespecial;

import java.io.IOException;
import java.io.Writer;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mx.com.afirme.midas2.component.BaseMidasComponent;
import mx.com.afirme.midas2.domain.condicionesespeciales.Factor;
import mx.com.afirme.midas2.domain.condicionesespeciales.FactorAreaCondEsp;
import mx.com.afirme.midas2.domain.condicionesespeciales.ValorFactor;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.catalogos.condicionesespeciales.CondicionEspecialService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import com.opensymphony.xwork2.util.ValueStack;

/**
 * 
 * @author Lizeth De La Garza
 *
 */

@Component
@Scope("prototype")
public class FactorComponent extends BaseMidasComponent {

	protected Long idFactor;

	protected Long idAreaImpacto;

	protected Long idCondicionEspecial;

	@Autowired
	@Qualifier("condicionEspecialServiceEJB")
	CondicionEspecialService condicionEspecialService;
	
	@Autowired
	@Qualifier("listadoServiceEJB")
	ListadoService listadoService;

	public FactorComponent(ValueStack stack) {
		super(stack);
	}

	public boolean start(Writer writer) {

		SpringBeanAutowiringSupport.processInjectionBasedOnCurrentContext(this);
		Map<String, Object> attributes = new HashMap<String, Object>();
		Map<Object, Object> options = new HashMap<Object, Object>();		

		try {

			if (idFactor != null) {	

				Factor factor = condicionEspecialService.obtenerFactor(idFactor);			

				FactorAreaCondEsp factorCondicion = condicionEspecialService.obtenerFactorCondicion(idCondicionEspecial, idAreaImpacto, idFactor);
				
				if (factor.getTipo() == COMPONENT_TYPE.TEXT.getValue()) {
					attributes.put("name", factor.getId());
					attributes.put("id", factor.getId());
					attributes.put("onblur", "actualizaValorFactor(" + idAreaImpacto + ','  + idFactor + ",this.value)");
					attributes.put("size", factor.getSize());
					
					if (factorCondicion.getValor() != null) {
						attributes.put("value", factorCondicion.getValor());
					}

					renderLabel(writer, factor.getNombre());
					renderText(writer, attributes);

				} else if (factor.getTipo() == COMPONENT_TYPE.NUMERIC.getValue()) {

					attributes.put("name", factor.getId());
					attributes.put("id", factor.getId());
					attributes.put("onblur", "actualizaValorFactor(" + idAreaImpacto + ','  + idFactor + ",this.value)");
					attributes.put("size", "3");

					if (factorCondicion.getValor() != null) {
						attributes.put("value", factorCondicion.getValor());
					}

					renderLabel(writer, factor.getNombre());
					renderNumeric(writer, attributes);

				} else if (factor.getTipo() == COMPONENT_TYPE.COMBOLIST.getValue()) {

					attributes.put("id", factor.getId());
					attributes.put("name", factor.getId());	
					attributes.put("onchange", "actualizaValorFactor(" + idAreaImpacto + ','  + idFactor + ",this.value)");

					if (factorCondicion.getValor() != null) {
						value = factorCondicion.getValor();
					} else {
						options.put(0L, "Seleccione...");
					}
					
					List<ValorFactor> valores = condicionEspecialService.obtenerValoresFactor(idFactor);

					for (ValorFactor valor: valores) {

						options.put(valor.getId(), valor.getNombre());
					}
					
					if (factor.getTipoListado().equals(Factor.TIPO_LISTADO.VALOR_FACTOR.getValor())) {
						for (ValorFactor valor: valores) {

							options.put(valor.getId(), valor.getNombre());
						}

					} else if (factor.getTipoListado().equals(Factor.TIPO_LISTADO.TIPO_PRESTADOR.getValor())) {
						Map<String, String> tiposPrestador = listadoService.getMapTipoPrestador();
						
						for (Map.Entry<String, String> estado : tiposPrestador.entrySet()) {
							options.put(estado.getKey(), estado.getValue());
						}
					}


					renderLabel(writer, factor.getNombre());

					renderCombo(writer, attributes, options);
				}
			}
			else {
				writer.write("<b/>");
			}
		}
		catch (IOException e) {
			System.out.println("IOException error: " + e.getMessage());
		}
		return true;
	}

	public boolean end(Writer writer) {
		return true;
	}

	@Override
	public boolean usesBody() {
		return false;
	}

	public void setIdFactor(Long idFactor) {
		this.idFactor = idFactor;
	}

	public void setIdAreaImpacto(Long idAreaImpacto) {
		this.idAreaImpacto = idAreaImpacto;
	}

	public void setIdCondicionEspecial(Long idCondicionEspecial) {
		this.idCondicionEspecial = idCondicionEspecial;
	}

}