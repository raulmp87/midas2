package mx.com.afirme.midas2.utils;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import javax.persistence.Query;

import org.apache.commons.lang.StringUtils;

public class SystemCommonUtils {
	/**
	 * Valida si una cadena es nula o vacia
	 * @param value
	 * @return
	 */
	public static boolean isValid(String value){
		return (value!=null && !value.trim().isEmpty());
	}
	/**
	 * Valida si un objeto es nulo
	 * @param obj
	 * @return
	 */
	public static boolean isNull(Object obj){
		return (obj==null);
	}
	/**
	 * Valida si un objeto no es nulo
	 * @param obj
	 * @return
	 */
	public static boolean isNotNull(Object obj){
		return (obj!=null);
	}
	/**
	 * Valida si una lista es nula o vacia
	 * @param collection
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	public static boolean isEmptyList(Collection collection){
		return (collection==null || collection.isEmpty());
	}
	
	@SuppressWarnings("rawtypes")
	public static boolean isEmptyMap(Map map){
		return (map==null || map.isEmpty());
	}
	
	public static void addCondition(StringBuilder queryString,String conditional){
		if(!queryString.toString().contains("where")){
			queryString.append(" where ");
		}
		queryString.append(conditional);
		queryString.append(" and ");
	}
	
	public static void setQueryParametersByProperties(Query entityQuery, Map<String, Object> parameters){
		Iterator<Entry<String, Object>> it = parameters.entrySet().iterator();
		while(it.hasNext()){
			Map.Entry<String, Object> pairs = (Map.Entry<String, Object>)it.next();
			String key=getValidProperty(pairs.getKey());
			entityQuery.setParameter(key, pairs.getValue());
		}		
	}
	
	public static String getValidProperty(String property){
		String validProperty=property;
		StringBuilder str=new StringBuilder("");
		if(property.contains(".")){
			int i=0;
			for(String word:property.split("\\.")){
				if(i==0){
					word=word.toLowerCase();
				}else{
					word=StringUtils.capitalize(word);
				}
				str.append(word);
				i++;
			}
			validProperty=str.toString();
		}
		return validProperty;
	}
	
	public static String getQueryString(StringBuilder queryString){
		String query=queryString.toString();
		if(query.endsWith(" and ")){
			query=query.substring(0,(query.length())-(" and ").length());
		}
		return query;
	}

	/**
	 * Valida si un número no es nulo ni equivalente a cero
	 * Retornará true si el número es válido.
	 * @param o
	 * @return boolean
	 */
	public static boolean isNotNullNorZero(Object o){
		boolean retVal;
		int zero = 0;
		retVal = isNotNull(o);
		if (retVal){
			String objectType = o.getClass().getSimpleName(); 
			if(objectType.equals("BigDecimal")){
				retVal = !o.equals(BigDecimal.valueOf(zero));
			}else if(objectType.equals("Integer")){
				retVal = !o.equals(zero);
			}else if(objectType.equals("Long")){
				retVal = !o.equals(Long.valueOf(zero));
			}else if(objectType.equals("Double")){
				retVal = !o.equals(Double.valueOf(zero));
			}
		}
		return retVal;
	}
}
