package mx.com.afirme.midas2.utils;

public enum Attempt {
	ZERO(0),
	FIRST(30),
	SECOND(30),
	THIRD(1440);
	
	private final int minutes;
	
	Attempt(int minutes){
		this.minutes = minutes;
	}
	
	public int getMinutes(){
		return this.minutes;
	}
}
