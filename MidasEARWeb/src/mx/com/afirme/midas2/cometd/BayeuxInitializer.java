package mx.com.afirme.midas2.cometd;

/*
 * Copyright (c) 2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */



import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.inject.Inject;
import javax.servlet.ServletContext;

import org.cometd.annotation.ServerAnnotationProcessor;
import org.cometd.bayeux.server.BayeuxServer;
import org.cometd.server.BayeuxServerImpl;
import org.cometd.server.transport.JSONPTransport;
import org.cometd.server.transport.JSONTransport;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.DestructionAwareBeanPostProcessor;
import org.springframework.stereotype.Component;
import org.springframework.web.context.ServletContextAware;

import mx.com.afirme.midas2.timer.ConsumirEventosTimer;
import mx.com.afirme.midas2.timer.EventosClienteMovil;

@Component
public class BayeuxInitializer implements DestructionAwareBeanPostProcessor, ServletContextAware
{
    private ServerAnnotationProcessor processor;
    private ConsumirEventosTimer consumirEventosTimer;
    private EventosClienteMovil eventosClienteMovil;
    private BayeuxServer bayeuxServer;
    private ServletContext servletContext;
    
    @Inject
	public void setEventosClienteMovil(EventosClienteMovil eventosClienteMovil) {
		this.eventosClienteMovil = eventosClienteMovil;
	}
    
    @Inject
    public void setConsumirEventosTimer(ConsumirEventosTimer consumirEventosTimer) {
		this.consumirEventosTimer = consumirEventosTimer;
	}
    
	@PostConstruct
    public void init()
    {
		initializeBayeuxServer();		
        this.processor = new ServerAnnotationProcessor(bayeuxServer);
        consumirEventosTimer.start(bayeuxServer);
        eventosClienteMovil.start(bayeuxServer);
    }

	@PreDestroy
    public void destroy()
    {
    }
	
	public void initializeBayeuxServer()
    {
        BayeuxServerImpl bean = new BayeuxServerImpl();
        Map<String, Object> options = new HashMap<String,Object>();
        options.put("logLevel", 0);
        options.put("timeout", 60 * 1000);        
        bean.setOptions(options);
        bean.setTransports(new JSONTransport(bean), new JSONPTransport(bean));
        bean.setAllowedTransports(JSONTransport.NAME, JSONPTransport.NAME);
        this.bayeuxServer = bean;
        servletContext.setAttribute(BayeuxServer.ATTRIBUTE,  bayeuxServer);
        
    }

    public Object postProcessBeforeInitialization(Object bean, String name) throws BeansException
    {
        processor.processDependencies(bean);
        processor.processConfigurations(bean);
        processor.processCallbacks(bean);
        return bean;
    }

    public Object postProcessAfterInitialization(Object bean, String name) throws BeansException
    {
        return bean;
    }

    public void postProcessBeforeDestruction(Object bean, String name) throws BeansException
    {
        processor.deprocessCallbacks(bean);
    }

    public void setServletContext(ServletContext servletContext)
    {
    	this.servletContext = servletContext;
    }
    
}
