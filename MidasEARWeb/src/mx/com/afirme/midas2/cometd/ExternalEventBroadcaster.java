package mx.com.afirme.midas2.cometd;

import java.util.List;

import org.cometd.bayeux.server.BayeuxServer;
import org.cometd.bayeux.server.LocalSession;
import org.cometd.bayeux.server.ServerChannel;


public class ExternalEventBroadcaster {
    
    private final BayeuxServer bayeuxServer;
    
    
    public static class Message {
    	
    	private String content;
    	private String channel;
    	
    	public Message(String content, String channel) {
    		this.content = content;
    		this.channel = channel;
    	}
		
    	public String getContent() {
			return content;
		}
		
    	public void setContent(String content) {
			this.content = content;
		}
		
    	public String getChannel() {
			return channel;
		}
		
    	public void setChannel(String channel) {
			this.channel = channel;
		}    	
    }
    
    LocalSession session;

    public ExternalEventBroadcaster(BayeuxServer bayeuxServer)
    {
        this.bayeuxServer = bayeuxServer;

        this.session = bayeuxServer.newLocalSession("external");
        this.session.handshake();
    }

    public void onExternalEvent(String msg,String channelName)
    {
    	onExternalEvent(new Message(msg, channelName));       
    }
    
    public void onExternalEvent(Message message)
    {
        this.bayeuxServer.createIfAbsent(message.getChannel());
        ServerChannel channel = this.bayeuxServer.getChannel(message.getChannel());
        if (channel != null)
        {
            channel.publish(this.session, message.getContent(), null);
        }
       
    }
    
    public void onExternalEvent(List<Message> messages) {
    	for (Message message : messages) {
    		onExternalEvent(message);
    	}
    }
  
}
