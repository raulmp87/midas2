package mx.com.afirme.midas2.ws.agente;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.jws.WebMethod;
import javax.jws.WebService;

import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.service.fuerzaventa.AgenteMidasService;
import mx.com.afirme.midas2.service.reportes.GenerarPlantillaReporte;

@Stateless
@WebService(name = "Reporteador", 
			targetNamespace = "http://segurosafirme.com.mx/agentes/reporteador"/*, 
			wsdlLocation = "/WEB-INF/wsdl/ReporteadorService.wsdl"*/)
public class Reporteador {

	@EJB
	private GenerarPlantillaReporte generarPlantillaReporte;
	@EJB
	private AgenteMidasService agenteMidasService;

	@WebMethod
	public byte[] getReportPremiumDetail(
			 long agentCode,
			 Calendar finalDate,
			 String resultType) {
		byte[] resultado = null;
		try {
			Agente agente = new Agente();
			agente.setIdAgente(agentCode);
			String monthSt = String.valueOf(finalDate.get(Calendar.MONTH) + 1);
			monthSt = monthSt.length() == 1 ? "0" + monthSt : monthSt;
			String yearSt = String.valueOf(finalDate.get(Calendar.YEAR));
			agente = agenteMidasService.loadByClave(agente);
			resultado = generarPlantillaReporte.imprimirReporteDetallePrimas(
					yearSt, monthSt, agente, resultType).getByteArray();
			System.out.println(agente.getIdAgente());
		} catch (Exception e) {
			e.printStackTrace();
			resultado = "resultado".getBytes();
		}
		return resultado;
	}
	
	
		
	@WebMethod
	public byte[] getReportAccountStatement(
			 long agentCode,
			Calendar finalDate) {
		
		byte [] resultado = null;
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMM");
			long anioMes = Integer.valueOf(sdf.format(finalDate.getTime()));
			resultado = generarPlantillaReporte.imprimirReporteEstadoDeCuentaAgente(8L, agentCode, anioMes, false, true, true, false, null,false ).getByteArray();
		} catch(Exception e){
			e.printStackTrace();
			resultado = "resultado".getBytes();
		}
		return resultado;
	}
}