package mx.com.afirme.midas2.ws.hgs;

import javax.ejb.Stateless;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.ejb.EJB;

import mx.com.afirme.midas2.dto.siniestros.hgs.ConsultaHgsDto;
import mx.com.afirme.midas2.dto.siniestros.hgs.Efile;
import mx.com.afirme.midas2.dto.siniestros.hgs.EfileDevolucion;
import mx.com.afirme.midas2.dto.siniestros.hgs.LiquidacionHgs;
import mx.com.afirme.midas2.service.siniestros.valuacion.hgs.HgsService;

@Stateless
@WebService(name = "ValuacionHgs", targetNamespace = "https://segurosafirme.com.mx/hgs")
public class SumaAsegurada {
	
	@EJB
	private HgsService hgsService;
	
	@WebMethod(operationName = "obtenerSumaAsegurada")
	public ConsultaHgsDto obtenerSumaAsegurada(@WebParam(name = "idReporteSin") int idReporteSin){
		
		ConsultaHgsDto consultaHgsDto =this.hgsService.obtenerSumaAseguradaHGS(new Long (idReporteSin) );
		
		return consultaHgsDto;
	}
	
	@WebMethod(operationName = "crearValuacion")
	public String crearValuacion(@WebParam(name="eFile") Efile efile ){
		
		return this.hgsService.insertarValuacion(efile);
	}
	
	@WebMethod(operationName = "crearDevolucionPiezas")
	public String crearDevolucionPiezas(@WebParam(name="efileDevolucion")EfileDevolucion efileDevolucion){
		return this.hgsService.generarDevolucionPiezas(efileDevolucion);
	}
	
	
	@WebMethod(operationName = "notificaOrdenPagada")
	public void notificaOrdenPagada(@WebParam(name="liquidacionHgs")LiquidacionHgs liquidacionHgs){
		 this.hgsService.notificaEstatusOrdenCompra(liquidacionHgs.getIdLiquidacion());
	}
	
	
	@WebMethod(operationName = "bienvenidaMidas")
	public String testConexion(){
		 return "Siniestros Seguros Afirme";
	}

}
