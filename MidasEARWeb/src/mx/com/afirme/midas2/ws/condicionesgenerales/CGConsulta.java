package mx.com.afirme.midas2.ws.condicionesgenerales;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

import mx.com.afirme.midas2.service.condicionesGenerales.WSCondicionesGeneralesService;

import org.apache.log4j.Logger;

@Stateless
@WebService(name = "CGConsulta", targetNamespace = "http://segurosafirme.com.mx/condicionesgenerales/cgconsulta")
public class CGConsulta {
	
	private static final Logger LOG = Logger.getLogger(WSCondicionesGeneralesService.class);
	@EJB
	private WSCondicionesGeneralesService wsCondicionesGeneralesService;	

	
	@WebMethod(operationName = "consultaCondicionesGenerales")
	@WebResult(targetNamespace = "")	
	public byte[] impresionPoliza (@WebParam(name = "idPoliza") Long idPoliza, @WebParam(name = "apSolicita") String apSolicita){
		 	 
		try {
			return wsCondicionesGeneralesService.consultarCondicionesGenerales(idPoliza, apSolicita);
		} catch (Exception e) {
			LOG.error("Ocurrio un error en CGConsulta: " + e.getMessage());
			return "".getBytes();
		}
		 
	}

}
