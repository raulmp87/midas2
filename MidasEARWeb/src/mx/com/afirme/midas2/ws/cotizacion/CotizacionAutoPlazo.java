package mx.com.afirme.midas2.ws.cotizacion;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas2.dao.suscripcion.cotizacion.auto.ws.CotizacionView;
import mx.com.afirme.midas2.domain.ws.autoplazo.Cliente;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.CotizacionService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.WSCotizacionService;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.JsonParseException;

@Stateless
@WebService(name = "CotizacionAutoPlazo", targetNamespace = "http://segurosafirme.com.mx/cotizacion/cotizacionautoplazo")
public class CotizacionAutoPlazo{

	private static Logger LOG = Logger.getLogger(CotizacionAutoPlazo.class);
	@EJB
	private CotizacionService cotizacionService;
	@EJB
	private EntidadService entidadService;
	@EJB
	private WSCotizacionService wsCotizacionService;
	
	/**
	 * Servicio para crear cotizacion desde originacion, retornando idCotizacion y primatotal correspondiente .
	 *
	 * @param claveUsuario
	 * @param idNegocio
	 * @param idPaquete
	 * @param estilo
	 * @param modelo
	 * @param marca
	 * @param idEstado
	 * @param valorFactura
	 * @param descuento
	 * @param folio
	 * @param generaCotizacion
	 * @param nombreAsegurado
	 * @param apellidoPaterno
	 * @param apellidoMaterno
	 * @param RFC
	 * @param claveSexo
	 * @param claveEstadoCivil
	 * @param idEstadoNacimiento
	 * @param fechaNacimiento
	 * @param codigoPostal
	 * @param idCiudad
	 * @param estado
	 * @param colonia
	 * @param calleNumero
	 * @param telefonoCasa
	 * @param telefonoOficina
	 * @param email
	 * @return idCotizacion,primatotal
	 */
	@WebMethod(operationName = "cotizar")
	@WebResult(targetNamespace = "")	
	public String cotizar(	@WebParam(name = "idSucursal") Integer idSucursal,							
							@WebParam(name = "claveUsuario") String claveUsuario,							
							@WebParam(name = "idNegocio") Long idNegocio,
							@WebParam(name = "idPaquete") Long idPaquete,
							@WebParam(name = "estilo") Integer estilo,
							@WebParam(name = "modelo") String modelo,
							@WebParam(name = "marca") Integer marca,
							@WebParam(name = "idEstadoCirculacion") String idEstadoCirculacion,
							@WebParam(name = "idMunicipioCirculacion") String idMunicipcioCirculacion,
							@WebParam(name = "valorFactura") Double valorFactura,
							@WebParam(name = "descuento") Double descuento,
							@WebParam(name = "folio") String folio,
							@WebParam(name = "generaCotizacion") boolean generaCotizacion,
							@WebParam(name = "nombreAsegurado") String nombreAsegurado,
							@WebParam(name = "apellidoPaterno") String apellidoPaterno,
							@WebParam(name = "apellidoMaterno") String apellidoMaterno,
							@WebParam(name = "RFC") String RFC,
							@WebParam(name = "claveSexo") String claveSexo,
							@WebParam(name = "claveEstadoCivil") String claveEstadoCivil,
							@WebParam(name = "idEstadoNacimiento") String idEstadoNacimiento,
							@WebParam(name = "fechaNacimiento") Date fechaNacimiento,
							@WebParam(name = "codigoPostal") String codigoPostal,
							@WebParam(name = "idCiudad") String idCiudad,
							@WebParam(name = "idEstado") String idEstado,
							@WebParam(name = "colonia") String colonia,
							@WebParam(name = "calleNumero") String calleNumero,
							@WebParam(name = "telefonoCasa") String telefonoCasa,
							@WebParam(name = "telefonoOficina") String telefonoOficina,
							@WebParam(name = "email") String email) {		
		CotizacionDTO cotizacion = new CotizacionDTO();
		try {
			final int lengthFiledId = 5;				
			//mandamos llamar el metodo que crea la cotizacion, guarda inciso y guarda informacion del cliente
			cotizacion = wsCotizacionService.creaCotizacion(idSucursal,claveUsuario, idNegocio,descuento, folio,idPaquete,  estilo,  modelo,  marca,
					idEstadoCirculacion, idMunicipcioCirculacion,  valorFactura,	nombreAsegurado, apellidoPaterno,apellidoMaterno, RFC, 
					claveSexo, claveEstadoCivil, idEstadoNacimiento, fechaNacimiento,codigoPostal,idCiudad, idEstado, 
					colonia, calleNumero, telefonoCasa, telefonoOficina, email, null, null, null);
				
			if(generaCotizacion){
				Map<String, Object> excResultado = wsCotizacionService.evalueExcepcion(cotizacion);
				if(excResultado!= null && !excResultado.containsKey("statusExcepcion") && "true".equals(String.valueOf(excResultado.get("statusExcepcion")))){//JFGG
					//Borra Cotizacion
					cotizacionService.eliminarCotizacion(cotizacion.getIdToCotizacion());
					return String.valueOf(excResultado.get("mensaje"));
				} else {//JFGG
					return "{idCotizacion:" + cotizacion.getIdToCotizacion() + ", primaTotal:" + cotizacion.getValorPrimaTotal() + "}";
				}//JFGG
			}
			else{
				
				BigDecimal valorPrimaTotal = cotizacion.getValorPrimaTotal();
				//Borra Cotizacion
				cotizacionService.eliminarCotizacion(cotizacion.getIdToCotizacion());
				
				return "{primaTotal:" + valorPrimaTotal + "}";
			}				
		} catch (SystemException e) {
			LOG.error(e.getMessage(), e);
			return "No fue posible crear la cotizacion, "  + e.getMessage();
		} catch (Exception e){
			LOG.error(e.getMessage(), e);
			return "No fue posible crear la cotizacion" + e.getMessage();
		}		
	}
	
	/**
	 * Servicio para crear cotizacion desde originacion, retornando idCotizacion y primatotal correspondiente.
	 * 
	 * @param idSucursal
	 * @param claveUsuario
	 * @param idNegocio
	 * @param idPaquete
	 * @param estilo
	 * @param modelo
	 * @param marca
	 * @param idEstadoCirculacion
	 * @param idMunicipcioCirculacion
	 * @param valorFactura
	 * @param descuento
	 * @param folio
	 * @param generaCotizacion
	 * @param nombreAsegurado
	 * @param apellidoPaterno
	 * @param apellidoMaterno
	 * @param RFC
	 * @param claveSexo
	 * @param claveEstadoCivil
	 * @param idEstadoNacimiento
	 * @param fechaNacimiento
	 * @param codigoPostal
	 * @param idCiudad
	 * @param idEstado
	 * @param colonia
	 * @param calleNumero
	 * @param telefonoCasa
	 * @param telefonoOficina
	 * @param email
	 * @param numeroSerie
	 * @param numeroMotor
	 * @return idCotizacion,primatotal
	 */
	@WebMethod(operationName = "cotizarNuevo")
	@WebResult(targetNamespace = "")	
	public String cotizarNuevo(	@WebParam(name = "idSucursal") Integer idSucursal,							
							@WebParam(name = "claveUsuario") String claveUsuario,							
							@WebParam(name = "idNegocio") Long idNegocio,
							@WebParam(name = "idPaquete") Long idPaquete,
							@WebParam(name = "estilo") Integer estilo,
							@WebParam(name = "modelo") String modelo,
							@WebParam(name = "marca") Integer marca,
							@WebParam(name = "idEstadoCirculacion") String idEstadoCirculacion,
							@WebParam(name = "idMunicipioCirculacion") String idMunicipcioCirculacion,
							@WebParam(name = "valorFactura") Double valorFactura,
							@WebParam(name = "descuento") Double descuento,
							@WebParam(name = "folio") String folio,
							@WebParam(name = "generaCotizacion") boolean generaCotizacion,
							@WebParam(name = "nombreAsegurado") String nombreAsegurado,
							@WebParam(name = "apellidoPaterno") String apellidoPaterno,
							@WebParam(name = "apellidoMaterno") String apellidoMaterno,
							@WebParam(name = "RFC") String RFC,
							@WebParam(name = "claveSexo") String claveSexo,
							@WebParam(name = "claveEstadoCivil") String claveEstadoCivil,
							@WebParam(name = "idEstadoNacimiento") String idEstadoNacimiento,
							@WebParam(name = "fechaNacimiento") Date fechaNacimiento,
							@WebParam(name = "codigoPostal") String codigoPostal,
							@WebParam(name = "idCiudad") String idCiudad,
							@WebParam(name = "idEstado") String idEstado,
							@WebParam(name = "colonia") String colonia,
							@WebParam(name = "calleNumero") String calleNumero,
							@WebParam(name = "telefonoCasa") String telefonoCasa,
							@WebParam(name = "telefonoOficina") String telefonoOficina,
							@WebParam(name = "email") String email,
							@WebParam(name = "numeroSerie") String numeroSerie,
							@WebParam(name = "numeroMotor") String numeroMotor) {		
		CotizacionDTO cotizacion = new CotizacionDTO();
		try {
			//mandamos llamar el metodo que crea la cotizacion, guarda inciso y guarda informacion del cliente
			cotizacion = wsCotizacionService.creaCotizacion(idSucursal,claveUsuario, idNegocio,descuento, folio,idPaquete,  estilo,  modelo,  marca,
					idEstadoCirculacion, idMunicipcioCirculacion,  valorFactura,	nombreAsegurado, apellidoPaterno,apellidoMaterno, RFC, 
					claveSexo, claveEstadoCivil, idEstadoNacimiento, fechaNacimiento,codigoPostal,idCiudad, idEstado, 
					colonia, calleNumero, telefonoCasa, telefonoOficina, email, numeroSerie, numeroMotor, null);
				
			if(generaCotizacion){
				Map<String, Object> excResultado = wsCotizacionService.evalueExcepcion(cotizacion);
				if(excResultado!= null && !excResultado.containsKey("statusExcepcion") && "true".equals(String.valueOf(excResultado.get("statusExcepcion")))){//JFGG
					//Borra Cotizacion
					cotizacionService.eliminarCotizacion(cotizacion.getIdToCotizacion());
					return String.valueOf(excResultado.get("mensaje"));
				} else {//JFGG
					return "{idCotizacion:" + cotizacion.getIdToCotizacion() + ", primaTotal:" + cotizacion.getValorPrimaTotal() + "}";
				}//JFGG
			}
			else{
				
				BigDecimal valorPrimaTotal = cotizacion.getValorPrimaTotal();
				//Borra Cotizacion
				cotizacionService.eliminarCotizacion(cotizacion.getIdToCotizacion());
				
				return "{primaTotal:" + valorPrimaTotal + "}";
			}				
		} catch (SystemException e) {
			LOG.error(e.getMessage(), e);
			return "No fue posible crear la cotizacion, "  + e.getMessage();
		} catch (Exception e){
			LOG.error(e.getMessage(), e);
			return "No fue posible crear la cotizacion" + e.getMessage();
		}		
	}
	
	/**
	 * Servicio para emitir poliza segun el idCotizacion recibido, el folio se recibe como parametro para cancelar aquellas cotizaciones asociadas
	 * al mismo
	 * @param folio
	 * @param idCotizacion
	 * @param numeroSerie
	 * @param numeroMotor
	 * @return idPoliza,numeropoliza
	 */
	@WebMethod(operationName = "emitir")
	@WebResult(targetNamespace = "")
	public String emitir(@WebParam(name = "idPoliza") BigDecimal idPoliza,
						 @WebParam(name = "folio") String folio,
						 @WebParam(name = "numeroCredito") String numeroCredito,
						 @WebParam(name = "idCotizacion") BigDecimal idCotizacion,
						 @WebParam(name = "numeroSerie") String numeroSerie,
						 @WebParam(name = "numeroMotor") String numeroMotor) {		
		try {
			PolizaDTO poliza = wsCotizacionService.emitir(idCotizacion, numeroCredito, numeroSerie, numeroMotor,idPoliza, folio);
			return "{idPoliza:" + poliza.getIdToPoliza() + ", numeroPoliza:" + poliza.getNumeroPolizaFormateada() + "}";
		}
		catch (SystemException e) {
			LOG.error(e.getMessage(), e);
			return "No fue posible emitir, "  + e.getMessage();
		}	
		catch (Exception e) {
			LOG.error(e.getMessage(), e);
			return "No fue posible emitir, " + e.getMessage();
		}		
	}

	
	/**
	 * Servicio que cancela las cotizaciones asociadas al folio, excepto la cotizacion que tiene el idCotizacion recibido como parametro.
	 * Cuando el numero credito es null nos dice que el cliente no ejercio el credito de autoplazo en originacion.
	 * Cuando si recibimos el numero de credito nos dice que el cliente si aplico el credito de autoplazo, sin embargo rechazo el seguro de auto AFIRME. 
	 * @param numeroCredito
	 * @param folio
	 * @param idCotizacion
	 * @return
	 */
	@WebMethod(operationName = "cancelar")
	@WebResult(targetNamespace = "")
	public String cancelar(
			@WebParam(name = "numeroCredito") String numeroCredito,
			@WebParam(name = "folio") String folio,
			@WebParam(name = "idCotizacion") BigDecimal idCotizacion) {

		String result = "";
		CotizacionDTO cotizacion = entidadService.findById(CotizacionDTO.class,
				idCotizacion);

		if (numeroCredito != null && StringUtils.isNotEmpty(numeroCredito)) {
			cotizacion.setNumeroCredito(numeroCredito);
			entidadService.save(cotizacion);
		}

		cotizacionService.cancelarCotizacionByFolio(cotizacion.getIdToCotizacion(), folio);

		return result;
	}
	
	@WebMethod(operationName = "impresionPoliza")
	@WebResult(targetNamespace = "")	
	public byte[] impresionPoliza (@WebParam(name = "idPoliza") BigDecimal idPoliza){
		 	 
		try {
			return wsCotizacionService.imprimirPoliza(idPoliza);
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			return "".getBytes();
		}
		 
	}
	
	@WebMethod(operationName = "paqueteList")
	@WebResult(targetNamespace = "")	
	public List<String> listaPaquetesPorNegocio (@WebParam(name = "idNegocio") Long idNegocio){
			List<String> paquetes = new ArrayList<String>(1);
			try {
				paquetes = wsCotizacionService.listPaquetesByIdNegocio(idNegocio);
			} catch (Exception e) {
				LOG.error(e.getMessage(), e);
				paquetes.add("1-AMPLIA");
			}			
		
		return paquetes;
	}
	
	@WebMethod(operationName = "emision")
	@WebResult(targetNamespace = "")
	public String emision (
			 @WebParam(name = "idPoliza") BigDecimal idPoliza,
			 @WebParam(name = "folio") String folio,
			 @WebParam(name = "numeroCredito") String numeroCredito,
			 @WebParam(name = "idCotizacion") BigDecimal idCotizacion,
			 @WebParam(name = "numeroSerie") String numeroSerie,
			 @WebParam(name = "numeroMotor") String numeroMotor,
			 @WebParam(name = "cliente") Cliente cliente){
		try {
			PolizaDTO poliza = wsCotizacionService.emision(idCotizacion, numeroCredito, numeroSerie, numeroMotor,idPoliza, folio, cliente);
			return "{idPoliza:" + poliza.getIdToPoliza() + ", numeroPoliza:" + poliza.getNumeroPolizaFormateada() + "}";
		}catch (SystemException e) {
			LOG.error(e.getMessage(), e);
			return "No fue posible emitir, "  + e.getMessage();
		}catch (Exception e) {
			LOG.error(e.getMessage(), e);
			return "No fue posible emitir, " + e.getMessage();
		}					
	}	

	/**
	 * Servicio para crear cotizacion desde originacion, retornando idCotizacion y primatotal correspondiente.
	 * 
	 * @param json
	 * @return
	 */
	@WebMethod(operationName = "cotizacion")
	@WebResult(targetNamespace = "")	
	public String cotizacion(@WebParam(name = "json") String json){

		Gson gson = new Gson();
		CotizacionView cotizacionView = null;
		CotizacionDTO cotizacion = new CotizacionDTO();
		
		try {
			cotizacionView = gson.fromJson(json, CotizacionView.class);
		} catch (JsonParseException e) {
			return "Error al parsear la cadena JSON de los datos de entrada";
		}

		if (cotizacionView == null) {
			return "Favor de enviar los datos de la cotizacion";
		}
		
		try {
			cotizacion = wsCotizacionService.creaCotizacion(cotizacionView);
			
			if(cotizacionView.getDatosAdicionales() != null && cotizacionView.getDatosAdicionales().getGeneraCotizacion() == true){
				Map<String, Object> excResultado = wsCotizacionService.evalueExcepcion(cotizacion);
				if(excResultado!= null && !excResultado.containsKey("statusExcepcion") && "true".equals(String.valueOf(excResultado.get("statusExcepcion")))){//JFGG
					//Borra Cotizacion
					cotizacionService.eliminarCotizacion(cotizacion.getIdToCotizacion());
					return String.valueOf(excResultado.get("mensaje"));
				} else {//JFGG
					return "{idCotizacion:" + cotizacion.getIdToCotizacion() + ", primaTotal:" + cotizacion.getValorPrimaTotal() + "}";
				}//JFGG
			}
			else{				
				BigDecimal valorPrimaTotal = cotizacion.getValorPrimaTotal();				
				cotizacionService.eliminarCotizacion(cotizacion.getIdToCotizacion());				
				return "{primaTotal:" + valorPrimaTotal + "}";
			}
		} catch (SystemException e) {
			return "No fue posible crear la cotizacion, "  + e.getMessage();
		} catch (Exception e){
			return "No fue posible crear la cotizacion. "+e.getMessage();
		}
	}
}
