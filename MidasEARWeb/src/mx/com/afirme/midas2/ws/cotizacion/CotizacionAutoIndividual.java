package mx.com.afirme.midas2.ws.cotizacion;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonParseException;

import mx.com.afirme.midas.interfaz.cliente.ClienteGenericoDTO;
import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas2.dao.suscripcion.cotizacion.auto.ws.CoberturaView;
import mx.com.afirme.midas2.dao.suscripcion.cotizacion.auto.ws.CotizacionCoberturaView;
import mx.com.afirme.midas2.dao.suscripcion.cotizacion.auto.ws.CotizacionView;
import mx.com.afirme.midas2.dao.suscripcion.cotizacion.auto.ws.EmisionView;
import mx.com.afirme.midas2.dao.suscripcion.cotizacion.auto.ws.EstiloView;
import mx.com.afirme.midas2.dao.suscripcion.cotizacion.auto.ws.MotivosEndosoView;
import mx.com.afirme.midas2.dao.suscripcion.cotizacion.auto.ws.ParametrosImpresionCotizacionView;
import mx.com.afirme.midas2.dao.suscripcion.cotizacion.auto.ws.ParametrosImpresionPolizaView;
import mx.com.afirme.midas2.dao.suscripcion.cotizacion.auto.ws.flotilla.CotizarEndosoFlotillaRequest;
import mx.com.afirme.midas2.dao.suscripcion.cotizacion.auto.ws.flotilla.CotizarEndosoFlotillaResponse;
import mx.com.afirme.midas2.dao.suscripcion.cotizacion.auto.ws.flotilla.EmitirEndosoFlotillaRequest;
import mx.com.afirme.midas2.dao.suscripcion.cotizacion.auto.ws.flotilla.EmitirEndosoFlotillaResponse;
import mx.com.afirme.midas2.domain.emision.ppct.ReciboSeycos;
import mx.com.afirme.midas2.dto.endoso.EndosoTransporteDTO;
import mx.com.afirme.midas2.dto.endoso.cotizacion.auto.CotizacionEndosoDTO;
import mx.com.afirme.midas2.dto.endoso.recibos.RecibosEndosoTransporteDTO;
import mx.com.afirme.midas2.dto.impresiones.ReferenciaBancariaDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.recibos.MovimientoReciboDTO;
import mx.com.afirme.midas2.exeption.ApplicationException;
import mx.com.afirme.midas2.service.avisos.AvisosService;
import mx.com.afirme.midas2.service.bitemporal.endoso.cotizacion.auto.EndosoService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.componente.incisos.ListadoIncisosDinamicoService;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.WSCotizacionGenericaService;

@Stateless
@WebService(name = "CotizacionAutoIndividual", 
		targetNamespace = "http://segurosafirme.com.mx/cotizacion/cotizacionautoindividual")
public class CotizacionAutoIndividual {
	private static final Logger LOG = Logger
			.getLogger(CotizacionAutoIndividual.class);
	private static final Short MEDIO_PAGO_EFECTIVO = 15;
	private static final BigDecimal ID_PRODUCTO_AUTO_INDIVIDUAL = new BigDecimal(191);
	private static final BigDecimal ID_PRODUCTO_AUTO_FLOTILLA = new BigDecimal(192);
	public static final int AVISO_EMISION_WS = 3;
	
	@EJB
	private EntidadService entidadService;	
	@EJB
	private WSCotizacionGenericaService wsCotizacionService;
	@EJB
	private AvisosService avisosService;
	@EJB
	private ListadoIncisosDinamicoService listadoIncisosDinamicoService;
	@EJB(beanName = "UsuarioServiceDelegate")
	private UsuarioService usuarioService;
	@EJB
	private EndosoService endosoService;
	

	/**
	 * Servicio para crear cotizacion
	 * 
	 * @param idNegocio
	 * @param idToNegProducto
	 * @param idTipoPoliza
	 * @param idLineaNegocio
	 * @param idPaquete
	 * @param idFormaPago
	 * @param estilo
	 * @param modelo
	 * @param marca
	 * @param idEstadoCirculacion
	 * @param idMunicipcioCirculacion
	 * @param descuento
	 * @param folio
	 * @param nombreAsegurado
	 * @param apellidoPaterno
	 * @param apellidoMaterno
	 * @param RFC
	 * @param claveSexo
	 * @param claveEstadoCivil
	 * @param idEstadoNacimiento
	 * @param fechaNacimiento
	 * @param codigoPostal
	 * @param idCiudad
	 * @param idEstado
	 * @param colonia
	 * @param calleNumero
	 * @param telefonoCasa
	 * @param telefonoOficina
	 * @param email
	 * @param idAgente
	 * @param observaciones
	 * @param idCliente Obligatorio si no se establecen los datos del cliente
	 * @param coberturas Lista de los valores de las coberturas que requieran actualizarse<br>
	 * <p> Para obtenerla se debe invocar previamente al metodo <b>obtenerCoberturas</b><br>
	 * La lista de coberturas es opcional: Si no se envia, la cotizacion se creara con las coberturas 
	 * por default<br>Y en caso que si se envie la lista, el servicio realizara las validaciones.<br>
	 * Posibles mensajes de no validacion:<br>
	 * <em>"La cobertura no se encuentra y debe ser contratada"</em>
	 * <em>"La cobertura debe ser contratada"</em>
	 * <em>"La suma asegurada esta fuera de limite"</em>
	 * <em>"El valor del deducible no es valido"</em>
	 * <br>
	 * @param inicioVigencia // Opcional Si se envia debera enviarse tambien la fecha finVigencia.<br>
	 * @param finVigencia // Opcional Si se envia debera enviarse tambien la fecha inicioVigencia<br>
	 *  <p>En caso que no se envien las fechas: A la fechaInicioVigencia se le asigna la fecha actual<br>
	 *  y a la fecha finVigencia un año más que la fecha actual<br>
	 * @param token
	 * @return Cadena en formato JSON con la siguiente estructura:
	 * <p>
	 * <code> 
	 * {<br>
	 *		"esquemaPago": {<br>
	 *			"1": {<br>
	 *				"noRecibosSubsecuentes": 0,<br>
	 *				"pagoInicial": "$2,367.22",<br>
	 *				"pagoSubsecuente": 0,<br>
	 *				"formaPago": "ANUAL"<br>
	 *			},<br>
	 *			"2": {<br>
	 *				"noRecibosSubsecuentes": 9,<br>
	 *				"pagoInicial": "$318.09",<br>
	 *				"pagoSubsecuente": "$246.45",<br>
	 *				"formaPago": "MENSUAL"<br>
	 *			}<br>
	 *		},<br>
	 *		"coberturas": {<br>
	 *			"1": {<br>
	 *				"deducible": 5.0,<br>
	 *				"descripcion": "DAÑOS MATERIALES",<br>
	 *				"sumaAsegurada": "Valor Comercial",<br>
	 *				"primaNeta": 0.0<br>
	 *			},<br>
	 *			"2": {<br>
	 *				"deducible": 5.0,<br>
	 *				"descripcion": "ROBO TOTAL",<br>
	 *				"sumaAsegurada": "Valor Comercial",<br>
	 *				"primaNeta": 0.0<br>
	 *			},<br>
	 *			"3": {<br>
	 *				"deducible": 0.0,<br>
	 *				"descripcion": "RESPONSABILIDAD CIVIL",<br>
	 *				"sumaAsegurada": "4000000.0",<br>
	 *				"primaNeta": 842.5<br>
	 *			},<br>
	 *			"4": {<br>
	 *				"deducible": 5.0,<br>
	 *				"descripcion": "EXENCIÓN DE DEDUCIBLES DM",<br>
	 *				"sumaAsegurada": "Amparada",<br>
	 *				"primaNeta": 0.0<br>
	 *			},<br>
	 *			"5": {<br>
	 *				"deducible": 0.0,<br>
	 *				"descripcion": "GASTOS MÉDICOS",<br>
	 *				"sumaAsegurada": "100000.0",<br>
	 *				"primaNeta": 189.98<br>
	 *			},<br>
	 *			"6": {<br>
	 *				"deducible": 0.0,<br>
	 *				"descripcion": "ACCIDENTES AUTOMOVILÍSTICOS AL CONDUCTOR",<br>
	 *				"sumaAsegurada": "50000.0",<br>
	 *				"primaNeta": 21.2<br>
	 *			},<br>
	 *			"7": {<br>
	 *				"deducible": 0.0,<br>
	 *				"descripcion": "ASISTENCIA JURÍDICA",<br>
	 *				"sumaAsegurada": "Valor Comercial",<br>
	 *				"primaNeta": 168.22<br>
	 *			},<br>
	 *			"8": {<br>
	 *				"deducible": 0.0,<br>
	 *				"descripcion": "ASISTENCIA EN VIAJES Y VIAL KM \"0\"",<br>
	 *				"sumaAsegurada": "Amparada",<br>
	 *				"primaNeta": 131.21<br>
	 *			},<br>
	 *			"9": {<br>
	 *				"deducible": 0.0,<br>
	 *				"descripcion": "RESPONSABILIDAD CIVIL EN USA Y CANADA LUC",<br>
	 *				"sumaAsegurada": "Amparada",<br>
	 *				"primaNeta": 0.0<br>
	 *			},<br>
	 *			"10": {<br>
	 *				"deducible": 0.0,<br>
	 *				"descripcion": "PROTECCION AUTO SIGUE AFIRME",<br>
	 *				"sumaAsegurada": "Amparada",<br>
	 *				"primaNeta": 462.6<br>
	 *			}<br>
	 *		},<br>
	 *		"idToCotizacion": 9271540,<br>
	 *		"iva": 326.5136,<br>
	 *		"folio": "AUI-9271540",<br>
	 *		"primaTotal": 2367.2236,<br>
	 *		"recargo": 0.0,<br>
	 *		"descuentos": 0.0,<br>
	 *		"primaNeta": 1815.71,<br>
	 *		"derechoPago": 225.0<br>
	 *	}<br>
	 * </code>
	 * 
	 * @see #getListNegocios(Integer, String)
	 * @see #getListProductos(Long, String)
	 * @see #getListTiposPoliza(Long, String)
	 * @see #getListLineasNegocio(BigDecimal, String)
	 */
	@WebMethod(operationName = "cotizar")
	@WebResult(targetNamespace = "")	
	public String cotizar(	@WebParam(name = "idNegocio") Long idNegocio,
							@WebParam(name = "idProducto") BigDecimal idToNegProducto,
							@WebParam(name = "idTipoPoliza") BigDecimal idTipoPoliza,
							@WebParam(name = "idLineaNegocio") BigDecimal idToNegSeccion,
							@WebParam(name = "idPaquete") Long idPaquete,
							@WebParam(name = "idFormaPago") Integer idFormaPago,
							@WebParam(name = "estilo") Integer estilo,
							@WebParam(name = "modelo") Integer modelo,
							@WebParam(name = "marca") Integer marca,
							@WebParam(name = "idEstadoCirculacion") String idEstadoCirculacion,
							@WebParam(name = "idMunicipioCirculacion") String idMunicipcioCirculacion,
							@WebParam(name = "descuento") Double descuento,
							@WebParam(name = "folio") String folio,
							@WebParam(name = "nombreAsegurado") String nombreAsegurado,
							@WebParam(name = "apellidoPaterno") String apellidoPaterno,
							@WebParam(name = "apellidoMaterno") String apellidoMaterno,
							@WebParam(name = "RFC") String rfc,
							@WebParam(name = "claveSexo") String claveSexo,
							@WebParam(name = "claveEstadoCivil") String claveEstadoCivil,
							@WebParam(name = "idEstadoNacimiento") String idEstadoNacimiento,
							@WebParam(name = "fechaNacimiento") Date fechaNacimiento,
							@WebParam(name = "codigoPostal") String codigoPostal,
							@WebParam(name = "idCiudad") String idCiudad,
							@WebParam(name = "idEstado") String idEstado,
							@WebParam(name = "colonia") String colonia,
							@WebParam(name = "calleNumero") String calleNumero,
							@WebParam(name = "telefonoCasa") String telefonoCasa,
							@WebParam(name = "telefonoOficina") String telefonoOficina,
							@WebParam(name = "email") String email,
							@WebParam(name = "idAgente") Long idAgente,
							@WebParam(name = "observaciones") String observaciones,
							@WebParam(name = "idCliente") BigDecimal idCliente,
							@WebParam(name = "coberturas") List<CoberturaView> coberturaList,
							@WebParam(name = "inicioVigencia") Date inicioVigencia,
							@WebParam(name = "finVigencia") Date finVigencia,
							@WebParam(name = "token") String token){
		
		LOG.info(this.getClass().getName()+".cotizar ... entrando a cotizar");
		Map<String, Object> map = new HashMap<String, Object>(1);
		
		try {
			final int lengthFiledId = 5;
			BigDecimal idProducto = ID_PRODUCTO_AUTO_INDIVIDUAL;
			
			//mandamos llamar el metodo que crea la cotizacion, guarda inciso y guarda informacion del cliente
			map = wsCotizacionService.creaCotizacion(idNegocio, descuento, folio, idPaquete, idFormaPago,
					estilo,  modelo, marca, StringUtils.leftPad(idEstadoCirculacion, lengthFiledId,"0"), 
					StringUtils.leftPad(idMunicipcioCirculacion, lengthFiledId,"0"), nombreAsegurado, apellidoPaterno,apellidoMaterno, 
					rfc, claveSexo, claveEstadoCivil, StringUtils.leftPad(idEstadoNacimiento, lengthFiledId,"0"), fechaNacimiento,
					codigoPostal, StringUtils.leftPad(idCiudad, lengthFiledId,"0"), StringUtils.leftPad(idEstado, lengthFiledId,"0"), 
					colonia, calleNumero, telefonoCasa, telefonoOficina, email, idToNegProducto, idTipoPoliza, idToNegSeccion, idAgente,
					observaciones, idCliente, coberturaList, inicioVigencia, finVigencia, token);

			Gson gson = new Gson();
			return gson.toJson(map);
		} catch (SystemException e) {
			return "No fue posible crear la cotizacion, "  + e.getMessage();
		} catch (Exception e){
			return "No fue posible crear la cotizacion. "+e.getMessage();
		}
	}
	
	/**
	 * Método para emitir una póliza
	 * 
	 * @param idCotizacion
	 * @param numeroSerie
	 * @param numeroMotor
	 * @param medioPago
	 * @param formaPago
	 * @param numeroPlaca
	 * @param repuve
	 * @param tarjetaHabiente
	 * @param rfc
	 * @param codigoPostal
	 * @param idEstado
	 * @param idMunicipio
	 * @param idColonia
	 * @param nombreColonia
	 * @param calleNumero
	 * @param telefono
	 * @param correo
	 * @param idBanco
	 * @param idTipoTarjeta
	 * @param numeroTarjeta
	 * @param codigoSeguridadTarjeta
	 * @param fechaVencimientoTarjeta
	 * @param clavePais
	 * @param nombreAsegurado Parametro opcional, establecer el nombre completo si el asegurado es diferente al cliente, este saldra en la impresion de la poliza como Cliente y/o Asegurado
	 * @param token
	 * @return Cadena en formato JSON con la siguiente estructura:
	 * 
	 * <p>
	 * <code>
	 * {<br>
	 *		"idPoliza": 1329240,<br>
	 *		"numeroPoliza": "3101-860384-00",<br>
	 * 		"mensaje": "La póliza se emitió correctamente. El número de Póliza es: "<br>
	 * }<br>
	 * </code>
	 * 
	 */
	@WebMethod(operationName = "emitir")
	@WebResult(targetNamespace = "")
	public String emitir(
						 @WebParam(name = "idCotizacion") BigDecimal idCotizacion,
						 @WebParam(name = "numeroSerie") String numeroSerie,
						 @WebParam(name = "numeroMotor") String numeroMotor,
						 @WebParam(name = "medioPago") Short medioPago,
						 @WebParam(name = "formaPago") Integer formaPago,
						 @WebParam(name = "numeroPlaca") String numeroPlaca,
						 @WebParam(name = "repuve") String repuve,
						 @WebParam(name = "tarjetaHabiente") String tarjetaHabiente,
						 @WebParam(name = "rfc") String rfc,
						 @WebParam(name = "codigoPostal") String codigoPostal,
						 @WebParam(name = "idEstado") String idEstado,
						 @WebParam(name = "idMunicipio") String idMunicipio,
						 @WebParam(name = "idColonia") String idColonia,
						 @WebParam(name = "nombreColonia") String nombreColonia,
						 @WebParam(name = "calleNumero") String calleNumero,
						 @WebParam(name = "telefono") String telefono,
						 @WebParam(name = "correo") String correo,
						 @WebParam(name = "idBanco") Integer idBanco,
						 @WebParam(name = "idTipoTarjeta") String idTipoTarjeta,
						 @WebParam(name = "numeroTarjeta") String numeroTarjeta,
						 @WebParam(name = "codigoSeguridadTarjeta") String codigoSeguridadTarjeta,
						 @WebParam(name = "fechaVencimientoTarjeta") String fechaVencimientoTarjeta,
						 @WebParam(name = "clavePais") String clavePais,
						 @WebParam(name = "nombreAsegurado") String nombreAsegurado,
						 @WebParam(name = "token") String token) {
		LOG.info(this.getClass().getName()+".emitir ... entrando a emitir");
		Map<String, String> result = new HashMap<String, String>();
		
		try {
			if (medioPago == MEDIO_PAGO_EFECTIVO){//testing opcion uno no soporta transaccion - usado para pagos en efectivo
				result = wsCotizacionService.emitirPoliza(numeroSerie, numeroMotor, numeroPlaca, formaPago, 
						idCotizacion, medioPago, idTipoTarjeta, idBanco, fechaVencimientoTarjeta, codigoSeguridadTarjeta, tarjetaHabiente, numeroTarjeta, 
						correo, telefono, clavePais, idEstado, idMunicipio, idColonia, 
						nombreColonia, calleNumero, codigoPostal, rfc, nombreAsegurado, token);	
			} else{//testing opcion dos, transaccion requerida
				result = wsCotizacionService.emitir(numeroSerie, numeroMotor, numeroPlaca, formaPago, 
						idCotizacion, medioPago, idTipoTarjeta, idBanco, fechaVencimientoTarjeta, codigoSeguridadTarjeta, tarjetaHabiente, numeroTarjeta, 
						correo, telefono, clavePais, idEstado, idMunicipio, idColonia, 
						nombreColonia, calleNumero, codigoPostal, rfc, nombreAsegurado, token);
			}
			
			String icono = result.get("icono");
			String mensaje = result.get("mensaje");
			String idToPoliza = result.get("idpoliza");

			if (icono.equals(PolizaDTO.ICONO_CONFIRM)){
				//envia avisos a cobranza
				avisosService.enviarAviso(BigDecimal.valueOf(Long.valueOf(idToPoliza)),AVISO_EMISION_WS,null);
				
				PolizaDTO poliza = entidadService.findById(PolizaDTO.class,BigDecimal.valueOf(Long.valueOf(idToPoliza)));
				return "{\"idPoliza\":" + poliza.getIdToPoliza() + ", \"numeroPoliza\":\"" + poliza.getNumeroPolizaFormateada() + "\", \"mensaje\":\""+mensaje+" "+poliza.getNumeroPolizaFormateada()+"\"}";				
			} else if (icono.equals(PolizaDTO.ICONO_ERROR)) {
				return "{\"mensaje\":\""+mensaje+"\"}";
			}
			
			return "{\"mensaje\":\""+mensaje+"\"}";
		} catch (SystemException e) {
			return "{\"mensaje\":\" No fue posible emitir, "+e.getMessage()+"\"}";
		} catch (Exception e) {
			return "{\"mensaje\":\" No fue posible emitir: "+e.getMessage()+"\"}";
		}
		
	}
	
	/**
	 * Método para imprimir una póliza
	 * 
	 * @param idPoliza
	 * @param token
	 * @return retorna los bytes para la impresión de una póliza
	 * @deprecated usar servicio <code>imprimir</code>
	 */
	@WebMethod(operationName = "imprimirPoliza")
	@WebResult(targetNamespace = "")	
	public byte[] imprimirPoliza (@WebParam(name = "idPoliza") BigDecimal idPoliza,
								@WebParam(name = "token") String token){
		try {
			return wsCotizacionService.imprimirPoliza(idPoliza, token);
		} catch (Exception e) {
			return e.getMessage().getBytes();
		}
	}
	
	/**
	 * Método para imprimir una póliza
	 * 
	 * @param json
	 *            Cadena en formato JSON con el siguiente formato:
	 *            <p>
	 *            <code>
	 * 			{<br>
	 * 				numeroPoliza: "310186010800", // Opcional si se manda in idPoliza<br>
	 * 				idPoliza: 10598, // Opcional si se manda un numeroPoliza <br>
	 * 				caratula: true,<br>
	 * 				recibo: true,<br>
	 * 				todosLosIncisos: true,<br>
	 * 				certificadoNUPorInciso: true,<br>
	 * 				anexos: false,<br>
	 * 				condicionesEspeciales: false,<br>
	 * 				situacionActual: false,<br>
	 * 				incluirReferenciasBancarias: false<br>
	 * 			}<br>
	 * 			</code>
	 * @param token
	 *            Cadena con el token de sesion
	 * @return retorna los bytes para la impresión de una póliza
	 */
	@WebMethod(operationName = "imprimir")
	@WebResult(targetNamespace = "")	
	public byte[] imprimir(@WebParam(name = "json") String json,
								@WebParam(name = "token") String token){
		try {
			final Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss").create();
			ParametrosImpresionPolizaView params = gson.fromJson(json, ParametrosImpresionPolizaView.class);
			return wsCotizacionService.imprimirPoliza(params, token);
		} catch (Exception e) {
			return e.getMessage().getBytes();
		}
	}
	
	/**
	 * Obtiene la lista de agentes de un usuario dado
	 * 
	 * @param token
	 *            Cadena con el token de sesion
	 * @return lista de agentes de un usuario dado
	 */
	@WebMethod(operationName = "getListAgentes")
	@WebResult(targetNamespace = "")
	public String getListAgentes(@WebParam(name = "token") String token) {
		Gson gson = new Gson();
		Map<Long, String> map = null;
		
		try {
			map = wsCotizacionService.getListAgentes( token );
		} catch (SystemException e){
			return "No fue posible obtener los agentes. "+e.getMessage();
		}
		
		return gson.toJson(map);
	}
	
	/**
	 * Obtiene la lista de negocios de una agente dado
	 * 
	 * @param idAgente
	 * @param token
	 * @return lista de negocios de una agente dado
	 */
	@WebMethod(operationName = "getListNegocios")
	@WebResult(targetNamespace = "")	
	public String getListNegocios(@WebParam(name = "idAgente") Integer idAgente,
			 @WebParam(name = "token") String token){
		Gson gson = new Gson();
		Map<Long, String> map = null;
		
		try {
			map = wsCotizacionService.getListNegocios(idAgente,token);
		} catch(SystemException e){
			return "No fue posible obtener los negocios. "+e.getMessage();
		}
		
		return gson.toJson(map);
	}
	
	/**
	 * Obtiene la lista de marcas de una línea de negocio dado
	 * 
	 * @param idLineaNegocio
	 * @param token
	 * @return lista de marcas de una línea de negocio dado
	 */
	@WebMethod(operationName = "getListMarcas")
	@WebResult(targetNamespace = "")
	public String getListMarcas(@WebParam(name = "idLineaNegocio") BigDecimal idLineaNegocio,
			 @WebParam(name = "token") String token){
		Gson gson = new Gson();
		Map<BigDecimal, String> map = null;
		
		try {
			map = wsCotizacionService.getListMarcas(idLineaNegocio,token);
		} catch(SystemException e){
			return "No fue posible obtener las marcas. " + e.getMessage();
		}
		
		return gson.toJson(map);
	}
	
	/**
	 * Obtiene la lista de estilos de una marca dada
	 * 
	 * @return lista de estilos de la marca dada
	 * @deprecated Este servicio se quitara en una proxima actualizacion, en su lugar usar <code>buscarEstilo</code>
	 */
	@WebMethod(operationName = "getListEstilos")
	@WebResult(targetNamespace = "")
	@Deprecated
	public String getListEstilos(@WebParam(name = "idTcMarcaVehiculo") BigDecimal idTcMarcaVehiculo,
			 @WebParam(name = "token") String token){
		Gson gson = new Gson();
		Map<String, String> map = null;
		
		try {
			map = wsCotizacionService.getListEstilos(idTcMarcaVehiculo,token);
		} catch(SystemException e){
			return "No fue posible obtener los estilos. "+e.getMessage();
		}
		
		return gson.toJson(map);
	}
	
	
	/**
	 * Busca los estilos de vehiculos disponibles para los filtros enviados.
	 * Como regla <code>idMarca</code> ni <code>idLineaNegocio</code> pueden ser null.
	 * 
	 * @param json
	 *            Cadena en formato JSON con los valores de filtros disponibles para buscar un estilo, ejemplo: 
	 *            <p>
	 *            <code>
	 *            {<br>
	 *            	"idMarca" : 343,<br>
	 *            	"modelo" : 2016,<br>
	 *            	"idLineaNegocio" : 6993,<br>
	 *            	"descripcion" : "A4 TRENDY"<br>
	 *            }<br>
	 *            </code>
	 * @param token
	 * 			Cadena con el token de sesion
	 * @return Cadena en formato JSON con el resultado de la busqueda, ejemplo:
	 * 			<p>
	 * 			<code>
	 * 				{<br>
	 *			    	"81538": "81538 - AU A4 TRENDY PLUS 2.0T S TRONIC QUATTRO L4 4P ABS VP CQ CB",<br>
	 *				    "88062": "88062 - AU A4 TRENDY PLUS 1.8L MULTITRONIC",<br>
	 *				    "88837": "88837 - AU A4 TRENDY PLUS FRONT 2.0L MULTITRONIC"<br>
	 *				}<br>
	 * 			</code>
	 * @see #getListMarcas(BigDecimal, String)	 
	 * @see #getListModelos(BigDecimal, BigDecimal, String) 
	 * @see #getListLineasNegocio(BigDecimal, String) 
	 */
	@WebMethod(operationName = "buscarEstilo")
	@WebResult(targetNamespace = "")
	public String buscarEstilo(@WebParam(name = "json") String json,
			 @WebParam(name = "token") String token){
		Gson gson = new Gson();
		Map<String, String> map = null;
		
		try {
			final EstiloView filtro = gson.fromJson(json, EstiloView.class);
			map = wsCotizacionService.buscarEstilo(filtro,token);
		} catch(Exception e){
			return "No fue posible obtener los estilos. " + e.getMessage();
		}
		
		return gson.toJson(map);
	}
	
	
	/**
	 * Busca un cliente en el sistema y regresa su id para utilizarlo en la cotización.
	 * 
	 * @param json
	 * 		Cadena en formato JSON con los valores de filtros disponibles para buscar un cliente, ejemplos: 
	 * 	          <p>
	 * 			  <b>Busqueda por nombre</b><br>
	 *            <code>
	 *            {<br>
	 *            	"nombre" : "Rick",<br>
	 *            	"apellidoPaterno" : "Deckard",<br>
	 *            	"apellidoMaterno" : "Ford"<br>
	 *            }<br>
	 *            </code>
	 *            <p>
	 *            <b>Busqueda por RFC</b><br>
	 *            <code>
	 *            {<br>
	 *            	"codigoRFC" : "SEAM80D3126L2"<br>
	 *            }<br>
	 *            </code>
	 *            <p>
	 *            <b>Busqueda por Negocio</b> El negocio puede estar ligado a determinados clientes en este caso usar este parametro<br>
	 *            <code>
	 *            {<br>
	 *            	"idNegocio" : "100"<br>
	 *            }<br>
	 *            </code>
	 * @param token Cadena con el token de sesion
	 * @return Cadena en formato JSON con el resultado de la búsqueda
	 * 
	 * @see #getListNegocios(Integer, String)
	 */
	@WebMethod(operationName = "buscarCliente")
	@WebResult(targetNamespace = "")
	public String buscarCliente(@WebParam(name = "json") String json, @WebParam(name = "token") String token){
		final Gson gson = new Gson();
		ClienteGenericoDTO clienteFiltro = gson.fromJson(json, ClienteGenericoDTO.class);
		List<Map<String, Object>> result = null;
		try {
			result = wsCotizacionService.buscarCliente(clienteFiltro, token);
		} catch (Exception e) {
			return "No fue posible obtener la lista de clientes. " + e.getMessage();
		}
		return new Gson().toJson(result);
	}

	/**
	 * obtiene la lista de estados configurados al negocio dado
	 * 
	 * @param idNegocio
	 * @param token
	 * @return lista de estados configurados al negocio dado
	 */
	@WebMethod(operationName = "getListEstados")
	@WebResult(targetNamespace = "")
	public String getListEstados(@WebParam(name = "idNegocio") Long idNegocio,
			 @WebParam(name = "token") String token){
		Gson gson = new Gson();
		Map<String, String> map = null;
		
		try {
			map = wsCotizacionService.getListEstados(idNegocio,token);
		} catch(SystemException e){
			return "No fue posible obtener los estados. "+e.getMessage();
		}
		
		return gson.toJson(map);
	}
	
	/**
	 * Obtiene la lista de municipios de un estado dado
	 * 
	 * @param idEstado
	 * @param token
	 * @return lista de municipios de un estado dado
	 */
	@WebMethod(operationName = "getListMunicipios")
	@WebResult(targetNamespace = "")
	public String getListMunicipios(@WebParam(name = "idEstado") String idEstado,
			 @WebParam(name = "token") String token){
		Gson gson = new Gson();
		Map<String, String> map = null;
		
		try {
			map = wsCotizacionService.getListMunicipios(idEstado,token);
		} catch(SystemException e){
			return "No fue posible obtener los municipios. "+e.getMessage();
		}
		
		return gson.toJson(map);
	}
	
	/**
	 * obtiene la lista de paquetes de una línea de negocio dado
	 * 
	 * @param idToNegSeccion
	 * @param token
	 * @return lista de paquetes de una línea de negocio dado
	 */
	@WebMethod(operationName = "getListPaquetes")
	@WebResult(targetNamespace = "")
	public String getListPaquetes(@WebParam(name = "idLineaNegocio") BigDecimal idToNegSeccion,
			 @WebParam(name = "token") String token){
		Gson gson = new Gson();
		Map<Long, String> map = null;
		
		try {
			map = wsCotizacionService.getListPaquetes(idToNegSeccion,token);
		} catch(SystemException e){
			return "No fue posible obtener los paquetes. "+e.getMessage();
		}
		
		return gson.toJson(map);
	}
	
	/**
	 * Obtiene lista de formas de pago de un tipo de póliza dado
	 * 
	 * @param idToNegTipoPoliza
	 * @param token
	 * @return lista de formas de pago de un tipo de póliza dado
	 */
	@WebMethod(operationName = "getListFormasDePago")
	@WebResult(targetNamespace = "")
	public String getListFormasDePago(@WebParam(name = "idTipoPoliza") BigDecimal idToNegTipoPoliza,
			 @WebParam(name = "token") String token){
		Gson gson = new Gson();
		Map<Integer, String> map = null;
		
		try {
			map = wsCotizacionService.getListFormasPago(idToNegTipoPoliza,token);
		} catch(SystemException e){
			return "No fue posible obtener las formas de pago. "+e.getMessage();
		}
		
		return gson.toJson(map);
	}
	
	/**
	 * Obtiene la lista de medios de pago
	 * @param token
	 * @return lista de medios de pago
	 */
	@WebMethod(operationName = "getListMediosDePago")
	@WebResult(targetNamespace = "")
	public String getListMediosDePago(@WebParam(name = "token") String token){
		Gson gson = new Gson();
		Map<Integer, String> map = null;
		
		try {
			map = wsCotizacionService.getListMediosPago(token);
		} catch(SystemException e){
			return "No fue posible obtener los medios de pago. "+e.getMessage();
		}
		
		return gson.toJson(map);
	}
	
	/**
	 * Obtiene la lista de bancos
	 * @param token
	 * @return lista de bancos
	 */
	@WebMethod(operationName = "getListBancos")
	@WebResult(targetNamespace = "")
	public String getListBancos(@WebParam(name = "token") String token){
		Gson gson = new Gson();
		Map<Integer, String> map = null;
		
		try {
			map = wsCotizacionService.getListBancos(token);
		} catch(SystemException e){
			return "No fue posible obtener los bancos. "+e.getMessage();
		}
		
		return gson.toJson(map);
	}
	
	/**
	 * Obtiene la lista de tipos de tarjeta
	 * @param token
	 * @return lista de tipos de tarjeta
	 */
	@WebMethod(operationName = "getListTiposTarjeta")
	@WebResult(targetNamespace = "")
	public String getListTiposTarjeta(@WebParam(name = "token") String token){
		Gson gson = new Gson();
		Map<String, String> map = null;
		
		try {
			map = wsCotizacionService.getListTiposTarjeta(token);
		} catch(SystemException e){
			return "No fue posible obtener los tipos de tarjeta. "+e.getMessage();
		}
		
		return gson.toJson(map);
	}
	 
	/**
	 * Obtiene la lista de modelos para una línea de negocio y una marca
	 * 
	 * @param idToNegSeccion
	 * @param idMarca
	 * @param token
	 * @return lista de modelos para una línea de negocio y una marca
	 */
	@WebMethod(operationName = "getListModelos")
	@WebResult(targetNamespace = "")
	public String getListModelos(@WebParam(name = "idLineaNegocio") BigDecimal idToNegSeccion, @WebParam(name = "idMarca") BigDecimal idMarca,
			 @WebParam(name = "token") String token){
		Gson gson = new Gson();
		Map<Short, Short> map = null;
		
		try {
			map = wsCotizacionService.getListModelos(idToNegSeccion, new BigDecimal(484), idMarca,token);
		} catch(SystemException e){
			return "No fue posible obtener los modelos. "+e.getMessage();
		}
		
		return gson.toJson(map);
	}
	
	/**
	 * Obtiene la lista de productos de un negocio
	 * 
	 * @param idNegocio
	 * @param token
	 * @return lista de productos para un negocio
	 */
	@WebMethod(operationName = "getListProductos")
	@WebResult(targetNamespace = "")
	public String getListProductos(@WebParam(name = "idNegocio") Long idNegocio,
			 @WebParam(name = "token") String token){
		Gson gson = new Gson();
		Map<Long, String> map = null;
		
		try {
			map = wsCotizacionService.getListNegocioProductos(idNegocio, ID_PRODUCTO_AUTO_INDIVIDUAL, token);
			try {
				map.putAll(wsCotizacionService.getListNegocioProductos(idNegocio, ID_PRODUCTO_AUTO_FLOTILLA, token));
			} catch (Exception e) {
				LOG.error("getListProductos()-- flotilla", e);
			}
		} catch(SystemException e){
			return "No fue posible obtener los productos. "+e.getMessage();
		}
		
		return gson.toJson(map);
	}
	
	/**
	 * Obtiene la lista de tipos de póliza de una línea de negocio
	 * 
	 * @param idToNegProducto
	 * @param token
	 * @return lista de tipos de póliza de una línea de negocio
	 */
	@WebMethod(operationName = "getListTiposPoliza")
	@WebResult(targetNamespace = "")
	public String getListTiposPoliza(@WebParam(name = "idToNegProducto") Long idToNegProducto,
			 @WebParam(name = "token") String token){
		Gson gson = new Gson();
		Map<BigDecimal, String> map = null;
		
		try {
			map = wsCotizacionService.getListTiposPolizaByNegProducto(idToNegProducto,token);
		} catch(SystemException e){
			return "No fue posible obtener los tiposPoliza. "+e.getMessage();
		}
		
		return gson.toJson(map);
	}
	
	/**
	 * Obtiene lista de líneas de negocio
	 * @param idToNegTipoPoliza
	 * @param token
	 * @return lista de líneas de negocio
	 */
	@WebMethod(operationName = "getListLineasNegocio")
	@WebResult(targetNamespace = "")
	public String getListLineasNegocio(@WebParam(name = "idToNegTipoPoliza") BigDecimal idToNegTipoPoliza,
			 @WebParam(name = "token") String token){
		Gson gson = new Gson();
		Map<BigDecimal, String> map = null;
		
		try {
			map = wsCotizacionService.getListLineasNegocio(idToNegTipoPoliza, token);
		} catch(SystemException e){
			return "No fue posible obtener las lineas de negocio. "+e.getMessage();
		}
		
		return gson.toJson(map);
	}
	
	/**
	 * Obtiene lista de tipos de uso de una línea de negocio dado
	 * 
	 * @param idToNegSeccion
	 * @param token
	 * @return lista de tipos de uso de la línea de negocio dado
	 */
	@WebMethod(operationName = "getListTiposUso")
	@WebResult(targetNamespace = "")
	public String getListTiposUso(@WebParam(name = "idLineaNegocio") BigDecimal idToNegSeccion,
			 @WebParam(name = "token") String token){
		Gson gson = new Gson();
		Map<BigDecimal, String> map = null;
		
		try {
			map = wsCotizacionService.getListTiposUso(idToNegSeccion,token);
		} catch(SystemException e){
			return "No fue posible obtener los tipos de uso. "+e.getMessage();
		}
		
		return gson.toJson(map);
	}
	
	/**
	 * Servicio para cotizar un endoso de nuevo conducto de cobro
	 * 
	 * @param json
	 * 		Cadena en formato JSON con los par\u00E1metros de entrada para la cotizaci\u00F3n, ejemplo: 
	 * 	          
	 * 			<p>
	 *            <code>
	 *	{ <br>
	 *		"numeroPoliza": "3101-861489-00", <br>
	 *		"claveTipoEndoso": "27", <br>
	 *		"fechaInicioVigenciaEndoso": "30/08/2016", <br>
	 *		"datosEndoso": { <br>
	 *			"conductoCobro": { <br>
	 *				"idMedioPago": "4", <br>
	 *				"datosTarjeta": { <br>
	 *					"idBanco": "51", <br>
	 *					"numeroTarjeta": "5120488000059437", <br>
	 *					"fechaVencimientoTarjeta": "0818", <br>
	 *					"codigoSeguridadTarjeta": "612", <br>
	 *					"idTipoTarjeta": "MC" <br>
	 *				}, <br>
	 *				"datosTitular": { <br>
	 *					"tarjetaHabiente": "Raul Montemayor Peralta", <br>
	 *					"correo": "0jose.ake@afirme.com", <br>
	 *					"telefono": "8118564045", <br>
	 *					"clavePais": "PAMEXI", <br>
	 *					"idEstado": "19000", <br>
	 *					"idMunicipio": "1903", <br>
	 *					"idColonia": "67515-6", <br>
	 *					"nombreColonia": "Los Nogales", <br>
	 *					"calleNumero": "217", <br>
	 *					"codigoPostal": "67515", <br>
	 *					"rfc": "aeue790224iz0" <br>
	 *				} <br>
	 *			} <br>
	 *		} <br>
	 *	} <br>
	 *            </code>
	 *            <p>
	 * @param token
	 * 		cadena con el token de sesi\u00F3n
	 * 			<p>
	 * @return cadena en formato JSON con el id de la cotizaci\u00F3n, ejemplo:
	 * 
	 *		<p>
	 *		<code>
	 *			{ <br>
	 *				"idCotizacion":3280997, <br>
	 *				"idConductoCobro":467 <br>
	 *			} <br>
	 *		</code>
	 *		</p>
	 */
	@WebMethod(operationName = "cotizarEndoso")
	@WebResult(targetNamespace = "")
	public String cotizarEndoso(@WebParam(name = "json") String json,
			@WebParam(name = "token") String token){
		Gson gson = new Gson();
		EndosoTransporteDTO dto = gson.fromJson(json, EndosoTransporteDTO.class);
		Map<String, Object> result = new HashMap<String, Object>();
		dto.setToken(token);
		
		try {
			result = wsCotizacionService.cotizarEndoso(dto);
		} catch (ApplicationException e) {
			return e.getMessage();
		} catch (SystemException e) {
			return e.getMessage();
		}catch (Exception e) {
			return "Error al cotizar el endoso... " + e.getMessage();
		}
		
		return gson.toJson(result);
	}

	/**
	 * Servicio para emitir un endoso de nuevo conducto de cobro
	 * 
	 * @param json
	 * 		Cadena en formato JSON con los par\u00E1metros de entrada para la emisi\u00F3n, ejemplo: 
	 * 	          
	 * 			<p>
	 *            <code>
	 *				{ <br>
	 *					"idCotizacion": "3280997" <br>
	 *				} <br>
	 *            </code>
	 *            <p>
	 * @param token
	 * 		cadena con el token de sesi\u00F3n
	 * 			<p>
	 * @return cadena en formato JSON con los datos de respuesta de la emisi\u00F3n, ejemplo:
	 * 
	 *		<p>
	 *		<code>
	 *		{ <br>
	 *			"idMedioPago": 8, <br>
	 *			"iva": 0.0, <br>
	 *			"descuento": 0.0, <br>
	 *			"idToPoliza": 1332633, <br>
	 *			"primaTotal": 0.0, <br>
	 *			"recargo": 0.0, <br>
	 *			"numeroEndoso": 6, <br>
	 *			"derechos": 0.0, <br>
	 *			"primaNeta": 0.0 <br>
	 *		} <br>
	 *		</code>
	 *		</p>
	 */
	@WebMethod(operationName = "emitirEndoso")
	@WebResult(targetNamespace = "")
	public String emitirEndoso(@WebParam(name = "json") String json,
			@WebParam(name = "token") String token){
		Gson gson = new Gson();
		EndosoTransporteDTO dto = gson.fromJson(json, EndosoTransporteDTO.class);
		Map<String, Object> result = new HashMap<String, Object>();
		dto.setToken(token);
		
		try {
			result = wsCotizacionService.emitirEndoso(dto);
		} catch (SystemException e) {
			return "Error al emitir el endoso... "+e.getMessage();
		}
		
		return gson.toJson(result);
	}
	
	/**
	 * Servicio para imprimir un endoso de nuevo conducto de cobro
	 * 
	 * @param json
	 * 		Cadena en formato JSON con los par\u00E1metros de entrada para la impresi\u00F3n, ejemplo: 
	 * 	          
	 * 			<p>
	 *            <code>
	 *				{ <br>
	 *					"numeroPoliza": "3101-01000145-00", <br>
	 *					"numeroEndoso": "1" <br>
	 *				} <br>
	 *            </code>
	 *            <p>
	 * @param token
	 * 		cadena con el token de sesi\u00F3n
	 * 			<p>
	 * @return cadena con el arreglo de bytes para la impresi\u00F3n
	 * 
	 */
	@WebMethod(operationName = "imprimirEndoso")
	@WebResult(targetNamespace = "")
	public byte[] imprimirEndoso(@WebParam(name = "json") String json,
			@WebParam(name = "token") String token){
		Gson gson = new Gson();
		EndosoTransporteDTO dto = gson.fromJson(json, EndosoTransporteDTO.class);
		dto.setToken(token);
		
		try {
			return wsCotizacionService.imprimirEndoso(dto);
		} catch (Exception e) {
			return e.getMessage().getBytes();
		}
	}
	
	/**
	 * Servicio para obtener la lista de endosos de una p\u00F3liza.
	 * 
	 * @param json
	 * 		Cadena en formato JSON con los par\u00E1metros de b\u00FAsqueda, ejemplo: 
	 * 	          
	 * 			<p>
	 *            <code>
	 *				{ <br>
	 *					"numeroPoliza": "3101-01000145-00" <br>
	 *				} <br>
	 *            </code>
	 *            <p>
	 * @param token
	 * 		cadena con el token de sesi\u00F3n
	 * 			<p>
	 * @return cadena en formato JSON con la lista de endosos, ejemplo:
	 * 
	 *		<p>
	 *		<code>
	 *			[{ <br>
	 *				"numeroCotizacionEndoso": 3281983, <br>
	 *				"idPoliza": 1332900, <br>
	 *				"numeroPolizaFormateado": "3101-01000145-00", <br>
	 *				"idTipoEndoso": 9, <br>
	 *				"fechaCotizacionEndoso": "Sep 14, 2016 12:00:00 AM", <br>
	 *				"estatusCotizacionEndoso": 16, <br>
	 *				"fechaEmisionEndoso": "Sep 29, 2016 10:13:55 AM", <br>
	 *				"fechaEmisionEndosoMidas": "Sep 29, 2016 10:13:55 AM", <br>
	 *				"primaEndoso": -77.2475552, <br>
	 *				"fechaInicioVigencia": "Sep 14, 2016 12:00:00 AM", <br>
	 *				"numeroEndoso": "2", <br>
	 *				"claveTipoEndoso": 6, <br>
	 *				"codigoUsuarioCreacion": "M2ADMINI" <br>
	 *			}, { <br>
	 *				"numeroCotizacionEndoso": 3281983, <br>
	 *				"idPoliza": 1332900, <br>
	 *				"numeroPolizaFormateado": "3101-01000145-00", <br>
	 *				"idTipoEndoso": 9, <br>
	 *				"fechaCotizacionEndoso": "Sep 14, 2016 12:00:00 AM", <br>
	 *				"estatusCotizacionEndoso": 16, <br>
	 *				"fechaEmisionEndoso": "Sep 14, 2016 2:06:07 PM", <br>
	 *				"fechaEmisionEndosoMidas": "Sep 14, 2016 2:06:07 PM", <br>
	 *				"primaEndoso": 176.34642712, <br>
	 *				"fechaInicioVigencia": "Sep 14, 2016 12:00:00 AM", <br>
	 *				"numeroEndoso": "1", <br>
	 *				"claveTipoEndoso": 6, <br>
	 *				"codigoUsuarioCreacion": "M2ADMINI" <br>
	 *			}] <br>
	 *		</code>
	 *		</p>
	 */
	@WebMethod(operationName = "getListEndososPoliza")
	@WebResult(targetNamespace = "")
	public String getListEndososPoliza(@WebParam(name = "json") String json,
			@WebParam(name = "token") String token){
		Gson gson = new Gson();
		List<CotizacionEndosoDTO> listaCotizacionesEndoso = new ArrayList<CotizacionEndosoDTO>();
		
		EndosoTransporteDTO dto = gson.fromJson(json, EndosoTransporteDTO.class);
		dto.setToken(token);
		
		try {
			listaCotizacionesEndoso = wsCotizacionService.buscarCotizacionesEndosoPoliza(dto);
		} catch (SystemException e) {
			return "Error al obtener los endosos de la póliza.\n"+e.getMessage();
		}
		
		return gson.toJson(listaCotizacionesEndoso);
	}
	
	/**
	 * Servicio para obtener la lista de colonias de un c\u00F3digo postal dado
	 * 
	 * @param cp
	 * 		c\u00F3digo postal, ejemplo: 67515 <br>
	 * 		<p>
	 * @param token
	 * 		cadena con el token de sesi\u00F3n
	 * 			<p>
	 * @return cadena en formato JSON con la lista de las colonias, ejemplo:
	 * 
	 *		<p>
	 *		<code>
	 *		{ <br>
	 *			"67515-13": "1", <br>
	 *			"67515-12": "190001-6", <br>
	 *			"67515-11": "67515-6", <br>
	 *			"67515-8": "BARRIO MATAMOROS", <br>
	 *			"67515-2": "DEL BOSQUE", <br>
	 *			"67515-4": "EL FRESNO", <br>
	 *			"67515-9": "FRACC REAL DEL VALLE", <br>
	 *			"67515-14": "LA LUZ", <br>
	 *			"67515-6": "LOS NOGALES", <br>
	 *			"67515-1": "LOS SABINOS", <br>
	 *			"67515-5": "LOS SABINOS MONTEMORELOS", <br>
	 *			"67515-3": "MARANATHA", <br>
	 *			"67515-10": "NOGALES", <br>
	 *			"67515-7": "REAL DEL VALLE" <br>
	 *		} <br>
	 *		</code>
	 *		</p>
	 */	
	@WebMethod(operationName = "getListColonias")
	@WebResult(targetNamespace = "")
	public String getListColonias(@WebParam(name = "cp") String cp, @WebParam(name = "token") String token){
		Gson gson = new Gson();
		Map<String, String> map = new HashMap<String, String>(1);
		
		try {
			map = wsCotizacionService.getListColoniasByCP(cp, token);
		} catch (SystemException e) {
			return "Error al obtener las colonias.\n"+e.getMessage();
		}
		
		return gson.toJson(map);
	}
	
	/**
	 * Regresa un listado de referencias bancarias de la 
	 * poliza consultada, la lista es de tipo ReferenciaBancariaDTO.
	 * 
	 * @param json
	 *            Cadena en formato JSON con el siguiente formato:
	 *            <p>
	 *            <code>
	 * 			{<br>
	 * 				numeroPoliza: "310186010800", // Opcional si se manda in idPoliza<br>
	 * 				idPoliza: 10598, // Opcional si se manda un numeroPoliza <br>
	 * 			}<br>
	 * 			</code>
	 * @param token
	 *            Cadena con el token de sesion
	 * 
	 * 
	 * @return
	 * 
	 * Ejemplo de respuesta:	  
[

   {

      "fechaVencimiento": "18/05/2016 12:00:00 AM",

      "poliza": 3101860716,

      "recibo": 5836073,

      "estatus": "EMI",

      "referenciaBancaria": "",

      "descripcionCobro": "total",

      "monto": 514.12,

      "afirmeTitulo": "",

      "afirmeCuenta": "CUENTA: 159100138",

      "afirmeReferencia": "58360731318316",

      "banorteTitulo": "BANORTE",

      "banorteCuenta": "EMPRESA: 60990",

      "banorteReferencia": "58360731318316",

      "santanderTitulo": "SANTANDER",

      "santanderCuenta": "CUENTA: 65502848358",

      "santanderReferencia": "583607313183239",

      "bancomerTitulo": "BBVA BANCOMER RUP",

      "bancomerCuenta": "BBVA BANCOMER RUP",

      "bancomerReferencia": "750009120062806000514120583607322156",

      "oxxoTitulo": "OXXO",

      "oxxoCuenta": "No Aplica",

      "oxxoReferencia": "36058360731707201600000514128",

      "fechaDesde": "18/05/2016 12:00:00 AM",

      "fechaHasta": "30/06/2016 12:00:00 AM"

   }
]	  
	  
	 */
	@WebMethod(operationName = "getReferenciasBancarias")
	@WebResult(targetNamespace = "")	
	public String getReferenciasBancarias( 
			@WebParam(name = "json") String json,
			@WebParam(name = "token") String token
			){
				
		List<ReferenciaBancariaDTO> resultList = null;
		Gson gson = new Gson();
		
		try {
			usuarioService.validateToken(token);
			ParametrosImpresionPolizaView params = gson.fromJson(json, ParametrosImpresionPolizaView.class);
			BigDecimal idPoliza = wsCotizacionService.obtenerIdPoliza(params);
			resultList = listadoIncisosDinamicoService.getReferenciasBancarias(idPoliza);
		} catch (Exception e) {
			return "Error al obtener las referencias bancarias.\n"+e.getMessage();
		}
		return gson.toJson(resultList);
		
	}
	
	/**
	 * Devuelve un listado de los motivos de endoso
	 * para determinada p\u00f3liza en funci\u00f3n del tipo
	 * de endoso.
	 * 
	 * @param json
	 *            Cadena en formato JSON con el siguiente formato:
	 *            <p>
	 *            <code>
	 * 			{<br>
	 * 				numeroPoliza: "310186010800", // Opcional si se manda un idPoliza<br>
	 * 				idPoliza: 10598, // Opcional si se manda un numeroPoliza <br>
	 * 				tipoEndoso: 11<br> 
	 * 			}<br>
	 * 			</code>
	 * @param token
	 * @return
	 * 
	 * Ejemplo de respuesta:
	 * 
	 * {"16":"SE FUE A VENTA DIRECTA",
	 * "17":"ALTA SINIESTRALIDAD",
	 * "18":"INTERES PERSONAL DEL CLIENTE",
	 * "19":"CAMBIO DE POLIZA",
	 * "20":"DUPLICIDAD",
	 * "22":"PAGO DE DAÑOS POR SUSTITUCION DE PERDIDA TOTAL",
	 * "8":"POR COMPETENCIA",
	 * "9":"VENTA DE UNIDAD",
	 * "12":"MAL SERVICIO SINIESTRO",
	 * "13":"MAL SERVICIO COBRANZA",
	 * "14":"MAL SERVICIO VENTAS",
	 * "15":"CAMBIO DE AGENTE"}
	 * 
	 */
	@WebMethod(operationName = "getListMotivosEndoso")
	@WebResult(targetNamespace = "")	
	public String getListMotivosEndoso( 
			@WebParam(name = "json") String json,
			@WebParam(name = "token") String token){
		
		Map<Integer, String> mapList = null;
		Gson gson = new Gson();
		
		try {
			MotivosEndosoView params = gson.fromJson(json, MotivosEndosoView.class);
			mapList = wsCotizacionService.getListMotivoEndoso(params, token);
		} catch (SystemException e) {
			return "Error al obtener los motivos de endoso.\n"+e.getMessage();
		}
		return gson.toJson(mapList);
	}
	
	/**
	 * Endoso de Cancelaci\u00f3n de p\u00f3liza
	 * 
	 * @param json 
	 * {
        		"numeroPoliza": "3101-860891-00",
        		"fechaInicioVigenciaEndoso": "14/06/2016",
        		"motivoEndoso": "9"
        }
	 * 
	 * @param token
	 * @return 
	 * Devuelve un String en formato json con los valores
	 * del endoso
	 * 
	 * ejemplo:
	 * 
	 * {
		"fechaInicioVigencia": "2/06/2016 12:00:00 AM",
		"valorDerechos": -250.0,
		"valorComision": -186.20999999839768,
		"valorComisionRPF": 0.0,
		"idCotizacion": 3274303,
		"valorRecargoPagoFrac": 0.0,
		"fechaFinVigencia": "2/06/2017 12:00:00 AM",
		"fechaCreacion": "13/06/2016 11:46:32 AM",
		"tipoCambio": 1.0,
		"valorPrimaTotal": -2449.999999991209,
		"valorIVA": -337.9300000042088,
		"descripcionTipoEndoso": "Cancelaci\u00f3n",
		"valorBonifComisionRPF": 0.0,
		"valorBonifComision": 0.0,
		"numeroEndoso": 5,
		"porcentajeBonifComision": 0.0,
		"valorPrimaNeta": -1862.069999987
	   }
	 * 
	 *
	 */
	@WebMethod(operationName = "cancelarPoliza")
	@WebResult(targetNamespace = "")
	public String cancelar(@WebParam(name = "json") String json,
			@WebParam(name = "token") String token){
		Gson gson = new Gson();
		EndosoTransporteDTO dto = gson.fromJson(json, EndosoTransporteDTO.class);
		Map<String, Object> result = new HashMap<String, Object>();
		dto.setToken(token);
		
		try {
			result = wsCotizacionService.cancelarPoliza(dto);
		} catch (SystemException e) {
			return "Error en endoso de cancelacion de poliza... "+e.getMessage();
		}
		
		return gson.toJson(result);
	}

	/**
	 * Servicio para obtener la lista de endosos que tienen recibos de un n\u00FAmero de p\u00F3liza dado.
	 * 
	 * @param json
	 * 		Cadena en formato JSON con los valores de filtros disponibles para la b\u00FAsqueda, ejemplo: 
	 * 	          
	 * 			<p>
	 *            <code>
	 *            {<br>
	 *            	"numeroPoliza" : "3101-860227-00"<br>
	 *            }<br>
	 *            </code>
	 *            <p>
	 * @param token
	 * 		cadena con el token de sesi\u00F3n
	 * 			<p>
	 * @return cadena en formato JSON con la lista de endosos disponibles, ejemplo:
	 * 
	 *		<p>
	 *		<code>
	 *			{<br>
	 *				"0": "0 - ALTA DE POLIZA" <br>
	 *			} <br>
	 *
	 *		</code>
	 *		</p>
	 */
	@WebMethod(operationName = "getListEndososConRecibosPoliza")
	@WebResult(targetNamespace = "")
	public String getListEndososConRecibosPoliza(@WebParam(name = "json") String json,
			@WebParam(name = "token") String token){
		Gson gson = new Gson();
		Map<BigDecimal, String> map = new LinkedHashMap<BigDecimal, String>();
		
		RecibosEndosoTransporteDTO dto = gson.fromJson(json, RecibosEndosoTransporteDTO.class);
		dto.setToken(token);
		
		try {
			map = wsCotizacionService.getListEndososConRecibosPoliza(dto);
		} catch (SystemException e) {
			return "Error al obtener los endosos de la p\u00F3liza.\n"+e.getMessage();
		}
		
		return gson.toJson(map);
	}

	/**
	 * Servicio para obtener la lista de programas de un n\u00FAmero de endoso y p\u00F3liza dados.
	 * 
	 * @param json
	 * 		Cadena en formato JSON con los valores de filtros disponibles para la b\u00FAsqueda, ejemplo: 
	 * 	          
	 * 			<p>
	 *            <code>
	 *            {<br>
	 *            	"numeroPoliza" : "3101-860227-00", <br>
	 *            	"numeroEndoso": "0"<br>
	 *            }<br>
	 *            </code>
	 *            <p>
	 * @param token
	 * 		cadena con el token de sesi\u00F3n
	 * 			<p>
	 * @return cadena en formato JSON con la lista de programas disponibles, ejemplo:
	 * 
	 *		<p>
	 *		<code>
	 *			{<br>
	 *				"1":"1" <br>
	 *			} <br>
	 *
	 *		</code>
	 *		</p>
	 */
	@WebMethod(operationName = "getListProgramasPagosEndosoPoliza")
	@WebResult(targetNamespace = "")
	public String getListProgramasPagosEndosoPoliza(@WebParam(name = "json") String json,
			@WebParam(name = "token") String token){
		Gson gson = new Gson();
		//List<CotizacionEndosoDTO> listaCotizacionesEndoso = new ArrayList<CotizacionEndosoDTO>();
		Map<BigDecimal, String> map = new LinkedHashMap<BigDecimal, String>();
		
		RecibosEndosoTransporteDTO dto = gson.fromJson(json, RecibosEndosoTransporteDTO.class);
		dto.setToken(token);
		
		try {
			map = wsCotizacionService.getListProgramasPagosEndosoPoliza(dto);
		} catch (SystemException e) {
			return "Error al obtener la lista de programas.\n"+e.getMessage();
		}
		
		return gson.toJson(map);
	}
	
	/**
	 * Servicio para obtener la lista de incisos de un n\u00FAmero de endoso, n\u00FAmero de p\u00F3liza y un id de programa dados.
	 * 
	 * @param json
	 * 		Cadena en formato JSON con los valores de filtros disponibles para la b\u00FAsqueda, ejemplo: 
	 * 	          
	 * 			<p>
	 *            <code>
	 *            {<br>
	 *            	"numeroPoliza" : "3101-860227-00", <br>
	 *            	"numeroEndoso": "0", <br>
	 *            	"idProgramaPago": "1" <br>
	 *            }<br>
	 *            </code>
	 *            <p>
	 * @param token
	 * 		cadena con el token de sesi\u00F3n
	 * 			<p>
	 * @return cadena en formato JSON con la lista de incisos disponibles, ejemplo:
	 * 
	 *		<p>
	 *		<code>
	 *			{<br>
	 *				"inciso":"1" <br>
	 *			} <br>
	 *
	 *		</code>
	 *		</p>
	 */	
	@WebMethod(operationName = "getIncisosProgPago")
	@WebResult(targetNamespace = "")
	public String getIncisosProgPago(@WebParam(name = "json") String json,
			@WebParam(name = "token") String token){
		Gson gson = new Gson();
		Map<String, String> map = new LinkedHashMap<String, String>();
		
		RecibosEndosoTransporteDTO dto = gson.fromJson(json, RecibosEndosoTransporteDTO.class);
		dto.setToken(token);
		String inciso = "";
		
		try {
			inciso = wsCotizacionService.getIncisosProgPago(dto);
			map.put("inciso", inciso);			
		} catch (SystemException e) {
			return "Error al obtener los incisos.\n"+e.getMessage();
		}
		
		return gson.toJson(map);
	}
	
	/**
	 * Servicio para obtener la lista de recibos de una p\u00F3liza.
	 * 
	 * @param json
	 * 		Cadena en formato JSON con los valores de filtros disponibles para la b\u00FAsqueda, ejemplo: 
	 * 	          
	 * 			<p>
	 *            <code>
	 *            {<br>
	 *            	"numeroPoliza" : "3101-01000145-00", <br>
	 *            	"numeroEndoso": "0", <br>
	 *            	"idProgramaPago": "1", <br>
	 *	            "idInciso": "1" <br>
	 *            }<br>
	 *            </code>
	 *            <p>
	 * @param token
	 * 		cadena con el token de sesi\u00F3n
	 * 			<p>
	 * @return cadena en formato JSON con la lista de recibos, ejemplo:
	 * 
	 *		<p>
	 *		<code>
	 *	[{ <br>
	 *		"id": { <br>
	 *			"idRecibo": 44888801 <br>
	 *		}, <br>
	 *		"numeroFolio": 1699142, <br>
	 *		"fechaInicioVigencia": "Sep 14, 2016 12:00:00 AM", <br>
	 *		"fechaFinVigencia": "Mar 14, 2017 12:00:00 AM", <br>
	 *		"cveTipoRecibo": "REC", <br>
	 *		"sitRecibo": "CAN", <br>
	 *		"impPrimaTotal": 3178.63 <br>
	 *	}, { <br>
	 *		"id": { <br>
	 *			"idRecibo": 44888802 <br>
	 *		}, <br>
	 *		"fechaInicioVigencia": "Mar 14, 2017 12:00:00 AM", <br>
	 *		"fechaFinVigencia": "Sep 14, 2017 12:00:00 AM", <br>
	 *		"cveTipoRecibo": "REC", <br>
	 *		"sitRecibo": "CAN", <br>
	 *		"impPrimaTotal": 2656.63 <br>
	 *	}, { <br>
	 *		"id": { <br>
	 *			"idRecibo": 44888813 <br>
	 *		}, <br>
	 *		"fechaInicioVigencia": "Jul 14, 2017 12:00:00 AM", <br>
	 *		"fechaFinVigencia": "Aug 14, 2017 12:00:00 AM", <br>
	 *		"cveTipoRecibo": "REC", <br>
	 *		"sitRecibo": "EMI", <br>
	 *		"impPrimaTotal": 457.47 <br>
	 *	}, { <br>
	 *		"id": { <br>
	 *			"idRecibo": 44888814 <br>
	 *		}, <br>
	 *		"fechaInicioVigencia": "Aug 14, 2017 12:00:00 AM", <br>
	 *		"fechaFinVigencia": "Sep 14, 2017 12:00:00 AM", <br>
	 *		"cveTipoRecibo": "REC", <br>
	 *		"sitRecibo": "EMI", <br>
	 *		"impPrimaTotal": 457.47 <br>
	 *	}] <br>
	 *		</code>
	 *		</p>
	 */	
	@WebMethod(operationName = "getRecibosPoliza")
	@WebResult(targetNamespace = "")
	public String getRecibosPoliza(@WebParam(name = "json") String json,
			@WebParam(name = "token") String token){
		Gson gson = new Gson();
		List<ReciboSeycos> reciboList = new ArrayList<ReciboSeycos>(1);
		
		RecibosEndosoTransporteDTO dto = gson.fromJson(json, RecibosEndosoTransporteDTO.class);
		dto.setToken(token);
		
		try {
			reciboList = wsCotizacionService.getRecibosPoliza(dto);
		} catch (SystemException e) {
			return "Error al obtener los recibos.\n"+e.getMessage();
		}
		
		return gson.toJson(reciboList);
	}
	
	/**
	 * Servicio para obtener el arreglo de bytes para la impresi\u00F3n de un recibo dado.
	 * 
	 * @param json
	 * 		Cadena en formato JSON con los valores de filtros disponibles para la b\u00FAsqueda, ejemplo: 
	 * 	          
	 * 			<p>
	 *            <code>
	 *            {<br>
	 *            	"numeroPoliza" : "3101-860227-00", <br>
	 *            	"idRecibo": "44778110" <br>
	 *            }<br>
	 *            </code>
	 *            <p>
	 * @param token
	 * 		cadena con el token de sesi\u00F3n
	 * 			<p>
	 * @return cadena con el arreglo de bytes para la impresi\u00F3n del recibo:
	 * 
	 */	
	@WebMethod(operationName = "imprimirRecibo")
	@WebResult(targetNamespace = "")
	public byte[] imprimirRecibo(@WebParam(name = "json") String json,
			@WebParam(name = "token") String token){
		Gson gson = new Gson();
		RecibosEndosoTransporteDTO dto = gson.fromJson(json, RecibosEndosoTransporteDTO.class);
		dto.setToken(token);
		
		try {
			return wsCotizacionService.imprimirRecibo(dto);
		} catch (Exception e) {
			return e.getMessage().getBytes();
		}
	}
	
	/**
	 * Servicio para obtener los movimientos de un recibo dado.
	 * 
	 * @param json
	 * 		Cadena en formato JSON con los valores de filtros disponibles para la b\u00FAsqueda, ejemplo: 
	 * 	          
	 * 			<p>
	 *            <code>
	 *            {<br>
	 *            	"numeroPoliza" : "3101-01000197-00", <br>
	 *            	"idRecibo": "44889704", <br>
	 *            	"idInciso": "1" <br>
	 *            }<br>
	 *            </code>
	 *            <p>
	 * @param token
	 * 		cadena con el token de sesi\u00F3n
	 * 			<p>
	 * @return cadena en formato JSON con la lista de movimientos del recibo, ejemplo:
	 * 
	 *		<p>
	 *		<code>
	 *	[{ <br>
	 *		"idRecibo": 44889704, <br>
	 *		"verRecibo": 1, <br>
	 *		"folioRecibo": 5927335, <br>
	 *		"sitRecibo": "EMI", <br>
	 *		"cveTDoctoVerr": "P", <br>
	 *		"idDoctovers": 9840063, <br>
	 *		"cveMovtoEnd": "POLIZA", <br>
	 *		"numEndoso": 0 <br>
	 *	}] <br>
	 *
	 *		</code>
	 *		</p>
	 */
	@WebMethod(operationName = "getMovimientosRecibosPoliza")
	@WebResult(targetNamespace = "")
	public String getMovimientosRecibosPoliza(@WebParam(name = "json") String json,
			@WebParam(name = "token") String token){
		Gson gson = new Gson();
		List<MovimientoReciboDTO> list = new ArrayList<MovimientoReciboDTO>(1);
		
		RecibosEndosoTransporteDTO dto = gson.fromJson(json, RecibosEndosoTransporteDTO.class);
		dto.setToken(token);
		
		try {
			list = wsCotizacionService.getMovimientosRecibosPoliza(dto);
		} catch (SystemException e) {
			return "Error al obtener los movimientos.\n"+e.getMessage();
		}
		
		return gson.toJson(list);
	}
	
	/**
	 * Busca las coberturas en base a los parametros enviados<br>
	 * El resultado contiene el listado de coberturas que se deben enviar
	 * -en caso que se desee incluirlas- al invocar el metodo <b>cotizar</b><br> 
	 *  Los valores del listado obtenido se pueden modificar siempre y cuando se respeten
	 *  los limites, comentados abajo, por ejemplo el campo <b>obligatoriedad</b>
	 *  solo puede tener un valor de entre: 0,1,2,3<br>
	 *  <b>contratada</b> solo puede ser true o false<br><br>
	 * 
	 * @param json
	 *		Cadena en formato JSON con los parametros para buscar las coberturas, ejemplo:
	 *		<p>
	 *		<code>
	 *			{<br>
	 *			"IdNegocio": 7,<br>
	 *			"idProducto": 191,<br>
	 *			"idTipoPoliza": 1301,<br>
	 *			"idLineaNegocio": 755,<br>
	 *			"idPaquete": 1,<br>
	 *			"idEstadoCirculacion": "01000",<br>
	 *			"idMunicipioCirculacion": "01001",<br>
	 *			"estilo": "88822",<br>
	 *			"modelo": 2014<br>
	 *			}<br>
	 *		</code>
	 * @param token
	 *	 	Cadena con el token de sesion
	 * @return Cadena en formato JSON con la lista de coberturas, ejemplo:
	 *		<p>
	 *		<code>
	 *			[{<br>
	 *				"idCobertura": 2710,<br>
	 *				"obligatoriedad": 0,<br>
	 *				"contratada": true,<br>
	 *				"descripcion": "DAÃOS MATERIALES",<br>
	 *				"sumaAsegurada": "Valor Comercial",<br>
	 *				"sumaAseguradaMin": "N/A",<br>
	 *				"sumaAseguradaMax": "N/A",<br>
	 *				"claveTipoDeducible": "1",<br>
	 *				"deducible": 5.0,<br>
	 *				"valoresDeducible": {<br>
	 *					"72640": "2.0",<br>
	 *					"72641": "3.0",<br>
	 *					"72642": "4.0",<br>
	 *					"72643": "5.0",<br>
	 *					"72644": "6.0",<br>
	 *					"72645": "7.0",<br>
	 *					"72646": "8.0",<br>
	 *					"72647": "9.0",<br>
	 *					"72648": "10.0",<br>
	 *					"72649": "15.0",<br>
	 *					"72650": "20.0",<br>
	 *					"72651": "25.0"<br>
	 *				}<br>
	 *			}, {<br>
	 *				"idCobertura": 4815,<br>
	 *				"obligatoriedad": 0,<br>
	 *				"contratada": true,<br>
	 *				"descripcion": "RESPONSABILIDAD CIVIL EN EXCESO POR MUERTE",<br>
	 *				"sumaAsegurada": "2000000.0",<br>
	 *				"sumaAseguradaMin": "2000000.0",<br>
	 *				"sumaAseguradaMax": "3500000.0",<br>
	 *				"claveTipoDeducible": "11",<br>
	 *				"deducible": 0.0,<br>
	 *				"valoresDeducible": {<br>
	 *					"320105": "0.0",<br>
	 *					"320108": "25.0",<br>
	 *					"320109": "50.0"<br>
	 *				}<br>
	 *			}]<br>
	 * 		</code>
	 * 		<p>
	 * 		<em>obligatoriedad. Define si la cobertura debe ser contratada.<br>
	 * 			0/1: Opcional, 2:Obligatorio Parcial, 3:Obligatorio</em>
	 * 		<em>contratada. Define si la cobertura ya viene contratada cuando se crea la cotizacion.</em>
	 * 		<em>sumaAsegurada. Valor por defecto de la suma asegurada.<br>
	 * 			El valor debe estar entre los rangos de sumaAseguradaMin y sumaAseguradaMax</em>
	 * 		<em>claveDeducible:
	 *			0	N/A<br>
	 *			1	% S/SA<br>
	 *			2	% S/VR<br>
	 *			3	% S/R<br>
	 *			4	% S/P<br>
	 *			7	% S/SA de cada Equipo DaÃ±ado<br>
	 *			8	% S/VT de Embarque<br>
	 *			9	% S/VR de cada Equipo DaÃ±ado<br>
	 *			10	Valor<br>
	 *			11	DSMGVDF<br>
	 *			12	Dias Laborables<br>
	 *			13	De acuerdo al Riesgo Afectado<br>
	 *			14	SegÃºn EspecificaciÃ³n Adjunta<br>
	 *			15	DÃ­as de Espera<br>
	 *			16	DÃ³lares o Equivalente en M.N.<br>
	 *			17	Por Evento<br>
	 *			18	UMA<br>
	 *			99	Libre<br>
	 * 		</em>
	 * 		<em>deducible. Valor por defecto de deducible</em>
	 * 		<em>valoresDeducible. Son los valores que puede tomar el deducible
	 * 			(son los campos de la segunda columna)</em>
	 * 		<p>
	 * 		Mensajes que se devuelven cuando hay excepcion:<br>
	 * 		<em>"Error al obtener las coberturas"</em>
	 * 		<em>"Error al crear el mapa de coberturas"</em>
	 * 
	 */	
	@WebMethod(operationName = "obtenerCoberturas")
	@WebResult(targetNamespace = "")	
	public String obtenerCoberturas( @WebParam(name = "cotizacionCobertura") String json,
			@WebParam(name = "token") String token){
		
		List<Object> coberturasList = new ArrayList<Object>();
		Gson gson = new Gson();
		CotizacionCoberturaView cot = gson.fromJson(json, CotizacionCoberturaView.class);
		
		try {
			coberturasList = wsCotizacionService.obtenerCoberturas(cot, token);
		} catch (SystemException e) {
			return "Error al obtener las coberturas.\n"+e.getMessage();
		}
		return gson.toJson(coberturasList);
		
	}
	
	/**
	 * Servicio para obtener los tipos de endoso disponibles para Web Service
	 * @param token
	 * 		Cadena con el token de sesion
	 * @return
	 * 		Cadena en formato JSON con la lista de tipos de endoso disponibles
	 */
	@WebMethod(operationName = "getListTiposEndoso")
	@WebResult(targetNamespace = "")
	public String getListTiposEndoso(@WebParam(name = "token") String token){
		Gson gson = new Gson();
		Map<Integer, String> map = new LinkedHashMap<Integer, String>();
		
		try {
			map = wsCotizacionService.getListTiposEndoso(token);
		} catch (SystemException e) {
			return "Error al obtener los tipos de endoso... "+e.getMessage();
		}
		
		return gson.toJson(map);
	}
	
	@WebMethod(operationName = "getListFormasDePagoDePoliza")
	@WebResult(targetNamespace = "")
	public String getListFormasDePagoDePoliza(@WebParam(name = "json") String json,
			@WebParam(name = "token") String token){
		Gson gson = new Gson();
		EndosoTransporteDTO dto = gson.fromJson(json, EndosoTransporteDTO.class);
		Map<Integer, String> map = new LinkedHashMap<Integer, String>();
		dto.setToken(token);
		
		try {
			map = wsCotizacionService.getListFormasPagoDePoliza(dto);
		} catch(SystemException e){
			return "No fue posible obtener las formas de pago de la póliza. "+e.getMessage();
		}
		
		return gson.toJson(map);
	}
	
	@WebMethod(operationName = "getMunicipioByCP")
	@WebResult(targetNamespace = "")
	public String getMunicipioByCP(@WebParam(name = "codigoPostal") String codigoPostal,
			 @WebParam(name = "token") String token){
		Gson gson = new Gson();
		Map<String, String> map = null;
		
		try {
			map = wsCotizacionService.getMunicipioByCP(codigoPostal, token);
		} catch(SystemException e){
			return "No fue posible obtener los municipios. "+e.getMessage();
		}
		
		return gson.toJson(map);
	}
	
	@WebMethod(operationName = "cotizarPoliza")
	@WebResult(targetNamespace = "")	
	public String cotizarPoliza(	@WebParam(name = "json") String json,
							@WebParam(name = "token") String token){
		LOG.info(this.getClass().getName()+".cotizar ... entrando a cotizar nuevo");
		Gson gson = new Gson();
		CotizacionView cotizacionView = null;
		
		try {
			cotizacionView = gson.fromJson(json, CotizacionView.class);
		} catch (JsonParseException e) {
			return "Error al parsear la cadena JSON de los datos de entrada";
		}

		if (cotizacionView == null) {
			return "Favor de enviar los datos de la cotización";
		}
		
		Map<String, Object> map = new HashMap<String, Object>(1);
		
		try {
			map = wsCotizacionService.creaCotizacion(cotizacionView, token);

			return gson.toJson(map);
		} catch (SystemException e) {
			return "No fue posible crear la cotizacion, "  + e.getMessage();
		} catch (Exception e){
			return "No fue posible crear la cotizacion. "+e.getMessage();
		}
	}
	
	@WebMethod(operationName = "emitirPoliza")
	@WebResult(targetNamespace = "")	
	public String emitirPoliza(	@WebParam(name = "json") String json,
							@WebParam(name = "token") String token){
		LOG.info(this.getClass().getName()+".cotizar ... entrando a emitir nuevo");
		Gson gson = new Gson();
		EmisionView emisionView = null;
		
		try {
			emisionView = gson.fromJson(json, EmisionView.class);
		} catch (JsonParseException e) {
			return "Error al parsear la cadena JSON de los datos de entrada";
		}
		
		if (emisionView == null) {
			return "Favor de enviar los datos de la cotización";
		}
		
		Map<String, String> map = new HashMap<String, String>();
		
		try {
			map = wsCotizacionService.emitirPoliza(emisionView, token);

			String icono = map.get("icono");
			String mensaje = map.get("mensaje");
			String idToPoliza = map.get("idpoliza");

			if (icono.equals(PolizaDTO.ICONO_CONFIRM)){
				//envia avisos a cobranza
				avisosService.enviarAviso(BigDecimal.valueOf(Long.valueOf(idToPoliza)),AVISO_EMISION_WS,null);
				
				PolizaDTO poliza = entidadService.findById(PolizaDTO.class,BigDecimal.valueOf(Long.valueOf(idToPoliza)));
				return "{\"idPoliza\":" + poliza.getIdToPoliza() + ", \"numeroPoliza\":\"" + poliza.getNumeroPolizaFormateada() + "\", \"mensaje\":\""+mensaje+" "+poliza.getNumeroPolizaFormateada()+"\"}";				
			} else if (icono.equals(PolizaDTO.ICONO_ERROR)) {
				return "{\"mensaje\":\""+mensaje+"\"}";
			}
			
			return "{\"mensaje\":\""+mensaje+"\"}";
		} catch (SystemException e) {
			return "No fue posible emitir la cotizacion, "  + e.getMessage();
		} catch (Exception e){
			return "No fue posible emitir la cotizacion. "+e.getMessage();
		}
	}
	
	@WebMethod(operationName = "cotizarEndosoConductoCobro")
	@WebResult(targetNamespace = "")
	public String cotizarEndosoConductoCobro(@WebParam(name = "json") String json,
			@WebParam(name = "token") String token){
		Gson gson = new Gson();
		EndosoTransporteDTO dto = gson.fromJson(json, EndosoTransporteDTO.class);
		Map<String, Object> result = new HashMap<String, Object>();
		dto.setToken(token);
		
		try {
			result = wsCotizacionService.cotizarEndoso(dto);
		} catch (ApplicationException e) {
			return e.getMessage();
		} catch (SystemException e) {
			return e.getMessage();
		}catch (Exception e) {
			return "Error al cotizar el endoso de conducto de cobro: " + e.getMessage();
		}
		
		return gson.toJson(result);
	}
	
	@WebMethod(operationName = "cotizarEndosoCambioFormaPago")
	@WebResult(targetNamespace = "")
	public String cotizarEndosoCambioFormaPago(@WebParam(name = "json") String json,
			@WebParam(name = "token") String token){
		Gson gson = new Gson();
		EndosoTransporteDTO dto = gson.fromJson(json, EndosoTransporteDTO.class);
		Map<String, Object> result = new HashMap<String, Object>();
		dto.setToken(token);
		
		try {
			result = wsCotizacionService.cotizarEndoso(dto);
		} catch (ApplicationException e) {
			return e.getMessage();
		} catch (SystemException e) {
			return e.getMessage();
		}catch (Exception e) {
			return "Error al cotizar el endoso de Cambio de Forma de Pago: " + e.getMessage();
		}
		
		return gson.toJson(result);
	}
	
	@WebMethod(operationName = "cotizarEndosoCambioDatos")
	@WebResult(targetNamespace = "")
	public String cotizarEndosoCambioDatos(@WebParam(name = "json") String json,
			@WebParam(name = "token") String token){
		Gson gson = new Gson();
		EndosoTransporteDTO dto = gson.fromJson(json, EndosoTransporteDTO.class);
		Map<String, Object> result = new HashMap<String, Object>();
		dto.setToken(token);
		
		try {
			result = wsCotizacionService.cotizarEndoso(dto);
		} catch (ApplicationException e) {
			return e.getMessage();
		} catch (SystemException e) {
			return e.getMessage();
		}catch (Exception e) {
			return "Error al cotizar el endoso de cambio de datos: " + e.getMessage();
		}
		
		return gson.toJson(result);
	}
	
	@WebMethod(operationName = "cotizarEndosoCancelacionPoliza")
	@WebResult(targetNamespace = "")
	public String cotizarEndosoCancelacionPoliza(@WebParam(name = "json") String json,
			@WebParam(name = "token") String token){
		Gson gson = new Gson();
		EndosoTransporteDTO dto = gson.fromJson(json, EndosoTransporteDTO.class);
		Map<String, Object> result = new HashMap<String, Object>();
		dto.setToken(token);
		
		try {
			result = wsCotizacionService.cotizarEndoso(dto);
		} catch (ApplicationException e) {
			return e.getMessage();
		} catch (SystemException e) {
			return e.getMessage();
		}catch (Exception e) {
			return "Error al cotizar el endoso de cancelación de póliza: " + e.getMessage();
		}
		
		return gson.toJson(result);
	}
	
	@WebMethod(operationName = "emitirEndosoConductoCobro")
	@WebResult(targetNamespace = "")
	public String emitirEndosoConductoCobro(@WebParam(name = "json") String json,
			@WebParam(name = "token") String token){
		Gson gson = new Gson();
		EndosoTransporteDTO dto = gson.fromJson(json, EndosoTransporteDTO.class);
		Map<String, Object> result = new HashMap<String, Object>();
		dto.setToken(token);
		
		try {
			result = wsCotizacionService.emitirEndoso(dto);
		} catch (SystemException e) {
			return "Error al emitir el endoso... "+e.getMessage();
		}
		
		return gson.toJson(result);
	}
	
	@WebMethod(operationName = "emitirEndosoCambioFormaPago")
	@WebResult(targetNamespace = "")
	public String emitirEndosoCambioFormaPago(@WebParam(name = "json") String json,
			@WebParam(name = "token") String token){
		Gson gson = new Gson();
		EndosoTransporteDTO dto = gson.fromJson(json, EndosoTransporteDTO.class);
		Map<String, Object> result = new HashMap<String, Object>();
		dto.setToken(token);
		
		try {
			result = wsCotizacionService.emitirEndoso(dto);
		} catch (SystemException e) {
			return "Error al emitir el endoso... "+e.getMessage();
		}
		
		return gson.toJson(result);
	}
	
	@WebMethod(operationName = "emitirEndosoCambioDatos")
	@WebResult(targetNamespace = "")
	public String emitirEndosoCambioDatos(@WebParam(name = "json") String json,
			@WebParam(name = "token") String token){
		Gson gson = new Gson();
		EndosoTransporteDTO dto = gson.fromJson(json, EndosoTransporteDTO.class);
		Map<String, Object> result = new HashMap<String, Object>();
		dto.setToken(token);
		
		try {
			result = wsCotizacionService.emitirEndoso(dto);
		} catch (SystemException e) {
			return "Error al emitir el endoso... "+e.getMessage();
		}
		
		return gson.toJson(result);
	}
	
	@WebMethod(operationName = "emitirEndosoCancelacionPoliza")
	@WebResult(targetNamespace = "")
	public String emitirEndosoCancelacionPoliza(@WebParam(name = "json") String json,
			@WebParam(name = "token") String token){
		Gson gson = new Gson();
		EndosoTransporteDTO dto = gson.fromJson(json, EndosoTransporteDTO.class);
		Map<String, Object> result = new HashMap<String, Object>();
		dto.setToken(token);
		
		try {
			result = wsCotizacionService.emitirEndoso(dto);
		} catch (SystemException e) {
			return "Error al emitir el endoso... "+e.getMessage();
		}
		
		return gson.toJson(result);
	}
	
	@WebMethod(operationName = "imprimirCotizacion")
	@WebResult(targetNamespace = "")	
	public byte[] imprimirCotizacion(@WebParam(name = "json") String json,
								@WebParam(name = "token") String token){
		try {
			final Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss").create();
			ParametrosImpresionCotizacionView params = gson.fromJson(json, ParametrosImpresionCotizacionView.class);
			return wsCotizacionService.imprimirCotizacion(params, token);
		} catch (Exception e) {
			return e.getMessage().getBytes();
		}
	}
	
	
	@WebMethod(operationName = "cotizarEndosoFlotilla")
	@WebResult(targetNamespace = "")	
	public String cotizarEndosoFlotilla(@WebParam(name = "json") String json,
								@WebParam(name = "token") String token){
		try {
			final Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss").create();
			CotizarEndosoFlotillaRequest request = gson.fromJson(json, CotizarEndosoFlotillaRequest.class);
			final PolizaDTO poliza = wsCotizacionService.validatePoliza(request.getNumeroPoliza());
			
			CotizarEndosoFlotillaResponse response = wsCotizacionService.cotizarEndosoFlotilla(request, token);
			return gson.toJson(response);
		} catch (Exception e) {
			return e.getMessage();
		}
	}
	
	
	@WebMethod(operationName = "emitirEndosoFlotilla")
	@WebResult(targetNamespace = "")	
	public String emitirEndosoFlotilla(@WebParam(name = "json") String json,
								@WebParam(name = "token") String token){
		try {
			final Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss").create();
			EmitirEndosoFlotillaRequest request = gson.fromJson(json, EmitirEndosoFlotillaRequest.class);
			EmitirEndosoFlotillaResponse response = wsCotizacionService.emitirEndosoFlotilla(request, token);
			return gson.toJson(response);
		} catch (Exception e) {
			return e.getMessage();
		}
	}
	
	
	
}