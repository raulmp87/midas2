package mx.com.afirme.midas2.ws.coberturas;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.controlArchivo.ControlArchivoDN;
import mx.com.afirme.midas.sistema.controlArchivo.ControlArchivoDTO;
import mx.com.afirme.midas.sistema.controlArchivo.FileManagerService;
import mx.com.afirme.midas.solicitud.SolicitudDTO;
import mx.com.afirme.midas.solicitud.documento.DocumentoDigitalSolicitudDN;
import mx.com.afirme.midas.solicitud.documento.DocumentoDigitalSolicitudDTO;
import mx.com.afirme.midas2.domain.coberturas.kilometros.GeneraSolicitudRequest;
import mx.com.afirme.midas2.domain.coberturas.kilometros.GeneraSolicitudResponse;
import mx.com.afirme.midas2.domain.suscripcion.solicitud.comentarios.Comentario;
import mx.com.afirme.midas2.exeption.ApplicationException;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.coberturas.kilometros.CoberturaAdicionalService;
import mx.com.afirme.midas2.service.suscripcion.solicitud.comentarios.ComentariosService;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@Stateless
@WebService(name = "CoberturaAdicional", serviceName="CoberturaAdicional", targetNamespace = "https://segurosafirme.com.mx/Cobertura")
public class CoberturaAdicionalWs {
	
	private static final Logger LOG = Logger.getLogger(CoberturaAdicionalWs.class);
	private static final String CLAVE_TIPO_DOCUMENTO = "16";
	private static final String EXTENTION_FILE = ".txt";
	
	@EJB
	private CoberturaAdicionalService coberturaAdicionalService;
	
	@EJB
	private FileManagerService fileManagerService;
	
	@EJB
	private EntidadService entidadService;
	
	@EJB
	private ComentariosService comentariosService;
	
	/**
	 * Generaci&oacute;n de una nueva Solicitud
	 * 
	 * @param json
	 * 		Cadena en formato JSON con los valores para generar la nueva solicitud, ejemplo:
	 * 	          <p>
	 * 			  <b>Generaci&oacute;n de Solicitud</b><br>
	 *            <code>
	 *            {<br>
	 *            	"numeroPoliza":"01-310101095532-00",<br>
	 *            	"distanciaTotal":"12345.6789",<br>
	 *            	"fechaHoraInicio":"2017-04-20T16:25:50",<br>
	 *            	"fechaHoraFin":"2017-04-23T16:25:50"<br>
	 *            }<br>
	 *            </code>
	 * @return Cadena en formato JSON con el resultado de la Solicitud Generada
	 * <p>
	 * <b>Generaci&oacute;n de Solicitud</b><br>
	 * <code>
	 * {<br>
	 *		"numeroSolicitud":"454545",<br>
	 *		"valorPrimaCobertura":"1500.7899"<br>
	 * }<br>
	 * </code>
	 * 
	 */
	@WebMethod(operationName = "generaSolicitud")
	public String generaSolicitud(@WebParam(name = "json") String json){
		GeneraSolicitudResponse generaSolicitudResponse = new GeneraSolicitudResponse();
		final Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss").create();
		Map<String, Object> informacionSolicitud = new HashMap<String, Object>();
		
		try {
			GeneraSolicitudRequest generaSolicitudRequest = gson.fromJson(json, GeneraSolicitudRequest.class);

			if (generaSolicitudRequest != null) {
				informacionSolicitud = coberturaAdicionalService.generaSolicitud(generaSolicitudRequest);
				
				generaSolicitudResponse = (GeneraSolicitudResponse)informacionSolicitud.get("generaSolicitudResponse");
				
				this.registrarArchivoSolicitud(generaSolicitudResponse.getNumeroSolicitud(), json, informacionSolicitud);
				this.registraComentarioSolicitud(generaSolicitudRequest, generaSolicitudResponse);
			} else {
				return "Error al obtener los parametros iniciales para generar la Solicitud.";
			}
		} catch (ApplicationException ae) {
			return gson.toJson(ae.getFieldErrors());
		} catch (Exception ex) {
			return "Error al generar la Solicitud.\n" + ex.getMessage();
		}
		
		return gson.toJson(this.resultadoSolicitud(generaSolicitudResponse));
	}
	
	/**
	 * Contrataci&oacute;n de una nueva Solicitud creada
	 * 
	 * @param json
	 * 		Cadena en formato JSON con los valores para contratar la nueva solicitud, ejemplo:
	 * 	          <p>
	 * 			  <b>Par&aacute;metros para la contrataci&oacute;n de la Solicitud</b><br>
	 *            <code>
	 *            {<br>
	 *            	"numeroSolicitud":"454545",<br>
	 *            	"contratar":"true"<br>
	 *            }<br>
	 *            </code>
	 * @return Cadena en formato JSON para contratar una solicitud ya Existente
	 * <p>
	 * <b>Salida del M&eacute;todo de Contrataci&oacute;n de Solicitud</b><br>
	 * <code>
	 * {<br>
	 *		"numeroSolicitud":"454545",<br>
	 *		"contratada":"true"<br>
	 *		"mensaje":"**SOLICITUD CONTRATADA**"<br>
	 * }<br>
	 * </code>
	 * <b>Salida del M&eacute;todo de NO Contrataci&oacute;n de Solicitud</b><br>
	 * <code>
	 * {<br>
	 *		"numeroSolicitud":"454545",<br>
	 *		"contratada":"false"<br>
	 *		"mensaje":"**SOLICITUD NO CONTRATADA**"<br>
	 * }<br>
	 * </code>
	 * 
	 */
	@WebMethod(operationName = "contrataSolicitud")
	public String contrataSolicitud(@WebParam(name = "json") String json){
		final Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss").create();
		String mensajeRegreso = "";
		
		try {
			GeneraSolicitudRequest generaSolicitudRequest = gson.fromJson(json, GeneraSolicitudRequest.class);

			if (generaSolicitudRequest == null) {
				mensajeRegreso = "{\"mensaje\":\"Error al obtener los par\u00e1metros iniciales para generar la Solicitud.\"}";
			}else{
				if (generaSolicitudRequest.getNumeroSolicitud() != null && generaSolicitudRequest.getNumeroSolicitud().intValue() > 0){
					mensajeRegreso = coberturaAdicionalService.contrataSolicitud(generaSolicitudRequest.getNumeroSolicitud(), generaSolicitudRequest.getContratar());
				}else{
					mensajeRegreso = "{\"mensaje\":\"No se especific\u00f3 una Solicitud correcta.\"}";
				}
			}
		} catch (ApplicationException ae) {
			return gson.toJson(ae.getFieldErrors());
		} catch (Exception ex) {
			return "Error al contratar la Solicitud.\n" + ex.getMessage();
		}
		
		return mensajeRegreso;
	}
	
	private void registraComentarioSolicitud(GeneraSolicitudRequest generaSolicitudRequest, GeneraSolicitudResponse generaSolicitudResponse){
		SolicitudDTO solicitudDTO = entidadService.findById(SolicitudDTO.class, generaSolicitudResponse.getNumeroSolicitud());
		
		if (solicitudDTO != null){
			String comentarios = coberturaAdicionalService.obtenerParametrosSolicitud(true, generaSolicitudRequest, generaSolicitudResponse);
			
			Comentario comentario = new Comentario();
			comentario.setSolicitudDTO(solicitudDTO);
			comentario.setCodigoUsuarioCreacion(solicitudDTO.getCodigoUsuarioCreacion());
			comentario.setValor(comentariosService.validaComentarioSolicitudCotizacion(comentarios, CoberturaAdicionalService.TIPO_COMENTARIO));
			comentariosService.guardarComentarioCoberturaAdicional(comentario);
		}
	}
	
	private Map<String, String> resultadoSolicitud(GeneraSolicitudResponse generaSolicitudResponse){
		Map<String, String> informacionSolicitud = new HashMap<String, String>();
		informacionSolicitud.put("numeroSolicitud", generaSolicitudResponse.getNumeroSolicitud().toString());
		informacionSolicitud.put("valorPrimaCobertura", UtileriasWeb.formatoMoneda(generaSolicitudResponse.getValorPrimaCobertura()));
		return informacionSolicitud;
	}
	
	private void registrarArchivoSolicitud(BigDecimal idToSolicitud, String parameterData, Map<String, Object> informacionSolicitud){
	    String fileName = "solicitudCoberturaAdicional" + idToSolicitud.toString() + EXTENTION_FILE;
		String textFileContent = String.format("La Solicitud de Endoso No. [%s] para la Cobertura Adicional fue creada con los siguientes argumentos \n\n %s  \n\nCon un monto de [%s]",
						idToSolicitud,
						parameterData,
						UtileriasWeb.formatoMoneda(((GeneraSolicitudResponse)informacionSolicitud.get("generaSolicitudResponse")).getValorPrimaCobertura())).trim();
	    	    
	    try{
	        ControlArchivoDTO controlArchivoDTO = getControlArchivoDTO(CLAVE_TIPO_DOCUMENTO, fileName);
	        
			try {
				fileManagerService.uploadFile(fileName, controlArchivoDTO.getIdToControlArchivo().toString(), textFileContent.getBytes());
			} catch (Exception e) {
				LOG.error("No se pudo subir el archivo para la solicitud", e);
			}
			
			DocumentoDigitalSolicitudDTO documentoDigitalSolicitudDTO = new DocumentoDigitalSolicitudDTO();
			documentoDigitalSolicitudDTO.setCodigoUsuarioCreacion(informacionSolicitud.get("usuarioCreaModifica").toString());
			documentoDigitalSolicitudDTO.setNombreUsuarioCreacion(informacionSolicitud.get("nombreUsuarioCreaModifica").toString());
			documentoDigitalSolicitudDTO.setFechaCreacion(new Date());
			documentoDigitalSolicitudDTO.setCodigoUsuarioModificacion(informacionSolicitud.get("usuarioCreaModifica").toString());
			documentoDigitalSolicitudDTO.setNombreUsuarioModificacion(informacionSolicitud.get("nombreUsuarioCreaModifica").toString());
			documentoDigitalSolicitudDTO.setFechaModificacion(new Date());
			documentoDigitalSolicitudDTO.setIdToControlArchivo(controlArchivoDTO.getIdToControlArchivo());
			
			SolicitudDTO solicitudDTO = new SolicitudDTO();
			solicitudDTO.setIdToSolicitud(idToSolicitud);
			documentoDigitalSolicitudDTO.setSolicitudDTO(solicitudDTO);
			
			DocumentoDigitalSolicitudDN documentoDigitalSolicitudDN = DocumentoDigitalSolicitudDN.getInstancia();
			documentoDigitalSolicitudDN.agregar(documentoDigitalSolicitudDTO);
	    }catch(Exception ex){
	    	LOG.error("No se pudo crear el archivo para la solicitud", ex);
	    }
	}
	
	private ControlArchivoDTO getControlArchivoDTO(String claveTipo, String fileName) throws SystemException {
		ControlArchivoDTO controlArchivoDTO = new ControlArchivoDTO();
		controlArchivoDTO.setClaveTipo(claveTipo);
		controlArchivoDTO.setNombreArchivoOriginal(fileName);
		ControlArchivoDN controlArchivoDN = ControlArchivoDN.getInstancia();
		return controlArchivoDN.agregar(controlArchivoDTO);
	}
}