package mx.com.afirme.midas2.ws.siniestros;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

import mx.com.afirme.midas2.action.movil.ajustador.AjustadorMovilAction.MostrarPolizaParameter;
import mx.com.afirme.midas2.domain.movil.ajustador.FindPolizaParameter;
import mx.com.afirme.midas2.domain.movil.ajustador.FindPolizaResponse;
import mx.com.afirme.midas2.domain.movil.ajustador.Poliza;
import mx.com.afirme.midas2.service.movil.ajustador.AjustadorMovilService;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@Stateless
@WebService(name = "SiniestrosPoliza", 
serviceName="SiniestrosPoliza", 
targetNamespace = "https://segurosafirme.com.mx/siniestros/Poliza")
public class PolizaWs  {

	@EJB
	AjustadorMovilService service;
	
	@EJB(beanName = "UsuarioServiceDelegate")
	private UsuarioService usuarioService;
	
	/**
	 * Busca los incisos de una P&oacute;liza
	 * 
	 * @param json
	 * 		Cadena en formato JSON con los valores de filtros disponibles para buscar los incisos, ejemplos:
	 * 	          <p>
	 * 			  <b>B&uacute;squeda por N&uacute;mero de P&oacute;liza</b><br>
	 *            <code>
	 *            {<br>
	 *            	"poliza":"001-3101559225-00",<br>
	 *            	"inciso":null, <br>
	 *            	"numeroSerie":"",<br>
	 *            	"contratante":"",<br>
	 *            	"fechaOcurrido":"2016-06-30T00:00:00"<br>
	 *            }<br>
	 *            </code>
	 *            <p>
	 *            <b>B&uacute;squeda por N&uacute;mero de P&oacute;liza e Inciso</b><br>
	 *            <code>
	 *            {<br>
	 *            	"poliza":"001-3101559225-00",<br>
	 *            	"inciso":1,<br>
	 *            	"numeroSerie":"",<br>
	 *            	"contratante":"",<br>
	 *            	"fechaOcurrido":"2016-06-30T00:00:00"<br>
	 *            }<br>
	 *            </code>
	 *            <p>
	 *            <b>B&uacute;squeda N&uacute;mero de Serie<br>
	 *            <code>
	 *            {<br>
	 *            	"poliza":"",<br>
	 *            	"inciso":null,<br>
	 *            	"numeroSerie":"0XX0XX0X0XX000000",<br>
	 *            	"contratante":"",<br>
	 *            	"fechaOcurrido":"2016-06-30T00:00:00"<br>
	 *            }<br>
	 *            </code> 
	 *            <p>
	 *            <b>B&uacute;squeda por Contratante<br>
	 *            <code>
	 *            {<br>
	 *            	"poliza":"",<br>
	 *            	"inciso":null,<br>
	 *            	"numeroSerie":"",<br>
	 *            	"contratante":"JORGE PEREZ PEREZ",<br>
	 *            	"fechaOcurrido":"2016-06-30T00:00:00"<br>
	 *            }<br>
	 *            </code>
	 * @param token Cadena con el token de sesi&oacute;n
	 * @return Cadena en formato JSON con el resultado de la b&uacute;squeda con la siguiente estructura
	 * 
	 * <p>
	 * <b>Resultado de la B&uacute;squeda</b><br>
	 * <code>
	 * {<br>
	 *		"id":"8538126-5-1",<br>
	 *		"numeroPoliza":"001-3101559225-00",<br>
	 *		"numeroInciso":1,<br>
	 *		"numeroSerie":"0XX0XX0X0XX000000",<br>
	 *		"vehiculo":"HARLEY DAVIDSON XL1200 X FORTY EIGHT2015",<br>
	 *		"fechaInicioVigencia":"2015-03-05T00:00:00",<br>
	 *		"fechaFinVigencia":"2016-03-05T00:00:00",<br>
	 *		"estatus":"PÓLIZA EN VIGOR-EMITIDA",<br>
	 *		"placa":"ND      ",<br>
	 *		"procedeSiniestro":<br>
	 *		{<br>
	 *			"procede":false,<br>
	 *			"descripcion":"N FUERA DE VIGENCIA"<br>
	 *		},<br>
	 *		"nombreContratante":"JORGE PEREZ PEREZ"<br>
	 * }<br>
	 * </code>
	 * 
	 */
	@WebMethod(operationName = "busquedaIncisosPoliza")
	public String busquedaIncisosPoliza(@WebParam(name = "json") String json,@WebParam(name = "token") String token){
		List<FindPolizaResponse> resultList = null;
		final Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss").create();

		try{
			usuarioService.validateToken(token);
			FindPolizaParameter parameter=gson.fromJson(json, FindPolizaParameter.class);
			resultList=service.findPoliza(parameter);
		}catch (Exception e) {
			return "Error al obtener los incisos por Poliza.\n"+e.getMessage();
		}
		return gson.toJson(resultList);
	}
	
	/**
	 * Busca la informaci&oacute;n de una P&oacute;liza
	 * 
	 * @param json
	 * 		Cadena en formato JSON con los valores de filtros disponibles para buscar una P&oacute;liza, ejemplo:
	 * 	          <p>
	 * 			  <b>B&uacute;squeda por P&oacute;liza</b><br>
	 *            <code>
	 *            {<br>
	 *            	"fechaOcurrido":"2016-06-30T00:00:00",<br>
	 *            	"id":"8538126-5-1"<br>
	 *            }<br>
	 *            </code>
	 * @param token Cadena con el token de sesi&oacute;n
	 * @return Cadena en formato JSON con el resultado de la b&uacute;squeda con la siguiente estructura
	 * <p>
	 * <b>Resultado de la B&uacute;squeda</b><br>
	 * <code>
	 * {<br>
	 *		"id":"8538126-5-1",<br>
	 *		"numeroPoliza":"001-3101559225-00",<br>
	 *		"numeroInciso":1,<br>
	 *		"fechaInicioVigencia":"2015-03-05T00:00:00",<br>
	 *		"fechaFinVigencia":"2016-03-05T00:00:00",<br>
	 *		"lineaNegocio":"AF MOTOCICLETAS INDIVIDUALES",<br>
	 *		"estatus":"POLIZA NO VIGENTE",<br>
	 *		"procedeSiniestro":<br>
	 *		{<br>
	 *			"procede":false,<br>
	 *			"descripcion":"N FUERA DE VIGENCIA"<br>
	 *		},<br>
	 *		"nombreContratante":"JORGE PEREZ PEREZ",<br>
	 *		"nombreConductor":"  ",<br>
	 *		"vehiculo":"HARLEY DAVIDSON XL1200 X FORTY EIGHT 2015",<br>
	 *		"numeroSerie":"0XX0XX0X0XX000000",<br>
	 *		"numeroMotor":"XX0X000000",<br>
	 *		"placa":"ND      ",<br>
	 *		"capacidad":"5 Pasajeros",<br>
	 *		"servicio":"PARTICULAR",<br>
	 *		"uso":"NORMAL",<br>
	 *		"paquete":"AMPLIA",<br>
	 *		"coberturas":[],<br>
	 *		"recibos":<br>
	 *		[<br>
	 *			{<br>
	 *				"id":39000764,<br>
	 *				"folio":4800002,<br>
	 *				"fechaInicio":"2015-03-05T00:00:00",<br>
	 *				"fechaFin":"2016-03-05T00:00:00",<br>
	 *				"estatus":"PAG"<br>
	 *			}<br>
	 *		],<br>
	 *		"alertas":[]<br>
	 * }<br>
	 * </code>
	 * 
	 */
	@WebMethod(operationName = "mostrarPoliza")
	public String mostrarPoliza(@WebParam(name = "json") String json,@WebParam(name = "token") String token) {
		Poliza polizaResult=null;
		final Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss").create();
		
		try{
			usuarioService.validateToken(token);
			MostrarPolizaParameter parameter=gson.fromJson(json, MostrarPolizaParameter.class);
			polizaResult=service.getPoliza(parameter.getId(), parameter.getFechaOcurrido());
		}catch (Exception e) {
			return "Error al obtener la información de la Poliza.\n"+e.getMessage();
		}
		
		return gson.toJson(polizaResult);
	}
}

