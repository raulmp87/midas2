package mx.com.afirme.midas2.ws.siniestros;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

import org.apache.log4j.Logger;

import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas2.domain.sistema.bitacora.Bitacora.EVENTO;
import mx.com.afirme.midas2.domain.sistema.bitacora.Bitacora.TIPO_BITACORA;
import mx.com.afirme.midas2.dto.siniestros.ComplementoReporteDTO;
import mx.com.afirme.midas2.dto.siniestros.CreacionReporteDTO;
import mx.com.afirme.midas2.dto.siniestros.IncisoBusquedaPoliza;
import mx.com.afirme.midas2.dto.siniestros.IncisoPolizaDTO;
import mx.com.afirme.midas2.service.bitemporal.siniestros.PolizaSiniestroService;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;
import mx.com.afirme.midas2.service.siniestros.cabina.reportecabina.ReporteCabinaService;
import mx.com.afirme.midas2.service.sistema.bitacora.BitacoraService;
import mx.com.afirme.midas2.util.StringUtil;

@Stateless
@WebService(name = "SiniestrosCabina",portName="SiniestroSoapBinding", serviceName="SiniestrosCabina", targetNamespace = "https://segurosafirme.com.mx/SiniestrosCabina")
public class SiniestrosWs  {

	public  static final Logger log = Logger.getLogger(SiniestrosWs.class);
	 private static final String RG_24_HRS = "([01]?[0-9]|2[0-3]):[0-5][0-9]";
	
	@EJB
	ReporteCabinaService reporteCabinaService;
	
	@EJB
	PolizaSiniestroService polizaSiniestroService; 
	
	@EJB 
	BitacoraService bitacoraService;
	
	@EJB(beanName = "UsuarioServiceDelegate")
	private UsuarioService usuarioService;
	
	
	@WebMethod(operationName = "generarReporteCabina")
	public CreacionReporteDTO generarReporteCabina(
			@WebParam(name = "continuityId"    )Long continuityId, 
			@WebParam(name = "oficinaId"       )String codigoOficina, 
			@WebParam(name = "personaReporte"  )String personaReporte, 
			@WebParam(name = "fechaOcurrido"   )Date fechaOcurrido, 
			@WebParam(name = "horaOcurrido"    )String horaOcurrido,
			@WebParam(name = "lada"            )String lada, 
			@WebParam(name = "telefono"        )String telefono, 
			@WebParam(name = "personaConductor")String personaConductor,
			@WebParam(name = "usuario")         String usuario
			){
		
		CreacionReporteDTO creacion = new CreacionReporteDTO();
		String mensajeError = "" ;
		
		try{
		
			if ( continuityId == 0 ) continuityId = null;
			
			if( StringUtil.isEmpty(codigoOficina) ){
				mensajeError+=" codigoOficina es OBLIGATORIO / ";
			}
			
			if( StringUtil.isEmpty(personaReporte) || personaReporte.length() > 100 ){
				mensajeError+=" personaReporte es OBLIGATORIO o menor/igual a 100 caracteres / ";
			}
			
			if( fechaOcurrido == null){
				mensajeError+=" fechaOcurrido es OBLIGATORIO / ";
			}else{
				// FORMATEAR FECHA A YYYY-MM-DD sin minutos
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd"); 
				String tmpFecha = sdf.format(fechaOcurrido);
				fechaOcurrido   = sdf.parse(tmpFecha);
			}
			
			if( StringUtil.isEmpty(horaOcurrido) ){
				mensajeError+=" horaOcurrido es OBLIGATORIO o en formato HH:MM / ";
			}else{
				 Pattern p = Pattern.compile(RG_24_HRS);
				 Matcher m = p.matcher(horaOcurrido);
				 if ( !m.matches() ){
					 mensajeError+=" horaOcurrido NO CUMPLE formato 24 HRS HH:MM / ";
				 }
				 
				
			}
			
			if( StringUtil.isEmpty(lada) || lada.length() > 3 ){
				mensajeError+=" lada es OBLIGATORIO o menor/igual a 3 caracteres / ";
			}
			
			if( StringUtil.isEmpty(telefono) || telefono.length() > 8 ){
				mensajeError+=" teléfono es OBLIGATORIO o menor/igual a 8 caracteres / ";
			}
			
			if( StringUtil.isEmpty(usuario) ){
				mensajeError+=" usuario es OBLIGATORIO / ";
			}
			
			if(StringUtil.isEmpty(mensajeError) ) {
				
				creacion = this.reporteCabinaService.generarReporte
				(continuityId, codigoOficina, personaReporte, fechaOcurrido, horaOcurrido, lada, telefono, personaConductor,usuario);
				
			}else{
				creacion.setDescripcionError(mensajeError);
			}
		
		}catch(Exception e){
			mensajeError = e.getMessage();
			creacion.setDescripcionError(mensajeError);
		}
		
		// BITACORA	
		try{
			if( !StringUtil.isEmpty(mensajeError) ){
				bitacoraService.registrar(TIPO_BITACORA.REPORTE_CABINA, EVENTO.GENERAR_REPORTE_CABINA_WS, "0", "BUSQUEDA_POLIZA_WS", "ERROR AL GENERAR REPORTE CABINA: "+mensajeError, usuario);
			}
		}catch(Exception e){
			log.error("Error WS SiniestrosWS generarReporteCabina", e);
		}
		
		return creacion;
	}
	
	@WebMethod(operationName = "busquedaIncisosPoliza")
	public IncisoBusquedaPoliza busquedaIncisosPoliza(
			@WebParam(name = "numeroPoliza"          )  String numeroPoliza,
			@WebParam(name = "tipoPoliza"            )  Integer tipoPoliza,
			@WebParam(name = "nombreAsegurado"       )  String nombreAsegurado, 
			@WebParam(name = "numeroSerie"           )  String numeroSerie, 
			@WebParam(name = "numeroInciso"          )  Integer numeroInciso, 
			@WebParam(name = "fechaOcurridoSiniestro")  Date fechaOcurridoSiniestro,
			@WebParam(name = "usr")                     String usr){
		
		IncisoBusquedaPoliza resultadoBusqueda = new IncisoBusquedaPoliza();
		
		String mensajeError = "";
		
		// VALIDACIONES
		
		try{
		
			if( StringUtil.isEmpty(numeroPoliza) && StringUtil.isEmpty(nombreAsegurado) && StringUtil.isEmpty(numeroSerie)  ){
				mensajeError +="Debe ingresar la Fecha de Ocurrido y usuario con al menos algún parámetro extra (número póliza, nombre asegurado, número de serie o número de inciso) para poder realizar la búsqueda";
			}else{
			
				if ( fechaOcurridoSiniestro == null ){
					mensajeError += "La Fecha Ocurrido Siniestro no puede ser nula /";
				}
				
				if ( !StringUtil.isEmpty(numeroSerie) && numeroSerie.length() > 30 ){
					mensajeError += "numeroSerie no puede ser mayor a 30 caracteres /";
				}
				
				if ( !StringUtil.isEmpty(nombreAsegurado) && nombreAsegurado.length() > 30 ){
					mensajeError += "nombreAsegurado no puede ser mayor a 100 caracteres /";
				}
				
				if ( StringUtil.isEmpty(usr) ){
					mensajeError += "usuario es OBLIGATORIO /";
				}else{
					Usuario usuarioWs = this.usuarioService.buscarUsuarioPorNombreUsuario(usr);
					if( usuarioWs == null ){
						mensajeError +="USUARIO no valido / ";
					}
				}
				
				if( !tipoPoliza.equals(0) && !tipoPoliza.equals(1) ) {
					mensajeError += "Indique el tipo de póliza a buscar 1-seycos 0-midas /";
				}
				
				if ( numeroInciso == 0 ) numeroInciso = null;
			
			}
			
			if(StringUtil.isEmpty(mensajeError) ) {
				List<IncisoPolizaDTO> lInncisos =  this.polizaSiniestroService.buscarIncisosPoliza(numeroPoliza, nombreAsegurado, numeroSerie, numeroInciso, fechaOcurridoSiniestro,Short.parseShort(tipoPoliza.toString()));
				resultadoBusqueda.setlIncisosPolizaDTO(lInncisos);
			}else{
				resultadoBusqueda.setMensajesError(mensajeError);
			}
		
		}catch(Exception e){
			mensajeError = e.getMessage();
			resultadoBusqueda.setMensajesError(mensajeError);
		}
		
		try{
			if( !StringUtil.isEmpty(mensajeError) ){
				// BITACORA	
				if ( StringUtil.isEmpty(usr) ){
					usr += "WSBusq";
				}
				bitacoraService.registrar(TIPO_BITACORA.REPORTE_CABINA, EVENTO.GENERAR_REPORTE_CABINA_WS, "0", "BUSQUEDA_POLIZA_WS", "ERROR AL BUSCAR LA POLIZA: "+mensajeError, usr);
			}
		}catch(Exception e){
			log.error("Error WS SiniestrosWS busquedaIncisosPoliza", e);
		}
		
		return resultadoBusqueda;
	}
	
	
	@WebMethod(operationName = "bienvenidaMidas")
	public String testConexion(){
		 return "Siniestros Seguros Afirme";
	}
	
	
	@WebMethod(operationName = "sincronizarReporte")
	public String asignarPolizaReporte( @WebParam(name = "complemento" ) ComplementoReporteDTO complementoReporteDto ){
		
		try {
			return this.reporteCabinaService.sincronizarReporteWS(complementoReporteDto);
		}catch(Exception e) {
			log.error("Error WS SiniestrosWS asignarPolizaReporte", e);
			return "Error al sincronzar reporte";
		}
		
	}
	
}
