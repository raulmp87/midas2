package mx.com.afirme.midas2.ws.endosos;

import java.math.BigDecimal;
import java.util.Date;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

import com.opensymphony.xwork2.Action;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.IncisoContinuity;
import mx.com.afirme.midas2.service.tarea.EmisionPendienteService;

@Stateless
@WebService(name = "EndososM2", targetNamespace = "https://segurosafirme.com.mx/endososM2")
public class EndosoPTAutomatico {

	@EJB
	private EmisionPendienteService emisionPendienteService;
	
	@WebMethod(operationName = "genEndosoPTAutomatico")
	public String genEndosoPTAutomatico(
			@WebParam(name = "idPoliza") BigDecimal idToPoliza,
			@WebParam(name = "tipoIndemnizacion") String tipoIndemnizacion,
			@WebParam(name = "fechaInicioVigencia") Date fechaInicioVigencia,
			@WebParam(name = "incisoContinuityId") Long incisoContinuityId,
			@WebParam(name = "idSiniestroPT") Long idSiniestroPT){
		
		String resultado = Action.SUCCESS;
		
		IncisoContinuity incisoContinuity = new IncisoContinuity();
		incisoContinuity.setId(incisoContinuityId);
		
		try{
			emisionPendienteService.generarEndosoPerdidaTotalAutomatico(idToPoliza, 
					tipoIndemnizacion, fechaInicioVigencia, 
					incisoContinuity, idSiniestroPT);			
		}catch(Exception e)
		{	
			resultado = Action.ERROR + " " + e.getMessage();						
		}		
		
		return resultado;
	}
}
