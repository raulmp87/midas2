package mx.com.afirme.midas2.interceptor;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJBException;
import javax.ejb.EJBTransactionRolledbackException;
import javax.persistence.OptimisticLockException;
import javax.transaction.RollbackException;
import javax.validation.ConstraintViolationException;

import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.exeption.ApplicationException;
import mx.com.afirme.midas2.exeption.NegocioEJBExeption;

import org.eclipse.persistence.exceptions.DatabaseException;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.js.util.StringUtil;
import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.Interceptor;


@Component
@Scope("prototype")
public class PersistenceErrorInterceptor implements Interceptor {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7450452155241848936L;
	
	private static Logger logger;

	@Override
	public void destroy() {
		// TODO Auto-generated method stub

	}

	@Override
	public void init() {
		// TODO Auto-generated method stub

	}

	@Override
	public String intercept(ActionInvocation invocation) throws Exception {
		
		Action action = ( Action ) invocation.getAction();
		
		try {
			
			return invocation.invoke(); 
			
		} 
		catch (Exception e) {
			if(action instanceof BaseAction){
				BaseAction baseAction = (BaseAction) action;
				if(StringUtil.isEmpty(baseAction.getMensaje())){
					baseAction.setMensajeError(BaseAction.MENSAJE_ERROR_GENERAL);
				}
				if(e instanceof EJBTransactionRolledbackException){
					EJBTransactionRolledbackException ejbTransactionRolledbackException = (EJBTransactionRolledbackException)e;
					RollbackException rollbackException = (RollbackException)ejbTransactionRolledbackException.getCause();
					DatabaseException databaseException = (DatabaseException)rollbackException.getCause();
					
					
					int errorCode = databaseException.getErrorCode();
					switch(errorCode){
						case 4002: 
							((BaseAction)action).setMensajeError(BaseAction.MENSAJE_ERROR_CONSTRAIN);
							break;
						case 5001:
						case 5003: 
							((BaseAction)action).setMensajeError(BaseAction.MENSAJE_ERROR_ELIMINAR);
							break;
						case 5004:
						case 5006:
							((BaseAction)action).setMensajeError(BaseAction.MENSAJE_ERROR_ACTUALIZAR);
							break;
						default: 
							((BaseAction)action).setMensajeError(BaseAction.MENSAJE_ERROR_GENERAL);
							break;
					}
					logger = Logger.getLogger("MidasPU");
					logger.log(Level.SEVERE, "MidasPU Failed, error code: "+ errorCode +" - Description: ", e.getCause());
					
				}else if(e instanceof NegocioEJBExeption){
					logger = Logger.getLogger("[NegocioEJBExeption] Exception");
					NegocioEJBExeption  negExp = null;
					try {
						String msg = "";
						negExp = (NegocioEJBExeption)e;
					
						msg = ((BaseAction)action).getText(negExp.getErrorCode());
						if(msg != null && !msg.equals(negExp.getErrorCode())){
							if(negExp.getValues() != null && negExp.getValues().length > 0)
								msg = String.format(msg, negExp.getValues());
						}else{
							msg= negExp.getMessageClean();
						}
						((BaseAction)action).setMensaje(msg);
						((BaseAction)action).setTipoMensaje(BaseAction.TIPO_MENSAJE_INFORMACION);
						logger.log(Level.SEVERE, "[NegocioEJBExeption]", e);
						
					} catch (Exception e2) {
						logger.log(Level.SEVERE, "[NegocioEJBExeption] System Failed", e);
					}	
				} else if (e instanceof EJBException) {
					Throwable cause = e.getCause();
					if (cause instanceof OptimisticLockException) {
						((BaseAction)action).setMensajeError(BaseAction.MENSAJE_ERROR_OPTIMISTIC_LOCK);
					} else if(cause instanceof ConstraintViolationException){
						ConstraintViolationException constraintViolationException = (ConstraintViolationException) cause;						
	                    baseAction.addErrors(constraintViolationException.getConstraintViolations());
	                    baseAction.setMensajeError("");
					} else if(cause instanceof ApplicationException) {
						ApplicationException applicationException = (ApplicationException) cause;
	                    baseAction.addErrors(applicationException);
	                    baseAction.setMensajeError("");
					}
				} else if (e instanceof ApplicationException){
					baseAction.addErrors((ApplicationException) e);					
					baseAction.setMensajeError("");
				} else{
					((BaseAction)action).setMensajeError(BaseAction.MENSAJE_ERROR_GENERAL);
					logger = Logger.getLogger("General Exception");
					logger.log(Level.SEVERE, "System Failed", e);
				}
			} 
			return Action.INPUT;			
		}
	}
	
	


}
