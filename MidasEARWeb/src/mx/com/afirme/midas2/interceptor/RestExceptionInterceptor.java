package mx.com.afirme.midas2.interceptor;

import javax.ejb.EJBException;
import javax.validation.ConstraintViolationException;

import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.exeption.ApplicationException;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.Interceptor;


@Component
@Scope("prototype")
public class RestExceptionInterceptor implements Interceptor {

	private static final long serialVersionUID = 7239038458566301120L;

	@Override
	public void destroy() {
		// TODO Auto-generated method stub

	}

	@Override
	public void init() {
		// TODO Auto-generated method stub

	}

	@Override
	public String intercept(ActionInvocation invocation) throws Exception {
		Action action = (Action) invocation.getAction();
		try {
			return invocation.invoke(); 
		} 
		catch (Throwable t) {
			if(action instanceof BaseAction){
				BaseAction baseAction = (BaseAction) action;
				if (t instanceof ApplicationException) {
                    baseAction.addErrors((ApplicationException) t);
				} else if (t instanceof EJBException) {
					Throwable cause = t.getCause();
					if(cause instanceof ConstraintViolationException){
						ConstraintViolationException constraintViolationException = (ConstraintViolationException) cause;						
	                    baseAction.addErrors(constraintViolationException.getConstraintViolations());
					} else if(cause instanceof ApplicationException) {
						ApplicationException applicationException = (ApplicationException) cause;
	                    baseAction.addErrors(applicationException);
					} else {
						baseAction.addActionError("Ocurrio un error inesperado.");
					}
				}
				else{
					baseAction.addActionError("Ocurrio un error inesperado.");
				}
			}
			return Action.INPUT;			
		}
	}
	
	


}
