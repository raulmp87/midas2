package mx.com.afirme.midas2.interceptor;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.ValidationAware;
import com.opensymphony.xwork2.interceptor.ExceptionHolder;
import com.opensymphony.xwork2.interceptor.ExceptionMappingInterceptor;

/**
 * This implementation adds a generic error message in the <code>actionError</code>.
 * @author amosomar
 */
public class CustomExceptionMappingInterceptor extends
		ExceptionMappingInterceptor {
	
	private static final long serialVersionUID = 6969811910761493069L;

	@Override
	protected void publishException(ActionInvocation invocation,
			ExceptionHolder exceptionHolder) {
		Object action = invocation.getAction();
		if (action instanceof ValidationAware) {
			ValidationAware validationAware = (ValidationAware) action;
			validationAware.setActionErrors(new ArrayList<String>());
			validationAware.setFieldErrors(new LinkedHashMap<String, List<String>>());
			validationAware.addActionError("Ocurrio un error inesperado.");
		}
	}

}
