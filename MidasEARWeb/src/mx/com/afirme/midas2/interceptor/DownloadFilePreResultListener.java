package mx.com.afirme.midas2.interceptor;

import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.PreResultListener;

public class DownloadFilePreResultListener implements PreResultListener {

	@Override
	public void beforeResult(ActionInvocation invocation, String resultCode) {
		
		try {
    	
			DownloadFileActionInterceptor actionInterceptor = new DownloadFileActionInterceptor();
	    	actionInterceptor.process(invocation, true);
	    	
		} catch (Exception e) {
			
			throw new RuntimeException(e);
			
		}
	
	}
	
	
}
