package mx.com.afirme.midas2.interceptor;

import java.lang.reflect.Method;

import mx.com.afirme.midas2.action.BaseAction;

import org.apache.struts2.convention.annotation.InterceptorRef;

import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.Interceptor;

public class DownloadFileActionInterceptor implements Interceptor {

	private static final long serialVersionUID = -6682256080432629382L;

	@Override
	public void destroy() {
		
	}

	@Override
	public void init() {
		
	}

	@Override
	public String intercept(ActionInvocation invocation) throws Exception {
		
		process(invocation, false);
		
		invocation.addPreResultListener(new DownloadFilePreResultListener());
				
		return invocation.invoke();
				
	}

	public void process(ActionInvocation invocation, Boolean isAfterInvoking) {
		
		BaseAction baseAction = null;
		String contextName = null;
		String downloadFileChannelPreffix = "/downloadFile/";
		String uuid = null;
		String fileExtension = null;
		Method methodIntercepted = null;
		InterceptorRef interceptorRef = null;
		org.apache.struts2.convention.annotation.Action actionAnnotation = null;
		Action action =  (Action) invocation.getAction();
		
		try {
		
			if (action instanceof BaseAction) {
							
				baseAction = (BaseAction) action;
							
				methodIntercepted = action.getClass().getMethod(invocation.getProxy().getMethod());
							
				actionAnnotation = methodIntercepted.getAnnotation(org.apache.struts2.convention.annotation.Action.class);
				
				for (InterceptorRef interceptor : actionAnnotation.interceptorRefs()) {
					
					if (interceptor.value().equals("downloadFileStack")) {
						
						interceptorRef = interceptor;
						break;
						
					}
					
				}
							
				// parameters (optional)
				//{"action":"start/end","uuid":"<uuid>", "name":"<context name>","ext":"<file extension>"}
				
				uuid = baseAction.getUuid();
				
				if (interceptorRef.params() != null && interceptorRef.params().length > 0) {
					
					contextName = ((interceptorRef.params()[0]!= null && !interceptorRef.params()[0].isEmpty())
							? interceptorRef.params()[0].trim() : "solicitud");
					
					if (interceptorRef.params().length > 1) {
						
						fileExtension = ((interceptorRef.params()[1]!= null && !interceptorRef.params()[1].isEmpty())
								? interceptorRef.params()[1].trim().toUpperCase() : "");
					
					}
					
				}
				
				if (isAfterInvoking) {
					
					baseAction.push("{\"action\":\"end\",\"uuid\":\"" + uuid + "\", \"name\":\"" + contextName + "\", \"ext\":\"" + fileExtension + "\"}", 
							downloadFileChannelPreffix + uuid);
					
				} else {
					
					baseAction.push("{\"action\":\"start\",\"uuid\":\"" + uuid + "\", \"name\":\"" + contextName + "\", \"ext\":\"" + fileExtension + "\"}", 
							downloadFileChannelPreffix + uuid);
				
				}
			
			}
			
		} catch(Exception ex) {
			
			throw new RuntimeException(ex);
			
		}
		
	}
	
}
