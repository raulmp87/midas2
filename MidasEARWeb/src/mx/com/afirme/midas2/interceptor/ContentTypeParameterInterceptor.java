package mx.com.afirme.midas2.interceptor;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.StrutsStatics;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.Interceptor;
import com.opensymphony.xwork2.interceptor.PreResultListener;

/**
 * This interceptor appends the suffix of the request parameter <code>contentType</code> to the
 * generated <code>result</code>. This should be the first interceptor in the stack.
 * 
 * @author amosomar
 *
 */
public class ContentTypeParameterInterceptor implements Interceptor {


	private static final long serialVersionUID = 4705474026119341466L;

	@Override
	public void destroy() {
	}

	@Override
	public void init() {
	}

	@Override
	public String intercept(ActionInvocation invocation) throws Exception {
		
		invocation.addPreResultListener(new PreResultListener() {
			
			@Override
			public void beforeResult(ActionInvocation invocation, String resultCode) {
				final ActionContext context = invocation.getInvocationContext();
				HttpServletRequest request = (HttpServletRequest) context.get(StrutsStatics.HTTP_REQUEST);
				String contentType = request.getParameter("contentType");
				
				if (contentType != null && contentType != "") {
					resultCode = resultCode + contentType;
				}				
				
				invocation.setResultCode(resultCode);
			}
		});
		
		return invocation.invoke();
	}

}
