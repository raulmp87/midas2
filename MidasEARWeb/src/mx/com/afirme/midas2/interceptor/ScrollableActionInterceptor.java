package mx.com.afirme.midas2.interceptor;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.HashMap;
import java.util.Map;

import mx.com.afirme.midas.base.Scrollable;
import mx.com.afirme.midas2.action.BaseAction;

import org.apache.struts2.convention.annotation.InterceptorRef;

import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.Interceptor;

public class ScrollableActionInterceptor implements Interceptor {

	private static final long serialVersionUID = -3096023763751582451L;

	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void init() {
		// TODO Auto-generated method stub
		
	}
	

	@Override
	public String intercept(ActionInvocation invocation) throws Exception {
		
		process(invocation, false);
		
		invocation.addPreResultListener(new ScrollPreResultListener());
				
		return invocation.invoke();
				
	}
	
	
	public void process(ActionInvocation invocation, Boolean isAfterInvoking) throws Exception {
		
		BaseAction baseAction = null;
		Scrollable scrollable = null;
		Method methodIntercepted = null;
		Field field = null;
		Field listField = null;
		Map<String, Object> context = null;
		InterceptorRef interceptorRef = null;
		org.apache.struts2.convention.annotation.Action actionAnnotation = null;
		Action action =  (Action) invocation.getAction();
				
		if (action instanceof BaseAction) {
						
			baseAction = (BaseAction) action;
						
			methodIntercepted = action.getClass().getMethod(invocation.getProxy().getMethod());
						
			actionAnnotation = methodIntercepted.getAnnotation(org.apache.struts2.convention.annotation.Action.class);
			
			for (InterceptorRef interceptor : actionAnnotation.interceptorRefs()) {
				
				if (interceptor.value().equals("scrollableStack")) {
					
					interceptorRef = interceptor;
					break;
					
				}
				
			}
							
			field = action.getClass().getDeclaredField(interceptorRef.params()[0]);
			field.setAccessible(true);
			scrollable = (Scrollable) field.get(action);
			
			if (isAfterInvoking) {
				
				//Se recupera el valor del total de registros en caso de no tenerlo
				copyNavigationProperties(scrollable, baseAction);
				
				//Se encuentra el listado de los objetos y se prepara para el resultado redirigido
				listField = action.getClass().getDeclaredField(interceptorRef.params()[1]);
				listField.setAccessible(true);
				context = new HashMap<String, Object>();
				
				context.put("scrollableList", listField.get(action));
				
				ActionContext.getContext().getValueStack().push(context);
				
				//Se redirige el resultado a scrollableGrid.jsp
				invocation.setResultCode("scrollableGrid");
				
			} else {
				
				copyNavigationProperties(baseAction, scrollable);
			
			}
		
		}
		
	}
	
	private void copyNavigationProperties(Object source, Object target) {
		
		Field sourceField;
		Field targetField;
		Class<?> sourceClass;
		Class<?> targetClass;
		
		try {
			
			if (source instanceof Scrollable) {
				sourceClass = Scrollable.class;
				targetClass = BaseAction.class;
			} else {
				sourceClass = BaseAction.class;
				targetClass = Scrollable.class;
			}
			
			
			for (Field field : Scrollable.class.getDeclaredFields()) {
				if (!Modifier.isFinal(field.getModifiers()) && !field.getName().startsWith("_")) {
					
					sourceField = sourceClass.getDeclaredField(field.getName());
					targetField = targetClass.getDeclaredField(field.getName());
					sourceField.setAccessible(true); 
					targetField.setAccessible(true);
					targetField.set(target, sourceField.get(source));
				}
			}
			
		} catch (SecurityException e) {
			throw new RuntimeException(e);
		} catch (NoSuchFieldException e) {
			throw new RuntimeException(e);
		} catch (IllegalArgumentException e) {
			throw new RuntimeException(e);
		} catch (IllegalAccessException e) {
			throw new RuntimeException(e);
		}
			
	}
	
}
