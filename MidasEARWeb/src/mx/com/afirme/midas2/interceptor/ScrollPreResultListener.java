package mx.com.afirme.midas2.interceptor;

import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.PreResultListener;

public class ScrollPreResultListener implements PreResultListener {

	@Override
	public void beforeResult(ActionInvocation invocation, String resultCode) {
		
		try {
    	
	    	ScrollableActionInterceptor actionInterceptor = new ScrollableActionInterceptor();
	    	actionInterceptor.process(invocation, true);
	    	
		} catch (Exception e) {
			
			throw new RuntimeException(e);
			
		}
	
	}
	
}
