package mx.com.afirme.midas2.excels;

import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


















import javax.ejb.EJB;

import mx.com.afirme.midas.catalogos.estilovehiculo.EstiloVehiculoDTO;
import mx.com.afirme.midas.catalogos.institucionbancaria.BancoEmisorDTO;
import mx.com.afirme.midas.consultas.formapago.FormaPagoDTO;
import mx.com.afirme.midas.consultas.mediopago.MedioPagoDTO;
import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionId;
import mx.com.afirme.midas.interfaz.cliente.ClienteDTO;
import mx.com.afirme.midas.interfaz.cliente.ClienteFacadeRemote;
import mx.com.afirme.midas.interfaz.cliente.ClienteGenericoDTO;
import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDTO;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.controlArchivo.ControlArchivoDN;
import mx.com.afirme.midas.sistema.controlArchivo.ControlArchivoDTO;
import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas.solicitud.SolicitudDTO;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoDTO;
import mx.com.afirme.midas2.action.poliza.seguroobligatorio.GeneraSeguroObligatorio;
import mx.com.afirme.midas2.clientesapi.dto.ClientesApiService;
import mx.com.afirme.midas2.clientesapi.dto.EntrevistaDTO;
import mx.com.afirme.midas2.clientesapi.dto.InterviewException;
import mx.com.afirme.midas2.domain.catalogos.Domicilio;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Ejecutivo;
import mx.com.afirme.midas2.domain.condicionesespeciales.CondicionEspecial;
import mx.com.afirme.midas2.domain.negocio.Negocio;
import mx.com.afirme.midas2.domain.negocio.derechos.NegocioDerechoPoliza;
import mx.com.afirme.midas2.domain.negocio.estadodescuento.NegocioEstadoDescuento;
import mx.com.afirme.midas2.domain.negocio.producto.formapago.NegocioFormaPago;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.NegocioTipoPoliza;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.NegocioSeccion;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.paquete.NegocioPaqueteSeccion;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.paquete.cobertura.deducibles.NegocioDeducibleCob;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.tipouso.NegocioTipoUso;
import mx.com.afirme.midas2.domain.persona.Persona;
import mx.com.afirme.midas2.domain.personadireccion.ColoniaMidas;
import mx.com.afirme.midas2.domain.poliza.seguroobligatorio.ParametroSeguroObligatorio;
import mx.com.afirme.midas2.domain.poliza.seguroobligatorio.PolizaAnexa;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.cargamasiva.DetalleCargaMasivaAutoCot;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.cargamasivaindividual.CargaMasivaIndividualAutoCot;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.cargamasivaindividual.DetalleCargaMasivaIndAutoCot;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.inciso.IncisoAutoCot;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.inciso.configuracion.ConfiguracionDatoInciso;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.inciso.configuracion.DatoIncisoCotAuto;
import mx.com.afirme.midas2.dto.excepcion.ExcepcionSuscripcionReporteDTO;
import mx.com.afirme.midas2.dto.fuerzaventa.RegistroFuerzaDeVentaDTO;
import mx.com.afirme.midas2.dto.persona.PersonaSeycosDTO;
import mx.com.afirme.midas2.dto.persona.PersonaSeycosFacadeRemote;
import mx.com.afirme.midas2.dto.persona.PersonaSeycosFacadeRemote.TipoDomicilio;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.ResumenCostosDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.TerminarCotizacionDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.cargamasiva.LogErroresCargaMasivaDTO;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.catalogos.banco.BancoMidasService;
import mx.com.afirme.midas2.service.personadireccion.DireccionMidasService;
import mx.com.afirme.midas2.service.poliza.renovacionmasiva.RenovacionMasivaService;
import mx.com.afirme.midas2.service.poliza.seguroobligatorio.SeguroObligatorioService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.calculo.CalculoService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.cargaMasiva.CargaMasivaService;
import mx.com.afirme.midas2.validators.NumSerieValidador;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.hssf.usermodel.DVConstraint;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFDataValidation;
import org.apache.poi.hssf.usermodel.HSSFName;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.DataValidationConstraint.ValidationType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.util.CellRangeAddressList;
import org.apache.poi.ss.util.CellReference;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("prototype")
public class GeneraExcelCargaMasivaIndividual  extends GenerarExcelBase {
	
	
	private int maxColumns 										= 100;
	private List<DetalleCargaMasivaIndAutoCot> detalleList 		= new ArrayList<DetalleCargaMasivaIndAutoCot>(1);
	private DetalleCargaMasivaIndAutoCot ejemplo 				= new DetalleCargaMasivaIndAutoCot();
	private Short claveTipo;
	private Long idUsuario;
	CreationHelper creationHelper = null;
	CellStyle dateStyleFV = null;
	private BancoMidasService bancoMidasService;
	private NegocioTipoPoliza negocioTipoPoliza;
	private CargaMasivaService cargaMasivaService;
	private ClienteFacadeRemote clienteFacadeRemote;
	private DireccionMidasService direccionMidasService;
	private PersonaSeycosFacadeRemote personaSeycosFacadeRemote;
	private Map<String, String> bancos;
	
	private BigDecimal idToNegocio;
	
	private static final int CAMPO_ESTATUS 								= 0;
	private static final int CAMPO_ID_AGENTE 							= 1;
	private static final int CAMPO_MONEDA 								= 2;
	private static final int CAMPO_ID_CENTRO_EMISOR 					= 3;
	private static final int CAMPO_ID_OFICINA 							= 4;
	private static final int CAMPO_NUMERO_COTIZACION 					= 5;
	private static final int CAMPO_NUMERO_POLIZA 						= 6;
	private static final int CAMPO_NUMERO_LIQUIDACION 					= 7;
	private static final int CAMPO_AUTORIZACION_PROSA 					= 8;
	private static final int CAMPO_FECHA_VIGENCIA_INICIO 				= 9;
	private static final int CAMPO_FECHA_EMISION 						= 10;
	private static final int CAMPO_FECHA_VIGENCIA_FIN 					= 11;
	private static final int CAMPO_LINEA_NEGOCIO 						= 12;
	private static final int CAMPO_ID_PAQUETE 							= 13;
	private static final int CAMPO_NUMERO_EMPLEADO 						= 14;
	private static final int CAMPO_TIPO_PERSONA_CLIENTE 				= 15;
	private static final int CAMPO_RFC_CLIENTE 							= 16;
	private static final int CAMPO_CLIENTE_VIP 							= 17;
	private static final int CAMPO_NOMBRE_RAZON_SOCIAL 					= 18;
	private static final int CAMPO_APELLIDO_PATERNO_CLIENTE 			= 19;
	private static final int CAMPO_APELLIDO_MATERNO_CLIENTE 			= 20;
	private static final int CAMPO_CODIGO_POSTAL_CLIENTE 				= 21;
	private static final int CAMPO_COLONIA_CLIENTE 						= 22;
	private static final int CAMPO_TELEFONO_CLIENTE 					= 23;
	private static final int CAMPO_CVE_AMIS 							= 24;
	private static final int CAMPO_MODELO 								= 25;
	private static final int CAMPO_NCI_REPUVE 							= 26;
	private static final int CAMPO_CVE_USO 								= 27;
	private static final int CAMPO_PLACAS 								= 28;
	private static final int CAMPO_FORMA_PAGO 							= 29;
	private static final int CAMPO_NUMERO_MOTOR 						= 30;
	private static final int CAMPO_NUMERO_SERIE 						= 31;
	private static final int CAMPO_VALOR_COMERCIAL 						= 32;
	private static final int CAMPO_TOTAL_PASAJEROS 						= 33;
	private static final int CAMPO_DESCRIPCION_VEHICULO 				= 34;
	private static final int CAMPO_PRIMA_TOTAL 							= 35;
	private static final int CAMPO_DEDUCIBLE_DANOS_MATERIALES 			= 36;
	private static final int CAMPO_DEDUCIBLE_ROBO_TOTAL 				= 37;
	private static final int CAMPO_LIMITE_RC_TERCEROS 					= 38;
	private static final int CAMPO_DEDUCIBLE_RC_TERCEROS 				= 39;
	private static final int CAMPO_LIMITE_GASTOS_MEDICOS 				= 40;
	private static final int CAMPO_LIMITE_MUERTE 						= 41;
	private static final int CAMPO_LIMITE_RC_VIAJERO 					= 42;
	private static final int CAMPO_ASISTENCIA_JURIDICA 					= 43;
	private static final int CAMPO_LIMITE_ADAPTACION_CONVERSION 		= 44;
	private static final int CAMPO_LIMITE_EQUIPO_ESPECIAL 				= 45;
	private static final int CAMPO_DEDUCIBLE_EQUIPO_ESPECIAL 			= 46;
	private static final int CAMPO_IGUALACION 							= 47;
	private static final int CAMPO_DERECHOS 							= 48;
	private static final int CAMPO_SOLICITAR_AUTORIZACION 				= 49;
	private static final int CAMPO_CAUSA_AUTORIZACION 					= 50;
	private static final int CAMPO_DESCRIPCION_EQUIPO_ESPECIAL 			= 51;
	private static final int CAMPO_DESCRIPCION_ADAPTACION_CONVERSION 	= 52;
	private static final int CAMPO_NOMBRE 								= 53;
	private static final int CAMPO_APELLIDO_PATERNO 					= 54;
	private static final int CAMPO_APELLIDO_MATERNO 					= 55;
	private static final int CAMPO_NUMERO_LICENCIA 						= 56;
	private static final int CAMPO_FECHA_NACIMIENTO 					= 57;
	private static final int CAMPO_OCUPACION 							= 58;
	private static final int CAMPO_IDCLIENTE 							= 59;
	private static final int CAMPO_NUMERO_REMOLQUES 					= 60;
	private static final int CAMPO_TIPO_CARGA 							= 61;
	private static final int CAMPO_OBSERVACIONES_INCISO 				= 62;
	private static final int CAMPO_NOMBRE_ASEGURADO 					= 63;
	private static final int CAMPO_LIMITE_ACCIDENTES_CONDUCTOR 			= 64;
	private static final int CAMPO_RPF 									= 65;
	private static final int CAMPO_SUMA_ASEGURADA_DANOS_MATERIALES 		= 66;
	private static final int CAMPO_SUMA_ASEGURADA_ROBO_TOTAL 			= 67;
	private static final int CAMPO_EMAIL 								= 68;
	private static final int CAMPO_ALTA_CONDICION_ESPECIAL				= 69;
	private static final int CAMPO_BAJA_CONDICION_ESPECIAL				= 70;
	private static final int CAMPO_ERROR_NUMERO_SERIE 					= 71;
	private static final int CAMPO_ERROR_EXCEPCIONES 					= 72;
	private static final int CAMPO_CALLE_CLIENTE						= 73;
	private static final int CAMPO_ID_REPRESENTANTE						= 74;
	private static final int CAMPO_TIPO_PERSONA_REPRESENTANTE			= 75;
	private static final int CAMPO_RFC_REPRESENTANTE					= 76;
	private static final int CAMPO_NOMBRE_RAZON_SOCIAL_REPRESENTANTE	= 77;
	private static final int CAMPO_APELLIDO_PATERNO_REPRESENTANTE		= 78;
	private static final int CAMPO_APELLIDO_MATERNO_REPRESENTANTE		= 79;
	private static final int CAMPO_CODIGO_POSTAL_REPRESENTANTE			= 80;
	private static final int CAMPO_COLONIA_REPRESENTANTE				= 81;
	private static final int CAMPO_TELEFONO_REPRESENTANTE				= 82;
	private static final int CAMPO_CALLE_REPRESENTANTE					= 83;
	private static final int CAMPO_ENTREVISTA_PRIMA_ESTIMADA = 84;
	private static final int CAMPO_ENTREVISTA_MONEDA = 85;
	private static final int CAMPO_ENTREVISTA_NUMERO_FIEL = 86;
	private static final int CAMPO_ENTREVISTA_PEP= 87;
	private static final int CAMPO_ENTREVISTA_PARENTESCO = 88;
	private static final int CAMPO_ENTREVISTA_NOMBRE_PUESTO = 89;
	private static final int CAMPO_ENTREVISTA_PERIODO_FUNCIONES = 90;
	private static final int CAMPO_ENTREVISTA_CUOTA_PROPIA = 91;
	private static final int CAMPO_ENTREVISTA_NOMBRE_PARENTESCO = 92;
	private static final int CAMPO_ENTREVISTA_DOCUMENTACION_REPRESENTACIOM = 93;
	private static final int CAMPO_ENTREVISTA_FOLIO_MERCANTIL = 94;
	private static final int CAMPO_CONDUCTO_COBRO = 95;
	private static final int CAMPO_INSTITUCION_BANCARIA = 96;
	private static final int CAMPO_TIPO_TARJETA = 97;
	private static final int CAMPO_NUMERO_TARJETA = 98;
	private static final int CAMPO_CODIGO_SEGURIDAD = 99;
	private static final int CAMPO_FECHA_VENCIMIENTO = 100;
	
	private static final String NOMBRE_CAMPO_NUMERO_REMOLQUES 			= "NUMERO REMOLQUES";
	private static final String NOMBRE_CAMPO_TIPO_CARGA 				= "TIPO CARGA";
	private static final String NOMBRE_CAMPO_OBSERVACIONES_INCISO 		= "OBSERVACIONES";
	
	public static final String VALOR_VISA = "VISA";
	public static final String VALOR_MASTERCARD = "MASTERD CARD";
	public static final String VALOR_TC= "TARJETA DE CREDITO";
	public static final String VALOR_DOMICILIACION= "COBRANZA DOMICILIADA";
	public static final String VALOR_EFECTIVO= "EFECTIVO";
	public static final String VALOR_CUENTAAFIRME= "CUENTA AFIRME (CREDITO O DEBITO)";

	private static final String URL_PLANTILLA 		= "/mx/com/afirme/midas2/impresiones/jrxml/plantillaCargaIndividual.xls";
	private ClientesApiService  clienteRest;
	private static final int HOJA_1 					= 0;
	private static final int HOJA_2 					= 1;
	private static final int HOJA_3 					= 2;
	
	private static final int TAMANO_MIN_ELEMENTOS		= 1;
	/**
	 * El nombre de la lista de valores utilizado par las validaciones del campo TIPO CARGA.
	 */
	private static final String CAMPO_TIPO_CARGA_NOMBRE_RANGO = "TP_TIPO_CARGA";
	private static final String MEDIO_PAGO_TC = "4";
	private static final String MEDIO_PAGO_DOMICILIACION = "8";
	private static final String MEDIO_PAGO_EFECTIVO = "15";
	private static final String MEDIO_PAGO_CUENTA_AFIRME= "1386";
	
	private static final String TIPO_CUENTA_DEBITO = "DEBITO";
	private static final String TIPO_CUENTA_CUENTA_CLABE = "CUENTA CLABE";
	private static final String TIPO_CUENTA_CUENTA_BANCARIA = "CUENTA BANCARIA";

	public static final String PESOS = "NACIONAL";
	public static final String DOLARES = "DOLARES";
	
	public static final String FISICA = "FISICA";
	public static final String MORAL = "MORAL";
	
	public static final String VALOR_SI = "SI";
	public static final String VALOR_NO = "NO";	
	
	//Seguro obligatorio
	private RenovacionMasivaService renovacionMasivaService;
	private SeguroObligatorioService seguroObligatorioService;
	
	@EJB
	private ListadoService listadoService;
	
	private CalculoService calculoService;
	
	public void setRenovacionMasivaService(RenovacionMasivaService renovacionMasivaService) {
		this.renovacionMasivaService = renovacionMasivaService;
	}
	
	public void setSeguroObligatorioService(SeguroObligatorioService seguroObligatorioService) {
		this.seguroObligatorioService = seguroObligatorioService;
	}

	public void setListadoService(ListadoService listadoService) {
		this.listadoService = listadoService;
	}

	public void setCalculoService(CalculoService calculoService) {
		this.calculoService = calculoService;
	}	
	
	
	public GeneraExcelCargaMasivaIndividual(CargaMasivaService cargaMasivaService,ClientesApiService clienteRest) {
		this.setCargaMasivaService(cargaMasivaService);
		this.clienteRest=clienteRest;
		//uploadFolder = cargaMasivaService.getUploadFolder();
		String OSName = System.getProperty("os.name");
		if (OSName.toLowerCase().indexOf("windows")!=-1)
			uploadFolder = Sistema.UPLOAD_FOLDER;
		else
			uploadFolder = Sistema.LINUX_UPLOAD_FOLDER; //UPLOAD_FOLDER;  //LINUX_UPLOAD_FOLDER;
	}
	
	

	public InputStream generaPlantillaCargaMasiva( ListadoService listadoService )throws IOException {
		
		HSSFWorkbook workbook 				= null;
		HSSFSheet validaciones 				= null;
		HSSFSheet datos 					= null;
		HSSFSheet condicionesEspeciales		= null;
		ByteArrayOutputStream bos 			= null;
		short firstCell 					= 0;
		this.listadoService = listadoService;
		
		try {
			// Obtiene plantilla con macros
			workbook = cargaPlantilla( URL_PLANTILLA );
			creationHelper = workbook.getCreationHelper();
			dateStyleFV = workbook.createCellStyle();
			dateStyleFV.setDataFormat(creationHelper.createDataFormat().getFormat("mm/yy"));
			
			validaciones 			= workbook.getSheetAt(HOJA_1);
			datos 					= workbook.getSheetAt(HOJA_2);
			condicionesEspeciales 	= workbook.getSheetAt(HOJA_3);

			buildDataSheet(datos);
			datos.protectSheet("midas");

			buildValidationsSheet(validaciones);
		
			createCondicionesSheet(condicionesEspeciales, idToNegocio);

			// Ajusta tamano de columnas
			for (int i = 0; i <= maxColumns; i++) {
					validaciones.autoSizeColumn(i);
			}
			
			/*for (int i = 0; i <= 5; i++) {
				condicionesEspeciales.autoSizeColumn(i);
			}*/

			// Oculta hoja de datos
			workbook.setSheetHidden(HOJA_2, true);
			// Muestra primera hoja
			workbook.setActiveSheet(HOJA_1);
			validaciones.showInPane(firstCell, firstCell);

			//fos = new FileOutputStream(uploadFolder + "CargaMasivaInd" + negocioTipoPoliza.getIdToNegTipoPoliza() + ".xls");
			//bos = new BufferedOutputStream(fos);
			bos = new ByteArrayOutputStream();

			workbook.write(bos);

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (bos != null) {
				bos.close();
			}
		}

		//File file = new File(uploadFolder + "CargaMasivaInd" + negocioTipoPoliza.getIdToNegTipoPoliza() + ".xls");
		//return new FileInputStream(file);
		if(bos == null){
			return null;
		}
		return new ByteArrayInputStream(bos.toByteArray());
	}
	
	
	private void armaDerechos(HSSFSheet sheet){
		HSSFRow row = null;
		HSSFCell cell = null;
		HSSFName name = null;
		List<NegocioDerechoPoliza> list = cargaMasivaService.getNegocioDerechoPolizaByNegTipoPoliza(negocioTipoPoliza);
		
		if (list != null && !list.isEmpty()) {
			row = sheet.createRow(dataRows);
			dataRows++;
			int linea = dataRows;
			int column = 0;

			Double importeDefault = new Double(0.0);
			for (NegocioDerechoPoliza item : list) {
				cell = row.createCell(column);
				cell.setCellValue(item.getImporteDerecho());
				column++;
				
				importeDefault = item.getImporteDerecho();
				if(item.getClaveDefault()){
					ejemplo.setDerechos(item.getImporteDerecho());
				}
				
			}
			if(ejemplo.getDerechos() == null){
				ejemplo.setDerechos(importeDefault);
			}
			CellReference cellRef = new CellReference(dataRows, (column - 1));

			name = sheet.getWorkbook().createName();
			name.setNameName("DERECHOS");
			String rango = "$A$" + linea + ":$" + cellRef.getCellRefParts()[2]
					+ "$" + linea;
			name.setRefersToFormula("'DataCombo'!" + rango + "");

		}
	}
	
	private void armaFormasPago(HSSFSheet sheet){
		HSSFRow row = null;
		HSSFCell cell = null;
		HSSFName name = null;
		List<NegocioFormaPago> list = cargaMasivaService.getNegocioFormaPagoByNegTipoPoliza(negocioTipoPoliza);
		
		if (list != null && !list.isEmpty()) {
			row = sheet.createRow(dataRows);
			dataRows++;
			int linea = dataRows;
			int column = 0;
			boolean isFirst = true;
			for (NegocioFormaPago item : list) {
				cell = row.createCell(column);
				cell.setCellValue(item.getFormaPagoDTO().getDescripcion());
				column++;
				
				if(isFirst){
					ejemplo.setFormaPago(item.getFormaPagoDTO().getDescripcion());
					isFirst = false;
				}
				
			}
			CellReference cellRef = new CellReference(dataRows, (column - 1));

			name = sheet.getWorkbook().createName();
			name.setNameName("FORMASPAGO");
			String rango = "$A$" + linea + ":$" + cellRef.getCellRefParts()[2]
					+ "$" + linea;
			name.setRefersToFormula("'DataCombo'!" + rango + "");

		}
	}
	
	private void armaNegocioSeccion(HSSFSheet sheet){
		HSSFRow row = null;
		HSSFCell cell = null;
		HSSFName name = null;
		List<NegocioSeccion> list = cargaMasivaService.getNegocioSeccionByNegTipoPoliza(negocioTipoPoliza);
		
		if (list != null && !list.isEmpty()) {
			row = sheet.createRow(dataRows);
			dataRows++;
			int linea = dataRows;
			int column = 0;
			boolean isFirst = true;
			for (NegocioSeccion item : list) {
				cell = row.createCell(column);
				cell.setCellValue(remplazaEspacios(item.getSeccionDTO().getDescripcion()));
				column++;
				
				armaTipoUso(sheet, item, isFirst);
				armaPaquete(sheet, item, isFirst);
				
				if(isFirst){
					ejemplo.setLineaNegocio(remplazaEspacios(item.getSeccionDTO().getDescripcion()));
					isFirst = false;
				}
			}
			CellReference cellRef = new CellReference(dataRows, (column - 1));

			name = sheet.getWorkbook().createName();
			name.setNameName("NEGOCIOSECCION");
			String rango = "$A$" + linea + ":$" + cellRef.getCellRefParts()[2]
					+ "$" + linea;
			name.setRefersToFormula("'DataCombo'!" + rango + "");

		}
	}
	
	private void armaTipoCarga(HSSFSheet sheet) {
		HSSFRow row = null;
		HSSFCell cell = null;
		HSSFName name = null;

		List<CatalogoValorFijoDTO> tipoCargas = cargaMasivaService.getTipoCargas();
		if (!tipoCargas.isEmpty()) {
			row = sheet.createRow(dataRows);
			dataRows++;
			int column = 0;
			int linea = dataRows;

			for (CatalogoValorFijoDTO item2 : tipoCargas) {
				cell = row.createCell(column);
				cell.setCellValue(item2.getDescripcion());
				
				column++;
			}
			CellReference cellRef = new CellReference(dataRows, (column - 1));

			name = sheet.getWorkbook().createName();
			name.setNameName(CAMPO_TIPO_CARGA_NOMBRE_RANGO);

			String rango = "$A$" + linea + ":$" + cellRef.getCellRefParts()[2]
					+ "$" + linea;
			name.setRefersToFormula("'DataCombo'!" + rango + "");
		}
	}
	
	private void armaTipoUso(HSSFSheet sheet, NegocioSeccion negocioSeccion, boolean isFirst) {
		HSSFRow row = null;
		HSSFCell cell = null;
		HSSFName name = null;

		SeccionDTO seccion = cargaMasivaService.getSeccionDTOById(negocioSeccion);
		List<NegocioTipoUso> negocioTipoUsoList = cargaMasivaService.getNegocioTipoUsoListByNegSeccion(negocioSeccion);
		if (negocioTipoUsoList != null && !negocioTipoUsoList.isEmpty()) {
			row = sheet.createRow(dataRows);
			dataRows++;
			int column = 0;
			int linea = dataRows;

			for (NegocioTipoUso item2 : negocioTipoUsoList) {
				cell = row.createCell(column);
				cell.setCellValue(item2.getTipoUsoVehiculoDTO()
						.getDescripcionTipoUsoVehiculo());

				column++;
				
				if(isFirst){
					ejemplo.setClaveUso(item2.getTipoUsoVehiculoDTO()
							.getDescripcionTipoUsoVehiculo());
					isFirst = false;
				}
			}
			CellReference cellRef = new CellReference(dataRows, (column - 1));

			name = sheet.getWorkbook().createName();
			String nombre = "TP_" + seccion.getDescripcion();
			name.setNameName(remplazaEspacios(nombre).toUpperCase());

			String rango = "$A$" + linea + ":$" + cellRef.getCellRefParts()[2]
					+ "$" + linea;
			name.setRefersToFormula("'DataCombo'!" + rango + "");

		}
	}
	
	private void armaPaquete(HSSFSheet sheet, NegocioSeccion negocioSeccion, boolean isFirst){
		HSSFRow row = null;
		HSSFCell cell = null;
		HSSFName name = null;

		SeccionDTO seccion = cargaMasivaService.getSeccionDTOById(negocioSeccion);
		List<NegocioPaqueteSeccion> negocioPaqueteSeccionList = cargaMasivaService.getNegocioPaqueteSeccionByNegSeccion(negocioSeccion);
		if (negocioPaqueteSeccionList != null
				&& !negocioPaqueteSeccionList.isEmpty()) {
			row = sheet.createRow(dataRows);
			dataRows++;
			int column = 0;
			int linea = dataRows;

			for (NegocioPaqueteSeccion item2 : negocioPaqueteSeccionList) {
				cell = row.createCell(column);
				cell.setCellValue(item2.getPaquete().getDescripcion());
				column++;
				
				if(isFirst){
					ejemplo.setPaquete(item2.getPaquete().getDescripcion());
					isFirst = false;
				}
			}
			CellReference cellRef = new CellReference(dataRows, (column - 1));

			name = sheet.getWorkbook().createName();
			String nombre = "P_" + seccion.getDescripcion();
			name.setNameName(remplazaEspacios(nombre).toUpperCase());

			String rango = "$A$" + linea + ":$" + cellRef.getCellRefParts()[2]
					+ "$" + linea;
			name.setRefersToFormula("'DataCombo'!" + rango + "");

		}
	}
	
	private void armaCentroEmisor(HSSFSheet sheet){
		HSSFRow row = null;
		HSSFCell cell = null;
		HSSFName name = null;
		List<RegistroFuerzaDeVentaDTO> centroEmisorList = cargaMasivaService.getCentroEmisores();
		
		if (centroEmisorList != null && !centroEmisorList.isEmpty()) {
			row = sheet.createRow(dataRows);
			dataRows++;
			int linea = dataRows;
			int column = 0;

			for (RegistroFuerzaDeVentaDTO item : centroEmisorList) {
				cell = row.createCell(column);
				cell.setCellValue(item.getDescription());
				column++;
				
			}
			CellReference cellRef = new CellReference(dataRows, (column - 1));

			name = sheet.getWorkbook().createName();
			name.setNameName("CENTROEMISOR");
			String rango = "$A$" + linea + ":$" + cellRef.getCellRefParts()[2]
					+ "$" + linea;
			name.setRefersToFormula("'DataCombo'!" + rango + "");

		}
	}
	
	private void armaEjecutivo(HSSFSheet sheet){
		HSSFRow row = null;
		HSSFCell cell = null;
		HSSFName name = null;
		List<Ejecutivo> ejecutivosList = cargaMasivaService.getEjecutivos();
		
		if (ejecutivosList != null && !ejecutivosList.isEmpty()) {
			row = sheet.createRow(dataRows);
			dataRows++;
			int linea = dataRows;
			int column = 0;
			boolean isFirst = true;

			for (Ejecutivo ejecutivo : ejecutivosList) {
				if(ejecutivo!=null && ejecutivo.getClaveEstatus()==1L){
					Persona responsable=(ejecutivo.getPersonaResponsable()!=null && ejecutivo.getPersonaResponsable().getIdPersona()!=null)?ejecutivo.getPersonaResponsable():null;
					String nombre=(responsable!=null)?responsable.getNombreCompleto():null;
					cell = row.createCell(column);
					cell.setCellValue(nombre);
					column++;
					if(isFirst){
						ejemplo.setIdOficina(nombre);
						isFirst = false;
					}					
				}
				
			}
			CellReference cellRef = new CellReference(dataRows, (column - 1));

			name = sheet.getWorkbook().createName();
			name.setNameName("OFICINAS");
			String rango = "$A$" + linea + ":$" + cellRef.getCellRefParts()[2]
					+ "$" + linea;
			name.setRefersToFormula("'DataCombo'!" + rango + "");

		}
	}
	

	
	private void llenaLineaEjemplo(HSSFSheet sheet){
		HSSFRow row = sheet.getRow(1);
		if(row == null){
			row = sheet.createRow(1);
		}
		HSSFCell cell = row.createCell(CAMPO_ID_OFICINA);
		cell.setCellValue(ejemplo.getIdOficina());
		cell = row.createCell(CAMPO_LINEA_NEGOCIO);
		cell.setCellValue(ejemplo.getLineaNegocio());
		cell = row.createCell(CAMPO_ID_PAQUETE);
		cell.setCellValue(ejemplo.getPaquete());
		cell = row.createCell(CAMPO_CVE_USO);
		cell.setCellValue(ejemplo.getClaveUso());
		cell = row.createCell(CAMPO_FORMA_PAGO);
		cell.setCellValue(ejemplo.getFormaPago());
		cell = row.createCell(CAMPO_DERECHOS);
		cell.setCellValue(ejemplo.getDerechos());
		cell = row.createCell(CAMPO_FECHA_VIGENCIA_INICIO);
		cell.setCellValue(new Date());
		cell = row.createCell(CAMPO_FECHA_VIGENCIA_FIN);
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(new Date());
		calendar.add(Calendar.YEAR, 1);
		cell.setCellValue(calendar.getTime());
		cell = row.createCell(CAMPO_CONDUCTO_COBRO);
		cell.setCellValue("EFECTIVO");
		cell = row.createCell(CAMPO_INSTITUCION_BANCARIA);
		cell.setCellValue("AFIRME");
		cell = row.createCell(CAMPO_TIPO_TARJETA);
		cell.setCellValue("VISA");
		cell = row.createCell(CAMPO_NUMERO_TARJETA);
		cell.setCellValue("");
		cell = row.createCell(CAMPO_CODIGO_SEGURIDAD);
		cell.setCellValue("");
		cell = row.createCell(CAMPO_FECHA_VENCIMIENTO);
		cell.setCellValue("");
	}


	
	public void validaCargaMasiva(BigDecimal idControlArchivo, Short tipoCarga, Short claveTipo, BancoMidasService bancoMidasService) {

		try {
			this.tipoCarga = tipoCarga;
			this.claveTipo = claveTipo;
			this.bancoMidasService = bancoMidasService;

			ControlArchivoDN controlArchivoDN = ControlArchivoDN.getInstancia();
			controlArchivoDTO = controlArchivoDN.getPorId(idControlArchivo);
			String fileName = this.obtenerNombreArchivo(controlArchivoDTO);

			HSSFWorkbook workbook = this.cargaArchivodeCarga(uploadFolder
					+ fileName);
			HSSFSheet validaciones = workbook.getSheetAt(0);
			leeLineasArchivoCarga(validaciones);


			FileOutputStream fos = new FileOutputStream(uploadFolder + fileName);
			BufferedOutputStream bos = new BufferedOutputStream(fos);

			workbook.write(bos);
			
			bos.flush();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (SystemException e) {
			e.printStackTrace();
		}
	}
	
	private DetalleCargaMasivaIndAutoCot creaLineaDetalleCarga(HSSFRow hssfRow) {

		DetalleCargaMasivaIndAutoCot detalle = new DetalleCargaMasivaIndAutoCot();
		Boolean datosConductorObligatorios = false;
		DataFormatter formatter = new DataFormatter();
		EntrevistaDTO entrevista = new EntrevistaDTO();
		
		int fila = hssfRow.getRowNum() + 1;
		StringBuilder mensajeError = new StringBuilder("");
		String lineaDeNegocio = "";
		Short uno = 1;
		boolean lineaVacia = true;
		for (int i = 0; i <= maxColumns; i++) {
			HSSFCell cell = hssfRow.getCell(i);
			LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
			switch (i) {
			case CAMPO_ESTATUS:
				break;
			case CAMPO_ID_AGENTE:
				if (cell != null && !cell.toString().isEmpty()) {
					detalle.setIdAgente(cell.toString());
					lineaVacia = false;
				}else {
					error.setNumeroInciso(new BigDecimal(fila));
					error.setNombreSeccion(lineaDeNegocio);
					error.setNombreCampoError("AGENTE");
					error.setRecomendaciones("Ingrese un Agente");
					listErrores.add(error);
					mensajeError.append("Campo: ").append(error.getNombreCampoError()).append(" - ").append(error.getRecomendaciones()).append("\n");
				}
				break;
			case CAMPO_MONEDA:
				if (cell != null && !cell.toString().isEmpty()) {
					detalle.setMoneda(cell.toString());
					lineaVacia = false;
				}else {
					error.setNumeroInciso(new BigDecimal(fila));
					error.setNombreSeccion(lineaDeNegocio);
					error.setNombreCampoError("MONEDA");
					error.setRecomendaciones("Seleccione una Moneda");
					listErrores.add(error);
					mensajeError.append("Campo: ").append(error.getNombreCampoError()).append(" - ").append(error.getRecomendaciones()).append("\n");
				}
				break;
			case CAMPO_ID_CENTRO_EMISOR:
				if (cell != null && !cell.toString().isEmpty()) {
					detalle.setIdCentroEmisor(cell.toString());
					lineaVacia = false;
				}else {
					error.setNumeroInciso(new BigDecimal(fila));
					error.setNombreSeccion(lineaDeNegocio);
					error.setNombreCampoError("CENTRO EMISOR");
					error.setRecomendaciones("Seleccione un Centro Emisor");
					listErrores.add(error);
					mensajeError.append("Campo: ").append(error.getNombreCampoError()).append(" - ").append(error.getRecomendaciones()).append("\n");
				}
				break;
			case CAMPO_ID_OFICINA:
				if (cell != null && !cell.toString().isEmpty()) {
					detalle.setIdOficina(cell.toString());
					lineaVacia = false;
				}else {
					error.setNumeroInciso(new BigDecimal(fila));
					error.setNombreSeccion(lineaDeNegocio);
					error.setNombreCampoError("EJECUTIVO");
					error.setRecomendaciones("Ingrese una Ejecutivo");
					listErrores.add(error);
					mensajeError.append("Campo: ").append(error.getNombreCampoError()).append(" - ").append(error.getRecomendaciones()).append("\n");
				}
				break;
			case CAMPO_NUMERO_COTIZACION:
				if(claveTipo.equals(CargaMasivaIndividualAutoCot.TIPO_EMISION)){
				if (cell != null && !cell.toString().isEmpty()) {
					try{
					detalle.setNumeroCotizacion(new BigDecimal(cell.toString()));
					lineaVacia = false;
					try{
						CotizacionDTO cotizacion = cargaMasivaService.getCotizacionByDetalleCargaMasivaIndAutoCot(detalle.getNumeroCotizacion());
						if(cotizacion == null){
							error.setNumeroInciso(new BigDecimal(fila));
							error.setNombreSeccion(lineaDeNegocio);
							error.setNombreCampoError("NUMERO DE COTIZACION");
							error.setRecomendaciones("Valor Invalido");
							listErrores.add(error);
							mensajeError.append("Campo: ").append(error.getNombreCampoError()).append(" - ").append(error.getRecomendaciones()).append("\r\n").append(System.getProperty("line.separator"));
						}else{
							detalle.setIdToNegocio( cotizacion.getNegocioDerechoPoliza().getNegocio().getIdToNegocio()  );
							IncisoCotizacionDTO inciso = cargaMasivaService.obtieneIncisoCotizacion(cotizacion, new BigDecimal(1));
							if (inciso != null) {
								detalle.setDescripcionVehiculo(inciso.getIncisoAutoCot().getDescripcionFinal());
								datosConductorObligatorios = cargaMasivaService.datosDelConductorRequeridos(inciso);
							}
						}
					}catch(Exception e){
						e.printStackTrace();
					}
					}catch(Exception e){
						error.setNumeroInciso(new BigDecimal(fila));
						error.setNombreSeccion(lineaDeNegocio);
						error.setNombreCampoError("NUMERO DE COTIZACION");
						error.setRecomendaciones("Valor Invalido");
						listErrores.add(error);
						mensajeError.append("Campo: ").append(error.getNombreCampoError()).append(" - ").append(error.getRecomendaciones()).append("\r\n").append(System.getProperty("line.separator"));
					}
				}else {
					error.setNumeroInciso(new BigDecimal(fila));
					error.setNombreSeccion(lineaDeNegocio);
					error.setNombreCampoError("NUMERO DE COTIZACION");
					error.setRecomendaciones("Ingrese un Numero de Cotizacion");
					listErrores.add(error);
					mensajeError.append("Campo: ").append(error.getNombreCampoError()).append(" - ").append(error.getRecomendaciones()).append("\n");
				}
				}
				break;
			case CAMPO_NUMERO_POLIZA:
				/*
				if (cell != null && !cell.toString().isEmpty()) {
					detalle.setNumeroPoliza(new BigDecimal(cell.toString()));
				}else {
					error.setNumeroInciso(new BigDecimal(fila));
					error.setNombreSeccion(lineaDeNegocio);
					error.setNombreCampoError("NUMERO DE POLIZA");
					error.setRecomendaciones("Ingrese un Numero de Poliza");
					listErrores.add(error);
					mensajeError += "Campo: " + error.getNombreCampoError() + " - " + error.getRecomendaciones() + "\n";
				}*/
				break;
			case CAMPO_NUMERO_LIQUIDACION:
				if (cell != null && !cell.toString().isEmpty()) {
					try{
					detalle.setNumeroLiquidacion(new BigDecimal(cell.toString()));
					lineaVacia = false;
					}catch(Exception e){
					}
				}
				break;
			case CAMPO_AUTORIZACION_PROSA:
				if (cell != null && !cell.toString().isEmpty()) {
					detalle.setAutorizacionProsa(cell.toString().toUpperCase());
					lineaVacia = false;
				}
				break;
			case CAMPO_FECHA_VIGENCIA_INICIO:
				if (cell != null && !cell.toString().isEmpty()) {
					try{
						detalle.setFechaVigenciaInicio(cell.getDateCellValue());
						lineaVacia = false;
					}catch(Exception e){
						error.setNumeroInciso(new BigDecimal(fila));
						error.setNombreSeccion(lineaDeNegocio);
						error.setNombreCampoError("FECHA VIGENCIA INICIO");
						error.setRecomendaciones("Valor Invalido");
						listErrores.add(error);
						mensajeError.append("Campo: ").append(error.getNombreCampoError()).append(" - ").append(error.getRecomendaciones()).append("\r\n").append(System.getProperty("line.separator"));
					}
				}else {
					error.setNumeroInciso(new BigDecimal(fila));
					error.setNombreSeccion(lineaDeNegocio);
					error.setNombreCampoError("FECHA VIGENCIA INICIO");
					error.setRecomendaciones("Ingrese la Fecha de Vigencia Inicio");
					listErrores.add(error);
					mensajeError.append("Campo: ").append(error.getNombreCampoError()).append(" - ").append(error.getRecomendaciones()).append("\n");
				}
				break;
			case CAMPO_FECHA_EMISION:
				break;
			case CAMPO_FECHA_VIGENCIA_FIN:
				if (cell != null && !cell.toString().isEmpty()) {
					try{
						detalle.setFechaVigenciaFin(cell.getDateCellValue());
						lineaVacia = false;
					}catch(Exception e){
						error.setNumeroInciso(new BigDecimal(fila));
						error.setNombreSeccion(lineaDeNegocio);
						error.setNombreCampoError("FECHA VIGENCIA FIN");
						error.setRecomendaciones("Valor Invalido");
						listErrores.add(error);
						mensajeError.append("Campo: ").append(error.getNombreCampoError()).append(" - ").append(error.getRecomendaciones()).append("\r\n").append(System.getProperty("line.separator"));
					}
				}else {
					error.setNumeroInciso(new BigDecimal(fila));
					error.setNombreSeccion(lineaDeNegocio);
					error.setNombreCampoError("FECHA VIGENCIA FIN");
					error.setRecomendaciones("Ingrese la Fecha Vigencia Fin");
					listErrores.add(error);
					mensajeError.append("Campo: ").append( error.getNombreCampoError()).append(" - ").append(error.getRecomendaciones()).append("\n");
				}
				break;
			case CAMPO_LINEA_NEGOCIO:
				if (cell != null && !cell.toString().isEmpty()) {
					detalle.setLineaNegocio(cell.toString());
					lineaVacia = false;
				}else {
					error.setNumeroInciso(new BigDecimal(fila));
					error.setNombreSeccion(lineaDeNegocio);
					error.setNombreCampoError("LINEA DE NEGOCIO");
					error.setRecomendaciones("Seleccione una Linea de Negocio");
					listErrores.add(error);
					mensajeError.append("Campo: ").append(error.getNombreCampoError()).append(" - ").append(error.getRecomendaciones()).append("\n");
				}
				break;
			case CAMPO_ID_PAQUETE:
				if (cell != null && !cell.toString().isEmpty()) {
					detalle.setPaquete(cell.toString());
					lineaVacia = false;
				}else {
					error.setNumeroInciso(new BigDecimal(fila));
					error.setNombreSeccion(lineaDeNegocio);
					error.setNombreCampoError("PAQUETE");
					error.setRecomendaciones("Seleccione un Paquete");
					listErrores.add(error);
					mensajeError.append("Campo: ").append(error.getNombreCampoError()).append(" - ").append(error.getRecomendaciones()).append("\n");
				}
				break;
			case CAMPO_NUMERO_EMPLEADO:
				if (cell != null && !cell.toString().isEmpty()) {
					try{
						detalle.setNumeroEmpleado(new BigDecimal(cell.toString()));
						lineaVacia = false;
					}catch(Exception e){
					}
				}
				break;
			case CAMPO_TIPO_PERSONA_CLIENTE:
				if (cell != null && !cell.toString().isEmpty()) {
					if(cell.toString().equals(FISICA)){
						detalle.setTipoPersonaCliente((short) 1);
					}else{
						detalle.setTipoPersonaCliente((short) 2);
					}
					lineaVacia = false;
				}else {
					error.setNumeroInciso(new BigDecimal(fila));
					error.setNombreSeccion(lineaDeNegocio);
					error.setNombreCampoError("TIPO PERSONA CLIENTE");
					error.setRecomendaciones("Ingrese el Tipo Persona Cliente");
					listErrores.add(error);
					mensajeError.append("Campo: ").append(error.getNombreCampoError()).append(" - ").append(error.getRecomendaciones()).append("\n");
				}
				break;
			case CAMPO_RFC_CLIENTE:
				if (cell != null && !cell.toString().isEmpty()) {
					detalle.setRfcCliente(cell.toString().toUpperCase());
					lineaVacia = false;
				}else {
					error.setNumeroInciso(new BigDecimal(fila));
					error.setNombreSeccion(lineaDeNegocio);
					error.setNombreCampoError("RFC CLIENTE");
					error.setRecomendaciones("Ingrese el RFC Cliente");
					listErrores.add(error);
					mensajeError.append("Campo: ").append(error.getNombreCampoError()).append(" - ").append(error.getRecomendaciones()).append("\n");
				}
				break;
			case CAMPO_CLIENTE_VIP:
				if (cell != null && !cell.toString().isEmpty()) {
					if(cell.toString().equals(GeneraExcelCargaMasiva.VALOR_SI)){
						detalle.setClienteVIP((short) 1);
					}else{
						detalle.setClienteVIP((short) 0);
					}
					lineaVacia = false;
				}
				break;
			case CAMPO_NOMBRE_RAZON_SOCIAL:
				if (cell != null && !cell.toString().isEmpty()) {
					detalle.setNombreORazonSocial(cell.toString().toUpperCase());
					lineaVacia = false;
				}else {
					error.setNumeroInciso(new BigDecimal(fila));
					error.setNombreSeccion(lineaDeNegocio);
					error.setNombreCampoError("NOMBRE O RAZON SOCIAL");
					error.setRecomendaciones("Ingrese el Nombre o Razon Social");
					listErrores.add(error);
					mensajeError.append("Campo: ").append(error.getNombreCampoError()).append(" - ").append(error.getRecomendaciones()).append("\n");
				}
				break;
			case CAMPO_APELLIDO_PATERNO_CLIENTE:				
				if(detalle.getTipoPersonaCliente() != null && detalle.getTipoPersonaCliente().equals(uno)){
				if (cell != null && !cell.toString().isEmpty()) {
					detalle.setApellidoPaternoCliente(cell.toString().toUpperCase());
					lineaVacia = false;
				}else {
					error.setNumeroInciso(new BigDecimal(fila));
					error.setNombreSeccion(lineaDeNegocio);
					error.setNombreCampoError("APELLIDO PATERNO CLIENTE");
					error.setRecomendaciones("Ingrese el Apellido Paterno Cliente");
					listErrores.add(error);
					mensajeError.append("Campo: ").append(error.getNombreCampoError()).append(" - ").append(error.getRecomendaciones()).append("\n");
				}
				}
				break;
			case CAMPO_APELLIDO_MATERNO_CLIENTE:
				if(detalle.getTipoPersonaCliente() != null && detalle.getTipoPersonaCliente().equals(uno)){
				if (cell != null && !cell.toString().isEmpty()) {
					detalle.setApellidoMaternoCliente(cell.toString().toUpperCase());
					lineaVacia = false;
				}else {
					error.setNumeroInciso(new BigDecimal(fila));
					error.setNombreSeccion(lineaDeNegocio);
					error.setNombreCampoError("APELLIDO MATERNO CLIENTE");
					error.setRecomendaciones("Ingrese el Apellido Materno Cliente");
					listErrores.add(error);
					mensajeError.append("Campo: ").append(error.getNombreCampoError()).append(" - ").append(error.getRecomendaciones()).append("\n");
				}
				}
				break;
			case CAMPO_CODIGO_POSTAL_CLIENTE:
				if (cell != null && !cell.toString().isEmpty()) {
					String valor = cell.toString();
					if (valor.indexOf(".") != -1) {
						valor = valor.substring(0, valor.indexOf("."));
					}
					detalle.setCodigoPostalCliente(valor);
					lineaVacia = false;
				}else {
					error.setNumeroInciso(new BigDecimal(fila));
					error.setNombreSeccion(lineaDeNegocio);
					error.setNombreCampoError("CODIGO POSTAL CLIENTE");
					error.setRecomendaciones("Ingrese el Codigo Postal Cliente");
					listErrores.add(error);
					mensajeError.append("Campo: ").append(error.getNombreCampoError()).append(" - ").append(error.getRecomendaciones()).append("\n");
				}
				break;
			case CAMPO_COLONIA_CLIENTE:
				if (cell != null && !cell.toString().isEmpty()) {
					detalle.setColoniaCliente(cell.toString().toUpperCase().trim());
					lineaVacia = false;
				}else {
					error.setNumeroInciso(new BigDecimal(fila));
					error.setNombreSeccion(lineaDeNegocio);
					error.setNombreCampoError("COLONIA CLIENTE");
					error.setRecomendaciones("Ingrese el Colonia Cliente");
					listErrores.add(error);
					mensajeError.append("Campo: ").append(error.getNombreCampoError()).append(" - ").append(error.getRecomendaciones()).append("\n");
				}
				break;
			case CAMPO_TELEFONO_CLIENTE:
				if (cell != null && !cell.toString().isEmpty()) {
					String telefono = formatter.formatCellValue(cell); 
					detalle.setTelefonoCliente(telefono);
					lineaVacia = false;
				}else {
					error.setNumeroInciso(new BigDecimal(fila));
					error.setNombreSeccion(lineaDeNegocio);
					error.setNombreCampoError("TELEFONO CLIENTE");
					error.setRecomendaciones("Ingrese el Telefono Cliente");
					listErrores.add(error);
					mensajeError.append("Campo: ").append(error.getNombreCampoError()).append(" - ").append(error.getRecomendaciones()).append("\n");
				}
				break;	
			case CAMPO_CVE_AMIS:
				if (cell != null && !cell.toString().isEmpty()) {
					String valorStr = "";
					if(cell.getCellType() == HSSFCell.CELL_TYPE_NUMERIC){
						double valor = cell.getNumericCellValue();
						BigDecimal valorB = new BigDecimal(valor);
						valorStr = valorB.toPlainString();
					}else if(cell.getCellType() == HSSFCell.CELL_TYPE_STRING){
						valorStr = cell.getStringCellValue();
					}else{
						valorStr = cell.toString();
					}
					if (valorStr.indexOf(".") != -1) {
						valorStr = valorStr.substring(0, valorStr.indexOf("."));
					}
					detalle.setClaveAMIS(valorStr);
					lineaVacia = false;
				}else {
					error.setNumeroInciso(new BigDecimal(fila));
					error.setNombreSeccion(lineaDeNegocio);
					error.setNombreCampoError("CLAVE AMIS");
					error.setRecomendaciones("Ingrese la Clave AMIS");
					listErrores.add(error);
					mensajeError.append("Campo: ").append(error.getNombreCampoError()).append(" - ").append(error.getRecomendaciones()).append("\n");
				}
				break;
			case CAMPO_MODELO:
				if (cell != null && !cell.toString().isEmpty()) {
					String valor = cell.toString();
					if (valor.indexOf(".") != -1) {
						valor = valor.substring(0, valor.indexOf("."));
					}
					try{
						detalle.setModelo(new Short(valor));
						lineaVacia = false;
					}catch(Exception e){
						error.setNumeroInciso(new BigDecimal(fila));
						error.setNombreSeccion(lineaDeNegocio);
						error.setNombreCampoError("MODELO");
						error.setRecomendaciones("Valor Invalido");
						listErrores.add(error);
						mensajeError.append("Campo: ").append(error.getNombreCampoError()).append(" - ").append(error.getRecomendaciones()).append("\r\n").append(System.getProperty("line.separator"));
					}
				}else {
					error.setNumeroInciso(new BigDecimal(fila));
					error.setNombreSeccion(lineaDeNegocio);
					error.setNombreCampoError("MODELO");
					error.setRecomendaciones("Ingrese el Modelo");
					listErrores.add(error);
					mensajeError.append("Campo: ").append(error.getNombreCampoError()).append(" - ").append(error.getRecomendaciones()).append("\n");
				}
				break;
			case CAMPO_NCI_REPUVE:
				if (cell != null && !cell.toString().isEmpty()) {
					detalle.setNciRepuve(cell.toString().toUpperCase());
					lineaVacia = false;
				}
				break;
			case CAMPO_CVE_USO:
				if (cell != null && !cell.toString().isEmpty()) {
					detalle.setClaveUso(cell.toString());
					lineaVacia = false;
				}else {
					error.setNumeroInciso(new BigDecimal(fila));
					error.setNombreSeccion(lineaDeNegocio);
					error.setNombreCampoError("CLAVE USO");
					error.setRecomendaciones("Ingrese la Clave Uso");
					listErrores.add(error);
					mensajeError.append("Campo: ").append(error.getNombreCampoError()).append(" - ").append(error.getRecomendaciones()).append("\n");
				}
				break;
			case CAMPO_PLACAS:
				if (cell != null && !cell.toString().isEmpty()) {
					detalle.setPlacas(cell.toString().toUpperCase());
					lineaVacia = false;
				}else {
					error.setNumeroInciso(new BigDecimal(fila));
					error.setNombreSeccion(lineaDeNegocio);
					error.setNombreCampoError("PLACAS");
					error.setRecomendaciones("Ingrese las Placas");
					listErrores.add(error);
					mensajeError.append("Campo: ").append(error.getNombreCampoError()).append(" - ").append(error.getRecomendaciones()).append("\n");
				}
				break;
			case CAMPO_FORMA_PAGO:
				if (cell != null && !cell.toString().isEmpty()) {
					detalle.setFormaPago(cell.toString());
					lineaVacia = false;
				}else {
					error.setNumeroInciso(new BigDecimal(fila));
					error.setNombreSeccion(lineaDeNegocio);
					error.setNombreCampoError("FORMAS DE PAGO");
					error.setRecomendaciones("Seleccione la Forma de Pago");
					listErrores.add(error);
					mensajeError.append("Campo: ").append(error.getNombreCampoError()).append(" - ").append(error.getRecomendaciones()).append("\n");
				}
				break;
			case CAMPO_NUMERO_MOTOR:
				if (cell != null && !cell.toString().isEmpty()) {
					if(cell.getCellType() == HSSFCell.CELL_TYPE_NUMERIC){
						double valor = cell.getNumericCellValue();
						BigDecimal valorB = new BigDecimal(valor);
						detalle.setNumeroMotor(valorB.toPlainString().toUpperCase());
					}else if(cell.getCellType() == HSSFCell.CELL_TYPE_STRING){
						detalle.setNumeroMotor(cell.toString().toUpperCase());
					}else{
						detalle.setNumeroMotor(cell.toString().toUpperCase());
					}
					lineaVacia = false;
				}else {
					error.setNumeroInciso(new BigDecimal(fila));
					error.setNombreSeccion(lineaDeNegocio);
					error.setNombreCampoError("NUMERO DE MOTOR");
					error.setRecomendaciones("Ingrese el Numero de Motor");
					listErrores.add(error);
					mensajeError.append("Campo: ").append(error.getNombreCampoError()).append(" - ").append(error.getRecomendaciones()).append("\n");
				}
				break;
			case CAMPO_NUMERO_SERIE:
				if (cell != null && !cell.toString().isEmpty()) {
					if(cell.getCellType() == HSSFCell.CELL_TYPE_NUMERIC){
						double valor = cell.getNumericCellValue();
						BigDecimal valorB = new BigDecimal(valor);
						detalle.setNumeroSerie(valorB.toPlainString());
					}else if(cell.getCellType() == HSSFCell.CELL_TYPE_STRING){
						detalle.setNumeroSerie(cell.toString());
					}else{
						detalle.setNumeroSerie(cell.toString());
					}
					lineaVacia = false;
				}else {
					error.setNumeroInciso(new BigDecimal(fila));
					error.setNombreSeccion(lineaDeNegocio);
					error.setNombreCampoError("NUMERO DE SERIE");
					error.setRecomendaciones("Ingrese el Numero de Serie");
					listErrores.add(error);
					mensajeError.append("Campo: ").append(error.getNombreCampoError()).append(" - ").append(error.getRecomendaciones()).append("\n");
				}
				break;
			case CAMPO_VALOR_COMERCIAL:
				if (cell != null && !cell.toString().isEmpty()) {
					try{
						detalle.setValorComercial(new BigDecimal(cell.toString()));
						lineaVacia = false;
					}catch(Exception e){
					}
				}
				break;
			case CAMPO_TOTAL_PASAJEROS:
				if (cell != null && !cell.toString().isEmpty()) {
					String valor = cell.toString();
					if (valor.indexOf(".") != -1) {
						valor = valor.substring(0, valor.indexOf("."));
					}
					try{
						detalle.setNumeroPasajeros(new Short(valor));
						lineaVacia = false;
					}catch(Exception e){
					}
				}
				break;
			case CAMPO_DESCRIPCION_VEHICULO:
				if (cell != null && !cell.toString().isEmpty()) {
					detalle.setDescripcionVehiculo(cell.toString().toUpperCase());
					lineaVacia = false;
				}
				break;
			case CAMPO_PRIMA_TOTAL:
				if (cell != null && !cell.toString().isEmpty()) {
					try{
						detalle.setPrimaTotal(new Double(cell.toString()));
						lineaVacia = false;
					}catch(Exception e){
					}
				}
				break;
			case CAMPO_DEDUCIBLE_DANOS_MATERIALES:
				if (cell != null && !cell.toString().isEmpty()) {
					try{
						detalle.setDeducibleDanosMateriales(new Double(cell.toString()));
						lineaVacia = false;
					}catch(Exception e){
					}
				}
				break;
			case CAMPO_DEDUCIBLE_ROBO_TOTAL:
				if (cell != null && !cell.toString().isEmpty()) {
					try{
						detalle.setDeducibleRoboTotal(new Double(cell.toString()));
						lineaVacia = false;
					}catch(Exception e){
					}
				}
				break;
			case CAMPO_LIMITE_RC_TERCEROS:
				if (cell != null && !cell.toString().isEmpty()) {
					try{
						detalle.setLimiteRcTerceros(new Double(cell.toString()));
						lineaVacia = false;
					}catch(Exception e){
					}
				}
				break;
			case CAMPO_DEDUCIBLE_RC_TERCEROS:
				if (cell != null && !cell.toString().isEmpty()) {
					try{
						detalle.setDeducibleRcTerceros(new Double(cell.toString()));
						lineaVacia = false;
					}catch(Exception e){
					}
				}
				break;
			case CAMPO_LIMITE_GASTOS_MEDICOS:
				if (cell != null && !cell.toString().isEmpty()) {
					try{
						detalle.setLimiteGastosMedicos(new Double(cell.toString()));
						lineaVacia = false;
					}catch(Exception e){
					}
				}
				break;
			case CAMPO_LIMITE_MUERTE:
				if (cell != null && !cell.toString().isEmpty()) {
					try{
						detalle.setLimiteMuerte(new Double(cell.toString()));
						lineaVacia = false;
					}catch(Exception e){
					}
				}
				break;
			case CAMPO_LIMITE_RC_VIAJERO:
				if (cell != null && !cell.toString().isEmpty()) {
					try{
						detalle.setLimiteRcViajero(new Double(cell.toString()));
						lineaVacia = false;
					}catch(Exception e){
					}
				}
				break;
			case CAMPO_ASISTENCIA_JURIDICA:
				if (cell != null && !cell.toString().isEmpty()) {
					detalle.setAsistenciaJuridica(validaSINO(cell.toString()));
					lineaVacia = false;
				}
				break;
			case CAMPO_LIMITE_ADAPTACION_CONVERSION:
				if (cell != null && !cell.toString().isEmpty()) {
					try{
						detalle.setLimiteAdaptacionConversion(new Double(cell.toString()));
						lineaVacia = false;
					}catch(Exception e){
					}
				}
				break;
			case CAMPO_LIMITE_EQUIPO_ESPECIAL:
				if (cell != null && !cell.toString().isEmpty()) {
					try{
						detalle.setLimiteEquipoEspecial(new Double(cell.toString()));
						lineaVacia = false;
					}catch(Exception e){
					}
				}
				break;
			case CAMPO_DEDUCIBLE_EQUIPO_ESPECIAL:
				if (cell != null && !cell.toString().isEmpty()) {
					try{
						detalle.setDeducibleEquipoEspecial(new Double(cell.toString()));
						lineaVacia = false;
					}catch(Exception e){
					}
				}
				break;
			case CAMPO_IGUALACION:
				if (cell != null && !cell.toString().isEmpty()) {
					detalle.setIgualacion(cell.toString());
					lineaVacia = false;
				}
				break;
			case CAMPO_DERECHOS:
				if (cell != null && !cell.toString().isEmpty()) {
					try{
						detalle.setDerechos(new Double(cell.toString()));
						lineaVacia = false;
					}catch(Exception e){
						error.setNumeroInciso(new BigDecimal(fila));
						error.setNombreSeccion(lineaDeNegocio);
						error.setNombreCampoError("DERECHOS");
						error.setRecomendaciones("Valor Invalido");
						listErrores.add(error);
						mensajeError.append("Campo: ").append(error.getNombreCampoError()).append(" - ").append(error.getRecomendaciones()).append("\r\n").append(System.getProperty("line.separator"));
					}
				}else{
					error.setNumeroInciso(new BigDecimal(fila));
					error.setNombreSeccion(lineaDeNegocio);
					error.setNombreCampoError("DERECHOS");
					error.setRecomendaciones("Ingrese el Derecho");
					listErrores.add(error);
					mensajeError.append("Campo: ").append(error.getNombreCampoError()).append(" - ").append(error.getRecomendaciones()).append("\n");
				}
				break;	
			case CAMPO_SOLICITAR_AUTORIZACION:
				if (cell != null && !cell.toString().isEmpty()) {
					detalle.setSolicitarAutorizacion(cell.toString());
					lineaVacia = false;
				}
				break;	
			case CAMPO_CAUSA_AUTORIZACION:
				if (cell != null && !cell.toString().isEmpty()) {
					detalle.setCausaAutorizacion(cell.toString());
					lineaVacia = false;
				}
				break;
			case CAMPO_DESCRIPCION_EQUIPO_ESPECIAL:
				if (cell != null && !cell.toString().isEmpty()) {
					detalle.setDescripcionEquipoEspecial(cell.toString().toUpperCase());
					lineaVacia = false;
				}
				break;	
			case CAMPO_DESCRIPCION_ADAPTACION_CONVERSION:
				if (cell != null && !cell.toString().isEmpty()) {
					detalle.setDescripcionAdaptacionConversion(cell.toString().toUpperCase());
					lineaVacia = false;
				}
				break;
			case CAMPO_NOMBRE:
				if (cell != null && !cell.toString().isEmpty()) {
					detalle.setNombre(cell.toString().toUpperCase());
					lineaVacia = false;
				} else {
					if(datosConductorObligatorios){
					error.setNumeroInciso(new BigDecimal(fila));
					error.setNombreSeccion(lineaDeNegocio);
					error.setNombreCampoError("NOMBRE");
					error.setRecomendaciones("Ingrese un Nombre");
					listErrores.add(error);
					mensajeError.append("Campo: NOMBRE -  Ingrese un Nombre \n");
					}
				}
				break;
			case CAMPO_APELLIDO_PATERNO:
				if (cell != null && !cell.toString().isEmpty()) {
					detalle.setApellidoPaterno(cell.toString().toUpperCase());
					lineaVacia = false;
				} else {
					if(datosConductorObligatorios){
					error.setNumeroInciso(new BigDecimal(fila));
					error.setNombreSeccion(lineaDeNegocio);
					error.setNombreCampoError("APELLIDO PATERNO");
					error.setRecomendaciones("Ingrese el Apellido Paterno");
					listErrores.add(error);
					mensajeError.append("Campo: APELLIDO PATERNO -  Ingrese el Apellido Paterno \n");
					}
				}
				break;
			case CAMPO_APELLIDO_MATERNO:
				if (cell != null && !cell.toString().isEmpty()) {
					detalle.setApellidoMaterno(cell.toString().toUpperCase());
					lineaVacia = false;
				}else{
					if(datosConductorObligatorios){
					error.setNumeroInciso(new BigDecimal(fila));
					error.setNombreSeccion(lineaDeNegocio);
					error.setNombreCampoError("APELLIDO MATERNO");
					error.setRecomendaciones("Ingrese el Apellido Materno");
					listErrores.add(error);
					mensajeError.append("Campo: APELLIDO MATERNO -  Ingrese el Apellido Materno \n");
					}
				}
				break;
			case CAMPO_NUMERO_LICENCIA:
				if (cell != null && !cell.toString().isEmpty()) {
					detalle.setNumeroLicencia(cell.toString().toUpperCase());
					lineaVacia = false;
				}else{
					if(datosConductorObligatorios){
						error.setNumeroInciso(new BigDecimal(fila));
						error.setNombreSeccion(lineaDeNegocio);
						error.setNombreCampoError("NUMERO LICENCIA");
						error.setRecomendaciones("Ingrese el Numero de Licencia");
						listErrores.add(error);
						mensajeError.append("Campo: ").append(error.getNombreCampoError()).append(" - ").append(error.getRecomendaciones()).append("\n");
					}					
				}
				break;
			case CAMPO_FECHA_NACIMIENTO:
				if (cell != null && !cell.toString().isEmpty()) {
					try {						
						detalle.setFechaNacimiento(cell.getDateCellValue());
						lineaVacia = false;
					} catch (Exception e) {
						if(datosConductorObligatorios){
							error.setNumeroInciso(new BigDecimal(fila));
							error.setNombreSeccion(lineaDeNegocio);
							error.setNombreCampoError("FECHA DE NACIMIENTO");
							error.setRecomendaciones("Fecha de Nacimiento Invalida");
							listErrores.add(error);
							mensajeError.append("Campo: FECHA DE NACIMIENTO -  Fecha de Nacimiento Invalida \n");
						}
					}
				} else {
					if(datosConductorObligatorios){
					error.setNumeroInciso(new BigDecimal(fila));
					error.setNombreSeccion(lineaDeNegocio);
					error.setNombreCampoError("FECHA DE NACIMIENTO");
					error.setRecomendaciones("Ingrese la Fecha de Nacimiento");
					listErrores.add(error);
					mensajeError.append("Campo: FECHA DE NACIMIENTO -  Ingrese la Fecha de Nacimiento \n");
					}
				}
				break;
			case CAMPO_OCUPACION:
				if (cell != null && !cell.toString().isEmpty()) {
					detalle.setOcupacion(cell.toString().toUpperCase());
					lineaVacia = false;
				}else{
					if(datosConductorObligatorios){
						error.setNumeroInciso(new BigDecimal(fila));
						error.setNombreSeccion(lineaDeNegocio);
						error.setNombreCampoError("OCUPACION");
						error.setRecomendaciones("Ingrese la Ocupacion");
						listErrores.add(error);
						mensajeError.append("Campo: ").append(error.getNombreCampoError()).append(" - ").append(error.getRecomendaciones()).append("\n");
					}					
				}
				break;
			case CAMPO_IDCLIENTE:
				if (cell != null && !cell.toString().isEmpty()) {
					try{
						BigDecimal valorB = new BigDecimal(cell.toString().trim());
						detalle.setIdCliente(valorB);
					} catch (Exception ex) {
						error.setNumeroInciso(new BigDecimal(fila));
						error.setNombreSeccion(lineaDeNegocio);
						error.setNombreCampoError("ID CLIENTE");
						error.setRecomendaciones("Se esperaba un valor numerico.");
						listErrores.add(error);
						mensajeError.append("Campo: ID CLIENTE -  Se esperaba un valor numerico. \n");
					}
					lineaVacia = false;
				}
				break;
			case CAMPO_NUMERO_REMOLQUES:
				if (cell != null && !cell.toString().isEmpty()) {
					try{
						detalle.setNumeroRemolques(Double.valueOf(cell.toString()).intValue());
						lineaVacia = false;
					}catch(Exception e){
					}
				}
				break;
			case CAMPO_TIPO_CARGA:
				if (cell != null && !cell.toString().isEmpty()) {
					try{
						String tipoCargaDescripcion = cell.toString();
						detalle.setTipoCargaDescripcion(tipoCargaDescripcion);
						if (!StringUtils.isBlank(tipoCargaDescripcion)) {
							CatalogoValorFijoDTO tipoCarga = cargaMasivaService.obtieneTipoCargaPorDescripcion(tipoCargaDescripcion);
							detalle.setTipoCarga(tipoCarga);
						}
						lineaVacia = false;
					}catch(Exception e){
					}
				}
				break;
			case CAMPO_OBSERVACIONES_INCISO:
				if (cell != null && !cell.toString().isEmpty()) {
					detalle.setObservacionesInciso(cell.toString().toUpperCase());
					lineaVacia = false;
				}
				break;
			case CAMPO_NOMBRE_ASEGURADO:
				if (cell != null && !cell.toString().isEmpty()) {
					detalle.setNombreAsegurado(cell.toString().toUpperCase());
					lineaVacia = false;
				}
				break;
			case CAMPO_LIMITE_ACCIDENTES_CONDUCTOR:
				if (cell != null && !cell.toString().isEmpty()) {
					try{
						detalle.setLimiteAccidentesConductor(new Double(cell.toString()));
						lineaVacia = false;
					}catch(Exception e){
					}
				}
				break;
			case CAMPO_RPF:
				if (cell != null && !cell.toString().isEmpty()) {
					try{
						detalle.setPorcentajePagoFraccionado(new Double(cell.toString()));
						lineaVacia = false;
					}catch(Exception e){
					}
				}
				break;
			case CAMPO_SUMA_ASEGURADA_DANOS_MATERIALES:
				if (cell != null && !cell.toString().isEmpty()) {
					try{
						detalle.setValorSumaAseguradaDM(new Double(cell.toString()));
						lineaVacia = false;
					}catch(Exception e){
					}
				}
				break;
			case CAMPO_SUMA_ASEGURADA_ROBO_TOTAL:
				if (cell != null && !cell.toString().isEmpty()) {
					try{
						detalle.setValorSumaAseguradaRT(new Double(cell.toString()));
						lineaVacia = false;
					}catch(Exception e){
					}
				}
				break;
			case CAMPO_EMAIL:
				if (cell != null && !cell.toString().isEmpty()) {
					try{
						detalle.setEmailContactos(cell.toString().toLowerCase());
						lineaVacia = false;
					}catch(Exception e){
					}
				}
				break;
			case CAMPO_ALTA_CONDICION_ESPECIAL:
				if (cell != null && !cell.toString().isEmpty()) {
					try{
						detalle.setAltaCondicionesEspeciales( transformAndValidateStringToArray( cell.toString(), new BigDecimal(fila),METODO_ALTA ) );
						lineaVacia = false;
					}catch(Exception e){
					}
				}
				break;
			case CAMPO_BAJA_CONDICION_ESPECIAL:
				if (cell != null && !cell.toString().isEmpty()) {
					try{
						detalle.setBajaCondicionesEspeciales( transformAndValidateStringToArray( cell.toString(),new BigDecimal(fila),METODO_BAJA  )  );
						lineaVacia = false;
					}catch(Exception e){
					}
				}
				break;
			case CAMPO_CALLE_CLIENTE:
				if (cell != null && !cell.toString().isEmpty()) {
					try{
						detalle.setCalleCliente( cell.toString().toUpperCase() );
						lineaVacia = false;
					}catch(Exception e){
					}
				}
				break;
			case CAMPO_ID_REPRESENTANTE:
				if (cell != null && !cell.toString().isEmpty()) {
					try{
						String idRepresentanteString = formatter.formatCellValue(cell);
						detalle.setIdRepresentante( Long.parseLong(idRepresentanteString) );
						lineaVacia = false;
					}catch(Exception e){
						e.printStackTrace();
					}
				}
				break;
			case CAMPO_TIPO_PERSONA_REPRESENTANTE:
				if (cell != null && !cell.toString().isEmpty()) {
					if(cell.toString().equals(FISICA)){
						detalle.setTipoPersonaRepresentante((short) 1);
					}else{
						detalle.setTipoPersonaRepresentante((short) 2);
					}
					lineaVacia = false;
				}else {
				}
				break;
			case CAMPO_RFC_REPRESENTANTE:
				if (cell != null && !cell.toString().isEmpty()) {
					try{
						detalle.setRfcRepresentante( cell.toString().toUpperCase() );
						lineaVacia = false;
					}catch(Exception e){
					}
				}
				break;
			case CAMPO_NOMBRE_RAZON_SOCIAL_REPRESENTANTE:
				if (cell != null && !cell.toString().isEmpty()) {
					try{
						detalle.setNombreRazonRepresentante( cell.toString().toUpperCase() );
						lineaVacia = false;
					}catch(Exception e){
					}
				}
				break;
			case CAMPO_APELLIDO_PATERNO_REPRESENTANTE:
				if (cell != null && !cell.toString().isEmpty()) {
					try{
						detalle.setApellidoPaternoRepresentante( cell.toString().toUpperCase() );
						lineaVacia = false;
					}catch(Exception e){
					}
				}
				break;
			case CAMPO_APELLIDO_MATERNO_REPRESENTANTE:
				if (cell != null && !cell.toString().isEmpty()) {
					try{
						detalle.setApellidoMaternoRepresentante( cell.toString().toUpperCase() );
						lineaVacia = false;
					}catch(Exception e){
					}
				}
				break;
			case CAMPO_CODIGO_POSTAL_REPRESENTANTE:
				if (cell != null && !cell.toString().isEmpty()) {
					try{
						String valor = cell.toString();
						if (valor.indexOf(".") != -1) {
							valor = valor.substring(0, valor.indexOf("."));
						}
						detalle.setCodigoPostalRepresentante( valor );
						lineaVacia = false;
					}catch(Exception e){
					}
				}
				break;
			case CAMPO_COLONIA_REPRESENTANTE:
				if (cell != null && !cell.toString().isEmpty()) {
					try{
						detalle.setColoniaRepresentante( cell.toString().toUpperCase() );
						lineaVacia = false;
					}catch(Exception e){
					}
				}
				break;
			case CAMPO_TELEFONO_REPRESENTANTE:
				if (cell != null && !cell.toString().isEmpty()) {
					try{
						String telefono = formatter.formatCellValue(cell); 
						detalle.setTelefonoRepresentante( telefono );
						lineaVacia = false;
					}catch(Exception e){
					}
				}
				break;
			case CAMPO_CALLE_REPRESENTANTE:
				if (cell != null && !cell.toString().isEmpty()) {
					try{
						detalle.setCalleRepresentante( cell.toString().toUpperCase() );
						lineaVacia = false;
					}catch(Exception e){
					}
				}
				break;
			case CAMPO_ENTREVISTA_PRIMA_ESTIMADA:
				if (cell != null && !cell.toString().isEmpty()) {
					entrevista.setPrimaEstimada(new BigDecimal(cell.toString()));
					;
				}
			break;
			case CAMPO_ENTREVISTA_MONEDA:
				if (cell != null && !cell.toString().isEmpty()) {
					BigDecimal moneda=new BigDecimal(cell.toString());
					entrevista.setMoneda(moneda.intValue());
				}else{
					entrevista.setMoneda(484);					
				}
			break;
			case CAMPO_ENTREVISTA_PEP:
				if (cell != null && !cell.toString().isEmpty()) {
					String valPep=cell.toString();
					if(valPep.toUpperCase().contains("S")){
						entrevista.setPep(true);
					}else{
						entrevista.setPep(false);
					}
					
				}
			break;
			case CAMPO_ENTREVISTA_PARENTESCO:
				if (cell != null && !cell.toString().isEmpty()) {
					entrevista.setParentesco(cell.toString());
					
				}
				break;
			case CAMPO_ENTREVISTA_NOMBRE_PUESTO:
				if (cell != null && !cell.toString().isEmpty()) {
					entrevista.setNombrePuesto(cell.toString());
					
				}
				break;
			case CAMPO_ENTREVISTA_PERIODO_FUNCIONES:
				if (cell != null && !cell.toString().isEmpty()) {
					entrevista.setPeriodoFunciones(cell.toString());
					
				}
				break;
			case CAMPO_ENTREVISTA_CUOTA_PROPIA:
				if (cell != null && !cell.toString().isEmpty()) {

					String valPep=cell.toString();
					if(valPep.toUpperCase().contains("S")){
						entrevista.setCuentaPropia(true);
					}else{
						entrevista.setCuentaPropia(false);
					}
					
				}
				break;
			case CAMPO_ENTREVISTA_NOMBRE_PARENTESCO:
				if (cell != null && !cell.toString().isEmpty()) {
					entrevista.setNombreRepresentado(cell.toString());
					
				}
			break;
			case CAMPO_ENTREVISTA_DOCUMENTACION_REPRESENTACIOM:
				if (cell != null && !cell.toString().isEmpty()) {
					String valPep=cell.toString();
					if(valPep.toUpperCase().contains("S")){
						entrevista.setDocumentoRepresentacion(true);
					}else{
						entrevista.setDocumentoRepresentacion(false);
					}
					
				}
			break;
			case CAMPO_ENTREVISTA_NUMERO_FIEL:
				if (cell != null && !cell.toString().isEmpty()) {
					entrevista.setNumeroSerieFiel(cell.toString());
					
				}
			break;
			case CAMPO_ENTREVISTA_FOLIO_MERCANTIL:
				if (cell != null && !cell.toString().isEmpty()) {
					entrevista.setFolioMercantil(cell.toString());
					
				}
			break;
			case CAMPO_CONDUCTO_COBRO:
				if (cell != null && !cell.toString().isEmpty()) {
					detalle.setConductoCobro(cell.toString());
					lineaVacia = false;
					
					if(cell.toString().equals(VALOR_TC)){
						detalle.setIdMedioPago(MEDIO_PAGO_TC);
					}else if(cell.toString().equals(VALOR_DOMICILIACION)){
						detalle.setIdMedioPago(MEDIO_PAGO_DOMICILIACION);
					}else if(cell.toString().equals(VALOR_EFECTIVO)){
						detalle.setIdMedioPago(MEDIO_PAGO_EFECTIVO);
					}else if(cell.toString().equals(VALOR_CUENTAAFIRME)){
						detalle.setIdMedioPago(MEDIO_PAGO_CUENTA_AFIRME);
					}
				}
				break;
			case CAMPO_INSTITUCION_BANCARIA:
				if (cell != null && !cell.toString().isEmpty()) {
					detalle.setInstitucionBancaria(cell.toString());
				}
				break;	
			case CAMPO_TIPO_TARJETA:
				if (cell != null && !cell.toString().isEmpty()) {
					detalle.setTipoTarjeta(cell.toString());
				}
				break;	
			case CAMPO_NUMERO_TARJETA:
				if (cell != null && !cell.toString().isEmpty()) {
					cell.setCellType(cell.CELL_TYPE_STRING);
					detalle.setNumeroTarjetaClave(cell.toString());
				}
				break;
			case CAMPO_CODIGO_SEGURIDAD:
				if (cell != null && !cell.toString().isEmpty()) {
					cell.setCellType(cell.CELL_TYPE_STRING);
					detalle.setCodigoSeguridad(cell.toString());
				}
				break;
			case CAMPO_FECHA_VENCIMIENTO:
				if (cell != null && !cell.toString().isEmpty()) {
					String fechaVencimiento = validarFecha(cell.toString()); 					
					if(fechaVencimiento==null){
						error.setNumeroInciso(new BigDecimal(fila));
						error.setNombreSeccion("FECHA DE VENCIMIENTO");
						error.setNombreCampoError("FECHA DE VENCIMIENTO");
						error.setRecomendaciones("Fecha de Vencimiento no valida. Usar el formato MM/yy y una tarjeta vigente");
						listErrores.add(error);
					}else{						
						detalle.setFechaVencimiento(fechaVencimiento);
					}
				}
				break;
			}
			
			
		}
		try {if(detalle.getIdCliente()!=null){
			clienteRest.findInterViews(detalle.getIdCliente().longValue());
		}
			
		} catch (InterviewException e) {
		clienteRest.createInterview(detalle.getIdCliente().longValue(), entrevista);}
		detalle.setMensajeError(mensajeError.toString());
		
		if (mensajeError.toString().isEmpty()) {
			detalle.setClaveEstatus(DetalleCargaMasivaAutoCot.ESTATUS_PENDIENTE);
		} else {
			detalle.setClaveEstatus(DetalleCargaMasivaAutoCot.ESTATUS_ERROR);
		}
		if(lineaVacia && !mensajeError.toString().isEmpty()){
			Iterator<LogErroresCargaMasivaDTO> it = listErrores.iterator();
			while(it.hasNext()){
				LogErroresCargaMasivaDTO error = it.next();
				if(error.getNumeroInciso().equals(new BigDecimal(fila))){
					it.remove();
				}
			}
			return null;
		}

		return detalle;
	}
	
	
	private String[] transformAndValidateStringToArray(String cadena, BigDecimal fila, String metodo ){
		
		String[] splitCadena 			= cadena.split(SEPARADOR_CONDICION_ESPECIAL);
		int tamano 						= splitCadena.length;
		String[] resultado 				= new String[tamano];
		LogErroresCargaMasivaDTO error 	= new LogErroresCargaMasivaDTO();
		
		if(tamano >0){
			if(tamano == TAMANO_MIN_ELEMENTOS){
				try{
					BigDecimal decimalValue = new BigDecimal( splitCadena[0] );
					int intValue = decimalValue.intValue();
					resultado[0]= String.valueOf( intValue );
				}catch(Exception e){
					error.setNumeroInciso(fila);
					error.setNombreSeccion("");
					error.setNombreCampoError(metodo+ " CARACTER INVALIDO" );
					error.setRecomendaciones("CARACTER INVALIDO");
					listErrores.add(error);
				}
			}else{
				for(int iterador=0;iterador<tamano;iterador++){
					if( isInt( splitCadena[iterador]  )){
						BigDecimal decimalValue = new BigDecimal( splitCadena[iterador] );
						int intValue = decimalValue.intValue();
						resultado[iterador]= String.valueOf( intValue );
					}else{
						error.setNumeroInciso(fila);
						error.setNombreSeccion("");
						error.setNombreCampoError(metodo+ " CARACTER INVALIDO" );
						error.setRecomendaciones("CARACTER INVALIDO");
						listErrores.add(error);
					}
					
				}
			}
		}
		
		return resultado;
	}
	

	private ClienteGenericoDTO crearCliente(DetalleCargaMasivaIndAutoCot detalle,  
			StringBuilder mensajeError, BigDecimal numeroInciso, Long idToNegocio){
		ClienteGenericoDTO cliente = new ClienteGenericoDTO();
		
		short claveTipoPersona = (detalle.getTipoPersonaCliente());
		DecimalFormat decFormat = new DecimalFormat("00000");
		String postalCode = decFormat.format(Integer.parseInt(detalle.getCodigoPostalCliente().trim()));
		List<ColoniaMidas> listColonias = direccionMidasService.obtenerColoniasPorCP(postalCode);
		ColoniaMidas colonia = null;
		if(!listColonias.isEmpty()){
			//1. buscando colonia identica a la capturada
			for(ColoniaMidas col : listColonias){
				if(col.getDescripcion().equalsIgnoreCase(detalle.getColoniaCliente())){
					colonia = col;
					break;
				}
			}
			//2. si no encuentra colonia identica busca la mas parecida (menos diferente)
			if(colonia == null){
				colonia = buscarColoniaSimilar(listColonias, detalle.getColoniaCliente());
			}
		}
		//3. si despues de esto no se asigno colonia lanza error 
		if(colonia == null){ 
			LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
			error.setNumeroInciso(numeroInciso);
			error.setNombreSeccion("");
			error.setNombreCampoError("COLONIA CLIENTE, CODIGO POSTAL CLIENTE");
			error.setRecomendaciones("Error al crear el cliente. La colonia no coincide con el codigo postal");
			listErrores.add(error);
			mensajeError.append("Campo: ").append(error.getNombreCampoError()).append(" - ").append(error.getRecomendaciones()).append("\n");
		}else{
		
//		DATOS GENERALES
		cliente.setIdNegocio(idToNegocio);
		
		cliente.setClaveTipoPersona(claveTipoPersona);
		cliente.setNombre(detalle.getNombreORazonSocial());
		
		cliente.setFechaNacimiento(obtenerFechaNacimientoCliente(detalle.getRfcCliente()));

		if(claveTipoPersona == ClienteFacadeRemote.CLAVE_PERSONA_MORAL){
			cliente.setRazonSocial(detalle.getNombreORazonSocial());
			cliente.setRazonSocialFiscal(detalle.getNombreORazonSocial());
			cliente.setFechaConstitucion(cliente.getFechaNacimiento());
			cliente.setNombreContacto(detalle.getNombreORazonSocial());
			cliente.setFechaNacimientoFiscal(cliente.getFechaConstitucion());
			PersonaSeycosDTO representanteLegal = obtenerRepresentanteLegalCliente(detalle, mensajeError, numeroInciso);
			cliente.setIdRepresentante((representanteLegal != null && representanteLegal.getIdPersona() != null) ? BigDecimal.valueOf(representanteLegal.getIdPersona()) : BigDecimal.ZERO); 
		}
		cliente.setApellidoPaterno(detalle.getApellidoPaternoCliente());
		cliente.setApellidoMaterno(detalle.getApellidoMaternoCliente());
		cliente.setSexo("M");
		
		cliente.setCodigoRFC(detalle.getRfcCliente());
		cliente.setTelefono(detalle.getTelefonoCliente());
		cliente.setEstadoCivil("S");
		
		cliente.setClaveNacionalidad(colonia.getCiudad().getEstado().getPais().getId());
		cliente.setClaveEstadoNacimiento(colonia.getCiudad().getEstado().getId());
		cliente.setIdEstadoString(colonia.getCiudad().getEstado().getId());
		cliente.setEstadoNacimiento(colonia.getCiudad().getEstado().getDescripcion());
		cliente.setClaveCiudadNacimiento(StringUtils.leftPad(colonia.getCiudad().getId(), 5, '0'));
		
		cliente.setNombreCalle(detalle.getCalleCliente());
		cliente.setCodigoPostal(detalle.getCodigoPostalCliente());
		cliente.setNombreColonia(colonia.getDescripcion());
		cliente.setIdColoniaString(colonia.getId());
		
		
		cliente.setIdMunicipioString(StringUtils.leftPad(colonia.getCiudad().getId(), 5, '0'));
		cliente.setTipoSituacionString("A");
		
//		DATOS FISCALES
		cliente.setCodigoRFCFiscal(detalle.getRfcCliente());
		cliente.setIdEstadoFiscal(colonia.getCiudad().getEstado().getId());
		cliente.setTelefonoFiscal(detalle.getTelefonoCliente());
		cliente.setCodigoPostalFiscal(detalle.getCodigoPostalCliente());
		cliente.setNombreColoniaFiscal(colonia.getDescripcion());
		cliente.setNombreCalleFiscal(detalle.getCalleCliente());
		cliente.setIdMunicipioFiscal(StringUtils.leftPad(colonia.getCiudad().getId(), 5, '0'));
		cliente.setNombreFiscal(detalle.getNombreORazonSocial());
		cliente.setApellidoPaternoFiscal(detalle.getApellidoPaternoCliente());
		cliente.setApellidoMaternoFiscal(detalle.getApellidoMaternoCliente());
		
		
		try{ 
			cliente = clienteFacadeRemote.saveFullData(cliente, null, true);
		}catch(Exception ex){
			LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
			error.setNumeroInciso(numeroInciso);
			error.setNombreSeccion("");
			error.setNombreCampoError("CLIENTE");
			error.setRecomendaciones("Error al crear el cliente");
			listErrores.add(error);
			mensajeError.append("Campo: ").append(error.getNombreCampoError()).append(" - ").append(error.getRecomendaciones()).append("\n");
		}
		}
		
		return cliente;
	}
	
	/**
	 * Si el id del representante legal (seycos.persona) es proporcionado, busca en el catalogo de personas
	 * de lo contrario crea una persona nueva con la informacion proporcionada en el excel de carga masiva
	 * @param detalle
	 * @param mensajeError
	 * @param numeroInciso
	 * @return PersonaSeycosDTO con la informacion del cliente encontrado/creado
	 */
	private PersonaSeycosDTO obtenerRepresentanteLegalCliente(DetalleCargaMasivaIndAutoCot detalle, 
			StringBuilder mensajeError, BigDecimal numeroInciso){
		PersonaSeycosDTO personaSeycos = null;
		
		try{
			if(detalle.getIdRepresentante() != null){ //Si se proporciona el ID del representante se intenta buscar
				personaSeycos = personaSeycosFacadeRemote.findById(detalle.getIdRepresentante());
			}
			if(personaSeycos == null){// En caso de no proporcionar ID o no encontrar resultado se crea el representante
				
				//	BUSCANDO INFO DOMICILIO POR CODIGO POSTAL
				DecimalFormat decFormat = new DecimalFormat("00000");
				String postalCode = decFormat.format(Integer.parseInt(detalle.getCodigoPostalRepresentante().trim()));
				List<ColoniaMidas> listColonias = direccionMidasService.obtenerColoniasPorCP(postalCode);
				ColoniaMidas colonia = null;
				if(!listColonias.isEmpty()){
					for(ColoniaMidas col : listColonias){
						if(col.getDescripcion().equalsIgnoreCase(detalle.getColoniaRepresentante())){
							colonia = col;
							break;
						}
					}
				}
				if(colonia == null){
						colonia = buscarColoniaSimilar(listColonias, detalle.getColoniaCliente());
				}
				if(colonia == null){
					LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
					error.setNumeroInciso(numeroInciso);
					error.setNombreSeccion("");
					error.setNombreCampoError("COLONIA CLIENTE, CODIGO POSTAL CLIENTE");
					error.setRecomendaciones("Error al crear el cliente. La colonia no coincide con el codigo postal");
					listErrores.add(error);
					mensajeError.append("Campo: ").append(error.getNombreCampoError()).append(" - ").append(error.getRecomendaciones()).append("\n");
				}else{
				
					personaSeycos = new PersonaSeycosDTO();
					personaSeycos.setFechaAlta(new Date());
					personaSeycos.setSituacionPersona("AC");
					personaSeycos.setClaveTipoPersona(detalle.getTipoPersonaRepresentante());
					personaSeycos.setNombre(detalle.getNombreRazonRepresentante());
					personaSeycos.setApellidoPaterno(detalle.getApellidoPaternoRepresentante());
					personaSeycos.setApellidoMaterno(detalle.getApellidoMaternoRepresentante());
					personaSeycos.setSexo("M");
					personaSeycos.setEstadoCivil("S");
					personaSeycos.setClavePaisNacimiento("PAMEXI");
					personaSeycos.setClaveEstadoNacimiento(colonia.getCiudad().getEstado().getId());
					personaSeycos.setClaveCiudadNacimiento(colonia.getCiudad().getId());
					personaSeycos.setFechaNacimiento(new Date()); 
					personaSeycos.setCodigoRFC(detalle.getRfcRepresentante());
					personaSeycos.setCodigoCURP("");
					personaSeycos.setTelOficina(detalle.getTelefonoRepresentante());
					personaSeycos.setTelCasa(detalle.getTelefonoRepresentante());
					personaSeycos.setTelCelular(detalle.getTelefonoRepresentante());
					personaSeycos.setCorreosAdicionales("");
					//	DOMICILIO PERSONAL 
					personaSeycos.setDomicilios(new ArrayList<Domicilio>());
					Domicilio domicilio = new Domicilio();
					domicilio.setTipoDomicilio(TipoDomicilio.PERSONAL.getValue());
					domicilio.setCalleNumero(detalle.getCalleRepresentante());
					domicilio.setCodigoPostal(detalle.getCodigoPostalRepresentante());
					domicilio.setNombreColonia(colonia.getDescripcion());
					domicilio.setClaveCiudad(colonia.getCiudad().getId());
					domicilio.setClaveEstado(colonia.getCiudad().getEstado().getId());
					domicilio.setClavePais(colonia.getCiudad().getEstado().getPais().getId());
					personaSeycos.getDomicilios().add(domicilio);
					//	DOMICILIO FISCAL
					domicilio = new Domicilio();
					domicilio.setTipoDomicilio(TipoDomicilio.FISCAL.getValue());
					domicilio.setCalleNumero(detalle.getCalleRepresentante());
					domicilio.setCodigoPostal(detalle.getCodigoPostalRepresentante());
					domicilio.setNombreColonia(colonia.getDescripcion());
					domicilio.setClaveCiudad(colonia.getCiudad().getId());
					domicilio.setClaveEstado(colonia.getCiudad().getEstado().getId());
					domicilio.setClavePais(colonia.getCiudad().getEstado().getPais().getId());
					personaSeycos.getDomicilios().add(domicilio);
					//	DOMICILIO OFICINA
					domicilio = new Domicilio();
					domicilio.setTipoDomicilio(TipoDomicilio.OFICINA.getValue());
					domicilio.setCalleNumero(detalle.getCalleRepresentante());
					domicilio.setCodigoPostal(detalle.getCodigoPostalRepresentante());
					domicilio.setNombreColonia(colonia.getDescripcion());
					domicilio.setClaveCiudad(colonia.getCiudad().getId());
					domicilio.setClaveEstado(colonia.getCiudad().getEstado().getId());
					domicilio.setClavePais(colonia.getCiudad().getEstado().getPais().getId());
					personaSeycos.getDomicilios().add(domicilio);
					
					personaSeycos = personaSeycosFacadeRemote.save(personaSeycos);
					personaSeycos = personaSeycosFacadeRemote.saveContactData(personaSeycos);
				}
			}
		}catch(Exception ex){
			LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
			error.setNumeroInciso(numeroInciso);
			error.setNombreSeccion("");
			error.setNombreCampoError("CLIENTE");
			error.setRecomendaciones("No se pudo configurar el representante legal del cliente");
			listErrores.add(error);
			mensajeError.append("Campo: ").append(error.getNombreCampoError()).append(" - ").append(error.getRecomendaciones()).append("\n");
		}
			
		return personaSeycos;
	}
	
	private Date obtenerFechaNacimientoCliente(String RFC){
		Matcher matcher = Pattern.compile("\\d+").matcher(RFC);
		matcher.find();
		String fechaRFC = matcher.group();
		
		Integer fechaAnio = Integer.valueOf(fechaRFC.substring(0, 2));
		Integer fechaMes = Integer.valueOf(fechaRFC.substring(2, 4));
		Integer fechaDia = Integer.valueOf(fechaRFC.substring(4, 6));
		
		int anioActual = DateTime.now().getYear();
		int anioActualCorto = anioActual % 100;
		fechaAnio = (fechaAnio <= anioActualCorto)? 
				anioActual - anioActualCorto + fechaAnio : 
					anioActual - anioActualCorto - 100 + fechaAnio; 
		
		DateTime fechaNacimiento = new DateTime(fechaAnio, fechaMes, fechaDia, 0, 0, 0, 0);
		
		return fechaNacimiento.toDate();
	}
	
	private ColoniaMidas buscarColoniaSimilar(List<ColoniaMidas> listColonias, String coloniaCliente){
		ColoniaMidas colonia = null;
		int distanciaMenor = 1000; // se inicializa con un numero alto
		int distanciaColoniaActual;
		
		for(ColoniaMidas col : listColonias){
			distanciaColoniaActual = StringUtils.getLevenshteinDistance(coloniaCliente, col.getDescripcion()); 
			if(distanciaColoniaActual < distanciaMenor){
				colonia = col;
				distanciaMenor = distanciaColoniaActual;
			}
		}
		
		return colonia;
	}

	
	public void validaDetalleCargaMasiva(
			List<DetalleCargaMasivaIndAutoCot> detalleCargaMasivaList, HSSFSheet sheet) {
StringBuilder mensajeError = new StringBuilder("");
		int fila = 1;
		for (DetalleCargaMasivaIndAutoCot detalle : detalleCargaMasivaList) {
			fila++;
			BigDecimal filaDecimal = BigDecimal.valueOf(fila);
			try{
			if (detalle.getClaveEstatus() != null && !detalle.getClaveEstatus().equals(
					DetalleCargaMasivaAutoCot.ESTATUS_ERROR)) {
				
				//String mensajeError = "";
				mensajeError.delete(0, mensajeError.length());
				CotizacionDTO cotizacion = new CotizacionDTO();
				
				Negocio negocio = negocioTipoPoliza.getNegocioProducto().getNegocio();
				SolicitudDTO solicitud = new SolicitudDTO();
				solicitud.setNegocio(negocio);
				
				
				//Codigo Agente				
				solicitud.setCodigoAgente(detalle.getIdAgente());
				Agente agente = cargaMasivaService.validaAgente(solicitud.getCodigoAgente().longValue(), "A", negocio.getIdToNegocio());
				if(agente == null){
					LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
					error.setNumeroInciso(new BigDecimal(fila));
					error.setNombreSeccion("");
					error.setNombreCampoError("AGENTE");
					error.setRecomendaciones("Agente Invalido");
					listErrores.add(error);
					mensajeError.append("Campo: ").append(error.getNombreCampoError()).append(" - ").append(error.getRecomendaciones()).append("\n");
				}else{
					solicitud.setCodigoAgente(new BigDecimal(agente.getId()));
				}
				
				//Moneda
				if(detalle.getMoneda().equals(DOLARES)){
					cotizacion.setIdMoneda(new BigDecimal(840));
				}
				if(detalle.getMoneda().equals(PESOS)){
					cotizacion.setIdMoneda(new BigDecimal(484));
				}
				
				cotizacion.setSolicitudDTO(solicitud);
				cotizacion.setNegocioTipoPoliza(getNegocioTipoPoliza());
				
				cotizacion = cargaMasivaService.crearCotizacion(cotizacion);
				
				cotizacion.setFechaInicioVigencia(detalle.getFechaVigenciaInicio());
				cotizacion.setFechaFinVigencia(detalle.getFechaVigenciaFin());
				
				FormaPagoDTO formaPago = cargaMasivaService.obtieneFormaPago(detalle.getFormaPago());
				if(formaPago != null){
					cotizacion.setIdFormaPago(new BigDecimal(formaPago.getIdFormaPago()));
				}else{
					LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
					error.setNumeroInciso(new BigDecimal(fila));
					error.setNombreSeccion("");
					error.setNombreCampoError("FORMA DE PAGO");
					error.setRecomendaciones("Forma de Pago Invalido");
					listErrores.add(error);
					mensajeError.append("Campo: ").append(error.getNombreCampoError()).append(" - ").append(error.getRecomendaciones()).append("\n");
				}
				
				//Medio de Pago
				if(detalle.getConductoCobro() != null){
					cotizacion.setIdMedioPago(new BigDecimal(detalle.getIdMedioPago()));
				}else{
					LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
					error.setNumeroInciso(new BigDecimal(fila));
					error.setNombreSeccion("");
					error.setNombreCampoError("MEDIO DE PAGO");
					error.setRecomendaciones("Medio de Pago no definido en sistema");
					listErrores.add(error);
					mensajeError.append("Campo: ").append(error.getNombreCampoError()).append(" - ").append(error.getRecomendaciones()).append("\n");
			}
				
				NegocioDerechoPoliza negocioDerechoPoliza = cargaMasivaService.obtieneNegocioDerechoPoliza(new BigDecimal(negocio.getIdToNegocio()), detalle.getDerechos());
				if(negocioDerechoPoliza != null){
					cotizacion.setNegocioDerechoPoliza(negocioDerechoPoliza);
				}else{
					LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
					error.setNumeroInciso(new BigDecimal(fila));
					error.setNombreSeccion("");
					error.setNombreCampoError("DERECHOS");
					error.setRecomendaciones("Derechos Invalidos");
					listErrores.add(error);
					mensajeError.append("Campo: ").append(error.getNombreCampoError()).append(" - ").append(error.getRecomendaciones()).append("\n");

				}
				
				//Bono Comision
				if(cotizacion.getPorcentajebonifcomision() == null && cotizacion.getSolicitudDTO().getNegocio().getPctPrimaCeder() != null){
					cotizacion.setPorcentajebonifcomision(cotizacion.getSolicitudDTO().getNegocio().getPctPrimaCeder());
				}
				// Obtiene el descuento global default
				if (cotizacion.getSolicitudDTO().getNegocio().getPctDescuentoDefault() != null && cotizacion.getPorcentajeDescuentoGlobal() == null) {
				  cotizacion.setPorcentajeDescuentoGlobal(new Double(0)); //Dejara de aplicarse el descuento global
				}
				
				//Coloca PctPagoFraccionado si negocio lo permite
				try{
					if(negocio.getAplicaPctPagoFraccionado() && detalle.getPorcentajePagoFraccionado() != null){
						cotizacion.setPorcentajePagoFraccionado(detalle.getPorcentajePagoFraccionado());
					}
				}catch(Exception e){
				}
				
				if(cotizacion.getPorcentajePagoFraccionado() == null){
					try{
						Double pctPagoFrac = cargaMasivaService
								.getPctePagoFraccionado(cotizacion
										.getIdFormaPago().intValue(),
										cotizacion.getIdMoneda().shortValue());
						if (pctPagoFrac == null) {
							cotizacion.setPorcentajePagoFraccionado(new Double(
									0));
						} else {
							cotizacion
									.setPorcentajePagoFraccionado(pctPagoFrac);
						}
					}catch(Exception e){
						cotizacion.setPorcentajePagoFraccionado(new Double(0));
					}
				}
				
				//IVA
				Double iva = cargaMasivaService.obtieneIVAPorCodigoPostalColonia(detalle.getCodigoPostalCliente(), detalle.getColoniaCliente());
				cotizacion.setPorcentajeIva(iva);
				
				//Contratante
				List<ClienteGenericoDTO> clientes = cargaMasivaService.validaClienteContratante(detalle);
				ClienteGenericoDTO clienteNuevo = null;
				
				Boolean domicilioValido = false;
				
				if(clientes != null && !clientes.isEmpty()) {
					
					for (ClienteGenericoDTO cliente : clientes) {
						
						if (cliente.getIdDomicilioConsulta() != null && cliente.getCodigoPostalFiscal() != null
								&& !cliente.getCodigoPostalFiscal().trim().equals("")) {
							
							if (((detalle.getColoniaCliente() == null || detalle.getColoniaCliente().trim().equals(""))
									&& (detalle.getCodigoPostalCliente() == null || detalle.getCodigoPostalCliente().trim().equals("")))
								|| ((detalle.getColoniaCliente() != null 
										&& detalle.getColoniaCliente().trim().equals(cliente.getNombreColoniaFiscal().trim()))
									&&(detalle.getCodigoPostalCliente() != null 
												&& detalle.getCodigoPostalCliente().trim().equals(cliente.getCodigoPostalFiscal().trim())))) {
								//No se valida la direccion porque no existe en la plantilla
								
								cotizacion.setNombreAsegurado(cliente.getNombreCompleto());
								cotizacion.setIdToPersonaAsegurado(cliente.getIdCliente());
								cotizacion.setIdDomicilioContratante(BigDecimal.valueOf(cliente.getIdDomicilioConsulta()));
								cotizacion.setIdToPersonaContratante(cliente.getIdCliente());
								cotizacion.setNombreContratante(cliente.getNombreCompleto());
								cotizacion.getSolicitudDTO().setClaveTipoPersona(cliente.getClaveTipoPersona());
								
								domicilioValido = true;
								break;
							}
							
						}
						
					}
						
					if (!domicilioValido) {
						clienteNuevo = crearCliente(detalle, mensajeError, BigDecimal.valueOf(fila), negocio.getIdToNegocio());
//						LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
//						error.setNumeroInciso(new BigDecimal(fila));
//						error.setNombreSeccion("");
//						error.setNombreCampoError("CLIENTE");
//						error.setRecomendaciones("Favor de Verificar Codigo Postal y Domicilio correspondiente con Domicilio Fiscal");
//						listErrores.add(error);
//						mensajeError.append("Campo: ").append(error.getNombreCampoError()).append(" - ").append(error.getRecomendaciones()).append("\n");
					}
					
				}else{
					clienteNuevo =crearCliente(detalle, mensajeError, BigDecimal.valueOf(fila), negocio.getIdToNegocio());
//					LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
//					error.setNumeroInciso(new BigDecimal(fila));
//					error.setNombreSeccion("");
//					error.setNombreCampoError("CLIENTE");
//					error.setRecomendaciones("Cliente No Existe");
//					listErrores.add(error);
//					mensajeError.append("Campo: ").append(error.getNombreCampoError()).append(" - ").append(error.getRecomendaciones()).append("\n");
				}
				
				if(clienteNuevo != null && clienteNuevo.getIdCliente() != null){
					List<ClienteGenericoDTO> resultado = clienteFacadeRemote.findByIdRFC(clienteNuevo, null);
					if(resultado != null
							&& !resultado.isEmpty()){
						clienteNuevo = resultado.get(0);
						cotizacion.setNombreAsegurado(clienteNuevo.getNombreCompleto());
						cotizacion.setIdToPersonaAsegurado(clienteNuevo.getIdCliente());
						cotizacion.setIdDomicilioContratante(BigDecimal.valueOf(clienteNuevo.getIdDomicilioConsulta()));
						cotizacion.setIdToPersonaContratante(clienteNuevo.getIdCliente());
						cotizacion.setNombreContratante(clienteNuevo.getNombreCompleto());
						cotizacion.getSolicitudDTO().setClaveTipoPersona(clienteNuevo.getClaveTipoPersona());
					}
				}else if(!domicilioValido){
					LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
					error.setNumeroInciso(new BigDecimal(fila));
					error.setNombreSeccion("");
					error.setNombreCampoError("CLIENTE");
					error.setRecomendaciones("Error al asignar CLIENTE a la cotizacion");
					listErrores.add(error);
					mensajeError.append("Campo: ").append(error.getNombreCampoError()).append(" - ").append(error.getRecomendaciones()).append("\n");
				}
				
				//IdClienteConductoCobro
				int conductoCobro = 0;
				if(clienteNuevo != null && clienteNuevo.getIdCliente() != null){
					conductoCobro = listadoService.getConductosCobro(clienteNuevo.getIdCliente().longValue(), Integer.parseInt(detalle.getIdMedioPago())).size()-1;
					conductoCobro = (conductoCobro>0)? conductoCobro : 1;
					cotizacion.setIdConductoCobroCliente(new Long(conductoCobro));
				}
				
				cotizacion = cargaMasivaService.guardarCotizacion(cotizacion);
				
				detalle.setIdToCotizacion(cotizacion.getIdToCotizacion());
				
				HSSFCell cell = sheet.getRow((fila - 1)).getCell(CAMPO_NUMERO_COTIZACION);
				if(cell == null){
					cell = sheet.getRow((fila - 1)).createCell(CAMPO_NUMERO_COTIZACION);
				}
				cell.setCellValue(cotizacion.getIdToCotizacion().toPlainString());
				
				
				//INCISO
				boolean requiereAutorizacion = false;
				if(mensajeError.toString().isEmpty()){
					IncisoCotizacionDTO incisoCotizacion = new IncisoCotizacionDTO();
					IncisoCotizacionId incisoCotizacionId = new IncisoCotizacionId();
					incisoCotizacionId.setIdToCotizacion(cotizacion
							.getIdToCotizacion());
					incisoCotizacion.setCotizacionDTO(cotizacion);
					incisoCotizacion.setId(incisoCotizacionId);
					
					//Correo
					if(detalle.getEmailContactos() != null){
						incisoCotizacion.setEmailContacto(detalle.getEmailContactos());
					}					
					
					IncisoAutoCot incisoAutoCot = new IncisoAutoCot();

					NegocioSeccion negocioSeccion = null;
					negocioSeccion = cargaMasivaService
							.obtieneNegocioSeccionPorDescripcion(cotizacion,
									this.remplazaGuion(detalle.getLineaNegocio()));
					if (negocioSeccion != null) {
						incisoAutoCot.setNegocioSeccionId(negocioSeccion
								.getIdToNegSeccion().longValue());
					} else {
						LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
						error.setNumeroInciso(new BigDecimal(fila));
						error.setNombreSeccion("");
						error.setNombreCampoError("LINEA DE NEGOCIO");
						error.setRecomendaciones("Linea de Negocio Invalida");
						listErrores.add(error);
						mensajeError.append("Campo: LINEA DE NEGOCIO -  Linea de Negocio Invalida \n");
					}
					
					NegocioTipoUso negocioTipoUso = cargaMasivaService
							.obtieneNegocioTipoUsoPorDescripcion(
									negocioSeccion,
									detalle.getClaveUso());
					if (negocioTipoUso != null) {
						incisoAutoCot.setTipoUsoId(negocioTipoUso
								.getTipoUsoVehiculoDTO()
								.getIdTcTipoUsoVehiculo().longValue());
					} else {
						LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
						error.setNumeroInciso(new BigDecimal(fila));
						error.setNombreSeccion("");
						error.setNombreCampoError("TIPO DE USO");
						error.setRecomendaciones("TipoUso Invalido");
						listErrores.add(error);
						mensajeError.append("Campo: TIPO DE USO -  TipoUso Invalido \n");
					}
					
					EstiloVehiculoDTO estiloVehiculo = cargaMasivaService
							.obtieneEstiloVehiculoDTOPorClaveAMIS(cotizacion,
									negocioSeccion, detalle.getClaveAMIS());
					if (estiloVehiculo != null) {
						incisoAutoCot.setMarcaId(estiloVehiculo
								.getMarcaVehiculoDTO().getIdTcMarcaVehiculo());
						incisoAutoCot.setEstiloId(estiloVehiculo.getId()
								.getStrId());
						try{
							incisoAutoCot.setClaveTipoBien(estiloVehiculo.getId().getClaveTipoBien());
							incisoAutoCot.setIdVersionCarga(estiloVehiculo.getId().getIdVersionCarga());
							incisoAutoCot.setIdMoneda(cotizacion.getIdMoneda().shortValue());
						}catch(Exception e){
						}
						if(detalle.getDescripcionVehiculo() != null && !detalle.getDescripcionVehiculo().isEmpty()){
							incisoAutoCot.setDescripcionFinal(detalle.getDescripcionVehiculo().toUpperCase());
						}else{
							incisoAutoCot.setDescripcionFinal(estiloVehiculo.getDescripcionEstilo());
							detalle.setDescripcionVehiculo(estiloVehiculo.getDescripcionEstilo().toUpperCase());
						}
					} else {
						LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
						error.setNumeroInciso(new BigDecimal(fila));
						error.setNombreSeccion("");
						error.setNombreCampoError("CLAVE AMIS");
						error.setRecomendaciones("Clave AMIS Invalida");
						listErrores.add(error);
						mensajeError.append("Campo: CLAVE AMIS -  Clave AMIS Invalida \n");
					}
					
					if(estiloVehiculo != null){
					if(cargaMasivaService.validaModeloVehiculo(cotizacion.getIdMoneda(), estiloVehiculo.getId().getStrId(), 
							negocioSeccion.getIdToNegSeccion(), detalle.getModelo())){
						incisoAutoCot.setModeloVehiculo(detalle.getModelo());
					}else{
						LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
						error.setNumeroInciso(new BigDecimal(fila));
						error.setNombreSeccion("");
						error.setNombreCampoError("MODELO VEHICULO");
						error.setRecomendaciones("Modelo Vehiculo Invalido");
						listErrores.add(error);
						mensajeError.append("Campo: ").append(error.getNombreCampoError()).append(" - ").append(error.getRecomendaciones()).append("\n");
					}
					}
					

					NegocioPaqueteSeccion negocioPaquete = cargaMasivaService
							.obtieneNegocioPaqueteSeccionPorDescripcion(
									negocioSeccion, detalle.getPaquete());

					if (negocioPaquete != null) {
						incisoAutoCot.setNegocioPaqueteId(negocioPaquete
								.getIdToNegPaqueteSeccion());
					} else {
						LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
						error.setNumeroInciso(new BigDecimal(fila));
						error.setNombreSeccion("");
						error.setNombreCampoError("PAQUETE");
						error.setRecomendaciones("Paquete Invalido");
						listErrores.add(error);
						mensajeError.append("Campo: PAQUETE -  Paquete Invalido \n");
					}
					
					String municipioId = cargaMasivaService
							.obtieneMunicipioIdPorCodigoPostal(detalle.getCodigoPostalCliente());
					if (municipioId != null) {
						incisoAutoCot.setMunicipioId(municipioId);

						
						String estadoId = cargaMasivaService
								.obtieneEstadoIdPorMunicipio(municipioId);
						incisoAutoCot.setEstadoId(estadoId);
						
						if(!cargaMasivaService.validaEstadoMunicipio(negocio.getIdToNegocio(), estadoId, municipioId)){
							LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
							error.setNumeroInciso(new BigDecimal(fila));
							error.setNombreSeccion("");
							error.setNombreCampoError("CODIGO POSTAL");
							error.setRecomendaciones("Codigo Postal Invalido");
							listErrores.add(error);
							mensajeError.append("Campo: CODIGO POSTAL -  Codigo Postal Invalido \n");							
						}

					} else {
						LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
						error.setNumeroInciso(new BigDecimal(fila));
						error.setNombreSeccion("");
						error.setNombreCampoError("CODIGO POSTAL");
						error.setRecomendaciones("Codigo Postal Invalido");
						listErrores.add(error);
						mensajeError.append("Campo: CODIGO POSTAL -  Codigo Postal Invalido \n");
					}
					
					
					//Otorgar el maximo descuento por negocio-estado permitido
					Double pctDescuentoEstado = 0.0;
					if(cargaMasivaService.getAplicaDescuentoNegocioPaqueteSeccion(incisoAutoCot.getNegocioPaqueteId())) {
						NegocioEstadoDescuento negocioEstadoDescuento = new NegocioEstadoDescuento();
						negocioEstadoDescuento = cargaMasivaService.findByNegocioAndEstado(
								cotizacion.getSolicitudDTO().getNegocio().getIdToNegocio(), 
								incisoAutoCot.getEstadoId());
						if(negocioEstadoDescuento != null && negocioEstadoDescuento.getPctDescuentoDefault() != null) {
							pctDescuentoEstado = negocioEstadoDescuento.getPctDescuentoDefault();
						}
					}
					incisoAutoCot.setPctDescuentoEstado(pctDescuentoEstado);
					
					
					List<DatoIncisoCotAuto> datoIncisoCotAutos = new ArrayList<DatoIncisoCotAuto>();
					
					if (mensajeError.toString().isEmpty()) {
						incisoCotizacion.setIncisoAutoCot(incisoAutoCot);
						List<CoberturaCotizacionDTO> coberturaCotizacionList = cargaMasivaService
								.obtieneCoberturasDelInciso(incisoCotizacion,
										negocioPaquete, estiloVehiculo.getId());
						
						
					    if(coberturaCotizacionList != null && !coberturaCotizacionList.isEmpty()){
							Short claveContrato = 1;
							Short claveObligatoriedad = 0;
							Short valorSI = 1;
							Short valorNO = 0;
							
							for(CoberturaCotizacionDTO coberturaCotizacion : coberturaCotizacionList){
				    			//Valida si es obligatoria
								Boolean esObligatoria = false;
				    			if(coberturaCotizacion.getClaveContrato().equals(claveContrato) && 
				    					coberturaCotizacion.getClaveObligatoriedad().equals(claveObligatoriedad)){
				    				esObligatoria = true;
				    			}
				    			
				    			//Valida Permite Suma Asegurada
				    			Boolean permiteSumaAsegurada = false;				    			
				    			if(coberturaCotizacion.getCoberturaSeccionDTO().getCoberturaDTO().getClaveFuenteSumaAsegurada().equals(CoberturaDTO.CLAVE_FUENTE_SA_VALOR_PROPORCIONADO) ||
				    					coberturaCotizacion.getCoberturaSeccionDTO().getCoberturaDTO().getClaveFuenteSumaAsegurada().equals(CoberturaDTO.CLAVE_FUENTE_SA_VALOR_FACTURA)){
				    				permiteSumaAsegurada = true;
				    			}
				    			
				    			//Valida Rangos
				    			Boolean validaRangos = false;
				    			if(coberturaCotizacion.getCoberturaSeccionDTO().getCoberturaDTO().getClaveFuenteSumaAsegurada().equals(CoberturaDTO.CLAVE_FUENTE_SA_VALOR_PROPORCIONADO) &&
				    					coberturaCotizacion.getValorSumaAseguradaMin() != 0 && coberturaCotizacion.getValorSumaAseguradaMax() != 0){
				    				validaRangos = true;
				    			}
				    			
				    			//Valida Deducibles
				    			Boolean validaDeducibles = false;
				    			if(!coberturaCotizacion.getCoberturaSeccionDTO().getCoberturaDTO().getClaveTipoDeducible().equals("0")){
				    				validaDeducibles = true;
				    			}
				    			
				    			System.out.println("Cobertura: " + coberturaCotizacion.getCoberturaSeccionDTO().getCoberturaDTO().getNombreComercial());
					    		if(coberturaCotizacion.getCoberturaSeccionDTO().getCoberturaDTO().getNombreComercial().toUpperCase().equals(
					    				GeneraExcelCargaMasiva.NOMBRE_DEDUCIBLE_DANOS_MATERIALES.toUpperCase())){
					    			if(permiteSumaAsegurada && detalle.getValorSumaAseguradaDM() != null){
					    				boolean valido = true;
						    			if(validaRangos){
						    				valido = validaRangos(coberturaCotizacion.getValorSumaAseguradaMin(), 
						    						coberturaCotizacion.getValorSumaAseguradaMax(), detalle.getValorSumaAseguradaDM());
						    			}
						    			if(valido){
					    					coberturaCotizacion.setValorSumaAsegurada(detalle.getValorSumaAseguradaDM());
					    				}else{
					    					LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
					    					error.setNumeroInciso(new BigDecimal(fila));
					    					error.setNombreSeccion("");
					    					error.setNombreCampoError("SUMA ASEGURADA DANOS MATERIALES");
					    					error.setRecomendaciones("Valor no valido");
					    					listErrores.add(error);
					    					mensajeError.append("Campo: SUMA ASEGURADA DM -  Valor debe ser entre ").append(coberturaCotizacion.getValorSumaAseguradaMin()).append(
					    					" y ").append(coberturaCotizacion.getValorSumaAseguradaMax()).append(" ");
					    				}
					    			}
					    			if(detalle.getDeducibleDanosMateriales() != null){
					    				boolean valido = true;
					    				if(validaDeducibles){
					    					valido = validaDeducible(coberturaCotizacion.getDeducibles(), detalle.getDeducibleDanosMateriales());
					    				}
					    				if(valido){
					    					coberturaCotizacion.setValorDeducible(detalle.getDeducibleDanosMateriales());
					    					coberturaCotizacion.setPorcentajeDeducible(detalle.getDeducibleDanosMateriales());
					    					coberturaCotizacion.setClaveContrato(claveContrato);
					    				}else{
					    					LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
					    					error.setNumeroInciso(new BigDecimal(fila));
					    					error.setNombreSeccion("");
					    					error.setNombreCampoError("DEDUCIBLE DA\u00d1OS MATERIALES");
					    					error.setRecomendaciones("Valor no valido");
					    					listErrores.add(error);
											mensajeError.append("Campo: ").append(error.getNombreCampoError()).append(" - ").append(error.getRecomendaciones()).append("\n");
					    				}
					    			}
						 		}else if(coberturaCotizacion.getCoberturaSeccionDTO().getCoberturaDTO().getNombreComercial().toUpperCase().equals(
						 				GeneraExcelCargaMasiva.NOMBRE_DEDUCIBLE_ROBO_TOTAL.toUpperCase())){
						 			if(permiteSumaAsegurada && detalle.getValorSumaAseguradaRT() != null){
					    				boolean valido = true;
						    			if(validaRangos){
						    				valido = validaRangos(coberturaCotizacion.getValorSumaAseguradaMin(), 
						    						coberturaCotizacion.getValorSumaAseguradaMax(), detalle.getValorSumaAseguradaRT());
						    			}
						    			if(valido){
					    					coberturaCotizacion.setValorSumaAsegurada(detalle.getValorSumaAseguradaRT());
					    				}else{
					    					LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
					    					error.setNumeroInciso(new BigDecimal(fila));
					    					error.setNombreSeccion("");
					    					error.setNombreCampoError("SUMA ASEGURADA ROBO TOTAL");
					    					error.setRecomendaciones("Valor no valido");
					    					listErrores.add(error);
					    					mensajeError.append("Campo: SUMA ASEGURADA RT -  Valor debe ser entre ").append(coberturaCotizacion.getValorSumaAseguradaMin()).append(
					    					" y ").append(coberturaCotizacion.getValorSumaAseguradaMax()).append(" ");
					    				}
					    			}						 			
					    			if(detalle.getDeducibleRoboTotal() != null){
					    				boolean valido = true;
					    				if(validaDeducibles){
					    					valido = validaDeducible(coberturaCotizacion.getDeducibles(), detalle.getDeducibleRoboTotal());
					    				}
					    				if(valido){
					    					coberturaCotizacion.setValorDeducible(detalle.getDeducibleRoboTotal());
					    					coberturaCotizacion.setPorcentajeDeducible(detalle.getDeducibleRoboTotal());
					    					coberturaCotizacion.setClaveContrato(claveContrato);
					    				}else{
					    					LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
					    					error.setNumeroInciso(new BigDecimal(fila));
					    					error.setNombreSeccion("");
					    					error.setNombreCampoError("DEDUCIBLE ROBO TOTAL");
					    					error.setRecomendaciones("Valor no valido");
					    					listErrores.add(error);
											mensajeError.append("Campo: ").append(error.getNombreCampoError()).append(" - ").append(error.getRecomendaciones()).append("\n");
					    				}
					    			}
						 		}else if(coberturaCotizacion.getCoberturaSeccionDTO().getCoberturaDTO().getNombreComercial().toUpperCase().equals(
						 				GeneraExcelCargaMasiva.NOMBRE_LIMITE_RC_TERCEROS.toUpperCase())){
							    		if(detalle.getLimiteRcTerceros() != null){
							    			boolean valido = true;
							    			if(validaRangos){
							    				valido = validaRangos(coberturaCotizacion.getValorSumaAseguradaMin(), 
							    						coberturaCotizacion.getValorSumaAseguradaMax(), detalle.getLimiteRcTerceros());
							    			}
							    			if(valido){
								    			coberturaCotizacion.setValorSumaAsegurada(detalle.getLimiteRcTerceros());
								    			coberturaCotizacion.setClaveContrato(claveContrato);						    				
							    			}else{
						    					LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
						    					error.setNumeroInciso(new BigDecimal(fila));
						    					error.setNombreSeccion("");
						    					error.setNombreCampoError("LIMITE RC TERCEROS");
						    					error.setRecomendaciones("Valor no valido");
						    					listErrores.add(error);
						    					mensajeError.append("Campo: LIMITE RC TERCEROS -  Valor debe ser entre ").append(coberturaCotizacion.getValorSumaAseguradaMin()).append(
						    					" y ").append(coberturaCotizacion.getValorSumaAseguradaMax()).append(" ");
							    			}
							    			//
						    				if(validaDeducibles){
						    					valido = validaDeducible(coberturaCotizacion.getDeducibles(), detalle.getDeducibleRcTerceros());
						    				}
							    			if(valido){
								    			coberturaCotizacion.setValorDeducible(detalle.getDeducibleRcTerceros());
								    			coberturaCotizacion.setPorcentajeDeducible(detalle.getDeducibleRcTerceros());
								    			coberturaCotizacion.setClaveContrato(claveContrato);						    				
							    			}else{
						    					LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
						    					error.setNumeroInciso(new BigDecimal(fila));
						    					error.setNombreSeccion("");
						    					error.setNombreCampoError("DEDUCIBLE RC TERCEROS");
						    					error.setRecomendaciones("Valor no valido");
						    					listErrores.add(error);
												mensajeError.append("Campo: ").append(error.getNombreCampoError()).append(" - ").append(error.getRecomendaciones());
												}
							    		}
						 		}else if(coberturaCotizacion.getCoberturaSeccionDTO().getCoberturaDTO().getNombreComercial().toUpperCase().equals(
						 				GeneraExcelCargaMasiva.NOMBRE_LIMITE_GASTOS_MEDICOS.toUpperCase())){
								    		if(detalle.getLimiteGastosMedicos() != null){
								    			boolean valido = true;
								    			if(validaRangos){
								    				valido = validaRangos(coberturaCotizacion.getValorSumaAseguradaMin(), 
								    						coberturaCotizacion.getValorSumaAseguradaMax(), detalle.getLimiteGastosMedicos());
								    			}
								    			if(valido){
									    			coberturaCotizacion.setValorSumaAsegurada(detalle.getLimiteGastosMedicos());
									    			coberturaCotizacion.setClaveContrato(claveContrato);					    				
								    			}else{
							    					LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
							    					error.setNumeroInciso(new BigDecimal(fila));
							    					error.setNombreSeccion("");
							    					error.setNombreCampoError("LIMITE GASTOS MEDICOS");
							    					error.setRecomendaciones("Valor no valido");
							    					listErrores.add(error);
							    					mensajeError.append("Campo: LIMITE GASTOS MEDICOS -  Valor debe ser entre ").append(coberturaCotizacion.getValorSumaAseguradaMin()).append(
							    					" y ").append(coberturaCotizacion.getValorSumaAseguradaMax()).append(" ");
								    			}
								    		}
						 		}else if(coberturaCotizacion.getCoberturaSeccionDTO().getCoberturaDTO().getNombreComercial().toUpperCase().equals(
						 				GeneraExcelCargaMasiva.NOMBRE_LIMITE_MUERTE.toUpperCase())){
								    		if(detalle.getLimiteMuerte() != null){
								    			boolean valido = true;
								    			if(validaRangos){
								    				valido = validaRangos(coberturaCotizacion.getValorSumaAseguradaMin(), 
								    						coberturaCotizacion.getValorSumaAseguradaMax(), detalle.getLimiteMuerte());
								    			}
								    			if(valido){
									    			coberturaCotizacion.setValorSumaAsegurada(detalle.getLimiteMuerte());
									    			coberturaCotizacion.setClaveContrato(claveContrato);						    				
								    			}else{
							    					LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
							    					error.setNumeroInciso(new BigDecimal(fila));
							    					error.setNombreSeccion("");
							    					error.setNombreCampoError("LIMITE MUERTE");
							    					error.setRecomendaciones("Valor no valido");
							    					listErrores.add(error);
							    					mensajeError.append("Campo: LIMITE MUERTE -  Valor debe ser entre ").append(coberturaCotizacion.getValorSumaAseguradaMin()).append(
							    					" y ").append(coberturaCotizacion.getValorSumaAseguradaMax()).append(" ");
								    			}
								    		}else{
							    				if(!esObligatoria){
								    				coberturaCotizacion.setClaveContrato(valorNO);
								    			}
								    		}
						 		}else if(coberturaCotizacion.getCoberturaSeccionDTO().getCoberturaDTO().getNombreComercial().toUpperCase().equals(
						 				GeneraExcelCargaMasiva.NOMBRE_LIMITE_RC_VIAJERO.toUpperCase())){
								    		if(detalle.getLimiteRcViajero() != null){
								    			boolean valido = true;
								    			if(validaRangos){
								    				valido = validaRangos(coberturaCotizacion.getValorSumaAseguradaMin(), 
								    						coberturaCotizacion.getValorSumaAseguradaMax(), detalle.getLimiteRcViajero());
								    			}
								    			if(valido){
									    			//coberturaCotizacion.setValorSumaAsegurada(detalle.getLimiteRcViajero());
								    				coberturaCotizacion.setDiasSalarioMinimo(detalle.getLimiteRcViajero().intValue());
									    			coberturaCotizacion.setClaveContrato(claveContrato);						    				
								    			}else{
							    					LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
							    					error.setNumeroInciso(new BigDecimal(fila));
							    					error.setNombreSeccion("");
							    					error.setNombreCampoError("LIMITE RC VIAJERO");
							    					error.setRecomendaciones("Valor no valido");
							    					listErrores.add(error);
							    					mensajeError.append("Campo: LIMITE RC VIAJERO -  Valor debe ser entre ").append(coberturaCotizacion.getValorSumaAseguradaMin()).append(
							    					" y ").append(coberturaCotizacion.getValorSumaAseguradaMax()).append(" ");
								    			}
								    		}
						 		}else if(coberturaCotizacion.getCoberturaSeccionDTO().getCoberturaDTO().getNombreComercial().toUpperCase().equals(
						 				GeneraExcelCargaMasiva.NOMBRE_ASISTENCIA_JURIDICA.toUpperCase())){
								    		if(detalle.getAsistenciaJuridica() != null){
								    			if(detalle.getAsistenciaJuridica().equals(valorSI)){
								    				coberturaCotizacion.setClaveContrato(claveContrato);
								    			}else{
								    				if(!esObligatoria){
									    				coberturaCotizacion.setClaveContrato(valorNO);
									    			}
								    			}
								    		}
						 		}else if(coberturaCotizacion.getCoberturaSeccionDTO().getCoberturaDTO().getNombreComercial().toUpperCase().startsWith(
						 				GeneraExcelCargaMasiva.NOMBRE_ASISTENCIA_EN_VIAJES.toUpperCase())){

						 			//
						 		}else if(coberturaCotizacion.getCoberturaSeccionDTO().getCoberturaDTO().getNombreComercial().toUpperCase().equals(
						 				GeneraExcelCargaMasiva.NOMBRE_EXENCION_DEDUCIBLE_DANOS.toUpperCase())){
						 			//
						 		}else if(coberturaCotizacion.getCoberturaSeccionDTO().getCoberturaDTO().getNombreComercial().toUpperCase().equals(
						 				GeneraExcelCargaMasiva.NOMBRE_LIMITE_ACCIDENTE_CONDUCTOR.toUpperCase())){
						    		if(detalle.getLimiteAccidentesConductor() != null){
						    			boolean valido = true;
						    			if(validaRangos){
						    				valido = validaRangos(coberturaCotizacion.getValorSumaAseguradaMin(), 
						    						coberturaCotizacion.getValorSumaAseguradaMax(), detalle.getLimiteAccidentesConductor());
						    			}
						    			if(valido){
							    			coberturaCotizacion.setValorSumaAsegurada(detalle.getLimiteAccidentesConductor());
							    			coberturaCotizacion.setClaveContrato(claveContrato);					    				
						    			}else{
					    					LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
					    					error.setNumeroInciso(new BigDecimal(fila));
					    					error.setNombreSeccion("");
					    					error.setNombreCampoError("LIMITE ACCIDENTE CONDUCTOR");
					    					error.setRecomendaciones("Valor no valido");
					    					listErrores.add(error);
					    					mensajeError.append("Campo: LIMITE ACCIDENTE CONDUCTOR -  Valor debe ser entre ").append(coberturaCotizacion.getValorSumaAseguradaMin()).append(
					    					" y ").append(coberturaCotizacion.getValorSumaAseguradaMax()).append("").append("\r\n").append(System.getProperty("line.separator"));
						    			}
						    		}else{
					    				if(!esObligatoria){
						    				coberturaCotizacion.setClaveContrato(valorNO);
						    			}
						    		}
						 		}else if(coberturaCotizacion.getCoberturaSeccionDTO().getCoberturaDTO().getNombreComercial().toUpperCase().equals(
						 				GeneraExcelCargaMasiva.NOMBRE_EXTENCION_RC.toUpperCase())){
						 			//
						 		}else if(coberturaCotizacion.getCoberturaSeccionDTO().getCoberturaDTO().getNombreComercial().toUpperCase().equals(
						 				GeneraExcelCargaMasiva.NOMBRE_LIMITE_ADAPTACION_CONVERSION.toUpperCase())){
						    		if(detalle.getLimiteAdaptacionConversion() != null){
						    			boolean valido = true;
						    			if(validaRangos){
						    				valido = validaRangos(coberturaCotizacion.getValorSumaAseguradaMin(), 
						    						coberturaCotizacion.getValorSumaAseguradaMax(), detalle.getLimiteAdaptacionConversion());
						    			}
						    			if(valido){
							    			coberturaCotizacion.setValorSumaAsegurada(detalle.getLimiteAdaptacionConversion());
							    			coberturaCotizacion.setClaveContrato(claveContrato);					    				
						    			}else{
					    					LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
					    					error.setNumeroInciso(new BigDecimal(fila));
					    					error.setNombreSeccion("");
					    					error.setNombreCampoError("LIMITE ADAPTACION CONVERSION");
					    					error.setRecomendaciones("Valor no valido");
					    					listErrores.add(error);
					    					mensajeError.append("Campo: LIMITE ADAPTACION CONVERSION -  Valor debe ser entre ").append(coberturaCotizacion.getValorSumaAseguradaMin()).append(
					    					" y ").append(coberturaCotizacion.getValorSumaAseguradaMax()).append("").append("\r\n").append(System.getProperty("line.separator"));
						    			}
						    		}
						 		}else if(coberturaCotizacion.getCoberturaSeccionDTO().getCoberturaDTO().getNombreComercial().toUpperCase().equals(
						 				GeneraExcelCargaMasiva.NOMBRE_LIMITE_EQUIPO_ESPECIAL.toUpperCase())){
						    		if(detalle.getLimiteEquipoEspecial() != null){
						    			boolean valido = true;
						    			if(validaRangos){
						    				valido = validaRangos(coberturaCotizacion.getValorSumaAseguradaMin(), 
						    						coberturaCotizacion.getValorSumaAseguradaMax(), detalle.getLimiteEquipoEspecial());
						    			}
						    			if(valido){
							    			coberturaCotizacion.setValorSumaAsegurada(detalle.getLimiteEquipoEspecial());
							    			coberturaCotizacion.setClaveContrato(claveContrato);					    				
						    			}else{
					    					LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
					    					error.setNumeroInciso(new BigDecimal(fila));
					    					error.setNombreSeccion("");
					    					error.setNombreCampoError("LIMITE EQUIPO ESPECIAL");
					    					error.setRecomendaciones("Valor no valido");
					    					listErrores.add(error);
					    					mensajeError.append("Campo: LIMITE EQUIPO ESPECIAL -  Valor debe ser entre ").append(coberturaCotizacion.getValorSumaAseguradaMin()).append(
					    					" y ").append(coberturaCotizacion.getValorSumaAseguradaMax()).append("").append("\r\n").append(System.getProperty("line.separator"));
						    			}
						    			if(valido && detalle.getDeducibleEquipoEspecial() != null){
						    				if(validaDeducibles){
						    					//valido = validaDeducible(coberturaCotizacion.getDeducibles(), detalle.getDeducibleEquipoEspecial());
						    					valido = true;
						    				}
						    				if(valido){
						    					coberturaCotizacion.setValorDeducible(detalle.getDeducibleEquipoEspecial());
						    					coberturaCotizacion.setPorcentajeDeducible(detalle.getDeducibleEquipoEspecial());
						    					coberturaCotizacion.setClaveContrato(claveContrato);
						    				}else{
						    					LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
						    					error.setNumeroInciso(new BigDecimal(fila));
						    					error.setNombreSeccion("");
						    					error.setNombreCampoError("DEDUCIBLE EQUIPO ESPECIAL");
						    					error.setRecomendaciones("Valor no valido");
						    					listErrores.add(error);
						    					mensajeError.append("Campo: DEDUCIBLE EQUIPO ESPECIAL -  Valor no valido ").append("\r\n").append(System.getProperty("line.separator"));
						    				}
						    			}
						    		}
						 		}else if(coberturaCotizacion.getCoberturaSeccionDTO().getCoberturaDTO().getNombreComercial().toUpperCase().equals(
						 				GeneraExcelCargaMasiva.NOMBRE_EXENCION_DEDUCIBLE_ROBO.toUpperCase())){
						 			//
						 		}else if(coberturaCotizacion.getCoberturaSeccionDTO().getCoberturaDTO().getNombreComercial().toUpperCase().equals(
						 				GeneraExcelCargaMasiva.NOMBRE_RESPONSABILIDAD_CIVIL_USA.toUpperCase())){
						 			//
						 		}else if(coberturaCotizacion.getCoberturaSeccionDTO().getCoberturaDTO().getNombreComercial().toUpperCase().equals(
						 				CoberturaDTO.DANOS_OCASIONADOS_CARGA_NOMBRE_COMERCIAL.toUpperCase())){
					 				boolean danosCargaContratada = esObligatoria || detalle.isDanosCargaContratada();
					 				coberturaCotizacion.setClaveContratoBoolean(danosCargaContratada);
						 		}						 
						 	}
						 
						}

				CoberturaCotizacionDTO responsabilidadCivilCoberturaCotizacion = cargaMasivaService
						.getCoberturaCotizacion(
								coberturaCotizacionList,
								CoberturaDTO.RESPONSABILIDAD_CIVIL_AUTOS_NOMBRE_COMERCIAL);					
				if (responsabilidadCivilCoberturaCotizacion != null && responsabilidadCivilCoberturaCotizacion.getClaveContratoBoolean()) {
					DatoIncisoCotAuto datoIncisoCotAutoNumeroRemolques = cargaMasivaService
					.obtieneDatoIncisoCotAutoSubRamo(cotizacion,
							responsabilidadCivilCoberturaCotizacion, incisoCotizacion
									.getId().getNumeroInciso(), ConfiguracionDatoInciso.DescripcionEtiqueta.NUMERO_REMOLQUES, String
									.valueOf(detalle
											.getNumeroRemolques()));
					if (datoIncisoCotAutoNumeroRemolques != null) {							
						List<LogErroresCargaMasivaDTO> tmpErrors = new ArrayList<LogErroresCargaMasivaDTO>();				 			
						cargaMasivaService.validaNoNulo(detalle.getNumeroRemolques(), NOMBRE_CAMPO_NUMERO_REMOLQUES, filaDecimal, "", tmpErrors);
						cargaMasivaService.validaRangos(0, 9, detalle.getNumeroRemolques(), NOMBRE_CAMPO_NUMERO_REMOLQUES, filaDecimal, "", tmpErrors);
						listErrores.addAll(tmpErrors);
						mensajeError.append(cargaMasivaService.generaMensajeError(tmpErrors));
						datoIncisoCotAutos.add(datoIncisoCotAutoNumeroRemolques);
					}
				}
				
				CoberturaCotizacionDTO danosOcasionadosPorCargaCoberturaCotizacion = cargaMasivaService
				.getCoberturaCotizacion(
						coberturaCotizacionList,
						CoberturaDTO.DANOS_OCASIONADOS_CARGA_NOMBRE_COMERCIAL);
				if (danosOcasionadosPorCargaCoberturaCotizacion != null && danosOcasionadosPorCargaCoberturaCotizacion.getClaveContratoBoolean()) {
					List<LogErroresCargaMasivaDTO> tmpErrors = new ArrayList<LogErroresCargaMasivaDTO>();				 			
					
					
					if (cargaMasivaService.validaNoNulo(detalle.getTipoCargaDescripcion(), NOMBRE_CAMPO_TIPO_CARGA, filaDecimal, "", tmpErrors)) {
						cargaMasivaService.validaNoNulo2(detalle.getTipoCarga(), NOMBRE_CAMPO_TIPO_CARGA, filaDecimal, "", tmpErrors);			
					}
					
					if (responsabilidadCivilCoberturaCotizacion == null) {
						throw new RuntimeException("Paquete mal configurado ya que los paquetes que incluyen la cobertura de " +
								"daños por la carga deben tambien incluir la cobertura de responsabilidad civil.");
					}
					
					if (!responsabilidadCivilCoberturaCotizacion.getClaveContratoBoolean()) {
						cargaMasivaService
								.agregaError(
										"La cobertura responsabilidad civil debe estar contratada para poder contratar la cobertura de daños por la carga.",
										CoberturaDTO.DANOS_OCASIONADOS_CARGA_NOMBRE_COMERCIAL, filaDecimal, "",
										tmpErrors);
					}
					
					listErrores.addAll(tmpErrors);
					mensajeError.append(cargaMasivaService.generaMensajeError(tmpErrors));
					
					if (tmpErrors.size() == 0) {
						DatoIncisoCotAuto datoIncisoCotAutoTipoCarga = cargaMasivaService
								.obtieneDatoIncisoCotAutoCobertura(cotizacion,
										danosOcasionadosPorCargaCoberturaCotizacion, incisoCotizacion
												.getId().getNumeroInciso(),
										ConfiguracionDatoInciso.DescripcionEtiqueta.TIPO_CARGA, String.valueOf(detalle.getTipoCarga()
												.getId().getIdDato()));
	
						if (datoIncisoCotAutoTipoCarga == null) {
							throw new RuntimeException(
									"Error en la configuración de datos de inciso. Debe estar configurado el dato de inciso de tipo de carga para "
											+ "la cobertura de daños ocasionados por la carga.");
						}
						datoIncisoCotAutos.add(datoIncisoCotAutoTipoCarga);
					}
				}
					  
					
					//Observaciones de inciso
					incisoCotizacion.getIncisoAutoCot().setObservacionesinciso(detalle.getObservacionesInciso());
					

					// Asegurado
					if (detalle.getNombreAsegurado() != null) {
						incisoCotizacion.getIncisoAutoCot().setNombreAsegurado(
								detalle.getNombreAsegurado().toUpperCase());
						incisoCotizacion.getIncisoAutoCot().setPersonaAseguradoId(null);
					}

				
					//Datos Vehiculo
					incisoCotizacion.getIncisoAutoCot().setNumeroMotor(
							detalle.getNumeroMotor());
					
					//String numeroSerieRempl = remplazarCaracteresNumSerie(detalle.getNumeroSerie());
					incisoCotizacion.getIncisoAutoCot().setNumeroSerie(detalle.getNumeroSerie());
					List<String> errors = NumSerieValidador.getInstance()
							.validate(
									incisoCotizacion.getIncisoAutoCot()
											.getNumeroSerie());
					if (!errors.isEmpty()) {
						/*
						LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
						error.setNumeroInciso(new BigDecimal(fila));
						error.setNombreSeccion("");
						error.setNombreCampoError("NUMERO DE SERIE");
						error.setRecomendaciones("Error al validar el n\u00FAmero de serie:");
						listErrores.add(error);
						mensajeError += "Campo: NUMERO DE SERIE -  " + errors.get(0) + "";
						*/
						HSSFCell cellStat = sheet.getRow((fila - 1)).getCell(CAMPO_ERROR_NUMERO_SERIE);
						if(cellStat == null){
							cellStat = sheet.getRow((fila - 1)).createCell(CAMPO_ERROR_NUMERO_SERIE);
						}
						cellStat.setCellValue("Error en construccion del n\u00FAmero de serie:" + detalle.getNumeroSerie());
					}
					incisoCotizacion.getIncisoAutoCot().setPlaca(
							detalle.getPlacas());
					incisoCotizacion.getIncisoAutoCot().setRepuve(
							detalle.getNciRepuve());
					
					String tipoCuenta = detalle.getTipoTarjeta();
					
					if (!detalle.getConductoCobro().equals(VALOR_EFECTIVO)) {
						if(detalle.getIdMedioPago().toString().equals(MEDIO_PAGO_DOMICILIACION)||detalle.getIdMedioPago().toString().equals(MEDIO_PAGO_CUENTA_AFIRME)){
							if (detalle.getNumeroTarjetaClave().length()==16){
								tipoCuenta = TIPO_CUENTA_DEBITO;
							}else if(detalle.getNumeroTarjetaClave().length()==18){
								tipoCuenta = TIPO_CUENTA_CUENTA_CLABE;
							}else{
								tipoCuenta = TIPO_CUENTA_CUENTA_BANCARIA;
							}
						}
						
						if(!tipoCuenta.equals(TIPO_CUENTA_CUENTA_BANCARIA)){
							if (detalle.getInstitucionBancaria() == null) {
								LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
								error.setNumeroInciso(new BigDecimal(fila));
								error.setNombreCampoError("INSTITUCION BANCARIA");
								error.setNombreSeccion("INSTITUCION BANCARIA");
								error.setRecomendaciones("No debe estar vacio INSTITUCION BANCARIA cuando el tipo de cobro es Tarjeta de credito o domiciliacion");
								listErrores.add(error);
							}
							if (detalle.getTipoTarjeta() == null) {
								LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
								error.setNumeroInciso(new BigDecimal(fila));
								error.setNombreSeccion("TIPO TARJETA");
								error.setNombreCampoError("TIPO TARJETA");
								error.setRecomendaciones("No debe estar vacio TIPO TARJETA cuando el tipo de cobro es Tarjeta de credito o domiciliacion");
								listErrores.add(error);
							}
							String numTarjeta = detalle.getNumeroTarjetaClave();
							if (numTarjeta == null || !(numTarjeta.length() == 16 || numTarjeta.length() == 18)) {
								LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
								error.setNumeroInciso(new BigDecimal(fila));
								error.setNombreSeccion("NUMERO TARJETA / CLABE");
								error.setNombreCampoError("NUMERO TARJETA / CLABE");
								error.setRecomendaciones("Numero de Tarjeta debe tener 16 digitos. CLABE debe tener 18 digitos");
								listErrores.add(error);
							}
							
							if(!tipoCuenta.equals(TIPO_CUENTA_CUENTA_CLABE)){
								
								String cvv = detalle.getCodigoSeguridad();
								if (cvv == null || !(cvv.length() == 3 || cvv.length() == 4)) {
									LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
									error.setNumeroInciso(new BigDecimal(fila));
									error.setNombreSeccion("CODIGO DE SEGURIDAD");
									error.setNombreCampoError("CODIGO DE SEGURIDAD");
									error.setRecomendaciones("El codigo de seguridad debe tener 3 o 4 digitos");
									listErrores.add(error);
								}
								String vigenciaTarjeta = detalle.getFechaVencimiento();
								if (vigenciaTarjeta == null || !validaVigenciaTarjeta(vigenciaTarjeta)) {
									LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
									error.setNumeroInciso(new BigDecimal(fila));
									error.setNombreSeccion("FECHA DE VENCIMIENTO");
									error.setNombreCampoError("FECHA DE VENCIMIENTO");
									error.setRecomendaciones("Fecha de Vencimiento no valida. Usar el formato MM/yy y una tarjeta vigente");
									listErrores.add(error);
								}
							}
						}
						
						// Guardar datos de pago encriptados
						if (mensajeError.toString().isEmpty()) {
							
							if(clienteNuevo != null && clienteNuevo.getIdCliente() != null){
								incisoCotizacion.setIdClienteCob(clienteNuevo.getIdCliente());
								incisoCotizacion.setIdConductoCobroCliente(new Long(conductoCobro));
							}
							incisoCotizacion.setIdMedioPago(new BigDecimal(detalle.getIdMedioPago()));
							incisoCotizacion.setConductoCobro(detalle.getConductoCobro());
							incisoCotizacion.setInstitucionBancaria(detalle.getInstitucionBancaria());
							incisoCotizacion.setTipoTarjeta(tipoCuenta);
							
							detalle = bancoMidasService.encriptarCuentasBancariasCargaMasivaInd(detalle);
							incisoCotizacion.setNumeroTarjetaClave(detalle.getNumeroTarjetaClave());
							incisoCotizacion.setCodigoSeguridad(detalle.getCodigoSeguridad());
							incisoCotizacion.setFechaVencimiento(detalle.getFechaVencimiento());
						}
						
					}
						if (mensajeError.toString().isEmpty()) {

							try{
								incisoCotizacion = cargaMasivaService.guardaIncisoCotizacion(incisoCotizacion,
												coberturaCotizacionList, datoIncisoCotAutos);

								incisoCotizacion = cargaMasivaService.calculoInciso(incisoCotizacion);
								
								
 								Boolean datosConductorObligatorios = false;
								try{
									datosConductorObligatorios = cargaMasivaService.datosDelConductorRequeridos(incisoCotizacion);
								}catch(Exception e){
									
								}
								
								//Valida datos de Riesgo
								List<CoberturaCotizacionDTO> coberturas = cargaMasivaService.obtieneCoberturasContratadas(incisoCotizacion);
								DatoIncisoCotAuto datoIncisoCotAutoEquipoEspecial = null;
								DatoIncisoCotAuto datoIncisoCotAutoAdaptacionConversion = null;
								List<DatoIncisoCotAuto> datoIncisoCotAutosComp = new ArrayList<DatoIncisoCotAuto>();
								for(CoberturaCotizacionDTO coberturaCotizacion : coberturas){
									if(coberturaCotizacion.getCoberturaSeccionDTO().getCoberturaDTO().getNombreComercial().toUpperCase().equals(
											GeneraExcelCargaMasiva.NOMBRE_LIMITE_EQUIPO_ESPECIAL.toUpperCase())){
										datoIncisoCotAutoEquipoEspecial = cargaMasivaService.obtieneDatoIncisoCotAuto(cotizacion, coberturaCotizacion, incisoCotizacion.getId().getNumeroInciso(), 
												detalle.getDescripcionEquipoEspecial());
										if(datoIncisoCotAutoEquipoEspecial == null){
											LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
											error.setNumeroInciso(new BigDecimal(fila));
											error.setNombreSeccion("");
											error.setNombreCampoError("DESCRIPCION EQUIPO ESPECIAL");
											error.setRecomendaciones("Error al validar Descripcion Equipo Especial");
											listErrores.add(error);
											mensajeError.append("Campo: DESCRIPCION EQUIPO ESPECIAL -  Error al validar Descripcion Equipo Especial ");
										}
										datoIncisoCotAutosComp.add(datoIncisoCotAutoEquipoEspecial);
									}
									if(coberturaCotizacion.getCoberturaSeccionDTO().getCoberturaDTO().getNombreComercial().toUpperCase().equals(
											GeneraExcelCargaMasiva.NOMBRE_LIMITE_ADAPTACION_CONVERSION.toUpperCase())){
										datoIncisoCotAutoAdaptacionConversion = cargaMasivaService.obtieneDatoIncisoCotAuto(cotizacion, coberturaCotizacion, incisoCotizacion.getId().getNumeroInciso(), 
												detalle.getDescripcionAdaptacionConversion());
										if(datoIncisoCotAutoAdaptacionConversion == null){
											LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
											error.setNumeroInciso(new BigDecimal(fila));
											error.setNombreSeccion("");
											error.setNombreCampoError("DESCRIPCION ADAPTACION Y CONVERSION");
											error.setRecomendaciones("Error al validar Descripcion Adaptacion y Conversion");
											listErrores.add(error);
											mensajeError.append("Campo: DESCRIPCION ADAPTACION Y CONVERSION -  Error al validar Descripcion Adaptacion y Conversion ");
										}
										datoIncisoCotAutosComp.add(datoIncisoCotAutoAdaptacionConversion);
									}
								}
								

								//Datos del COnductor
								if(datosConductorObligatorios){
									incisoCotizacion.getIncisoAutoCot().setNombreConductor(
										detalle.getNombre());
									incisoCotizacion.getIncisoAutoCot().setPaternoConductor(
										detalle.getApellidoPaterno());
									incisoCotizacion.getIncisoAutoCot().setMaternoConductor(
										detalle.getApellidoMaterno());
									incisoCotizacion.getIncisoAutoCot().setNumeroLicencia(
										detalle.getNumeroLicencia());
									incisoCotizacion.getIncisoAutoCot().setFechaNacConductor(
										detalle.getFechaNacimiento());
									incisoCotizacion.getIncisoAutoCot().setOcupacionConductor(
										detalle.getOcupacion());
									
									try{
										IncisoAutoCot incisoAutoCotComp = cargaMasivaService.obtieneIncisoAutoCot(incisoCotizacion);
										incisoAutoCotComp.setNombreConductor(detalle.getNombre());
										incisoAutoCotComp.setPaternoConductor(detalle.getApellidoPaterno());
										incisoAutoCotComp.setMaternoConductor(detalle.getApellidoMaterno());
										incisoAutoCotComp.setNumeroLicencia(detalle.getNumeroLicencia());
										incisoAutoCotComp.setFechaNacConductor(detalle.getFechaNacimiento());
										incisoAutoCotComp.setOcupacionConductor(detalle.getOcupacion());
										IncisoCotizacionDTO incisoComp = new IncisoCotizacionDTO();
										incisoComp.setIncisoAutoCot(incisoAutoCotComp);
										
										cargaMasivaService.guardarComplementarInciso(incisoComp, datoIncisoCotAutosComp);
									}catch(Exception e){
										e.printStackTrace();
									}
								}else{
									try{
										cargaMasivaService.guardarComplementarInciso(null, datoIncisoCotAutosComp);
									}catch(Exception e){
									}
								}
								
							}catch(Exception e){
								e.printStackTrace();
								incisoCotizacion = null;
							}
							if (incisoCotizacion == null) {
								LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
								error.setNumeroInciso(new BigDecimal(fila));
								error.setNombreSeccion("");
								error.setNombreCampoError("ERROR AL GUARDAR INCISO");
								error.setRecomendaciones("");
								listErrores.add(error);
								mensajeError.append("Error al guardar Inciso \n");
							}else{
								
								try{
									if(incisoCotizacion.getIdMedioPago()!=null&&incisoCotizacion.getConductoCobro()!=null&&incisoCotizacion.getInstitucionBancaria()!=null){
										ClienteGenericoDTO cuenta = clienteMigracion(incisoCotizacion,clienteNuevo);
										clienteFacadeRemote.guardarDatosCobranza(cuenta, "M2ADMINI",true);
									}
								}catch(Exception e){
									e.printStackTrace();
									LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
									error.setNumeroInciso(new BigDecimal(fila));
									error.setNombreSeccion("");
									error.setNombreCampoError("ERROR AL GUARDAR CONDUCTO COBRO");
									error.setRecomendaciones(e.getMessage());
									listErrores.add(error);
									mensajeError.append("Error al guardar conducto cobro\n");
								}
								
								try{							
									ResumenCostosDTO resumen = cargaMasivaService.calculaCotizacion(cotizacion);
									cotizacion.setValorPrimaTotal(new BigDecimal(resumen.getPrimaTotal()));
									
									if(detalle.getPrimaTotal() != null && detalle.getIgualacion() != null && detalle.getIgualacion().equals(GeneraExcelCargaMasiva.VALOR_SI)){
										cargaMasivaService.igualarPrima(cotizacion.getIdToCotizacion(), detalle.getPrimaTotal());
										//cotizacion = cargaMasivaService.getCotizacionByDetalleCargaMasivaIndAutoCot(cotizacion.getIdToCotizacion());
										//detalle.setPrimaTotal(cotizacion.getValorPrimaTotal().doubleValue());
									}else{
										detalle.setPrimaTotal(resumen.getPrimaTotal());
									}
									HSSFCell cellPrima = sheet.getRow((fila - 1)).getCell(CAMPO_PRIMA_TOTAL);
									if(cellPrima == null){
										cellPrima = sheet.getRow((fila - 1)).createCell(CAMPO_PRIMA_TOTAL);
									}
									cellPrima.setCellValue(detalle.getPrimaTotal());
								}catch(Exception e){
									e.printStackTrace();
								}
								
								if(mensajeError.toString().isEmpty()){
									TerminarCotizacionDTO terminarCotizacionDTO = cargaMasivaService.terminaCotizacion(cotizacion);
								if(terminarCotizacionDTO != null){
									if(terminarCotizacionDTO.getEstatus() == TerminarCotizacionDTO.Estatus.ERROR_EXCEPCIONES.getEstatus().shortValue()){
										if(detalle.getSolicitarAutorizacion() != null && detalle.getSolicitarAutorizacion().equals(GeneraExcelCargaMasiva.VALOR_SI)){
											Long idSolicitud = cargaMasivaService.solicitudAutorizacion(terminarCotizacionDTO.getExcepcionesList(), cotizacion.getIdToCotizacion(), idUsuario);
											detalle.setMensajeError("Solicitud Autorizacion:" + idSolicitud);
											detalle.setClaveEstatus(DetalleCargaMasivaIndAutoCot.ESTATUS_AUTORIZACION);
											requiereAutorizacion = true;
											HSSFCell cellStat = sheet.getRow((fila - 1)).getCell(CAMPO_ESTATUS);
											if(cellStat == null){
												cellStat = sheet.getRow((fila - 1)).createCell(CAMPO_ESTATUS);
											}
											cellStat.setCellValue("SOLICITO AUTORIZACION");
										}else{
											LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
											error.setNumeroInciso(new BigDecimal(fila));
											error.setNombreSeccion("");
											error.setNombreCampoError(terminarCotizacionDTO.getMensajeError());
											error.setRecomendaciones("");
											listErrores.add(error);
											
											List<ExcepcionSuscripcionReporteDTO> list = terminarCotizacionDTO.getExcepcionesList();
											boolean isFirst = true;
											for(ExcepcionSuscripcionReporteDTO item : list){
												if(isFirst){
													isFirst = false;
												}else{
													mensajeError.append(", ");
												}
												mensajeError.append(item.getDescripcionExcepcion());
											}
											//mensajeError += terminarCotizacionDTO.getMensajeError();
										}
									}else{
										LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
										error.setNumeroInciso(new BigDecimal(fila));
										error.setNombreSeccion("");
										error.setNombreCampoError(terminarCotizacionDTO.getMensajeError());
										error.setRecomendaciones("");
										listErrores.add(error);
										mensajeError.append(terminarCotizacionDTO.getMensajeError());
									}
								}
								}
							}
				    	}
					}
					
				}////////INCISO
				
				if(!requiereAutorizacion){
					detalle.setMensajeError(mensajeError.toString());
					if (mensajeError.toString().isEmpty()) {
						detalle.setClaveEstatus(DetalleCargaMasivaIndAutoCot.ESTATUS_TERMINADO);
						HSSFCell cellStat = sheet.getRow((fila - 1)).getCell(CAMPO_ESTATUS);
						if(cellStat == null){
							cellStat = sheet.getRow((fila - 1)).createCell(CAMPO_ESTATUS);
						}
						cellStat.setCellValue("TERMINADO");
					} else {
						detalle.setClaveEstatus(DetalleCargaMasivaIndAutoCot.ESTATUS_ERROR);
						HSSFCell cellStat = sheet.getRow((fila - 1)).getCell(CAMPO_ESTATUS);
						if(cellStat == null){
							cellStat = sheet.getRow((fila - 1)).createCell(CAMPO_ESTATUS);
						}
						cellStat.setCellValue("ERROR");
						HSSFCell cell2 = sheet.getRow((fila - 1)).getCell(CAMPO_NUMERO_COTIZACION);
						if(cell2 == null){
							cell2 = sheet.getRow((fila - 1)).createCell(CAMPO_NUMERO_COTIZACION);
						}
						cell2.setCellValue("");
						HSSFCell cell3 = sheet.getRow((fila - 1)).getCell(CAMPO_ERROR_EXCEPCIONES);
						if(cell3 == null){
							cell3 = sheet.getRow((fila - 1)).createCell(CAMPO_ERROR_EXCEPCIONES);
						}
						cell3.setCellValue("EXCEPTION:" + mensajeError);
						
						//Elimina Cotizacion
						if(cotizacion != null && cotizacion.getIdToCotizacion() != null){
							cargaMasivaService.eliminaCotizacionError(cotizacion);
							detalle.setIdToCotizacion(BigDecimal.ZERO);
						}
						
					}
				}
			}//ERROR
			}catch(Exception e){
				e.printStackTrace();
				detalle.setClaveEstatus(DetalleCargaMasivaIndAutoCot.ESTATUS_ERROR);
				HSSFCell cellStat = sheet.getRow((fila - 1)).getCell(CAMPO_ESTATUS);
				if(cellStat == null){
					cellStat = sheet.getRow((fila - 1)).createCell(CAMPO_ESTATUS);
				}
				cellStat.setCellValue("ERROR");
				HSSFCell cell2 = sheet.getRow((fila - 1)).getCell(CAMPO_NUMERO_COTIZACION);
				if(cell2 == null){
					cell2 = sheet.getRow((fila - 1)).createCell(CAMPO_NUMERO_COTIZACION);
				}
				cell2.setCellValue("");
				HSSFCell cell3 = sheet.getRow((fila - 1)).getCell(CAMPO_ERROR_EXCEPCIONES);
				if(cell3 == null){
					cell3 = sheet.getRow((fila - 1)).createCell(CAMPO_ERROR_EXCEPCIONES);
				}
				String exc = e.toString();
				if(exc.length() > 200){
					exc = exc.substring(0, 200);
				}
				cell3.setCellValue("EXCEPTION:" + exc);
			}
		}//FOR
	}
	

	private ClienteGenericoDTO clienteMigracion(IncisoCotizacionDTO inciso, ClienteDTO clienteDto) {
		ClienteGenericoDTO cliente = new ClienteGenericoDTO();
		cliente.setIdCliente(inciso.getIdClienteCob());
		cliente.setIdConductoCobranza(inciso.getIdConductoCobroCliente());
		cliente.setNombreTarjetaHabienteCobranza(inciso.getIncisoAutoCot().getNombreAsegurado());
		cliente.setEmailCobranza("");
		cliente.setTelefonoCobranza("");
		cliente.setNombreCalleCobranza(clienteDto.getNombreCalle());
		cliente.setCodigoPostalCobranza(clienteDto.getCodigoPostal());
		cliente.setNombreColoniaCobranza(clienteDto.getNombreColonia());
		cliente.setIdTipoConductoCobro(inciso.getIdMedioPago().intValue());
		
		List<BancoEmisorDTO> bancos = bancoMidasService.bancosSeycos();
		Integer idBancoCobranza = null;
		for(BancoEmisorDTO banco: bancos){
			if(inciso.getInstitucionBancaria().equals(banco.getNombreBanco())){
				idBancoCobranza =  banco.getIdBanco();
			}
		}
		cliente.setIdBancoCobranza(new BigDecimal(idBancoCobranza));
		cliente.setIdTipoTarjetaCobranza(inciso.getTipoTarjeta());
		cliente.setNumeroTarjetaCobranza(bancoMidasService.desencriptaInformacionBancaria(inciso.getNumeroTarjetaClave()).trim());
		//Concidicon para cuando el conducto de cobro es del tipo tarjeta de credito
		if (inciso.getCodigoSeguridad() != null&&inciso.getFechaVencimiento()!=null) {
			cliente.setCodigoSeguridadCobranza(bancoMidasService.desencriptaInformacionBancaria(inciso.getCodigoSeguridad()).trim());
			cliente.setFechaVencimientoTarjetaCobranza(bancoMidasService.desencriptaInformacionBancaria(inciso.getFechaVencimiento()).replace("/", ""));
		}		
		cliente.setTipoPromocion("");
		cliente.setDiaPagoTarjetaCobranza(new BigDecimal(0));				
		cliente.setRfcCobranza(clienteDto.getCodigoRFC());
		
		return cliente;
	}
	
	
	public void validaEmisionCargaMasiva(List<DetalleCargaMasivaIndAutoCot> detalleCargaMasivaList, HSSFSheet sheet){
		
		List<CondicionEspecial> listaAltaCondicionEspecial = null;
		List<CondicionEspecial> listaBajaCondicionEspecial = null;
		StringBuilder mensajeError = new StringBuilder();
		int fila = 1;
		for (DetalleCargaMasivaIndAutoCot detalle : detalleCargaMasivaList) {

			fila++;
			mensajeError.delete(0,mensajeError.length());//String mensajeError = "";
			if (detalle.getClaveEstatus() != null && !detalle.getClaveEstatus().equals(DetalleCargaMasivaAutoCot.ESTATUS_ERROR)) {
				
				
				
				CotizacionDTO cotizacion = cargaMasivaService.getCotizacionByDetalleCargaMasivaIndAutoCot(detalle.getNumeroCotizacion());
				IncisoCotizacionDTO incisoCotizacion 	= null;
				
								
				
				//Termina cotizacion si esta en proceso
				if(cotizacion != null &&  cotizacion.getClaveEstatus() != null && 
						cotizacion.getClaveEstatus().equals(CotizacionDTO.ESTATUS_COT_EN_PROCESO)){
					TerminarCotizacionDTO terminarCotizacionDTO = cargaMasivaService.terminaCotizacion(cotizacion);
					if(terminarCotizacionDTO == null){
						cotizacion = cargaMasivaService.getCotizacionByDetalleCargaMasivaIndAutoCot(detalle.getNumeroCotizacion());
					}
				}
				
				if(cotizacion != null &&  cotizacion.getClaveEstatus() != null && 
						cotizacion.getClaveEstatus().equals(CotizacionDTO.ESTATUS_COT_TERMINADA)){
					
					detalle.setNumeroCotizacion(cotizacion.getIdToCotizacion());
					detalle.setIdToCotizacion(cotizacion.getIdToCotizacion());
					
					
				 incisoCotizacion = cargaMasivaService.obtieneIncisoCotizacion(cotizacion, new BigDecimal(1));
				
				
				
				

				if (incisoCotizacion == null) {
					LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
					error.setNumeroInciso(new BigDecimal(fila));
					error.setNombreSeccion("");
					error.setNombreCampoError("NUMERO DE INCISO");
					error.setRecomendaciones("No existe inciso");
					listErrores.add(error);
					mensajeError.append("Campo: ").append(error.getNombreCampoError()).append(" - ").append(error.getRecomendaciones()).append(" \n");
				} else {

					// Asegurado
					if (detalle.getNombreAsegurado() != null) {
						incisoCotizacion.getIncisoAutoCot().setNombreAsegurado(
								detalle.getNombreAsegurado().toUpperCase());
						incisoCotizacion.getIncisoAutoCot().setPersonaAseguradoId(null);
					} else {
						if(cotizacion.getIdToPersonaContratante() != null && cotizacion.getNombreContratante() != null){
							incisoCotizacion.getIncisoAutoCot().setNombreAsegurado(
									cotizacion.getNombreContratante().toUpperCase());
							incisoCotizacion.getIncisoAutoCot().setPersonaAseguradoId(
									cotizacion.getIdToPersonaContratante().longValue());
						}
					}

					//Datos del COnductor
					Boolean datosConductorObligatorios = false;
					try{
						datosConductorObligatorios = cargaMasivaService.datosDelConductorRequeridos(incisoCotizacion);
					}catch(Exception e){
						
					}
					if(datosConductorObligatorios){
						incisoCotizacion.getIncisoAutoCot().setNombreConductor(detalle.getNombre());
						incisoCotizacion.getIncisoAutoCot().setPaternoConductor(detalle.getApellidoPaterno());
						incisoCotizacion.getIncisoAutoCot().setMaternoConductor(detalle.getApellidoMaterno());
						incisoCotizacion.getIncisoAutoCot().setNumeroLicencia(detalle.getNumeroLicencia());
						
						//Valida fechaNacimiento
						GregorianCalendar gcFecha = new GregorianCalendar();
						gcFecha.setTime(new Date());
						gcFecha.add(GregorianCalendar.YEAR, -18);
						
						if(detalle.getFechaNacimiento().before(gcFecha.getTime())){
							incisoCotizacion.getIncisoAutoCot().setFechaNacConductor(detalle.getFechaNacimiento());
						}else{
							incisoCotizacion.getIncisoAutoCot().setFechaNacConductor(null);
							LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
							error.setNumeroInciso(new BigDecimal(fila));
							error.setNombreSeccion("");
							error.setNombreCampoError("FECHA NACIMIENTO CONDUCTOR");
							error.setRecomendaciones("Conductor debe ser mayor de edad");
							listErrores.add(error);
							mensajeError.append("Campo: ").append(error.getNombreCampoError()).append(" - ").append(error.getRecomendaciones()).append(" ").append("\r\n").append(System.getProperty("line.separator"));
						}
						
						
						incisoCotizacion.getIncisoAutoCot().setOcupacionConductor(
							detalle.getOcupacion());
					}
					//Datos Vehiculo
					incisoCotizacion.getIncisoAutoCot().setNumeroMotor(detalle.getNumeroMotor());
					
					//String numeroSerieRempl = remplazarCaracteresNumSerie(detalle.getNumeroSerie());
					//incisoCotizacion.getIncisoAutoCot().setNumeroSerie(numeroSerieRempl);
					incisoCotizacion.getIncisoAutoCot().setNumeroSerie(detalle.getNumeroSerie());

					
					List<String> errors = NumSerieValidador.getInstance().validate(incisoCotizacion.getIncisoAutoCot().getNumeroSerie());
					if (!errors.isEmpty()) {
						/*
						LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
						error.setNumeroInciso(new BigDecimal(fila));
						error.setNombreSeccion("");
						error.setNombreCampoError("NUMERO DE SERIE");
						error.setRecomendaciones("Error al validar el n\u00FAmero de serie:");
						listErrores.add(error);
						mensajeError += "Campo: NUMERO DE SERIE -  " + errors.get(0) + "";
						*/
						HSSFCell cellStat = sheet.getRow((fila - 1)).getCell(CAMPO_ERROR_NUMERO_SERIE);
						if(cellStat == null){
							cellStat = sheet.getRow((fila - 1)).createCell(CAMPO_ERROR_NUMERO_SERIE);
						}
						cellStat.setCellValue("Error en construccion del n\u00FAmero de serie:" + detalle.getNumeroSerie());
					}
					incisoCotizacion.getIncisoAutoCot().setPlaca(detalle.getPlacas());
					incisoCotizacion.getIncisoAutoCot().setRepuve(detalle.getNciRepuve());
					
					
					//Valida datos de Riesgo
					List<CoberturaCotizacionDTO> coberturas 					= cargaMasivaService.obtieneCoberturasContratadas(incisoCotizacion);
					DatoIncisoCotAuto datoIncisoCotAutoEquipoEspecial 			= null;
					DatoIncisoCotAuto datoIncisoCotAutoAdaptacionConversion 	= null;
					List<DatoIncisoCotAuto> datoIncisoCotAutos 					= new ArrayList<DatoIncisoCotAuto>();
					boolean danosPorLaCargaContratada 							= false; 
					
					for(CoberturaCotizacionDTO coberturaCotizacion : coberturas){
						if(coberturaCotizacion.getCoberturaSeccionDTO().getCoberturaDTO().getNombreComercial().toUpperCase().equals(
								GeneraExcelCargaMasiva.NOMBRE_LIMITE_EQUIPO_ESPECIAL.toUpperCase())){
							datoIncisoCotAutoEquipoEspecial = cargaMasivaService.obtieneDatoIncisoCotAuto(cotizacion, coberturaCotizacion, incisoCotizacion.getId().getNumeroInciso(), 
									detalle.getDescripcionEquipoEspecial());
							if(datoIncisoCotAutoEquipoEspecial == null){
								LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
								error.setNumeroInciso(new BigDecimal(fila));
								error.setNombreSeccion("");
								error.setNombreCampoError("DESCRIPCION EQUIPO ESPECIAL");
								error.setRecomendaciones("Error al validar Descripcion Equipo Especial");
								listErrores.add(error);
								mensajeError.append("Campo: DESCRIPCION EQUIPO ESPECIAL -  Error al validar Descripcion Equipo Especial ");
							}
							datoIncisoCotAutos.add(datoIncisoCotAutoEquipoEspecial);
						}
						if(coberturaCotizacion.getCoberturaSeccionDTO().getCoberturaDTO().getNombreComercial().toUpperCase().equals(
								GeneraExcelCargaMasiva.NOMBRE_LIMITE_ADAPTACION_CONVERSION.toUpperCase())){
							datoIncisoCotAutoAdaptacionConversion = cargaMasivaService.obtieneDatoIncisoCotAuto(cotizacion, coberturaCotizacion, incisoCotizacion.getId().getNumeroInciso(), 
									detalle.getDescripcionAdaptacionConversion());
							if(datoIncisoCotAutoAdaptacionConversion == null){
								LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
								error.setNumeroInciso(new BigDecimal(fila));
								error.setNombreSeccion("");
								error.setNombreCampoError("DESCRIPCION ADAPTACION Y CONVERSION");
								error.setRecomendaciones("Error al validar Descripcion Adaptacion y Conversion");
								listErrores.add(error);
								mensajeError.append("Campo: DESCRIPCION ADAPTACION Y CONVERSION -  Error al validar Descripcion Adaptacion y Conversion ");
							}
							datoIncisoCotAutos.add(datoIncisoCotAutoAdaptacionConversion);
						}
						
						if(coberturaCotizacion.getCoberturaSeccionDTO().getCoberturaDTO().getNombreComercial().toUpperCase().equals(
				 				CoberturaDTO.DANOS_OCASIONADOS_CARGA_NOMBRE_COMERCIAL.toUpperCase())){
				 			danosPorLaCargaContratada = coberturaCotizacion.getClaveContratoBoolean();
						}
					}
										
					//Observaciones de inciso
					boolean observacionesIncisoRequerido = danosPorLaCargaContratada;
					String observacionesInciso = !StringUtils.isBlank(detalle.getObservacionesInciso()) ? detalle.getObservacionesInciso() : incisoCotizacion.getIncisoAutoCot().getObservacionesinciso();
					if (observacionesIncisoRequerido) {
						List<LogErroresCargaMasivaDTO> erroresObservaciones = new ArrayList<LogErroresCargaMasivaDTO>();
						cargaMasivaService.validaNoNulo(observacionesInciso, NOMBRE_CAMPO_OBSERVACIONES_INCISO, BigDecimal.valueOf(fila), "", erroresObservaciones);
						if (erroresObservaciones.size() == 0) {
							incisoCotizacion.getIncisoAutoCot().setObservacionesinciso(observacionesInciso);
			 			} else {
							listErrores.addAll(erroresObservaciones);
				 			mensajeError.append(cargaMasivaService.generaMensajeError(erroresObservaciones));
			 			}
					} else {
						//Guardar el valor de todos modos aunque la cobertura no este contratada.
						incisoCotizacion.getIncisoAutoCot().setObservacionesinciso(observacionesInciso);
					}
					
					
					
					//VALIDACION ALTA DE CONDICIONE ESPECIALES				
					
					if( detalle.getAltaCondicionesEspeciales() !=null ){
						listaAltaCondicionEspecial = convertirCondicionesEspeciales(detalle.getAltaCondicionesEspeciales(),  new BigDecimal(fila),METODO_ALTA);
						mensajeError.append(validarCondicionesAlta(detalle.getIdToNegocio(), detalle.getAltaCondicionesEspeciales(), new BigDecimal(fila)));
					}
			
					//VALIDACION BAJA DE CONDICIONE ESPECIALES
					
					if( detalle.getBajaCondicionesEspeciales() !=null ){
						listaBajaCondicionEspecial = convertirCondicionesEspeciales(detalle.getBajaCondicionesEspeciales(),  new BigDecimal(fila),METODO_BAJA);
						mensajeError.append(validarCondicionesBaja(detalle.getIdToNegocio(), detalle.getBajaCondicionesEspeciales(), new BigDecimal(fila)));
					}
					
					
					
					if (mensajeError.toString().isEmpty()) {
						cargaMasivaService.guardarComplementarInciso(incisoCotizacion, datoIncisoCotAutos);
						
					

					Map<String, String> mensajeEmision 	= cargaMasivaService.emitirCotizacion(cotizacion);
					String icono 						= mensajeEmision.get("icono");
					String mensaje 						= mensajeEmision.get("mensaje");
					
					if(icono.equals("30")){
						String idPoliza = mensajeEmision.get("idpoliza");
						PolizaDTO poliza = cargaMasivaService.getPolizaByStringId(idPoliza);
						HSSFCell cellStat = sheet.getRow((fila - 1)).getCell(CAMPO_NUMERO_POLIZA);
						if(cellStat == null){
							cellStat = sheet.getRow((fila - 1)).createCell(CAMPO_NUMERO_POLIZA);
						}
						cellStat.setCellValue(poliza.getNumeroPolizaFormateada());
						detalle.setMensajeError("Numero de Poliza: " + poliza.getNumeroPolizaFormateada());
						detalle.setNumeroPoliza(poliza.getNumeroPolizaFormateada());
						detalle.setIdToPoliza(poliza.getIdToPoliza());
						HSSFCell cellFechaEmision = sheet.getRow((fila - 1)).getCell(CAMPO_FECHA_EMISION);
						if(cellFechaEmision == null){
							cellFechaEmision = sheet.getRow((fila - 1)).createCell(CAMPO_FECHA_EMISION);
						}
						cellFechaEmision.setCellValue(poliza.getFechaCreacion());
						
						//Seguro obligatorio
						try{
							if(seguroObligatorioService.estaHabilitado(ParametroSeguroObligatorio.PARAMETRO_CARGA_MASIVA)){
								GeneraSeguroObligatorio generaSeguroObligatorio = new GeneraSeguroObligatorio();
								generaSeguroObligatorio.setRenovacionMasivaService(renovacionMasivaService);
								generaSeguroObligatorio.setSeguroObligatorioService(seguroObligatorioService);
								generaSeguroObligatorio.setListadoService(listadoService);
								generaSeguroObligatorio.setCalculoService(calculoService);
								PolizaDTO polizaVol = renovacionMasivaService.getPolizaDTOById(BigDecimal.valueOf(Long.valueOf(idPoliza)));
								generaSeguroObligatorio.creaPolizaSeguroObligatorio(polizaVol, PolizaAnexa.TIPO_AUTOMATICA);
							}
						}catch(Exception e){
							e.printStackTrace();
						}
					}else if(icono.equals("10")){
						LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
						error.setNumeroInciso(new BigDecimal(fila));
						error.setNombreSeccion("");
						error.setNombreCampoError("ERROR AL EMITIR COTIZACION");
						if(mensaje != null){
							mensaje = mensaje.replaceAll("\"", "");
						}
						error.setRecomendaciones(mensaje);
						listErrores.add(error);
						mensajeError.append("Campo: ").append(error.getNombreCampoError()).append(" - ").append(error.getRecomendaciones()).append(" \n");
					}
					}//NO VALIDO
					}//NO INCISO
				}else{
					if(cotizacion != null &&  cotizacion.getClaveEstatus() != null && 
							cotizacion.getClaveEstatus().equals(CotizacionDTO.ESTATUS_COT_EMITIDA)){
						//Busca poliza de la cotizacion
						PolizaDTO poliza = cargaMasivaService.getPolizaByCotizacion(cotizacion.getIdToCotizacion());
						if(poliza != null){
							HSSFCell cellStat = sheet.getRow((fila - 1)).getCell(CAMPO_NUMERO_POLIZA);
							if(cellStat == null){
								cellStat = sheet.getRow((fila - 1)).createCell(CAMPO_NUMERO_POLIZA);
							}
							cellStat.setCellValue(poliza.getNumeroPolizaFormateada());
							detalle.setMensajeError("Numero de Poliza: " + poliza.getNumeroPolizaFormateada());
							detalle.setNumeroPoliza(poliza.getNumeroPolizaFormateada());
							detalle.setIdToPoliza(poliza.getIdToPoliza());
							detalle.setIdToCotizacion(poliza.getCotizacionDTO().getIdToCotizacion());
							detalle.setFechaVigenciaInicio(poliza.getCotizacionDTO().getFechaInicioVigencia());
							detalle.setNombreORazonSocial(poliza.getCotizacionDTO().getNombreContratante());
							detalle.setApellidoPaternoCliente("");
							detalle.setApellidoMaternoCliente("");
							HSSFCell cellFechaEmision = sheet.getRow((fila - 1)).getCell(CAMPO_FECHA_EMISION);
							if(cellFechaEmision == null){
								cellFechaEmision = sheet.getRow((fila - 1)).createCell(CAMPO_FECHA_EMISION);
							}
							cellFechaEmision.setCellValue(poliza.getFechaCreacion());
						}else{
							LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
							error.setNumeroInciso(new BigDecimal(fila));
							error.setNombreSeccion("");
							error.setNombreCampoError("NUMERO DE COTIZACION");
							error.setRecomendaciones("Cotizacion Emitida sin Poliza Relacionada");
							listErrores.add(error);
							mensajeError.append("Campo: ").append(error.getNombreCampoError()).append(" - ").append(error.getRecomendaciones()).append(" \n");
						}
						
					}else{
						LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
						error.setNumeroInciso(new BigDecimal(fila));
						error.setNombreSeccion("");
						error.setNombreCampoError("NUMERO DE COTIZACION");
						error.setRecomendaciones("Cotizacion no Terminada");
						listErrores.add(error);
						mensajeError.append("Campo: ").append(error.getNombreCampoError()).append(" - ").append(error.getRecomendaciones()).append(" \n");
					}
				}
				
				
				if (mensajeError.toString().isEmpty()) {
					if(cotizacion != null &&  cotizacion.getClaveEstatus() != null && 
							cotizacion.getClaveEstatus().equals(CotizacionDTO.ESTATUS_COT_EMITIDA)){
						detalle.setClaveEstatus(DetalleCargaMasivaIndAutoCot.ESTATUS_EMITIDO_ANT);
						HSSFCell cellStat = sheet.getRow((fila - 1)).getCell(CAMPO_ESTATUS);
						if(cellStat == null){
							cellStat = sheet.getRow((fila - 1)).createCell(CAMPO_ESTATUS);
						}
						cellStat.setCellValue("POLIZA EMITIDA ANTERIORMENTE");						
					}else{
						
						
						if(listaAltaCondicionEspecial != null && listaAltaCondicionEspecial.size() > 0 ){
							guardarCondicionesEspeciales(listaAltaCondicionEspecial, cotizacion, incisoCotizacion);
						}
						
						if(listaBajaCondicionEspecial != null && listaBajaCondicionEspecial.size() > 0 ){
							eliminarCondicionesEspeciales(listaBajaCondicionEspecial, cotizacion, incisoCotizacion);
						}
						
						detalle.setClaveEstatus(DetalleCargaMasivaIndAutoCot.ESTATUS_EMITIDO);
						HSSFCell cellStat = sheet.getRow((fila - 1)).getCell(CAMPO_ESTATUS);
						if(cellStat == null){
							cellStat = sheet.getRow((fila - 1)).createCell(CAMPO_ESTATUS);
						}
						cellStat.setCellValue("POLIZA EMITIDA");
					}
				} else {
					detalle.setMensajeError(mensajeError.toString());
					detalle.setClaveEstatus(DetalleCargaMasivaIndAutoCot.ESTATUS_ERROR);
					HSSFCell cellStat = sheet.getRow((fila - 1)).getCell(CAMPO_ESTATUS);
					if(cellStat == null){
						cellStat = sheet.getRow((fila - 1)).createCell(CAMPO_ESTATUS);
					}
					cellStat.setCellValue("ERROR");
				}
			}
		}
			
	}
	
	public void procesaCargasPendientes(){
		//Obtiene cargas pendientes
		List<CargaMasivaIndividualAutoCot> cargasPendientes = cargaMasivaService.obtieneCargasMasivasIndividualesPendientes();
		for(CargaMasivaIndividualAutoCot carga: cargasPendientes){
			try {
				this.setDetalleList(new ArrayList<DetalleCargaMasivaIndAutoCot>(
						1));
				setHasLogErrors(false);
				if (carga.getIdToNegTipoPoliza() != null) {
					negocioTipoPoliza = cargaMasivaService.getNegocioTipoPolizaByCarga(carga.getIdToNegTipoPoliza());
				}
				setNegocioTipoPoliza(negocioTipoPoliza);
				Usuario usuario = cargaMasivaService.getUsuarioByCarga(carga.getCodigoUsuarioCreacion());
				cargaMasivaService.setUsuarioActual(usuario);
				setIdUsuario(usuario.getId().longValue());
				validaCargaMasiva(carga.getIdToControlArchivo(), GeneraExcelCargaMasiva.TIPO_CARGA_NORMAL, carga.getClaveTipo(), bancoMidasService);
			} catch (Exception e) {
				setHasLogErrors(true);
			}
			if(!isHasLogErrors()){
				carga.setClaveEstatus(CargaMasivaIndividualAutoCot.ESTATUS_TERMINADO);
				cargaMasivaService.guardaCargaMasivaIndividualAutoCot(carga);
			}else{
				carga.setClaveEstatus(CargaMasivaIndividualAutoCot.ESTATUS_CON_ERROR);
				cargaMasivaService.guardaCargaMasivaIndividualAutoCot(carga);				
			}
			
			cargaMasivaService.guardaDetalleCargaMasivaIndividual(carga, getDetalleList());
		}
	}
	

	
	
	public boolean validaDeducible(List<NegocioDeducibleCob> deducibles, Double valor){
		boolean esValido = false;
		if(deducibles != null){
			for(NegocioDeducibleCob deducible : deducibles){
				if(deducible.getValorDeducible().equals(valor)){
					esValido = true;
					break;
				}
			}
		}
		return esValido;
	}
	
	/*
	 * NO SE ELIMINO POR QUE ERA DIFERENTE, POR SI SE NECESITA REGRESAR EL CAMBIO
	 * public boolean validaRangos(Double valorMinimo, Double valorMaximo, Double valor){
		boolean esValido = true;
		if(valor.doubleValue() < valorMinimo.doubleValue() || valor.doubleValue() > valorMaximo.doubleValue()){
			esValido = false;
		}
		return esValido;
	}*/
	
	
	private void setInfoHoja3(){
		
	}



	@Override
	protected void buildDataSheet(HSSFSheet sheet) {
		armaCentroEmisor(sheet);
		armaNegocioSeccion(sheet);
		armaFormasPago(sheet);
		armaDerechos(sheet);
		armaEjecutivo(sheet);
		armaTipoCarga(sheet);
		armaInciso(sheet);
	}

	public void armaInciso (HSSFSheet sheet){
		HSSFRow row = sheet.createRow(dataRows++);
		row.createCell(CAMPO_CONDUCTO_COBRO).setCellValue("CONDUCTO COBRO");
		row.createCell(CAMPO_INSTITUCION_BANCARIA).setCellValue("INSTITUCION BANCARIA");
		row.createCell(CAMPO_TIPO_TARJETA).setCellValue("TIPO DE TARJETA");
		row.createCell(CAMPO_NUMERO_TARJETA).setCellValue("NUMERO DE TARJETA O CLABE");
		row.createCell(CAMPO_CODIGO_SEGURIDAD).setCellValue("CODIGO DE SEGURIDAD");
		HSSFCell cell = row.createCell(CAMPO_FECHA_VENCIMIENTO);
		cell.setCellValue("FECHA VENCIMIENTO");
		cell.setCellStyle(dateStyleFV);
		
	}

	protected void buildValidationsSheet(HSSFSheet sheet) {
		llenaLineaEjemplo(sheet);

		DVConstraint dvConstraintSINO = DVConstraint.createExplicitListConstraint(new String[] { GeneraExcelCargaMasiva.VALOR_SI, GeneraExcelCargaMasiva.VALOR_NO });
		DVConstraint dvConstraintMONEDA = DVConstraint.createExplicitListConstraint(new String[] { PESOS, DOLARES });
		DVConstraint dvConstraintTIPOPERSONA = DVConstraint.createExplicitListConstraint(new String[] { FISICA, MORAL });
		
		HSSFDataValidation data_validation = new HSSFDataValidation(
				new CellRangeAddressList(1, maxRows, CAMPO_CLIENTE_VIP,CAMPO_CLIENTE_VIP), dvConstraintSINO);
		data_validation.setSuppressDropDownArrow(false);
		sheet.addValidationData(data_validation);
		
		data_validation = new HSSFDataValidation(
				new CellRangeAddressList(1, maxRows, CAMPO_ASISTENCIA_JURIDICA,CAMPO_ASISTENCIA_JURIDICA), dvConstraintSINO);
		data_validation.setSuppressDropDownArrow(false);
		sheet.addValidationData(data_validation);
		
		data_validation = new HSSFDataValidation(
				new CellRangeAddressList(1, maxRows, CAMPO_IGUALACION,CAMPO_IGUALACION), dvConstraintSINO);
		data_validation.setSuppressDropDownArrow(false);
		sheet.addValidationData(data_validation);
		
		data_validation = new HSSFDataValidation(
				new CellRangeAddressList(1, maxRows, CAMPO_SOLICITAR_AUTORIZACION,CAMPO_SOLICITAR_AUTORIZACION), dvConstraintSINO);
		data_validation.setSuppressDropDownArrow(false);
		sheet.addValidationData(data_validation);
		
		data_validation = new HSSFDataValidation(
				new CellRangeAddressList(1, maxRows, CAMPO_CAUSA_AUTORIZACION,CAMPO_CAUSA_AUTORIZACION), dvConstraintSINO);
		data_validation.setSuppressDropDownArrow(false);
		sheet.addValidationData(data_validation);
		
		data_validation = new HSSFDataValidation(
				new CellRangeAddressList(1, maxRows, CAMPO_MONEDA,CAMPO_MONEDA), dvConstraintMONEDA);
		data_validation.setSuppressDropDownArrow(false);
		sheet.addValidationData(data_validation);
		
		DVConstraint dvConstraint = DVConstraint.createFormulaListConstraint("CENTROEMISOR");
		data_validation = new HSSFDataValidation(
		new CellRangeAddressList(1, maxRows, CAMPO_ID_CENTRO_EMISOR,CAMPO_ID_CENTRO_EMISOR), dvConstraint);
		data_validation.setSuppressDropDownArrow(false);
		sheet.addValidationData(data_validation);
		
		DVConstraint dvConstraint2 = DVConstraint.createDateConstraint(DVConstraint.OperatorType.GREATER_THAN,
				"01/01/1900", "01/01/1900", "dd/MM/yyyy");
		data_validation = new HSSFDataValidation(new CellRangeAddressList(1,
				maxRows, CAMPO_FECHA_VIGENCIA_INICIO, CAMPO_FECHA_VIGENCIA_FIN),
				dvConstraint2);
		sheet.addValidationData(data_validation);
		
		DVConstraint dvConstraint3 = DVConstraint.createNumericConstraint(
				ValidationType.INTEGER, DVConstraint.OperatorType.GREATER_THAN,
				"0", "0");
		DVConstraint dvConstraint4 = DVConstraint.createNumericConstraint(
				ValidationType.DECIMAL, DVConstraint.OperatorType.GREATER_THAN,
				"0", "0");
		DVConstraint dvConstraint5 = DVConstraint.createNumericConstraint(
				ValidationType.DECIMAL, DVConstraint.OperatorType.GREATER_OR_EQUAL,
				"0", "0");
		data_validation = new HSSFDataValidation(
				new CellRangeAddressList(1, maxRows, CAMPO_NUMERO_POLIZA,
						CAMPO_NUMERO_POLIZA), dvConstraint3);
		sheet.addValidationData(data_validation);
		
		data_validation = new HSSFDataValidation(
				new CellRangeAddressList(1, maxRows, CAMPO_NUMERO_COTIZACION,
						CAMPO_NUMERO_COTIZACION), dvConstraint3);
		sheet.addValidationData(data_validation);
		
		data_validation = new HSSFDataValidation(
				new CellRangeAddressList(1, maxRows, CAMPO_ID_AGENTE,
						CAMPO_ID_AGENTE), dvConstraint3);
		sheet.addValidationData(data_validation);
		
		data_validation = new HSSFDataValidation(
				new CellRangeAddressList(1, maxRows, CAMPO_IDCLIENTE,
						CAMPO_IDCLIENTE), dvConstraint3);
		sheet.addValidationData(data_validation);
		
		data_validation = new HSSFDataValidation(
				new CellRangeAddressList(1, maxRows, CAMPO_PRIMA_TOTAL,
						CAMPO_PRIMA_TOTAL), dvConstraint4);
		sheet.addValidationData(data_validation);
		
		data_validation = new HSSFDataValidation(
				new CellRangeAddressList(1, maxRows, CAMPO_DEDUCIBLE_DANOS_MATERIALES,
						CAMPO_LIMITE_RC_TERCEROS), dvConstraint3);
		sheet.addValidationData(data_validation);
		
		data_validation = new HSSFDataValidation(
				new CellRangeAddressList(1, maxRows, CAMPO_DEDUCIBLE_RC_TERCEROS,
						CAMPO_DEDUCIBLE_RC_TERCEROS), dvConstraint5);
		sheet.addValidationData(data_validation);
		
		data_validation = new HSSFDataValidation(
				new CellRangeAddressList(1, maxRows, CAMPO_LIMITE_GASTOS_MEDICOS,
						CAMPO_LIMITE_RC_VIAJERO), dvConstraint3);
		sheet.addValidationData(data_validation);
		
		data_validation = new HSSFDataValidation(
				new CellRangeAddressList(1, maxRows, CAMPO_LIMITE_ADAPTACION_CONVERSION,
						CAMPO_LIMITE_ADAPTACION_CONVERSION), dvConstraint3);
		sheet.addValidationData(data_validation);
		
		data_validation = new HSSFDataValidation(
				new CellRangeAddressList(1, maxRows, CAMPO_LIMITE_EQUIPO_ESPECIAL,
						CAMPO_LIMITE_EQUIPO_ESPECIAL), dvConstraint3);
		sheet.addValidationData(data_validation);
		
		data_validation = new HSSFDataValidation(
				new CellRangeAddressList(1, maxRows, CAMPO_DEDUCIBLE_EQUIPO_ESPECIAL,
						CAMPO_DEDUCIBLE_EQUIPO_ESPECIAL), dvConstraint3);
		sheet.addValidationData(data_validation);
		
		data_validation = new HSSFDataValidation(
				new CellRangeAddressList(1, maxRows, CAMPO_TIPO_PERSONA_CLIENTE,CAMPO_TIPO_PERSONA_CLIENTE), dvConstraintTIPOPERSONA);
		data_validation.setSuppressDropDownArrow(false);
		sheet.addValidationData(data_validation);
		
		data_validation = new HSSFDataValidation(
				new CellRangeAddressList(1, maxRows, CAMPO_TIPO_PERSONA_REPRESENTANTE,CAMPO_TIPO_PERSONA_REPRESENTANTE), dvConstraintTIPOPERSONA);
		data_validation.setSuppressDropDownArrow(false);
		sheet.addValidationData(data_validation);
		
		dvConstraint = DVConstraint.createFormulaListConstraint("NEGOCIOSECCION");
		data_validation = new HSSFDataValidation(
		new CellRangeAddressList(1, maxRows, CAMPO_LINEA_NEGOCIO,CAMPO_LINEA_NEGOCIO), dvConstraint);
		data_validation.setSuppressDropDownArrow(false);
		sheet.addValidationData(data_validation);
		
		dvConstraint = DVConstraint.createFormulaListConstraint("FORMASPAGO");
		data_validation = new HSSFDataValidation(
		new CellRangeAddressList(1, maxRows, CAMPO_FORMA_PAGO,CAMPO_FORMA_PAGO), dvConstraint);
		data_validation.setSuppressDropDownArrow(false);
		sheet.addValidationData(data_validation);
		
		dvConstraint = DVConstraint.createFormulaListConstraint("DERECHOS");
		data_validation = new HSSFDataValidation(
		new CellRangeAddressList(1, maxRows, CAMPO_DERECHOS,CAMPO_DERECHOS), dvConstraint);
		data_validation.setSuppressDropDownArrow(false);
		sheet.addValidationData(data_validation);
		
		dvConstraint = DVConstraint.createFormulaListConstraint("OFICINAS");
		data_validation = new HSSFDataValidation(
		new CellRangeAddressList(1, maxRows, CAMPO_ID_OFICINA,CAMPO_ID_OFICINA), dvConstraint);
		data_validation.setSuppressDropDownArrow(false);
		sheet.addValidationData(data_validation);
		
		CellReference cellRef = new CellReference(1, CAMPO_LINEA_NEGOCIO);
		
		for (int i = 2; i < maxRows; i++) {

			dvConstraint = DVConstraint
					.createFormulaListConstraint("INDIRECT(UPPER(\"P_\"&$" + cellRef.getCellRefParts()[2] + "$"
							+ i + "))");
			data_validation = new HSSFDataValidation(new CellRangeAddressList(
					1, (i - 1), CAMPO_ID_PAQUETE, CAMPO_ID_PAQUETE), dvConstraint);
			sheet.addValidationData(data_validation);
			
			dvConstraint = DVConstraint
					.createFormulaListConstraint("INDIRECT(UPPER(\"TP_\"&$"
							+ cellRef.getCellRefParts()[2] + "$" + i + "))");
			data_validation = new HSSFDataValidation(new CellRangeAddressList(
					1, (i - 1), CAMPO_CVE_USO, CAMPO_CVE_USO),
					dvConstraint);
			sheet.addValidationData(data_validation);

		}
		
		DVConstraint dvConstraint0_9 = DVConstraint
		.createNumericConstraint(ValidationType.INTEGER,
				DVConstraint.OperatorType.BETWEEN, "0", "9");
		
		data_validation = new HSSFDataValidation(new CellRangeAddressList(1,
				maxRows, CAMPO_NUMERO_REMOLQUES,
				CAMPO_NUMERO_REMOLQUES), dvConstraint0_9);
		sheet.addValidationData(data_validation);
		
		
		dvConstraint = DVConstraint
				.createFormulaListConstraint(CAMPO_TIPO_CARGA_NOMBRE_RANGO);
		data_validation = new HSSFDataValidation(
				new CellRangeAddressList(1, maxRows, CAMPO_TIPO_CARGA,
						CAMPO_TIPO_CARGA), dvConstraint);
		data_validation.setSuppressDropDownArrow(false);
		sheet.addValidationData(data_validation);
		
		data_validation = new HSSFDataValidation(
				new CellRangeAddressList(1, maxRows, CAMPO_LIMITE_ACCIDENTES_CONDUCTOR,
						CAMPO_LIMITE_ACCIDENTES_CONDUCTOR), dvConstraint3);
		sheet.addValidationData(data_validation);
		
		DVConstraint dvConstraintTipoConductoCobro = DVConstraint
				.createExplicitListConstraint(new String[] { VALOR_TC, VALOR_DOMICILIACION, VALOR_EFECTIVO ,VALOR_CUENTAAFIRME});
		data_validation = new HSSFDataValidation(new CellRangeAddressList(1,
				maxRows, CAMPO_CONDUCTO_COBRO,
				CAMPO_CONDUCTO_COBRO), dvConstraintTipoConductoCobro);
		data_validation.setSuppressDropDownArrow(false);
		sheet.addValidationData(data_validation);

		bancos = listadoService.getBancos();
		String[] bancosArray = bancos.values().toArray(new String[0]);
		Arrays.sort(bancosArray);
		
		DVConstraint dvConstraintInsBan = DVConstraint
				.createExplicitListConstraint(bancosArray);
		data_validation = new HSSFDataValidation(new CellRangeAddressList(1,
				maxRows, CAMPO_INSTITUCION_BANCARIA,
				CAMPO_INSTITUCION_BANCARIA), dvConstraintInsBan);
		data_validation.setSuppressDropDownArrow(false);
		sheet.addValidationData(data_validation);
		
		DVConstraint dvConstraintTipoTC = DVConstraint
				.createExplicitListConstraint(new String[] { VALOR_VISA, VALOR_MASTERCARD });
		data_validation = new HSSFDataValidation(new CellRangeAddressList(1,
				maxRows, CAMPO_TIPO_TARJETA,
				CAMPO_TIPO_TARJETA), dvConstraintTipoTC);
		data_validation.setSuppressDropDownArrow(false);
		sheet.addValidationData(data_validation);
		
		DVConstraint dvConstraintNumTarjeta = DVConstraint.createNumericConstraint(ValidationType.INTEGER, DVConstraint.OperatorType.GREATER_THAN, "0", "0");
		data_validation = new HSSFDataValidation(new CellRangeAddressList(1,
				maxRows, CAMPO_NUMERO_TARJETA, CAMPO_NUMERO_TARJETA),
				dvConstraintNumTarjeta);
		sheet.addValidationData(data_validation);
		
		DVConstraint dvConstraintCVV = DVConstraint.createNumericConstraint(ValidationType.INTEGER, DVConstraint.OperatorType.GREATER_THAN, "0", "0");
		data_validation = new HSSFDataValidation(new CellRangeAddressList(1,
				maxRows, CAMPO_CODIGO_SEGURIDAD, CAMPO_CODIGO_SEGURIDAD),
				dvConstraintCVV);
		sheet.addValidationData(data_validation);	
	}



	@Override
	protected void leeLineasArchivoCarga(HSSFSheet sheet) {
		
		Iterator<Row> rowIterator 	= sheet.rowIterator();
		boolean isFirst 			= true;
		HSSFRow hssfRow 			= null;
		
		while (rowIterator.hasNext()) {
			hssfRow = (HSSFRow) rowIterator.next();
			if (!isFirst) {
				DetalleCargaMasivaIndAutoCot detalle = creaLineaDetalleCarga(hssfRow);
				if(detalle != null){
					detalleList.add(detalle);
				}
			} else {
				isFirst = false;
			}
		}
				
		// Valida datos para cotizacion
		try{
			if (tipoCarga.equals(GeneraExcelCargaMasiva.TIPO_CARGA_NORMAL)) {
				if (claveTipo.equals(CargaMasivaIndividualAutoCot.TIPO_COTIZACION)) {
					this.validaDetalleCargaMasiva(detalleList, sheet);
				}
				if (claveTipo.equals(CargaMasivaIndividualAutoCot.TIPO_EMISION)) {
					this.validaEmisionCargaMasiva(detalleList, sheet);
				}
			}
		}catch(Exception e){
			LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
			error.setNumeroInciso(new BigDecimal(0));
			error.setNombreSeccion("");
			error.setNombreCampoError("ERROR INESPERADO");
			error.setRecomendaciones("Error Inesperado.");
			listErrores.add(error);
			e.printStackTrace();
		}

		if (this.getListErrores().isEmpty()) {
			System.out.println("Sin Errores de carga");
			setHasLogErrors(false);
		} else {
			creaLogErrores();
			setHasLogErrors(true);
		}
		
	}
	
	
	/*private HashMap<Long,NegocioCondicionEspecial> fillInfoNegocioEspecial(){
		HashMap<Long,NegocioCondicionEspecial> map = new HashMap<Long,NegocioCondicionEspecial> ();
		
		NegocioCondicionEspecial entidad1 		= new NegocioCondicionEspecial();
		CondicionEspecial condicionEspecial1 	= new CondicionEspecial();
		condicionEspecial1.setCodigo(1L);
		entidad1.setId(1L);
		entidad1.setCondicionEspecial(condicionEspecial1);
		map.put(condicionEspecial1.getCodigo(), entidad1);
		
		NegocioCondicionEspecial entidad2 		= new NegocioCondicionEspecial();
		CondicionEspecial condicionEspecial2 	= new CondicionEspecial();
		condicionEspecial1.setCodigo(3L);
		entidad2.setId(3L);
		entidad2.setCondicionEspecial(condicionEspecial2);
		map.put(condicionEspecial2.getCodigo(), entidad2);
		
		NegocioCondicionEspecial entidad3 		= new NegocioCondicionEspecial();
		CondicionEspecial condicionEspecial3 	= new CondicionEspecial();
		condicionEspecial3.setCodigo(4L);
		entidad3.setId(4L);
		entidad3.setCondicionEspecial(condicionEspecial3);
		map.put(condicionEspecial3.getCodigo(), entidad3);
		
		NegocioCondicionEspecial entidad4 		= new NegocioCondicionEspecial();
		CondicionEspecial condicionEspecial4 	= new CondicionEspecial();
		condicionEspecial4.setCodigo(8L);
		entidad4.setId(8L);
		entidad4.setCondicionEspecial(condicionEspecial4);
		map.put(condicionEspecial4.getCodigo(), entidad4);
		
		return map;
	}*/
	
	private Short validaSINO(String campo) {
		Short valor = 0;
		if (campo.equals(VALOR_SI)) {
			valor = 1;
		}
		return valor;
	}	
	
	
	
	/**  GETTER & SETTER	 */
	
	
	
	
	public void setListErrores(List<LogErroresCargaMasivaDTO> listErrores) {
		this.listErrores = listErrores;
	}
	public List<LogErroresCargaMasivaDTO> getListErrores() {
		return listErrores;
	}
	public void setControlArchivoDTO(ControlArchivoDTO controlArchivoDTO) {
		this.controlArchivoDTO = controlArchivoDTO;
	}
	public ControlArchivoDTO getControlArchivoDTO() {
		return controlArchivoDTO;
	}
	public void setCargaMasivaService(CargaMasivaService cargaMasivaService) {
		this.cargaMasivaService = cargaMasivaService;
	}
	public CargaMasivaService getCargaMasivaService() {
		return cargaMasivaService;
	}
	public void setLogErrores(FileInputStream logErrores) {
		this.logErrores = logErrores;
	}
	public FileInputStream getLogErrores() {
		return logErrores;
	}
	public void setHasLogErrors(boolean hasLogErrors) {
		this.hasLogErrors = hasLogErrors;
	}
	public boolean isHasLogErrors() {
		return hasLogErrors;
	}
	public void setTipoCarga(Short tipoCarga) {
		this.tipoCarga = tipoCarga;
	}
	public Short getTipoCarga() {
		return tipoCarga;
	}



	public void setNegocioTipoPoliza(NegocioTipoPoliza negocioTipoPoliza) {
		this.negocioTipoPoliza = negocioTipoPoliza;
	}



	public NegocioTipoPoliza getNegocioTipoPoliza() {
		return negocioTipoPoliza;
	}



	public void setDetalleList(List<DetalleCargaMasivaIndAutoCot> detalleList) {
		this.detalleList = detalleList;
	}



	public List<DetalleCargaMasivaIndAutoCot> getDetalleList() {
		return detalleList;
	}



	public void setIdUsuario(Long idUsuario) {
		this.idUsuario = idUsuario;
	}



	public Long getIdUsuario() {
		return idUsuario;
	}



	public void setClaveTipo(Short claveTipo) {
		this.claveTipo = claveTipo;
	}



	public Short getClaveTipo() {
		return claveTipo;
	}



	public void setEjemplo(DetalleCargaMasivaIndAutoCot ejemplo) {
		this.ejemplo = ejemplo;
	}

	public DetalleCargaMasivaIndAutoCot getEjemplo() {
		return ejemplo;
	}



	public BigDecimal getIdToNegocio() {
		return idToNegocio;
	}


	public void setIdToNegocio(BigDecimal idToNegocio) {
		this.idToNegocio = idToNegocio;
	}

	public ClienteFacadeRemote getClienteFacadeRemote() {
		return clienteFacadeRemote;
	}

	public void setClienteFacadeRemote(ClienteFacadeRemote clienteFacadeRemote) {
		this.clienteFacadeRemote = clienteFacadeRemote;
	}

	public DireccionMidasService getDireccionMidasService() {
		return direccionMidasService;
	}

	public void setDireccionMidasService(DireccionMidasService direccionMidasService) {
		this.direccionMidasService = direccionMidasService;
	}

	public void setPersonaSeycosFacadeRemote(PersonaSeycosFacadeRemote personaSeycosFacadeRemote) {
		this.personaSeycosFacadeRemote = personaSeycosFacadeRemote;
	}
	
}
