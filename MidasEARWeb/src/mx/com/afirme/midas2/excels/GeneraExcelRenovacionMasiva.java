package mx.com.afirme.midas2.excels;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import mx.com.afirme.midas.catalogos.codigo.postal.PaisDTO;
import mx.com.afirme.midas.consultas.formapago.FormaPagoDTO;
import mx.com.afirme.midas.consultas.mediopago.MedioPagoDTO;
import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionId;
import mx.com.afirme.midas.interfaz.cliente.ClienteGenericoDTO;
import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.BitemporalCotizacion;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.BitemporalInciso;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.auto.BitemporalAutoInciso;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion.BitemporalSeccionInciso;
import mx.com.afirme.midas2.domain.catalogos.DomicilioImpresion;
import mx.com.afirme.midas2.domain.negocio.renovacion.NegocioRenovacion;
import mx.com.afirme.midas2.domain.poliza.renovacionmasiva.OrdenRenovacionMasiva;
import mx.com.afirme.midas2.domain.poliza.renovacionmasiva.OrdenRenovacionMasivaDet;
import mx.com.afirme.midas2.domain.poliza.renovacionmasiva.bitacoraguia.BitacoraGuia;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.inciso.IncisoAutoCot;
import mx.com.afirme.midas2.dto.ControlDescripcionArchivoDTO;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.dto.pago.CuentaPagoDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.ResumenCostosDTO;
import mx.com.afirme.midas2.service.bitemporal.EntidadBitemporal;
import mx.com.afirme.midas2.service.custShipmentTracking.TrackingService;
import mx.com.afirme.midas2.service.poliza.renovacionmasiva.RenovacionMasivaService;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.springframework.beans.factory.annotation.Autowired;

import com.lowagie.text.Document;
import com.lowagie.text.pdf.PRAcroForm;
import com.lowagie.text.pdf.PdfCopy;
import com.lowagie.text.pdf.PdfImportedPage;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.SimpleBookmark;

public class GeneraExcelRenovacionMasiva {
	
	public static final Logger LOG = Logger.getLogger(GeneraExcelRenovacionMasiva.class);
	
	private int dataRows = 0;
	// private int maxRows = SpreadsheetVersion.EXCEL97.getMaxRows();
	private static final int maxColumns = 63;
	private static final int max = 100; 
	private String fileName;
	
	private RenovacionMasivaService renovacionMasivaService;
	private TrackingService trackingService;
	
	public static final int TIPO_EXCEL_SISTEMA = 1;
	public static final int TIPO_EXCEL_PROVEEDOR = 2;
	
	public GeneraExcelRenovacionMasiva() {
	}
	

	public InputStream generaPlantillaCotizacionesRenovacion(BigDecimal idToOrdenRenovacion, int tipo)
			throws IOException {
		HSSFWorkbook workbook = null;
		HSSFSheet cotizaciones = null;
		ByteArrayOutputStream bos = null;

		try {
			// Obtiene plantilla con macros
			workbook = cargaPlantilla();
			cotizaciones = workbook.getSheetAt(0);
			
			llenaDatosCotizacionesRenovacion(cotizaciones, idToOrdenRenovacion, tipo);

			// Ajusta tamano de columnas
			for (int i = 0; i <= maxColumns; i++) {
				cotizaciones.autoSizeColumn(i);
			}

			// Muestra primera hoja
			workbook.setActiveSheet(0);
			short firstCell = 0;
			cotizaciones.showInPane(firstCell, firstCell);

			bos = new ByteArrayOutputStream();

			workbook.write(bos);

		} catch (Exception e) {
			LOG.error(e);
		} finally {
			if (bos != null) {
				bos.close();
			}
		}

		if(bos == null){
			return null;
		}
		return new ByteArrayInputStream(bos.toByteArray());
	}
	
	public InputStream generaExcelControl(BigDecimal idToOrdenRenovacion) throws IOException {
		HSSFWorkbook workbook = null;
		HSSFSheet cotizaciones = null;
		ByteArrayOutputStream bos = null;

		try {
			// Obtiene plantilla con macros
			workbook = cargaPlantilla();
			cotizaciones = workbook.getSheetAt(0);

			llenaDatosExcel(cotizaciones, idToOrdenRenovacion);

			// Ajusta tamano de columnas
			for (int i = 0; i <= maxColumns; i++) {
				cotizaciones.autoSizeColumn(i);
			}

			// Muestra primera hoja
			workbook.setActiveSheet(0);
			short firstCell = 0;
			cotizaciones.showInPane(firstCell, firstCell);

			bos = new ByteArrayOutputStream();

			workbook.write(bos);

		} catch (Exception e) {
			LOG.error(e);
		} finally {
			if (bos != null) {
				bos.close();
			}
		}

		if (bos == null) {
			return null;
		}
		return new ByteArrayInputStream(bos.toByteArray());
	}

	private HSSFWorkbook cargaPlantilla() throws IOException {
		HSSFWorkbook workbook = null;
		POIFSFileSystem plantilla = new POIFSFileSystem(
				this.getClass()
						.getResourceAsStream(
								"/mx/com/afirme/midas2/impresiones/jrxml/plantillaRenovacion.xls"));
		workbook = new HSSFWorkbook(plantilla);
		return workbook;
	}
	
	private void llenaDatosCotizacionesRenovacion(HSSFSheet cotizaciones, BigDecimal idToOrdenRenovacion, int tipo){
		llenaEncabezados(cotizaciones, tipo);
		llenaDatos(cotizaciones, idToOrdenRenovacion);
	}
	
	@SuppressWarnings("rawtypes")
	private void llenaDatos(HSSFSheet sheet, BigDecimal idToOrdenRenovacion){
		OrdenRenovacionMasiva orden = renovacionMasivaService.obtieneOrdenRenovacionMasiva(idToOrdenRenovacion);
		List<OrdenRenovacionMasivaDet> ordenesDetalle = getDetalleOrdenRenovacionMasiva(idToOrdenRenovacion);
		this.dataRows = 1;
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		for(OrdenRenovacionMasivaDet ordenDet : ordenesDetalle){
			try{
				IncisoAutoCot incisoAutoCot = getIncisoAutoCotizacion(ordenDet.getIdToCotizacion(), 
						ordenDet.getCotizacion().getTipoPolizaDTO().getClaveAplicaFlotillas() == 0);
				IncisoAutoCot incisoAutoCotAnt = null;
				ClienteGenericoDTO clienteContratante = null;
				List<EntidadBitemporal> coberturasCotinuityList = null;
			
			
			if(ordenDet.getPolizaAnterior().getCotizacionDTO() != null &&
					ordenDet.getPolizaAnterior().getCotizacionDTO().getTipoPolizaDTO().getClaveAplicaFlotillas() == 0){
				//incisoAutoCotAnt = renovacionMasivaService.regresaIncisoAutoCot(ordenDet.getPolizaAnterior().getCotizacionDTO().getIdToCotizacion());
				//Obtiene datos de bitemporalidad
				try{
					BitemporalCotizacion cotizacion = renovacionMasivaService.obtieneBitemporalCotizacionByIdCotizacion(ordenDet.getPolizaAnterior().getCotizacionDTO().getIdToCotizacion());
					List<EntidadBitemporal> incisoCotinuityList = renovacionMasivaService.obtenerIncisoCotinuityList(cotizacion.getContinuity().getId());
					for(EntidadBitemporal item : incisoCotinuityList){			
						BitemporalInciso bitemporalInciso = (BitemporalInciso) item;
						IncisoCotizacionDTO inciso = new IncisoCotizacionDTO(bitemporalInciso.getValue());
						List<EntidadBitemporal> autoIncisoCotinuityList = renovacionMasivaService.obtenerAutoIncisoCotinuityList(bitemporalInciso.getContinuity().getId());
						if(autoIncisoCotinuityList != null && autoIncisoCotinuityList.size() > 0){
							BitemporalAutoInciso autoIncisoContinuity = (BitemporalAutoInciso) autoIncisoCotinuityList.get(0);
							incisoAutoCotAnt = new IncisoAutoCot(autoIncisoContinuity.getValue());
							IncisoCotizacionId incisoCotizacionId = new IncisoCotizacionId();
							incisoCotizacionId.setNumeroInciso(new BigDecimal(bitemporalInciso.getContinuity().getNumero()));
							incisoCotizacionId.setIdToCotizacion(ordenDet.getPolizaAnterior().getCotizacionDTO().getIdToCotizacion());
							inciso.setId(incisoCotizacionId);
							incisoAutoCotAnt.setIncisoCotizacionDTO(inciso);
						}						
						List<EntidadBitemporal> seccionIncisoCotinuityList =  renovacionMasivaService.obtenerSeccionIncisoCotinuityList(bitemporalInciso.getContinuity().getId());
						for(EntidadBitemporal item2 : seccionIncisoCotinuityList){
							BitemporalSeccionInciso bitemporalSeccionInciso = (BitemporalSeccionInciso) item2;
							if(bitemporalSeccionInciso != null && bitemporalSeccionInciso.getValue() != null){
								coberturasCotinuityList = renovacionMasivaService.obtenerCoberturasCotinuityList(bitemporalSeccionInciso.getContinuity().getId());							
							}
						}
					}
				}catch(Exception e){
					LOG.error(e);
				}
			}
			if(ordenDet.getIdToCotizacion() != null && ordenDet.getCotizacion().getIdToPersonaContratante() != null){
				clienteContratante = getDatosCliente(ordenDet.getCotizacion().getIdToPersonaContratante());
			}
			
			ResumenCostosDTO resumenCostosDTO = null;
			try{
				resumenCostosDTO = renovacionMasivaService.resumenCostosCaratulaPoliza(ordenDet.getPolizaAnterior().getIdToPoliza());
			}catch(Exception e){
				LOG.error(e);
			}
			ResumenCostosDTO resumenCostosRen = null;
			try{
				//resumenCostosRen = renovacionMasivaService.resumenCostosCaratulaPoliza(ordenDet.getIdToPolizaRenovada());
				resumenCostosRen = renovacionMasivaService.obtenerResumenCotizacion(ordenDet.getCotizacion());
			}catch(Exception e){
				LOG.error(e);
			}
			
			HSSFRow row = sheet.createRow(dataRows);
			HSSFCell cell = row.createCell(OrdenRenovacionMasiva.SIT_POLIZA);
			cell.setCellValue(ordenDet.getDescripcionEstatus());
			cell = row.createCell(OrdenRenovacionMasiva.ID_COTIZACION);
			if(ordenDet.getIdToCotizacion() != null){
				cell.setCellValue(ordenDet.getIdToCotizacion() + "");
			}else{
				cell.setCellValue("");
			}
			cell = row.createCell(OrdenRenovacionMasiva.ID_VERSION_POL);
			if(ordenDet.getIdToPolizaRenovada() != null){
				cell.setCellValue(ordenDet.getPolizaRenovada().getNumeroRenovacion());
			}else{
				cell.setCellValue("");
			}
			cell = row.createCell(OrdenRenovacionMasiva.ID_CONTRATANTE);
			if(ordenDet.getIdToCotizacion() != null){
				cell.setCellValue(ordenDet.getCotizacion().getIdToPersonaContratante() + "");
			}else{
				cell.setCellValue("");
			}
			cell = row.createCell(OrdenRenovacionMasiva.ID_PRODUCTO);
			if(ordenDet.getIdToCotizacion() != null){
				cell.setCellValue(ordenDet.getCotizacion().getSolicitudDTO().getProductoDTO().getIdToProducto() + "");
			}else{
				cell.setCellValue("");
			}
			cell = row.createCell(OrdenRenovacionMasiva.CVE_T_POLIZA);
			if(ordenDet.getIdToCotizacion() != null){
				cell.setCellValue(ordenDet.getCotizacion().getTipoPolizaDTO().getNombreComercial() + "");
			}else{
				cell.setCellValue("");
			}
			cell = row.createCell(OrdenRenovacionMasiva.F_INI_VIGENCIA);
			if(ordenDet.getIdToCotizacion() != null){
				cell.setCellValue(sdf.format(ordenDet.getCotizacion().getFechaInicioVigencia()) + "");
			}else{
				cell.setCellValue("");
			}
			cell = row.createCell(OrdenRenovacionMasiva.F_FIN_VIGENCIA);
			if(ordenDet.getIdToCotizacion() != null){
				cell.setCellValue(sdf.format(ordenDet.getCotizacion().getFechaFinVigencia()) + "");
			}else{
				cell.setCellValue("");
			}
			cell = row.createCell(OrdenRenovacionMasiva.NUM_COTIZACION);
			if(ordenDet.getCotizacion() != null){
				cell.setCellValue(ordenDet.getCotizacion().getIdToCotizacion() + "");
			}else{
				cell.setCellValue("");
			}
			cell = row.createCell(OrdenRenovacionMasiva.NUM_POLIZA);
			if(ordenDet.getIdToPolizaRenovada() != null){
				cell.setCellValue(ordenDet.getPolizaRenovada().getNumeroPolizaFormateada() + "");
			}else{
				cell.setCellValue("");
			}
			cell = row.createCell(OrdenRenovacionMasiva.NUM_POLIZA_ANT);
			if(ordenDet.getPolizaAnterior() != null){
				cell.setCellValue(ordenDet.getPolizaAnterior().getNumeroPolizaFormateada() + "");
			}else{
				cell.setCellValue("");
			}
			cell = row.createCell(OrdenRenovacionMasiva.ID_USUARIO_EMIS);
			if(ordenDet.getPolizaRenovada() != null && ordenDet.getPolizaRenovada().getCodigoUsuarioCreacion() != null){
				cell.setCellValue(ordenDet.getPolizaRenovada().getCodigoUsuarioCreacion() + "");
			}else{
				cell.setCellValue("");
			}
			cell = row.createCell(OrdenRenovacionMasiva.NOM_SOLICITANTE);
			if(ordenDet.getIdToCotizacion() != null){
				cell.setCellValue(ordenDet.getCotizacion().getNombreContratante() + "");
			}else{
				cell.setCellValue("");
			}
			cell = row.createCell(OrdenRenovacionMasiva.ID_CENTRO_EMISRG);
			if(ordenDet.getIdToPolizaRenovada() != null){
				cell.setCellValue(ordenDet.getPolizaRenovada().getIdCentroEmisor() + "");
			}else{
				cell.setCellValue("");
			}
			
			cell = row.createCell(OrdenRenovacionMasiva.ID_EMPRESA);
			cell.setCellValue("8");
			/*
			cell = row.createCell(OrdenRenovacionMasiva.ID_CENTRO_EMIS);
			if(ordenDet.getIdToCotizacion() != null){
				cell.setCellValue(ordenDet.getCotizacion().getSolicitudDTO().getIdCentroEmisor() + "");
			}else{
				cell.setCellValue("");
			}*/
			
			cell = row.createCell(OrdenRenovacionMasiva.ID_USUARIO_CREAC);
			if(ordenDet.getIdToCotizacion() != null){
				cell.setCellValue(ordenDet.getCotizacion().getCodigoUsuarioCreacion() + "");
			}else{
				cell.setCellValue("");
			}
			cell = row.createCell(OrdenRenovacionMasiva.ID_BITACORA);
			cell.setCellValue("");
			
			cell = row.createCell(OrdenRenovacionMasiva.ID_MONEDA);
			if(ordenDet.getIdToCotizacion() != null){
				cell.setCellValue(ordenDet.getCotizacion().getIdMoneda() + "");
			}else{
				cell.setCellValue("");
			}
			cell = row.createCell(OrdenRenovacionMasiva.CVE_T_POLIZA_1);
			if(ordenDet.getIdToPolizaRenovada() != null){
				cell.setCellValue(ordenDet.getPolizaRenovada().getCotizacionDTO().getTipoPolizaDTO().getNombreComercial() + "");
			}else{
				cell.setCellValue("");
			}
			cell = row.createCell(OrdenRenovacionMasiva.ID_USUARIO_AUTOR);
			if(orden != null){
				cell.setCellValue(orden.getCodigoUsuarioCreacion() + "");
			}else{
				cell.setCellValue("");
			}
			cell = row.createCell(OrdenRenovacionMasiva.IMP_PRIMA_TOTAL);
			
			BigDecimal primaActual = BigDecimal.ZERO;
			if(ordenDet.getCotizacion() != null && ordenDet.getCotizacion().getValorPrimaTotal() != null){
				cell.setCellValue(ordenDet.getCotizacion().getValorPrimaTotal().doubleValue());
				primaActual = ordenDet.getCotizacion().getValorPrimaTotal();
			}else{
				cell.setCellValue("");
			}
			cell = row.createCell(OrdenRenovacionMasiva.FH_CREACION);
			if(ordenDet.getIdToCotizacion() != null){
				cell.setCellValue(sdf.format(ordenDet.getCotizacion().getFechaCreacion()) + "");
			}else{
				cell.setCellValue("");
			}
			cell = row.createCell(OrdenRenovacionMasiva.IMP_PRIMA_ANT);
			BigDecimal primaAnterior = BigDecimal.ZERO;
			if(ordenDet.getPolizaAnterior() != null && resumenCostosDTO != null
					&& resumenCostosDTO.getPrimaTotal() != null){
				cell.setCellValue(resumenCostosDTO.getPrimaTotal().doubleValue());
				primaAnterior = new BigDecimal(resumenCostosDTO.getPrimaTotal());
			}else{
				cell.setCellValue("");
			}
			cell = row.createCell(OrdenRenovacionMasiva.INCREMENTO);
			if(ordenDet.getIdToCotizacion() != null){
				Double incremento = (primaActual.doubleValue() - primaAnterior.doubleValue())/ primaAnterior.doubleValue() * max;
				DecimalFormat df = new DecimalFormat("#.##");
				cell.setCellValue(df.format(incremento));
			}else{
				cell.setCellValue("");
			}
			cell = row.createCell(OrdenRenovacionMasiva.NOM_LINEA_NEG);
			if(ordenDet.getIdToCotizacion() != null && ordenDet.getCotizacion().getTipoPolizaDTO().getClaveAplicaFlotillas() == 0){				
				cell.setCellValue(renovacionMasivaService.getCamposExcel(incisoAutoCot, OrdenRenovacionMasiva.NOM_LINEA_NEG, coberturasCotinuityList) + "");
			}else{
				cell.setCellValue("");
			}
			cell = row.createCell(OrdenRenovacionMasiva.PAQUETE);
			if(ordenDet.getIdToCotizacion() != null && ordenDet.getCotizacion().getTipoPolizaDTO().getClaveAplicaFlotillas() == 0){
				cell.setCellValue(renovacionMasivaService.getCamposExcel(incisoAutoCot, OrdenRenovacionMasiva.PAQUETE, coberturasCotinuityList)+ "");
			}else{
				cell.setCellValue("");
			}
			
			
			cell = row.createCell(OrdenRenovacionMasiva.LINEA_ANTERIOR);
			if(ordenDet.getPolizaAnterior() != null && ordenDet.getPolizaAnterior().getCotizacionDTO().getTipoPolizaDTO().getClaveAplicaFlotillas() == 0 && incisoAutoCotAnt != null){
				cell.setCellValue(renovacionMasivaService.getCamposExcel(incisoAutoCotAnt, OrdenRenovacionMasiva.LINEA_ANTERIOR, coberturasCotinuityList)+ "");
			}else{
				cell.setCellValue("");
			}
			
			cell = row.createCell(OrdenRenovacionMasiva.ORIGEN);
			if(ordenDet.getIdToCotizacion() != null){
				Short tipoRiesgo = renovacionMasivaService.obtieneTipoRiesgo(ordenDet.getIdToCotizacion());
				if(tipoRiesgo.equals(NegocioRenovacion.TipoRiesgo.NORMAL.getObtenerTipo())){
					cell.setCellValue("NORMAL");
				}else if(tipoRiesgo.equals(NegocioRenovacion.TipoRiesgo.ALTORIESGO.getObtenerTipo())){
					cell.setCellValue("ALTO RIESGO");
				}else if(tipoRiesgo.equals(NegocioRenovacion.TipoRiesgo.ALTAFRECUENCIA.getObtenerTipo())){
					cell.setCellValue("ALTA FRECUENCIA");
				}else{
					cell.setCellValue("");
				}			
			}else{
				cell.setCellValue("");
			}
			cell = row.createCell(OrdenRenovacionMasiva.RENOVACION);
			//Comentario Integracion
			if(ordenDet.getPolizaAnterior() != null && ordenDet.getPolizaAnterior().getNumeroRenovacion() != null){
				cell.setCellValue((ordenDet.getPolizaAnterior().getNumeroRenovacion() + 1) + "");
			}else{
				cell.setCellValue("");
			}
			cell = row.createCell(OrdenRenovacionMasiva.NUM_SIN_PREVIO);
			if(ordenDet.getPolizaAnterior() != null){
				PolizaDTO polizaSin = renovacionMasivaService.getCantidadSiniestro(ordenDet.getPolizaAnterior().getIdToPoliza());
				if(polizaSin != null){
					cell.setCellValue(polizaSin.getNumeroSiniestro() + "");
				}else{
					cell.setCellValue("0");
				}
			}else{
				cell.setCellValue("");
			}
			cell = row.createCell(OrdenRenovacionMasiva.NUM_SIN_TOTAL);
			if(ordenDet.getPolizaAnterior() != null){
				int totalSiniestros = renovacionMasivaService.getCantidadSiniestroTotal(ordenDet.getPolizaAnterior().getIdToPoliza());
				cell.setCellValue(totalSiniestros + "");
			}else{
				cell.setCellValue("");
			}
			cell = row.createCell(OrdenRenovacionMasiva.DESCUENTO);
			if(ordenDet.getDescuentoNoSiniestro() != null){
				cell.setCellValue(ordenDet.getDescuentoNoSiniestro() + "");
			}else{
				cell.setCellValue("0");
			}
			cell = row.createCell(OrdenRenovacionMasiva.DESCUENTO_POR_ESTADO);
			if(ordenDet.getDescuentoNoSiniestro() != null){
				Double descuentoPorEstado = incisoAutoCot.getPctDescuentoEstado() != null ? incisoAutoCot.getPctDescuentoEstado() : 0.0;
				Double descuento = descuentoPorEstado - ordenDet.getDescuentoNoSiniestro();
				cell.setCellValue(descuento + "");
			}else{
				cell.setCellValue("0");
			}
			cell = row.createCell(OrdenRenovacionMasiva.ESTILO);
			if(ordenDet.getIdToCotizacion() != null && ordenDet.getCotizacion().getTipoPolizaDTO().getClaveAplicaFlotillas() == 0){
				cell.setCellValue(incisoAutoCot.getEstiloId()+ "");
			}else{
				cell.setCellValue("");
			}
			cell = row.createCell(OrdenRenovacionMasiva.DESC_VEHIC);
			if(ordenDet.getIdToCotizacion() != null && ordenDet.getCotizacion().getTipoPolizaDTO().getClaveAplicaFlotillas() == 0){
				cell.setCellValue(renovacionMasivaService.getCamposExcel(incisoAutoCot, OrdenRenovacionMasiva.DESC_VEHIC, coberturasCotinuityList)+ "");
			}else{
				cell.setCellValue("");
			}
			cell = row.createCell(OrdenRenovacionMasiva.MODELO);
			if(ordenDet.getIdToCotizacion() != null && ordenDet.getCotizacion().getTipoPolizaDTO().getClaveAplicaFlotillas() == 0){
				cell.setCellValue(incisoAutoCot.getModeloVehiculo()+ "");
			}else{
				cell.setCellValue("");
			}
			cell = row.createCell(OrdenRenovacionMasiva.ESTADO);
			if(ordenDet.getIdToCotizacion() != null && ordenDet.getCotizacion().getTipoPolizaDTO().getClaveAplicaFlotillas() == 0){
				cell.setCellValue(renovacionMasivaService.getCamposExcel(incisoAutoCot, OrdenRenovacionMasiva.ESTADO, coberturasCotinuityList)+ "");
			}else{
				cell.setCellValue("");
			}
			/*
			cell = row.createCell(OrdenRenovacionMasiva.CLASE);
			if(ordenDet.getIdToCotizacion() != null){
				cell.setCellValue("");
			}else{
				cell.setCellValue("");
			}*/
			cell = row.createCell(OrdenRenovacionMasiva.DEDUCIBLE_RT);
			if(ordenDet.getIdToCotizacion() != null && ordenDet.getCotizacion().getTipoPolizaDTO().getClaveAplicaFlotillas() == 0){
				cell.setCellValue(renovacionMasivaService.getCamposExcel(incisoAutoCot, OrdenRenovacionMasiva.DEDUCIBLE_RT, coberturasCotinuityList)+ "");
			}else{
				cell.setCellValue("");
			}
			cell = row.createCell(OrdenRenovacionMasiva.ESTATUS_ORD_RENOV);
			cell.setCellValue(orden.getDescripcionEstatus());
			
			cell = row.createCell(OrdenRenovacionMasiva.CLAVEAGENTE);
			if(ordenDet.getIdToCotizacion() != null){
				try{
					cell.setCellValue(renovacionMasivaService.getIdAgente(ordenDet.getCotizacion().getSolicitudDTO().getCodigoAgente().longValue()) + "");
				}catch(Exception e){
					LOG.error(e);
					cell.setCellValue("");
				}
			}else{
				cell.setCellValue("");
			}
			
			cell = row.createCell(OrdenRenovacionMasiva.NOMBRE_AGENTE);
			if(ordenDet.getIdToCotizacion() != null){
				cell.setCellValue(ordenDet.getCotizacion().getSolicitudDTO().getNombreAgente().toUpperCase() + "");
			}else{
				cell.setCellValue("");
			}
			
			cell = row.createCell(OrdenRenovacionMasiva.NOMBRE_NEGOCIO_POLIZA_ANTERIOR);
			if(ordenDet.getPolizaAnterior() != null){
				cell.setCellValue(ordenDet.getPolizaAnterior().getCotizacionDTO().getSolicitudDTO().getNegocio().getDescripcionNegocio().toUpperCase() + "");
			}else{
				cell.setCellValue("");
			}
			
			cell = row.createCell(OrdenRenovacionMasiva.NOMBRE_NEGOCIO_POLIZA_RENOVAR);
			if(ordenDet.getIdToCotizacion() != null){
				cell.setCellValue(ordenDet.getCotizacion().getSolicitudDTO().getNegocio().getDescripcionNegocio().toUpperCase() + "");
			}else{
				cell.setCellValue("");
			}
			
			//cell = row.createCell(OrdenRenovacionMasiva.DOMICILIO_CONTRATANTE);
			if(ordenDet.getIdToCotizacion() != null && ordenDet.getCotizacion().getIdDomicilioContratante() != null){
				DomicilioImpresion domicilio = renovacionMasivaService.obtieneDomicilioPorId(ordenDet.getCotizacion().getIdDomicilioContratante());
				/*String domicilioStr = "";
				if(domicilio != null){
					domicilioStr = domicilio.getCalleNumero() + " " + domicilio.getCiudad() + ", " + domicilio.getEstado() + " " + domicilio.getCodigoPostal();
				}
				cell.setCellValue(domicilioStr);
				*/
				if(domicilio != null){
				cell = row.createCell(OrdenRenovacionMasiva.DOMICILIO_CALLE_NUMERO);
				cell.setCellValue(domicilio.getCalleNumero());
				cell = row.createCell(OrdenRenovacionMasiva.DOMICILIO_COLONIA);
				cell.setCellValue(domicilio.getNombreColonia());
				cell = row.createCell(OrdenRenovacionMasiva.DOMICILIO_CODIGO_POSTAL);
				cell.setCellValue(domicilio.getCodigoPostal());
				cell = row.createCell(OrdenRenovacionMasiva.DOMICILIO_CIUDAD);
				cell.setCellValue(domicilio.getCiudad());
				cell = row.createCell(OrdenRenovacionMasiva.DOMICILIO_ESTADO);
				cell.setCellValue(domicilio.getEstado());
				}
			}else{
				if(ordenDet.getIdToCotizacion() != null && ordenDet.getCotizacion().getIdToPersonaContratante() != null){
					try{
						ClienteGenericoDTO cliente = getDatosCliente(ordenDet.getCotizacion().getIdToPersonaContratante());
						DomicilioImpresion domicilio = renovacionMasivaService.obtieneDomicilioPorId(new BigDecimal(cliente.getIdDomicilioConsulta()));
						/*String domicilioStr = "";
						if(domicilio != null){
							domicilioStr = domicilio.getCalleNumero() + " " + domicilio.getCiudad() + ", " + domicilio.getEstado() + " " + domicilio.getCodigoPostal();
						}
						cell.setCellValue(domicilioStr);
						*/
						if(domicilio != null){
						cell = row.createCell(OrdenRenovacionMasiva.DOMICILIO_CALLE_NUMERO);
						cell.setCellValue(domicilio.getCalleNumero());
						cell = row.createCell(OrdenRenovacionMasiva.DOMICILIO_COLONIA);
						cell.setCellValue(domicilio.getNombreColonia());
						cell = row.createCell(OrdenRenovacionMasiva.DOMICILIO_CODIGO_POSTAL);
						cell.setCellValue(domicilio.getCodigoPostal());
						cell = row.createCell(OrdenRenovacionMasiva.DOMICILIO_CIUDAD);
						cell.setCellValue(domicilio.getCiudad());
						cell = row.createCell(OrdenRenovacionMasiva.DOMICILIO_ESTADO);
						cell.setCellValue(domicilio.getEstado());
						}
					}catch(Exception e){
						
					}
				}else{
					//cell.setCellValue("");
					cell = row.createCell(OrdenRenovacionMasiva.DOMICILIO_CALLE_NUMERO);
					cell.setCellValue("");
					cell = row.createCell(OrdenRenovacionMasiva.DOMICILIO_COLONIA);
					cell.setCellValue("");
					cell = row.createCell(OrdenRenovacionMasiva.DOMICILIO_CODIGO_POSTAL);
					cell.setCellValue("");
					cell = row.createCell(OrdenRenovacionMasiva.DOMICILIO_CIUDAD);
					cell.setCellValue("");
					cell = row.createCell(OrdenRenovacionMasiva.DOMICILIO_ESTADO);
					cell.setCellValue("");
				}
			}
			
			cell = row.createCell(OrdenRenovacionMasiva.FORMA_PAGO);
			if(ordenDet.getIdToCotizacion() != null && ordenDet.getCotizacion().getIdFormaPago() != null){
				FormaPagoDTO formaPago = renovacionMasivaService.obtieneFormaPagoPorId(ordenDet.getCotizacion().getIdFormaPago());
				cell.setCellValue(formaPago.getDescripcion() + "");
			}else{
				cell.setCellValue("");
			}
			
			cell = row.createCell(OrdenRenovacionMasiva.CONDUCTO_COBRO);
			if(ordenDet.getIdToCotizacion() != null && ordenDet.getCotizacion().getIdMedioPago() != null){
				MedioPagoDTO medioPago = renovacionMasivaService.obtieneMedioPagoPorId(ordenDet.getCotizacion().getIdMedioPago());
				cell.setCellValue(medioPago.getDescripcion() + "");
			}else{
				cell.setCellValue("");
			}
			
			cell = row.createCell(OrdenRenovacionMasiva.NOMBRE_TITULAR_CONDUCTO_COBRO);
			if(ordenDet.getIdToCotizacion() != null && ordenDet.getCotizacion().getIdConductoCobroCliente() != null){
				try{
					CuentaPagoDTO cuentaPagoDTO = renovacionMasivaService.obtenerConductoCobro(ordenDet.getIdToCotizacion());
					if(cuentaPagoDTO != null){
						cell.setCellValue(cuentaPagoDTO.getTarjetaHabiente() + "");
					}
				}catch(Exception e){
				}
			}else{
				cell.setCellValue("");
			}
			
			cell = row.createCell(OrdenRenovacionMasiva.TELEFONO_CASA);
			if(ordenDet.getIdToCotizacion() != null && clienteContratante != null){
				cell.setCellValue(clienteContratante.getTelefono() + "");
			}else{
				cell.setCellValue("");
			}
			
			cell = row.createCell(OrdenRenovacionMasiva.TELEFONO_OFICINA);
			if(ordenDet.getIdToCotizacion() != null && clienteContratante != null){
				cell.setCellValue(clienteContratante.getTelefonoOficinaContacto() + "");
			}else{
				cell.setCellValue("");
			}
			
			cell = row.createCell(OrdenRenovacionMasiva.NUM_POLIZA_SEYCOS_INT);
			if(ordenDet.getPolizaAnterior() != null){
				String numeroPoliza = ordenDet.getPolizaAnterior().numeroPolizaMidasSeycos();
				cell.setCellValue(numeroPoliza);
			}else{
				cell.setCellValue("");
			}
			
			cell = row.createCell(OrdenRenovacionMasiva.DEDUCIBLE_DANOS_MATERIALES_INT_1);
			if(incisoAutoCotAnt != null){
				cell.setCellValue(renovacionMasivaService.getCamposExcel(incisoAutoCotAnt, OrdenRenovacionMasiva.DEDUCIBLE_DANOS_MATERIALES_BIT, coberturasCotinuityList)+ "");
			}else{
				cell.setCellValue("");
			}
			
			cell = row.createCell(OrdenRenovacionMasiva.DEDUCIBLE_DANOS_MATERIALES_INT_2);
			if(ordenDet.getIdToCotizacion() != null && ordenDet.getCotizacion().getTipoPolizaDTO().getClaveAplicaFlotillas() == 0){
				cell.setCellValue(renovacionMasivaService.getCamposExcel(incisoAutoCot, OrdenRenovacionMasiva.DEDUCIBLE_DANOS_MATERIALES, coberturasCotinuityList)+ "");
			}else{
				cell.setCellValue("");
			}
			
			cell = row.createCell(OrdenRenovacionMasiva.PORCENTAJE_IVA);
			if(ordenDet.getIdToCotizacion() != null){
				cell.setCellValue(ordenDet.getCotizacion().getPorcentajeIva()+ "");
			}else{
				cell.setCellValue("");
			}
			
			cell = row.createCell(OrdenRenovacionMasiva.TOTAL_PRIMAS);
			if(ordenDet.getIdToCotizacion() != null && resumenCostosRen != null){
				cell.setCellValue(resumenCostosRen.getPrimaNetaCoberturas()+ "");
			}else{
				cell.setCellValue("");
			}
			
			cell = row.createCell(OrdenRenovacionMasiva.DESCUENTO_COMIS_CEDIDA);
			if(ordenDet.getIdToCotizacion() != null && resumenCostosRen != null){
				cell.setCellValue(resumenCostosRen.getDescuentoComisionCedida()+ "");
			}else{
				cell.setCellValue("");
			}
			
			cell = row.createCell(OrdenRenovacionMasiva.PRIMA_NETA);
			if(ordenDet.getIdToCotizacion() != null && resumenCostosRen != null){
				cell.setCellValue(resumenCostosRen.getTotalPrimas()+ "");
			}else{
				cell.setCellValue("");
			}
			
			cell = row.createCell(OrdenRenovacionMasiva.RECARGO);
			if(ordenDet.getIdToCotizacion() != null && resumenCostosRen != null){
				cell.setCellValue(resumenCostosRen.getRecargo()+ "");
			}else{
				cell.setCellValue("");
			}
			
			cell = row.createCell(OrdenRenovacionMasiva.DERECHOS);
			if(ordenDet.getIdToCotizacion() != null && resumenCostosRen != null){
				cell.setCellValue(resumenCostosRen.getDerechos()+ "");
			}else{
				cell.setCellValue("");
			}
			
			cell = row.createCell(OrdenRenovacionMasiva.IVA);
			if(ordenDet.getIdToCotizacion() != null && resumenCostosRen != null){
				cell.setCellValue(resumenCostosRen.getIva()+ "");
			}else{
				cell.setCellValue("");
			}
			
			cell = row.createCell(OrdenRenovacionMasiva.ENVIADO);
			if(orden != null){
				if(orden.getEnviado() != null && orden.getEnviado().intValue() == 1){
					cell.setCellValue("ENVIADO");
				}else{
					cell.setCellValue("PENDIENTE");
				}
			}
			}catch(Exception e){
				e.printStackTrace();
			}
			dataRows++;
		}
	}
	
	private void llenaDatosExcel(HSSFSheet cotizaciones, BigDecimal idToOrdenRenovacion){
		llenaEncabezadosExcelControl(cotizaciones);
		llenaDatosExcelControl(cotizaciones, idToOrdenRenovacion);
	}
	
	private void llenaDatosExcelControl(HSSFSheet sheet, BigDecimal idToOrdenRenovacion){
		List<OrdenRenovacionMasivaDet> ordenesDetalle = getDetalleOrdenRenovacionMasiva(idToOrdenRenovacion);
		this.dataRows = 1;
		for(OrdenRenovacionMasivaDet ordenDet : ordenesDetalle){
			if(ordenDet != null && ordenDet.getClaveEstatus() != null && ordenDet.getClaveEstatus().equals(OrdenRenovacionMasivaDet.ESTATUS_TERMINADA)){
				try{
					BigDecimal idToCotizacion = ordenDet.getIdToCotizacion();
					boolean aplicaFlotillas = ordenDet.getCotizacion().getTipoPolizaDTO().getClaveAplicaFlotillas() == 0;
					IncisoAutoCot incisoAutoCot = getIncisoAutoCotizacion(idToCotizacion, 
							aplicaFlotillas);
					ClienteGenericoDTO cliente = getDatosCliente(ordenDet.getCotizacion().getIdToPersonaContratante());
					PolizaDTO polzaAnterior = ordenDet.getPolizaAnterior();
					PolizaDTO polizaRenovada = ordenDet.getPolizaRenovada();
					
					HSSFRow row = sheet.createRow(dataRows);
					HSSFCell cell = row.createCell(OrdenRenovacionMasiva.NUM_POLIZA_SEYCOS);				
					cell.setCellValue(polzaAnterior != null?polzaAnterior.numeroPolizaMidasSeycos():"");
			
					cell = row.createCell(OrdenRenovacionMasiva.NUM_POLIZA_MIDAS);
					cell.setCellValue(polzaAnterior != null?polzaAnterior.getNumeroPolizaFormateada():"");
			
					cell = row.createCell(OrdenRenovacionMasiva.NUM_POLIZA_SEYCOS_NUEVA);
					cell.setCellValue(polizaRenovada!=null?polizaRenovada.numeroPolizaMidasSeycos():"");			
			
					cell = row.createCell(OrdenRenovacionMasiva.NUM_POLIZA_MIDAS_NUEVA);
					cell.setCellValue(polizaRenovada!=null?polizaRenovada.getNumeroPolizaFormateada():"");
			
					cell = row.createCell(OrdenRenovacionMasiva.CONTRATANTE);
					cell.setCellValue(idToCotizacion!=null?ordenDet.getCotizacion().getNombreContratante():"");
			
					cell = row.createCell(OrdenRenovacionMasiva.TITULAR);
					cell.setCellValue(incisoAutoCot!=null?incisoAutoCot.getNombreAsegurado():"");			
			
					String domicilioTx = "";
					DomicilioImpresion domicilio = getDomicilioContratante(idToCotizacion, ordenDet.getCotizacion().getIdDomicilioContratante(), cliente);
					
					if(domicilio != null){
							cell = row.createCell(OrdenRenovacionMasiva.CALLE_NUMERO);
							cell.setCellValue(domicilio.getCalleNumero());
							cell = row.createCell(OrdenRenovacionMasiva.COLONIA);
							cell.setCellValue(domicilio.getNombreColonia());
							cell = row.createCell(OrdenRenovacionMasiva.CODIGO_POSTAL);
							cell.setCellValue(domicilio.getCodigoPostal());
							cell = row.createCell(OrdenRenovacionMasiva.CIUDAD_P);
							cell.setCellValue(domicilio.getCiudad());
							cell = row.createCell(OrdenRenovacionMasiva.ESTADO_P);
							cell.setCellValue(domicilio.getEstado());
												
							domicilioTx = getDomicilioTX(domicilio);
					}else{
						cell = row.createCell(OrdenRenovacionMasiva.CALLE_NUMERO);
						cell.setCellValue("");
						cell = row.createCell(OrdenRenovacionMasiva.COLONIA);
						cell.setCellValue("");
						cell = row.createCell(OrdenRenovacionMasiva.CODIGO_POSTAL);
						cell.setCellValue("");
						cell = row.createCell(OrdenRenovacionMasiva.CIUDAD_P);
						cell.setCellValue("");
						cell = row.createCell(OrdenRenovacionMasiva.ESTADO_P);
						cell.setCellValue("");
					}			

					String telefonoCliente = getTelefonoCliente(cliente);
					cell = row.createCell(OrdenRenovacionMasiva.TELEFONO_P);
					cell.setCellValue(telefonoCliente);
			
					String desc_vehiculo = getDatoExel(incisoAutoCot, OrdenRenovacionMasiva.DESC_VEHIC);
					cell = row.createCell(OrdenRenovacionMasiva.DESCRIPCION_VEHICULO);
					cell.setCellValue(desc_vehiculo);
					
					cell = row.createCell(OrdenRenovacionMasiva.MODELO_VEHICULO);
					cell.setCellValue(incisoAutoCot != null?incisoAutoCot.getModeloVehiculo()+ "":"");
			
					cell = row.createCell(OrdenRenovacionMasiva.SERIE);
					cell.setCellValue(incisoAutoCot != null?incisoAutoCot.getNumeroSerie()+ "":"");
					
					cell = row.createCell(OrdenRenovacionMasiva.PRIMA_TOTAL_POLIZA);
					if(ordenDet.getCotizacion() != null && ordenDet.getCotizacion().getValorPrimaTotal() != null){
						cell.setCellValue(ordenDet.getCotizacion().getValorPrimaTotal().doubleValue());
					}else{
						cell.setCellValue("");
					}
					
					String deducibleDM = getDatoExel(incisoAutoCot, 
							OrdenRenovacionMasiva.DEDUCIBLE_DANOS_MATERIALES);
					cell = row.createCell(OrdenRenovacionMasiva.DEDUCIBLE_DANOS_MATERIALES);
					cell.setCellValue(deducibleDM);
					
					String montoPrima = getMontoPrima(idToCotizacion, ordenDet.getCotizacion());					
					cell = row.createCell(OrdenRenovacionMasiva.IMPORTE_PRIMER_RECIBO);
					cell.setCellValue(montoPrima);
					
					cell = row.createCell(OrdenRenovacionMasiva.DOMICILIO_TX);
					cell.setCellValue(domicilioTx);
			
					String metodoPagoDesc = getDescripcionMetodoPago(idToCotizacion,
							ordenDet.getCotizacion().getIdMedioPago());
					cell = row.createCell(OrdenRenovacionMasiva.CONDUCTO_COBRO_P);
					cell.setCellValue(metodoPagoDesc + "");	
					
					BitacoraGuia bitacora = getBitacoraGuia(polizaRenovada.getIdToPoliza()); 
					DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
					cell = row.createCell(OrdenRenovacionMasiva.FECHA_DE_ENVIO);
					cell.setCellValue(dateFormat.format(bitacora.getFecha())+"");

					cell = row.createCell(OrdenRenovacionMasiva.ID_GUIA);
					cell.setCellValue(bitacora.getIdGuia()+"");

					cell = row.createCell(OrdenRenovacionMasiva.ESTATUS_GUIA);
					cell.setCellValue(getEstatusBitacora(bitacora.getEstatus()));

					cell = row.createCell(OrdenRenovacionMasiva.FECHA_DE_ENTREGA);
					cell.setCellValue(dateFormat.format(bitacora.getFechaEntrega())+"");

					cell = row.createCell(OrdenRenovacionMasiva.RECIBE_GUIA);
					cell.setCellValue(bitacora.getRecibe());

					cell = row.createCell(OrdenRenovacionMasiva.RECHAZO_GUIA);
					cell.setCellValue(bitacora.getMotivoRechazo());
			
				}catch(Exception e){
					LOG.error(e);
				}
				dataRows++;
			}
		}
	}

	private String getEstatusBitacora(Integer estatus) {
		return trackingService.getEstatusBitacora(estatus);
	}

	private void llenaEncabezados(HSSFSheet sheet, int tipo){
		HSSFRow row = sheet.createRow(0);
		HSSFCell cell = row.createCell(OrdenRenovacionMasiva.SIT_POLIZA);
		cell.setCellValue("SIT_POLIZA");
		cell = row.createCell(OrdenRenovacionMasiva.ID_COTIZACION);
		cell.setCellValue("ID_COTIZACION");
		cell = row.createCell(OrdenRenovacionMasiva.ID_VERSION_POL);
		cell.setCellValue("ID_VERSION_POL");
		cell = row.createCell(OrdenRenovacionMasiva.ID_CONTRATANTE);
		cell.setCellValue("ID_CONTRATANTE");
		cell = row.createCell(OrdenRenovacionMasiva.ID_PRODUCTO);
		cell.setCellValue("ID_PRODUCTO");
		cell = row.createCell(OrdenRenovacionMasiva.CVE_T_POLIZA);
		cell.setCellValue("CVE_T_POLIZA");
		cell = row.createCell(OrdenRenovacionMasiva.F_INI_VIGENCIA);
		cell.setCellValue("F_INI_VIGENCIA");
		cell = row.createCell(OrdenRenovacionMasiva.F_FIN_VIGENCIA);
		cell.setCellValue("F_FIN_VIGENCIA");
		cell = row.createCell(OrdenRenovacionMasiva.NUM_COTIZACION);
		cell.setCellValue("NUM_COTIZACION");
		cell = row.createCell(OrdenRenovacionMasiva.NUM_POLIZA);
		cell.setCellValue("NUM_POLIZA");
		cell = row.createCell(OrdenRenovacionMasiva.NUM_POLIZA_ANT);
		cell.setCellValue("NUM_POLIZA_ANT");
		cell = row.createCell(OrdenRenovacionMasiva.ID_USUARIO_EMIS);
		cell.setCellValue("ID_USUARIO_EMIS");
		cell = row.createCell(OrdenRenovacionMasiva.NOM_SOLICITANTE);
		cell.setCellValue("NOM_SOLICITANTE");
		cell = row.createCell(OrdenRenovacionMasiva.ID_CENTRO_EMISRG);
		cell.setCellValue("ID_CENTRO_EMISRG");
		cell = row.createCell(OrdenRenovacionMasiva.ID_EMPRESA);
		cell.setCellValue("ID_EMPRESA");
		//cell = row.createCell(OrdenRenovacionMasiva.ID_CENTRO_EMIS);
		//cell.setCellValue("ID_CENTRO_EMIS");
		cell = row.createCell(OrdenRenovacionMasiva.ID_USUARIO_CREAC);
		cell.setCellValue("ID_USUARIO_CREAC");
		cell = row.createCell(OrdenRenovacionMasiva.ID_BITACORA);
		cell.setCellValue("ID_BITACORA");
		cell = row.createCell(OrdenRenovacionMasiva.ID_MONEDA);
		cell.setCellValue("ID_MONEDA");
		cell = row.createCell(OrdenRenovacionMasiva.CVE_T_POLIZA_1);
		cell.setCellValue("CVE_T_POLIZA_1");
		cell = row.createCell(OrdenRenovacionMasiva.ID_USUARIO_AUTOR);
		cell.setCellValue("ID_USUARIO_AUTOR");
		cell = row.createCell(OrdenRenovacionMasiva.IMP_PRIMA_TOTAL);
		cell.setCellValue("IMP_PRIMA_TOTAL");
		cell = row.createCell(OrdenRenovacionMasiva.FH_CREACION);
		cell.setCellValue("FH_CREACION");
		cell = row.createCell(OrdenRenovacionMasiva.IMP_PRIMA_ANT);
		cell.setCellValue("IMP_PRIMA_ANT");
		cell = row.createCell(OrdenRenovacionMasiva.INCREMENTO);
		cell.setCellValue("INCREMENTO");
		cell = row.createCell(OrdenRenovacionMasiva.NOM_LINEA_NEG);
		cell.setCellValue("NOM_LINEA_NEG");
		cell = row.createCell(OrdenRenovacionMasiva.PAQUETE);
		cell.setCellValue("PAQUETE");
		cell = row.createCell(OrdenRenovacionMasiva.LINEA_ANTERIOR);
		cell.setCellValue("LINEA_ANTERIOR");
		cell = row.createCell(OrdenRenovacionMasiva.ORIGEN);
		cell.setCellValue("CLASE");
		cell = row.createCell(OrdenRenovacionMasiva.RENOVACION);
		cell.setCellValue("RENOVACION");
		cell = row.createCell(OrdenRenovacionMasiva.NUM_SIN_PREVIO);
		cell.setCellValue("NUM_SIN_PREVIO");
		cell = row.createCell(OrdenRenovacionMasiva.NUM_SIN_TOTAL);
		cell.setCellValue("NUM_SIN_TOTAL");
		cell = row.createCell(OrdenRenovacionMasiva.DESCUENTO);
		cell.setCellValue("DESCUENTO");
		cell = row.createCell(OrdenRenovacionMasiva.DESCUENTO_POR_ESTADO);
		cell.setCellValue("DESCUENTO_POR_ESTADO");
		cell = row.createCell(OrdenRenovacionMasiva.ESTILO);
		cell.setCellValue("ESTILO");
		cell = row.createCell(OrdenRenovacionMasiva.DESC_VEHIC);
		cell.setCellValue("DESC_VEHIC");
		cell = row.createCell(OrdenRenovacionMasiva.MODELO);
		cell.setCellValue("MODELO");
		cell = row.createCell(OrdenRenovacionMasiva.ESTADO);
		cell.setCellValue("ESTADO");
		//cell = row.createCell(OrdenRenovacionMasiva.CLASE);
		//cell.setCellValue("CLASE");
		cell = row.createCell(OrdenRenovacionMasiva.DEDUCIBLE_RT);
		cell.setCellValue("DEDUCIBLE_RT");
		cell = row.createCell(OrdenRenovacionMasiva.ESTATUS_ORD_RENOV);
		cell.setCellValue("ESTATUS_ORD_RENOV");
		
		cell = row.createCell(OrdenRenovacionMasiva.CLAVEAGENTE);
		cell.setCellValue("CLAVEAGENTE");
		cell = row.createCell(OrdenRenovacionMasiva.NOMBRE_AGENTE);
		cell.setCellValue("NOMBRE_AGENTE");
		cell = row.createCell(OrdenRenovacionMasiva.NOMBRE_NEGOCIO_POLIZA_ANTERIOR);
		cell.setCellValue("NOMBRE_NEGOCIO_POLIZA_ANTERIOR");
		cell = row.createCell(OrdenRenovacionMasiva.NOMBRE_NEGOCIO_POLIZA_RENOVAR);
		cell.setCellValue("NOMBRE_NEGOCIO_POLIZA_RENOVAR");
		//cell = row.createCell(OrdenRenovacionMasiva.DOMICILIO_CONTRATANTE);
		//cell.setCellValue("DOMICILIO_CONTRATANTE");
		cell = row.createCell(OrdenRenovacionMasiva.DOMICILIO_CALLE_NUMERO);
		cell.setCellValue("CALLE_NUMERO_CONTRATANTE");
		cell = row.createCell(OrdenRenovacionMasiva.DOMICILIO_COLONIA);
		cell.setCellValue("COLONIA_CONTRATANTE");
		cell = row.createCell(OrdenRenovacionMasiva.DOMICILIO_CODIGO_POSTAL);
		cell.setCellValue("CP_CONTRATANTE");
		cell = row.createCell(OrdenRenovacionMasiva.DOMICILIO_CIUDAD);
		cell.setCellValue("CIUDAD_CONTRATANTE");
		cell = row.createCell(OrdenRenovacionMasiva.DOMICILIO_ESTADO);
		cell.setCellValue("ESTADO_CONTRATANTE");
		cell = row.createCell(OrdenRenovacionMasiva.FORMA_PAGO);
		cell.setCellValue("FORMA_PAGO");
		cell = row.createCell(OrdenRenovacionMasiva.CONDUCTO_COBRO);
		cell.setCellValue("CONDUCTO_COBRO");
		cell = row.createCell(OrdenRenovacionMasiva.NOMBRE_TITULAR_CONDUCTO_COBRO);
		cell.setCellValue("NOMBRE_TITULAR_CONDUCTO_COBRO");
		cell = row.createCell(OrdenRenovacionMasiva.TELEFONO_CASA);
		cell.setCellValue("TELEFONO_CASA");
		cell = row.createCell(OrdenRenovacionMasiva.TELEFONO_OFICINA);
		cell.setCellValue("TELEFONO_OFICINA");
		cell = row.createCell(OrdenRenovacionMasiva.NUM_POLIZA_SEYCOS_INT);
		cell.setCellValue("NUM_POLIZA_SEYCOS_INT");
		cell = row.createCell(OrdenRenovacionMasiva.DEDUCIBLE_DANOS_MATERIALES_INT_1);
		cell.setCellValue("DEDUCIBLE_DM_ANTERIOR");
		cell = row.createCell(OrdenRenovacionMasiva.DEDUCIBLE_DANOS_MATERIALES_INT_2);
		cell.setCellValue("DEDUCIBLE_DM_RENOVAR");
		cell = row.createCell(OrdenRenovacionMasiva.PORCENTAJE_IVA);
		cell.setCellValue("PORCENTAJE_IVA");
		///
		cell = row.createCell(OrdenRenovacionMasiva.TOTAL_PRIMAS);
		cell.setCellValue("TOTAL_PRIMAS");
		cell = row.createCell(OrdenRenovacionMasiva.DESCUENTO_COMIS_CEDIDA);
		cell.setCellValue("DESCUENTO");
		cell = row.createCell(OrdenRenovacionMasiva.PRIMA_NETA);
		cell.setCellValue("PRIMA_NETA");
		cell = row.createCell(OrdenRenovacionMasiva.RECARGO);
		cell.setCellValue("FINANCIAMIENTO");
		cell = row.createCell(OrdenRenovacionMasiva.DERECHOS);
		cell.setCellValue("GASTOS_EXPEDICION");
		cell = row.createCell(OrdenRenovacionMasiva.IVA);
		cell.setCellValue("I_V_A");
		//
		cell = row.createCell(OrdenRenovacionMasiva.ENVIADO);
		cell.setCellValue("ENVIADO");	
		
		if(tipo == GeneraExcelRenovacionMasiva.TIPO_EXCEL_PROVEEDOR){
			cell = row.createCell(OrdenRenovacionMasiva.GUIA);
			cell.setCellValue("GUIA");	
			cell = row.createCell(OrdenRenovacionMasiva.DESTATUS);
			cell.setCellValue("DESTATUS");
			cell = row.createCell(OrdenRenovacionMasiva.FECHA_ESTATUS);
			cell.setCellValue("FECHA_ESTATUS");
			cell = row.createCell(OrdenRenovacionMasiva.FECHA_ENTREGA);
			cell.setCellValue("FECHA_ENTREGA");
			cell = row.createCell(OrdenRenovacionMasiva.PERSONA_RECIBE);
			cell.setCellValue("PERSONA_RECIBE");
			cell = row.createCell(OrdenRenovacionMasiva.DESC_NO_ENTREGA);
			cell.setCellValue("DESC_NO_ENTREGA");
			cell = row.createCell(OrdenRenovacionMasiva.CONTRATO);
			cell.setCellValue("CONTRATO");
			cell = row.createCell(OrdenRenovacionMasiva.SIT_CONTRATO);
			cell.setCellValue("SIT_CONTRATO");
			cell = row.createCell(OrdenRenovacionMasiva.FECHA_INI_CONTRATO);
			cell.setCellValue("FECHA_INI_CONTRATO");
			cell = row.createCell(OrdenRenovacionMasiva.FECHA_FIN_CONTRATO);
			cell.setCellValue("FECHA_FIN_CONTRATO");
		}
	}
	
	private void llenaEncabezadosExcelControl(HSSFSheet sheet){
		HSSFRow row = sheet.createRow(0);
		HSSFCell cell = row.createCell(OrdenRenovacionMasiva.NUM_POLIZA_SEYCOS);
		cell.setCellValue("NUMERO POLIZA SEYCOS");
		cell = row.createCell(OrdenRenovacionMasiva.NUM_POLIZA_MIDAS);
		cell.setCellValue("NUMERO POLIZA MIDAS");
		cell = row.createCell(OrdenRenovacionMasiva.NUM_POLIZA_SEYCOS_NUEVA);
		cell.setCellValue("NUMERO POLIZA SEYCOS RENOVADA");
		cell = row.createCell(OrdenRenovacionMasiva.NUM_POLIZA_MIDAS_NUEVA);
		cell.setCellValue("NUMERO POLIZA MIDAS RENOVADA");
		cell = row.createCell(OrdenRenovacionMasiva.CONTRATANTE);
		cell.setCellValue("CONTRATANTE");
		cell = row.createCell(OrdenRenovacionMasiva.TITULAR);
		cell.setCellValue("TITULAR");
		cell = row.createCell(OrdenRenovacionMasiva.CALLE_NUMERO);
		cell.setCellValue("CALLE Y NUMERO");
		cell = row.createCell(OrdenRenovacionMasiva.COLONIA);
		cell.setCellValue("COLONIA");
		cell = row.createCell(OrdenRenovacionMasiva.CODIGO_POSTAL);
		cell.setCellValue("C.P.");
		cell = row.createCell(OrdenRenovacionMasiva.CIUDAD_P);
		cell.setCellValue("CIUDAD");
		cell = row.createCell(OrdenRenovacionMasiva.ESTADO_P);
		cell.setCellValue("ESTADO");
		cell = row.createCell(OrdenRenovacionMasiva.TELEFONO_P);
		cell.setCellValue("TELEFONO");
		cell = row.createCell(OrdenRenovacionMasiva.DESCRIPCION_VEHICULO);
		cell.setCellValue("DESCRIPCION VEHICULO");
		cell = row.createCell(OrdenRenovacionMasiva.MODELO_VEHICULO);
		cell.setCellValue("MODELO");
		cell = row.createCell(OrdenRenovacionMasiva.SERIE);
		cell.setCellValue("SERIE");
		cell = row.createCell(OrdenRenovacionMasiva.PRIMA_TOTAL_POLIZA);
		cell.setCellValue("PRIMA TOTAL DE  POLIZA");
		cell = row.createCell(OrdenRenovacionMasiva.DEDUCIBLE_DANOS_MATERIALES);
		cell.setCellValue("% DEDUCIBLE DA\u00D1OS MATERIALES");
		cell = row.createCell(OrdenRenovacionMasiva.IMPORTE_PRIMER_RECIBO);
		cell.setCellValue("IMPORTE PRIMER RECIBO");
		cell = row.createCell(OrdenRenovacionMasiva.DOMICILIO_TX);
		cell.setCellValue("DOMICILIO TX");
		cell = row.createCell(OrdenRenovacionMasiva.CONDUCTO_COBRO_P);
		cell.setCellValue("CONDUCTO COBRO");
		cell = row.createCell(OrdenRenovacionMasiva.FECHA_DE_ENVIO);
		cell.setCellValue("FECHA DE ENVIO");
		cell = row.createCell(OrdenRenovacionMasiva.ESTATUS_GUIA);
		cell.setCellValue("ESTATUS");
		cell = row.createCell(OrdenRenovacionMasiva.FECHA_DE_ENTREGA);
		cell.setCellValue("FECHA DE ENTREGA");
		cell = row.createCell(OrdenRenovacionMasiva.RECIBE_GUIA);
		cell.setCellValue("RECIBE");
		cell = row.createCell(OrdenRenovacionMasiva.RECHAZO_GUIA);
		cell.setCellValue("RECHAZO");
	}
	
	
	public InputStream obtieneXMLOrdenRenovacion(BigDecimal idToOrdenRenovacion, Locale locale){
		InputStream plantillaInputStream = null;
		
		List<OrdenRenovacionMasivaDet> ordenRenovacionMasivaDetList = getDetalleOrdenRenovacionMasiva(idToOrdenRenovacion);	
		List<byte[]> inputByteArray = new ArrayList<byte[]>(1);	
		ByteArrayOutputStream output = new ByteArrayOutputStream();	
		for(OrdenRenovacionMasivaDet orden : ordenRenovacionMasivaDetList){
			try{
				TransporteImpresionDTO transporte = getXMLOrdenRenovacion(orden, locale);
				if(transporte!=null){
					inputByteArray.add(transporte.getByteArray());	
				}
			}catch(Exception e){
				LOG.error(e);
			}
		}
		if(!inputByteArray.isEmpty()){
			pdfConcantenate(inputByteArray, output);
			plantillaInputStream = new ByteArrayInputStream(output.toByteArray());
		}
		return plantillaInputStream;
	}
	
	public List<ControlDescripcionArchivoDTO> obtieneXMLOrdenRenovacionIndividual(BigDecimal idToOrdenRenovacion, Locale locale){
		List<ControlDescripcionArchivoDTO> plantillaInputStreamList = new ArrayList<ControlDescripcionArchivoDTO>(1);
				
		List<OrdenRenovacionMasivaDet> ordenRenovacionMasivaDetList = getDetalleOrdenRenovacionMasiva(idToOrdenRenovacion);	
		for(OrdenRenovacionMasivaDet orden : ordenRenovacionMasivaDetList){
			try{
				TransporteImpresionDTO transporte = getXMLOrdenRenovacion(orden, locale);
				if(transporte != null && transporte.getByteArray() != null && transporte.getByteArray().length > 0){
					InputStream plantillaInputStream = new ByteArrayInputStream(transporte.getByteArray());
					ControlDescripcionArchivoDTO cda = new ControlDescripcionArchivoDTO();
					cda.setExtension("pdf");
					cda.setNombreArchivo(orden.getPolizaRenovada().getNumeroPolizaFormateada());
					cda.setInputStream(plantillaInputStream);
					plantillaInputStreamList.add(cda);
				}
			}catch(Exception e){
				LOG.error(e);
			}
		}
		return plantillaInputStreamList;
	}

	public InputStream obtieneExcelOrdenRenovacion(BigDecimal idToOrdenRenovacion, Locale locale){
		InputStream plantillaInputStream = null;
		List<OrdenRenovacionMasivaDet> ordenRenovacionMasivaDetList = getDetalleOrdenRenovacionMasiva(idToOrdenRenovacion);	
		List<byte[]> inputByteArray = new ArrayList<byte[]>(1);	
		ByteArrayOutputStream output = new ByteArrayOutputStream();	
		for(OrdenRenovacionMasivaDet orden : ordenRenovacionMasivaDetList){
			if(orden.getIdToCotizacion() != null && orden.getIdToPolizaRenovada() != null){
					TransporteImpresionDTO transporte = getXMLOrdenRenovacion(orden, locale);
					if(transporte!=null){
						inputByteArray.add(transporte.getByteArray());
					}
			}
		}
		if(!inputByteArray.isEmpty()){
			pdfConcantenate(inputByteArray, output);
			plantillaInputStream = new ByteArrayInputStream(output.toByteArray());
		}
		return plantillaInputStream;
	}


	public void  pdfConcantenate(List inputByteArray, OutputStream outputStream){
		try {
			int pageOffset = 0;
			int f = 0;
			ArrayList master = new ArrayList();
			Document document = null;
			PdfCopy  writer = null;
			// we create a reader for a certain document
			Iterator<byte[]> iterator = inputByteArray.iterator();
			while(iterator.hasNext()){
				byte[] data = (byte[])iterator.next();
				if(data == null || data.length == 0){
					f++;
					continue;
				}
				PdfReader reader = new PdfReader(data);
	            reader.consolidateNamedDestinations();
	            // we retrieve the total number of pages
	            int n = reader.getNumberOfPages();
	            List bookmarks = SimpleBookmark.getBookmark(reader);
	            if (bookmarks != null) {
	            	if (pageOffset != 0){
	            		SimpleBookmark.shiftPageNumbers(bookmarks, pageOffset, null);
	            	}
	                master.addAll(bookmarks);
	            }	            
	            pageOffset += n;	                
	            if (f == 0) {
	            	// step 1: creation of a document-object
	            	document = new Document(reader.getPageSizeWithRotation(1));
	            	// step 2: we create a writer that listens to the document
	            	writer = new PdfCopy(document, outputStream);
	            	// step 3: we open the document
	            	document.open();           
	            }	              
	            // step 4: we add content
	            PdfImportedPage page;
	            for (int i = 0; i < n; ) {
	            	++i;
	            	page = writer.getImportedPage(reader, i);
	            	writer.addPage(page);
	            }	  
	            PRAcroForm form = reader.getAcroForm();
	            if (form != null){
	            	writer.copyAcroForm(reader);
	            }
	            f++;
			}
			if (!master.isEmpty()){
				writer.setOutlines(master);
			}
			//step 5: we close the document
			if(document != null){
				document.close();
			}
		}catch(Exception e) {	    
			LOG.error(e);      
		}	      
	}

	
	private TransporteImpresionDTO getXMLOrdenRenovacion(
			OrdenRenovacionMasivaDet orden, Locale locale) {
		TransporteImpresionDTO transporte = null;
		if(orden.getIdToCotizacion() != null && orden.getIdToPolizaRenovada() != null){
			transporte = renovacionMasivaService.obtieneXMLOrdenRenovacion(orden, locale);
		}
		return 	transporte;
	}
	
	private String getDatoExel(IncisoAutoCot incisoAutoCot,
			int deducibleDanosMateriales) {
		String datoExcel = "";
		if(incisoAutoCot != null){
			datoExcel = renovacionMasivaService.getCamposExcel(incisoAutoCot, OrdenRenovacionMasiva.DEDUCIBLE_DANOS_MATERIALES, null)+"";
		}
		return datoExcel;
	}

	private String getMontoPrima(BigDecimal idToCotizacion,
			CotizacionDTO cotizacion) {
		String montoPrima = "";
		if(idToCotizacion!=null){
			montoPrima = renovacionMasivaService.obtenerMontoPrimerRecibo(cotizacion)+"";
		}
		return montoPrima;
	}

	private DomicilioImpresion getDomicilioContratante(BigDecimal idToCotizacion, 
			BigDecimal idDomicilioContratante, ClienteGenericoDTO cliente) {
		DomicilioImpresion domicilio = null;
		if(idToCotizacion != null && idDomicilioContratante != null){
			domicilio = renovacionMasivaService.obtieneDomicilioPorId(idDomicilioContratante);
		}else if(cliente != null){
			try{						
				domicilio = renovacionMasivaService.obtieneDomicilioPorId(
						new BigDecimal(cliente.getIdDomicilioConsulta()));
			}catch(Exception e){
				LOG.error(e);						
			}
		}
		return domicilio;
	}

	private String getDomicilioTX(DomicilioImpresion domicilio){
		String domicilioTx = "";
		domicilioTx = domicilio.getCalleNumero() + ", " + domicilio.getNombreColonia()+ 
		" (" + domicilio.getCodigoPostal() + "), " +
		domicilio.getCiudad() + ", " + domicilio.getEstado();

		try{
			PaisDTO pais = renovacionMasivaService.getPais(domicilio.getClavePais());
			domicilioTx += ", " +pais.getCountryName();
		}catch(Exception e){
			LOG.error(e);
		}
		
		return domicilioTx;
	}

	private IncisoAutoCot getIncisoAutoCotizacion(BigDecimal idToCotizacion, boolean aplicaFlotillas) {
		IncisoAutoCot incisoAutoCot = null;
		if(idToCotizacion != null && aplicaFlotillas){
			incisoAutoCot = renovacionMasivaService.regresaIncisoAutoCot(idToCotizacion);
		}
		return incisoAutoCot;
	}

	private ClienteGenericoDTO getDatosCliente(BigDecimal idToPersonaContratante) {
		ClienteGenericoDTO cliente = null;
		try{
			cliente = renovacionMasivaService.obtieneClienteContratantePorId(idToPersonaContratante);				
		}catch(Exception e){
			LOG.error(e);
		}
		return cliente;
	}
	
	private String getTelefonoCliente(ClienteGenericoDTO cliente) {
		String telefono = "";
		if(cliente!=null){
			if(cliente.getTelefono() != null){
				telefono = cliente.getTelefono();
			}else if(cliente.getTelefonoOficinaContacto() != null){
				telefono = cliente.getTelefonoOficinaContacto();
			}else if(cliente.getTelefonoFiscal() != null){
				telefono = cliente.getTelefonoFiscal();
			}else if(cliente.getTelefonoCobranza() != null){
				telefono = cliente.getTelefonoCobranza();
			}else if(cliente.getTelefonoCasaAviso() != null){
				telefono = cliente.getTelefonoCasaAviso();
			}else if(cliente.getTelefonoOficinaAviso() != null){
				telefono = cliente.getTelefonoOficinaAviso();
			}
		}
		return telefono;
	}

	private String getDescripcionMetodoPago(BigDecimal idToCotizacion, BigDecimal idMedioPago) {
		MedioPagoDTO medioPago = null;
		if(idToCotizacion != null && idMedioPago != null){
			medioPago = renovacionMasivaService.obtieneMedioPagoPorId(idMedioPago);
		}		
		return medioPago!=null?medioPago.getDescription():"";
	}
	
	private BitacoraGuia getBitacoraGuia(BigDecimal idToPoliza){
		BitacoraGuia bitacora = new BitacoraGuia();
		List<BitacoraGuia> bitacoras = trackingService.getBitacoraByIdToPoliza(idToPoliza);
		if(bitacoras!=null && bitacoras.size()>0){
			bitacora = bitacoras.get(0);
		}
		return bitacora;
	}
	
	private List<OrdenRenovacionMasivaDet> getDetalleOrdenRenovacionMasiva(
			BigDecimal idToOrdenRenovacion) {
		return renovacionMasivaService.listarOrdenRenovacionDetalleByOrdenRenovacion(idToOrdenRenovacion);
	}

	public void setRenovacionMasivaService(RenovacionMasivaService renovacionMasivaService) {
		this.renovacionMasivaService = renovacionMasivaService;
	}

	public RenovacionMasivaService getRenovacionMasivaService() {
		return renovacionMasivaService;
	}

	public TrackingService getTrackingService() {
		return trackingService;
	}


	public void setTrackingService(TrackingService trackingService) {
		this.trackingService = trackingService;
	}


	public void setFileName(String fileName) {
		this.fileName = fileName;
	}


	public String getFileName() {
		return fileName;
	}
}
