package mx.com.afirme.midas2.excels;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import mx.com.afirme.midas.catalogos.estilovehiculo.EstiloVehiculoDTO;
import mx.com.afirme.midas.catalogos.valorfijo.ValorFijoDN;
import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDTO;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.controlArchivo.ControlArchivoDN;
import mx.com.afirme.midas.solicitud.SolicitudDTO;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoDTO;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.BitemporalCotizacion;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.BitemporalInciso;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.auto.BitemporalAutoInciso;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion.cobertura.BitemporalCoberturaSeccion;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.NegocioSeccion;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.paquete.NegocioPaqueteSeccion;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.paquete.cobertura.deducibles.NegocioDeducibleCob;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.tipouso.NegocioTipoUso;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.cargamasiva.DetalleCargaMasivaAutoCot;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.inciso.configuracion.ConfiguracionDatoInciso;
import mx.com.afirme.midas2.dto.TipoAccionDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.cargamasiva.LogErroresCargaMasivaDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.configuracion.inciso.ControlDinamicoRiesgoDTO;
import mx.com.afirme.midas2.service.bitemporal.endoso.inciso.IncisoService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.cargaMasiva.CargaMasivaService;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.hssf.usermodel.DVConstraint;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFDataValidation;
import org.apache.poi.hssf.usermodel.HSSFName;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataValidationConstraint.ValidationType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.util.CellRangeAddressList;
import org.apache.poi.ss.util.CellReference;
import org.joda.time.DateTime;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.anasoft.os.daofusion.bitemporal.TimeUtils;

@Component
@Scope("prototype")
public class GeneraExcelCargaMasivaEndosoAltaInciso extends GenerarExcelBase {

	private int maxColumns = 27;
	private CotizacionDTO cotizacion;
	private BitemporalCotizacion bitemporalCotizacion;
	private DateTime validoEn;
	private List<DetalleCargaMasivaAutoCot> detalleCargaMasivaAutoCotList = new ArrayList<DetalleCargaMasivaAutoCot>(1);
	private boolean hasLogErrors = false;
	private DetalleCargaMasivaAutoCot ejemplo = new DetalleCargaMasivaAutoCot();

	// Campos en proceso
	private static final int CAMPO_LINEA_NEGOCIO = 0;
	private static final int CAMPO_TIPO_USO = 1;
	private static final int CAMPO_DESCRIPCION = 2;
	private static final int CAMPO_CLAVE_AMIS = 3;
	private static final int CAMPO_MODELO = 4;
	private static final int CAMPO_PAQUETE = 5;
	private static final int CAMPO_CODIGO_POSTAL = 6;
	private static final int CAMPO_DEDUCIBLE_DANOS_MATERIALES = 7;
	private static final int CAMPO_DEDUCIBLE_ROBO_TOTAL = 8;
	private static final int CAMPO_LIMITE_RC_TERCEROS = 9;
	private static final int CAMPO_LIMITE_GASTOS_MEDICOS = 10;
	private static final int CAMPO_ASISTENCIA_JURIDICA = 11;
	private static final int CAMPO_ASISTENCIA_EN_VIAJES = 12;
	private static final int CAMPO_EXENCION_DEDUCIBLE_DANOS = 13;
	private static final int CAMPO_LIMITE_ACCIDENTE_CONDUCTOR = 14;
	private static final int CAMPO_EXTENCION_RC = 15;
	private static final int CAMPO_LIMITE_ADAPTACION_CONVERSION = 16;
	private static final int CAMPO_LIMITE_EQUIPO_ESPECIAL = 17;
	private static final int CAMPO_DEDUCIBLE_EQUIPO_ESPECIAL = 18;
	private static final int CAMPO_EXENCION_DEDUCIBLE_ROBO = 19;
	private static final int CAMPO_RESPONSABILIDAD_CIVIL_USA = 20;
	private static final int CAMPO_IGUALACION = 21;
	private static final int CAMPO_NUMERO_REMOLQUES = 22;
	private static final int CAMPO_TIPO_CARGA = 23;
	private static final int CAMPO_LIMITE_MUERTE = 24;
	private static final int CAMPO_SUMA_ASEGURADA_DANOS_MATERIALES = 25;
	private static final int CAMPO_SUMA_ASEGURADA_ROBO_TOTAL = 26;
	private static final int CAMPO_DIAS_SALARIO_MINIMO = 27;
	
	private static final String NOMBRE_CAMPO_NUMERO_REMOLQUES = "NUMERO REMOLQUES";
	private static final String NOMBRE_CAMPO_TIPO_CARGA = "TIPO CARGA";
	
	public static final String NOMBRE_DEDUCIBLE_DANOS_MATERIALES = "DA\u00D1OS MATERIALES";
	public static final String NOMBRE_DEDUCIBLE_ROBO_TOTAL = "ROBO TOTAL";
	public static final String NOMBRE_LIMITE_RC_TERCEROS = "Responsabilidad Civil";
	public static final String NOMBRE_LIMITE_GASTOS_MEDICOS = "GASTOS M\u00C9DICOS";
	public static final String NOMBRE_ASISTENCIA_JURIDICA = "ASISTENCIA JUR\u00CDDICA";
	public static final String NOMBRE_ASISTENCIA_EN_VIAJES = "ASISTENCIA EN VIAJES Y VIAL KM";
	public static final String NOMBRE_EXENCION_DEDUCIBLE_DANOS = "EXENCI\u00D3N DE DEDUCIBLES DM";
	public static final String NOMBRE_LIMITE_ACCIDENTE_CONDUCTOR = "ACCIDENTES AUTOMOVIL\u00CDSTICOS AL CONDUCTOR";
	public static final String NOMBRE_EXTENCION_RC = "EXTENSI\u00D3N DE RESPONSABILIDAD CIVIL";
	public static final String NOMBRE_LIMITE_ADAPTACION_CONVERSION = "ADAPTACIONES Y CONVERSIONES";
	public static final String NOMBRE_LIMITE_EQUIPO_ESPECIAL = "EQUIPO ESPECIAL";
	public static final String NOMBRE_DEDUCIBLE_EQUIPO_ESPECIAL = "EQUIPO ESPECIAL";
	public static final String NOMBRE_EXENCION_DEDUCIBLE_ROBO = "EXENCI\u00D3N DE DEDUCIBLES RT";
	public static final String NOMBRE_RESPONSABILIDAD_CIVIL_USA = "Responsabilidad Civil en USA Y CANADA LUC";
	public static final String NOMBRE_LIMITE_RC_VIAJERO = "Responsabilidad Civil Viajero";
	public static final String NOMBRE_LIMITE_MUERTE = "RESPONSABILIDAD CIVIL EN EXCESO POR MUERTE";
	private static final String PLANTILLA = "/mx/com/afirme/midas2/impresiones/jrxml/plantilla.xls";
	public static final String VALOR_SI = "SI";
	public static final String VALOR_NO = "NO";
	
	/**
	 * El nombre de la lista de valores utilizado par las validaciones del campo TIPO CARGA.
	 */
	private static final String CAMPO_TIPO_CARGA_NOMBRE_RANGO = "TP_TIPO_CARGA";
	
	
	private IncisoService incisoServiceBitemporal;
	
	public void setIncisoServiceBitemporal(IncisoService incisoServiceBitemporal){
		this.incisoServiceBitemporal = incisoServiceBitemporal;
	}

	@SuppressWarnings("deprecation")
	public GeneraExcelCargaMasivaEndosoAltaInciso(CargaMasivaService cargaMasivaService) {
		
		this.setCargaMasivaService(cargaMasivaService);

		String OSName = System.getProperty("os.name");
		if (OSName.toLowerCase().indexOf("windows")!=-1)
			this.uploadFolder = Sistema.UPLOAD_FOLDER;
		else
			this.uploadFolder = Sistema.LINUX_UPLOAD_FOLDER;
	}

	public InputStream generaPlantillaCargaMasiva(CotizacionDTO cotizacion)
			throws IOException {
		HSSFWorkbook workbook = null;
		HSSFSheet validaciones = null;
		HSSFSheet datos = null;

		ByteArrayOutputStream bos = null;

		this.cotizacion = cotizacion;

		try {
			// Obtiene plantilla con macros
			workbook = this.cargaPlantilla(PLANTILLA);
			validaciones = workbook.getSheetAt(0);
			datos = workbook.getSheetAt(1);
			
			buildDataSheet(datos);
			datos.protectSheet("midas");

			buildValidationsSheet(validaciones);

			// Ajusta tamano de columnas
			for (int i = 0; i <= maxColumns; i++) {
				if (i < 6) {
					validaciones.setColumnWidth(i, 9000);
				} else {
					validaciones.autoSizeColumn(i);
				}
			}

			// Oculta hoja de datos
			workbook.setSheetHidden(1, true);
			// Muestra primera hoja
			workbook.setActiveSheet(0);
			short firstCell = 0;
			validaciones.showInPane(firstCell, firstCell);
			
			bos = new ByteArrayOutputStream();

			workbook.write(bos);

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (bos != null) {
				bos.close();
			}
		}

		if(bos == null){
			return null;
		}
		return new ByteArrayInputStream(bos.toByteArray());
	}


	@Override
	protected void buildValidationsSheet(HSSFSheet sheet) {

		HSSFRow row = sheet.createRow(0);
		HSSFCell cell = row.createCell(CAMPO_LINEA_NEGOCIO);
		cell.setCellValue("LINEA DE NEGOCIO");
		cell = row.createCell(CAMPO_TIPO_USO);
		cell.setCellValue("TIPO USO");
		cell = row.createCell(CAMPO_DESCRIPCION);
		cell.setCellValue("DESCRIPCION");
		cell = row.createCell(CAMPO_CLAVE_AMIS);
		cell.setCellValue("CLAVE AMIS");
		cell = row.createCell(CAMPO_MODELO);
		cell.setCellValue("MODELO");
		cell = row.createCell(CAMPO_PAQUETE);
		cell.setCellValue("PAQUETE");
		cell = row.createCell(CAMPO_CODIGO_POSTAL);
		cell.setCellValue("CODIGO POSTAL");
		cell = row.createCell(CAMPO_DEDUCIBLE_DANOS_MATERIALES);
		cell.setCellValue("DEDUCIBLE DA\u00D1OS MATERIALES (%)");
		cell = row.createCell(CAMPO_DEDUCIBLE_ROBO_TOTAL);
		cell.setCellValue("DEDUCIBLE ROBO TOTAL (%)");
		cell = row.createCell(CAMPO_LIMITE_RC_TERCEROS);
		cell.setCellValue("L\u00CDMITE RC TERCEROS ($)");
		cell = row.createCell(CAMPO_LIMITE_GASTOS_MEDICOS);
		cell.setCellValue("L\u00CDMITE GASTOS MEDICOS ($)");
		cell = row.createCell(CAMPO_ASISTENCIA_JURIDICA);
		cell.setCellValue("ASISTENCIA JURIDICA (SI \u00D3 NO)");
		cell = row.createCell(CAMPO_ASISTENCIA_EN_VIAJES);
		cell.setCellValue("ASISTENCIA EN VIAJES Y VIAL KM 0 (SI \u00D3 NO)");
		cell = row.createCell(CAMPO_EXENCION_DEDUCIBLE_DANOS);
		cell.setCellValue("EXENCI\u00D3N DEDUCIBLE DA\u00D1OS MATERIALES (SI \u00D3 NO)");
		cell = row.createCell(CAMPO_LIMITE_ACCIDENTE_CONDUCTOR);
		cell.setCellValue("L\u00CDMITE ACCIDENTE CONDUCTOR ($)");
		cell = row.createCell(CAMPO_EXTENCION_RC);
		cell.setCellValue("EXTENCI\u00D3N RC (SI \u00D3 NO)");
		cell = row.createCell(CAMPO_LIMITE_ADAPTACION_CONVERSION);
		cell.setCellValue("L\u00CDMITE ADAPTACI\u00D3N Y CONVERSI\u00D3N  ($)");
		cell = row.createCell(CAMPO_LIMITE_EQUIPO_ESPECIAL);
		cell.setCellValue("L\u00CDMITE EQUIPO ESPECIAL ($)");
		cell = row.createCell(CAMPO_DEDUCIBLE_EQUIPO_ESPECIAL);
		cell.setCellValue("DEDUCIBLE EQUIPO ESPECIAL  (%)");
		cell = row.createCell(CAMPO_EXENCION_DEDUCIBLE_ROBO);
		cell.setCellValue("EXENCI\u00D3N DEDUCIBLE ROBO TOTAL (SI \u00D3 NO)");
		cell = row.createCell(CAMPO_RESPONSABILIDAD_CIVIL_USA);
		cell.setCellValue("RESPONSABILIDAD CIVIL USA Y CANADA (SI \u00D3 NO)");
		cell = row.createCell(CAMPO_IGUALACION);
		cell.setCellValue("IGUALACION");
		cell = row.createCell(CAMPO_NUMERO_REMOLQUES);
		cell.setCellValue(NOMBRE_CAMPO_NUMERO_REMOLQUES);
		cell = row.createCell(CAMPO_TIPO_CARGA);
		cell.setCellValue(NOMBRE_CAMPO_TIPO_CARGA);
		cell = row.createCell(CAMPO_LIMITE_MUERTE);
		cell.setCellValue("LIMITE MUERTE");
		cell = row.createCell(CAMPO_SUMA_ASEGURADA_DANOS_MATERIALES);
		cell.setCellValue("SUMA ASEGURADA DA\u00D1OS MATERIALES");
		cell = row.createCell(CAMPO_SUMA_ASEGURADA_ROBO_TOTAL);
		cell.setCellValue("SUMA ASEGURADA ROBO TOTAL");
		cell = row.createCell(CAMPO_DIAS_SALARIO_MINIMO);
		cell.setCellValue("DIAS SALARIO MINIMO");		
		
		
		llenaLineaEjemplo(sheet);

		DVConstraint dvConstraint = DVConstraint
				.createFormulaListConstraint("NEGOCIOSECCION");
		HSSFDataValidation data_validation = new HSSFDataValidation(
				new CellRangeAddressList(1, maxRows, CAMPO_LINEA_NEGOCIO,
						CAMPO_LINEA_NEGOCIO), dvConstraint);
		data_validation.setSuppressDropDownArrow(false);
		sheet.addValidationData(data_validation);

		dvConstraint = DVConstraint.createNumericConstraint(
				ValidationType.INTEGER, DVConstraint.OperatorType.GREATER_THAN,
				"0", "0");
		data_validation = new HSSFDataValidation(new CellRangeAddressList(1,
				maxRows, CAMPO_MODELO, CAMPO_MODELO), dvConstraint);
		sheet.addValidationData(data_validation);

		DVConstraint dvConstraintDecimal = DVConstraint
				.createNumericConstraint(ValidationType.DECIMAL,
						DVConstraint.OperatorType.GREATER_THAN, "0", "0");
		
		data_validation = new HSSFDataValidation(new CellRangeAddressList(1,
				maxRows, CAMPO_DEDUCIBLE_DANOS_MATERIALES,
				CAMPO_LIMITE_GASTOS_MEDICOS), dvConstraintDecimal);
		sheet.addValidationData(data_validation);

		DVConstraint dvConstraintSINO = DVConstraint
				.createExplicitListConstraint(new String[] { VALOR_SI, VALOR_NO });
		data_validation = new HSSFDataValidation(new CellRangeAddressList(1,
				maxRows, CAMPO_ASISTENCIA_JURIDICA,
				CAMPO_EXENCION_DEDUCIBLE_DANOS), dvConstraintSINO);
		data_validation.setSuppressDropDownArrow(false);
		sheet.addValidationData(data_validation);

		data_validation = new HSSFDataValidation(new CellRangeAddressList(1,
				maxRows, CAMPO_LIMITE_ACCIDENTE_CONDUCTOR,
				CAMPO_LIMITE_ACCIDENTE_CONDUCTOR), dvConstraintDecimal);
		sheet.addValidationData(data_validation);

		data_validation = new HSSFDataValidation(new CellRangeAddressList(1,
				maxRows, CAMPO_EXTENCION_RC, CAMPO_EXTENCION_RC),
				dvConstraintSINO);
		data_validation.setSuppressDropDownArrow(false);
		sheet.addValidationData(data_validation);

		data_validation = new HSSFDataValidation(new CellRangeAddressList(1,
				maxRows, CAMPO_LIMITE_ADAPTACION_CONVERSION,
				CAMPO_DEDUCIBLE_EQUIPO_ESPECIAL), dvConstraintDecimal);
		sheet.addValidationData(data_validation);

		data_validation = new HSSFDataValidation(new CellRangeAddressList(1,
				maxRows, CAMPO_EXENCION_DEDUCIBLE_ROBO,
				CAMPO_RESPONSABILIDAD_CIVIL_USA), dvConstraintSINO);
		data_validation.setSuppressDropDownArrow(false);
		sheet.addValidationData(data_validation);
		
		data_validation = new HSSFDataValidation(new CellRangeAddressList(1,
				maxRows, CAMPO_IGUALACION,
				CAMPO_IGUALACION), dvConstraintDecimal);
		sheet.addValidationData(data_validation);

		for (int i = 2; i < maxRows; i++) {

			dvConstraint = DVConstraint
					.createFormulaListConstraint("INDIRECT(UPPER(\"TP_\"&$A$"
							+ i + "))");
			data_validation = new HSSFDataValidation(new CellRangeAddressList(
					1, (i - 1), CAMPO_TIPO_USO, CAMPO_TIPO_USO), dvConstraint);
			sheet.addValidationData(data_validation);

			dvConstraint = DVConstraint
					.createFormulaListConstraint("INDIRECT(UPPER(\"P_\"&$A$"
							+ i + "))");
			data_validation = new HSSFDataValidation(new CellRangeAddressList(
					1, (i - 1), CAMPO_PAQUETE, CAMPO_PAQUETE), dvConstraint);
			sheet.addValidationData(data_validation);
		}
		
		DVConstraint dvConstraint0_9 = DVConstraint
		.createNumericConstraint(ValidationType.INTEGER,
				DVConstraint.OperatorType.BETWEEN, "0", "9");
		
		data_validation = new HSSFDataValidation(new CellRangeAddressList(1,
				maxRows, CAMPO_NUMERO_REMOLQUES,
				CAMPO_NUMERO_REMOLQUES), dvConstraint0_9);
		sheet.addValidationData(data_validation);
		
		
		dvConstraint = DVConstraint
				.createFormulaListConstraint(CAMPO_TIPO_CARGA_NOMBRE_RANGO);
		data_validation = new HSSFDataValidation(
				new CellRangeAddressList(1, maxRows, CAMPO_TIPO_CARGA,
						CAMPO_TIPO_CARGA), dvConstraint);
		data_validation.setSuppressDropDownArrow(false);
		sheet.addValidationData(data_validation);
		
		data_validation = new HSSFDataValidation(new CellRangeAddressList(1,
				maxRows, CAMPO_LIMITE_MUERTE,
				CAMPO_LIMITE_MUERTE), dvConstraintDecimal);
		sheet.addValidationData(data_validation);
	}
	
	private void llenaLineaEjemplo(HSSFSheet sheet){
		
		HSSFRow row = sheet.createRow(1);
		HSSFCell cell = row.createCell(CAMPO_LINEA_NEGOCIO);
		cell.setCellValue(ejemplo.getLineaNegocioDescripcion());
		cell = row.createCell(CAMPO_TIPO_USO);
		cell.setCellValue(ejemplo.getTipoUsoDescripcion());
		cell = row.createCell(CAMPO_DESCRIPCION);
		cell.setCellValue("");
		cell = row.createCell(CAMPO_CLAVE_AMIS);
		cell.setCellValue("99996");
		cell = row.createCell(CAMPO_MODELO);
		cell.setCellValue("2000");
		cell = row.createCell(CAMPO_PAQUETE);
		cell.setCellValue(ejemplo.getPaqueteDescripcion());
		cell = row.createCell(CAMPO_CODIGO_POSTAL);
		cell.setCellValue("67234");
		cell = row.createCell(CAMPO_DEDUCIBLE_DANOS_MATERIALES);
		cell.setCellValue("5");
		cell = row.createCell(CAMPO_DEDUCIBLE_ROBO_TOTAL);
		cell.setCellValue("5");
		cell = row.createCell(CAMPO_LIMITE_RC_TERCEROS);
		cell.setCellValue("10000");
		cell = row.createCell(CAMPO_LIMITE_GASTOS_MEDICOS);
		cell.setCellValue("40000");
		cell = row.createCell(CAMPO_ASISTENCIA_JURIDICA);
		cell.setCellValue("SI");
		cell = row.createCell(CAMPO_ASISTENCIA_EN_VIAJES);
		cell.setCellValue("NO");
		cell = row.createCell(CAMPO_EXENCION_DEDUCIBLE_DANOS);
		cell.setCellValue("NO");
		cell = row.createCell(CAMPO_LIMITE_ACCIDENTE_CONDUCTOR);
		cell.setCellValue("150000");
		cell = row.createCell(CAMPO_EXTENCION_RC);
		cell.setCellValue("SI");
		cell = row.createCell(CAMPO_LIMITE_ADAPTACION_CONVERSION);
		cell.setCellValue("100000");
		cell = row.createCell(CAMPO_LIMITE_EQUIPO_ESPECIAL);
		cell.setCellValue("60000");
		cell = row.createCell(CAMPO_DEDUCIBLE_EQUIPO_ESPECIAL);
		cell.setCellValue("25");
		cell = row.createCell(CAMPO_EXENCION_DEDUCIBLE_ROBO);
		cell.setCellValue("SI");
		cell = row.createCell(CAMPO_RESPONSABILIDAD_CIVIL_USA);
		cell.setCellValue("NO");
		cell = row.createCell(CAMPO_IGUALACION);
		cell.setCellValue("");
		cell = row.createCell(CAMPO_NUMERO_REMOLQUES);
		cell.setCellValue("");
		cell = row.createCell(CAMPO_TIPO_CARGA, Cell.CELL_TYPE_NUMERIC);
		cell.setCellValue("");
		cell = row.createCell(CAMPO_LIMITE_MUERTE);
		cell.setCellValue("100000");
		cell = row.createCell(CAMPO_DIAS_SALARIO_MINIMO);
		cell.setCellValue("5000");		
	}


	@Override
	protected void buildDataSheet(HSSFSheet sheet) {
		armaLineasNegocio(sheet);
		armaTipoCarga(sheet);
	}

	private void armaLineasNegocio(HSSFSheet sheet) {
		HSSFRow row = null;
		HSSFCell cell = null;
		HSSFName name = null;
		List<NegocioSeccion> negocioSeccionList = cargaMasivaService.getSeccionListByCotizacion(cotizacion);
		if (negocioSeccionList != null && !negocioSeccionList.isEmpty()) {
			row = sheet.createRow(dataRows);
			dataRows++;
			int linea = dataRows;
			int column = 0;
			boolean isFirst = true;
			for (NegocioSeccion item : negocioSeccionList) {
				cell = row.createCell(column);
				cell.setCellValue(remplazaEspacios(item.getSeccionDTO()
						.getDescripcion()));
				column++;
				
				if(isFirst){					
					ejemplo.setLineaNegocioDescripcion(cell.toString());
				}
				armaTipoUso(sheet, item, isFirst);
				armaPaquete(sheet, item, isFirst);				
				isFirst = false;
			}
			CellReference cellRef = new CellReference(dataRows, (column - 1));

			name = sheet.getWorkbook().createName();
			name.setNameName("NEGOCIOSECCION");
			String rango = "$A$" + linea + ":$" + cellRef.getCellRefParts()[2]
					+ "$" + linea;
			name.setRefersToFormula("'DataCombo'!" + rango + "");

		}
	}

	private void armaTipoCarga(HSSFSheet sheet) {
		HSSFRow row = null;
		HSSFCell cell = null;
		HSSFName name = null;

		List<CatalogoValorFijoDTO> tipoCargas = cargaMasivaService.getTipoCargas();
		if (!tipoCargas.isEmpty()) {
			row = sheet.createRow(dataRows);
			dataRows++;
			int column = 0;
			int linea = dataRows;

			for (CatalogoValorFijoDTO item2 : tipoCargas) {
				cell = row.createCell(column);
				cell.setCellValue(item2.getDescripcion());
				
				column++;
			}
			CellReference cellRef = new CellReference(dataRows, (column - 1));

			name = sheet.getWorkbook().createName();
			name.setNameName(CAMPO_TIPO_CARGA_NOMBRE_RANGO);

			String rango = "$A$" + linea + ":$" + cellRef.getCellRefParts()[2]
					+ "$" + linea;
			name.setRefersToFormula("'DataCombo'!" + rango + "");
		}
	}

	private void armaTipoUso(HSSFSheet sheet, NegocioSeccion negocioSeccion, boolean isFirst) {
		HSSFRow row = null;
		HSSFCell cell = null;
		HSSFName name = null;

		SeccionDTO seccion = cargaMasivaService.getSeccionDTOById(negocioSeccion);
		List<NegocioTipoUso> negocioTipoUsoList = cargaMasivaService.getNegocioTipoUsoListByNegSeccion(negocioSeccion);
		if (negocioTipoUsoList != null && !negocioTipoUsoList.isEmpty()) {
			row = sheet.createRow(dataRows);
			dataRows++;
			int column = 0;
			int linea = dataRows;

			for (NegocioTipoUso item2 : negocioTipoUsoList) {
				cell = row.createCell(column);
				cell.setCellValue(item2.getTipoUsoVehiculoDTO()
						.getDescripcionTipoUsoVehiculo());
				
				if(isFirst){
					isFirst = false;
					ejemplo.setTipoUsoDescripcion(cell.toString());
				}
				
				column++;
			}
			CellReference cellRef = new CellReference(dataRows, (column - 1));

			name = sheet.getWorkbook().createName();
			String nombre = "TP_" + seccion.getDescripcion();
			name.setNameName(remplazaEspacios(nombre).toUpperCase());

			String rango = "$A$" + linea + ":$" + cellRef.getCellRefParts()[2]
					+ "$" + linea;
			name.setRefersToFormula("'DataCombo'!" + rango + "");

		}
	}

	private void armaPaquete(HSSFSheet sheet, NegocioSeccion negocioSeccion, boolean isFirst) {
		HSSFRow row = null;
		HSSFCell cell = null;
		HSSFName name = null;

		SeccionDTO seccion = cargaMasivaService.getSeccionDTOById(negocioSeccion);
		List<NegocioPaqueteSeccion> negocioPaqueteSeccionList = cargaMasivaService.getNegocioPaqueteSeccionByNegSeccion(negocioSeccion);
		if (negocioPaqueteSeccionList != null
				&& !negocioPaqueteSeccionList.isEmpty()) {
			row = sheet.createRow(dataRows);
			dataRows++;
			int column = 0;
			int linea = dataRows;

			for (NegocioPaqueteSeccion item2 : negocioPaqueteSeccionList) {
				cell = row.createCell(column);
				cell.setCellValue(item2.getPaquete().getDescripcion());
				column++;
				
				if(isFirst){
					isFirst = false;
					ejemplo.setPaqueteDescripcion(cell.toString());
				}
			}
			CellReference cellRef = new CellReference(dataRows, (column - 1));

			name = sheet.getWorkbook().createName();
			String nombre = "P_" + seccion.getDescripcion();
			name.setNameName(remplazaEspacios(nombre).toUpperCase());

			String rango = "$A$" + linea + ":$" + cellRef.getCellRefParts()[2]
					+ "$" + linea;
			name.setRefersToFormula("'DataCombo'!" + rango + "");

		}
	}

	public void validaCargaMasiva(BitemporalCotizacion bitemporalCotizacion,
			BigDecimal idControlArchivo, Date validoEn) {

		try {

			this.bitemporalCotizacion = bitemporalCotizacion;
			this.tipoCarga = (short) 1;
			this.validoEn = TimeUtils.getDateTime(validoEn);
			this.cotizacion = new CotizacionDTO(bitemporalCotizacion.getValue());

			ControlArchivoDN controlArchivoDN = ControlArchivoDN.getInstancia();
			controlArchivoDTO = controlArchivoDN.getPorId(idControlArchivo);
			String fileName = this.obtenerNombreArchivo(controlArchivoDTO);

			HSSFWorkbook workbook = this.cargaArchivodeCarga(uploadFolder
					+ fileName);
			HSSFSheet validaciones = workbook.getSheetAt(0);
			leeLineasArchivoCarga(validaciones);

		} catch (IOException e) {
			e.printStackTrace();
		} catch (SystemException e) {
			e.printStackTrace();
		}
	}

	@Override
	protected void leeLineasArchivoCarga(HSSFSheet sheet) {
		Iterator<Row> rowIterator = sheet.rowIterator();
		boolean isFirst = true;
		
		while (rowIterator.hasNext()) {
			HSSFRow hssfRow = (HSSFRow) rowIterator.next();
			if (!isFirst) {
				DetalleCargaMasivaAutoCot detalle = creaLineaDetalleCarga(hssfRow);
				if(detalle != null){
					this.getDetalleCargaMasivaAutoCotList().add(detalle);
				}else{
					DetalleCargaMasivaAutoCot detalleVacio = new DetalleCargaMasivaAutoCot();
					this.getDetalleCargaMasivaAutoCotList().add(detalleVacio);
				}
			} else {
				isFirst = false;
			}
		}

		// Valida datos para incisos
		if (this.getListErrores().isEmpty()) {
				this.validaDetalleCargaMasiva(getDetalleCargaMasivaAutoCotList());
				
				try{
					if(bitemporalCotizacion.getValue().getNegocioDerechoEndosoId() == null){
						cargaMasivaService.obtieneNegocioDerechoEndosoAltaInciso(bitemporalCotizacion);
					}
					cargaMasivaService.guardaCotizacionEndosoMovimientos(bitemporalCotizacion);
					
					cargaMasivaService.calcularIncisosEndosoAltaInciso(bitemporalCotizacion, validoEn);

				}catch(Exception e){
					e.printStackTrace();
				}
				
		}
		if (this.getListErrores().isEmpty()) {
			setHasLogErrors(false);
		} else {
			creaLogErrores();
			setHasLogErrors(true);
		}
	}


	/** Valida que los datos obligatorio no sean nulos
	 * @param hssfRow
	 * @return
	 */
	private DetalleCargaMasivaAutoCot creaLineaDetalleCarga(HSSFRow hssfRow) {

		DetalleCargaMasivaAutoCot detalle = new DetalleCargaMasivaAutoCot();

		int fila = hssfRow.getRowNum() + 1;
		StringBuilder mensajeError = new StringBuilder("");
		String lineaDeNegocio = "";
		boolean lineaVacia = true;
		for (int i = 0; i <= maxColumns; i++) {
			HSSFCell cell = hssfRow.getCell(i);
			LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
			switch (i) {
			case CAMPO_LINEA_NEGOCIO:
				// Linea Negocio Obligatorio
				if (cell != null && !cell.toString().isEmpty()) {
					detalle.setLineaNegocioDescripcion(cell.toString());
					lineaDeNegocio = cell.toString();
					lineaVacia = false;
				}else{
					error.setNumeroInciso(new BigDecimal(fila));
					error.setNombreSeccion(lineaDeNegocio);
					error.setNombreCampoError("LINEA DE NEGOCIO");
					error.setRecomendaciones("Seleccione un campo del combo");
					listErrores.add(error);
					mensajeError.append("Campo: LINEA DE NEGOCIO -  Seleccione un campo del combo ").append("\r\n").append(System.getProperty("line.separator"));
				}
				break;
			case CAMPO_TIPO_USO:
				// Tipo Uso Obligatorio
				if (cell != null && !cell.toString().isEmpty()) {
					detalle.setTipoUsoDescripcion(cell.toString());
					lineaVacia = false;
				}else{
					error.setNumeroInciso(new BigDecimal(fila));
					error.setNombreSeccion(lineaDeNegocio);
					error.setNombreCampoError("TIPO DE USO");
					error.setRecomendaciones("Seleccione un campo del combo");
					listErrores.add(error);
					mensajeError.append("Campo: TIPO DE USO -  Seleccione un campo del combo ").append("\r\n").append(System.getProperty("line.separator"));
				}
				break;
			case CAMPO_DESCRIPCION:
				// Descripcion
				if (cell != null && !cell.toString().isEmpty()) {
					detalle.setDescripcion(cell.toString().toUpperCase());
					lineaVacia = false;
				}
				break;
			case CAMPO_CLAVE_AMIS:
				// Clave AMIS Obligatorio
				if (cell != null && !cell.toString().isEmpty()) {
					String valorStr = "";
					if(cell.getCellType() == HSSFCell.CELL_TYPE_NUMERIC){
						double valor = cell.getNumericCellValue();
						BigDecimal valorB = new BigDecimal(valor);
						valorStr = valorB.toPlainString();
					}else if(cell.getCellType() == HSSFCell.CELL_TYPE_STRING){
						valorStr = cell.getStringCellValue();
					}else{
						valorStr = cell.toString();
					}
					if (valorStr.indexOf(".") != -1) {
						valorStr = valorStr.substring(0, valorStr.indexOf("."));
					}
					detalle.setClaveAMIS(valorStr);
					lineaVacia = false;
				} else {
					error.setNumeroInciso(new BigDecimal(fila));
					error.setNombreSeccion(lineaDeNegocio);
					error.setNombreCampoError("CLAVE AMIS");
					error.setRecomendaciones("Ingrese la Clave AMIS");
					listErrores.add(error);
					mensajeError.append("Campo: CLAVE AMIS -  Ingrese la Clave AMIS ").append("\r\n").append(System.getProperty("line.separator"));
				}
				break;
			case CAMPO_MODELO:
				// Modelo Obligatorio
				if (cell != null && !cell.toString().isEmpty()) {
					String valor = cell.toString();
					if (valor.indexOf(".") != -1) {
						valor = valor.substring(0, valor.indexOf("."));
					}
					try{
						detalle.setModeloVehiculo(new Short(valor));
						lineaVacia = false;
					}catch(Exception e){
						error.setNumeroInciso(new BigDecimal(fila));
						error.setNombreSeccion(lineaDeNegocio);
						error.setNombreCampoError("MODELO");
						error.setRecomendaciones("Modelo del Vehiculo Invalido");
						listErrores.add(error);
						mensajeError.append("Campo: MODELO -  Modelo del Vehiculo Invalido ").append("\r\n").append(System.getProperty("line.separator"));						
					}
				}else{
					error.setNumeroInciso(new BigDecimal(fila));
					error.setNombreSeccion(lineaDeNegocio);
					error.setNombreCampoError("MODELO");
					error.setRecomendaciones("Ingrese el modelo del Vehiculo");
					listErrores.add(error);
					mensajeError.append("Campo: MODELO -  Ingrese el modelo del Vehiculo ").append("\r\n").append(System.getProperty("line.separator"));
				}
				break;
			case CAMPO_PAQUETE:
				// Paquete Obligatorio
				if (cell != null && !cell.toString().isEmpty()) {
					detalle.setPaqueteDescripcion(cell.toString());
					lineaVacia = false;
				}else{
					error.setNumeroInciso(new BigDecimal(fila));
					error.setNombreSeccion(lineaDeNegocio);
					error.setNombreCampoError("PAQUETE");
					error.setRecomendaciones("Seleccione un campo del combo");
					listErrores.add(error);
					mensajeError.append("Campo: PAQUETE -  Seleccione un campo del combo " ).append("\r\n").append(System.getProperty("line.separator"));
				}
				break;
			case CAMPO_CODIGO_POSTAL:
				// Codigo Postal Obligatorio
				if (cell != null && !cell.toString().isEmpty()) {
					String valor = cell.toString();
					if (valor.indexOf(".") != -1) {
						valor = valor.substring(0, valor.indexOf("."));
					}
					detalle.setCodigoPostal(valor);
					lineaVacia = false;
				}else {
					error.setNumeroInciso(new BigDecimal(fila));
					error.setNombreSeccion(lineaDeNegocio);
					error.setNombreCampoError("CODIGO POSTAL");
					error.setRecomendaciones("Ingrese el Codigo Postal");
					listErrores.add(error);
					mensajeError.append("Campo: CODIGO POSTAL -  Ingrese el Codigo Postal " ).append("\r\n").append(System.getProperty("line.separator"));
				}
				break;
			case CAMPO_DEDUCIBLE_DANOS_MATERIALES:
				if (cell != null && !cell.toString().isEmpty()) {
					try{
						detalle.setDeducibleDanosMateriales(new Double(cell.toString()));
						lineaVacia = false;
					}catch(Exception e){
					}
				}
				break;
			case CAMPO_DEDUCIBLE_ROBO_TOTAL:
				if (cell != null && !cell.toString().isEmpty()) {
					try{
						detalle.setDeducibleRoboTotal(new Double(cell.toString()));
						lineaVacia = false;
					}catch(Exception e){
					}
				}
				break;
			case CAMPO_LIMITE_RC_TERCEROS:
				if (cell != null && !cell.toString().isEmpty()) {
					try{
						detalle.setLimiteRcTerceros(new Double(cell.toString()));
						lineaVacia = false;
					}catch(Exception e){	
					}
				}
				break;
			case CAMPO_LIMITE_GASTOS_MEDICOS:
				if (cell != null && !cell.toString().isEmpty()) {
					try{
						detalle.setLimiteGastosMedicos(new Double(cell.toString()));
						lineaVacia = false;
					}catch(Exception e){	
					}
				}
				break;
			case CAMPO_ASISTENCIA_JURIDICA:
				// ASISTENCIA JURIDICA (Si O No)
				if (cell != null && !cell.toString().isEmpty()) {
					detalle.setAsistenciaJuridica(validaSINO(cell.toString()));
					lineaVacia = false;
				}
				break;
			case CAMPO_ASISTENCIA_EN_VIAJES:
				// ASISTENCIA EN VIAJES Y VIAL KM 0 (Si O No)
				if (cell != null && !cell.toString().isEmpty()) {
					detalle.setAsistenciaViajes(validaSINO(cell.toString()));
					lineaVacia = false;
				}
				break;
			case CAMPO_EXENCION_DEDUCIBLE_DANOS:
				// EXENCION DEDUCIBLE DANOS MATERIALES (Si O No)
				if (cell != null && !cell.toString().isEmpty()) {
					detalle.setExencionDeducibleDanosMateriales(validaSINO(cell.toString()));
					lineaVacia = false;
				}
				break;
			case CAMPO_LIMITE_ACCIDENTE_CONDUCTOR:
				// LIMITE ACCIDENTE CONDUCTOR ($)
				if (cell != null && !cell.toString().isEmpty()) {
					try{
						detalle.setLimiteAccidenteConductor(new Double(cell.toString()));
						lineaVacia = false;
					}catch(Exception e){
					}
				}
				break;
			case CAMPO_EXTENCION_RC:
				// EXTENCION RC (Si O No)
				if (cell != null && !cell.toString().isEmpty()) {
					detalle.setExencionRC(validaSINO(cell.toString()));
					lineaVacia = false;
				}
				break;
			case CAMPO_LIMITE_ADAPTACION_CONVERSION:
				// LIMITE ADAPTACION Y CONVERSION ($)
				if (cell != null && !cell.toString().isEmpty()) {
					try{
						detalle.setLimiteAdaptacionConversion(new Double(cell.toString()));
						lineaVacia = false;
					}catch(Exception e){
					}
				}
				break;
			case CAMPO_LIMITE_EQUIPO_ESPECIAL:
				// LIMITE EQUIPO ESPECIAL ($)
				if (cell != null && !cell.toString().isEmpty()) {
					try{
						detalle.setLimiteEquipoEspecial(new Double(cell.toString()));
						lineaVacia = false;
					}catch(Exception e){
					}
				}
				break;
			case CAMPO_DEDUCIBLE_EQUIPO_ESPECIAL:
				// DEDUCIBLE EQUIPO ESPECIAL (%)
				if (cell != null && !cell.toString().isEmpty()) {
					try{
						detalle.setDeducibleEquipoEspecial(new Double(cell.toString()));
						lineaVacia = false;
					}catch(Exception e){
					}
				}
				break;
			case CAMPO_EXENCION_DEDUCIBLE_ROBO:
				// EXENCION DEDUCIBLE ROBO TOTAL (Si O No)
				if (cell != null && !cell.toString().isEmpty()) {
					detalle.setExencionDeducibleRoboTotal(validaSINO(cell.toString()));
					lineaVacia = false;
				}
				break;
			case CAMPO_RESPONSABILIDAD_CIVIL_USA:
				// RESPONSABILIDAD CIVIL USA Y CANADA (Si O No)
				if (cell != null && !cell.toString().isEmpty()) {
					detalle.setResponsabilidadCivilUsaCanada(validaSINO(cell.toString()));
					lineaVacia = false;
				}
				break;
			case CAMPO_IGUALACION:
				if (cell != null && !cell.toString().isEmpty()) {
					try{
						detalle.setIgualacion(new Double(cell.toString()));
						lineaVacia = false;
					}catch(Exception e){
					}
				}
				break;
			case CAMPO_NUMERO_REMOLQUES:
				if (cell != null && !cell.toString().isEmpty()) {
					try{
						detalle.setNumeroRemolques(Double.valueOf(cell.toString()).intValue());
						lineaVacia = false;
					}catch(Exception e){
					}
				}
				break;
			case CAMPO_TIPO_CARGA:
				if (cell != null && !cell.toString().isEmpty()) {
					try{
						String tipoCargaDescripcion = cell.toString();
						detalle.setTipoCargaDescripcion(tipoCargaDescripcion);
						if (!StringUtils.isBlank(tipoCargaDescripcion)) {
							CatalogoValorFijoDTO tipoCarga = cargaMasivaService.obtieneTipoCargaPorDescripcion(tipoCargaDescripcion);
							detalle.setTipoCarga(tipoCarga);
						}
						lineaVacia = false;
					}catch(Exception e){
					}
				}
				break;
			case CAMPO_LIMITE_MUERTE:
				if (cell != null && !cell.toString().isEmpty()) {
					try{
						detalle.setLimiteMuerte(new Double(cell.toString()));
						lineaVacia = false;
					}catch(Exception e){	
					}
				}
				break;
			case CAMPO_SUMA_ASEGURADA_DANOS_MATERIALES:
				if (cell != null && !cell.toString().isEmpty()) {
					try{
						detalle.setValorSumaAseguradaDM(new Double(cell.toString()));
						lineaVacia = false;
					}catch(Exception e){
					}
				}
				break;
			case CAMPO_SUMA_ASEGURADA_ROBO_TOTAL:
				if (cell != null && !cell.toString().isEmpty()) {
					try{
						detalle.setValorSumaAseguradaRT(new Double(cell.toString()));
						lineaVacia = false;
					}catch(Exception e){
					}
				}
				break;
			case CAMPO_DIAS_SALARIO_MINIMO:
				if (cell != null && !cell.toString().isEmpty()) {
					try{
						detalle.setDiasSalarioMinimo(new Double(cell.toString()));
						lineaVacia = false;
					}catch(Exception e){
					}
				}
				break;				
			}
			
		}
		detalle.setMensajeError(mensajeError.toString());
		if (mensajeError.toString().isEmpty()) {
			detalle.setClaveEstatus(DetalleCargaMasivaAutoCot.ESTATUS_PENDIENTE);
		} else {
			detalle.setClaveEstatus(DetalleCargaMasivaAutoCot.ESTATUS_ERROR);
		}
		if(lineaVacia && !mensajeError.toString().isEmpty()){
			Iterator<LogErroresCargaMasivaDTO> it = listErrores.iterator();
			while(it.hasNext()){
				LogErroresCargaMasivaDTO error = it.next();
				if(error.getNumeroInciso().equals(new BigDecimal(fila))){
					it.remove();
				}
			}
			return null;
		}

		return detalle;
	}


	public void validaDetalleCargaMasiva(
			List<DetalleCargaMasivaAutoCot> detalleCargaMasivaList) {

		int fila = 1;
		StringBuilder mensajeError =  new StringBuilder();
		for (DetalleCargaMasivaAutoCot detalle : detalleCargaMasivaList) {

			fila++;
			if (detalle.getClaveEstatus() != null && !detalle.getClaveEstatus().equals(
					DetalleCargaMasivaAutoCot.ESTATUS_ERROR)) {
				BigDecimal numeroInciso = BigDecimal.valueOf(fila);

				
				BitemporalInciso bitemporalInciso = new BitemporalInciso();
				BitemporalAutoInciso bitemporalAutoInciso = new BitemporalAutoInciso();
				BitemporalCoberturaSeccion responsabilidadCivilCoberturaCotizacion = null;
				BitemporalCoberturaSeccion danosOcasionadosPorCargaCoberturaCotizacion = null;
				
				
				bitemporalInciso.getContinuity().setParentContinuity(bitemporalCotizacion.getContinuity());
				
				bitemporalAutoInciso.getValue().setAsociadaCotizacion(1);
				
				NegocioSeccion negocioSeccion = null;
				mensajeError.delete(0,mensajeError.length());
				String lineaDeNegocio = "";				

				negocioSeccion = cargaMasivaService
						.obtieneNegocioSeccionPorDescripcion(cotizacion, this
								.remplazaGuion(detalle
										.getLineaNegocioDescripcion()));
				if (negocioSeccion != null) {
					lineaDeNegocio = negocioSeccion.getSeccionDTO()
							.getDescripcion();
					bitemporalAutoInciso.getValue().setNegocioSeccionId(negocioSeccion
							.getIdToNegSeccion().longValue());
				} else {
					LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
					error.setNumeroInciso(new BigDecimal(fila));
					error.setNombreSeccion("");
					error.setNombreCampoError("LINEA DE NEGOCIO");
					error.setRecomendaciones("Linea de Negocio Invalida");
					listErrores.add(error);
					mensajeError.append("Campo: LINEA DE NEGOCIO -  Linea de Negocio Invalida ").append("\r\n").append(System.getProperty("line.separator"));
				}
				

				NegocioTipoUso negocioTipoUso = cargaMasivaService
						.obtieneNegocioTipoUsoPorDescripcion(negocioSeccion,
								detalle.getTipoUsoDescripcion());
				if (negocioTipoUso != null) {
					bitemporalAutoInciso.getValue().setTipoUsoId(negocioTipoUso
							.getTipoUsoVehiculoDTO().getIdTcTipoUsoVehiculo()
							.longValue());
				} else {
					LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
					error.setNumeroInciso(new BigDecimal(fila));
					error.setNombreSeccion(lineaDeNegocio);
					error.setNombreCampoError("TIPO DE USO");
					error.setRecomendaciones("TipoUso Invalido");
					listErrores.add(error);
					mensajeError.append("Campo: TIPO DE USO -  TipoUso Invalido ").append("\r\n").append(System.getProperty("line.separator"));
				}

				EstiloVehiculoDTO estiloVehiculo = cargaMasivaService
						.obtieneEstiloVehiculoDTOPorClaveAMIS(cotizacion,
								negocioSeccion, detalle.getClaveAMIS());
				if (estiloVehiculo != null) {
					bitemporalAutoInciso.getValue().setMarcaId(estiloVehiculo
							.getMarcaVehiculoDTO().getIdTcMarcaVehiculo());
					bitemporalAutoInciso.getValue()
							.setEstiloId(estiloVehiculo.getId().getStrId());
					
					try{
						bitemporalAutoInciso.getValue().setClaveTipoBien(estiloVehiculo.getId().getClaveTipoBien());
						bitemporalAutoInciso.getValue().setIdVersionCarga(estiloVehiculo.getId().getIdVersionCarga());
						bitemporalAutoInciso.getValue().setIdMoneda(cotizacion.getIdMoneda().shortValue());
					}catch(Exception e){
					}
					
					if(detalle.getDescripcion() != null && !detalle.getDescripcion().isEmpty()){
						bitemporalAutoInciso.getValue().setDescripcionFinal(detalle.getDescripcion().toUpperCase());
					}else{						
						bitemporalAutoInciso.getValue().setDescripcionFinal(estiloVehiculo.getDescripcionEstilo());
						detalle.setDescripcion(estiloVehiculo.getDescripcionEstilo().toUpperCase());
					}					
				} else {
					LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
					error.setNumeroInciso(new BigDecimal(fila));
					error.setNombreSeccion(lineaDeNegocio);
					error.setNombreCampoError("CLAVE AMIS");
					error.setRecomendaciones("Clave AMIS Invalida");
					listErrores.add(error);
					mensajeError.append("Campo: CLAVE AMIS -  Clave AMIS Invalida ").append("\r\n").append(System.getProperty("line.separator"));
				}

				//ValidaModelo
				if(estiloVehiculo != null){
					if(cargaMasivaService.validaModeloVehiculo(cotizacion.getIdMoneda(), estiloVehiculo.getId().getStrId(), 
							negocioSeccion.getIdToNegSeccion(), detalle.getModeloVehiculo())){
						bitemporalAutoInciso.getValue().setModeloVehiculo(detalle.getModeloVehiculo());
					}else{
						LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
						error.setNumeroInciso(new BigDecimal(fila));
						error.setNombreSeccion("");
						error.setNombreCampoError("MODELO VEHICULO");
						error.setRecomendaciones("Modelo Vehiculo Invalido");
						listErrores.add(error);
						mensajeError.append("Campo: ").append(error.getNombreCampoError()).append(" -  ").append(error.getRecomendaciones()).append(" ").append("\r\n").append(System.getProperty("line.separator"));			
					}
				}

				NegocioPaqueteSeccion negocioPaquete = cargaMasivaService
						.obtieneNegocioPaqueteSeccionPorDescripcion(
								negocioSeccion, detalle.getPaqueteDescripcion());

				if (negocioPaquete != null) {
					bitemporalAutoInciso.getValue().setNegocioPaqueteId(negocioPaquete
							.getIdToNegPaqueteSeccion());
					bitemporalAutoInciso.getValue().setPaquete(negocioPaquete.getPaquete());
				} else {
					LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
					error.setNumeroInciso(new BigDecimal(fila));
					error.setNombreSeccion(lineaDeNegocio);
					error.setNombreCampoError("PAQUETE");
					error.setRecomendaciones("Paquete Invalido");
					listErrores.add(error);
					mensajeError.append("Campo: PAQUETE -  Paquete Invalido ").append("\r\n").append(System.getProperty("line.separator"));			
				}

				String municipioId = cargaMasivaService
						.obtieneMunicipioIdPorCodigoPostal(detalle
								.getCodigoPostal());
				if (municipioId != null) {
					bitemporalAutoInciso.getValue().setMunicipioId(municipioId);

					String estadoId = cargaMasivaService
							.obtieneEstadoIdPorMunicipio(municipioId);
					bitemporalAutoInciso.getValue().setEstadoId(estadoId);
					
					if(!cargaMasivaService.validaEstadoMunicipio(negocioSeccion.getNegocioTipoPoliza().getNegocioProducto().getNegocio().getIdToNegocio(), 
							estadoId, municipioId)){
						LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
						error.setNumeroInciso(new BigDecimal(fila));
						error.setNombreSeccion("");
						error.setNombreCampoError("CODIGO POSTAL");
						error.setRecomendaciones("Codigo Postal Invalido");
						listErrores.add(error);
						mensajeError.append("Campo: CODIGO POSTAL -  Codigo Postal Invalido " ).append("\r\n").append(System.getProperty("line.separator"));
					}

				} else {
					LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
					error.setNumeroInciso(new BigDecimal(fila));
					error.setNombreSeccion(lineaDeNegocio);
					error.setNombreCampoError("CODIGO POSTAL");
					error.setRecomendaciones("Codigo Postal Invalido");
					listErrores.add(error);
					mensajeError.append("Campo: CODIGO POSTAL -  Codigo Postal Invalido ").append("\r\n").append(System.getProperty("line.separator"));
				}

				if (mensajeError.toString().isEmpty()) {
					
					List<BitemporalCoberturaSeccion> bitemporalCoberturaSeccionList = cargaMasivaService.mostrarCoberturas(
							bitemporalAutoInciso, bitemporalCotizacion.getContinuity().getId(), validoEn);
					
					
				    if(bitemporalCoberturaSeccionList != null && !bitemporalCoberturaSeccionList.isEmpty()){
						Short claveContrato = 1;
						Short claveObligatoriedad = 0;
						Short valorSI = 1;
						Short valorNO = 0;
						for(BitemporalCoberturaSeccion coberturaCotizacion : bitemporalCoberturaSeccionList){
							
			    			//Valida si es obligatoria
							Boolean esObligatoria = false;
			    			if(coberturaCotizacion.getValue().getClaveContrato().equals(claveContrato) && 
			    					coberturaCotizacion.getValue().getClaveObligatoriedad().equals(claveObligatoriedad)){
			    				esObligatoria = true;
			    			}
			    			
			    			//Valida Permite Suma Asegurada
			    			Boolean permiteSumaAsegurada = false;				    			
			    			if(coberturaCotizacion.getValue().getCoberturaSeccionDTO().getCoberturaDTO().getClaveFuenteSumaAsegurada().equals(CoberturaDTO.CLAVE_FUENTE_SA_VALOR_PROPORCIONADO) ||
			    					coberturaCotizacion.getValue().getCoberturaSeccionDTO().getCoberturaDTO().getClaveFuenteSumaAsegurada().equals(CoberturaDTO.CLAVE_FUENTE_SA_VALOR_FACTURA)){
			    				permiteSumaAsegurada = true;
			    			}			    			
			    			
			    			//Valida Rangos
			    			Boolean validaRangos = false;
			    			if(coberturaCotizacion.getValue().getCoberturaSeccionDTO().getCoberturaDTO().getClaveFuenteSumaAsegurada().equals("0") &&
			    					coberturaCotizacion.getValue().getValorSumaAseguradaMin() != 0 && coberturaCotizacion.getValue().getValorSumaAseguradaMax() != 0){
			    				validaRangos = true;
			    			}
			    			
			    			//Valida Deducibles
			    			Boolean validaDeducibles = false;
			    			if(!coberturaCotizacion.getValue().getCoberturaSeccionDTO().getCoberturaDTO().getClaveTipoDeducible().equals("0")){
			    				validaDeducibles = true;
			    			}
			    			
				    		if(coberturaCotizacion.getValue().getCoberturaSeccionDTO().getCoberturaDTO().getClaveTipoCalculo().toUpperCase().equals(
				    				CoberturaDTO.CLAVE_CALCULO_DANOS_MATERIALES.toUpperCase())){
				    			if(permiteSumaAsegurada && detalle.getValorSumaAseguradaDM() != null){
				    				boolean valido = true;
					    			if(validaRangos){
					    				valido = validaRangos(coberturaCotizacion.getValue().getValorSumaAseguradaMin(), 
					    						coberturaCotizacion.getValue().getValorSumaAseguradaMax(), detalle.getValorSumaAseguradaDM());
					    			}
					    			if(valido){
					    				coberturaCotizacion.getValue().setValorSumaAsegurada(detalle.getValorSumaAseguradaDM());
				    				}else{
				    					LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
				    					error.setNumeroInciso(new BigDecimal(fila));
				    					error.setNombreSeccion("");				    					
				    					error.setRecomendaciones("Valor no valido");
				    					mensajeError.append("Campo: SUMA ASEGURADA DM -  Valor debe ser entre ").append(coberturaCotizacion.getValue().getValorSumaAseguradaMin()).append(
				    					" y ").append(coberturaCotizacion.getValue().getValorSumaAseguradaMax()).append(" ");
				    					error.setNombreCampoError(mensajeError.toString());
				    					listErrores.add(error);
				    				}
				    			}				    			
				    			if(detalle.getDeducibleDanosMateriales() != null){
				    				boolean valido = true;
				    				if(validaDeducibles){
				    					valido = validaDeducible(coberturaCotizacion.getValue().getDeducibles(), detalle.getDeducibleDanosMateriales());
				    				}
				    				if(valido){
				    					coberturaCotizacion.getValue().setValorDeducible(detalle.getDeducibleDanosMateriales());
				    					coberturaCotizacion.getValue().setPorcentajeDeducible(detalle.getDeducibleDanosMateriales());
				    					coberturaCotizacion.getValue().setClaveContrato(claveContrato);
				    				}else{
				    					LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
				    					error.setNumeroInciso(new BigDecimal(fila));
				    					error.setNombreSeccion(lineaDeNegocio);
				    					error.setRecomendaciones("Valor no valido");
				    					mensajeError.append("Campo: DEDUCIBLE DA\u00d1OS MATERIALES -  Valor no valido ").append("\r\n").append(System.getProperty("line.separator"));
				    					error.setNombreCampoError(mensajeError.toString());
				    					listErrores.add(error);
				    				}
				    			}
					 		}else if(coberturaCotizacion.getValue().getCoberturaSeccionDTO().getCoberturaDTO().getClaveTipoCalculo().trim().toUpperCase().equals(
					 				CoberturaDTO.CLAVE_CALCULO_ROBO_TOTAL.trim().toUpperCase())){
					 			if(permiteSumaAsegurada && detalle.getValorSumaAseguradaRT() != null){
				    				boolean valido = true;
					    			if(validaRangos){
					    				valido = validaRangos(coberturaCotizacion.getValue().getValorSumaAseguradaMin(), 
					    						coberturaCotizacion.getValue().getValorSumaAseguradaMax(), detalle.getValorSumaAseguradaRT());
					    			}
					    			if(valido){
					    				coberturaCotizacion.getValue().setValorSumaAsegurada(detalle.getValorSumaAseguradaRT());
				    				}else{
				    					LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
				    					error.setNumeroInciso(new BigDecimal(fila));
				    					error.setNombreSeccion("");
				    					error.setRecomendaciones("Valor no valido");
				    					mensajeError.append("Campo: SUMA ASEGURADA RT -  Valor debe ser entre ").append(coberturaCotizacion.getValue().getValorSumaAseguradaMin()).append(
				    					" y ").append(coberturaCotizacion.getValue().getValorSumaAseguradaMax()).append(" ");
				    					error.setNombreCampoError(mensajeError.toString());
				    					listErrores.add(error);
				    				}
				    			}					 			
				    			if(detalle.getDeducibleRoboTotal() != null){
				    				boolean valido = true;
				    				if(validaDeducibles){
				    					valido = validaDeducible(coberturaCotizacion.getValue().getDeducibles(), detalle.getDeducibleRoboTotal());
				    				}
				    				if(valido){
				    					coberturaCotizacion.getValue().setValorDeducible(detalle.getDeducibleRoboTotal());
				    					coberturaCotizacion.getValue().setPorcentajeDeducible(detalle.getDeducibleRoboTotal());
				    					coberturaCotizacion.getValue().setClaveContrato(claveContrato);
				    				}else{
				    					LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
				    					error.setNumeroInciso(new BigDecimal(fila));
				    					error.setNombreSeccion(lineaDeNegocio);
				    					error.setRecomendaciones("Valor no valido");
				    					mensajeError.append("Campo: DEDUCIBLE ROBO TOTAL -  Valor no valido ").append("\r\n").append(System.getProperty("line.separator"));
				    					error.setNombreCampoError(mensajeError.toString());
				    					listErrores.add(error);
				    				}
				    			}
					 		}else if(coberturaCotizacion.getValue().getCoberturaSeccionDTO().getCoberturaDTO().getNombreComercial().toUpperCase().equals(
								 NOMBRE_LIMITE_RC_TERCEROS.toUpperCase())){
						    		if(detalle.getLimiteRcTerceros() != null){
						    			boolean valido = true;
						    			if(validaRangos){
						    				valido = validaRangos(coberturaCotizacion.getValue().getValorSumaAseguradaMin(), 
						    						coberturaCotizacion.getValue().getValorSumaAseguradaMax(), detalle.getLimiteRcTerceros());
						    			}
						    			if(valido){
						    				coberturaCotizacion.getValue().setValorSumaAsegurada(detalle.getLimiteRcTerceros());
						    				coberturaCotizacion.getValue().setClaveContrato(claveContrato);						    				
						    			}else{
					    					LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
					    					error.setNumeroInciso(new BigDecimal(fila));
					    					error.setNombreSeccion(lineaDeNegocio);
					    					error.setRecomendaciones("Valor no valido");
					    					mensajeError.append("Campo: LIMITE RC TERCEROS -  Valor debe ser entre ").append(coberturaCotizacion.getValue().getValorSumaAseguradaMin()).append(
					    					" y ").append(coberturaCotizacion.getValue().getValorSumaAseguradaMax()).append("").append("\r\n").append(System.getProperty("line.separator"));
					    					error.setNombreCampoError(mensajeError.toString());
					    					listErrores.add(error);
						    			}
						    		}
						    		responsabilidadCivilCoberturaCotizacion = coberturaCotizacion;
					 		}else if(coberturaCotizacion.getValue().getCoberturaSeccionDTO().getCoberturaDTO().getNombreComercial().toUpperCase().equals(
									 NOMBRE_LIMITE_GASTOS_MEDICOS.toUpperCase())){
							    		if(detalle.getLimiteGastosMedicos() != null){
							    			boolean valido = true;
							    			if(validaRangos){
							    				valido = validaRangos(coberturaCotizacion.getValue().getValorSumaAseguradaMin(), 
							    						coberturaCotizacion.getValue().getValorSumaAseguradaMax(), detalle.getLimiteGastosMedicos());
							    			}
							    			if(valido){
							    				coberturaCotizacion.getValue().setValorSumaAsegurada(detalle.getLimiteGastosMedicos());
							    				coberturaCotizacion.getValue().setClaveContrato(claveContrato);					    				
							    			}else{
						    					LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
						    					error.setNumeroInciso(new BigDecimal(fila));
						    					error.setNombreSeccion(lineaDeNegocio);
						    					error.setRecomendaciones("Valor no valido");
						    					mensajeError.append("Campo: LIMITE GASTOS MEDICOS -  Valor debe ser entre ").append(coberturaCotizacion.getValue().getValorSumaAseguradaMin()).append(
						    					" y ").append(coberturaCotizacion.getValue().getValorSumaAseguradaMax()).append("").append("\r\n").append(System.getProperty("line.separator"));
						    					error.setNombreCampoError(mensajeError.toString());
						    					listErrores.add(error);
							    			}
							    		}
					 		}else if(coberturaCotizacion.getValue().getCoberturaSeccionDTO().getCoberturaDTO().getNombreComercial().toUpperCase().equals(
									 NOMBRE_ASISTENCIA_JURIDICA.toUpperCase())){
							    		if(detalle.getAsistenciaJuridica() != null){
							    			if(detalle.getAsistenciaJuridica().equals(valorSI)){
							    				coberturaCotizacion.getValue().setClaveContrato(claveContrato);
							    			}else{
							    				if(!esObligatoria){
							    					coberturaCotizacion.getValue().setClaveContrato(valorNO);
								    			}
							    			}
							    		}
					 		}else if(coberturaCotizacion.getValue().getCoberturaSeccionDTO().getCoberturaDTO().getNombreComercial().toUpperCase().startsWith(
									 NOMBRE_ASISTENCIA_EN_VIAJES.toUpperCase())){
							    		if(detalle.getAsistenciaViajes() != null){
							    			if(detalle.getAsistenciaViajes().equals(valorSI)){
							    				coberturaCotizacion.getValue().setClaveContrato(claveContrato);
							    			}else{
							    				if(!esObligatoria){
							    					coberturaCotizacion.getValue().setClaveContrato(valorNO);
								    			}
							    			}
							    		}
					 		}else if(coberturaCotizacion.getValue().getCoberturaSeccionDTO().getCoberturaDTO().getNombreComercial().toUpperCase().equals(
									 NOMBRE_EXENCION_DEDUCIBLE_DANOS.toUpperCase())){
							    		if(detalle.getExencionDeducibleDanosMateriales() != null){
							    			if(detalle.getExencionDeducibleDanosMateriales().equals(valorSI)){
							    				coberturaCotizacion.getValue().setClaveContrato(claveContrato);
							    			}else{
							    				if(!esObligatoria){
							    					coberturaCotizacion.getValue().setClaveContrato(valorNO);
								    			}
							    			}
							    		}
					 		}else if(coberturaCotizacion.getValue().getCoberturaSeccionDTO().getCoberturaDTO().getNombreComercial().toUpperCase().equals(
									 NOMBRE_LIMITE_ACCIDENTE_CONDUCTOR.toUpperCase())){
							    		if(detalle.getLimiteAccidenteConductor() != null){
							    			boolean valido = true;
							    			if(validaRangos){
							    				valido = validaRangos(coberturaCotizacion.getValue().getValorSumaAseguradaMin(), 
							    						coberturaCotizacion.getValue().getValorSumaAseguradaMax(), detalle.getLimiteAccidenteConductor());
							    			}
							    			if(valido){
							    				coberturaCotizacion.getValue().setValorSumaAsegurada(detalle.getLimiteAccidenteConductor());
							    				coberturaCotizacion.getValue().setClaveContrato(claveContrato);					    				
							    			}else{
						    					LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
						    					error.setNumeroInciso(new BigDecimal(fila));
						    					error.setNombreSeccion(lineaDeNegocio);
						    					error.setRecomendaciones("Valor no valido");
						    					mensajeError.append("Campo: LIMITE ACCIDENTE CONDUCTOR -  Valor debe ser entre ").append(coberturaCotizacion.getValue().getValorSumaAseguradaMin()).append(
						    					" y ").append(coberturaCotizacion.getValue().getValorSumaAseguradaMax()).append("").append("\r\n").append(System.getProperty("line.separator"));
						    					error.setNombreCampoError(mensajeError.toString());
						    					listErrores.add(error);
							    			}
							    		}
					 		}else if(coberturaCotizacion.getValue().getCoberturaSeccionDTO().getCoberturaDTO().getNombreComercial().toUpperCase().equals(
									 NOMBRE_EXTENCION_RC.toUpperCase())){
					    		if(detalle.getExencionRC() != null){
					    			if(detalle.getExencionRC().equals(valorSI)){
					    				coberturaCotizacion.getValue().setClaveContrato(claveContrato);
					    			}else{
					    				if(!esObligatoria){
					    					coberturaCotizacion.getValue().setClaveContrato(valorNO);
						    			}
					    			}
					    		}
					 		}else if(coberturaCotizacion.getValue().getCoberturaSeccionDTO().getCoberturaDTO().getNombreComercial().toUpperCase().equals(
									 NOMBRE_LIMITE_ADAPTACION_CONVERSION.toUpperCase())){
							    		if(detalle.getLimiteAdaptacionConversion() != null){
							    			boolean valido = true;
							    			if(validaRangos){
							    				valido = validaRangos(coberturaCotizacion.getValue().getValorSumaAseguradaMin(), 
							    						coberturaCotizacion.getValue().getValorSumaAseguradaMax(), detalle.getLimiteAdaptacionConversion());
							    			}
							    			if(valido){
							    				coberturaCotizacion.getValue().setValorSumaAsegurada(detalle.getLimiteAdaptacionConversion());
							    				coberturaCotizacion.getValue().setClaveContrato(claveContrato);					    				
							    			}else{
						    					LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
						    					error.setNumeroInciso(new BigDecimal(fila));
						    					error.setNombreSeccion(lineaDeNegocio);
						    					error.setRecomendaciones("Valor no valido");
						    					mensajeError.append("Campo: LIMITE ADAPTACION CONVERSION -  Valor debe ser entre ").append(coberturaCotizacion.getValue().getValorSumaAseguradaMin()).append(
						    					" y ").append(coberturaCotizacion.getValue().getValorSumaAseguradaMax()).append("").append("\r\n").append(System.getProperty("line.separator"));
						    					error.setNombreCampoError(mensajeError.toString());
						    					listErrores.add(error);
							    			}
							    		}
					 		}else if(coberturaCotizacion.getValue().getCoberturaSeccionDTO().getCoberturaDTO().getNombreComercial().toUpperCase().equals(
									 NOMBRE_LIMITE_EQUIPO_ESPECIAL.toUpperCase())){
							    		if(detalle.getLimiteEquipoEspecial() != null){
							    			boolean valido = true;
							    			if(validaRangos){
							    				valido = validaRangos(coberturaCotizacion.getValue().getValorSumaAseguradaMin(), 
							    						coberturaCotizacion.getValue().getValorSumaAseguradaMax(), detalle.getLimiteEquipoEspecial());
							    			}
							    			if(valido){
							    				coberturaCotizacion.getValue().setValorSumaAsegurada(detalle.getLimiteEquipoEspecial());
							    				coberturaCotizacion.getValue().setClaveContrato(claveContrato);					    				
							    			}else{
						    					LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
						    					error.setNumeroInciso(new BigDecimal(fila));
						    					error.setNombreSeccion(lineaDeNegocio);
						    					error.setRecomendaciones("Valor no valido");
						    					mensajeError.append("Campo: LIMITE EQUIPO ESPECIAL -  Valor debe ser entre ").append(coberturaCotizacion.getValue().getValorSumaAseguradaMin()).append(
						    					" y ").append(coberturaCotizacion.getValue().getValorSumaAseguradaMax()).append("").append("\r\n").append(System.getProperty("line.separator"));
						    					error.setNombreCampoError(mensajeError.toString());
						    					listErrores.add(error);
							    			}
							    			if(valido && detalle.getDeducibleEquipoEspecial() != null){
							    				if(validaDeducibles){
							    					valido = validaDeducible(coberturaCotizacion.getValue().getDeducibles(), detalle.getDeducibleEquipoEspecial());
							    				}
							    				if(valido){
							    					coberturaCotizacion.getValue().setValorDeducible(detalle.getDeducibleEquipoEspecial());
							    					coberturaCotizacion.getValue().setPorcentajeDeducible(detalle.getDeducibleEquipoEspecial());
							    					coberturaCotizacion.getValue().setClaveContrato(claveContrato);
							    				}else{
							    					LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
							    					error.setNumeroInciso(new BigDecimal(fila));
							    					error.setNombreSeccion(lineaDeNegocio);
							    					error.setRecomendaciones("Valor no valido");
							    					mensajeError.append("Campo: DEDUCIBLE EQUIPO ESPECIAL -  Valor no valido ").append("\r\n").append(System.getProperty("line.separator"));
							    					error.setNombreCampoError(mensajeError.toString());
							    					listErrores.add(error);
							    				}
							    			}
							    		}
					 		}else if(coberturaCotizacion.getValue().getCoberturaSeccionDTO().getCoberturaDTO().getNombreComercial().toUpperCase().equals(
									 NOMBRE_EXENCION_DEDUCIBLE_ROBO.toUpperCase())){
					    		if(detalle.getExencionDeducibleRoboTotal() != null){
					    			if(detalle.getExencionDeducibleRoboTotal().equals(valorSI)){
					    				coberturaCotizacion.getValue().setClaveContrato(claveContrato);
					    			}else{
					    				if(!esObligatoria){
					    					coberturaCotizacion.getValue().setClaveContrato(valorNO);
						    			}
					    			}
					    		}
					 		}else if(coberturaCotizacion.getValue().getCoberturaSeccionDTO().getCoberturaDTO().getNombreComercial().toUpperCase().equals(
									 NOMBRE_RESPONSABILIDAD_CIVIL_USA.toUpperCase())){
					    		if(detalle.getResponsabilidadCivilUsaCanada() != null){
					    			if(detalle.getResponsabilidadCivilUsaCanada().equals(valorSI)){
					    				coberturaCotizacion.getValue().setClaveContrato(claveContrato);
					    			}else{
					    				if(!esObligatoria){
					    					coberturaCotizacion.getValue().setClaveContrato(valorNO);
						    			}
					    			}
					    		}
					 		}else if(coberturaCotizacion.getValue().getCoberturaSeccionDTO().getCoberturaDTO().getNombreComercial().toUpperCase().equals(
					 				CoberturaDTO.DANOS_OCASIONADOS_CARGA_NOMBRE_COMERCIAL.toUpperCase())){
				 				boolean danosCargaContratada = esObligatoria || detalle.isDanosCargaContratada();
				 				coberturaCotizacion.getValue().setClaveContratoBoolean(danosCargaContratada);
				 				
				 				danosOcasionadosPorCargaCoberturaCotizacion = coberturaCotizacion;
				 				
					 		}else if(coberturaCotizacion.getValue().getCoberturaSeccionDTO().getCoberturaDTO().getNombreComercial().toUpperCase().equals(
									 NOMBRE_LIMITE_MUERTE.toUpperCase())){
							    		if(detalle.getLimiteMuerte() != null){
							    			boolean valido = true;
							    			if(validaRangos){
							    				valido = validaRangos(coberturaCotizacion.getValue().getValorSumaAseguradaMin(), 
							    						coberturaCotizacion.getValue().getValorSumaAseguradaMax(), detalle.getLimiteMuerte());
							    			}
							    			if(valido){
							    				coberturaCotizacion.getValue().setValorSumaAsegurada(detalle.getLimiteMuerte());
							    				coberturaCotizacion.getValue().setClaveContrato(claveContrato);					    				
							    			}else{
						    					LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
						    					error.setNumeroInciso(new BigDecimal(fila));
						    					error.setNombreSeccion(lineaDeNegocio);
						    					error.setRecomendaciones("Valor no valido");
						    					mensajeError.append("Campo: LIMITE MUERTE -  Valor debe ser entre ").append(coberturaCotizacion.getValue().getValorSumaAseguradaMin()).append(
						    					" y ").append(coberturaCotizacion.getValue().getValorSumaAseguradaMax()).append("").append("\r\n").append(System.getProperty("line.separator"));
						    					error.setNombreCampoError(mensajeError.toString());
						    					listErrores.add(error);
							    			}
							    		}else{
						    				if(!esObligatoria){
						    					coberturaCotizacion.getValue().setClaveContrato(valorNO);
							    			}
							    		}
					 		}else if(coberturaCotizacion.getValue().getCoberturaSeccionDTO().getCoberturaDTO().getNombreComercial().toUpperCase().equals(
					 				NOMBRE_LIMITE_RC_VIAJERO.toUpperCase())){
							    		if(detalle.getDiasSalarioMinimo() != null){
							    			boolean valido = true;
							    			valido = validaDiasSalarioMinimo(detalle.getDiasSalarioMinimo());
							    			if(valido){
							    				coberturaCotizacion.getValue().setDiasSalarioMinimo(detalle.getDiasSalarioMinimo().intValue());
							    				//coberturaCotizacion.getValue().setValorSumaAsegurada(detalle.getLimiteMuerte());
							    				coberturaCotizacion.getValue().setClaveContrato(claveContrato);
							    			}else{
						    					LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
						    					error.setNumeroInciso(new BigDecimal(fila));
						    					error.setNombreSeccion(lineaDeNegocio);
						    					error.setRecomendaciones("Valor no valido");
						    					mensajeError.append("Campo: LIMITE MUERTE -  Valor debe ser entre ").append(coberturaCotizacion.getValue().getValorSumaAseguradaMin()).append(
						    					" y ").append(coberturaCotizacion.getValue().getValorSumaAseguradaMax()).append("").append("\r\n").append(System.getProperty("line.separator"));
						    					error.setNombreCampoError(mensajeError.toString());
						    					listErrores.add(error);
							    			}
							    		}else{
						    				if(!esObligatoria){
						    					coberturaCotizacion.getValue().setClaveContrato(valorNO);
							    			}
							    		}
					 		}
					 	}
					 
					}
				    
				    //Guarda Inciso
				    if (mensajeError.toString().isEmpty()) {
				    	Map<String, String> datosRiesgo = null;
				    	if(detalle.getIgualacion() != null && detalle.getIgualacion() > 0) {
				    		bitemporalInciso.getValue().setPrimaTotalAntesDeIgualacion(detalle.getIgualacion());
				    	}
				    	bitemporalAutoInciso.getValue().setPctDescuentoEstado(0.0);
				    	
				    	bitemporalInciso = incisoServiceBitemporal.prepareGuardarIncisoBorrador(
				    			bitemporalInciso, bitemporalAutoInciso, datosRiesgo, bitemporalCoberturaSeccionList, validoEn);

						if (bitemporalInciso == null) {
							LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
							error.setNumeroInciso(new BigDecimal(fila));
							error.setNombreSeccion(lineaDeNegocio);
							error.setRecomendaciones("");
							mensajeError.append("Error al guardar Inciso ").append("\r\n").append(System.getProperty("line.separator"));
							error.setNombreCampoError(mensajeError.toString());
	    					listErrores.add(error);
						} else {
				    
				    
						    //Carga Datos de Riesgo
						    Map<String, String> valores = new LinkedHashMap<String, String>();
						    List<ControlDinamicoRiesgoDTO> controles = cargaMasivaService.getDatosRiesgo(bitemporalInciso.getContinuity().getId(), validoEn.toDate(), 
									valores, SolicitudDTO.CVE_TIPO_ENDOSO_ALTA_INCISO, TipoAccionDTO.getAltaIncisoEndosoCot(), true);
							
						    if(controles != null && controles.size() > 0 ){
						    
								if (responsabilidadCivilCoberturaCotizacion != null && responsabilidadCivilCoberturaCotizacion.getValue().getClaveContrato().intValue() == 1) {
									
									for(int i = 0; i < controles.size(); i++){
										ControlDinamicoRiesgoDTO control = controles.get(i);
										if(control.getEtiqueta().equals(ConfiguracionDatoInciso.DescripcionEtiqueta.NUMERO_REMOLQUES.toString())){
											List<LogErroresCargaMasivaDTO> tmpErrors = new ArrayList<LogErroresCargaMasivaDTO>();				 			
											cargaMasivaService.validaNoNulo(detalle.getNumeroRemolques(), NOMBRE_CAMPO_NUMERO_REMOLQUES, numeroInciso, lineaDeNegocio, tmpErrors);
											cargaMasivaService.validaRangos(0, 9, detalle.getNumeroRemolques(), NOMBRE_CAMPO_NUMERO_REMOLQUES, numeroInciso, lineaDeNegocio, tmpErrors);
											listErrores.addAll(tmpErrors);
											mensajeError.append(cargaMasivaService.generaMensajeError(tmpErrors));								
											control.setValor(detalle.getNumeroRemolques().toString());
										}
									}
									
								}
						    
								if (danosOcasionadosPorCargaCoberturaCotizacion != null && danosOcasionadosPorCargaCoberturaCotizacion.getValue().getClaveContrato().intValue() == 1) {
									List<LogErroresCargaMasivaDTO> tmpErrors = new ArrayList<LogErroresCargaMasivaDTO>();				 			
									
									
									if (cargaMasivaService.validaNoNulo(detalle.getTipoCargaDescripcion(), NOMBRE_CAMPO_TIPO_CARGA, numeroInciso, lineaDeNegocio, tmpErrors)) {
										cargaMasivaService.validaNoNulo2(detalle.getTipoCarga(), NOMBRE_CAMPO_TIPO_CARGA, numeroInciso, lineaDeNegocio, tmpErrors);			
									}
									
									if (responsabilidadCivilCoberturaCotizacion == null) {
										throw new RuntimeException("Paquete mal configurado ya que los paquetes que incluyen la cobertura de " +
												"daños por la carga deben tambien incluir la cobertura de responsabilidad civil.");
									}
									
									if (responsabilidadCivilCoberturaCotizacion.getValue().getClaveContrato().intValue() == 0) {
										cargaMasivaService
												.agregaError(
														"La cobertura responsabilidad civil debe estar contratada para poder contratar la cobertura de daños por la carga.",
														CoberturaDTO.DANOS_OCASIONADOS_CARGA_NOMBRE_COMERCIAL, numeroInciso, lineaDeNegocio,
														tmpErrors);
									}
									
									listErrores.addAll(tmpErrors);
									mensajeError.append(cargaMasivaService.generaMensajeError(tmpErrors));
									if (tmpErrors.size() == 0) {
										
										for(int i = 0; i < controles.size(); i++){
											ControlDinamicoRiesgoDTO control = controles.get(i);
											
											if(control.getEtiqueta().equals(ConfiguracionDatoInciso.DescripcionEtiqueta.TIPO_CARGA.toString())){
												if (detalle.getTipoCarga() == null) {
													throw new RuntimeException(
															"Error en la configuración de datos de inciso. Debe estar configurado el dato de inciso de tipo de carga para "
																	+ "la cobertura de daños ocasionados por la carga.");
												}								
												control.setValor(String.valueOf(detalle.getTipoCarga().getId().getIdDato()));
											}
										}
									}
								}
								
								//Guarda datos de riesgo
								cargaMasivaService.guardarDatosAdicionalesPaquete(controles, bitemporalInciso.getContinuity().getId(), validoEn.toDate());
								
						    }
						}
				    }

				}
				detalle.setMensajeError(mensajeError.toString());
				if (mensajeError.toString().isEmpty()) {
					detalle.setClaveEstatus(DetalleCargaMasivaAutoCot.ESTATUS_COTIZADO);
					detalle.setNumeroInciso(new BigDecimal(bitemporalInciso.getContinuity().getNumero()));	
				} else {
					detalle.setClaveEstatus(DetalleCargaMasivaAutoCot.ESTATUS_ERROR);
					break;
				}
			}
		}//FOR
		
	}
		
	public boolean validaDeducible(List<NegocioDeducibleCob> deducibles, Double valor){
		boolean esValido = false;
		if(deducibles != null){
			for(NegocioDeducibleCob deducible : deducibles){
				if(deducible.getValorDeducible().equals(valor)){
					esValido = true;
					break;
				}
			}
		}
		return esValido;
	}
	
	public boolean validaRangos(Number valorMinimo, Number valorMaximo, Number valor){
		if (valor == null) {
			return true;
		}
		boolean esValido = true;
		if(valor.doubleValue() < valorMinimo.doubleValue() || valor.doubleValue() > valorMaximo.doubleValue()){
			esValido = false;
		}
		return esValido;
	}
	
	private boolean validaDiasSalarioMinimo(Number valor){
		if (valor == null) {
			return true;
		}
		
		boolean esValido = false;
		try {
			List<CatalogoValorFijoDTO> catalogoValorFijoDTOs = ValorFijoDN
					.getInstancia()
					.buscarPorPropiedad(
							"id.idGrupoValores",
							Integer.valueOf(CatalogoValorFijoDTO.IDGRUPO_DIAS_SALARIO));
			if (!catalogoValorFijoDTOs.isEmpty()
					&& catalogoValorFijoDTOs != null) {
					for (CatalogoValorFijoDTO estatus : catalogoValorFijoDTOs) {
						if(valor.doubleValue() == Double.parseDouble(estatus.getDescripcion())){
							esValido = true;
						}
					}
			}
		} catch (SystemException e) {
			e.printStackTrace();
		}
		return esValido;
	}	
	

	private Short validaSINO(String campo) {
		Short valor = 0;
		if (campo.equals(VALOR_SI)) {
			valor = 1;
		}
		return valor;
	}

	public FileInputStream descargaLogErrores(BigDecimal idControlArchivo) {
		try {
			ControlArchivoDN controlArchivoDN = ControlArchivoDN.getInstancia();
			controlArchivoDTO = controlArchivoDN.getPorId(idControlArchivo);

			File file = new File(uploadFolder + "Log"
					+ controlArchivoDTO.getIdToControlArchivo() + ".txt");

			return new FileInputStream(file);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	

	public void setCotizacion(CotizacionDTO cotizacion) {
		this.cotizacion = cotizacion;
	}

	public CotizacionDTO getCotizacion() {
		return cotizacion;
	}

	public void setCargaMasivaService(CargaMasivaService cargaMasivaService) {
		this.cargaMasivaService = cargaMasivaService;
	}

	public CargaMasivaService getCargaMasivaService() {
		return cargaMasivaService;
	}

	public void setListErrores(List<LogErroresCargaMasivaDTO> listErrores) {
		this.listErrores = listErrores;
	}

	public List<LogErroresCargaMasivaDTO> getListErrores() {
		return listErrores;
	}

	public void setLogErrores(FileInputStream logErrores) {
		this.logErrores = logErrores;
	}

	public FileInputStream getLogErrores() {
		return logErrores;
	}

	public void setHasLogErrors(boolean hasLogErrors) {
		this.hasLogErrors = hasLogErrors;
	}

	public boolean isHasLogErrors() {
		return hasLogErrors;
	}

	public void setDetalleCargaMasivaAutoCotList(
			List<DetalleCargaMasivaAutoCot> detalleCargaMasivaAutoCotList) {
		this.detalleCargaMasivaAutoCotList = detalleCargaMasivaAutoCotList;
	}

	public List<DetalleCargaMasivaAutoCot> getDetalleCargaMasivaAutoCotList() {
		return detalleCargaMasivaAutoCotList;
	}

	public void setTipoCarga(Short tipoCarga) {
		this.tipoCarga = tipoCarga;
	}

	public Short getTipoCarga() {
		return tipoCarga;
	}

	public void setEjemplo(DetalleCargaMasivaAutoCot ejemplo) {
		this.ejemplo = ejemplo;
	}

	public DetalleCargaMasivaAutoCot getEjemplo() {
		return ejemplo;
	}



}
