package mx.com.afirme.midas2.excels;
import java.io.File;
import java.io.FileInputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

import mx.com.afirme.midas.catalogos.estilovehiculo.EstiloVehiculoDTO;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas2.domain.movil.cotizador.TarifasDTO;
import mx.com.afirme.midas2.exeption.ApplicationException;
import mx.com.afirme.midas2.service.movil.cotizador.CotizacionMovilService;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.NotImplementedException;
import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("prototype")
public class CargarExcelTarifasCotizacionMovilOptimizacion extends GenerarExcelBase {
	
	/** Log de CargarExcelTarifasCotizacionMovil */
	private static final Logger LOG = Logger
			.getLogger(CargarExcelTarifasCotizacionMovilOptimizacion.class);
	@Autowired
	private CotizacionMovilService tarifasService;
	private String path;
	List<TarifasDTO> listaTarifas;
	//TarifasDTO tarifasDTO;
	private String STATUS="ERROR";
	private String INFO = "ARCHIVO NO CARGADO";
	private File fileUpload;
	private String fileUploadFileName;
	private String fileUploadContentType;
	boolean archivoValido = false;
	private String error="";
	private boolean estadosValidos;
	private static final int NUMERO_COLUMNAS = 10;

	public List<TarifasDTO> leerXLS(File archivoXls) {
		fileUpload = archivoXls;
		listaTarifas = new ArrayList<TarifasDTO>();
		fileUploadFileName = "ExcelTarifasCotizacionMovil.xlsx";
		FileInputStream file = null;
		try {
			File saveFile = null;
			String tempPath;// = System.getProperty("java.io.tmpdir");
			
			String OSName = System.getProperty("os.name");
			if (OSName.toLowerCase().indexOf("windows")!=-1)
				tempPath = Sistema.UPLOAD_FOLDER;
			else
				tempPath = Sistema.LINUX_UPLOAD_FOLDER;
			saveFile = new File(tempPath + File.separator + fileUploadFileName);
			FileUtils.copyFile(fileUpload, saveFile);
			LOG.info("fileUpload"+fileUpload);
			LOG.info("tempPath"+tempPath);
			file = new FileInputStream(saveFile);
			Workbook workbook = null;
			try {
				workbook = WorkbookFactory.create(file);
				archivoValido = true;
			} 
			catch (Exception e) {
				archivoValido = false;
				this.setError("Archivo Invalido, Favor de revisar.");
				listaTarifas=null;
				LOG.error("leerXLS", e);
			}
				if(archivoValido){
					cargarListaTarifas(workbook.getSheetAt(0));
				}
				else{
					throw new ApplicationException(getError());
				}
			file.close();
			try {
				saveFile.delete();
			} catch (Exception e) {
				LOG.error("leerXLS", e);
			}
			
		} 
		catch(ApplicationException e){
			LOG.error("leerXLS", e);
			throw new ApplicationException(e.getMessage());
		}
		catch (Exception e) {
			LOG.error("leerXLS", e);
			setSTATUS("ERROR");
			if (!archivoValido) {
				throw new ApplicationException("ERROR  DE TIPO DE ARCHIVO:" + e);
			} else {
				throw new ApplicationException("ERROR VERFIQUE TAMAÑO Y TIPO DEL ARCHIVO:" + e);
			}
			
		} finally {
			IOUtils.closeQuietly(file);
		}
		return listaTarifas;
	}
	
private void cargarListaTarifas(Sheet sheet) {
		Iterator<Row> rowIterator = sheet.iterator();
		int cont = 0;
		estadosValidos=true;
		while (rowIterator.hasNext()&&estadosValidos) {
			try {
				Row row = rowIterator.next();
				if (cont>=1){
					Iterator<Cell> cellIterator = row.cellIterator();
					TarifasDTO tarifa = new TarifasDTO();
					tarifa = obtenerVector(cellIterator);
					LOG.info( " fila:cont =>"+cont );
					if (tarifa != null) {
						if (isNotEmpty(tarifa.getClavetarifa())
								|| isNotEmpty(tarifa.getEstado())
								|| isNotEmpty(tarifa.getMarcavehiculo())
								|| isNotEmpty(tarifa.getDescripciontarifa())
								|| isNotEmpty(tarifa.getModelovehiculo())
								|| isNotEmpty(tarifa.getBajalogica())
								|| isNotEmpty(tarifa.getTarifaamplia())
								|| isNotEmpty(tarifa.getTarifalimitada())){
							tarifa.setTarifaValida(true);																	
						}
						else {
							tarifa= new TarifasDTO();
							tarifa.setTarifaValida(false);
							tarifa.setMensajeTarifaValida("Campos con errores");
						}
							
					} else {
						tarifa= new TarifasDTO();
						tarifa.setTarifaValida(false);
						tarifa.setMensajeTarifaValida("Campos con errores");
					}
					listaTarifas.add(tarifa);
				}
				cont=cont+1;
			}
			catch (NoSuchElementException ex) {			
				///salida de while
				LOG.error("Salida de while");
				break;
			}
			catch(ApplicationException e){
				throw new ApplicationException(e.getMessage());
			}			
			catch (Exception e) {
				listaTarifas=null;				
				LOG.error(e.getMessage(), e);
				throw new ApplicationException(e.getMessage());
			}			
			
		}
		
	}	
	
	private TarifasDTO obtenerVector(Iterator<Cell> cellIterator) {
		LOG.info("obtenerVector>"+cellIterator);
		TarifasDTO tarifa = new TarifasDTO();
		int columna = 0;
		String cellValue = "";
		String tipoDato = "";
	
		try {
			while (columna <= NUMERO_COLUMNAS) {// recorrido de las columnas
				Cell cell = cellIterator.next();
				cellValue = "";
				tipoDato = "";
				try {
					switch (cell.getCellType()) {
					case Cell.CELL_TYPE_NUMERIC:
						cellValue = String.valueOf(cell.getNumericCellValue());
						tipoDato = "NUMERIC";
						break;
					case Cell.CELL_TYPE_STRING:
						cellValue = cell.getStringCellValue();
						tipoDato = "STRING";
						break;
					default:
						cellValue = cell.getStringCellValue();
						break;
					}
				} catch (Exception ex) {		
					LOG.error(ex.getMessage() + "Eror al procesar columna=>"+columna +";cellValue="+cellValue, ex);
				}
				LOG.info( " obtenerVector columna=>"+columna+";cellValue="+cellValue+"tipo="+tipoDato);
				if(cellValue!=null){
					if (cellValue.trim().length() > 0
							&& tipoDato.trim().length() > 0) {
						if (tipoDato.equals("STRING")) {
							if (columna == 0) {
								tarifa.setClavetarifa(cellValue);
							}
							else if (columna == 1) {
								tarifa.setGrupovehiculos(cellValue);
							} else if (columna == 2) {
								tarifa.setMarcavehiculo(cellValue);
							} else if (columna == 3) {
								tarifa.setTipovehiculo(cellValue);
							}
							else if (columna == 4) {
								tarifa.setDescripciontarifa(cellValue);
					        } 
							else if (columna == 5) {
								tarifa.setModelovehiculo(Short.parseShort(cellValue
										.substring(0, 4)));
							} else if (columna == 6) {
								tarifa.setOc(Double.parseDouble(cellValue));
							} else if (columna == 7) {
								tarifa.setEstado(cellValue);
							}else if (columna == 8) {
									tarifa.setTarifaamplia(new BigDecimal(cellValue));
							} else if (columna == 9) {
									tarifa.setTarifalimitada(new BigDecimal(cellValue));
							} else if (columna == 10) {
									tarifa.setBajalogica(Short.valueOf(String.valueOf((cellValue.charAt(0)))));		
							}
						}
	
						else {
							if (columna == 0) {
								tarifa.setClavetarifa(cellValue);
							}
							else if (columna == 1) {
								tarifa.setGrupovehiculos(cellValue);
							} else if (columna == 2) {
								tarifa.setMarcavehiculo(cellValue);
							}else if (columna == 3) {
								tarifa.setTipovehiculo(cellValue);
							}
							else if (columna == 4) {
								tarifa.setDescripciontarifa(cellValue);
					        } 
							else if (columna == 5) {
								tarifa.setModelovehiculo(Short.parseShort(cellValue
										.substring(0, 4)));
							} else if (columna == 6) {
								tarifa.setOc(Double.parseDouble(cellValue));
							} else if (columna == 7) {
								tarifa.setEstado(cellValue);
							}else if (columna == 8) {
									tarifa.setTarifaamplia(new BigDecimal(cellValue));
							} else if (columna == 9) {
									tarifa.setTarifalimitada(new BigDecimal(cellValue));
							} else if (columna == 10) {
									tarifa.setBajalogica(Short.valueOf(String.valueOf((cellValue.charAt(0)))));		
							}
						}
					}
				}
				else{
					tarifa=null;
				}
	
				columna=columna+1;
			}
		} 
		catch (NoSuchElementException ex) {
			LOG.error("Error obtenerVector", ex);
			columna=columna+1;
			tarifa = null;
			throw  ex;
		}
		catch (Exception ex) {
			LOG.error("Error obtenerVector", ex);
			columna=columna+1;
			tarifa = null;
		}
		return tarifa;
	}
	
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	public File getFileUpload() {
		return fileUpload;
	}

	public void setFileUpload(File fileUpload) {
		this.fileUpload = fileUpload;
	}

	public String getFileUploadFileName() {
		return fileUploadFileName;
	}

	public void setFileUploadFileName(String fileUploadFileName) {
		this.fileUploadFileName = fileUploadFileName;
	}

	public String getFileUploadContentType() {
		return fileUploadContentType;
	}

	public void setFileUploadContentType(String fileUploadContentType) {
		this.fileUploadContentType = fileUploadContentType;
	}
	
	public String getSTATUS() {
		return STATUS;
	}
	public void setSTATUS(String sTATUS) {
		STATUS = sTATUS;
	}
	public String getINFO() {
		return INFO;
	}
	public void setINFO(String iNFO) {
		INFO = iNFO;
	}

	public List<TarifasDTO> getListaTarifas() {
		return listaTarifas;
	}

	public void setListaTarifas(List<TarifasDTO> listaTarifas) {
		this.listaTarifas = listaTarifas;
	}

	@Override
	protected void buildDataSheet(HSSFSheet sheet) {
		throw new NotImplementedException();
	}

	@Override
	protected void buildValidationsSheet(HSSFSheet sheet) {
		throw new NotImplementedException();
	}

	@Override
	protected void leeLineasArchivoCarga(HSSFSheet sheet) {
		throw new NotImplementedException();
	}
	public boolean isArchivoValido() {
		return archivoValido;
	}


	public void setArchivoValido(boolean archivoValido) {
		this.archivoValido = archivoValido;
	}


	public String getError() {
		return error;
	}


	public void setError(String error) {
		this.error = error;
	}


	public boolean isEstadosValidos() {
		return estadosValidos;
	}


	public void setEstadosValidos(boolean estadosValidos) {
		this.estadosValidos = estadosValidos;
	}
	public  boolean isNotEmpty(Object obj){
		return !(obj==null||obj.toString().trim().equals(""));
	}

	public CotizacionMovilService getTarifasService() {
		return tarifasService;
	}

	public void setTarifasService(CotizacionMovilService tarifasService) {
		this.tarifasService = tarifasService;
	}
}
