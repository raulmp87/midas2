package mx.com.afirme.midas2.excels;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.solicitud.SolicitudDTO;
import mx.com.afirme.midas2.domain.suscripcion.solicitud.comentarios.Comentario;
import mx.com.afirme.midas2.service.suscripcion.solicitud.comentarios.ComentariosService;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;

public class GeneraExcelSolicitudesPoliza {
	private ComentariosService comentariosService;
	
	// Campos en proceso
	private static final int CAMPO_FOLIO = 0;
	private static final int CAMPO_USUARIO = 1;
	private static final int CAMPO_FECHA_SOLICITUD = 2;
	private static final int CAMPO_HORA_SOLICITUD = 3;
	private static final int CAMPO_NOMBRE_CLIENTE = 4;
	private static final int CAMPO_APELLIDO_PATERNO = 5;
	private static final int CAMPO_APELLIDO_MATERNO = 6;
	private static final int CAMPO_PRODUCTO = 7;
	private static final int CAMPO_TIPO_MOVIMIENTO = 8;
	private static final int CAMPO_CLAVE_AGENTE = 9;
	private static final int CAMPO_NOMBRE_AGENTE = 10;
	private static final int CAMPO_NOMBRE_ASIGNACION = 11;
	private static final int CAMPO_OFICINA = 12;
	private static final int CAMPO_ESTATUS = 13;
	private static final int CAMPO_COMENTARIOS = 14;
	
	public GeneraExcelSolicitudesPoliza(ComentariosService comentariosService){
		this.comentariosService = comentariosService;
	}

	public InputStream generaExcelSolicitud(List<SolicitudDTO> solicitudes)
			throws IOException {
		HSSFWorkbook workbook = null;
		HSSFSheet validaciones = null;
		ByteArrayOutputStream bos = null;

		try {
			// Obtiene plantilla 
			workbook = cargaPlantilla();
			validaciones = workbook.getSheetAt(0);

			//Llena campos de solicitud
			int linea = 1;
			for(SolicitudDTO solicitud : solicitudes){
				llenaLineaSolicitud(validaciones, solicitud, linea);
				linea++;
			}

			// Ajusta tamano de columnas
			for (int i = 0; i <= 13; i++) {
				validaciones.autoSizeColumn(i);
			}

			// Muestra primera hoja
			workbook.setActiveSheet(0);
			short firstCell = 0;
			validaciones.showInPane(firstCell, firstCell);

			bos = new ByteArrayOutputStream();

			workbook.write(bos);

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (bos != null) {
				bos.close();
			}
		}
		if (bos == null) {
			return null;
		}
		return new ByteArrayInputStream(bos.toByteArray());
	}
	
	public HSSFWorkbook cargaPlantilla() throws IOException {
		HSSFWorkbook workbook = null;
		POIFSFileSystem plantilla = new POIFSFileSystem(
				this.getClass()
						.getResourceAsStream(
								"/mx/com/afirme/midas2/impresiones/jrxml/plantillaSolicitudPoliza.xls"));
		workbook = new HSSFWorkbook(plantilla);
		return workbook;
	}
	
	private void llenaLineaSolicitud(HSSFSheet sheet, SolicitudDTO solicitud, int linea){
		HSSFRow row = sheet.createRow(linea);
		HSSFCell cell = row.createCell(CAMPO_FOLIO);
		cell.setCellValue(solicitud.getNumeroSolicitud());
		cell = row.createCell(CAMPO_USUARIO);
		cell.setCellValue(solicitud.getCodigoUsuarioCreacion());
		cell = row.createCell(CAMPO_FECHA_SOLICITUD);
		String fechaSolicitud = UtileriasWeb.getFechaString(solicitud.getFechaCreacion());
		cell.setCellValue(fechaSolicitud);
		cell = row.createCell(CAMPO_HORA_SOLICITUD);
		String horaSolicitud = UtileriasWeb.getHoraConSegundosString(solicitud.getFechaCreacion());
		cell.setCellValue(horaSolicitud);
		cell = row.createCell(CAMPO_NOMBRE_CLIENTE);
		cell.setCellValue(solicitud.getNombrePersona());
		cell = row.createCell(CAMPO_APELLIDO_PATERNO);
		cell.setCellValue(solicitud.getApellidoPaterno());
		cell = row.createCell(CAMPO_APELLIDO_MATERNO);
		cell.setCellValue(solicitud.getApellidoMaterno());
		cell = row.createCell(CAMPO_PRODUCTO);
		cell.setCellValue(solicitud.getProductoDTO().getDescripcion());
		cell = row.createCell(CAMPO_TIPO_MOVIMIENTO);
		cell.setCellValue(solicitud.getDescripcionTipoSolicitud());
		cell = row.createCell(CAMPO_CLAVE_AGENTE);
		if(solicitud.getAgente() != null && solicitud.getAgente().getIdAgente() != null){
			cell.setCellValue(solicitud.getAgente().getIdAgente());
		}
		cell = row.createCell(CAMPO_NOMBRE_ASIGNACION);
		if(solicitud.getCodigoUsuarioAsignacion() != null){
			cell.setCellValue(solicitud.getCodigoUsuarioAsignacion());
		}
		cell = row.createCell(CAMPO_NOMBRE_AGENTE);
		if(solicitud.getAgente() != null){
			cell.setCellValue(solicitud.getNombreAgente());
		}
		cell = row.createCell(CAMPO_OFICINA);
		cell.setCellValue(solicitud.getNombreEjecutivo());
		cell = row.createCell(CAMPO_ESTATUS);
		cell.setCellValue(solicitud.getDescripcionEstatus());
		cell = row.createCell(CAMPO_COMENTARIOS);
		llenaComentarios(cell, solicitud);
	}
	
	private void llenaComentarios(HSSFCell cell, SolicitudDTO solicitud){
		List<Comentario> comentariosList= new ArrayList<Comentario>();
		if (solicitud != null) {
			comentariosList = comentariosService.getComentarioPorSolicitud(solicitud.getIdToSolicitud());
			//Sort
			if(comentariosList != null && !comentariosList.isEmpty()){
				Collections.sort(comentariosList, 
						new Comparator<Comentario>() {				
							public int compare(Comentario n1, Comentario n2){
								return n1.getId().compareTo(n2.getId());
							}
						});
			}
			
			StringBuilder valor = new StringBuilder("");
			String separador = " | ";
			for(Comentario comentario : comentariosList){
				if(!valor.toString().equals("")){
					valor.append(separador);
				}
				valor.append(comentario.getValor());
			}
			cell.setCellValue(valor.toString());
		}
	}

}
