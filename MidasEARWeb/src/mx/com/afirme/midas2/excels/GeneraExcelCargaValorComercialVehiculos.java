package mx.com.afirme.midas2.excels;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.controlArchivo.ControlArchivoDN;
import mx.com.afirme.midas2.domain.vehiculo.ValorComercialVehiculo;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;
import mx.com.afirme.midas2.service.vehiculo.ValorComercialVehiculoService;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

@SuppressWarnings("deprecation")
public class GeneraExcelCargaValorComercialVehiculos extends GenerarExcelBase {
	
	private BigDecimal 					    idControlArchivo;
	private ValorComercialVehiculoService   valorComercialVehiculoService;
	private ArrayList<Integer> filaError  = new ArrayList<Integer>();
	private static int COLUMNAS           = 7;
	private boolean hasLogErrors 		  = false;
	private EntidadService                  entidadService;
	private UsuarioService                  usuarioService;
	private int                             cantidadDatosProcesados;
	

	public GeneraExcelCargaValorComercialVehiculos(){
		this.subirDocumento();
	}
	
	public GeneraExcelCargaValorComercialVehiculos(BigDecimal xIdControlArchivo, ValorComercialVehiculoService xValorComercialVehiculoService, EntidadService xEntidadService, UsuarioService xUsuarioService ){
		this.subirDocumento();
		this.idControlArchivo 		       = xIdControlArchivo;
		this.valorComercialVehiculoService = xValorComercialVehiculoService;
		this.entidadService 			   = xEntidadService;
		this.usuarioService 			   = xUsuarioService;
	}
	

	private void subirDocumento (){

		String OSName = System.getProperty("os.name");
		if (OSName.toLowerCase().indexOf("windows")!=-1)
			this.uploadFolder = Sistema.UPLOAD_FOLDER;
		else
			this.uploadFolder = Sistema.LINUX_UPLOAD_FOLDER;
		
	}
	
	
	public void validaCargaMasiva(BigDecimal idControlArchivo) {

		try {
				
			ControlArchivoDN controlArchivoDN = ControlArchivoDN.getInstancia();
			controlArchivoDTO = controlArchivoDN.getPorId(this.idControlArchivo);
			String fileName = this.obtenerNombreArchivo(controlArchivoDTO);

			HSSFWorkbook workbook = this.cargaArchivodeCarga(uploadFolder
					+ fileName);
			HSSFSheet validaciones = workbook.getSheetAt(0);
			
			leeLineasArchivoCarga(validaciones);

		} catch (IOException e) {
			e.printStackTrace();
		} catch (SystemException e) {
			e.printStackTrace();
		}
	}
	

	@Override
	protected void leeLineasArchivoCarga(HSSFSheet sheet) {

		int fila                   = 0;
		String mensaje             = "";
		boolean celdaNoNula        = true;
		int totalColsPrecioModelos = 6;
		List<Integer> aniosModelos = new ArrayList<Integer>();
		List<ValorComercialVehiculo> lValorComercialVehiculo = new ArrayList<ValorComercialVehiculo>();
		
		Iterator<Row> rowIterator = sheet.rowIterator();
		while (rowIterator.hasNext()) {
			
			HSSFRow hssfRow = (HSSFRow) rowIterator.next();
			fila = hssfRow.getRowNum() + 1;
			
			ValorComercialVehiculo valorComercialVehiculo = new ValorComercialVehiculo();
			
			if( fila == 1){
				
				short ultimaPos    = hssfRow.getLastCellNum(); // # OBTIENE LA ULTIMA POSICION DEL EXCEL DONDE EXISTEN DATOS

				for( int cabI = 6; cabI <= ultimaPos; cabI++){
					
					HSSFCell cellCab = hssfRow.getCell(cabI);
					
					if( cellCab != null ){
						
						cellCab.setCellType(Cell.CELL_TYPE_STRING);
						// # GUARDA LA CABECERA DE AÑOS
						if( !cellCab.getStringCellValue().isEmpty() & !cellCab.getStringCellValue().equals("")  ){
							aniosModelos.add(Integer.parseInt( cellCab.getStringCellValue() ));
							// # GUARDA EL NUMERO DE COLUMNAS LEIDAS PARA ITERAR SOBRE ELLAS EN EL SEGUNDO MÉTODO QUE LEE EL LADO DERECHO (ANIO-PRECIO)
							totalColsPrecioModelos ++;
						}
						
					}
					
				}
				
			}else if ( fila > 1 ){
				
				for( int i = 0; i<=GeneraExcelCargaValorComercialVehiculos.COLUMNAS; i++){
					
					valorComercialVehiculo.setDocumento( new Long(this.idControlArchivo.toString()) );
					valorComercialVehiculo.setEstatusRegistro((short) 1);
					valorComercialVehiculo.setFilaExcel(fila);
					valorComercialVehiculo.setCodigoUsuarioCreacion( this.usuarioService.getUsuarioActual().getNombreUsuario() );
					
					HSSFCell cell = hssfRow.getCell(i);
					
					if ( cell != null){
						
						celdaNoNula = false;
						
						switch (i) {
							case 0 : // NUMERO PROVEEDOR
								
								cell.setCellType(Cell.CELL_TYPE_STRING);
								
								if ( !this.isInt(cell.getStringCellValue()) ){
									this.construyeListaErrores(fila, cell.getStringCellValue(), "NUMERO PROVEEDOR: Solo se permiten datos numéricos: ");
									valorComercialVehiculo.setError(true);
								}else{
									valorComercialVehiculo.setProveedorId( new Long(cell.getStringCellValue()) );
								}
								
								break;
							case 1 : // CLAVE AMIS
								cell.setCellType(Cell.CELL_TYPE_STRING);
								boolean validaAmis = true;
								
								if ( !this.isInt(cell.getStringCellValue()) ){
									validaAmis = false;
									this.construyeListaErrores(fila, cell.getStringCellValue(), "CLAVE AMIS: Solo se permiten datos numéricos: ");
								}
								
								if ( cell.getStringCellValue().trim().length() <= 0 || cell.getStringCellValue().trim().length() > 5  ){
									validaAmis = false;
									this.construyeListaErrores(fila, cell.getStringCellValue(), "CLAVE AMIS: no puede ser nula y/o mayor a 5 caracteres ");
								}
								
								if (validaAmis){
									valorComercialVehiculo.setClaveAmis(cell.getStringCellValue().trim());
								}else{
									valorComercialVehiculo.setError(true);
								}
								
								break;
							case 2 : // MES DE VALOR COMERCIAL
								cell.setCellType(Cell.CELL_TYPE_STRING);
								
								int mes = this.convertirMeses(cell.getStringCellValue());
								
								if ( mes == 0){
									this.construyeListaErrores(fila, cell.getStringCellValue(), "VALOR COMERCIAL MES: nombre del mes no reconocido ");
									valorComercialVehiculo.setError(true);
								}else{
									valorComercialVehiculo.setValorComercialMes( mes );
								}
								
								break;
							case 3 : // ANIO DE VALOR COMERCIAL
								cell.setCellType(Cell.CELL_TYPE_STRING);
								boolean validadoMes = true;
								
								if ( !this.isInt(cell.getStringCellValue()) ){
									validadoMes = false;
									this.construyeListaErrores(fila, cell.getStringCellValue(), "VALOR COMERCIAL AÑO: Solo se permiten datos numéricos: ");
								}
								
								if ( cell.getStringCellValue().length() != 4 ){
									validadoMes = false;
									this.construyeListaErrores(fila, cell.getStringCellValue(), "VALOR COMERCIAL AÑO:  El año se debe complementar de 4 dígitos YYYY: ");
								}
								
								if ( validadoMes ){
									valorComercialVehiculo.setValorComercialAnio(Integer.parseInt( cell.getStringCellValue()));
								}else{
									valorComercialVehiculo.setError(true);
								}
								
								break;
							case 4 : // DESCRIPCION ESTILO
								cell.setCellType(Cell.CELL_TYPE_STRING);
								valorComercialVehiculo.setDescripcionVehiculo(cell.getStringCellValue().toUpperCase());
								
								break;
							case 5 : // VALOR DE NUEVO
								cell.setCellType(Cell.CELL_TYPE_STRING);
								
								if ( !cell.getStringCellValue().isEmpty() & cell.getStringCellValue()!=null ){
									if ( !this.isDouble(cell.getStringCellValue()) ){
										this.construyeListaErrores(fila, cell.getStringCellValue(), "VALOR DE NUEVO: debe ser una cantidad: ");
										valorComercialVehiculo.setError(true);
									}else{
										valorComercialVehiculo.setValorComercialNuevo(Double.valueOf(cell.getStringCellValue()));
									}
								}
								
								break;
							case 6:
								
								Map<Integer,Double> datosModeloPrecio = new LinkedHashMap<Integer,Double>();								
								int posAnio = 0;
								
								if ( (totalColsPrecioModelos-6) > 0 ){
								
									//# LEER TODOS LOS DATOS DE AÑO Y FILA
									for( int pos=6; pos <= totalColsPrecioModelos - 1; pos++ ){
										
										HSSFCell cellPrecioModelo = hssfRow.getCell(pos);
										cellPrecioModelo.setCellType(Cell.CELL_TYPE_STRING);
										
										if ( this.isDouble(cellPrecioModelo.getStringCellValue()) ){
											datosModeloPrecio.put(aniosModelos.get(posAnio), Double.parseDouble(cellPrecioModelo.getStringCellValue()));
										}else{
											if ( !cellPrecioModelo.getStringCellValue().isEmpty() ){
												this.construyeListaErrores(fila, cellPrecioModelo.getStringCellValue(), "VALOR COMERCIAL: debe ser una cantidad valida: ");
											}
										}
										posAnio++;
									}
									
									valorComercialVehiculo.setDatosModeloPrecio(datosModeloPrecio);
								
								}else{
									this.construyeListaErrores(fila,"", "NO SE ENCONTRARON VALORES DE AÑO VÁLIDOS EN LA SECCIÓN DE MODELOS ");
									valorComercialVehiculo.setError(true);
								}
								
								break;
						}
					
					}else{
						celdaNoNula = true;
					}
					
				}
			}
			
			if ( fila > 1 ){
				
				if( !celdaNoNula ){
				
					// # VALIDAR DE FECHA NO MAYOR A 2 MESES
					mensaje = this.valorComercialVehiculoService.validarRegistro(valorComercialVehiculo,1);
					if( !mensaje.isEmpty() ){
						this.construyeListaErrores(fila, String.valueOf( valorComercialVehiculo.getValorComercialMes() ) , mensaje);
						valorComercialVehiculo.setError(true);
					}
					
					lValorComercialVehiculo.add(valorComercialVehiculo);
				
				}
				
			}
			
		}

		this.procesarPrecioModelo( lValorComercialVehiculo );
	}
	
	
	private void procesarPrecioModelo(List<ValorComercialVehiculo> lValorComercialVehiculo ){

		List<ValorComercialVehiculo> lValorComercialFinal = new ArrayList<ValorComercialVehiculo>();
		String mensaje = "";
		
		if( !lValorComercialVehiculo.isEmpty() ){
			
			for( ValorComercialVehiculo vcv : lValorComercialVehiculo ){
			
				// # VALIDAR EL CAMPO ERROR
				if ( !vcv.isError() ){
					
					// # ITERAR MAP
					for( Map.Entry<Integer,Double> dato : vcv.getDatosModeloPrecio().entrySet()  ){
						
						ValorComercialVehiculo valorComercialFinal = new ValorComercialVehiculo();
						
						valorComercialFinal.setDocumento             ( vcv.getDocumento() );
						valorComercialFinal.setEstatusRegistro       ( vcv.getEstatusRegistro() );
						valorComercialFinal.setFilaExcel             ( vcv.getFilaExcel() );
						valorComercialFinal.setError                 ( vcv.isError() );
						valorComercialFinal.setProveedorId           ( vcv.getProveedorId() );
						valorComercialFinal.setClaveAmis             ( vcv.getClaveAmis() );
						valorComercialFinal.setValorComercialMes     ( vcv.getValorComercialMes() );
						valorComercialFinal.setValorComercialAnio    ( vcv.getValorComercialAnio() );
						valorComercialFinal.setDescripcionVehiculo   ( vcv.getDescripcionVehiculo() );
						valorComercialFinal.setValorComercialNuevo   ( vcv.getValorComercialNuevo() );
						valorComercialFinal.setCodigoUsuarioCreacion ( vcv.getCodigoUsuarioCreacion() );
						valorComercialFinal.setModeloVehiculo        ( Integer.parseInt( dato.getKey().toString() ) );
						valorComercialFinal.setValorComercial        ( Double.valueOf( dato.getValue().toString() ) );
						
						// # VALIDAR AMIS,MES Y AÑO DUPLICADOS
						mensaje = this.valorComercialVehiculoService.validarRegistro(valorComercialFinal,2);
						
						if( !mensaje.contains("Registro ya existe") ){
							
							lValorComercialFinal.add(valorComercialFinal);
							
							if( mensaje.contains("monto de Suma Asegurada")){
								this.construyeListaErrores(valorComercialFinal.getFilaExcel(), String.valueOf( valorComercialFinal.getValorComercialMes() ) , mensaje);
							}
						}else{
							this.construyeListaErrores(valorComercialFinal.getFilaExcel(), String.valueOf( valorComercialFinal.getValorComercialMes() ) , mensaje);
						}						
						
					}
					
				}
			
			}
			
			// # PERSISTIR
			this.setCantidadDatosProcesados( lValorComercialFinal.size() );
			this.entidadService.saveAll(lValorComercialFinal);
		}

		if(this.listErrores.size() > 0){
			// # IMPRIME LOG DE ERRORES
			this.setHasLogErrors(true);
			this.creaLogErrores();
		}
		
	}
	
	
	private void construyeListaErrores(int fila, String contenidoColumna,String mensaje){
		this.filaError.add(fila);
		this.listErrores.add(
				this.crearError(BigDecimal.valueOf( fila ),contenidoColumna,mensaje,"")
		);
		
	}
	
	private int convertirMeses(String mes){
		
		int noMes = 0;
		
		mes = mes.trim().toLowerCase();
		
		if ( mes.equals("enero") ){
			noMes = 1;
		}else if ( mes.equals("febrero")){
			noMes = 2;
		}else if ( mes.equals("marzo")){
			noMes = 3;
		}else if ( mes.equals("abril")){
			noMes = 4;
		}else if ( mes.equals("mayo")){
			noMes = 5;
		}else if ( mes.equals("junio")){
			noMes = 6;
		}else if ( mes.equals("julio")){
			noMes = 7;
		}else if ( mes.equals("agosto")){
			noMes = 8;
		}else if ( mes.equals("septiembre")){
			noMes = 9;
		}else if ( mes.equals("octubre")){
			noMes = 10;
		}else if ( mes.equals("noviembre")){
			noMes = 11;
		}else if ( mes.equals("diciembre")){
			noMes = 12;
		}
		
		return noMes;
	}
	
	public FileInputStream descargaLogErrores(BigDecimal idControlArchivo) {
		try {
			ControlArchivoDN controlArchivoDN = ControlArchivoDN.getInstancia();
			controlArchivoDTO = controlArchivoDN.getPorId(idControlArchivo);

			File file = new File(uploadFolder + "Log"
					+ controlArchivoDTO.getIdToControlArchivo() + ".txt");

			return new FileInputStream(file);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	
	public ValorComercialVehiculoService getValorComercialVehiculoService() {
		return valorComercialVehiculoService;
	}

	public void setValorComercialVehiculoService(
			ValorComercialVehiculoService valorComercialVehiculoService) {
		this.valorComercialVehiculoService = valorComercialVehiculoService;
	}


	public boolean isHasLogErrors() {
		return hasLogErrors;
	}


	public void setHasLogErrors(boolean hasLogErrors) {
		this.hasLogErrors = hasLogErrors;
	}

	public EntidadService getEntidadService() {
		return entidadService;
	}

	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}

	public UsuarioService getUsuarioService() {
		return usuarioService;
	}

	public void setUsuarioService(UsuarioService usuarioService) {
		this.usuarioService = usuarioService;
	}

	public int getCantidadDatosProcesados() {
		return cantidadDatosProcesados;
	}

	public void setCantidadDatosProcesados(int cantidadDatosProcesados) {
		this.cantidadDatosProcesados = cantidadDatosProcesados;
	}

	@Override
	protected void buildDataSheet(HSSFSheet sheet) {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void buildValidationsSheet(HSSFSheet sheet) {
		// TODO Auto-generated method stub
		
	}

}
