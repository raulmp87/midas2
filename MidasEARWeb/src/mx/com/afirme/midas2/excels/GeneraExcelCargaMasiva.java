package mx.com.afirme.midas2.excels;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import mx.com.afirme.midas.catalogos.estilovehiculo.EstiloVehiculoDTO;
import mx.com.afirme.midas.catalogos.valorfijo.ValorFijoDN;
import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionId;
import mx.com.afirme.midas.endoso.EndosoDTO;
import mx.com.afirme.midas.interfaz.cliente.ClienteDTO;
import mx.com.afirme.midas.interfaz.cliente.ClienteFacadeRemote;
import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDTO;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.controlArchivo.ControlArchivoDN;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoDTO;
import mx.com.afirme.midas2.domain.condicionesespeciales.CondicionEspecial;
import mx.com.afirme.midas2.domain.condicionesespeciales.CondicionEspecial.NivelAplicacion;
import mx.com.afirme.midas2.domain.negocio.condicionespecial.NegocioCondicionEspecial;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.NegocioSeccion;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.paquete.NegocioPaqueteSeccion;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.paquete.cobertura.deducibles.NegocioDeducibleCob;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.tipouso.NegocioTipoUso;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.cargamasiva.CargaMasivaAutoCot;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.cargamasiva.DetalleCargaMasivaAutoCot;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.cargamasivaindividual.DetalleCargaMasivaIndAutoCot;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.inciso.IncisoAutoCot;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.inciso.configuracion.ConfiguracionDatoInciso;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.inciso.configuracion.DatoIncisoCotAuto;
import mx.com.afirme.midas2.dto.ControlDescripcionArchivoDTO;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.cargamasiva.LogErroresCargaMasivaDTO;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.catalogos.banco.BancoMidasService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.cargaMasiva.CargaMasivaService;
import mx.com.afirme.midas2.validators.NumSerieValidador;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.hssf.usermodel.DVConstraint;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFDataValidation;
import org.apache.poi.hssf.usermodel.HSSFName;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.DataFormat;
import org.apache.poi.ss.usermodel.DataValidationConstraint.ValidationType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.util.CellRangeAddressList;
import org.apache.poi.ss.util.CellReference;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.joda.time.DateTime;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("prototype")
public class GeneraExcelCargaMasiva extends GenerarExcelBase {

	private int maxColumns = 27;
	private CotizacionDTO cotizacion;
	private List<DetalleCargaMasivaAutoCot> detalleCargaMasivaAutoCotList = new ArrayList<DetalleCargaMasivaAutoCot>(1);
	private boolean hasLogErrors = false;
	private DetalleCargaMasivaAutoCot ejemplo = new DetalleCargaMasivaAutoCot();
	private List<IncisoCotizacionDTO> incisosCreadosList;
	List<NegocioCondicionEspecial> lNegocioCondicionEspecial;
	private ListadoService listadoService;
	private Map<String, String> bancos;
	private BancoMidasService bancoMidasService;
	private ClienteFacadeRemote clienteFacadeRemote;

	CreationHelper creationHelper = null;
	CellStyle dateStyleFV = null;
	DataFormat fmt = null;
	CellStyle cellStyle = null;
	
	// Campos en proceso
	private static final int CAMPO_LINEA_NEGOCIO = 0;
	private static final int CAMPO_TIPO_USO = 1;
	private static final int CAMPO_DESCRIPCION = 2;
	private static final int CAMPO_CLAVE_AMIS = 3;
	private static final int CAMPO_MODELO = 4;
	private static final int CAMPO_PAQUETE = 5;
	private static final int CAMPO_CODIGO_POSTAL = 6;
	private static final int CAMPO_DEDUCIBLE_DANOS_MATERIALES = 7;
	private static final int CAMPO_DEDUCIBLE_ROBO_TOTAL = 8;
	private static final int CAMPO_LIMITE_RC_TERCEROS = 9;
	private static final int CAMPO_LIMITE_GASTOS_MEDICOS = 10;
	private static final int CAMPO_ASISTENCIA_JURIDICA = 11;
	private static final int CAMPO_ASISTENCIA_EN_VIAJES = 12;
	private static final int CAMPO_EXENCION_DEDUCIBLE_DANOS = 13;
	private static final int CAMPO_LIMITE_ACCIDENTE_CONDUCTOR = 14;
	private static final int CAMPO_EXTENCION_RC = 15;
	private static final int CAMPO_LIMITE_ADAPTACION_CONVERSION = 16;
	private static final int CAMPO_LIMITE_EQUIPO_ESPECIAL = 17;
	private static final int CAMPO_DEDUCIBLE_EQUIPO_ESPECIAL = 18;
	private static final int CAMPO_EXENCION_DEDUCIBLE_ROBO = 19;
	private static final int CAMPO_RESPONSABILIDAD_CIVIL_USA = 20;
	private static final int CAMPO_IGUALACION = 21;
	private static final int CAMPO_NUMERO_REMOLQUES = 22;
	private static final int CAMPO_TIPO_CARGA = 23;
	private static final int CAMPO_LIMITE_MUERTE = 24;
	private static final int CAMPO_SUMA_ASEGURADA_DANOS_MATERIALES = 25;
	private static final int CAMPO_SUMA_ASEGURADA_ROBO_TOTAL = 26;
	private static final int CAMPO_DIAS_SALARIO_MINIMO = 27;
	
	private static final String NOMBRE_CAMPO_NUMERO_REMOLQUES = "NUMERO REMOLQUES";
	private static final String NOMBRE_CAMPO_TIPO_CARGA = "TIPO CARGA";
	

	public static final String NOMBRE_DEDUCIBLE_DANOS_MATERIALES = "DA\u00D1OS MATERIALES";
	public static final String NOMBRE_DEDUCIBLE_ROBO_TOTAL = "ROBO TOTAL";
	public static final String NOMBRE_LIMITE_RC_TERCEROS = "Responsabilidad Civil";
	public static final String NOMBRE_LIMITE_GASTOS_MEDICOS = "GASTOS M\u00C9DICOS";
	public static final String NOMBRE_ASISTENCIA_JURIDICA = "ASISTENCIA JUR\u00CDDICA";
	public static final String NOMBRE_ASISTENCIA_EN_VIAJES = "ASISTENCIA EN VIAJES Y VIAL KM";
	public static final String NOMBRE_EXENCION_DEDUCIBLE_DANOS = "EXENCI\u00D3N DE DEDUCIBLES DM";
	public static final String NOMBRE_LIMITE_ACCIDENTE_CONDUCTOR = "ACCIDENTES AUTOMOVIL\u00CDSTICOS AL CONDUCTOR";
	public static final String NOMBRE_EXTENCION_RC = "EXTENSI\u00D3N DE RESPONSABILIDAD CIVIL";
	public static final String NOMBRE_LIMITE_ADAPTACION_CONVERSION = "ADAPTACIONES Y CONVERSIONES";
	public static final String NOMBRE_LIMITE_EQUIPO_ESPECIAL = "EQUIPO ESPECIAL";
	public static final String NOMBRE_DEDUCIBLE_EQUIPO_ESPECIAL = "EQUIPO ESPECIAL";
	public static final String NOMBRE_EXENCION_DEDUCIBLE_ROBO = "EXENCI\u00D3N DE DEDUCIBLES RT";
	public static final String NOMBRE_RESPONSABILIDAD_CIVIL_USA = "Responsabilidad Civil en USA Y CANADA LUC";
	//public static final String NOMBRE_LIMITE_MUERTE = "Responsabilidad Civil por Muerte de Trabajadores Dom\u00E9sticos";
	public static final String NOMBRE_LIMITE_RC_VIAJERO = "Responsabilidad Civil Viajero";
	public static final String NOMBRE_LIMITE_MUERTE = "RESPONSABILIDAD CIVIL EN EXCESO POR MUERTE";
	private static final String PLANTILLA = "/mx/com/afirme/midas2/impresiones/jrxml/plantilla.xls";
	public static final String VALOR_SI = "SI";
	public static final String VALOR_NO = "NO";
	
	public static final String VALOR_VISA = "VISA";
	public static final String VALOR_MASTERCARD = "MASTERD CARD";
	public static final String VALOR_TC= "TARJETA DE CREDITO";
	public static final String VALOR_DOMICILIACION= "COBRANZA DOMICILIADA";
	public static final String VALOR_EFECTIVO= "EFECTIVO";
	public static final String VALOR_CUENTAAFIRME= "CUENTA AFIRME (CREDITO O DEBITO)";
	
	private static final String TIPO_CUENTA_DEBITO = "DEBITO";
	private static final String TIPO_CUENTA_CUENTA_CLABE = "CUENTA CLABE";
	private static final String TIPO_CUENTA_CUENTA_BANCARIA = "CUENTA BANCARIA";

	// Campos terminados Plantilla Complemento
	private static final int CAMPO_NUMERO_INCISO = 0;
	private static final int CAMPO_DESCRIPCION_VEHICULO = 1;
	private static final int CAMPO_ASEGURADO = 2;
	private static final int CAMPO_NOMBRE = 3;
	private static final int CAMPO_APELLIDO_PATERNO = 4;
	private static final int CAMPO_APELLIDO_MATERNO = 5;
	private static final int CAMPO_NUMERO_LICENCIA = 6;
	private static final int CAMPO_FECHA_NACIMIENTO = 7;
	private static final int CAMPO_OCUPACION = 8;
	private static final int CAMPO_NUMERO_MOTOR = 9;
	private static final int CAMPO_NUMERO_SERIE = 10;
	private static final int CAMPO_NUMERO_PLACA = 11;
	private static final int CAMPO_REPUVE = 12;
	private static final int CAMPO_DESCRIPCION_EQUIPO_ESPECIAL = 13;
	private static final int CAMPO_DESCRIPCION_ADAPTACION_CONVERSION = 14;
	//private static final int CAMPO_DESCRIPCION_DANOS = 15;
	private static final int CAMPO_OBSERVACIONES_INCISO = 15;
	private static final int CAMPO_EMAIL = 16;
	private static final int CAMPO_CONDICION_ESPECIAL_ALTA = 17;
	private static final int CAMPO_CONDICION_ESPECIAL_BAJA = 18;
	private static final int CAMPO_ERROR_NUMERO_SERIE = 19;
	private static final int CAMPO_CONDUCTO_COBRO = 20;
	private static final int CAMPO_INSTITUCION_BANCARIA = 21;
	private static final int CAMPO_TIPO_TARJETA= 22;
	private static final int CAMPO_NUMERO_TARJETACLABE = 23;
	private static final int CAMPO_CODIGO_SEGURIDAD = 24;
	private static final int CAMPO_FECHA_VENCIMIENTO = 25;

	private static final String NOMBRE_CAMPO_OBSERVACIONES_INCISO = "OBSERVACIONES";

	public static final Short TIPO_CARGA_NORMAL = 1;
	public static final Short TIPO_CARGA_SCHEDULE = 2;
	
	private static final String MEDIO_PAGO_TC = "4";
	private static final String MEDIO_PAGO_DOMICILIACION = "8";
	private static final String MEDIO_PAGO_EFECTIVO = "15";
	private static final String MEDIO_PAGO_CUENTA_AFIRME= "1386";
	
	/**
	 * El nombre de la lista de valores utilizado par las validaciones del campo TIPO CARGA.
	 */
	private static final String CAMPO_TIPO_CARGA_NOMBRE_RANGO = "TP_TIPO_CARGA";

	@SuppressWarnings("deprecation")
	public GeneraExcelCargaMasiva(CargaMasivaService cargaMasivaService) {
		
		this.setCargaMasivaService(cargaMasivaService);
		//this.setNegocioCondicionEspecialService(this.negocioCondicionEspecialService);
		
		//uploadFolder = cargaMasivaService.getUploadFolder();
		try {
			String OSName = System.getProperty("os.name");
			if (OSName.toLowerCase().indexOf("windows")!=-1)
				this.uploadFolder = Sistema.UPLOAD_FOLDER;
			else
				this.uploadFolder = Sistema.LINUX_UPLOAD_FOLDER;
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	public InputStream generaPlantillaCargaMasiva(CotizacionDTO cotizacion)
			throws IOException {
		HSSFWorkbook workbook = null;
		HSSFSheet validaciones = null;
		HSSFSheet datos = null;
		//FileOutputStream fos = null;
		//BufferedOutputStream bos = null;
		ByteArrayOutputStream bos = null;

		this.cotizacion = cotizacion;

		// System.out.println("Maximo de lineas segun: " +
		// SpreadsheetVersion.EXCEL97.getMaxRows());

		try {
			// Obtiene plantilla con macros
			workbook = this.cargaPlantilla(PLANTILLA);
			validaciones = workbook.getSheetAt(0);
			datos = workbook.getSheetAt(1);
			
			buildDataSheet(datos);
			datos.protectSheet("midas");

			buildValidationsSheet(validaciones);

			// Ajusta tamano de columnas
			for (int i = 0; i <= maxColumns; i++) {
				if (i < 6) {
					validaciones.setColumnWidth(i, 9000);
				} else {
					validaciones.autoSizeColumn(i);
				}
			}

			// Oculta hoja de datos
			workbook.setSheetHidden(1, true);
			// Muestra primera hoja
			workbook.setActiveSheet(0);
			short firstCell = 0;
			validaciones.showInPane(firstCell, firstCell);
			
			bos = new ByteArrayOutputStream();

			workbook.write(bos);

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (bos != null) {
				bos.close();
			}
		}

		//File file = new File(uploadFolder + "CargaMasivaCot"+ cotizacion.getIdToCotizacion() + ".xls");
		if(bos == null){
			return null;
		}
		return new ByteArrayInputStream(bos.toByteArray());
	}

	/**
	 * Metodo invocado en el boton "procesar ahora" en Emision-Autos-Cotizaciones-Cotizacion-CotizacionFormal.
	 * Obtiene el segundo excel una vez que la primera plantilla se ha subido
	 * */
	public InputStream obtienePlantillaCargaMasivaCompl(CotizacionDTO cotizacion, ListadoService listadoService)
			throws IOException {
		HSSFWorkbook workbook = null;
		HSSFSheet validaciones = null,condicionesEspeciales = null;
		ByteArrayOutputStream bos = null;
	
		this.cotizacion = cotizacion;
		this.listadoService = listadoService;
		
		try {
			// Obtiene plantilla con macros
			workbook = cargaPlantillaComp();
			validaciones = workbook.getSheetAt(0);
			creationHelper = workbook.getCreationHelper();
			dateStyleFV = workbook.createCellStyle();
			dateStyleFV.setDataFormat(creationHelper.createDataFormat().getFormat("mm/yy"));
			fmt = workbook.createDataFormat();
			cellStyle = workbook.createCellStyle();
			cellStyle.setDataFormat(fmt.getFormat("@"));

			buildValidationsComp(validaciones);

			// Ajusta tamano de columnas
			for (int i = 0; i <= maxColumns; i++) {
				validaciones.autoSizeColumn(i);
			}
			
			// Crea pestana de condiciones especiales
			workbook.createSheet("Condiciones Especiales");
			condicionesEspeciales = workbook.getSheetAt(1);
			this.createCondicionesSheet(condicionesEspeciales, BigDecimal.valueOf( cotizacion.getNegocioDerechoPoliza().getNegocio().getIdToNegocio() ) );

			// Muestra primera hoja
			workbook.setActiveSheet(0);
			short firstCell = 0;
			validaciones.showInPane(firstCell, firstCell);

			bos = new ByteArrayOutputStream();

			workbook.write(bos);

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (bos != null) {
				bos.close();
			}
		}

		//File file = new File(uploadFolder + "CargaMasivaCompCot" + cotizacion.getIdToCotizacion() + ".xls");
		if(bos == null){
			return null;
		}
		return new ByteArrayInputStream(bos.toByteArray());
	}

	public HSSFWorkbook cargaPlantillaComp() throws IOException {
		HSSFWorkbook workbook = null;
		POIFSFileSystem plantilla = new POIFSFileSystem(
				this.getClass()
						.getResourceAsStream(
								"/mx/com/afirme/midas2/impresiones/jrxml/plantillaCargaComp.xls"));
		workbook = new HSSFWorkbook(plantilla);
		return workbook;
	}

	@Override
	protected void buildValidationsSheet(HSSFSheet sheet) {

		HSSFRow row = sheet.createRow(0);
		HSSFCell cell = row.createCell(CAMPO_LINEA_NEGOCIO);
		cell.setCellValue("LINEA DE NEGOCIO");
		cell = row.createCell(CAMPO_TIPO_USO);
		cell.setCellValue("TIPO USO");
		cell = row.createCell(CAMPO_DESCRIPCION);
		cell.setCellValue("DESCRIPCION");
		cell = row.createCell(CAMPO_CLAVE_AMIS);
		cell.setCellValue("CLAVE AMIS");
		cell = row.createCell(CAMPO_MODELO);
		cell.setCellValue("MODELO");
		cell = row.createCell(CAMPO_PAQUETE);
		cell.setCellValue("PAQUETE");
		cell = row.createCell(CAMPO_CODIGO_POSTAL);
		cell.setCellValue("CODIGO POSTAL");
		cell = row.createCell(CAMPO_DEDUCIBLE_DANOS_MATERIALES);
		cell.setCellValue("DEDUCIBLE DA\u00D1OS MATERIALES (%)");
		cell = row.createCell(CAMPO_DEDUCIBLE_ROBO_TOTAL);
		cell.setCellValue("DEDUCIBLE ROBO TOTAL (%)");
		cell = row.createCell(CAMPO_LIMITE_RC_TERCEROS);
		cell.setCellValue("L\u00CDMITE RC TERCEROS ($)");
		cell = row.createCell(CAMPO_LIMITE_GASTOS_MEDICOS);
		cell.setCellValue("L\u00CDMITE GASTOS MEDICOS ($)");
		cell = row.createCell(CAMPO_ASISTENCIA_JURIDICA);
		cell.setCellValue("ASISTENCIA JURIDICA (SI \u00D3 NO)");
		cell = row.createCell(CAMPO_ASISTENCIA_EN_VIAJES);
		cell.setCellValue("ASISTENCIA EN VIAJES Y VIAL KM 0 (SI \u00D3 NO)");
		cell = row.createCell(CAMPO_EXENCION_DEDUCIBLE_DANOS);
		cell.setCellValue("EXENCI\u00D3N DEDUCIBLE DA\u00D1OS MATERIALES (SI \u00D3 NO)");
		cell = row.createCell(CAMPO_LIMITE_ACCIDENTE_CONDUCTOR);
		cell.setCellValue("L\u00CDMITE ACCIDENTE CONDUCTOR ($)");
		cell = row.createCell(CAMPO_EXTENCION_RC);
		cell.setCellValue("EXTENCI\u00D3N RC (SI \u00D3 NO)");
		cell = row.createCell(CAMPO_LIMITE_ADAPTACION_CONVERSION);
		cell.setCellValue("L\u00CDMITE ADAPTACI\u00D3N Y CONVERSI\u00D3N  ($)");
		cell = row.createCell(CAMPO_LIMITE_EQUIPO_ESPECIAL);
		cell.setCellValue("L\u00CDMITE EQUIPO ESPECIAL ($)");
		cell = row.createCell(CAMPO_DEDUCIBLE_EQUIPO_ESPECIAL);
		cell.setCellValue("DEDUCIBLE EQUIPO ESPECIAL  (%)");
		cell = row.createCell(CAMPO_EXENCION_DEDUCIBLE_ROBO);
		cell.setCellValue("EXENCI\u00D3N DEDUCIBLE ROBO TOTAL (SI \u00D3 NO)");
		cell = row.createCell(CAMPO_RESPONSABILIDAD_CIVIL_USA);
		cell.setCellValue("RESPONSABILIDAD CIVIL USA Y CANADA (SI \u00D3 NO)");
		cell = row.createCell(CAMPO_IGUALACION);
		cell.setCellValue("IGUALACION");
		cell = row.createCell(CAMPO_NUMERO_REMOLQUES);
		cell.setCellValue(NOMBRE_CAMPO_NUMERO_REMOLQUES);
		cell = row.createCell(CAMPO_TIPO_CARGA);
		cell.setCellValue(NOMBRE_CAMPO_TIPO_CARGA);
		cell = row.createCell(CAMPO_LIMITE_MUERTE);
		cell.setCellValue("LIMITE MUERTE");
		cell = row.createCell(CAMPO_SUMA_ASEGURADA_DANOS_MATERIALES);
		cell.setCellValue("SUMA ASEGURADA DA\u00D1OS MATERIALES");
		cell = row.createCell(CAMPO_SUMA_ASEGURADA_ROBO_TOTAL);
		cell.setCellValue("SUMA ASEGURADA ROBO TOTAL");		
		cell = row.createCell(CAMPO_DIAS_SALARIO_MINIMO);
		cell.setCellValue("DIAS SALARIO MINIMO");				
		
		llenaLineaEjemplo(sheet);

		DVConstraint dvConstraint = DVConstraint
				.createFormulaListConstraint("NEGOCIOSECCION");
		HSSFDataValidation data_validation = new HSSFDataValidation(
				new CellRangeAddressList(1, maxRows, CAMPO_LINEA_NEGOCIO,
						CAMPO_LINEA_NEGOCIO), dvConstraint);
		data_validation.setSuppressDropDownArrow(false);
		sheet.addValidationData(data_validation);

		dvConstraint = DVConstraint.createNumericConstraint(
				ValidationType.INTEGER, DVConstraint.OperatorType.GREATER_THAN,
				"0", "0");
		data_validation = new HSSFDataValidation(new CellRangeAddressList(1,
				maxRows, CAMPO_MODELO, CAMPO_MODELO), dvConstraint);
		sheet.addValidationData(data_validation);

		DVConstraint dvConstraintDecimal = DVConstraint
				.createNumericConstraint(ValidationType.DECIMAL,
						DVConstraint.OperatorType.GREATER_THAN, "0", "0");
		
		data_validation = new HSSFDataValidation(new CellRangeAddressList(1,
				maxRows, CAMPO_DEDUCIBLE_DANOS_MATERIALES,
				CAMPO_LIMITE_GASTOS_MEDICOS), dvConstraintDecimal);
		sheet.addValidationData(data_validation);

		DVConstraint dvConstraintSINO = DVConstraint
				.createExplicitListConstraint(new String[] { VALOR_SI, VALOR_NO });
		data_validation = new HSSFDataValidation(new CellRangeAddressList(1,
				maxRows, CAMPO_ASISTENCIA_JURIDICA,
				CAMPO_EXENCION_DEDUCIBLE_DANOS), dvConstraintSINO);
		data_validation.setSuppressDropDownArrow(false);
		sheet.addValidationData(data_validation);

		data_validation = new HSSFDataValidation(new CellRangeAddressList(1,
				maxRows, CAMPO_LIMITE_ACCIDENTE_CONDUCTOR,
				CAMPO_LIMITE_ACCIDENTE_CONDUCTOR), dvConstraintDecimal);
		sheet.addValidationData(data_validation);

		data_validation = new HSSFDataValidation(new CellRangeAddressList(1,
				maxRows, CAMPO_EXTENCION_RC, CAMPO_EXTENCION_RC),
				dvConstraintSINO);
		data_validation.setSuppressDropDownArrow(false);
		sheet.addValidationData(data_validation);

		data_validation = new HSSFDataValidation(new CellRangeAddressList(1,
				maxRows, CAMPO_LIMITE_ADAPTACION_CONVERSION,
				CAMPO_DEDUCIBLE_EQUIPO_ESPECIAL), dvConstraintDecimal);
		sheet.addValidationData(data_validation);

		data_validation = new HSSFDataValidation(new CellRangeAddressList(1,
				maxRows, CAMPO_EXENCION_DEDUCIBLE_ROBO,
				CAMPO_RESPONSABILIDAD_CIVIL_USA), dvConstraintSINO);
		data_validation.setSuppressDropDownArrow(false);
		sheet.addValidationData(data_validation);
		
		data_validation = new HSSFDataValidation(new CellRangeAddressList(1,
				maxRows, CAMPO_IGUALACION,
				CAMPO_IGUALACION), dvConstraintDecimal);
		sheet.addValidationData(data_validation);

		for (int i = 2; i < maxRows; i++) {

			dvConstraint = DVConstraint
					.createFormulaListConstraint("INDIRECT(UPPER(\"TP_\"&$A$"
							+ i + "))");
			data_validation = new HSSFDataValidation(new CellRangeAddressList(
					1, (i - 1), CAMPO_TIPO_USO, CAMPO_TIPO_USO), dvConstraint);
			sheet.addValidationData(data_validation);

			dvConstraint = DVConstraint
					.createFormulaListConstraint("INDIRECT(UPPER(\"P_\"&$A$"
							+ i + "))");
			data_validation = new HSSFDataValidation(new CellRangeAddressList(
					1, (i - 1), CAMPO_PAQUETE, CAMPO_PAQUETE), dvConstraint);
			sheet.addValidationData(data_validation);
		}
		
		DVConstraint dvConstraint0_9 = DVConstraint
		.createNumericConstraint(ValidationType.INTEGER,
				DVConstraint.OperatorType.BETWEEN, "0", "9");
		
		data_validation = new HSSFDataValidation(new CellRangeAddressList(1,
				maxRows, CAMPO_NUMERO_REMOLQUES,
				CAMPO_NUMERO_REMOLQUES), dvConstraint0_9);
		sheet.addValidationData(data_validation);
		
		
		dvConstraint = DVConstraint
				.createFormulaListConstraint(CAMPO_TIPO_CARGA_NOMBRE_RANGO);
		data_validation = new HSSFDataValidation(
				new CellRangeAddressList(1, maxRows, CAMPO_TIPO_CARGA,
						CAMPO_TIPO_CARGA), dvConstraint);
		data_validation.setSuppressDropDownArrow(false);
		sheet.addValidationData(data_validation);
		
		data_validation = new HSSFDataValidation(new CellRangeAddressList(1,
				maxRows, CAMPO_LIMITE_MUERTE,
				CAMPO_LIMITE_MUERTE), dvConstraintDecimal);
		sheet.addValidationData(data_validation);
	}
	
	private void llenaLineaEjemplo(HSSFSheet sheet){
		
		HSSFRow row = sheet.createRow(1);
		HSSFCell cell = row.createCell(CAMPO_LINEA_NEGOCIO);
		cell.setCellValue(ejemplo.getLineaNegocioDescripcion());
		cell = row.createCell(CAMPO_TIPO_USO);
		cell.setCellValue(ejemplo.getTipoUsoDescripcion());
		cell = row.createCell(CAMPO_DESCRIPCION);
		cell.setCellValue("");
		cell = row.createCell(CAMPO_CLAVE_AMIS);
		cell.setCellValue("99996");
		cell = row.createCell(CAMPO_MODELO);
		cell.setCellValue("2000");
		cell = row.createCell(CAMPO_PAQUETE);
		cell.setCellValue(ejemplo.getPaqueteDescripcion());
		cell = row.createCell(CAMPO_CODIGO_POSTAL);
		cell.setCellValue("67234");
		cell = row.createCell(CAMPO_DEDUCIBLE_DANOS_MATERIALES);
		cell.setCellValue("5");
		cell = row.createCell(CAMPO_DEDUCIBLE_ROBO_TOTAL);
		cell.setCellValue("5");
		cell = row.createCell(CAMPO_LIMITE_RC_TERCEROS);
		cell.setCellValue("10000");
		cell = row.createCell(CAMPO_LIMITE_GASTOS_MEDICOS);
		cell.setCellValue("40000");
		cell = row.createCell(CAMPO_ASISTENCIA_JURIDICA);
		cell.setCellValue("SI");
		cell = row.createCell(CAMPO_ASISTENCIA_EN_VIAJES);
		cell.setCellValue("NO");
		cell = row.createCell(CAMPO_EXENCION_DEDUCIBLE_DANOS);
		cell.setCellValue("NO");
		cell = row.createCell(CAMPO_LIMITE_ACCIDENTE_CONDUCTOR);
		cell.setCellValue("150000");
		cell = row.createCell(CAMPO_EXTENCION_RC);
		cell.setCellValue("SI");
		cell = row.createCell(CAMPO_LIMITE_ADAPTACION_CONVERSION);
		cell.setCellValue("100000");
		cell = row.createCell(CAMPO_LIMITE_EQUIPO_ESPECIAL);
		cell.setCellValue("60000");
		cell = row.createCell(CAMPO_DEDUCIBLE_EQUIPO_ESPECIAL);
		cell.setCellValue("25");
		cell = row.createCell(CAMPO_EXENCION_DEDUCIBLE_ROBO);
		cell.setCellValue("SI");
		cell = row.createCell(CAMPO_RESPONSABILIDAD_CIVIL_USA);
		cell.setCellValue("NO");
		cell = row.createCell(CAMPO_IGUALACION);
		cell.setCellValue("");
		cell = row.createCell(CAMPO_NUMERO_REMOLQUES);
		cell.setCellValue("");
		cell = row.createCell(CAMPO_TIPO_CARGA, Cell.CELL_TYPE_NUMERIC);
		cell.setCellValue("");
		cell = row.createCell(CAMPO_LIMITE_MUERTE);
		cell.setCellValue("100000");
		cell = row.createCell(CAMPO_DIAS_SALARIO_MINIMO);
		cell.setCellValue("5000");		
	}

	private void buildValidationsComp(HSSFSheet sheet) {
		HSSFRow row = sheet.createRow(0);
		HSSFCell cell = row.createCell(CAMPO_NUMERO_INCISO);
		cell.setCellValue("NUMERO INCISO");
		cell = row.createCell(CAMPO_DESCRIPCION_VEHICULO);
		cell.setCellValue("DESCRIPCION VEHICULO");
		cell = row.createCell(CAMPO_ASEGURADO);
		cell.setCellValue("ASEGURADO");
		cell = row.createCell(CAMPO_NOMBRE);
		cell.setCellValue("NOMBRE");
		cell = row.createCell(CAMPO_APELLIDO_PATERNO);
		cell.setCellValue("APELLIDO PATERNO");
		cell = row.createCell(CAMPO_APELLIDO_MATERNO);
		cell.setCellValue("APELLIDO MATERNO");
		cell = row.createCell(CAMPO_NUMERO_LICENCIA, Cell.CELL_TYPE_STRING);
		cell.setCellValue("NUMERO DE LICENCIA");
		cell = row.createCell(CAMPO_FECHA_NACIMIENTO);
		cell.setCellValue("FECHA DE NACIMIENTO");
		cell = row.createCell(CAMPO_OCUPACION);
		cell.setCellValue("OCUPACION");
		cell = row.createCell(CAMPO_NUMERO_MOTOR, Cell.CELL_TYPE_STRING);
		cell.setCellValue("NUMERO DE MOTOR");
		cell = row.createCell(CAMPO_NUMERO_SERIE, Cell.CELL_TYPE_STRING);
		cell.setCellValue("NUMERO DE SERIE");
		cell = row.createCell(CAMPO_NUMERO_PLACA, Cell.CELL_TYPE_STRING);
		cell.setCellValue("NUMERO DE PLACA");
		cell = row.createCell(CAMPO_REPUVE, Cell.CELL_TYPE_STRING);
		cell.setCellValue("REPUVE");
		cell = row.createCell(CAMPO_DESCRIPCION_EQUIPO_ESPECIAL, Cell.CELL_TYPE_STRING);
		cell.setCellValue("DESCRIPCION EQUIPO ESPECIAL");
		cell = row.createCell(CAMPO_DESCRIPCION_ADAPTACION_CONVERSION, Cell.CELL_TYPE_STRING);
		cell.setCellValue("DESCRIPCION ADAPTACION Y CONVERSION");
		cell = row.createCell(CAMPO_OBSERVACIONES_INCISO, Cell.CELL_TYPE_STRING);
		cell.setCellValue(NOMBRE_CAMPO_OBSERVACIONES_INCISO);
		cell = row.createCell(CAMPO_EMAIL, Cell.CELL_TYPE_STRING);
		cell.setCellValue("EMAIL CONTACTO");
		cell = row.createCell(CAMPO_CONDICION_ESPECIAL_ALTA, Cell.CELL_TYPE_STRING);
		cell.setCellValue("ALTA CONDICION ESPECIAL");
		cell = row.createCell(CAMPO_CONDICION_ESPECIAL_BAJA, Cell.CELL_TYPE_STRING);
		cell.setCellValue("BAJA CONDICION ESPECIAL");
		
		cell = row.createCell(CAMPO_CONDUCTO_COBRO);
		cell.setCellValue("CONDUCTO COBRO");
		cell = row.createCell(CAMPO_INSTITUCION_BANCARIA);
		cell.setCellValue("INSTITUCION BANCARIA");
		cell = row.createCell(CAMPO_TIPO_TARJETA);
		cell.setCellValue("TIPO DE TARJETA");
		cell = row.createCell(CAMPO_NUMERO_TARJETACLABE);
		cell.setCellValue("NUMERO DE TARJETA O CLABE");
		cell.setCellStyle(cellStyle);
		cell = row.createCell(CAMPO_CODIGO_SEGURIDAD);
		cell.setCellValue("CODIGO DE SEGURIDAD");
		cell.setCellStyle(cellStyle);
		cell = row.createCell(CAMPO_FECHA_VENCIMIENTO);
		cell.setCellValue("FECHA VENCIMIENTO");
		cell.setCellStyle(dateStyleFV);
		
		llenaLineaEjemploComp(sheet);
		
		DVConstraint dvConstraint = DVConstraint.createNumericConstraint(
				ValidationType.INTEGER, DVConstraint.OperatorType.GREATER_THAN,
				"0", "0");
		HSSFDataValidation data_validation = new HSSFDataValidation(
				new CellRangeAddressList(1, maxRows, CAMPO_NUMERO_INCISO,
						CAMPO_NUMERO_INCISO), dvConstraint);
		sheet.addValidationData(data_validation);

		DVConstraint dvConstraint2 = DVConstraint.createDateConstraint(DVConstraint.OperatorType.GREATER_THAN,
				"01/01/1900", "01/01/1900", "dd/MM/yyyy");
		data_validation = new HSSFDataValidation(new CellRangeAddressList(1,
				maxRows, CAMPO_FECHA_NACIMIENTO, CAMPO_FECHA_NACIMIENTO),
				dvConstraint2);
		sheet.addValidationData(data_validation);
		

		DVConstraint dvConstraintTipoConductoCobro = DVConstraint
				.createExplicitListConstraint(new String[] { VALOR_TC, VALOR_DOMICILIACION, VALOR_EFECTIVO, VALOR_CUENTAAFIRME });
		data_validation = new HSSFDataValidation(new CellRangeAddressList(1,
				maxRows, CAMPO_CONDUCTO_COBRO,
				CAMPO_CONDUCTO_COBRO), dvConstraintTipoConductoCobro);
		data_validation.setSuppressDropDownArrow(false);
		sheet.addValidationData(data_validation);

		bancos = listadoService.getBancos();
		String[] bancosArray = bancos.values().toArray(new String[0]);
		Arrays.sort(bancosArray);
		
		DVConstraint dvConstraintInsBan = DVConstraint
				.createExplicitListConstraint(bancosArray);
		data_validation = new HSSFDataValidation(new CellRangeAddressList(1,
				maxRows, CAMPO_INSTITUCION_BANCARIA,
				CAMPO_INSTITUCION_BANCARIA), dvConstraintInsBan);
		data_validation.setSuppressDropDownArrow(false);
		sheet.addValidationData(data_validation);
		
		DVConstraint dvConstraintTipoTC = DVConstraint
				.createExplicitListConstraint(new String[] { VALOR_VISA, VALOR_MASTERCARD });
		data_validation = new HSSFDataValidation(new CellRangeAddressList(1,
				maxRows, CAMPO_TIPO_TARJETA,
				CAMPO_TIPO_TARJETA), dvConstraintTipoTC);
		data_validation.setSuppressDropDownArrow(false);
		sheet.addValidationData(data_validation);
		
	}
	
	private void llenaLineaEjemploComp(HSSFSheet sheet){
		
		
		List<IncisoCotizacionDTO> incisos = cargaMasivaService.obtieneIncisos(cotizacion.getIdToCotizacion());
		int fila = 1;
		for(IncisoCotizacionDTO inciso : incisos){
			HSSFRow row = sheet.createRow(fila);
			HSSFCell cell = row.createCell(CAMPO_NUMERO_INCISO);
			cell.setCellValue(inciso.getNumeroSecuencia());
			
			cell = row.createCell(CAMPO_DESCRIPCION_VEHICULO);
			if(inciso.getIncisoAutoCot().getDescripcionFinal() != null){
				cell.setCellValue(inciso.getIncisoAutoCot().getDescripcionFinal());
			}else{
				cell.setCellValue("");
			}
			
			cell = row.createCell(CAMPO_ASEGURADO);
			if(inciso.getIncisoAutoCot().getNombreAseguradoUpper() != null){
				cell.setCellValue(inciso.getIncisoAutoCot().getNombreAseguradoUpper());
			}else{
				cell.setCellValue("");
			}
			cell = row.createCell(CAMPO_NOMBRE);
			if(inciso.getIncisoAutoCot().getNombreConductor() != null){
				cell.setCellValue(inciso.getIncisoAutoCot().getNombreConductor());
			}else{
				cell.setCellValue("");
			}
			cell = row.createCell(CAMPO_APELLIDO_PATERNO);
			if(inciso.getIncisoAutoCot().getPaternoConductor() != null){
				cell.setCellValue(inciso.getIncisoAutoCot().getPaternoConductor());
			}else{
				cell.setCellValue("");
			}
			cell = row.createCell(CAMPO_APELLIDO_MATERNO);
			if(inciso.getIncisoAutoCot().getMaternoConductor() != null){
				cell.setCellValue(inciso.getIncisoAutoCot().getMaternoConductor());
			}else{
				cell.setCellValue("");
			}
			cell = row.createCell(CAMPO_NUMERO_LICENCIA, Cell.CELL_TYPE_STRING);
			if(inciso.getIncisoAutoCot().getNumeroLicencia() != null){
				cell.setCellValue(inciso.getIncisoAutoCot().getNumeroLicencia());
			}else{
				cell.setCellValue("");
			}
			cell = row.createCell(CAMPO_FECHA_NACIMIENTO);
			if(inciso.getIncisoAutoCot().getFechaNacConductor() != null){
				cell.setCellValue(inciso.getIncisoAutoCot().getFechaNacConductor());
			}else{
				cell.setCellValue("");
			}
			cell = row.createCell(CAMPO_OCUPACION, Cell.CELL_TYPE_STRING);
			if(inciso.getIncisoAutoCot().getOcupacionConductor() != null){
				cell.setCellValue(inciso.getIncisoAutoCot().getOcupacionConductor());
			}else{
				cell.setCellValue("");
			}
			cell = row.createCell(CAMPO_NUMERO_MOTOR, Cell.CELL_TYPE_STRING);
			if(inciso.getIncisoAutoCot().getNumeroMotor() != null){
				cell.setCellValue(inciso.getIncisoAutoCot().getNumeroMotor());
			}else{
				cell.setCellValue("");
			}
			cell = row.createCell(CAMPO_NUMERO_SERIE, Cell.CELL_TYPE_STRING);
			if(inciso.getIncisoAutoCot().getNumeroSerie() != null){
				cell.setCellValue(inciso.getIncisoAutoCot().getNumeroSerie());
			}else{
				cell.setCellValue("");
			}
			cell = row.createCell(CAMPO_NUMERO_PLACA, Cell.CELL_TYPE_STRING);
			if(inciso.getIncisoAutoCot().getPlaca() != null){
				cell.setCellValue(inciso.getIncisoAutoCot().getPlaca());
			}else{
				cell.setCellValue("");
			}
			cell = row.createCell(CAMPO_REPUVE, Cell.CELL_TYPE_STRING);
			if(inciso.getIncisoAutoCot().getRepuve() != null){
				cell.setCellValue(inciso.getIncisoAutoCot().getRepuve());
			}else{
				cell.setCellValue("");
			}
			
			cell = row.createCell(CAMPO_OBSERVACIONES_INCISO, Cell.CELL_TYPE_STRING);
			if(inciso.getIncisoAutoCot().getObservacionesinciso() != null){
				cell.setCellValue(inciso.getIncisoAutoCot().getObservacionesinciso());
			}else{
				cell.setCellValue("");
			}
			
			// Condiciones Especiales
			cell = row.createCell(CAMPO_CONDICION_ESPECIAL_ALTA, Cell.CELL_TYPE_STRING);
			cell.setCellValue("");
			cell = row.createCell(CAMPO_CONDICION_ESPECIAL_BAJA, Cell.CELL_TYPE_STRING);
			cell.setCellValue("");
			
			//Valida datos de Riesgo
			List<CoberturaCotizacionDTO> coberturas = cargaMasivaService.obtieneCoberturasContratadas(inciso);
			DatoIncisoCotAuto datoIncisoCotAutoEquipoEspecial = null;
			DatoIncisoCotAuto datoIncisoCotAutoAdaptacionConversion = null;
			for(CoberturaCotizacionDTO coberturaCotizacion : coberturas){
				if(coberturaCotizacion.getCoberturaSeccionDTO().getCoberturaDTO().getNombreComercial().toUpperCase().equals(
						 NOMBRE_LIMITE_EQUIPO_ESPECIAL.toUpperCase())){
					datoIncisoCotAutoEquipoEspecial = cargaMasivaService.obtieneDatoIncisoCotAutoDelInciso(cotizacion, coberturaCotizacion, inciso.getId().getNumeroInciso());
					if(datoIncisoCotAutoEquipoEspecial != null){
						cell = row.createCell(CAMPO_DESCRIPCION_EQUIPO_ESPECIAL, Cell.CELL_TYPE_STRING);
						cell.setCellValue(datoIncisoCotAutoEquipoEspecial.getValor());
					}
				}
				if(coberturaCotizacion.getCoberturaSeccionDTO().getCoberturaDTO().getNombreComercial().toUpperCase().equals(
						 NOMBRE_LIMITE_ADAPTACION_CONVERSION.toUpperCase())){
					datoIncisoCotAutoAdaptacionConversion = cargaMasivaService.obtieneDatoIncisoCotAutoDelInciso(cotizacion, coberturaCotizacion, inciso.getId().getNumeroInciso());
					if(datoIncisoCotAutoAdaptacionConversion != null){
						cell = row.createCell(CAMPO_DESCRIPCION_ADAPTACION_CONVERSION, Cell.CELL_TYPE_STRING);
						cell.setCellValue(datoIncisoCotAutoAdaptacionConversion.getValor());	
					}
				}
				
			}
			
			cell = row.createCell(CAMPO_CONDUCTO_COBRO);
			cell.setCellValue("EFECTIVO");
			cell = row.createCell(CAMPO_INSTITUCION_BANCARIA);
			cell.setCellValue("");
			cell = row.createCell(CAMPO_TIPO_TARJETA);
			cell.setCellValue("");
			cell = row.createCell(CAMPO_NUMERO_TARJETACLABE);
			cell.setCellValue("");
			cell.setCellStyle(cellStyle);
			cell = row.createCell(CAMPO_CODIGO_SEGURIDAD);
			cell.setCellValue("");
			cell.setCellStyle(cellStyle);
			cell = row.createCell(CAMPO_FECHA_VENCIMIENTO);
			cell.setCellValue("");
			cell.setCellStyle(dateStyleFV);
			fila++;
		}
	}

	@Override
	protected void buildDataSheet(HSSFSheet sheet) {
		armaLineasNegocio(sheet);
		armaTipoCarga(sheet);
	}

	private void armaLineasNegocio(HSSFSheet sheet) {
		HSSFRow row = null;
		HSSFCell cell = null;
		HSSFName name = null;
		List<NegocioSeccion> negocioSeccionList = cargaMasivaService.getSeccionListByCotizacion(cotizacion);
		if (negocioSeccionList != null && !negocioSeccionList.isEmpty()) {
			row = sheet.createRow(dataRows);
			dataRows++;
			int linea = dataRows;
			int column = 0;
			boolean isFirst = true;
			for (NegocioSeccion item : negocioSeccionList) {
				cell = row.createCell(column);
				cell.setCellValue(remplazaEspacios(item.getSeccionDTO()
						.getDescripcion()));
				column++;
				
				if(isFirst){					
					ejemplo.setLineaNegocioDescripcion(cell.toString());
				}
				armaTipoUso(sheet, item, isFirst);
				armaPaquete(sheet, item, isFirst);				
				isFirst = false;
			}
			CellReference cellRef = new CellReference(dataRows, (column - 1));

			name = sheet.getWorkbook().createName();
			name.setNameName("NEGOCIOSECCION");
			String rango = "$A$" + linea + ":$" + cellRef.getCellRefParts()[2]
					+ "$" + linea;
			name.setRefersToFormula("'DataCombo'!" + rango + "");

		}
	}

	private void armaTipoCarga(HSSFSheet sheet) {
		HSSFRow row = null;
		HSSFCell cell = null;
		HSSFName name = null;

		List<CatalogoValorFijoDTO> tipoCargas = cargaMasivaService.getTipoCargas();
		if (!tipoCargas.isEmpty()) {
			row = sheet.createRow(dataRows);
			dataRows++;
			int column = 0;
			int linea = dataRows;

			for (CatalogoValorFijoDTO item2 : tipoCargas) {
				cell = row.createCell(column);
				cell.setCellValue(item2.getDescripcion());
				
				column++;
			}
			CellReference cellRef = new CellReference(dataRows, (column - 1));

			name = sheet.getWorkbook().createName();
			name.setNameName(CAMPO_TIPO_CARGA_NOMBRE_RANGO);

			String rango = "$A$" + linea + ":$" + cellRef.getCellRefParts()[2]
					+ "$" + linea;
			name.setRefersToFormula("'DataCombo'!" + rango + "");
		}
	}

	private void armaTipoUso(HSSFSheet sheet, NegocioSeccion negocioSeccion, boolean isFirst) {
		HSSFRow row = null;
		HSSFCell cell = null;
		HSSFName name = null;

		SeccionDTO seccion = cargaMasivaService.getSeccionDTOById(negocioSeccion);
		List<NegocioTipoUso> negocioTipoUsoList = cargaMasivaService.getNegocioTipoUsoListByNegSeccion(negocioSeccion);
		if (negocioTipoUsoList != null && !negocioTipoUsoList.isEmpty()) {
			row = sheet.createRow(dataRows);
			dataRows++;
			int column = 0;
			int linea = dataRows;

			for (NegocioTipoUso item2 : negocioTipoUsoList) {
				cell = row.createCell(column);
				cell.setCellValue(item2.getTipoUsoVehiculoDTO()
						.getDescripcionTipoUsoVehiculo());
				
				if(isFirst){
					isFirst = false;
					ejemplo.setTipoUsoDescripcion(cell.toString());
				}
				
				column++;
			}
			CellReference cellRef = new CellReference(dataRows, (column - 1));

			name = sheet.getWorkbook().createName();
			String nombre = "TP_" + seccion.getDescripcion();
			name.setNameName(remplazaEspacios(nombre).toUpperCase());

			String rango = "$A$" + linea + ":$" + cellRef.getCellRefParts()[2]
					+ "$" + linea;
			name.setRefersToFormula("'DataCombo'!" + rango + "");

		}
	}

	private void armaPaquete(HSSFSheet sheet, NegocioSeccion negocioSeccion, boolean isFirst) {
		HSSFRow row = null;
		HSSFCell cell = null;
		HSSFName name = null;

		SeccionDTO seccion = cargaMasivaService.getSeccionDTOById(negocioSeccion);
		List<NegocioPaqueteSeccion> negocioPaqueteSeccionList = cargaMasivaService.getNegocioPaqueteSeccionByNegSeccion(negocioSeccion);
		if (negocioPaqueteSeccionList != null
				&& !negocioPaqueteSeccionList.isEmpty()) {
			row = sheet.createRow(dataRows);
			dataRows++;
			int column = 0;
			int linea = dataRows;

			for (NegocioPaqueteSeccion item2 : negocioPaqueteSeccionList) {
				cell = row.createCell(column);
				cell.setCellValue(item2.getPaquete().getDescripcion());
				column++;
				
				if(isFirst){
					isFirst = false;
					ejemplo.setPaqueteDescripcion(cell.toString());
				}
			}
			CellReference cellRef = new CellReference(dataRows, (column - 1));

			name = sheet.getWorkbook().createName();
			String nombre = "P_" + seccion.getDescripcion();
			name.setNameName(remplazaEspacios(nombre).toUpperCase());

			String rango = "$A$" + linea + ":$" + cellRef.getCellRefParts()[2]
					+ "$" + linea;
			name.setRefersToFormula("'DataCombo'!" + rango + "");

		}
	}

	public void validaCargaMasiva(CotizacionDTO cotizacion,
			BigDecimal idControlArchivo, Short tipoCarga) {

		try {

			this.cotizacion = cotizacion;
			this.tipoCarga = tipoCarga;

			ControlArchivoDN controlArchivoDN = ControlArchivoDN.getInstancia();
			controlArchivoDTO = controlArchivoDN.getPorId(idControlArchivo);
			String fileName = this.obtenerNombreArchivo(controlArchivoDTO);

			HSSFWorkbook workbook = this.cargaArchivodeCarga(uploadFolder
					+ fileName);
			HSSFSheet validaciones = workbook.getSheetAt(0);
			leeLineasArchivoCarga(validaciones);

		} catch (IOException e) {
			e.printStackTrace();
		} catch (SystemException e) {
			e.printStackTrace();
		}
	}
	
	public void validaCargaMasivaComplemento(CotizacionDTO cotizacion ,BigDecimal idControlArchivo, Short tipoCarga, BancoMidasService bancoMidasService, ClienteFacadeRemote clienteFacadeRemote, ListadoService listadoService) {
		try {

			this.cotizacion = cotizacion;
			this.tipoCarga = tipoCarga;
			this.bancoMidasService=bancoMidasService;
			this.clienteFacadeRemote=clienteFacadeRemote;
			this.listadoService = listadoService;

			ControlArchivoDN controlArchivoDN = ControlArchivoDN.getInstancia();
			controlArchivoDTO = controlArchivoDN.getPorId(idControlArchivo);
			String fileName = this.obtenerNombreArchivo(controlArchivoDTO);

			HSSFWorkbook workbook = this.cargaArchivodeCarga(uploadFolder
					+ fileName);
			HSSFSheet validaciones = workbook.getSheetAt(0);
			leeLineasArchivoCarga(validaciones);

		} catch (IOException e) {
			e.printStackTrace();
		} catch (SystemException e) {
			e.printStackTrace();
		}
		
	}

	@Override
	protected void leeLineasArchivoCarga(HSSFSheet sheet) {
		Iterator<Row> rowIterator = sheet.rowIterator();
		boolean isFirst = true;
		
		// # PRECARGA CONDICIONES POR NEGOCIO
		this.lNegocioCondicionEspecial = negocioCondicionEspecialService.obtenerCondicionesNegocio(cotizacion.getNegocioDerechoPoliza().getNegocio().getIdToNegocio());
		
		while (rowIterator.hasNext()) {
			HSSFRow hssfRow = (HSSFRow) rowIterator.next();
			if (!isFirst) {
				if (cotizacion.getClaveEstatus().equals(
						CotizacionDTO.ESTATUS_COT_EN_PROCESO)) {
					DetalleCargaMasivaAutoCot detalle = creaLineaDetalleCarga(hssfRow);
					if(detalle != null){
						this.getDetalleCargaMasivaAutoCotList().add(detalle);
					}else{
						DetalleCargaMasivaAutoCot detalleVacio = new DetalleCargaMasivaAutoCot();
						this.getDetalleCargaMasivaAutoCotList().add(detalleVacio);
					}
				}
				if (cotizacion.getClaveEstatus().equals(
						CotizacionDTO.ESTATUS_COT_TERMINADA)) {
					DetalleCargaMasivaAutoCot detalle = creaLineaDetalleCargaComp(hssfRow);
					if(detalle != null){
						this.getDetalleCargaMasivaAutoCotList().add(detalle);
					}
				}
			} else {
				isFirst = false;
			}
		}

		//Solo se valida la informacion de pago cuando la cotizacion este marcada como terminada
		//Por que es la que carga la informacion de pago
		if (cotizacion.getClaveEstatus().equals(CotizacionDTO.ESTATUS_COT_TERMINADA)) {
			this.validaInformacionPago(getDetalleCargaMasivaAutoCotList());
		}
		
		// Valida datos para incisos
		if (this.getListErrores().isEmpty()) {
			if (cotizacion.getClaveEstatus().equals(CotizacionDTO.ESTATUS_COT_EN_PROCESO)
				&& this.tipoCarga.equals(TIPO_CARGA_NORMAL)) {
				this.validaDetalleCargaMasiva(getDetalleCargaMasivaAutoCotList());
				try{
					cargaMasivaService.calcularTodosLosIncisos(cotizacion);
					this.validaIgualacionInciso(getDetalleCargaMasivaAutoCotList());
					cargaMasivaService.calculaCotizacion(cotizacion);
				}catch(Exception e){
					e.printStackTrace();
				}
			}
			if (cotizacion.getClaveEstatus().equals(CotizacionDTO.ESTATUS_COT_TERMINADA)
				&& this.tipoCarga.equals(TIPO_CARGA_NORMAL)) {
				try{
					bancoMidasService.encriptarCuentasBancariasCargaMasiva(getDetalleCargaMasivaAutoCotList());
					this.validaDetalleCargaMasivaComp(getDetalleCargaMasivaAutoCotList(), sheet);
				}catch(Exception e){
					e.printStackTrace();
				}
			}
		}
		if (this.getListErrores().isEmpty()) {
			setHasLogErrors(false);
		} else {
			creaLogErrores();
			setHasLogErrors(true);
		}
	}
	
	private void validaInformacionPago(List<DetalleCargaMasivaAutoCot> detalleCargaMasivaAutoCotList) {

		int fila = 1;
		for (DetalleCargaMasivaAutoCot detalle : detalleCargaMasivaAutoCotList) {
			String tipoCuenta = detalle.getTipoTarjeta();

			if (!detalle.getConductoCobro().equals(VALOR_EFECTIVO)) {
				if(detalle.getIdMedioPago().toString().equals(MEDIO_PAGO_DOMICILIACION)||detalle.getIdMedioPago().toString().equals(MEDIO_PAGO_CUENTA_AFIRME)){
					if (detalle.getNumeroTarjetaClave().length()==16){
						tipoCuenta = TIPO_CUENTA_DEBITO;
					}else if(detalle.getNumeroTarjetaClave().length()==18){
						tipoCuenta = TIPO_CUENTA_CUENTA_CLABE;
					}else{
						tipoCuenta = TIPO_CUENTA_CUENTA_BANCARIA;
					}
				}
				
				detalle.setTipoTarjeta(tipoCuenta);
				
				if(!tipoCuenta.equals(TIPO_CUENTA_CUENTA_BANCARIA)){
					if (detalle.getInstitucionBancaria() == null) {
						LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
						error.setNumeroInciso(new BigDecimal(fila));
						error.setNombreCampoError("INSTITUCION BANCARIA");
						error.setNombreSeccion("INSTITUCION BANCARIA");
						error.setRecomendaciones("No debe estar vacio INSTITUCION BANCARIA cuando el tipo de cobro es Tarjeta de credito o domiciliacion");
						listErrores.add(error);
					}
					if (detalle.getTipoTarjeta() == null) {
						LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
						error.setNumeroInciso(new BigDecimal(fila));
						error.setNombreSeccion("TIPO TARJETA");
						error.setNombreCampoError("TIPO TARJETA");
						error.setRecomendaciones("No debe estar vacio TIPO TARJETA cuando el tipo de cobro es Tarjeta de credito o domiciliacion");
						listErrores.add(error);
					}
					String numTarjeta = detalle.getNumeroTarjetaClave();
					if (numTarjeta == null || !(numTarjeta.length() == 16 || numTarjeta.length() == 18)) {
						LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
						error.setNumeroInciso(new BigDecimal(fila));
						error.setNombreSeccion("NUMERO TARJETA / CLABE");
						error.setNombreCampoError("NUMERO TARJETA / CLABE");
						error.setRecomendaciones("Numero de Tarjeta debe tener 16 digitos. CLABE debe tener 18 digitos");
						listErrores.add(error);
					}
					
					if(!tipoCuenta.equals(TIPO_CUENTA_CUENTA_CLABE)){
						
						String cvv = detalle.getCodigoSeguridad();
						if (cvv == null || !(cvv.length() == 3 || cvv.length() == 4)) {
							LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
							error.setNumeroInciso(new BigDecimal(fila));
							error.setNombreSeccion("CODIGO DE SEGURIDAD");
							error.setNombreCampoError("CODIGO DE SEGURIDAD");
							error.setRecomendaciones("El codigo de seguridad debe tener 3 o 4 digitos");
							listErrores.add(error);
						}
						String vigenciaTarjeta = detalle.getFechaVencimiento();
						if (vigenciaTarjeta == null || !validaVigenciaTarjeta(vigenciaTarjeta)) {
							LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
							error.setNumeroInciso(new BigDecimal(fila));
							error.setNombreSeccion("FECHA DE VENCIMIENTO");
							error.setNombreCampoError("FECHA DE VENCIMIENTO");
							error.setRecomendaciones("Fecha de Vencimiento no valida. Usar el formato MM/yy y una tarjeta vigente");
							listErrores.add(error);
						}
					}
				}
			}else{
				detalle.setInstitucionBancaria("");
				detalle.setNumeroTarjetaClave("");
				detalle.setCodigoSeguridad("");
				detalle.setFechaVencimiento("");
			}
			fila++;
		}

	}

	
	private void validaIgualacionInciso(List<DetalleCargaMasivaAutoCot> detalleCargaMasivaList) {

			if(incisosCreadosList != null && !incisosCreadosList.isEmpty()){
				for (DetalleCargaMasivaAutoCot detalle : detalleCargaMasivaList) {
					if (detalle.getClaveEstatus() != null && !detalle.getClaveEstatus().equals(
						DetalleCargaMasivaAutoCot.ESTATUS_ERROR)) {
						if(detalle.getIgualacion() != null && detalle.getIgualacion() > 0){
							cargaMasivaService.igualacionInciso(cotizacion.getIdToCotizacion(), 
									detalle.getNumeroInciso(), detalle.getIgualacion());
						}
					}
				}
			}
	}

	/** Valida que los datos obligatorio no sean nulos
	 * @param hssfRow
	 * @return
	 */
	private DetalleCargaMasivaAutoCot creaLineaDetalleCarga(HSSFRow hssfRow) {

		DetalleCargaMasivaAutoCot detalle = new DetalleCargaMasivaAutoCot();

		int fila = hssfRow.getRowNum() + 1;
		StringBuilder mensajeError = new StringBuilder("");
		String lineaDeNegocio = "";
		boolean lineaVacia = true;
		for (int i = 0; i <= maxColumns; i++) {
			HSSFCell cell = hssfRow.getCell(i);
			LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
			switch (i) {
			case CAMPO_LINEA_NEGOCIO:
				// Linea Negocio Obligatorio
				if (cell != null && !cell.toString().isEmpty()) {
					detalle.setLineaNegocioDescripcion(cell.toString());
					lineaDeNegocio = cell.toString();
					lineaVacia = false;
				}else{
					error.setNumeroInciso(new BigDecimal(fila));
					error.setNombreSeccion(lineaDeNegocio);
					error.setNombreCampoError("LINEA DE NEGOCIO");
					error.setRecomendaciones("Seleccione un campo del combo");
					listErrores.add(error);
					mensajeError.append("Campo: LINEA DE NEGOCIO -  Seleccione un campo del combo ").append("\r\n").append( System.getProperty("line.separator"));
				}
				break;
			case CAMPO_TIPO_USO:
				// Tipo Uso Obligatorio
				if (cell != null && !cell.toString().isEmpty()) {
					detalle.setTipoUsoDescripcion(cell.toString());
					lineaVacia = false;
				}else{
					error.setNumeroInciso(new BigDecimal(fila));
					error.setNombreSeccion(lineaDeNegocio);
					error.setNombreCampoError("TIPO DE USO");
					error.setRecomendaciones("Seleccione un campo del combo");
					listErrores.add(error);
					mensajeError.append("Campo: TIPO DE USO -  Seleccione un campo del combo ").append("\r\n" ).append(System.getProperty("line.separator"));
				}
				break;
			case CAMPO_DESCRIPCION:
				// Descripcion
				if (cell != null && !cell.toString().isEmpty()) {
					detalle.setDescripcion(cell.toString().toUpperCase());
					lineaVacia = false;
				}
				break;
			case CAMPO_CLAVE_AMIS:
				// Clave AMIS Obligatorio
				if (cell != null && !cell.toString().isEmpty()) {
					String valorStr = "";
					if(cell.getCellType() == HSSFCell.CELL_TYPE_NUMERIC){
						double valor = cell.getNumericCellValue();
						BigDecimal valorB = new BigDecimal(valor);
						valorStr = valorB.toPlainString();
					}else if(cell.getCellType() == HSSFCell.CELL_TYPE_STRING){
						valorStr = cell.getStringCellValue();
					}else{
						valorStr = cell.toString();
					}
					if (valorStr.indexOf(".") != -1) {
						valorStr = valorStr.substring(0, valorStr.indexOf("."));
					}
					detalle.setClaveAMIS(valorStr);
					lineaVacia = false;
				} else {
					error.setNumeroInciso(new BigDecimal(fila));
					error.setNombreSeccion(lineaDeNegocio);
					error.setNombreCampoError("CLAVE AMIS");
					error.setRecomendaciones("Ingrese la Clave AMIS");
					listErrores.add(error);
					mensajeError.append("Campo: CLAVE AMIS -  Ingrese la Clave AMIS ").append("\r\n" ).append(System.getProperty("line.separator"));
				}
				break;
			case CAMPO_MODELO:
				// Modelo Obligatorio
				if (cell != null && !cell.toString().isEmpty()) {
					String valor = cell.toString();
					if (valor.indexOf(".") != -1) {
						valor = valor.substring(0, valor.indexOf("."));
					}
					try{
						detalle.setModeloVehiculo(new Short(valor));
						lineaVacia = false;
					}catch(Exception e){
						error.setNumeroInciso(new BigDecimal(fila));
						error.setNombreSeccion(lineaDeNegocio);
						error.setNombreCampoError("MODELO");
						error.setRecomendaciones("Modelo del Vehiculo Invalido");
						listErrores.add(error);
						mensajeError.append("Campo: MODELO -  Modelo del Vehiculo Invalido ").append("\r\n" ).append(System.getProperty("line.separator"));						
					}
				}else{
					error.setNumeroInciso(new BigDecimal(fila));
					error.setNombreSeccion(lineaDeNegocio);
					error.setNombreCampoError("MODELO");
					error.setRecomendaciones("Ingrese el modelo del Vehiculo");
					listErrores.add(error);
					mensajeError.append("Campo: MODELO -  Ingrese el modelo del Vehiculo ").append("\r\n" ).append(System.getProperty("line.separator"));
				}
				break;
			case CAMPO_PAQUETE:
				// Paquete Obligatorio
				if (cell != null && !cell.toString().isEmpty()) {
					detalle.setPaqueteDescripcion(cell.toString());
					lineaVacia = false;
				}else{
					error.setNumeroInciso(new BigDecimal(fila));
					error.setNombreSeccion(lineaDeNegocio);
					error.setNombreCampoError("PAQUETE");
					error.setRecomendaciones("Seleccione un campo del combo");
					listErrores.add(error);
					mensajeError.append("Campo: PAQUETE -  Seleccione un campo del combo ").append("\r\n" ).append(System.getProperty("line.separator"));
				}
				break;
			case CAMPO_CODIGO_POSTAL:
				// Codigo Postal Obligatorio
				if (cell != null && !cell.toString().isEmpty()) {
					String valor = cell.toString();
					if (valor.indexOf(".") != -1) {
						valor = valor.substring(0, valor.indexOf("."));
					}
					detalle.setCodigoPostal(valor);
					lineaVacia = false;
				}else {
					error.setNumeroInciso(new BigDecimal(fila));
					error.setNombreSeccion(lineaDeNegocio);
					error.setNombreCampoError("CODIGO POSTAL");
					error.setRecomendaciones("Ingrese el Codigo Postal");
					listErrores.add(error);
					mensajeError.append("Campo: CODIGO POSTAL -  Ingrese el Codigo Postal ").append("\r\n" ).append(System.getProperty("line.separator"));
				}
				break;
			case CAMPO_DEDUCIBLE_DANOS_MATERIALES:
				if (cell != null && !cell.toString().isEmpty()) {
					try{
						detalle.setDeducibleDanosMateriales(new Double(cell.toString()));
						lineaVacia = false;
					}catch(Exception e){
					}
				}
				break;
			case CAMPO_DEDUCIBLE_ROBO_TOTAL:
				if (cell != null && !cell.toString().isEmpty()) {
					try{
						detalle.setDeducibleRoboTotal(new Double(cell.toString()));
						lineaVacia = false;
					}catch(Exception e){
					}
				}
				break;
			case CAMPO_LIMITE_RC_TERCEROS:
				if (cell != null && !cell.toString().isEmpty()) {
					try{
						detalle.setLimiteRcTerceros(new Double(cell.toString()));
						lineaVacia = false;
					}catch(Exception e){	
					}
				}
				break;
			case CAMPO_LIMITE_GASTOS_MEDICOS:
				if (cell != null && !cell.toString().isEmpty()) {
					try{
						detalle.setLimiteGastosMedicos(new Double(cell.toString()));
						lineaVacia = false;
					}catch(Exception e){	
					}
				}
				break;
			case CAMPO_ASISTENCIA_JURIDICA:
				// ASISTENCIA JURIDICA (Si O No)
				if (cell != null && !cell.toString().isEmpty()) {
					detalle.setAsistenciaJuridica(validaSINO(cell.toString()));
					lineaVacia = false;
				}
				break;
			case CAMPO_ASISTENCIA_EN_VIAJES:
				// ASISTENCIA EN VIAJES Y VIAL KM 0 (Si O No)
				if (cell != null && !cell.toString().isEmpty()) {
					detalle.setAsistenciaViajes(validaSINO(cell.toString()));
					lineaVacia = false;
				}
				break;
			case CAMPO_EXENCION_DEDUCIBLE_DANOS:
				// EXENCION DEDUCIBLE DANOS MATERIALES (Si O No)
				if (cell != null && !cell.toString().isEmpty()) {
					detalle.setExencionDeducibleDanosMateriales(validaSINO(cell.toString()));
					lineaVacia = false;
				}
				break;
			case CAMPO_LIMITE_ACCIDENTE_CONDUCTOR:
				// LIMITE ACCIDENTE CONDUCTOR ($)
				if (cell != null && !cell.toString().isEmpty()) {
					try{
						detalle.setLimiteAccidenteConductor(new Double(cell.toString()));
						lineaVacia = false;
					}catch(Exception e){
					}
				}
				break;
			case CAMPO_EXTENCION_RC:
				// EXTENCION RC (Si O No)
				if (cell != null && !cell.toString().isEmpty()) {
					detalle.setExencionRC(validaSINO(cell.toString()));
					lineaVacia = false;
				}
				break;
			case CAMPO_LIMITE_ADAPTACION_CONVERSION:
				// LIMITE ADAPTACION Y CONVERSION ($)
				if (cell != null && !cell.toString().isEmpty()) {
					try{
						detalle.setLimiteAdaptacionConversion(new Double(cell.toString()));
						lineaVacia = false;
					}catch(Exception e){
					}
				}
				break;
			case CAMPO_LIMITE_EQUIPO_ESPECIAL:
				// LIMITE EQUIPO ESPECIAL ($)
				if (cell != null && !cell.toString().isEmpty()) {
					try{
						detalle.setLimiteEquipoEspecial(new Double(cell.toString()));
						lineaVacia = false;
					}catch(Exception e){
					}
				}
				break;
			case CAMPO_DEDUCIBLE_EQUIPO_ESPECIAL:
				// DEDUCIBLE EQUIPO ESPECIAL (%)
				if (cell != null && !cell.toString().isEmpty()) {
					try{
						detalle.setDeducibleEquipoEspecial(new Double(cell.toString()));
						lineaVacia = false;
					}catch(Exception e){
					}
				}
				break;
			case CAMPO_EXENCION_DEDUCIBLE_ROBO:
				// EXENCION DEDUCIBLE ROBO TOTAL (Si O No)
				if (cell != null && !cell.toString().isEmpty()) {
					detalle.setExencionDeducibleRoboTotal(validaSINO(cell.toString()));
					lineaVacia = false;
				}
				break;
			case CAMPO_RESPONSABILIDAD_CIVIL_USA:
				// RESPONSABILIDAD CIVIL USA Y CANADA (Si O No)
				if (cell != null && !cell.toString().isEmpty()) {
					detalle.setResponsabilidadCivilUsaCanada(validaSINO(cell.toString()));
					lineaVacia = false;
				}
				break;
			case CAMPO_IGUALACION:
				if (cell != null && !cell.toString().isEmpty()) {
					try{
						detalle.setIgualacion(new Double(cell.toString()));
						lineaVacia = false;
					}catch(Exception e){
					}
				}
				break;
			case CAMPO_NUMERO_REMOLQUES:
				if (cell != null && !cell.toString().isEmpty()) {
					try{
						detalle.setNumeroRemolques(Double.valueOf(cell.toString()).intValue());
						lineaVacia = false;
					}catch(Exception e){
					}
				}
				break;
			case CAMPO_TIPO_CARGA:
				if (cell != null && !cell.toString().isEmpty()) {
					try{
						String tipoCargaDescripcion = cell.toString();
						detalle.setTipoCargaDescripcion(tipoCargaDescripcion);
						if (!StringUtils.isBlank(tipoCargaDescripcion)) {
							CatalogoValorFijoDTO tipoCarga = cargaMasivaService.obtieneTipoCargaPorDescripcion(tipoCargaDescripcion);
							detalle.setTipoCarga(tipoCarga);
						}
						lineaVacia = false;
					}catch(Exception e){
					}
				}
				break;
			case CAMPO_LIMITE_MUERTE:
				if (cell != null && !cell.toString().isEmpty()) {
					try{
						detalle.setLimiteMuerte(new Double(cell.toString()));
						lineaVacia = false;
					}catch(Exception e){	
					}
				}
				break;
			case CAMPO_SUMA_ASEGURADA_DANOS_MATERIALES:
				if (cell != null && !cell.toString().isEmpty()) {
					try{
						detalle.setValorSumaAseguradaDM(new Double(cell.toString()));
						lineaVacia = false;
					}catch(Exception e){
					}
				}
				break;
			case CAMPO_SUMA_ASEGURADA_ROBO_TOTAL:
				if (cell != null && !cell.toString().isEmpty()) {
					try{
						detalle.setValorSumaAseguradaRT(new Double(cell.toString()));
						lineaVacia = false;
					}catch(Exception e){
					}
				}
				break;
			case CAMPO_DIAS_SALARIO_MINIMO:
				if (cell != null && !cell.toString().isEmpty()) {
					try{
						detalle.setDiasSalarioMinimo(new Double(cell.toString()));
						lineaVacia = false;
					}catch(Exception e){
					}
				}
				break;				
			}
			
		}
		detalle.setMensajeError(mensajeError.toString());
		if (mensajeError.toString().isEmpty()) {
			detalle.setClaveEstatus(DetalleCargaMasivaAutoCot.ESTATUS_PENDIENTE);
		} else {
			detalle.setClaveEstatus(DetalleCargaMasivaAutoCot.ESTATUS_ERROR);
		}
		if(lineaVacia && !mensajeError.toString().isEmpty()){
			Iterator<LogErroresCargaMasivaDTO> it = listErrores.iterator();
			while(it.hasNext()){
				LogErroresCargaMasivaDTO error = it.next();
				if(error.getNumeroInciso().equals(new BigDecimal(fila))){
					it.remove();
				}
			}
			return null;
		}

		return detalle;
	}
		
	private DetalleCargaMasivaAutoCot creaLineaDetalleCargaComp(HSSFRow hssfRow) {

		Boolean datosConductorObligatorios = false;
		DetalleCargaMasivaAutoCot detalle = new DetalleCargaMasivaAutoCot();
		List<String> altas = new ArrayList<String>(),bajas = new ArrayList<String>();
		String[] sAltas, sBajas;
		
		int fila = hssfRow.getRowNum();
		StringBuilder mensajeError = new StringBuilder("");
		String lineaDeNegocio = "";
		boolean lineaVacia = true;
		for (int i = 0; i <= maxColumns; i++) {
			HSSFCell cell = hssfRow.getCell(i);
			LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
			switch (i) {
			case CAMPO_NUMERO_INCISO:
				if (cell != null && !cell.toString().isEmpty()) {
					try{
					detalle.setNumeroInciso(new BigDecimal(cell.toString()));
					lineaVacia = false;
					IncisoCotizacionDTO inciso = cargaMasivaService.obtieneIncisoCotizacion(cotizacion, detalle.getNumeroInciso());
						if(inciso != null){
							detalle.setIncisoCotizacion(inciso);
							detalle.setDescripcion(inciso.getIncisoAutoCot().getDescripcionFinal());
							try{
								NegocioSeccion negocioSeccion = cargaMasivaService.getNegocioSeccionById(new BigDecimal(inciso.getIncisoAutoCot().getNegocioSeccionId()));
								detalle.setLineaNegocioDescripcion(negocioSeccion.getSeccionDTO().getDescripcion());						
								datosConductorObligatorios = cargaMasivaService.datosDelConductorRequeridos(inciso);
							}catch(Exception e){
								e.printStackTrace();
							}
						}else{
							error.setNumeroInciso(new BigDecimal(fila));
							error.setNombreSeccion(lineaDeNegocio);
							error.setNombreCampoError("NUMERO INCISO");
							error.setRecomendaciones("Ingrese un Numero de Inciso");
							listErrores.add(error);
							mensajeError.append("Campo: NUMERO INCISO -  Ingrese un Numero de Inciso " ).append("\r\n" ).append(System.getProperty("line.separator"));						
						}
					}catch(Exception e){
						e.printStackTrace();
						error.setNumeroInciso(new BigDecimal(fila));
						error.setNombreSeccion(lineaDeNegocio);
						error.setNombreCampoError("NUMERO INCISO");
						error.setRecomendaciones("Ingrese un Numero de Inciso");
						listErrores.add(error);
						mensajeError.append("Campo: NUMERO INCISO -  Ingrese un Numero de Inciso ").append("\r\n" ).append(System.getProperty("line.separator"));
					}
				}else{
					error.setNumeroInciso(new BigDecimal(fila));
					error.setNombreSeccion(lineaDeNegocio);
					error.setNombreCampoError("NUMERO INCISO");
					error.setRecomendaciones("Ingrese un Numero de Inciso");
					listErrores.add(error);
					mensajeError.append("Campo: NUMERO INCISO -  Ingrese un Numero de Inciso ").append("\r\n" ).append(System.getProperty("line.separator"));
				}
				break;
			case CAMPO_ASEGURADO:
				if (cell != null && !cell.toString().isEmpty()) {
					detalle.setAsegurado(cell.toString().toUpperCase());
					lineaVacia = false;
				}
				break;
			case CAMPO_NOMBRE:
				if (cell != null && !cell.toString().isEmpty()) {
					detalle.setNombre(cell.toString().toUpperCase());
					lineaVacia = false;
				} else {
					if(datosConductorObligatorios){
					error.setNumeroInciso(new BigDecimal(fila));
					error.setNombreSeccion(lineaDeNegocio);
					error.setNombreCampoError("NOMBRE");
					error.setRecomendaciones("Ingrese un Nombre");
					listErrores.add(error);
					mensajeError.append("Campo: NOMBRE -  Ingrese un Nombre ").append("\r\n" ).append(System.getProperty("line.separator"));
					}
				}
				break;
			case CAMPO_APELLIDO_PATERNO:
				if (cell != null && !cell.toString().isEmpty()) {
					detalle.setApellidoPaterno(cell.toString().toUpperCase());
					lineaVacia = false;
				} else {
					if(datosConductorObligatorios){
					error.setNumeroInciso(new BigDecimal(fila));
					error.setNombreSeccion(lineaDeNegocio);
					error.setNombreCampoError("APELLIDO PATERNO");
					error.setRecomendaciones("Ingrese el Apellido Paterno");
					listErrores.add(error);
					mensajeError.append("Campo: APELLIDO PATERNO -  Ingrese el Apellido Paterno ").append("\r\n" ).append(System.getProperty("line.separator"));
					}
				}
				break;
			case CAMPO_APELLIDO_MATERNO:
				if (cell != null && !cell.toString().isEmpty()) {
					detalle.setApellidoMaterno(cell.toString().toUpperCase());
					lineaVacia = false;
				}else{
					if(datosConductorObligatorios){
					error.setNumeroInciso(new BigDecimal(fila));
					error.setNombreSeccion(lineaDeNegocio);
					error.setNombreCampoError("APELLIDO MATERNO");
					error.setRecomendaciones("Ingrese el Apellido Materno");
					listErrores.add(error);
					mensajeError.append("Campo: APELLIDO MATERNO -  Ingrese el Apellido Materno ").append("\r\n" ).append(System.getProperty("line.separator"));
					}
				}
				break;
			case CAMPO_NUMERO_LICENCIA:
				if (cell != null && !cell.toString().isEmpty()) {
					detalle.setNumeroLicencia(cell.toString().toUpperCase());
					lineaVacia = false;
				}else{
					if(datosConductorObligatorios){
						error.setNumeroInciso(new BigDecimal(fila));
						error.setNombreSeccion(lineaDeNegocio);
						error.setNombreCampoError("NUMERO LICENCIA");
						error.setRecomendaciones("Ingrese el Numero de Licencia");
						listErrores.add(error);
						mensajeError.append("Campo: ").append(error.getNombreCampoError()).append(" - ").append(error.getRecomendaciones()).append("").append("\r\n").append(System.getProperty("line.separator"));
					}					
				}
				break;
			case CAMPO_FECHA_NACIMIENTO:
				if (cell != null && !cell.toString().isEmpty()) {
					try {						
						detalle.setFechaNacimiento(cell.getDateCellValue());
						lineaVacia = false;
					} catch (Exception e) {
						if(datosConductorObligatorios){
							error.setNumeroInciso(new BigDecimal(fila));
							error.setNombreSeccion(lineaDeNegocio);
							error.setNombreCampoError("FECHA DE NACIMIENTO");
							error.setRecomendaciones("Fecha de Nacimiento Invalida");
							listErrores.add(error);
							mensajeError.append("Campo: FECHA DE NACIMIENTO -  Fecha de Nacimiento Invalida ").append("\r\n" ).append(System.getProperty("line.separator"));
						}
					}
				} else {
					if(datosConductorObligatorios){
					error.setNumeroInciso(new BigDecimal(fila));
					error.setNombreSeccion(lineaDeNegocio);
					error.setNombreCampoError("FECHA DE NACIMIENTO");
					error.setRecomendaciones("Ingrese la Fecha de Nacimiento");
					listErrores.add(error);
					mensajeError.append("Campo: FECHA DE NACIMIENTO -  Ingrese la Fecha de Nacimiento").append("\r\n" ).append(System.getProperty("line.separator"));
					}
				}
				break;
			case CAMPO_OCUPACION:
				if (cell != null && !cell.toString().isEmpty()) {
					detalle.setOcupacion(cell.toString().toUpperCase());
					lineaVacia = false;
				}else{
					if(datosConductorObligatorios){
						error.setNumeroInciso(new BigDecimal(fila));
						error.setNombreSeccion(lineaDeNegocio);
						error.setNombreCampoError("OCUPACION");
						error.setRecomendaciones("Ingrese la Ocupacion");
						listErrores.add(error);
						mensajeError.append("Campo: ").append(error.getNombreCampoError()).append(" - ").append(error.getRecomendaciones()).append("").append("\r\n" ).append(System.getProperty("line.separator"));
					}					
				}
				break;
			case CAMPO_NUMERO_MOTOR:
				if (cell != null && !cell.toString().isEmpty()) {
					if(cell.getCellType() == HSSFCell.CELL_TYPE_NUMERIC){
						double valor = cell.getNumericCellValue();
						BigDecimal valorB = new BigDecimal(valor);
						detalle.setNumeroMotor(valorB.toPlainString().toUpperCase());
					}else if(cell.getCellType() == HSSFCell.CELL_TYPE_STRING){
						detalle.setNumeroMotor(cell.toString().toUpperCase());
					}else{
						detalle.setNumeroMotor(cell.toString().toUpperCase());
					}					
					lineaVacia = false;
				} else {
					error.setNumeroInciso(new BigDecimal(fila));
					error.setNombreSeccion(lineaDeNegocio);
					error.setNombreCampoError("NUMERO DE MOTOR");
					error.setRecomendaciones("Ingrese el Numero de Motor");
					listErrores.add(error);
					mensajeError.append("Campo: NUMERO DE MOTOR -  Ingrese el Numero de Motor ").append("\r\n" ).append(System.getProperty("line.separator"));
				}
				break;
			case CAMPO_NUMERO_SERIE:
				if (cell != null && !cell.toString().isEmpty()) {
					if(cell.getCellType() == HSSFCell.CELL_TYPE_NUMERIC){
						double valor = cell.getNumericCellValue();
						BigDecimal valorB = new BigDecimal(valor);
						detalle.setNumeroSerie(valorB.toPlainString());
					}else if(cell.getCellType() == HSSFCell.CELL_TYPE_STRING){
						detalle.setNumeroSerie(cell.toString());
					}else{
						detalle.setNumeroSerie(cell.toString());
					}
					lineaVacia = false;
				} else {
					error.setNumeroInciso(new BigDecimal(fila));
					error.setNombreSeccion(lineaDeNegocio);
					error.setNombreCampoError("NUMERO DE SERIE");
					error.setRecomendaciones("Ingrese el N\u00famero de Serie");
					listErrores.add(error);
					mensajeError.append("Campo: NUMERO DE SERIE -  Ingrese el N\u00famero de Serie ").append("\r\n" ).append(System.getProperty("line.separator"));
				}
				break;
			case CAMPO_NUMERO_PLACA:
				if (cell != null && !cell.toString().isEmpty()) {
					detalle.setNumeroPlaca(cell.toString().toUpperCase());
					lineaVacia = false;
				}else{
					if(datosConductorObligatorios){
						error.setNumeroInciso(new BigDecimal(fila));
						error.setNombreSeccion(lineaDeNegocio);
						error.setNombreCampoError("NUMERO PLACA");
						error.setRecomendaciones("Ingrese el Numero de Placa");
						listErrores.add(error);
						mensajeError.append("Campo: ").append(error.getNombreCampoError()).append(" - ").append(error.getRecomendaciones()).append("").append("\r\n" ).append(System.getProperty("line.separator"));
					}					
				}
				break;
			case CAMPO_REPUVE:
				if (cell != null && !cell.toString().isEmpty()) {
					detalle.setRepuve(cell.toString().toUpperCase());
					lineaVacia = false;
				}
				break;
			case CAMPO_DESCRIPCION_EQUIPO_ESPECIAL:
				if (cell != null && !cell.toString().isEmpty()) {
					detalle.setDescripcionEquipoEspecial(cell.toString().toUpperCase());
					lineaVacia = false;
				}
				break;
			case CAMPO_DESCRIPCION_ADAPTACION_CONVERSION:
				if (cell != null && !cell.toString().isEmpty()) {
					detalle.setDescripcionAdaptacionConversion(cell.toString().toUpperCase());
					lineaVacia = false;
				}
				break;
			case CAMPO_OBSERVACIONES_INCISO:
				if (cell != null && !cell.toString().isEmpty()) {
					detalle.setObservacionesInciso(cell.toString().toUpperCase());
					lineaVacia = false;
				}
				break;
			case CAMPO_EMAIL:
				if (cell != null && !cell.toString().isEmpty()) {
					try{
						detalle.setEmailContactos(cell.toString().toLowerCase());
						lineaVacia = false;
					}catch(Exception e){
					}
				}
				break;
			case CAMPO_CONDICION_ESPECIAL_ALTA:
				if (cell != null && !cell.toString().isEmpty()) {
				 cell.setCellType(Cell.CELL_TYPE_STRING);
				 sAltas = cell.toString().split(SEPARADOR_CONDICION_ESPECIAL);
				 for(int pos=0; pos<= sAltas.length-1; pos++){
					 //Validar no sea nulo
					 if(sAltas[pos]!=null && sAltas[pos].length()>0){
						 if(this.isInt(sAltas[pos])){
							 altas.add(sAltas[pos]);
						 }else{
							 // Agrega errores
							 error.setNumeroInciso(new BigDecimal(fila));
							 error.setNombreSeccion(lineaDeNegocio);
							 error.setNombreCampoError("CONDICIONES ALTA - Ingrese solo N\u00fameros, en caso de ser mas una condicion separarlos por una ,(coma)");
							 error.setRecomendaciones("Ingrese solo N\u00fameros, en caso de ser mas una condicion separarlos por una ,(coma)");
							 listErrores.add(error);
							 mensajeError.append("Campo: CONDICION ESPECIAL ALTA -  N\u00famero (").append(sAltas[pos]).append(") CON CARACTERES NO VALIDOS ").append("\r\n" ).append(System.getProperty("line.separator"));
						 } 
					 }
				 }
				 
				 
				 if(!altas.isEmpty()){
					 
					 List<String> noValidos = negocioCondicionEspecialService.validaCondicionEspecialAlta(this.lNegocioCondicionEspecial , altas);
					 
					 if(!noValidos.isEmpty()){
					
						error.setNumeroInciso(new BigDecimal(fila));
						error.setNombreSeccion("");
						error.setNombreCampoError("CONDICIONES ALTA - La(s) condición(es) "+noValidos.toString()+" no pertenece al negocio seleccionado");
						error.setRecomendaciones("La(s) condición(es) "+noValidos.toString()+" no pertenece al negocio seleccionado");
						listErrores.add(error);
						mensajeError.append("Campo: CONDICIONES ALTA - La(s) condicion(es) no pertenecen:").append(noValidos.toString()).append(" al negocio ").append("\r\n" ).append(System.getProperty("line.separator"));
						
						
					 }else{
						// Agregar un List<CondicionEspecial> al objeto
						 List<CondicionEspecial> lCondicionesEspeciales = this.convertirCondicionesEspeciales(
								 altas.toArray(new String[altas.size()]), 
								 new BigDecimal(fila), 
								 "CONDICIONES ALTA - NO SE ENCUENTRAN DATO DE CONDICION ESPECIAL VALIDO"
								 );
						 
						 if (this.listErrores.isEmpty()) {
							 detalle.setAltaCondicionesEspeciales( lCondicionesEspeciales );
						 }
					 }
				 }
				}
				break;
			case CAMPO_CONDICION_ESPECIAL_BAJA:
				if (cell != null && !cell.toString().isEmpty()) {
				cell.setCellType(Cell.CELL_TYPE_STRING);
				sBajas = cell.toString().split(SEPARADOR_CONDICION_ESPECIAL);
				 for(int pos=0; pos<= sBajas.length-1; pos++){ 
					 if(sBajas[pos]!=null && sBajas[pos].length()>0){
						 if(this.isInt(sBajas[pos])){
							 bajas.add(sBajas[pos]) ;
						 }else{
							 // Agrega errores
							 error.setNumeroInciso(new BigDecimal(fila));
							 error.setNombreSeccion(lineaDeNegocio);
							 error.setNombreCampoError("CONDICIONES BAJA - Ingrese solo N\u00fameros, en caso de ser mas una condicion separarlos por una ,(coma)");
							 error.setRecomendaciones("Ingrese solo N\u00fameros, en caso de ser mas una condicion separarlos por una ,(coma)");
							 listErrores.add(error);
							 mensajeError.append("Campo: CONDICION ESPECIAL BAJA -  N\u00famero (").append(sBajas[pos]).append(") CON CARACTERES NO VALIDOS ").append("\r\n" ).append(System.getProperty("line.separator"));
						 }	 
					 }
				 }
				 
				 if(bajas.size() > 0){
					 List<String> noValidos = negocioCondicionEspecialService.validaCondicionEspecialBaja(
								cotizacion.getNegocioDerechoPoliza().getNegocio().getIdToNegocio(), 
								bajas);
					 
					 if(noValidos.size() > 0){
					
						error.setNumeroInciso(new BigDecimal(fila));
						error.setNombreSeccion("");
						error.setNombreCampoError("CONDICIONES BAJA La(s) condición(es) "+noValidos.toString()+" no pertenece al negocio seleccionado");
						error.setRecomendaciones("La(s) condición(es) "+noValidos.toString()+" no pertenece al negocio seleccionado");
						listErrores.add(error);
						mensajeError.append("Campo: CONDICIONES ALTA - La(s) condicion(es) son obligatorias o no pertenecen:").append(noValidos.toString()).append(" al negocio y no se pueden eliminars ").append("\r\n" ).append(System.getProperty("line.separator"));		
					 }else{
						// Agregar un List<CondicionEspecial> al objeto
						 List<CondicionEspecial> lCondicionesEspeciales = this.convertirCondicionesEspeciales(
								 bajas.toArray(new String[bajas.size()]), 
								 new BigDecimal(fila), 
								 "CONDICIONES BAJA"
								 );
						 
						 if(this.listErrores.size() == 0){
							 detalle.setBajaCondicionesEspeciales( lCondicionesEspeciales );
						 }
					 }
				 }
				}
				break;
				
			case CAMPO_CONDUCTO_COBRO:
				if (cell != null && !cell.toString().isEmpty()) {
					detalle.setConductoCobro(cell.toString());
					lineaVacia = false;
					
					if(cell.toString().equals(VALOR_TC)){
						detalle.setIdMedioPago(MEDIO_PAGO_TC);
					}else if(cell.toString().equals(VALOR_DOMICILIACION)){
						detalle.setIdMedioPago(MEDIO_PAGO_DOMICILIACION);
					}else if(cell.toString().equals(VALOR_EFECTIVO)){
						detalle.setIdMedioPago(MEDIO_PAGO_EFECTIVO);
					}else if(cell.toString().equals(VALOR_CUENTAAFIRME)){
						detalle.setIdMedioPago(MEDIO_PAGO_CUENTA_AFIRME);
					}
				}
				break;
			case CAMPO_INSTITUCION_BANCARIA:
				if (cell != null && !cell.toString().isEmpty()) {
					detalle.setInstitucionBancaria(cell.toString());
				}
				break;	
			case CAMPO_TIPO_TARJETA:
				if (cell != null && !cell.toString().isEmpty()) {
					detalle.setTipoTarjeta(cell.toString());
				}
				break;	
			case CAMPO_NUMERO_TARJETACLABE:
				if (cell != null && !cell.toString().isEmpty()) {
					cell.setCellType(Cell.CELL_TYPE_STRING);
					detalle.setNumeroTarjetaClave(cell.toString());
				}
				break;
			case CAMPO_CODIGO_SEGURIDAD:
				if (cell != null && !cell.toString().isEmpty()) {
					cell.setCellType(Cell.CELL_TYPE_STRING);
					detalle.setCodigoSeguridad(cell.toString());
				}
				break;
			case CAMPO_FECHA_VENCIMIENTO:
				if (cell != null && !cell.toString().isEmpty()) {
					String fechaVencimiento = validarFecha(cell.toString()); 					
					if(fechaVencimiento==null){
						error.setNumeroInciso(new BigDecimal(fila));
						error.setNombreSeccion("FECHA DE VENCIMIENTO");
						error.setNombreCampoError("FECHA DE VENCIMIENTO");
						error.setRecomendaciones("Fecha de Vencimiento no valida. Usar el formato MM/yy y una tarjeta vigente");
						listErrores.add(error);
					}else{						
						detalle.setFechaVencimiento(fechaVencimiento);
					}
				}
				break;
				
			}
			
		}
		detalle.setMensajeError(mensajeError.toString());
		if (mensajeError.toString().isEmpty()) {
			detalle.setClaveEstatus(DetalleCargaMasivaAutoCot.ESTATUS_PENDIENTE);
		} else {
			detalle.setClaveEstatus(DetalleCargaMasivaAutoCot.ESTATUS_ERROR);
		}
		if(lineaVacia && !mensajeError.toString().isEmpty()){
			Iterator<LogErroresCargaMasivaDTO> it = listErrores.iterator();
			while(it.hasNext()){
				LogErroresCargaMasivaDTO error = it.next();
				if(error.getNumeroInciso().equals(new BigDecimal(fila))){
					it.remove();
				}
			}
			return null;
		}

		return detalle;
	}
	
	public void validaDetalleCargaMasiva(
			List<DetalleCargaMasivaAutoCot> detalleCargaMasivaList) {
		StringBuilder mensajeError = new StringBuilder("");
		int fila = 1;
		incisosCreadosList = new ArrayList<IncisoCotizacionDTO>(1);
		for (DetalleCargaMasivaAutoCot detalle : detalleCargaMasivaList) {

			fila++;
			if (detalle.getClaveEstatus() != null && !detalle.getClaveEstatus().equals(
					DetalleCargaMasivaAutoCot.ESTATUS_ERROR)) {
				BigDecimal numeroInciso = BigDecimal.valueOf(fila);

				IncisoCotizacionId incisoCotizacionId = new IncisoCotizacionId();
				IncisoCotizacionDTO incisoCotizacion = new IncisoCotizacionDTO();
				incisoCotizacionId.setIdToCotizacion(cotizacion
						.getIdToCotizacion());
				incisoCotizacion.setCotizacionDTO(cotizacion);
				incisoCotizacion.setId(incisoCotizacionId);

				IncisoAutoCot incisoAutoCot = new IncisoAutoCot();

				NegocioSeccion negocioSeccion = null;
				//String mensajeError = "";
				String lineaDeNegocio = "";

				negocioSeccion = cargaMasivaService
						.obtieneNegocioSeccionPorDescripcion(cotizacion, this
								.remplazaGuion(detalle
										.getLineaNegocioDescripcion()));
				if (negocioSeccion != null) {
					lineaDeNegocio = negocioSeccion.getSeccionDTO()
							.getDescripcion();
					incisoAutoCot.setNegocioSeccionId(negocioSeccion
							.getIdToNegSeccion().longValue());
				} else {
					LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
					error.setNumeroInciso(new BigDecimal(fila));
					error.setNombreSeccion("");
					error.setNombreCampoError("LINEA DE NEGOCIO");
					error.setRecomendaciones("Linea de Negocio Invalida");
					listErrores.add(error);
					mensajeError.append("Campo: LINEA DE NEGOCIO -  Linea de Negocio Invalida ").append("\r\n").append(System.getProperty("line.separator"));
				}

				NegocioTipoUso negocioTipoUso = cargaMasivaService
						.obtieneNegocioTipoUsoPorDescripcion(negocioSeccion,
								detalle.getTipoUsoDescripcion());
				if (negocioTipoUso != null) {
					incisoAutoCot.setTipoUsoId(negocioTipoUso
							.getTipoUsoVehiculoDTO().getIdTcTipoUsoVehiculo()
							.longValue());
				} else {
					LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
					error.setNumeroInciso(new BigDecimal(fila));
					error.setNombreSeccion(lineaDeNegocio);
					error.setNombreCampoError("TIPO DE USO");
					error.setRecomendaciones("TipoUso Invalido");
					listErrores.add(error);
					mensajeError.append("Campo: TIPO DE USO -  TipoUso Invalido ").append("\r\n").append(System.getProperty("line.separator"));
				}

				EstiloVehiculoDTO estiloVehiculo = cargaMasivaService
						.obtieneEstiloVehiculoDTOPorClaveAMIS(cotizacion,
								negocioSeccion, detalle.getClaveAMIS());
				if (estiloVehiculo != null) {
					incisoAutoCot.setMarcaId(estiloVehiculo
							.getMarcaVehiculoDTO().getIdTcMarcaVehiculo());
					incisoAutoCot
							.setEstiloId(estiloVehiculo.getId().getStrId());
					
					try{
						incisoAutoCot.setClaveTipoBien(estiloVehiculo.getId().getClaveTipoBien());
						incisoAutoCot.setIdVersionCarga(estiloVehiculo.getId().getIdVersionCarga());
						incisoAutoCot.setIdMoneda(cotizacion.getIdMoneda().shortValue());
					}catch(Exception e){
					}
					
					if(detalle.getDescripcion() != null && !detalle.getDescripcion().isEmpty()){
						incisoAutoCot.setDescripcionFinal(detalle.getDescripcion().toUpperCase());
					}else{						
						incisoAutoCot.setDescripcionFinal(estiloVehiculo.getDescripcionEstilo());
						detalle.setDescripcion(estiloVehiculo.getDescripcionEstilo().toUpperCase());
					}					
				} else {
					LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
					error.setNumeroInciso(new BigDecimal(fila));
					error.setNombreSeccion(lineaDeNegocio);
					error.setNombreCampoError("CLAVE AMIS");
					error.setRecomendaciones("Clave AMIS Invalida");
					listErrores.add(error);
					mensajeError.append("Campo: CLAVE AMIS -  Clave AMIS Invalida ").append("\r\n").append(System.getProperty("line.separator"));
				}

				//ValidaModelo
				if(estiloVehiculo != null){
					if(cargaMasivaService.validaModeloVehiculo(cotizacion.getIdMoneda(), estiloVehiculo.getId().getStrId(), 
							negocioSeccion.getIdToNegSeccion(), detalle.getModeloVehiculo())){
						incisoAutoCot.setModeloVehiculo(detalle.getModeloVehiculo());
					}else{
						LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
						error.setNumeroInciso(new BigDecimal(fila));
						error.setNombreSeccion("");
						error.setNombreCampoError("MODELO VEHICULO");
						error.setRecomendaciones("Modelo Vehiculo Invalido");
						listErrores.add(error);
						mensajeError.append("Campo: ").append(error.getNombreCampoError()).append(" -  ").append(error.getRecomendaciones()).append(" ").append("\r\n").append(System.getProperty("line.separator"));
					}
				}

				NegocioPaqueteSeccion negocioPaquete = cargaMasivaService
						.obtieneNegocioPaqueteSeccionPorDescripcion(
								negocioSeccion, detalle.getPaqueteDescripcion());

				if (negocioPaquete != null) {
					incisoAutoCot.setNegocioPaqueteId(negocioPaquete
							.getIdToNegPaqueteSeccion());
				} else {
					LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
					error.setNumeroInciso(new BigDecimal(fila));
					error.setNombreSeccion(lineaDeNegocio);
					error.setNombreCampoError("PAQUETE");
					error.setRecomendaciones("Paquete Invalido");
					listErrores.add(error);
					mensajeError.append("Campo: PAQUETE -  Paquete Invalido ").append("\r\n").append(System.getProperty("line.separator"));
				}

				String municipioId = cargaMasivaService
						.obtieneMunicipioIdPorCodigoPostal(detalle
								.getCodigoPostal());
				if (municipioId != null) {
					incisoAutoCot.setMunicipioId(municipioId);

					String estadoId = cargaMasivaService
							.obtieneEstadoIdPorMunicipio(municipioId);
					incisoAutoCot.setEstadoId(estadoId);
					
					if(!cargaMasivaService.validaEstadoMunicipio(negocioSeccion.getNegocioTipoPoliza().getNegocioProducto().getNegocio().getIdToNegocio(), 
							estadoId, municipioId)){
						LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
						error.setNumeroInciso(new BigDecimal(fila));
						error.setNombreSeccion("");
						error.setNombreCampoError("CODIGO POSTAL");
						error.setRecomendaciones("Codigo Postal Invalido");
						listErrores.add(error);
						mensajeError.append("Campo: CODIGO POSTAL -  Codigo Postal Invalido ").append("\r\n").append(System.getProperty("line.separator"));							
					}

				} else {
					LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
					error.setNumeroInciso(new BigDecimal(fila));
					error.setNombreSeccion(lineaDeNegocio);
					error.setNombreCampoError("CODIGO POSTAL");
					error.setRecomendaciones("Codigo Postal Invalido");
					listErrores.add(error);
					mensajeError.append("Campo: CODIGO POSTAL -  Codigo Postal Invalido ").append("\r\n").append(System.getProperty("line.separator"));
				}

				if (mensajeError.toString().isEmpty()) {
					incisoCotizacion.setIncisoAutoCot(incisoAutoCot);
					List<CoberturaCotizacionDTO> coberturaCotizacionList = cargaMasivaService
							.obtieneCoberturasDelInciso(incisoCotizacion,
									negocioPaquete, estiloVehiculo.getId());					
					List<DatoIncisoCotAuto> datoIncisoCotAutos = new ArrayList<DatoIncisoCotAuto>();
					
				    if(coberturaCotizacionList != null && !coberturaCotizacionList.isEmpty()){
						Short claveContrato = 1;
						Short claveObligatoriedad = 0;
						Short valorSI = 1;
						Short valorNO = 0;
						for(CoberturaCotizacionDTO coberturaCotizacion : coberturaCotizacionList){
							
			    			//Valida si es obligatoria
							Boolean esObligatoria = false;
			    			if(coberturaCotizacion.getClaveContrato().equals(claveContrato) && 
			    					coberturaCotizacion.getClaveObligatoriedad().equals(claveObligatoriedad)){
			    				esObligatoria = true;
			    			}
			    			
			    			//Valida Permite Suma Asegurada
			    			Boolean permiteSumaAsegurada = false;				    			
			    			if(coberturaCotizacion.getCoberturaSeccionDTO().getCoberturaDTO().getClaveFuenteSumaAsegurada().equals(CoberturaDTO.CLAVE_FUENTE_SA_VALOR_PROPORCIONADO) ||
			    					coberturaCotizacion.getCoberturaSeccionDTO().getCoberturaDTO().getClaveFuenteSumaAsegurada().equals(CoberturaDTO.CLAVE_FUENTE_SA_VALOR_FACTURA)){
			    				permiteSumaAsegurada = true;
			    			}			    			
			    			
			    			//Valida Rangos
			    			Boolean validaRangos = false;
			    			if(coberturaCotizacion.getCoberturaSeccionDTO().getCoberturaDTO().getClaveFuenteSumaAsegurada().equals("0") &&
			    					coberturaCotizacion.getValorSumaAseguradaMin() != 0 && coberturaCotizacion.getValorSumaAseguradaMax() != 0){
			    				validaRangos = true;
			    			}
			    			
			    			//Valida Deducibles
			    			Boolean validaDeducibles = false;
			    			if(!coberturaCotizacion.getCoberturaSeccionDTO().getCoberturaDTO().getClaveTipoDeducible().equals("0")){
			    				validaDeducibles = true;
			    			}
			    			
				    		if(coberturaCotizacion.getCoberturaSeccionDTO().getCoberturaDTO().getClaveTipoCalculo().toUpperCase().equals(
							 CoberturaDTO.CLAVE_CALCULO_DANOS_MATERIALES.toUpperCase())){
				    			if(permiteSumaAsegurada && detalle.getValorSumaAseguradaDM() != null){
				    				boolean valido = true;
					    			if(validaRangos){
					    				valido = validaRangos(coberturaCotizacion.getValorSumaAseguradaMin(), 
					    						coberturaCotizacion.getValorSumaAseguradaMax(), detalle.getValorSumaAseguradaDM());
					    			}
					    			if(valido){
				    					coberturaCotizacion.setValorSumaAsegurada(detalle.getValorSumaAseguradaDM());
				    				}else{
				    					LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
				    					error.setNumeroInciso(new BigDecimal(fila));
				    					error.setNombreSeccion("");
				    					error.setNombreCampoError("SUMA ASEGURADA DANOS MATERIALES");
				    					error.setRecomendaciones("Valor no valido");
				    					listErrores.add(error);
				    					mensajeError.append("Campo: SUMA ASEGURADA DM -  Valor debe ser entre ").append(coberturaCotizacion.getValorSumaAseguradaMin()).append(
				    					" y ").append(coberturaCotizacion.getValorSumaAseguradaMax()).append(" ");
				    				}
				    			}				    			
				    			if(detalle.getDeducibleDanosMateriales() != null){
				    				boolean valido = true;
				    				if(validaDeducibles){
				    					valido = validaDeducible(coberturaCotizacion.getDeducibles(), detalle.getDeducibleDanosMateriales());
				    				}
				    				if(valido){
				    					coberturaCotizacion.setValorDeducible(detalle.getDeducibleDanosMateriales());
				    					coberturaCotizacion.setPorcentajeDeducible(detalle.getDeducibleDanosMateriales());
				    					coberturaCotizacion.setClaveContrato(claveContrato);
				    				}else{
				    					LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
				    					error.setNumeroInciso(new BigDecimal(fila));
				    					error.setNombreSeccion(lineaDeNegocio);
				    					error.setNombreCampoError("DEDUCIBLE DA\u00D1OS MATERIALES");
				    					error.setRecomendaciones("Valor no valido");
				    					listErrores.add(error);
				    					mensajeError.append("Campo: DEDUCIBLE DA\u00d1OS MATERIALES -  Valor no valido ").append("\r\n").append(System.getProperty("line.separator"));
				    				}
				    			}
					 		}else if(coberturaCotizacion.getCoberturaSeccionDTO().getCoberturaDTO().getClaveTipoCalculo().trim().toUpperCase().equals(
							 CoberturaDTO.CLAVE_CALCULO_ROBO_TOTAL.trim().toUpperCase())){
					 			if(permiteSumaAsegurada && detalle.getValorSumaAseguradaRT() != null){
				    				boolean valido = true;
					    			if(validaRangos){
					    				valido = validaRangos(coberturaCotizacion.getValorSumaAseguradaMin(), 
					    						coberturaCotizacion.getValorSumaAseguradaMax(), detalle.getValorSumaAseguradaRT());
					    			}
					    			if(valido){
				    					coberturaCotizacion.setValorSumaAsegurada(detalle.getValorSumaAseguradaRT());
				    				}else{
				    					LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
				    					error.setNumeroInciso(new BigDecimal(fila));
				    					error.setNombreSeccion("");
				    					error.setNombreCampoError("SUMA ASEGURADA ROBO TOTAL");
				    					error.setRecomendaciones("Valor no valido");
				    					listErrores.add(error);
				    					mensajeError.append("Campo: SUMA ASEGURADA RT -  Valor debe ser entre ").append(coberturaCotizacion.getValorSumaAseguradaMin()).append(
				    					" y " + coberturaCotizacion.getValorSumaAseguradaMax()).append(" ");
				    				}
				    			}					 			
				    			if(detalle.getDeducibleRoboTotal() != null){
				    				boolean valido = true;
				    				if(validaDeducibles){
				    					valido = validaDeducible(coberturaCotizacion.getDeducibles(), detalle.getDeducibleRoboTotal());
				    				}
				    				if(valido){
				    					coberturaCotizacion.setValorDeducible(detalle.getDeducibleRoboTotal());
				    					coberturaCotizacion.setPorcentajeDeducible(detalle.getDeducibleRoboTotal());
				    					coberturaCotizacion.setClaveContrato(claveContrato);
				    				}else{
				    					LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
				    					error.setNumeroInciso(new BigDecimal(fila));
				    					error.setNombreSeccion(lineaDeNegocio);
				    					error.setNombreCampoError("DEDUCIBLE ROBO TOTAL");
				    					error.setRecomendaciones("Valor no valido");
				    					listErrores.add(error);
				    					mensajeError.append("Campo: DEDUCIBLE ROBO TOTAL -  Valor no valido ").append("\r\n").append(System.getProperty("line.separator"));
				    				}
				    			}
					 		}else if(coberturaCotizacion.getCoberturaSeccionDTO().getCoberturaDTO().getNombreComercial().toUpperCase().equals(
								 NOMBRE_LIMITE_RC_TERCEROS.toUpperCase())){
						    		if(detalle.getLimiteRcTerceros() != null){
						    			boolean valido = true;
						    			if(validaRangos){
						    				valido = validaRangos(coberturaCotizacion.getValorSumaAseguradaMin(), 
						    						coberturaCotizacion.getValorSumaAseguradaMax(), detalle.getLimiteRcTerceros());
						    			}
						    			if(valido){
							    			coberturaCotizacion.setValorSumaAsegurada(detalle.getLimiteRcTerceros());
							    			coberturaCotizacion.setClaveContrato(claveContrato);						    				
						    			}else{
					    					LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
					    					error.setNumeroInciso(new BigDecimal(fila));
					    					error.setNombreSeccion(lineaDeNegocio);
					    					error.setNombreCampoError("LIMITE RC TERCEROS");
					    					error.setRecomendaciones("Valor no valido");
					    					listErrores.add(error);
					    					mensajeError.append("Campo: LIMITE RC TERCEROS -  Valor debe ser entre ").append(coberturaCotizacion.getValorSumaAseguradaMin()).append(
					    					" y ").append(coberturaCotizacion.getValorSumaAseguradaMax()).append("").append("\r\n").append(System.getProperty("line.separator"));
						    			}
						    		}
					 		}else if(coberturaCotizacion.getCoberturaSeccionDTO().getCoberturaDTO().getNombreComercial().toUpperCase().equals(
									 NOMBRE_LIMITE_GASTOS_MEDICOS.toUpperCase())){
							    		if(detalle.getLimiteGastosMedicos() != null){
							    			boolean valido = true;
							    			if(validaRangos){
							    				valido = validaRangos(coberturaCotizacion.getValorSumaAseguradaMin(), 
							    						coberturaCotizacion.getValorSumaAseguradaMax(), detalle.getLimiteGastosMedicos());
							    			}
							    			if(valido){
								    			coberturaCotizacion.setValorSumaAsegurada(detalle.getLimiteGastosMedicos());
								    			coberturaCotizacion.setClaveContrato(claveContrato);					    				
							    			}else{
						    					LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
						    					error.setNumeroInciso(new BigDecimal(fila));
						    					error.setNombreSeccion(lineaDeNegocio);
						    					error.setNombreCampoError("LIMITE GASTOS MEDICOS");
						    					error.setRecomendaciones("Valor no valido");
						    					listErrores.add(error);
						    					mensajeError.append("Campo: LIMITE GASTOS MEDICOS -  Valor debe ser entre ").append(coberturaCotizacion.getValorSumaAseguradaMin()).append(
						    					" y ").append(coberturaCotizacion.getValorSumaAseguradaMax()).append("").append("\r\n").append(System.getProperty("line.separator"));
							    			}
							    		}
					 		}else if(coberturaCotizacion.getCoberturaSeccionDTO().getCoberturaDTO().getNombreComercial().toUpperCase().equals(
									 NOMBRE_ASISTENCIA_JURIDICA.toUpperCase())){
							    		if(detalle.getAsistenciaJuridica() != null){
							    			if(detalle.getAsistenciaJuridica().equals(valorSI)){
							    				coberturaCotizacion.setClaveContrato(claveContrato);
							    			}else{
							    				if(!esObligatoria){
								    				coberturaCotizacion.setClaveContrato(valorNO);
								    			}
							    			}
							    		}
					 		}else if(coberturaCotizacion.getCoberturaSeccionDTO().getCoberturaDTO().getNombreComercial().toUpperCase().startsWith(
									 NOMBRE_ASISTENCIA_EN_VIAJES.toUpperCase())){
							    		if(detalle.getAsistenciaViajes() != null){
							    			if(detalle.getAsistenciaViajes().equals(valorSI)){
							    				coberturaCotizacion.setClaveContrato(claveContrato);
							    			}else{
							    				if(!esObligatoria){
								    				coberturaCotizacion.setClaveContrato(valorNO);
								    			}
							    			}
							    		}
					 		}else if(coberturaCotizacion.getCoberturaSeccionDTO().getCoberturaDTO().getNombreComercial().toUpperCase().equals(
									 NOMBRE_EXENCION_DEDUCIBLE_DANOS.toUpperCase())){
							    		if(detalle.getExencionDeducibleDanosMateriales() != null){
							    			if(detalle.getExencionDeducibleDanosMateriales().equals(valorSI)){
							    				coberturaCotizacion.setClaveContrato(claveContrato);
							    			}else{
							    				if(!esObligatoria){
								    				coberturaCotizacion.setClaveContrato(valorNO);
								    			}
							    			}
							    		}
					 		}else if(coberturaCotizacion.getCoberturaSeccionDTO().getCoberturaDTO().getNombreComercial().toUpperCase().equals(
									 NOMBRE_LIMITE_ACCIDENTE_CONDUCTOR.toUpperCase())){
							    		if(detalle.getLimiteAccidenteConductor() != null){
							    			boolean valido = true;
							    			if(validaRangos){
							    				valido = validaRangos(coberturaCotizacion.getValorSumaAseguradaMin(), 
							    						coberturaCotizacion.getValorSumaAseguradaMax(), detalle.getLimiteAccidenteConductor());
							    			}
							    			if(valido){
								    			coberturaCotizacion.setValorSumaAsegurada(detalle.getLimiteAccidenteConductor());
								    			coberturaCotizacion.setClaveContrato(claveContrato);					    				
							    			}else{
						    					LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
						    					error.setNumeroInciso(new BigDecimal(fila));
						    					error.setNombreSeccion(lineaDeNegocio);
						    					error.setNombreCampoError("LIMITE ACCIDENTE CONDUCTOR");
						    					error.setRecomendaciones("Valor no valido");
						    					listErrores.add(error);
						    					mensajeError.append("Campo: LIMITE ACCIDENTE CONDUCTOR -  Valor debe ser entre ").append(coberturaCotizacion.getValorSumaAseguradaMin()).append(
						    					" y ").append(coberturaCotizacion.getValorSumaAseguradaMax()).append("").append("\r\n").append(System.getProperty("line.separator"));
							    			}
							    		}
					 		}else if(coberturaCotizacion.getCoberturaSeccionDTO().getCoberturaDTO().getNombreComercial().toUpperCase().equals(
									 NOMBRE_EXTENCION_RC.toUpperCase())){
					    		if(detalle.getExencionRC() != null){
					    			if(detalle.getExencionRC().equals(valorSI)){
					    				coberturaCotizacion.setClaveContrato(claveContrato);
					    			}else{
					    				if(!esObligatoria){
						    				coberturaCotizacion.setClaveContrato(valorNO);
						    			}
					    			}
					    		}
					 		}else if(coberturaCotizacion.getCoberturaSeccionDTO().getCoberturaDTO().getNombreComercial().toUpperCase().equals(
									 NOMBRE_LIMITE_ADAPTACION_CONVERSION.toUpperCase())){
							    		if(detalle.getLimiteAdaptacionConversion() != null){
							    			boolean valido = true;
							    			if(validaRangos){
							    				valido = validaRangos(coberturaCotizacion.getValorSumaAseguradaMin(), 
							    						coberturaCotizacion.getValorSumaAseguradaMax(), detalle.getLimiteAdaptacionConversion());
							    			}
							    			if(valido){
								    			coberturaCotizacion.setValorSumaAsegurada(detalle.getLimiteAdaptacionConversion());
								    			coberturaCotizacion.setClaveContrato(claveContrato);					    				
							    			}else{
						    					LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
						    					error.setNumeroInciso(new BigDecimal(fila));
						    					error.setNombreSeccion(lineaDeNegocio);
						    					error.setNombreCampoError("LIMITE ADAPTACION CONVERSION");
						    					error.setRecomendaciones("Valor no valido");
						    					listErrores.add(error);
						    					mensajeError.append("Campo: LIMITE ADAPTACION CONVERSION -  Valor debe ser entre ").append(coberturaCotizacion.getValorSumaAseguradaMin()).append(
						    					" y ").append(coberturaCotizacion.getValorSumaAseguradaMax()).append("").append("\r\n").append(System.getProperty("line.separator"));
							    			}
							    		}
					 		}else if(coberturaCotizacion.getCoberturaSeccionDTO().getCoberturaDTO().getNombreComercial().toUpperCase().equals(
									 NOMBRE_LIMITE_EQUIPO_ESPECIAL.toUpperCase())){
							    		if(detalle.getLimiteEquipoEspecial() != null){
							    			boolean valido = true;
							    			if(validaRangos){
							    				valido = validaRangos(coberturaCotizacion.getValorSumaAseguradaMin(), 
							    						coberturaCotizacion.getValorSumaAseguradaMax(), detalle.getLimiteEquipoEspecial());
							    			}
							    			if(valido){
								    			coberturaCotizacion.setValorSumaAsegurada(detalle.getLimiteEquipoEspecial());
								    			coberturaCotizacion.setClaveContrato(claveContrato);					    				
							    			}else{
						    					LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
						    					error.setNumeroInciso(new BigDecimal(fila));
						    					error.setNombreSeccion(lineaDeNegocio);
						    					error.setNombreCampoError("LIMITE EQUIPO ESPECIAL");
						    					error.setRecomendaciones("Valor no valido");
						    					listErrores.add(error);
						    					mensajeError.append("Campo: LIMITE EQUIPO ESPECIAL -  Valor debe ser entre ").append(coberturaCotizacion.getValorSumaAseguradaMin()).append(
						    					" y ").append(coberturaCotizacion.getValorSumaAseguradaMax()).append("").append("\r\n").append(System.getProperty("line.separator"));
							    			}
							    			if(valido && detalle.getDeducibleEquipoEspecial() != null){
							    				if(validaDeducibles){
							    					valido = validaDeducible(coberturaCotizacion.getDeducibles(), detalle.getDeducibleEquipoEspecial());
							    				}
							    				if(valido){
							    					coberturaCotizacion.setValorDeducible(detalle.getDeducibleEquipoEspecial());
							    					coberturaCotizacion.setPorcentajeDeducible(detalle.getDeducibleEquipoEspecial());
							    					coberturaCotizacion.setClaveContrato(claveContrato);
							    				}else{
							    					LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
							    					error.setNumeroInciso(new BigDecimal(fila));
							    					error.setNombreSeccion(lineaDeNegocio);
							    					error.setNombreCampoError("DEDUCIBLE EQUIPO ESPECIAL");
							    					error.setRecomendaciones("Valor no valido");
							    					listErrores.add(error);
							    					mensajeError.append("Campo: DEDUCIBLE EQUIPO ESPECIAL -  Valor no valido ").append("\r\n").append(System.getProperty("line.separator"));
							    				}
							    			}
							    		}
					 		}else if(coberturaCotizacion.getCoberturaSeccionDTO().getCoberturaDTO().getNombreComercial().toUpperCase().equals(
									 NOMBRE_EXENCION_DEDUCIBLE_ROBO.toUpperCase())){
					    		if(detalle.getExencionDeducibleRoboTotal() != null){
					    			if(detalle.getExencionDeducibleRoboTotal().equals(valorSI)){
					    				coberturaCotizacion.setClaveContrato(claveContrato);
					    			}else{
					    				if(!esObligatoria){
						    				coberturaCotizacion.setClaveContrato(valorNO);
						    			}
					    			}
					    		}
					 		}else if(coberturaCotizacion.getCoberturaSeccionDTO().getCoberturaDTO().getNombreComercial().toUpperCase().equals(
									 NOMBRE_RESPONSABILIDAD_CIVIL_USA.toUpperCase())){
					    		if(detalle.getResponsabilidadCivilUsaCanada() != null){
					    			if(detalle.getResponsabilidadCivilUsaCanada().equals(valorSI)){
					    				coberturaCotizacion.setClaveContrato(claveContrato);
					    			}else{
					    				if(!esObligatoria){
						    				coberturaCotizacion.setClaveContrato(valorNO);
						    			}
					    			}
					    		}
					 		}else if(coberturaCotizacion.getCoberturaSeccionDTO().getCoberturaDTO().getNombreComercial().toUpperCase().equals(
					 				CoberturaDTO.DANOS_OCASIONADOS_CARGA_NOMBRE_COMERCIAL.toUpperCase())){
				 				boolean danosCargaContratada = esObligatoria || detalle.isDanosCargaContratada();
				 				coberturaCotizacion.setClaveContratoBoolean(danosCargaContratada);
					 		}else if(coberturaCotizacion.getCoberturaSeccionDTO().getCoberturaDTO().getNombreComercial().toUpperCase().equals(
									 NOMBRE_LIMITE_MUERTE.toUpperCase())){
							    		if(detalle.getLimiteMuerte() != null){
							    			boolean valido = true;
							    			if(validaRangos){
							    				valido = validaRangos(coberturaCotizacion.getValorSumaAseguradaMin(), 
							    						coberturaCotizacion.getValorSumaAseguradaMax(), detalle.getLimiteMuerte());
							    			}
							    			if(valido){
								    			coberturaCotizacion.setValorSumaAsegurada(detalle.getLimiteMuerte());
								    			coberturaCotizacion.setClaveContrato(claveContrato);					    				
							    			}else{
						    					LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
						    					error.setNumeroInciso(new BigDecimal(fila));
						    					error.setNombreSeccion(lineaDeNegocio);
						    					error.setNombreCampoError("LIMITE MUERTE");
						    					error.setRecomendaciones("Valor no valido");
						    					listErrores.add(error);
						    					mensajeError.append("Campo: LIMITE MUERTE -  Valor debe ser entre ").append(coberturaCotizacion.getValorSumaAseguradaMin()).append(
						    					" y ").append(coberturaCotizacion.getValorSumaAseguradaMax()).append("").append("\r\n").append(System.getProperty("line.separator"));
							    			}
							    		}else{
						    				if(!esObligatoria){
							    				coberturaCotizacion.setClaveContrato(valorNO);
							    			}
							    		}
					 		}else if(coberturaCotizacion.getCoberturaSeccionDTO().getCoberturaDTO().getNombreComercial().toUpperCase().equals(
					 				NOMBRE_LIMITE_RC_VIAJERO.toUpperCase())){
							    		if(detalle.getDiasSalarioMinimo() != null){
							    			boolean valido = true;
							    			valido = validaDiasSalarioMinimo(detalle.getDiasSalarioMinimo());
							    			if(valido){
							    				coberturaCotizacion.setDiasSalarioMinimo(detalle.getDiasSalarioMinimo().intValue());
							    				//coberturaCotizacion.getValue().setValorSumaAsegurada(detalle.getLimiteMuerte());
							    				coberturaCotizacion.setClaveContrato(claveContrato);
							    			}else{
						    					LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
						    					error.setNumeroInciso(new BigDecimal(fila));
						    					error.setNombreSeccion(lineaDeNegocio);
						    					error.setRecomendaciones("Valor no valido");
						    					mensajeError.append("Campo: LIMITE MUERTE -  Valor debe ser entre ").append(coberturaCotizacion.getValorSumaAseguradaMin()).append(
						    					" y ").append(coberturaCotizacion.getValorSumaAseguradaMax()).append("").append("\r\n").append(System.getProperty("line.separator"));
						    					error.setNombreCampoError(mensajeError.toString());
						    					listErrores.add(error);
							    			}
							    		}else{
						    				if(!esObligatoria){
						    					coberturaCotizacion.setClaveContrato(valorNO);
							    			}
							    		}
					 		}
					 	}
					 
					}
				    
					CoberturaCotizacionDTO responsabilidadCivilCoberturaCotizacion = cargaMasivaService
							.getCoberturaCotizacion(
									coberturaCotizacionList,
									CoberturaDTO.RESPONSABILIDAD_CIVIL_AUTOS_NOMBRE_COMERCIAL);					
					if (responsabilidadCivilCoberturaCotizacion != null && responsabilidadCivilCoberturaCotizacion.getClaveContratoBoolean()) {
						DatoIncisoCotAuto datoIncisoCotAutoNumeroRemolques = cargaMasivaService
						.obtieneDatoIncisoCotAutoSubRamo(cotizacion,
								responsabilidadCivilCoberturaCotizacion, incisoCotizacion
										.getId().getNumeroInciso(), ConfiguracionDatoInciso.DescripcionEtiqueta.NUMERO_REMOLQUES, String
										.valueOf(detalle
												.getNumeroRemolques()));
						if (datoIncisoCotAutoNumeroRemolques != null) {							
							List<LogErroresCargaMasivaDTO> tmpErrors = new ArrayList<LogErroresCargaMasivaDTO>();				 			
							cargaMasivaService.validaNoNulo(detalle.getNumeroRemolques(), NOMBRE_CAMPO_NUMERO_REMOLQUES, numeroInciso, lineaDeNegocio, tmpErrors);
							cargaMasivaService.validaRangos(0, 9, detalle.getNumeroRemolques(), NOMBRE_CAMPO_NUMERO_REMOLQUES, numeroInciso, lineaDeNegocio, tmpErrors);
							listErrores.addAll(tmpErrors);
							mensajeError.append(cargaMasivaService.generaMensajeError(tmpErrors));
							datoIncisoCotAutos.add(datoIncisoCotAutoNumeroRemolques);
						}
					}
					
					CoberturaCotizacionDTO danosOcasionadosPorCargaCoberturaCotizacion = cargaMasivaService
					.getCoberturaCotizacion(
							coberturaCotizacionList,
							CoberturaDTO.DANOS_OCASIONADOS_CARGA_NOMBRE_COMERCIAL);
					if (danosOcasionadosPorCargaCoberturaCotizacion != null && danosOcasionadosPorCargaCoberturaCotizacion.getClaveContratoBoolean()) {
						List<LogErroresCargaMasivaDTO> tmpErrors = new ArrayList<LogErroresCargaMasivaDTO>();				 			
						
						
						if (cargaMasivaService.validaNoNulo(detalle.getTipoCargaDescripcion(), NOMBRE_CAMPO_TIPO_CARGA, numeroInciso, lineaDeNegocio, tmpErrors)) {
							cargaMasivaService.validaNoNulo2(detalle.getTipoCarga(), NOMBRE_CAMPO_TIPO_CARGA, numeroInciso, lineaDeNegocio, tmpErrors);			
						}
						
						if (responsabilidadCivilCoberturaCotizacion == null) {
							throw new RuntimeException("Paquete mal configurado ya que los paquetes que incluyen la cobertura de " +
									"daños por la carga deben tambien incluir la cobertura de responsabilidad civil.");
						}
						
						if (!responsabilidadCivilCoberturaCotizacion.getClaveContratoBoolean()) {
							cargaMasivaService
									.agregaError(
											"La cobertura responsabilidad civil debe estar contratada para poder contratar la cobertura de daños por la carga.",
											CoberturaDTO.DANOS_OCASIONADOS_CARGA_NOMBRE_COMERCIAL, numeroInciso, lineaDeNegocio,
											tmpErrors);
						}
						
						listErrores.addAll(tmpErrors);
						mensajeError.append(cargaMasivaService.generaMensajeError(tmpErrors));
						if (tmpErrors.size() == 0) {
							DatoIncisoCotAuto datoIncisoCotAutoTipoCarga = cargaMasivaService
									.obtieneDatoIncisoCotAutoCobertura(cotizacion,
											danosOcasionadosPorCargaCoberturaCotizacion, incisoCotizacion
													.getId().getNumeroInciso(),
											ConfiguracionDatoInciso.DescripcionEtiqueta.TIPO_CARGA, String.valueOf(detalle.getTipoCarga()
													.getId().getIdDato()));
	
							if (datoIncisoCotAutoTipoCarga == null) {
								throw new RuntimeException(
										"Error en la configuración de datos de inciso. Debe estar configurado el dato de inciso de tipo de carga para "
												+ "la cobertura de daños ocasionados por la carga.");
							}
							datoIncisoCotAutos.add(datoIncisoCotAutoTipoCarga);
						}
					}
					

					 
				    if (mensajeError.toString().isEmpty()) {
						incisoCotizacion = cargaMasivaService
								.guardaIncisoCotizacion(incisoCotizacion,
										coberturaCotizacionList, datoIncisoCotAutos);

						if (incisoCotizacion == null) {
							LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
							error.setNumeroInciso(new BigDecimal(fila));
							error.setNombreSeccion(lineaDeNegocio);
							error.setNombreCampoError("ERROR AL GUARDAR INCISO");
							error.setRecomendaciones("");
							listErrores.add(error);
							mensajeError.append("Error al guardar Inciso ").append("\r\n").append(System.getProperty("line.separator"));
						} 
				    }
				}
				detalle.setMensajeError(mensajeError.toString());
				if (mensajeError.toString().isEmpty()) {
					detalle.setClaveEstatus(DetalleCargaMasivaAutoCot.ESTATUS_COTIZADO);
					detalle.setNumeroInciso(incisoCotizacion.getId().getNumeroInciso());
					incisosCreadosList.add(incisoCotizacion);
				} else {
					detalle.setClaveEstatus(DetalleCargaMasivaAutoCot.ESTATUS_ERROR);
					//Elimina incisosCreados si existe error
					if(incisosCreadosList != null && !incisosCreadosList.isEmpty()){
						
							String mensajeElimina = cargaMasivaService.eliminarIncisosCreados(incisosCreadosList, cotizacion.getIdToCotizacion());
							if(mensajeElimina != null){
								LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
								error.setNumeroInciso(new BigDecimal(0));
								error.setNombreSeccion(" --- ");
								error.setNombreCampoError("ERROR AL ELIMINAR INCISO");
								error.setRecomendaciones("");
								listErrores.add(error);							
							}
					}
					break;
				}
			}
			mensajeError.delete(0,mensajeError.length());
		}//FOR
		
	}
		
	public boolean validaDeducible(List<NegocioDeducibleCob> deducibles, Double valor){
		boolean esValido = false;
		if(deducibles != null){
			for(NegocioDeducibleCob deducible : deducibles){
				if(deducible.getValorDeducible().equals(valor)){
					esValido = true;
					break;
				}
			}
		}
		return esValido;
	}
	
	public boolean validaRangos(Number valorMinimo, Number valorMaximo, Number valor){
		if (valor == null) {
			return true;
		}
		boolean esValido = true;
		if(valor.doubleValue() < valorMinimo.doubleValue() || valor.doubleValue() > valorMaximo.doubleValue()){
			esValido = false;
		}
		return esValido;
	}
	
	private boolean validaDiasSalarioMinimo(Number valor){
		if (valor == null) {
			return true;
		}
		
		boolean esValido = false;
		try {
			List<CatalogoValorFijoDTO> catalogoValorFijoDTOs = ValorFijoDN
					.getInstancia()
					.buscarPorPropiedad(
							"id.idGrupoValores",
							Integer.valueOf(CatalogoValorFijoDTO.IDGRUPO_DIAS_SALARIO));
			if (!catalogoValorFijoDTOs.isEmpty()
					&& catalogoValorFijoDTOs != null) {
					for (CatalogoValorFijoDTO estatus : catalogoValorFijoDTOs) {
						if(valor.doubleValue() == Double.parseDouble(estatus.getDescripcion())){
							esValido = true;
						}
					}
			}
		} catch (SystemException e) {
			e.printStackTrace();
		}
		return esValido;
	}		

	/***
	 * Obtener las condiciones especiales vinculadas al inciso
	 * @param detalleCargaMasivaList
	 * @return
	 */
	private Map<Long,List<CondicionEspecial>> preCargaIncisosCondiciones( List<DetalleCargaMasivaAutoCot> detalleCargaMasivaList ){
		
		Map<Long,List<CondicionEspecial>> lIncisosIniciales = new HashMap<Long,List<CondicionEspecial>>();
		
		// # OBTIENE LOS INCISOS ASOCIADOS A UNA COTIZACION
		List<IncisoCotizacionDTO> lKeyIncisosCotiazcion = this.cotizacion.getIncisoCotizacionDTOs();
		
		for( IncisoCotizacionDTO icd : lKeyIncisosCotiazcion ){
			
			lIncisosIniciales.put( Long.valueOf( icd.getId().getNumeroInciso().toString() ), icd.getCondicionesEspeciales() );
			icd.getCondicionesEspeciales();
		}
		
		return lIncisosIniciales;
	}
	
	private List<Long> preCargaPolizaCondiciones(List<CondicionEspecial> condiciones){
		List<Long> lCondicionesKey = new ArrayList<Long>();
		
		if(condiciones != null && condiciones.size() > 0 ){
			for(CondicionEspecial condicion :condiciones){
				lCondicionesKey.add(condicion.getId());
			}
		}
		
		return lCondicionesKey;
	}
	
	/**
	 * Valida los atributos con respecto a la base de datos
	 * @param detalleCargaMasivaList
	 * @param sheet
	 */
	public void validaDetalleCargaMasivaComp(List<DetalleCargaMasivaAutoCot> detalleCargaMasivaList, HSSFSheet sheet) {

		int fila = 1;
		int idNexConductoCobro=0;
		// # CARGA LOS INCISOS CON SUS CONDICIONES AGREGADAS
		Map<Long,List<CondicionEspecial>> lIncisosCondicionesIniciales = this.preCargaIncisosCondiciones(detalleCargaMasivaList);
		// # CARGA CONDICIONES AGREGADA A LA POLIZA
		List<Long> lPolizaCondicionesIniciales = this.preCargaPolizaCondiciones( cotizacion.getCondicionesEspeciales() );
		
		// # CONTENEDOR CON INCISOS Y CONDICIONES AGREGAR
		Map<Long,List<CondicionEspecial>> lKeyGlobalCondicionesInciso = new HashMap<Long,List<CondicionEspecial>>();
		// # CONTENEDOR DE ID INCISOS POR POLIZA
		Set<Long> lKeyGlobalCondicionesPoliza = new HashSet<Long>();
		StringBuilder mensajeError = new StringBuilder();
		for (DetalleCargaMasivaAutoCot detalle : detalleCargaMasivaList) {

			fila++;
			BigDecimal numeroInciso = new BigDecimal(fila);
			try{
				
			if (detalle.getClaveEstatus() != null && !detalle.getClaveEstatus().equals(
					DetalleCargaMasivaAutoCot.ESTATUS_ERROR)) {

				//String mensajeError = "";
				IncisoCotizacionDTO incisoCotizacion = cargaMasivaService
						.obtieneIncisoCotizacion(cotizacion,
								detalle.getNumeroInciso());

				incisoCotizacion.setIdMedioPago(new BigDecimal(detalle.getIdMedioPago()));
				if (!detalle.getConductoCobro().equals(VALOR_EFECTIVO)) {
					//Para guardar el cliente cob y el conducto de cobro
					ClienteDTO cliente = null;
					cliente = clienteFacadeRemote.findById(incisoCotizacion.getCotizacionDTO().getIdToPersonaContratante(),"nombreUsuario");
					if (cliente != null) {
						incisoCotizacion.setIdClienteCob(cliente.getIdCliente());
						if(detalle.getIdMedioPago()!=null){
							int conductoCobro = listadoService.getConductosCobro(cliente.getIdCliente().longValue(), new Integer(detalle.getIdMedioPago())).size()-1+idNexConductoCobro;
							incisoCotizacion.setIdConductoCobroCliente(new Long((conductoCobro>0)?(conductoCobro):1));
							idNexConductoCobro++;
						}
					}
					
					//Complementar medios de pago
					incisoCotizacion.setConductoCobro(detalle.getConductoCobro());
					incisoCotizacion.setInstitucionBancaria(detalle.getInstitucionBancaria());
					incisoCotizacion.setTipoTarjeta(detalle.getTipoTarjeta());
					incisoCotizacion.setNumeroTarjetaClave(detalle.getNumeroTarjetaClave());
					incisoCotizacion.setCodigoSeguridad(detalle.getCodigoSeguridad());
					incisoCotizacion.setFechaVencimiento(detalle.getFechaVencimiento());
				}
				
				//Correo
				if(detalle.getEmailContactos() != null){
					incisoCotizacion.setEmailContacto(detalle.getEmailContactos());
					cargaMasivaService.guardaIncisoCotizacion(incisoCotizacion);
				}
				
				//Bono Comision
				Boolean guardarCotizacion = false;
				if(cotizacion.getPorcentajebonifcomision() == null){
					cotizacion.setPorcentajebonifcomision(new Double(0));
					guardarCotizacion = true;
				}
				if(cotizacion.getPorcentajePagoFraccionado() == null){
					cotizacion.setPorcentajePagoFraccionado(new Double(0));
					guardarCotizacion = true;
				}
				if(guardarCotizacion){
					try{
						cargaMasivaService.guardarCotizacion(cotizacion);
					}catch(Exception e){
						
					}
				}
 
				if (incisoCotizacion == null) {
					LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
					error.setNumeroInciso(new BigDecimal(fila));
					error.setNombreSeccion("");
					error.setNombreCampoError("NUMERO DE INCISO");
					error.setRecomendaciones("No existe inciso");
					listErrores.add(error);
					mensajeError.append("Campo: NUMERO DE INCISO -  No existe inciso ").append("\r\n").append(System.getProperty("line.separator"));
				} else {

					// Asegurado
					if (detalle.getAsegurado() != null) {
						incisoCotizacion.getIncisoAutoCot().setNombreAsegurado(
								detalle.getAsegurado().toUpperCase());
						incisoCotizacion.getIncisoAutoCot().setPersonaAseguradoId(null);
					} else {
						if(cotizacion.getIdToPersonaContratante() != null && cotizacion.getNombreContratante() != null){
							incisoCotizacion.getIncisoAutoCot().setNombreAsegurado(
									cotizacion.getNombreContratante().toUpperCase());
							incisoCotizacion.getIncisoAutoCot().setPersonaAseguradoId(
									cotizacion.getIdToPersonaContratante().longValue());
						}
					}

					Boolean datosConductorObligatorios = false;
					try{
						datosConductorObligatorios = cargaMasivaService.datosDelConductorRequeridos(incisoCotizacion);
					}catch(Exception e){
						
					}
					
					if(datosConductorObligatorios){
						incisoCotizacion.getIncisoAutoCot().setNombreConductor(
							detalle.getNombre());
						incisoCotizacion.getIncisoAutoCot().setPaternoConductor(
							detalle.getApellidoPaterno());
						incisoCotizacion.getIncisoAutoCot().setMaternoConductor(
							detalle.getApellidoMaterno());
						incisoCotizacion.getIncisoAutoCot().setNumeroLicencia(
							detalle.getNumeroLicencia());
						
						//Valida fechaNacimiento
						GregorianCalendar gcFecha = new GregorianCalendar();
						gcFecha.setTime(new Date());
						gcFecha.add(GregorianCalendar.YEAR, -18);
						
						if(detalle.getFechaNacimiento().before(gcFecha.getTime())){
							incisoCotizacion.getIncisoAutoCot().setFechaNacConductor(detalle.getFechaNacimiento());
						}else{
							incisoCotizacion.getIncisoAutoCot().setFechaNacConductor(null);
							LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
							error.setNumeroInciso(new BigDecimal(fila));
							error.setNombreSeccion("");
							error.setNombreCampoError("FECHA NACIMIENTO CONDUCTOR");
							error.setRecomendaciones("Conductor debe ser mayor de edad");
							listErrores.add(error);
							mensajeError.append("Campo: ").append(error.getNombreCampoError()).append(" -  ").append(error.getRecomendaciones()).append(" ").append("\r\n").append(System.getProperty("line.separator"));
						}
						
						incisoCotizacion.getIncisoAutoCot().setOcupacionConductor(
							detalle.getOcupacion());
					}
					incisoCotizacion.getIncisoAutoCot().setNumeroMotor(
							detalle.getNumeroMotor());
					
					//String numeroSerieRempl = remplazarCaracteresNumSerie(detalle.getNumeroSerie());
					//incisoCotizacion.getIncisoAutoCot().setNumeroSerie(numeroSerieRempl);
					incisoCotizacion.getIncisoAutoCot().setNumeroSerie(detalle.getNumeroSerie());
					
					List<String> errors = NumSerieValidador.getInstance()
							.validate(
									incisoCotizacion.getIncisoAutoCot()
											.getNumeroSerie());
					if (!errors.isEmpty()) {
						/*
						LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
						error.setNumeroInciso(new BigDecimal(fila));
						error.setNombreSeccion("");
						error.setNombreCampoError("NUMERO DE SERIE");
						error.setRecomendaciones("Error al validar el n\u00FAmero de serie:");
						listErrores.add(error);
						mensajeError += "Campo: NUMERO DE SERIE -  " + errors.get(0) + "" + "\r\n" + System.getProperty("line.separator");
						*/
						HSSFCell cellStat = sheet.getRow((fila - 1)).getCell(CAMPO_ERROR_NUMERO_SERIE);
						if(cellStat == null){
							cellStat = sheet.getRow((fila - 1)).createCell(CAMPO_ERROR_NUMERO_SERIE);
						}
						cellStat.setCellValue("Error en construccion del n\u00FAmero de serie:" + detalle.getNumeroSerie());
					}
					incisoCotizacion.getIncisoAutoCot().setPlaca(
							detalle.getNumeroPlaca());
					incisoCotizacion.getIncisoAutoCot().setRepuve(
							detalle.getRepuve());
					
					
					//Valida datos de Riesgo
					List<CoberturaCotizacionDTO> coberturas = cargaMasivaService.obtieneCoberturasContratadas(incisoCotizacion);
					List<DatoIncisoCotAuto> datoIncisoCotAutos = new ArrayList<DatoIncisoCotAuto>();
					DatoIncisoCotAuto datoIncisoCotAutoEquipoEspecial = null;
					DatoIncisoCotAuto datoIncisoCotAutoAdaptacionConversion = null;
					for(CoberturaCotizacionDTO coberturaCotizacion : coberturas){
						if(coberturaCotizacion.getCoberturaSeccionDTO().getCoberturaDTO().getNombreComercial().toUpperCase().equals(
								 NOMBRE_LIMITE_EQUIPO_ESPECIAL.toUpperCase())){
							datoIncisoCotAutoEquipoEspecial = cargaMasivaService.obtieneDatoIncisoCotAuto(cotizacion, coberturaCotizacion, incisoCotizacion.getId().getNumeroInciso(), 
									detalle.getDescripcionEquipoEspecial());
							if(datoIncisoCotAutoEquipoEspecial == null){
								LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
								error.setNumeroInciso(new BigDecimal(fila));
								error.setNombreSeccion("");
								error.setNombreCampoError("DESCRIPCION EQUIPO ESPECIAL");
								error.setRecomendaciones("Error al validar Descripcion Equipo Especial");
								listErrores.add(error);
								mensajeError.append("Campo: DESCRIPCION EQUIPO ESPECIAL -  Error al validar Descripcion Equipo Especial ").append("\r\n").append(System.getProperty("line.separator"));
							}
							datoIncisoCotAutos.add(datoIncisoCotAutoEquipoEspecial);
						}
						if(coberturaCotizacion.getCoberturaSeccionDTO().getCoberturaDTO().getNombreComercial().toUpperCase().equals(
								 NOMBRE_LIMITE_ADAPTACION_CONVERSION.toUpperCase())){
							datoIncisoCotAutoAdaptacionConversion = cargaMasivaService.obtieneDatoIncisoCotAuto(cotizacion, coberturaCotizacion, incisoCotizacion.getId().getNumeroInciso(), 
									detalle.getDescripcionAdaptacionConversion());
							if(datoIncisoCotAutoAdaptacionConversion == null){
								LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
								error.setNumeroInciso(new BigDecimal(fila));
								error.setNombreSeccion("");
								error.setNombreCampoError("DESCRIPCION ADAPTACION Y CONVERSION");
								error.setRecomendaciones("Error al validar Descripcion Adaptacion y Conversion");
								listErrores.add(error);
								mensajeError.append("Campo: DESCRIPCION ADAPTACION Y CONVERSION -  Error al validar Descripcion Adaptacion y Conversion ").append("\r\n").append(System.getProperty("line.separator"));
							}
							datoIncisoCotAutos.add(datoIncisoCotAutoAdaptacionConversion);
						}
						
						if(detalle.getObservacionesInciso() != null){
							incisoCotizacion.getIncisoAutoCot().setObservacionesinciso(detalle.getObservacionesInciso());
						}
						
						if(coberturaCotizacion.getCoberturaSeccionDTO().getCoberturaDTO().getNombreComercial().toUpperCase().equals(
				 				CoberturaDTO.DANOS_OCASIONADOS_CARGA_NOMBRE_COMERCIAL.toUpperCase())){
							if (coberturaCotizacion.getClaveContratoBoolean()) {
								List<LogErroresCargaMasivaDTO> erroresObservaciones = new ArrayList<LogErroresCargaMasivaDTO>();
								cargaMasivaService.validaNoNulo(detalle.getObservacionesInciso(), NOMBRE_CAMPO_OBSERVACIONES_INCISO, numeroInciso, "", erroresObservaciones);

								if (erroresObservaciones.size() == 0) {
									incisoCotizacion.getIncisoAutoCot().setObservacionesinciso(detalle.getObservacionesInciso());
					 			} else {
						 			listErrores.addAll(erroresObservaciones);
						 			mensajeError.append(cargaMasivaService.generaMensajeError(erroresObservaciones));
					 			}
							}			
				 		}
					}
					

					
					if (mensajeError.toString().isEmpty()) {
						try{
							cargaMasivaService.guardarComplementarInciso(incisoCotizacion, datoIncisoCotAutos);
						}catch(Exception e){
							e.printStackTrace();
							LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
							error.setNumeroInciso(new BigDecimal(fila));
							error.setNombreSeccion("");
							error.setNombreCampoError("COMPLEMENTAR INCISO");
							error.setRecomendaciones("Error Inesperado");
							listErrores.add(error);
							mensajeError.append("Campo: ").append(error.getNombreCampoError()).append(" - ").append(error.getRecomendaciones()).append(": ").append(e.getLocalizedMessage()).append("").append("\r\n").append(System.getProperty("line.separator"));						
						}
					}
				}// cierra else if incisoCotizacion
				
				detalle.setMensajeError(mensajeError.toString());
				if (mensajeError.toString().isEmpty()) {
					detalle.setClaveEstatus(DetalleCargaMasivaAutoCot.ESTATUS_COTIZADO);
					if(incisoCotizacion.getNumeroSecuencia() != null){
						detalle.setNumeroInciso(new BigDecimal(incisoCotizacion.getNumeroSecuencia()));
					}else{
						detalle.setNumeroInciso(incisoCotizacion.getId().getNumeroInciso());
					}
					
					try{
						
						// # ALTA CONDICIONES ESPECIALES
						if( (detalle.getAltaCondicionesEspeciales()!=null) && !detalle.getAltaCondicionesEspeciales().isEmpty() ){
							
							List<CondicionEspecial> listCondicionesPorInciso = new ArrayList<CondicionEspecial>();
							
							// # OBTENER KEY Y VALOR DEL INCISO DEFINIDO EN LA PRECARGA
							List<CondicionEspecial> lBDIncisosIniciales = lIncisosCondicionesIniciales.get( Long.valueOf( detalle.getIncisoCotizacion().getId().getNumeroInciso().toString() ) );
							
							for(CondicionEspecial condicion : detalle.getAltaCondicionesEspeciales() ){
								
								if(CondicionEspecial.NivelAplicacion.POLIZA.getNivel() == condicion.getNivelAplicacion()){
									
									lKeyGlobalCondicionesPoliza.add(condicion.getId());
									
								}else if (CondicionEspecial.NivelAplicacion.INCISO.getNivel() == condicion.getNivelAplicacion()){									
									
									if (comparaCondicionesInciso(lBDIncisosIniciales, condicion)) {
										listCondicionesPorInciso.add(condicion);
									}
									
								}
								
							}
							
							// # ALMACENA LAS CONDICIONES A VINCULAR POR INCISO
							lKeyGlobalCondicionesInciso.put(Long.valueOf( detalle.getIncisoCotizacion().getId().getNumeroInciso().toString()),
									listCondicionesPorInciso);
							
						}
						
						// Eliminar condiciones especiales
						if( (detalle.getBajaCondicionesEspeciales() !=null) && !detalle.getBajaCondicionesEspeciales().isEmpty() ){
							this.eliminarCondicionesEspeciales(
									detalle.getBajaCondicionesEspeciales(), 
									cotizacion, 
									detalle.getIncisoCotizacion()
							);
						}						
						
					}catch (Exception e){
						
						e.printStackTrace();
						LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
						error.setNumeroInciso(new BigDecimal(fila));
						error.setNombreSeccion("");
						error.setNombreCampoError("COMPLEMENTAR INCISO");
						error.setRecomendaciones("Error Inesperado. No se pudieron vincular las condiciones especiales");
						listErrores.add(error);
					}
					
				} else {
					detalle.setClaveEstatus(DetalleCargaMasivaAutoCot.ESTATUS_ERROR);
				}
			}
			}catch(Exception e){
				e.printStackTrace();
				LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
				error.setNumeroInciso(new BigDecimal(fila));
				error.setNombreSeccion("");
				error.setNombreCampoError("COMPLEMENTAR INCISO");
				error.setRecomendaciones("Error Inesperado");
				listErrores.add(error);
			}
			mensajeError.delete(0,mensajeError.length());
		}// FOR
		
		
		// # PROCESAR ALTA DE CONDICIONES POR POLIZA E INCISO
		try{
			
			this.procesaCondicionesInciso(lKeyGlobalCondicionesInciso);
			this.procesaCondicionesPoliza(lKeyGlobalCondicionesPoliza, lPolizaCondicionesIniciales);
			
		}catch(Exception e){
			e.printStackTrace();
			LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
			error.setNumeroInciso( BigDecimal.ZERO );
			error.setNombreSeccion("");
			error.setNombreCampoError("Error al procesar la alta de condiciones especiales ");
			error.setRecomendaciones("Error Inesperado");
			listErrores.add(error);
		}
		
	}

	/***
	 * Procesa las condiciones de poliza
	 * @param lKeyGlobalCondicionesPoliza - Condiciones que vienen del excel
	 * @param lPolizaCondicionesIniciales - Condiciones que viene cargas inicialmente 
	 */
	private void procesaCondicionesPoliza(Set<Long> lKeyGlobalCondicionesPoliza, List<Long> lPolizaCondicionesIniciales){
		
		// # ELIMINAR DE lPolizaCondicionesIniciales LLAVES QUE VENGAN EN lKeyGlobalCondicionesPoliza
		if( !lPolizaCondicionesIniciales.isEmpty() ){
			if( !lPolizaCondicionesIniciales.isEmpty() ){
				for( Long lPolizaIniciales : lPolizaCondicionesIniciales ){
					if( lKeyGlobalCondicionesPoliza.contains(lPolizaIniciales) ){
						lKeyGlobalCondicionesPoliza.remove( lPolizaIniciales );
					}
				}
			}
		}
		
		System.out.println( lKeyGlobalCondicionesPoliza.size() );
		
		// # GUARDA CONDICIONES ESPECIAL
		
		if( !lKeyGlobalCondicionesPoliza.isEmpty() ){
			for( Long lPoliza : lKeyGlobalCondicionesPoliza ){
				condicionEspecialCotizacionService.agregarCondicionEspecial(cotizacion.getIdToCotizacion(), null, lPoliza );
			}
		}
		
	}
	
	private void procesaCondicionesInciso(Map<Long,List<CondicionEspecial>> lKeyGlobalCondicionesInciso){
		
		if( !lKeyGlobalCondicionesInciso.isEmpty() ){
			
			for (Entry<Long, List<CondicionEspecial>> entry : lKeyGlobalCondicionesInciso.entrySet() ){
			    
			    // # ITERAR LISTA DE CONDICIONES ESPECIALES VINCULADAS A UN INCISO
			    if( !entry.getValue().isEmpty() ){
			    	for( CondicionEspecial lCondicion : entry.getValue() ){
				    	condicionEspecialCotizacionService.agregarCondicionEspecial(cotizacion.getIdToCotizacion(), BigDecimal.valueOf(entry.getKey()) , lCondicion.getId() );
				    }
			    }
			    
			}

		}
	}
	
	private boolean comparaCondicionesInciso(List<CondicionEspecial> lBDIncisosIniciales, CondicionEspecial condicion) {
		
		for( CondicionEspecial a1 : lBDIncisosIniciales ){
			if (a1.getCodigo() == condicion.getCodigo()) {
				return false;
			}
		}
		
		return true;
	}

	private Short validaSINO(String campo) {
		Short valor = 0;
		if (campo.equals(VALOR_SI)) {
			valor = 1;
		}
		return valor;
	}

	public FileInputStream descargaLogErrores(BigDecimal idControlArchivo) {
		try {
			ControlArchivoDN controlArchivoDN = ControlArchivoDN.getInstancia();
			controlArchivoDTO = controlArchivoDN.getPorId(idControlArchivo);

			File file = new File(uploadFolder + "Log"
					+ controlArchivoDTO.getIdToControlArchivo() + ".txt");

			return new FileInputStream(file);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public InputStream descargaLogErroresInciso(String textoError) {

		InputStream is = null;
		//out.write("N\u00FAmero de L\u00EDnea |  Nombre de la secci\u00F3n |   Nombre de campo que no cumpli\u00F3 con la validaci\u00F3n \r\n" + System.getProperty("line.separator"));
        try {
            is = new ByteArrayInputStream(textoError.getBytes("UTF-8"));
        } catch (UnsupportedEncodingException e) {
            textoError = e.getMessage();
            try {
				is = new ByteArrayInputStream(textoError.getBytes("UTF-8"));
			} catch (Exception e1) {
				e1.printStackTrace();
			}
        }
		return is;
	}
	
	public InputStream descargarPolizasCargaMasiva(BigDecimal idToCargaMasivaIndAutoCot, Locale locale){
		
		try {
			List<DetalleCargaMasivaIndAutoCot> cargaMasivaDetalleIndividualList = cargaMasivaService.getCargaMasivaDetalleIndividualList(idToCargaMasivaIndAutoCot);
			List<ControlDescripcionArchivoDTO> plantillaList = new ArrayList<ControlDescripcionArchivoDTO>(1);
			for(DetalleCargaMasivaIndAutoCot item : cargaMasivaDetalleIndividualList){
				try{
					if(item != null && (item.getClaveEstatus().equals(DetalleCargaMasivaIndAutoCot.ESTATUS_EMITIDO) ||
							item.getClaveEstatus().equals(DetalleCargaMasivaIndAutoCot.ESTATUS_EMITIDO_ANT))
							&& item.getIdToPoliza() != null){
						List<EndosoDTO> endosoPolizaList = cargaMasivaService.getEndosos(item.getIdToPoliza());
						DateTime validOnDT = null;
						DateTime recordFromDT = null;
						Short claveTipoEndoso = 2;
						if(endosoPolizaList != null && !endosoPolizaList.isEmpty()){
							EndosoDTO primerEndoso = endosoPolizaList.get(0);
						
							validOnDT = new DateTime(primerEndoso.getValidFrom().getTime());
							recordFromDT = new DateTime(primerEndoso.getRecordFrom().getTime());
							claveTipoEndoso = primerEndoso.getClaveTipoEndoso();
						}		
						TransporteImpresionDTO transporte = cargaMasivaService.imprimirPoliza(item.getIdToPoliza(), locale, validOnDT,
							recordFromDT, claveTipoEndoso);
						
						if(transporte != null && transporte.getByteArray() != null && transporte.getByteArray().length > 0){
							InputStream plantillaInputStream = new ByteArrayInputStream(transporte.getByteArray());
							ControlDescripcionArchivoDTO cda = new ControlDescripcionArchivoDTO();
							cda.setExtension(".pdf");							
							PolizaDTO poliza = cargaMasivaService.getPolizaByStringId(item.getIdToPoliza().toString());
							cda.setNombreArchivo(poliza.getNumeroPolizaFormateada());
							cda.setInputStream(plantillaInputStream);
							plantillaList.add(cda);
						}
					}
				}catch(Exception e){
					e.printStackTrace();
				}
			}
			
			if(plantillaList.size() > 0){
				return armaZip(plantillaList);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public void procesaCargasPendientes(){
		//Obtiene cargas pendientes
		List<CargaMasivaAutoCot> cargasPendientes = cargaMasivaService.obtieneCargasMasivasPendientes();
		for(CargaMasivaAutoCot carga: cargasPendientes){
			try{
			setDetalleCargaMasivaAutoCotList(new ArrayList<DetalleCargaMasivaAutoCot>(1));
			setHasLogErrors(false);
			
			CotizacionDTO cotizacion = cargaMasivaService.obtieneCotizacion(carga);
			validaCargaMasiva(cotizacion, carga.getIdToControlArchivo(), TIPO_CARGA_NORMAL);
			
			if(!isHasLogErrors()){
				carga.setClaveEstatus(CargaMasivaAutoCot.ESTATUS_TERMINADO);
				cargaMasivaService.guardaCargaMasivaAutoCot(carga);
			}else{
				carga.setClaveEstatus(CargaMasivaAutoCot.ESTATUS_CON_ERROR);
				cargaMasivaService.guardaCargaMasivaAutoCot(carga);				
			}
			
			cargaMasivaService.guardaDetalleCargaMasiva(carga, getDetalleCargaMasivaAutoCotList());
			}catch(Exception e){
				
			}
		}
	}

	public void setCotizacion(CotizacionDTO cotizacion) {
		this.cotizacion = cotizacion;
	}

	public CotizacionDTO getCotizacion() {
		return cotizacion;
	}

	public void setCargaMasivaService(CargaMasivaService cargaMasivaService) {
		this.cargaMasivaService = cargaMasivaService;
	}

	public CargaMasivaService getCargaMasivaService() {
		return cargaMasivaService;
	}

	public void setListErrores(List<LogErroresCargaMasivaDTO> listErrores) {
		this.listErrores = listErrores;
	}

	public List<LogErroresCargaMasivaDTO> getListErrores() {
		return listErrores;
	}

	public void setLogErrores(FileInputStream logErrores) {
		this.logErrores = logErrores;
	}

	public FileInputStream getLogErrores() {
		return logErrores;
	}

	public void setHasLogErrors(boolean hasLogErrors) {
		this.hasLogErrors = hasLogErrors;
	}

	public boolean isHasLogErrors() {
		return hasLogErrors;
	}

	public void setDetalleCargaMasivaAutoCotList(
			List<DetalleCargaMasivaAutoCot> detalleCargaMasivaAutoCotList) {
		this.detalleCargaMasivaAutoCotList = detalleCargaMasivaAutoCotList;
	}

	public List<DetalleCargaMasivaAutoCot> getDetalleCargaMasivaAutoCotList() {
		return detalleCargaMasivaAutoCotList;
	}

	public void setTipoCarga(Short tipoCarga) {
		this.tipoCarga = tipoCarga;
	}

	public Short getTipoCarga() {
		return tipoCarga;
	}

	public void setEjemplo(DetalleCargaMasivaAutoCot ejemplo) {
		this.ejemplo = ejemplo;
	}

	public DetalleCargaMasivaAutoCot getEjemplo() {
		return ejemplo;
	}

}
