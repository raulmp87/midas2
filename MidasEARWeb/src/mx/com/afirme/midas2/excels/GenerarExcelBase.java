package mx.com.afirme.midas2.excels;

import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.Deflater;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDTO;
import mx.com.afirme.midas.sistema.controlArchivo.ControlArchivoDTO;
import mx.com.afirme.midas2.domain.condicionesespeciales.CondicionEspecial;
import mx.com.afirme.midas2.domain.condicionesespeciales.CondicionEspecial.NivelAplicacion;
import mx.com.afirme.midas2.domain.negocio.condicionespecial.NegocioCondicionEspecial;
import mx.com.afirme.midas2.dto.ControlDescripcionArchivoDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.cargamasiva.LogErroresCargaMasivaDTO;
import mx.com.afirme.midas2.service.bitemporal.endoso.inciso.IncisoSoporteService;
import mx.com.afirme.midas2.service.catalogos.condicionesespeciales.CondicionEspecialService;
import mx.com.afirme.midas2.service.componente.condicionespecial.CondicionEspecialCotizacionService;
import mx.com.afirme.midas2.service.negocio.condicionespecial.NegocioCondicionEspecialService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.CotizacionService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.cargaMasiva.CargaMasivaService;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.jfree.util.Log;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/** Clase abstracta que define los métodos a usar para 
 * la generación de los excel de carga masiva: flotillas e indiviual 
**/ 

@Component
@Scope("prototype")
public abstract class GenerarExcelBase {
	
	
	protected static final String REGEXT_INT = "^\\d+$";
	protected int dataRows = 0;
	protected int maxRows = 15000;
	protected String uploadFolder;
	protected ControlArchivoDTO controlArchivoDTO;
	protected boolean hasLogErrors = false;
	protected List<LogErroresCargaMasivaDTO> listErrores = new ArrayList<LogErroresCargaMasivaDTO>(1);
	protected FileInputStream logErrores;
	protected Short tipoCarga;
	private Map<Long, CondicionEspecial > pilaCondiciones = new LinkedHashMap<Long,CondicionEspecial>();
	
	
	//private int maxColumns = 26;
	
	protected static final String SEPARADOR_CONDICION_ESPECIAL		= ",";
	protected static final String METODO_ALTA						= "ALTA";
	protected static final String METODO_BAJA						= "BAJA";
	private static final int VALOR_TRUE								= 1;
	
	private static final String NIVEL_APLICACION_INCISO						= "Inciso";
	private static final String NIVEL_APLICACION_POLIZA						= "Poliza";
	private static final String NIVEL_APLICACION_TODAS						= "Todas";
	
	
	protected CargaMasivaService cargaMasivaService;
	protected CotizacionService cotizacionService;	
	protected NegocioCondicionEspecialService negocioCondicionEspecialService;
	protected IncisoSoporteService incisoSoporteService;
	protected CondicionEspecialService condicionEspecialService;
	protected CondicionEspecialCotizacionService condicionEspecialCotizacionService;
	
	
	public InputStream armaZip(List<ControlDescripcionArchivoDTO> plantillaList) throws Exception{
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
	    ZipOutputStream os = new ZipOutputStream(baos);
		os.setLevel(Deflater.DEFAULT_COMPRESSION);
		os.setMethod(Deflater.DEFLATED);
		try {
			int i = 1;
			for(ControlDescripcionArchivoDTO transporte: plantillaList){
				ZipEntry entrada = new ZipEntry(transporte.getNombreArchivo()+transporte.getExtension());
				os.putNextEntry(entrada);
				
				InputStream fis = transporte.getInputStream();
				byte [] buffer = new byte[1024];
				int leido=0;
				while (0 < (leido=fis.read(buffer))){
					os.write(buffer,0,leido);
				}
				fis.close();
				os.closeEntry();
				i++;
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			os.close();
		}
		InputStream is = new ByteArrayInputStream(baos.toByteArray());
		baos.close();
		
		return is;
	}
	
	public HSSFWorkbook cargaArchivodeCarga(String archivo) throws IOException {
		HSSFWorkbook workbook = null;
		File file = new File(archivo);
		POIFSFileSystem plantilla = new POIFSFileSystem(new FileInputStream(
				file));
		workbook = new HSSFWorkbook(plantilla);
		return workbook;
	}
	
	public HSSFWorkbook cargaPlantilla(String url ) throws IOException {
		HSSFWorkbook workbook = null;
		POIFSFileSystem plantilla = new POIFSFileSystem(
				this.getClass()
						.getResourceAsStream(url));
		workbook = new HSSFWorkbook(plantilla);
		return workbook;
	}
	
	public void creaLogErrores() {
		if (!this.getListErrores().isEmpty()) {
	 		File file = new File(uploadFolder + "Log"
					+ controlArchivoDTO.getIdToControlArchivo() + ".txt");
			try {
				FileWriter outFile = new FileWriter(file);
				//PrintWriter out = new PrintWriter(outFile);

				BufferedWriter out = new BufferedWriter(outFile);
				//out.println("N\u00FAmero de L\u00EDnea |  Nombre de la secci\u00F3n |   Nombre de campo que no cumpli\u00F3 con la validaci\u00F3n");
				out.write("N\u00FAmero de L\u00EDnea |  Nombre de la secci\u00F3n |   Nombre de campo que no cumpli\u00F3 con la validaci\u00F3n  |  Recomendaciones \r\n" + System.getProperty("line.separator"));
				out.newLine();
				for (LogErroresCargaMasivaDTO error : this.getListErrores()) {
					/*out.println(error.getNumeroInciso() + " | "
							+ error.getNombreSeccion() + " | "
							+ error.getNombreCampoError() + "\r\n");
							*/
					out.write(" " + error.getNumeroInciso() + " | "
					+ error.getNombreSeccion() 		+ " | "
					+ error.getNombreCampoError()  	+ " | " 
					+ error.getRecomendaciones()	+ "\r\n" + System.getProperty("line.separator"));
					out.newLine();
				}
				out.flush();
				out.close();
				outFile.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	public String obtenerNombreArchivo(ControlArchivoDTO controlArchivoDTO) {
		String fileName = controlArchivoDTO.getNombreArchivoOriginal();
		String extension = (fileName.lastIndexOf(".") == -1) ? "" : fileName
				.substring(fileName.lastIndexOf("."), fileName.length());
		return controlArchivoDTO.getIdToControlArchivo().toString() + extension;
	}
	
	public String remplazaEspacios(String valor) {
		return valor.replaceAll(" ", "_");
	}
	
	public String remplazaGuion(String valor) {
		return valor.replaceAll("_", " ");
	}
	
	public String remplazarCaracteresNumSerie(String numeroSerie){
		String nuevoNumeroSerie = numeroSerie.toUpperCase();
		
		nuevoNumeroSerie = nuevoNumeroSerie.replaceAll("\u00D1", "N");
		nuevoNumeroSerie = nuevoNumeroSerie.replaceAll("O", "0");
		nuevoNumeroSerie = nuevoNumeroSerie.replaceAll("Q", "0");
		nuevoNumeroSerie = nuevoNumeroSerie.replaceAll("I", "1");
		nuevoNumeroSerie = nuevoNumeroSerie.replaceAll(" ", "");
		
		return nuevoNumeroSerie;
	}
	
	public boolean validaRangos(Number valorMinimo, Number valorMaximo, Number valor){
		if (valor == null) {
			return true;
		}
		boolean esValido = true;
		if(valor.doubleValue() < valorMinimo.doubleValue() || valor.doubleValue() > valorMaximo.doubleValue()){
			esValido = false;
		}
		return esValido;
	}
	
	protected long[] processInfoCellCondicionEspecial( String cadena ){
		
		String[] info 						= cadena.split(SEPARADOR_CONDICION_ESPECIAL);
		int tamano 							= info.length;
		long[] idCondicionesEspeciales 		= new long[tamano];
		
		for(int iterador=0 ;iterador<tamano;iterador++){
			idCondicionesEspeciales[iterador] = Long.parseLong( info[iterador] );
		}
		
		return idCondicionesEspeciales;
	}
	
	protected void createCondicionesSheet(HSSFSheet sheet, BigDecimal idToNegocio, NivelAplicacion nivelAplicacion){
		
		List<NegocioCondicionEspecial> listaCondicionesEspeciales = negocioCondicionEspecialService.obtenerCondicionesNegocio(idToNegocio.longValue(),nivelAplicacion);
		this.grabaCondicionesExcel(listaCondicionesEspeciales, sheet, idToNegocio);
	}
	
	protected void createCondicionesSheet(HSSFSheet sheet, BigDecimal idToNegocio){

		List<NegocioCondicionEspecial> listaCondicionesEspeciales = negocioCondicionEspecialService.obtenerCondicionesNegocio(idToNegocio.longValue(),NivelAplicacion.TODAS);
		this.grabaCondicionesExcel(listaCondicionesEspeciales, sheet, idToNegocio);

	}
	
	private void grabaCondicionesExcel(List<NegocioCondicionEspecial> listaCondicionesEspeciales,HSSFSheet sheet, BigDecimal idToNegocio ){
		
		dataRows = 1;
		HSSFRow row = null;
		HSSFCell cell = null;
		
		// Graba titulos cabecera hoja adicional
		this.grabaCabeceraCondicionesEspeciales(sheet);
		
		if (listaCondicionesEspeciales != null && !listaCondicionesEspeciales.isEmpty()) {
			for (NegocioCondicionEspecial item : listaCondicionesEspeciales) {
				row = sheet.createRow(dataRows);
				dataRows++;
				int column = 0;
				
				sheet.autoSizeColumn(column);
				printCell(cell, row, column, String.valueOf(item.getCondicionEspecial().getCodigo()) );
				
				column++;
				sheet.autoSizeColumn(column);
				printCell(cell, row, column, item.getCondicionEspecial().getNombre() );
				
				column++;
				sheet.autoSizeColumn(column);
				printCell(cell, row, column, item.getCondicionEspecial().getDescripcion() );
				
				column++;
				sheet.autoSizeColumn(column);
				printCell(cell, row, column, getTipoCondicion(item.getCondicionEspecial()) );
				
				column++;
				sheet.autoSizeColumn(column);
				printCell(cell, row, column, validateObligatoria(item) );
			}
		}
	}
	
	private String getTipoCondicion(CondicionEspecial condicionEspecial){
		String resultado = "";
		
		if( condicionEspecial.getNivelAplicacion().equals( NivelAplicacion.INCISO.getNivel())  ){
			resultado = NIVEL_APLICACION_INCISO;
		}else if( condicionEspecial.getNivelAplicacion().equals( NivelAplicacion.POLIZA.getNivel()) ){
			resultado = NIVEL_APLICACION_POLIZA;
		}else if( condicionEspecial.getNivelAplicacion().equals( NivelAplicacion.TODAS.getNivel()) ){
			resultado = NIVEL_APLICACION_TODAS;
		}
		
		return resultado;
	}
	
	/**
	 * Graba titulos de cabecera en la pestaña secundaria
	 * @param sheet
	 */
	private void grabaCabeceraCondicionesEspeciales(HSSFSheet sheet){
		// Instancia para generar una fila
		HSSFRow row = sheet.createRow(0);
		HSSFCell cell = null;
		
		String[] titulos = {"Código","Nombre","Descripción","Tipo de condición","Obligatoriedad"};
		
		for(int i=0; i<=titulos.length-1; i++){
			this.printCell(cell, row, i,String.valueOf(titulos[i]) );
		}
		
	}
	
	private String validateObligatoria(NegocioCondicionEspecial negocioCondicionEspecial){
		String resultado = "";
		
		if( negocioCondicionEspecial.getObligatoria() == VALOR_TRUE){
			resultado = "Si";
		}else{
			resultado = "No";
		}
		return resultado;
	}
	
	
	private void printCell(HSSFCell cell ,HSSFRow row,int column ,String valor){
		cell = row.createCell(column);
		cell.setCellValue(valor);
	}
	
	
	

	protected List<CondicionEspecial> convertirCondicionesEspeciales(String[] codigosCondiciones, BigDecimal fila , String metodo){
		
		List<CondicionEspecial> listCondicionEspecial 	= new ArrayList<CondicionEspecial>();
		CondicionEspecial condicionEspecial 			= null;
		LogErroresCargaMasivaDTO error 					= null;
		int tamano 										= codigosCondiciones.length;
		long codigo 									= 0;
		
		
		for(int iterador=0 ; iterador<tamano;iterador++){
			codigo = Long.parseLong( codigosCondiciones[iterador] );
			condicionEspecial = this.contieneCondicionEspecial(codigo, fila, metodo);
			if(condicionEspecial == null){
				error = new LogErroresCargaMasivaDTO();
				error.setNumeroInciso(fila);
				error.setNombreSeccion("");
				error.setNombreCampoError("CONDICION NO REGISTRADA "+ metodo + " - " +codigosCondiciones[iterador]);
				error.setRecomendaciones("Valor no valido");
				listErrores.add(error);
			}else{
				listCondicionEspecial.add(condicionEspecial);
			}
		}
		
		return listCondicionEspecial;
	}
	
	private CondicionEspecial contieneCondicionEspecial( long codigo , BigDecimal fila, String metodo ){
		CondicionEspecial condicionEspecial = null;
		LogErroresCargaMasivaDTO error 		= null;
		
		try{
			if( this.pilaCondiciones.containsKey(codigo) ){
				return pilaCondiciones.get(codigo);
			}else{
				condicionEspecial = condicionEspecialService.obtenerCondicionCodigo( codigo );
				this.pilaCondiciones.put(codigo, condicionEspecial);
				return condicionEspecial;
			}
		}catch(Exception e){
			error = new LogErroresCargaMasivaDTO();
			error.setNumeroInciso(fila);
			error.setNombreSeccion("");
			error.setNombreCampoError("CONDICION NO REGISTRADA "+ metodo + " - " +codigo);
			error.setRecomendaciones("Valor no valido");
			listErrores.add(error);
		}
		
		return condicionEspecial;
	}
	
	protected String validarCondicionesAlta(long idToNegocio, String[] condiciones , BigDecimal fila ){
		
		List<String> listCodigosCondiciones 		= null;
		List<String> listCodigoCondicionesErroneas 	= null;
		listCodigosCondiciones 						= transformArrayToList(condiciones);
		String mensajeError 						= "";
		
		listCodigoCondicionesErroneas = negocioCondicionEspecialService.validaCondicionEspecialAlta(idToNegocio, listCodigosCondiciones); 
			
		if( listCodigoCondicionesErroneas != null & !listCodigoCondicionesErroneas.isEmpty()){
			for(String codigo:listCodigoCondicionesErroneas){
				listErrores.add( crearError(fila, codigo, "CONDICION NO PERTENECE AL NEGOCIO - ", "Valor no valido") );
			}
			mensajeError += "Campo: ALTA DE CONDICION ESPECIAL -  Error al validar Condicion Especial ";
		}
		
		return mensajeError;
	}
	
	protected String validarCondicionesBaja(long idToNegocio, String[] condiciones , BigDecimal fila){
		
		List<String> listCodigosCondiciones 		= null;
		List<String> listCodigoCondicionesErroneas 	= null;
		LogErroresCargaMasivaDTO error 				= null;
		listCodigosCondiciones 						= transformArrayToList(condiciones);
		String mensajeError 						= "";
		
		listCodigoCondicionesErroneas = negocioCondicionEspecialService.validaCondicionEspecialAlta(idToNegocio, listCodigosCondiciones); 
		
		if( listCodigoCondicionesErroneas != null & !listCodigoCondicionesErroneas.isEmpty() ){
			for(String codigo:listCodigoCondicionesErroneas){
				listErrores.add( crearError(fila, codigo, "CONDICION NO PERTENECE AL NEGOCIO,NO SE PUEDE DAR DE BAJA  - ", "Valor no valido") );
			}
			
			mensajeError += "Campo: BAJA DE CONDICION ESPECIAL -  Error al validar Condicion Especial ";
		}
		
		listCodigoCondicionesErroneas = negocioCondicionEspecialService.validaCondicionEspecialBaja(idToNegocio, listCodigosCondiciones); 
		
		if( listCodigoCondicionesErroneas != null & !listCodigoCondicionesErroneas.isEmpty() ){
			for(String codigo:listCodigoCondicionesErroneas){
				listErrores.add( crearError(fila, codigo, "CONDICION ES OBLIGATORIA,NO SE PUEDE DAR DE BAJA  - ", "Valor no valido") );
			}
			mensajeError += "Campo: BAJA DE CONDICION ESPECIAL -  Condicion Obligatoria";
		}
			
		return mensajeError;
	}
	
	public LogErroresCargaMasivaDTO crearError(BigDecimal fila , String codigo , String leyenda , String recomendacion){
		LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
		error.setNumeroInciso(fila);
		error.setNombreSeccion("");
		error.setNombreCampoError(leyenda + codigo);
		error.setRecomendaciones(recomendacion);
		
		return error;
	}
	
	
	private List<String> transformArrayToList(String[] condiciones){
		
		List<String> listCodigosCondiciones 	= new ArrayList<String>();
		int tamano 								= condiciones.length;
		
		for(int iterador=0;iterador < tamano;iterador++){
			listCodigosCondiciones.add( condiciones[ iterador ]);
		}
		
		return listCodigosCondiciones;
	}
	
	protected void guardarCondicionesEspeciales(List<CondicionEspecial> condiciones, CotizacionDTO cotizacion, IncisoCotizacionDTO inciso){		
		if(condiciones != null && condiciones.size() > 0 ){
			for(CondicionEspecial condicion :condiciones){
				if(CondicionEspecial.NivelAplicacion.POLIZA.getNivel() == condicion.getNivelAplicacion()){
					condicionEspecialCotizacionService.agregarCondicionEspecial(cotizacion.getIdToCotizacion(), null, condicion.getId());
				}else if (CondicionEspecial.NivelAplicacion.INCISO.getNivel() == condicion.getNivelAplicacion()){
					condicionEspecialCotizacionService.agregarCondicionEspecial(cotizacion.getIdToCotizacion(), inciso.getId().getNumeroInciso(), condicion.getId());
				}
			}
		}		
	}
	
	
	
	protected void eliminarCondicionesEspeciales(List<CondicionEspecial> condiciones,CotizacionDTO cotizacion, IncisoCotizacionDTO inciso){
		if(condiciones != null && condiciones.size() > 0){
			for(CondicionEspecial condicion :condiciones){
				if(CondicionEspecial.NivelAplicacion.POLIZA.getNivel() == condicion.getNivelAplicacion()){
					condicionEspecialCotizacionService.removerCondicionEspecial(cotizacion.getIdToCotizacion(), null, condicion.getId());
				}else if (CondicionEspecial.NivelAplicacion.INCISO.getNivel() == condicion.getNivelAplicacion()){
					condicionEspecialCotizacionService.removerCondicionEspecial(cotizacion.getIdToCotizacion(), inciso.getId().getNumeroInciso(), condicion.getId());
				}
			}
		}		
	}
	
	protected String validarFecha(String inDate) {
		SimpleDateFormat creditCardFormat = new SimpleDateFormat("MM/yy");
		SimpleDateFormat dateFormat = new SimpleDateFormat();
		String fechaVencimientoTarjeta = null;

		dateFormat.setLenient(false);
		creditCardFormat.setLenient(false);

		try {
			dateFormat.applyPattern("dd/MM/yyyy");
			fechaVencimientoTarjeta = creditCardFormat.format(dateFormat.parse(inDate.trim()));

		} catch (ParseException par) {
			try {
				dateFormat.applyPattern("dd-MMM-yyyy");
				fechaVencimientoTarjeta = creditCardFormat.format(dateFormat.parse(inDate.trim()));
			} catch (ParseException pe) {
				try {
					dateFormat.applyPattern("MM-yy");
					fechaVencimientoTarjeta = creditCardFormat.format(dateFormat.parse(inDate.trim()));
				} catch (ParseException pex) {
					try {
						dateFormat.applyPattern("MM/yy");
						fechaVencimientoTarjeta = creditCardFormat.format(dateFormat.parse(inDate.trim()));
					} catch (ParseException pars) {
						fechaVencimientoTarjeta = null;
					}
				}
			}
		}

		return fechaVencimientoTarjeta;
	}

	protected boolean validaVigenciaTarjeta(String vigencia) {
		Calendar cal = Calendar.getInstance();
		String[] fecha = vigencia.split("/");
		
		Integer mesVigencia = Integer.valueOf(fecha[0]) - 1;
		if(mesVigencia < 0 || mesVigencia > 11) {
			return false;
		}
		if(fecha[1].length() != 2){
			return false;
		}
		
		String anioActual = String.valueOf(cal.get(Calendar.YEAR));
		Integer anioVigencia = Integer.valueOf(anioActual.substring(0, 2) + fecha[1]);		
		
	    cal.set(Calendar.MONTH, mesVigencia);
	    cal.set(Calendar.YEAR, anioVigencia);
	    cal.set(Calendar.DAY_OF_MONTH, 1);
	    cal.set(Calendar.DATE, cal.getActualMaximum(Calendar.DATE));	    
	    
	    Date hoy = new Date();
	    return hoy.before(cal.getTime());
	
	}
	
	/**
	 * Valida si el string es un numero - se usa para validar las condiciones especiales que solo se ingresen digitos
	 * @param numero
	 * @return
	 */
	protected boolean isInt(String numero){
		return numero.matches(REGEXT_INT);
	}
	
	protected boolean isDouble(String numero){
		try{
			Double.parseDouble(String.valueOf( numero) );
			return true;
		}catch(Exception e){
			return false;
		}
	}
	
	protected abstract void buildDataSheet(HSSFSheet sheet);
	protected abstract void buildValidationsSheet(HSSFSheet sheet);
	protected abstract void leeLineasArchivoCarga(HSSFSheet sheet);
	
	
	
	public void setListErrores(List<LogErroresCargaMasivaDTO> listErrores) {
		this.listErrores = listErrores;
	}

	public List<LogErroresCargaMasivaDTO> getListErrores() {
		return listErrores;
	}
	
	public void setLogErrores(FileInputStream logErrores) {
		this.logErrores = logErrores;
	}

	public FileInputStream getLogErrores() {
		return logErrores;
	}

	public void setHasLogErrors(boolean hasLogErrors) {
		this.hasLogErrors = hasLogErrors;
	}

	public boolean isHasLogErrors() {
		return hasLogErrors;
	}

	public void setTipoCarga(Short tipoCarga) {
		this.tipoCarga = tipoCarga;
	}

	public Short getTipoCarga() {
		return tipoCarga;
	}

	public CotizacionService getCotizacionService() {
		return cotizacionService;
	}

	public void setCotizacionService(CotizacionService cotizacionService) {
		this.cotizacionService = cotizacionService;
	}

	public NegocioCondicionEspecialService getNegocioCondicionEspecialService() {
		return negocioCondicionEspecialService;
	}

	public void setNegocioCondicionEspecialService(
			NegocioCondicionEspecialService negocioCondicionEspecialService) {
		this.negocioCondicionEspecialService = negocioCondicionEspecialService;
	}

	public IncisoSoporteService getIncisoSoporteService() {
		return incisoSoporteService;
	}

	public void setIncisoSoporteService(IncisoSoporteService incisoSoporteService) {
		this.incisoSoporteService = incisoSoporteService;
	}

	public CargaMasivaService getCargaMasivaService() {
		return cargaMasivaService;
	}

	public void setCargaMasivaService(CargaMasivaService cargaMasivaService) {
		this.cargaMasivaService = cargaMasivaService;
	}

	public CondicionEspecialService getCondicionEspecialService() {
		return condicionEspecialService;
	}

	public void setCondicionEspecialService(
			CondicionEspecialService condicionEspecialService) {
		this.condicionEspecialService = condicionEspecialService;
	}

	public CondicionEspecialCotizacionService getCondicionEspecialCotizacionService() {
		return condicionEspecialCotizacionService;
	}

	public void setCondicionEspecialCotizacionService(
			CondicionEspecialCotizacionService condicionEspecialCotizacionService) {
		this.condicionEspecialCotizacionService = condicionEspecialCotizacionService;
	}

	
	
	
	
}
