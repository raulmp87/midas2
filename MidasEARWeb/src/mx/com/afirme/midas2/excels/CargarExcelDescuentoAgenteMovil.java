package mx.com.afirme.midas2.excels;
import java.io.FileInputStream;
import mx.com.afirme.midas.catalogos.valorfijo.ValorFijoDN;
import mx.com.afirme.midas2.domain.movil.cotizador.DescuentosAgenteDTO;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoDTO;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

import java.io.File;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import org.apache.commons.io.FileUtils;
@SuppressWarnings("deprecation")
public class CargarExcelDescuentoAgenteMovil extends GenerarExcelBase {
	
	private String path;
	List<DescuentosAgenteDTO> listaDescuentosAgente;
	//Vector vector;
	private String STATUS="ERROR";
	private String INFO="ARCHIVO NO CARGADO";
	private File fileUpload;
	private String fileUploadFileName;
	private String fileUploadContentType;
	boolean archivoValido=false;
	public List<DescuentosAgenteDTO>  leerXLS(File archivoXls)
	{
		fileUpload=archivoXls;
		listaDescuentosAgente= new ArrayList<DescuentosAgenteDTO>();
		try
		{								
			File saveFile = null;
			String tempPath;// = System.getProperty("java.io.tmpdir");
			fileUploadFileName="CargarExcelDescuentoAgenteMovil.xlsx";
			String OSName = System.getProperty("os.name");
			if (OSName.toLowerCase().indexOf("windows")!=-1)
				tempPath = Sistema.UPLOAD_FOLDER;
			else
				tempPath = Sistema.LINUX_UPLOAD_FOLDER;
			saveFile = new File(tempPath + File.separator + fileUploadFileName);
			FileUtils.copyFile(fileUpload, saveFile);
			FileInputStream file = new FileInputStream(saveFile);
			Workbook workbook =null;
			try{
				workbook =WorkbookFactory.create(saveFile);
				archivoValido=true;
			}
			catch(Exception e){
				archivoValido=false;
			}
				cargarListaAgenteDescuento(workbook.getSheetAt(0));
			file.close();
			try{
				saveFile.delete();
			}
			catch(Exception e){
				e.printStackTrace();
			}
			
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			if(archivoValido==false){
				setINFO("ERROR  DE TIPO DE ARCHIVO:"+e);
			}
			else{
				setINFO("ERROR VERFIQUE TAMAÑO Y TIPO DEL ARCHIVO:"+e);
			}
			setSTATUS("ERROR");
			
		}
		return listaDescuentosAgente;
	}
	private void  cargarListaAgenteDescuento(Sheet sheet){
		Iterator<Row> rowIterator = sheet.iterator();
		int cont=0;
		while (rowIterator.hasNext()) // recorrido de las filas
		{
			try{
				DescuentosAgenteDTO descuentosAgenteDTO;
				Row row = rowIterator.next();
				if (cont>1){
					Iterator<Cell> cellIterator = row.cellIterator();
					descuentosAgenteDTO= new DescuentosAgenteDTO();
					descuentosAgenteDTO = this.obtenerVector(cellIterator);
					if (descuentosAgenteDTO != null){
						listaDescuentosAgente.add(descuentosAgenteDTO);
					}
				}
				cont++;
			}
			catch(Exception e){
				
			}
			
		}
		
	}	
	private DescuentosAgenteDTO obtenerVector(Iterator<Cell> cellIterator){
		SimpleDateFormat simpleFormat = (SimpleDateFormat) DateFormat.getDateInstance();
		simpleFormat.applyPattern("dd/MM/yyyy");
		
		DescuentosAgenteDTO descuentosAgenteDTO;
		descuentosAgenteDTO = new DescuentosAgenteDTO();
		int columna=0;
		String cellValue ="";
		String tipoDato ="";
		try{
			while (columna<=6) {// recorrido de las columnas					
				Cell cell = cellIterator.next();
					cellValue = "";
					tipoDato ="";
					try{
						switch (cell.getCellType()) {							
							case Cell.CELL_TYPE_NUMERIC:
								cellValue = String.valueOf(cell.getNumericCellValue());
								tipoDato ="NUMERIC";
								break;
							case Cell.CELL_TYPE_STRING:
								cellValue = cell.getStringCellValue();
								tipoDato ="STRING";
								break;	
							default:
								cellValue = cell.getStringCellValue();
								
								break;
						}
					}
					catch(Exception ex) {
						//LogDeMidasEJB3.log(ex.toString());
					}
					if (cellValue.trim().length()>0 && tipoDato.trim().length()> 0){
						if (tipoDato.equals("STRING")){              
							if(columna==1){
								descuentosAgenteDTO.setNombre(cellValue);
							}
							if(columna==6){
								descuentosAgenteDTO.setEmail(cellValue);
							}
							else if(columna==5){
								descuentosAgenteDTO.setNumerotelefono(cellValue);
							}
							
						}
						else{
							if(columna==0){
								descuentosAgenteDTO.setClaveagente(cellValue);
							}
							else if(columna==2){
								descuentosAgenteDTO.setPorcentaje(cellValue);
							}
							else if(columna==3){
								descuentosAgenteDTO.setBajalogica(new Short(cellValue));
							}
							else if(columna==4){
								descuentosAgenteDTO.setClavepromo(cellValue);
							}							
						}

					}
				columna++;				
			}
		}catch(Exception ex){
			descuentosAgenteDTO = null;
		}
		return descuentosAgenteDTO;
	}
	private Timestamp getFechaActual() {
        Date ahora = new Date();
        SimpleDateFormat formateador = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");
        return Timestamp.valueOf(formateador.format(ahora));
    }
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	public File getFileUpload() {
		return fileUpload;
	}

	public void setFileUpload(File fileUpload) {
		this.fileUpload = fileUpload;
	}

	public String getFileUploadFileName() {
		return fileUploadFileName;
	}

	public void setFileUploadFileName(String fileUploadFileName) {
		this.fileUploadFileName = fileUploadFileName;
	}

	public String getFileUploadContentType() {
		return fileUploadContentType;
	}

	public void setFileUploadContentType(String fileUploadContentType) {
		this.fileUploadContentType = fileUploadContentType;
	}
	
	
	public String getSTATUS() {
		return STATUS;
	}
	public void setSTATUS(String sTATUS) {
		STATUS = sTATUS;
	}
	public String getINFO() {
		return INFO;
	}
	public void setINFO(String iNFO) {
		INFO = iNFO;
	}



	@Override
	protected void buildDataSheet(HSSFSheet sheet) {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void buildValidationsSheet(HSSFSheet sheet) {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void leeLineasArchivoCarga(HSSFSheet sheet) {
		// TODO Auto-generated method stub
		
	}
	
}
