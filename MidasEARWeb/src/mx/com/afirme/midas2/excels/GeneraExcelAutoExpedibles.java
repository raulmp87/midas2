package mx.com.afirme.midas2.excels;

import java.io.BufferedOutputStream;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import mx.com.afirme.midas.catalogos.estilovehiculo.EstiloVehiculoDTO;
import mx.com.afirme.midas.consultas.formapago.FormaPagoDTO;
import mx.com.afirme.midas.consultas.mediopago.MedioPagoDTO;
import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionId;
import mx.com.afirme.midas.interfaz.cliente.ClienteGenericoDTO;
import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionDTO;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.controlArchivo.ControlArchivoDN;
import mx.com.afirme.midas.sistema.controlArchivo.ControlArchivoDTO;
import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas.solicitud.SolicitudDTO;
import mx.com.afirme.midas2.clientesapi.dto.ClientesApiService;
import mx.com.afirme.midas2.clientesapi.dto.EntrevistaDTO;
import mx.com.afirme.midas2.clientesapi.dto.InterviewException;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Ejecutivo;
import mx.com.afirme.midas2.domain.negocio.Negocio;
import mx.com.afirme.midas2.domain.negocio.derechos.NegocioDerechoPoliza;
import mx.com.afirme.midas2.domain.negocio.producto.formapago.NegocioFormaPago;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.NegocioTipoPoliza;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.NegocioSeccion;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.paquete.NegocioPaqueteSeccion;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.paquete.cobertura.deducibles.NegocioDeducibleCob;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.tipouso.NegocioTipoUso;
import mx.com.afirme.midas2.domain.persona.Persona;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.autoexpedibles.AutoExpediblesAutoCot;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.autoexpedibles.AutoExpediblesDetalleAutoCot;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.cargamasiva.DetalleCargaMasivaAutoCot;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.cargamasivaindividual.CargaMasivaIndividualAutoCot;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.cargamasivaindividual.DetalleCargaMasivaIndAutoCot;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.inciso.IncisoAutoCot;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.inciso.configuracion.DatoIncisoCotAuto;
import mx.com.afirme.midas2.dto.fuerzaventa.RegistroFuerzaDeVentaDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.ResumenCostosDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.TerminarCotizacionDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.cargamasiva.LogErroresCargaMasivaDTO;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.cargaMasiva.CargaMasivaService;
import mx.com.afirme.midas2.validators.NumSerieValidador;

import org.apache.poi.hssf.usermodel.DVConstraint;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFDataValidation;
import org.apache.poi.hssf.usermodel.HSSFName;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.DataValidationConstraint.ValidationType;
import org.apache.poi.ss.util.CellRangeAddressList;
import org.apache.poi.ss.util.CellReference;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("prototype")
public class GeneraExcelAutoExpedibles {
	
	private int dataRows = 0;
	private int maxRows = 15000;
	private int maxColumns = 60;
	private String uploadFolder;
	private ControlArchivoDTO controlArchivoDTO;
	private List<LogErroresCargaMasivaDTO> listErrores = new ArrayList<LogErroresCargaMasivaDTO>(1);
	private List<AutoExpediblesDetalleAutoCot> detalleList = new ArrayList<AutoExpediblesDetalleAutoCot>(1);
	private AutoExpediblesDetalleAutoCot ejemplo = new AutoExpediblesDetalleAutoCot();
	private FileInputStream logErrores;
	private boolean hasLogErrors = false;
	private Short tipoCarga;
	private Short claveTipo;
	private Long idUsuario;
	
	private NegocioTipoPoliza negocioTipoPoliza;
	private CargaMasivaService cargaMasivaService;
	private ClientesApiService  clienteRest;
		
	private static final int CAMPO_ESTATUS = 0;
	private static final int CAMPO_ID_AGENTE = 1;
	private static final int CAMPO_MONEDA = 2;
	private static final int CAMPO_ID_CENTRO_EMISOR = 3;
	private static final int CAMPO_ID_OFICINA = 4;
	private static final int CAMPO_NUMERO_POLIZA_AUTOEXPEDIBLE = 5;
	private static final int CAMPO_NUMERO_COTIZACION = 6;
	private static final int CAMPO_NUMERO_POLIZA = 7;
	private static final int CAMPO_NUMERO_LIQUIDACION = 8;
	private static final int CAMPO_AUTORIZACION_PROSA = 9;
	private static final int CAMPO_FECHA_VIGENCIA_INICIO = 10;
	private static final int CAMPO_FECHA_EMISION = 11;
	private static final int CAMPO_FECHA_VIGENCIA_FIN = 12;
	private static final int CAMPO_LINEA_NEGOCIO = 13;
	private static final int CAMPO_ID_PAQUETE = 14;
	private static final int CAMPO_NUMERO_EMPLEADO = 15;
	private static final int CAMPO_TIPO_PERSONA_CLIENTE = 16;
	private static final int CAMPO_RFC_CLIENTE = 17;
	private static final int CAMPO_CLIENTE_VIP = 18;
	private static final int CAMPO_NOMBRE_RAZON_SOCIAL = 19;
	private static final int CAMPO_APELLIDO_PATERNO_CLIENTE = 20;
	private static final int CAMPO_APELLIDO_MATERNO_CLIENTE = 21;
	private static final int CAMPO_CODIGO_POSTAL_CLIENTE = 22;
	private static final int CAMPO_COLONIA_CLIENTE = 23;
	private static final int CAMPO_TELEFONO_CLIENTE = 24;
	private static final int CAMPO_CVE_AMIS = 25;
	private static final int CAMPO_MODELO = 26;
	private static final int CAMPO_NCI_REPUVE = 27;
	private static final int CAMPO_CVE_USO = 28;
	private static final int CAMPO_PLACAS = 29;
	private static final int CAMPO_FORMA_PAGO = 30;
	private static final int CAMPO_NUMERO_MOTOR = 31;
	private static final int CAMPO_NUMERO_SERIE = 32;
	private static final int CAMPO_VALOR_COMERCIAL = 33;
	private static final int CAMPO_TOTAL_PASAJEROS = 34;
	private static final int CAMPO_DESCRIPCION_VEHICULO = 35;
	private static final int CAMPO_PRIMA_TOTAL = 36;
	private static final int CAMPO_DEDUCIBLE_DANOS_MATERIALES = 37;
	private static final int CAMPO_DEDUCIBLE_ROBO_TOTAL = 38;
	private static final int CAMPO_LIMITE_RC_TERCEROS = 39;
	private static final int CAMPO_DEDUCIBLE_RC_TERCEROS = 40;
	private static final int CAMPO_LIMITE_GASTOS_MEDICOS = 41;
	private static final int CAMPO_LIMITE_MUERTE = 42;
	private static final int CAMPO_LIMITE_RC_VIAJERO = 43;
	private static final int CAMPO_ASISTENCIA_JURIDICA = 44;
	private static final int CAMPO_LIMITE_ADAPTACION_CONVERSION = 45;
	private static final int CAMPO_LIMITE_EQUIPO_ESPECIAL = 46;
	private static final int CAMPO_DEDUCIBLE_EQUIPO_ESPECIAL = 47;
	private static final int CAMPO_IGUALACION = 48;
	private static final int CAMPO_DERECHOS = 49;
	private static final int CAMPO_SOLICITAR_AUTORIZACION = 50;
	private static final int CAMPO_CAUSA_AUTORIZACION = 51;
	private static final int CAMPO_DESCRIPCION_EQUIPO_ESPECIAL = 52;
	private static final int CAMPO_DESCRIPCION_ADAPTACION_CONVERSION = 53;
	private static final int CAMPO_NOMBRE = 54;
	private static final int CAMPO_APELLIDO_PATERNO = 55;
	private static final int CAMPO_APELLIDO_MATERNO = 56;
	private static final int CAMPO_NUMERO_LICENCIA = 57;
	private static final int CAMPO_FECHA_NACIMIENTO = 58;
	private static final int CAMPO_OCUPACION = 59;
	private static final int CAMPO_IDCLIENTE = 60;
	private static final int CAMPO_ENTREVISTA_PRIMA_ESTIMADA = 61;
	private static final int CAMPO_ENTREVISTA_MONEDA = 62;
	private static final int CAMPO_ENTREVISTA_PEP= 63;
	private static final int CAMPO_ENTREVISTA_PARENTESCO = 64;
	private static final int CAMPO_ENTREVISTA_NOMBRE_PUESTO = 65;
	private static final int CAMPO_ENTREVISTA_PERIODO_FUNCIONES = 66;
	private static final int CAMPO_ENTREVISTA_CUOTA_PROPIA = 67;
	private static final int CAMPO_ENTREVISTA_NOMBRE_PARENTESCO = 68;
	private static final int CAMPO_ENTREVISTA_DOCUMENTACION_REPRESENTACIOM = 69;
	private static final int CAMPO_ENTREVISTA_NUMERO_FIEL = 70;
	private static final int CAMPO_ENTREVISTA_FOLIO_MERCANTIL = 71;
	
	
	public static final String PESOS = "NACIONAL";
	public static final String DOLARES = "DOLARES";
	
	public static final String FISICA = "FISICA";
	public static final String MORAL = "MORAL";
	
	
	public GeneraExcelAutoExpedibles(CargaMasivaService cargaMasivaService,  ClientesApiService  clienteRest	) {
		this.setCargaMasivaService(cargaMasivaService);
		this.clienteRest= clienteRest;
		//uploadFolder = cargaMasivaService.getUploadFolder();
		String OSName = System.getProperty("os.name");
		if (OSName.toLowerCase().indexOf("windows")!=-1)
			uploadFolder = Sistema.UPLOAD_FOLDER;
		else
			uploadFolder = Sistema.LINUX_UPLOAD_FOLDER;
	}
	
	

	public InputStream generaPlantillaAutoExpedibles()
			throws IOException {
		HSSFWorkbook workbook = null;
		HSSFSheet validaciones = null;
		HSSFSheet datos = null;
		ByteArrayOutputStream bos = null;

		try {
			// Obtiene plantilla con macros
			workbook = cargaPlantilla();
			validaciones = workbook.getSheetAt(0);
			datos = workbook.getSheetAt(1);

			buildDataSheet(datos);
			datos.protectSheet("midas");

			buildValidationsSheet(validaciones);

			// Ajusta tamano de columnas
			for (int i = 0; i <= maxColumns; i++) {
					validaciones.autoSizeColumn(i);
			}

			// Oculta hoja de datos
			workbook.setSheetHidden(1, true);
			// Muestra primera hoja
			workbook.setActiveSheet(0);
			short firstCell = 0;
			validaciones.showInPane(firstCell, firstCell);

			bos = new ByteArrayOutputStream();
			workbook.write(bos);

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (bos != null) {
				bos.close();
			}
		}

		if(bos == null){
			return null;
		}
		return new ByteArrayInputStream(bos.toByteArray());
	}

	public HSSFWorkbook cargaPlantilla() throws IOException {
		HSSFWorkbook workbook = null;
		POIFSFileSystem plantilla = new POIFSFileSystem(
				this.getClass()
						.getResourceAsStream(
								"/mx/com/afirme/midas2/impresiones/jrxml/plantillaAutoExpedibles.xls"));
		workbook = new HSSFWorkbook(plantilla);
		return workbook;
	}

	private void buildDataSheet(HSSFSheet sheet) {
		armaCentroEmisor(sheet);
		armaNegocioSeccion(sheet);
		armaFormasPago(sheet);
		armaDerechos(sheet);
		armaEjecutivo(sheet);
	}
	
	private void armaDerechos(HSSFSheet sheet){
		HSSFRow row = null;
		HSSFCell cell = null;
		HSSFName name = null;
		List<NegocioDerechoPoliza> list = cargaMasivaService.getNegocioDerechoPolizaByNegTipoPoliza(negocioTipoPoliza);
		
		if (list != null && !list.isEmpty()) {
			row = sheet.createRow(dataRows);
			dataRows++;
			int linea = dataRows;
			int column = 0;

			Double importeDefault = new Double(0.0);
			for (NegocioDerechoPoliza item : list) {
				cell = row.createCell(column);
				cell.setCellValue(item.getImporteDerecho());
				column++;
				
				importeDefault = item.getImporteDerecho();
				if(item.getClaveDefault()){
					ejemplo.setDerechos(item.getImporteDerecho());
				}
				
			}
			if(ejemplo.getDerechos() == null){
				ejemplo.setDerechos(importeDefault);
			}
			CellReference cellRef = new CellReference(dataRows, (column - 1));

			name = sheet.getWorkbook().createName();
			name.setNameName("DERECHOS");
			String rango = "$A$" + linea + ":$" + cellRef.getCellRefParts()[2]
					+ "$" + linea;
			name.setRefersToFormula("'DataCombo'!" + rango + "");

		}
	}
	
	private void armaFormasPago(HSSFSheet sheet){
		HSSFRow row = null;
		HSSFCell cell = null;
		HSSFName name = null;
		List<NegocioFormaPago> list = cargaMasivaService.getNegocioFormaPagoByNegTipoPoliza(negocioTipoPoliza);
		
		if (list != null && !list.isEmpty()) {
			row = sheet.createRow(dataRows);
			dataRows++;
			int linea = dataRows;
			int column = 0;
			boolean isFirst = true;
			for (NegocioFormaPago item : list) {
				cell = row.createCell(column);
				cell.setCellValue(item.getFormaPagoDTO().getDescripcion());
				column++;
				
				if(isFirst){
					ejemplo.setFormaPago(item.getFormaPagoDTO().getDescripcion());
					isFirst = false;
				}
				
			}
			CellReference cellRef = new CellReference(dataRows, (column - 1));

			name = sheet.getWorkbook().createName();
			name.setNameName("FORMASPAGO");
			String rango = "$A$" + linea + ":$" + cellRef.getCellRefParts()[2]
					+ "$" + linea;
			name.setRefersToFormula("'DataCombo'!" + rango + "");

		}
	}
	
	private void armaNegocioSeccion(HSSFSheet sheet){
		HSSFRow row = null;
		HSSFCell cell = null;
		HSSFName name = null;
		List<NegocioSeccion> list = cargaMasivaService.getNegocioSeccionByNegTipoPoliza(negocioTipoPoliza);
		
		if (list != null && !list.isEmpty()) {
			row = sheet.createRow(dataRows);
			dataRows++;
			int linea = dataRows;
			int column = 0;
			boolean isFirst = true;
			for (NegocioSeccion item : list) {
				cell = row.createCell(column);
				cell.setCellValue(remplazaEspacios(item.getSeccionDTO().getDescripcion()));
				column++;
				
				armaTipoUso(sheet, item, isFirst);
				armaPaquete(sheet, item, isFirst);
				
				if(isFirst){
					ejemplo.setLineaNegocio(remplazaEspacios(item.getSeccionDTO().getDescripcion()));
					isFirst = false;
				}
			}
			CellReference cellRef = new CellReference(dataRows, (column - 1));

			name = sheet.getWorkbook().createName();
			name.setNameName("NEGOCIOSECCION");
			String rango = "$A$" + linea + ":$" + cellRef.getCellRefParts()[2]
					+ "$" + linea;
			name.setRefersToFormula("'DataCombo'!" + rango + "");

		}
	}
	
	private void armaTipoUso(HSSFSheet sheet, NegocioSeccion negocioSeccion, boolean isFirst) {
		HSSFRow row = null;
		HSSFCell cell = null;
		HSSFName name = null;

		SeccionDTO seccion = cargaMasivaService.getSeccionDTOById(negocioSeccion);
		List<NegocioTipoUso> negocioTipoUsoList = cargaMasivaService.getNegocioTipoUsoListByNegSeccion(negocioSeccion);
		if (negocioTipoUsoList != null && !negocioTipoUsoList.isEmpty()) {
			row = sheet.createRow(dataRows);
			dataRows++;
			int column = 0;
			int linea = dataRows;

			for (NegocioTipoUso item2 : negocioTipoUsoList) {
				cell = row.createCell(column);
				cell.setCellValue(item2.getTipoUsoVehiculoDTO()
						.getDescripcionTipoUsoVehiculo());

				column++;
				
				if(isFirst){
					ejemplo.setClaveUso(item2.getTipoUsoVehiculoDTO()
							.getDescripcionTipoUsoVehiculo());
					isFirst = false;
				}
			}
			CellReference cellRef = new CellReference(dataRows, (column - 1));

			name = sheet.getWorkbook().createName();
			String nombre = "TP_" + seccion.getDescripcion();
			name.setNameName(remplazaEspacios(nombre).toUpperCase());

			String rango = "$A$" + linea + ":$" + cellRef.getCellRefParts()[2]
					+ "$" + linea;
			name.setRefersToFormula("'DataCombo'!" + rango + "");

		}
	}
	
	private void armaPaquete(HSSFSheet sheet, NegocioSeccion negocioSeccion, boolean isFirst){
		HSSFRow row = null;
		HSSFCell cell = null;
		HSSFName name = null;

		SeccionDTO seccion = cargaMasivaService.getSeccionDTOById(negocioSeccion);
		List<NegocioPaqueteSeccion> negocioPaqueteSeccionList = cargaMasivaService.getNegocioPaqueteSeccionByNegSeccion(negocioSeccion);
		if (negocioPaqueteSeccionList != null
				&& !negocioPaqueteSeccionList.isEmpty()) {
			row = sheet.createRow(dataRows);
			dataRows++;
			int column = 0;
			int linea = dataRows;

			for (NegocioPaqueteSeccion item2 : negocioPaqueteSeccionList) {
				cell = row.createCell(column);
				cell.setCellValue(item2.getPaquete().getDescripcion());
				column++;
				
				if(isFirst){
					ejemplo.setPaquete(item2.getPaquete().getDescripcion());
					isFirst = false;
				}
			}
			CellReference cellRef = new CellReference(dataRows, (column - 1));

			name = sheet.getWorkbook().createName();
			String nombre = "P_" + seccion.getDescripcion();
			name.setNameName(remplazaEspacios(nombre).toUpperCase());

			String rango = "$A$" + linea + ":$" + cellRef.getCellRefParts()[2]
					+ "$" + linea;
			name.setRefersToFormula("'DataCombo'!" + rango + "");

		}
	}
	
	private void armaCentroEmisor(HSSFSheet sheet){
		HSSFRow row = null;
		HSSFCell cell = null;
		HSSFName name = null;
		List<RegistroFuerzaDeVentaDTO> centroEmisorList = cargaMasivaService.getCentroEmisores();
		
		if (centroEmisorList != null && !centroEmisorList.isEmpty()) {
			row = sheet.createRow(dataRows);
			dataRows++;
			int linea = dataRows;
			int column = 0;

			for (RegistroFuerzaDeVentaDTO item : centroEmisorList) {
				cell = row.createCell(column);
				cell.setCellValue(item.getDescription());
				column++;
				
			}
			CellReference cellRef = new CellReference(dataRows, (column - 1));

			name = sheet.getWorkbook().createName();
			name.setNameName("CENTROEMISOR");
			String rango = "$A$" + linea + ":$" + cellRef.getCellRefParts()[2]
					+ "$" + linea;
			name.setRefersToFormula("'DataCombo'!" + rango + "");

		}
	}
	
	private void armaEjecutivo(HSSFSheet sheet){
		HSSFRow row = null;
		HSSFCell cell = null;
		HSSFName name = null;
		List<Ejecutivo> ejecutivosList = cargaMasivaService.getEjecutivos();
		
		if (ejecutivosList != null && !ejecutivosList.isEmpty()) {
			row = sheet.createRow(dataRows);
			dataRows++;
			int linea = dataRows;
			int column = 0;
			boolean isFirst = true;

			for (Ejecutivo ejecutivo : ejecutivosList) {
				if(ejecutivo!=null && ejecutivo.getClaveEstatus()==1L){
					Persona responsable=(ejecutivo.getPersonaResponsable()!=null && ejecutivo.getPersonaResponsable().getIdPersona()!=null)?ejecutivo.getPersonaResponsable():null;
					String nombre=(responsable!=null)?responsable.getNombreCompleto():null;
					cell = row.createCell(column);
					cell.setCellValue(nombre);
					column++;
					if(isFirst){
						ejemplo.setIdOficina(nombre);
						isFirst = false;
					}					
				}
				
			}
			CellReference cellRef = new CellReference(dataRows, (column - 1));

			name = sheet.getWorkbook().createName();
			name.setNameName("OFICINAS");
			String rango = "$A$" + linea + ":$" + cellRef.getCellRefParts()[2]
					+ "$" + linea;
			name.setRefersToFormula("'DataCombo'!" + rango + "");

		}
	}
	
	private void buildValidationsSheet(HSSFSheet sheet) {
		
		llenaLineaEjemplo(sheet);

		DVConstraint dvConstraintSINO = DVConstraint.createExplicitListConstraint(new String[] { GeneraExcelCargaMasiva.VALOR_SI, GeneraExcelCargaMasiva.VALOR_NO });
		DVConstraint dvConstraintMONEDA = DVConstraint.createExplicitListConstraint(new String[] { PESOS, DOLARES });
		DVConstraint dvConstraintTIPOPERSONA = DVConstraint.createExplicitListConstraint(new String[] { FISICA, MORAL });
		
		HSSFDataValidation data_validation = new HSSFDataValidation(
				new CellRangeAddressList(1, maxRows, CAMPO_CLIENTE_VIP,CAMPO_CLIENTE_VIP), dvConstraintSINO);
		data_validation.setSuppressDropDownArrow(false);
		sheet.addValidationData(data_validation);
		
		data_validation = new HSSFDataValidation(
				new CellRangeAddressList(1, maxRows, CAMPO_ASISTENCIA_JURIDICA,CAMPO_ASISTENCIA_JURIDICA), dvConstraintSINO);
		data_validation.setSuppressDropDownArrow(false);
		sheet.addValidationData(data_validation);
		
		data_validation = new HSSFDataValidation(
				new CellRangeAddressList(1, maxRows, CAMPO_IGUALACION,CAMPO_IGUALACION), dvConstraintSINO);
		data_validation.setSuppressDropDownArrow(false);
		sheet.addValidationData(data_validation);
		
		data_validation = new HSSFDataValidation(
				new CellRangeAddressList(1, maxRows, CAMPO_SOLICITAR_AUTORIZACION,CAMPO_SOLICITAR_AUTORIZACION), dvConstraintSINO);
		data_validation.setSuppressDropDownArrow(false);
		sheet.addValidationData(data_validation);
		
		data_validation = new HSSFDataValidation(
				new CellRangeAddressList(1, maxRows, CAMPO_CAUSA_AUTORIZACION,CAMPO_CAUSA_AUTORIZACION), dvConstraintSINO);
		data_validation.setSuppressDropDownArrow(false);
		sheet.addValidationData(data_validation);
		
		data_validation = new HSSFDataValidation(
				new CellRangeAddressList(1, maxRows, CAMPO_MONEDA,CAMPO_MONEDA), dvConstraintMONEDA);
		data_validation.setSuppressDropDownArrow(false);
		sheet.addValidationData(data_validation);
		
		DVConstraint dvConstraint = DVConstraint.createFormulaListConstraint("CENTROEMISOR");
		data_validation = new HSSFDataValidation(
		new CellRangeAddressList(1, maxRows, CAMPO_ID_CENTRO_EMISOR,CAMPO_ID_CENTRO_EMISOR), dvConstraint);
		data_validation.setSuppressDropDownArrow(false);
		sheet.addValidationData(data_validation);
		
		DVConstraint dvConstraint2 = DVConstraint.createDateConstraint(DVConstraint.OperatorType.GREATER_THAN,
				"01/01/1900", "01/01/1900", "dd/MM/yyyy");
		data_validation = new HSSFDataValidation(new CellRangeAddressList(1,
				maxRows, CAMPO_FECHA_VIGENCIA_INICIO, CAMPO_FECHA_VIGENCIA_FIN),
				dvConstraint2);
		sheet.addValidationData(data_validation);
		
		DVConstraint dvConstraint3 = DVConstraint.createNumericConstraint(
				ValidationType.INTEGER, DVConstraint.OperatorType.GREATER_THAN,
				"0", "0");
		DVConstraint dvConstraint4 = DVConstraint.createNumericConstraint(
				ValidationType.DECIMAL, DVConstraint.OperatorType.GREATER_THAN,
				"0", "0");
		data_validation = new HSSFDataValidation(
				new CellRangeAddressList(1, maxRows, CAMPO_NUMERO_POLIZA,
						CAMPO_NUMERO_POLIZA), dvConstraint3);
		sheet.addValidationData(data_validation);
		
		data_validation = new HSSFDataValidation(
				new CellRangeAddressList(1, maxRows, CAMPO_NUMERO_COTIZACION,
						CAMPO_NUMERO_COTIZACION), dvConstraint3);
		sheet.addValidationData(data_validation);
		
		data_validation = new HSSFDataValidation(
				new CellRangeAddressList(1, maxRows, CAMPO_ID_AGENTE,
						CAMPO_ID_AGENTE), dvConstraint3);
		sheet.addValidationData(data_validation);
		
		data_validation = new HSSFDataValidation(
				new CellRangeAddressList(1, maxRows, CAMPO_IDCLIENTE,
						CAMPO_IDCLIENTE), dvConstraint3);
		sheet.addValidationData(data_validation);
		
		data_validation = new HSSFDataValidation(
				new CellRangeAddressList(1, maxRows, CAMPO_PRIMA_TOTAL,
						CAMPO_PRIMA_TOTAL), dvConstraint4);
		sheet.addValidationData(data_validation);
		
		data_validation = new HSSFDataValidation(
				new CellRangeAddressList(1, maxRows, CAMPO_DEDUCIBLE_DANOS_MATERIALES,
						CAMPO_LIMITE_RC_VIAJERO), dvConstraint3);
		sheet.addValidationData(data_validation);
		
		data_validation = new HSSFDataValidation(
				new CellRangeAddressList(1, maxRows, CAMPO_LIMITE_ADAPTACION_CONVERSION,
						CAMPO_LIMITE_ADAPTACION_CONVERSION), dvConstraint3);
		sheet.addValidationData(data_validation);
		
		data_validation = new HSSFDataValidation(
				new CellRangeAddressList(1, maxRows, CAMPO_LIMITE_EQUIPO_ESPECIAL,
						CAMPO_LIMITE_EQUIPO_ESPECIAL), dvConstraint3);
		sheet.addValidationData(data_validation);
		
		data_validation = new HSSFDataValidation(
				new CellRangeAddressList(1, maxRows, CAMPO_DEDUCIBLE_EQUIPO_ESPECIAL,
						CAMPO_DEDUCIBLE_EQUIPO_ESPECIAL), dvConstraint3);
		sheet.addValidationData(data_validation);
		
		data_validation = new HSSFDataValidation(
				new CellRangeAddressList(1, maxRows, CAMPO_TIPO_PERSONA_CLIENTE,CAMPO_TIPO_PERSONA_CLIENTE), dvConstraintTIPOPERSONA);
		data_validation.setSuppressDropDownArrow(false);
		sheet.addValidationData(data_validation);
		
		dvConstraint = DVConstraint.createFormulaListConstraint("NEGOCIOSECCION");
		data_validation = new HSSFDataValidation(
		new CellRangeAddressList(1, maxRows, CAMPO_LINEA_NEGOCIO,CAMPO_LINEA_NEGOCIO), dvConstraint);
		data_validation.setSuppressDropDownArrow(false);
		sheet.addValidationData(data_validation);
		
		dvConstraint = DVConstraint.createFormulaListConstraint("FORMASPAGO");
		data_validation = new HSSFDataValidation(
		new CellRangeAddressList(1, maxRows, CAMPO_FORMA_PAGO,CAMPO_FORMA_PAGO), dvConstraint);
		data_validation.setSuppressDropDownArrow(false);
		sheet.addValidationData(data_validation);
		
		dvConstraint = DVConstraint.createFormulaListConstraint("DERECHOS");
		data_validation = new HSSFDataValidation(
		new CellRangeAddressList(1, maxRows, CAMPO_DERECHOS,CAMPO_DERECHOS), dvConstraint);
		data_validation.setSuppressDropDownArrow(false);
		sheet.addValidationData(data_validation);
		
		dvConstraint = DVConstraint.createFormulaListConstraint("OFICINAS");
		data_validation = new HSSFDataValidation(
		new CellRangeAddressList(1, maxRows, CAMPO_ID_OFICINA,CAMPO_ID_OFICINA), dvConstraint);
		data_validation.setSuppressDropDownArrow(false);
		sheet.addValidationData(data_validation);
		
		CellReference cellRef = new CellReference(1, CAMPO_LINEA_NEGOCIO);
		
		for (int i = 2; i < maxRows; i++) {

			dvConstraint = DVConstraint
					.createFormulaListConstraint("INDIRECT(UPPER(\"P_\"&$" + cellRef.getCellRefParts()[2] + "$"
							+ i + "))");
			data_validation = new HSSFDataValidation(new CellRangeAddressList(
					1, (i - 1), CAMPO_ID_PAQUETE, CAMPO_ID_PAQUETE), dvConstraint);
			sheet.addValidationData(data_validation);
			
			dvConstraint = DVConstraint
					.createFormulaListConstraint("INDIRECT(UPPER(\"TP_\"&$"
							+ cellRef.getCellRefParts()[2] + "$" + i + "))");
			data_validation = new HSSFDataValidation(new CellRangeAddressList(
					1, (i - 1), CAMPO_CVE_USO, CAMPO_CVE_USO),
					dvConstraint);
			sheet.addValidationData(data_validation);

		}
	}

	
	private void llenaLineaEjemplo(HSSFSheet sheet){
		HSSFRow row = sheet.getRow(1);
		if(row == null){
			row = sheet.createRow(1);
		}
		HSSFCell cell = row.createCell(CAMPO_ID_OFICINA);
		cell.setCellValue(ejemplo.getIdOficina());
		cell = row.createCell(CAMPO_LINEA_NEGOCIO);
		cell.setCellValue(ejemplo.getLineaNegocio());
		cell = row.createCell(CAMPO_ID_PAQUETE);
		cell.setCellValue(ejemplo.getPaquete());
		cell = row.createCell(CAMPO_CVE_USO);
		cell.setCellValue(ejemplo.getClaveUso());
		cell = row.createCell(CAMPO_FORMA_PAGO);
		cell.setCellValue(ejemplo.getFormaPago());
		cell = row.createCell(CAMPO_DERECHOS);
		cell.setCellValue(ejemplo.getDerechos());
		cell = row.createCell(CAMPO_FECHA_VIGENCIA_INICIO);
		cell.setCellValue(new Date());
		cell = row.createCell(CAMPO_FECHA_VIGENCIA_FIN);
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(new Date());
		calendar.add(Calendar.YEAR, 1);
		cell.setCellValue(calendar.getTime());
	}


	
	public void validaAutoExpedibles(BigDecimal idControlArchivo, Short tipoCarga, Short claveTipo) {

		try {
			this.tipoCarga = tipoCarga;
			this.claveTipo = claveTipo;

			ControlArchivoDN controlArchivoDN = ControlArchivoDN.getInstancia();
			controlArchivoDTO = controlArchivoDN.getPorId(idControlArchivo);
			String fileName = this.obtenerNombreArchivo(controlArchivoDTO);

			HSSFWorkbook workbook = this.cargaArchivodeCarga(uploadFolder
					+ fileName);
			HSSFSheet validaciones = workbook.getSheetAt(0);
			leeLineasArchivoCarga(validaciones);


			FileOutputStream fos = new FileOutputStream(uploadFolder + fileName);
			BufferedOutputStream bos = new BufferedOutputStream(fos);

			workbook.write(bos);
			
			bos.flush();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (SystemException e) {
			e.printStackTrace();
		}
	}
	
	private void leeLineasArchivoCarga(HSSFSheet sheet) {
		Iterator<Row> rowIterator = sheet.rowIterator();
		boolean isFirst = true;
		while (rowIterator.hasNext()) {
			HSSFRow hssfRow = (HSSFRow) rowIterator.next();
			if (!isFirst) {
				AutoExpediblesDetalleAutoCot detalle = creaLineaDetalleCarga(hssfRow);
				if(detalle != null){
					detalleList.add(detalle);
				}else{
					AutoExpediblesDetalleAutoCot detalleVacio = new AutoExpediblesDetalleAutoCot();
					detalleList.add(detalleVacio);
				}
			} else {
				isFirst = false;
			}
		}

		// Valida datos para cotizacion
		try{
			if (tipoCarga.equals(GeneraExcelCargaMasiva.TIPO_CARGA_NORMAL)) {
				if (claveTipo.equals(CargaMasivaIndividualAutoCot.TIPO_COTIZACION)) {
					this.validaDetalleAutoExpedibles(detalleList, sheet);
				}
				if (claveTipo.equals(CargaMasivaIndividualAutoCot.TIPO_EMISION)) {
					this.validaEmisionAutoExpedibles(detalleList, sheet);
				}
			}
		}catch(Exception e){
			LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
			error.setNumeroInciso(new BigDecimal(0));
			error.setNombreSeccion("");
			error.setNombreCampoError("ERROR INESPERADO");
			error.setRecomendaciones("Error Inesperado.");
			listErrores.add(error);
			e.printStackTrace();
		}

		if (this.getListErrores().isEmpty()) {
			System.out.println("Sin Errores de carga");
			setHasLogErrors(false);
		} else {
			creaLogErrores();
			setHasLogErrors(true);
		}
	}
	
	private AutoExpediblesDetalleAutoCot creaLineaDetalleCarga(HSSFRow hssfRow) {

		AutoExpediblesDetalleAutoCot detalle = new AutoExpediblesDetalleAutoCot();
		EntrevistaDTO entrevista = new EntrevistaDTO();
		
		Boolean datosConductorObligatorios = false;

		int fila = hssfRow.getRowNum() + 1;
		StringBuilder mensajeError = new StringBuilder("");
		String lineaDeNegocio = "";
		Short uno = 1;
		boolean lineaVacia = true;
		for (int i = 0; i <= maxColumns; i++) {
			HSSFCell cell = hssfRow.getCell(i);
			LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
			switch (i) {
			case CAMPO_ESTATUS:
				break;
			case CAMPO_ID_AGENTE:
				if (cell != null && !cell.toString().isEmpty()) {
					detalle.setIdAgente(cell.toString());
					lineaVacia = false;
				}else {
					error.setNumeroInciso(new BigDecimal(fila));
					error.setNombreSeccion(lineaDeNegocio);
					error.setNombreCampoError("AGENTE");
					error.setRecomendaciones("Ingrese un Agente");
					listErrores.add(error);
					mensajeError.append("Campo: " ).append(error.getNombreCampoError()).append(" - " ).append(error.getRecomendaciones()).append("\n");
				}
				break;
			case CAMPO_MONEDA:
				if (cell != null && !cell.toString().isEmpty()) {
					detalle.setMoneda(cell.toString());
					lineaVacia = false;
				}else {
					error.setNumeroInciso(new BigDecimal(fila));
					error.setNombreSeccion(lineaDeNegocio);
					error.setNombreCampoError("MONEDA");
					error.setRecomendaciones("Seleccione una Moneda");
					listErrores.add(error);
					mensajeError.append("Campo: ").append(error.getNombreCampoError()).append(" - ").append(error.getRecomendaciones()).append("\n");
				}
				break;
			case CAMPO_ID_CENTRO_EMISOR:
				if (cell != null && !cell.toString().isEmpty()) {
					detalle.setIdCentroEmisor(cell.toString());
					lineaVacia = false;
				}else {
					error.setNumeroInciso(new BigDecimal(fila));
					error.setNombreSeccion(lineaDeNegocio);
					error.setNombreCampoError("CENTRO EMISOR");
					error.setRecomendaciones("Seleccione un Centro Emisor");
					listErrores.add(error);
					mensajeError.append("Campo: ").append(error.getNombreCampoError()).append(" - ").append(error.getRecomendaciones()).append("\n");
				}
				break;
			case CAMPO_ID_OFICINA:
				if (cell != null && !cell.toString().isEmpty()) {
					detalle.setIdOficina(cell.toString());
					lineaVacia = false;
				}else {
					error.setNumeroInciso(new BigDecimal(fila));
					error.setNombreSeccion(lineaDeNegocio);
					error.setNombreCampoError("EJECUTIVO");
					error.setRecomendaciones("Ingrese una Ejecutivo");
					listErrores.add(error);
					mensajeError.append("Campo: ").append(error.getNombreCampoError()).append(" - ").append(error.getRecomendaciones()).append("\n");
				}
				break;
			case CAMPO_NUMERO_POLIZA_AUTOEXPEDIBLE:
				if (cell != null && !cell.toString().isEmpty()) {
					try{
						detalle.setIdPolizaAutoExpedible(new BigDecimal(cell.toString()));
						lineaVacia = false;
					}catch(Exception e){
						error.setNumeroInciso(new BigDecimal(fila));
						error.setNombreSeccion(lineaDeNegocio);
						error.setNombreCampoError("NUMERO POLIZA AUTOEXPEDIBLE");
						error.setRecomendaciones("Ingrese un Numero Poliza AutoExpedible Valido");
						listErrores.add(error);
						mensajeError.append("Campo: ").append(error.getNombreCampoError()).append(" - ").append(error.getRecomendaciones()).append("\n");						
					}
				}else {
					error.setNumeroInciso(new BigDecimal(fila));
					error.setNombreSeccion(lineaDeNegocio);
					error.setNombreCampoError("NUMERO POLIZA AUTOEXPEDIBLE");
					error.setRecomendaciones("Ingrese un Numero Poliza AutoExpedible");
					listErrores.add(error);
					mensajeError.append("Campo: ").append(error.getNombreCampoError()).append(" - ").append(error.getRecomendaciones()).append("\n");
				}
				break;
			case CAMPO_NUMERO_COTIZACION:
				if(claveTipo.equals(CargaMasivaIndividualAutoCot.TIPO_EMISION)){
				if (cell != null && !cell.toString().isEmpty()) {
					try{
						detalle.setNumeroCotizacion(new BigDecimal(cell.toString()));
						lineaVacia = false;
					}catch(Exception e){
						error.setNumeroInciso(new BigDecimal(fila));
						error.setNombreSeccion(lineaDeNegocio);
						error.setNombreCampoError("NUMERO COTIZACION");
						error.setRecomendaciones("Valor Invalido");
						listErrores.add(error);
						mensajeError.append("Campo: ").append(error.getNombreCampoError()).append(" - " ).append(error.getRecomendaciones()).append("\r\n").append(System.getProperty("line.separator"));
					}
					try{
						CotizacionDTO cotizacion = cargaMasivaService.getCotizacionByDetalleCargaMasivaIndAutoCot(detalle.getNumeroCotizacion());
						if(cotizacion == null){
							error.setNumeroInciso(new BigDecimal(fila));
							error.setNombreSeccion(lineaDeNegocio);
							error.setNombreCampoError("NUMERO COTIZACION");
							error.setRecomendaciones("Valor Invalido");
							listErrores.add(error);
							mensajeError.append("Campo: ").append(error.getNombreCampoError()).append(" - ").append(error.getRecomendaciones()).append("\r\n").append(System.getProperty("line.separator"));							
						}else{
							IncisoCotizacionDTO inciso = cargaMasivaService.obtieneIncisoCotizacion(cotizacion, new BigDecimal(1));
							if (inciso != null) {
								detalle.setDescripcionVehiculo(inciso.getIncisoAutoCot().getDescripcionFinal());
								datosConductorObligatorios = cargaMasivaService.datosDelConductorRequeridos(inciso);
							}
						}
					}catch(Exception e){
						e.printStackTrace();
					}
				}else {
					error.setNumeroInciso(new BigDecimal(fila));
					error.setNombreSeccion(lineaDeNegocio);
					error.setNombreCampoError("NUMERO DE COTIZACION");
					error.setRecomendaciones("Ingrese un Numero de Cotizacion");
					listErrores.add(error);
					mensajeError.append("Campo: ").append(error.getNombreCampoError()).append(" - ").append(error.getRecomendaciones()).append("\n");
				}
				}
				break;
			case CAMPO_NUMERO_POLIZA:
				break;
			case CAMPO_NUMERO_LIQUIDACION:
				if (cell != null && !cell.toString().isEmpty()) {
					try{
						detalle.setNumeroLiquidacion(new BigDecimal(cell.toString()));
						lineaVacia = false;
					}catch(Exception e){
					}
				}
				break;
			case CAMPO_AUTORIZACION_PROSA:
				if (cell != null && !cell.toString().isEmpty()) {
					detalle.setAutorizacionProsa(cell.toString().toUpperCase());
					lineaVacia = false;
				}
				break;
			case CAMPO_FECHA_VIGENCIA_INICIO:
				if (cell != null && !cell.toString().isEmpty()) {
					try{
						detalle.setFechaVigenciaInicio(cell.getDateCellValue());
						lineaVacia = false;
					}catch(Exception e){
						error.setNumeroInciso(new BigDecimal(fila));
						error.setNombreSeccion(lineaDeNegocio);
						error.setNombreCampoError("FECHA VIGENCIA INICIO");
						error.setRecomendaciones("Valor Invalido");
						listErrores.add(error);
						mensajeError.append("Campo: ").append(error.getNombreCampoError()).append(" - ").append(error.getRecomendaciones()).append("\r\n").append(System.getProperty("line.separator"));
					}
				}else {
					error.setNumeroInciso(new BigDecimal(fila));
					error.setNombreSeccion(lineaDeNegocio);
					error.setNombreCampoError("FECHA VIGENCIA INICIO");
					error.setRecomendaciones("Ingrese la Fecha de Vigencia Inicio");
					listErrores.add(error);
					mensajeError.append("Campo: ").append(error.getNombreCampoError()).append(" - ").append(error.getRecomendaciones()).append("\n");
				}
				break;
			case CAMPO_FECHA_EMISION:
				break;
			case CAMPO_FECHA_VIGENCIA_FIN:
				if (cell != null && !cell.toString().isEmpty()) {
					try{
						detalle.setFechaVigenciaFin(cell.getDateCellValue());
						lineaVacia = false;
					}catch(Exception e){
						error.setNumeroInciso(new BigDecimal(fila));
						error.setNombreSeccion(lineaDeNegocio);
						error.setNombreCampoError("FECHA VIGENCIA FIN");
						error.setRecomendaciones("Valor Invalido");
						listErrores.add(error);
						mensajeError.append("Campo: ").append(error.getNombreCampoError()).append(" - ").append(error.getRecomendaciones()).append("\r\n").append(System.getProperty("line.separator"));
					}
				}else {
					error.setNumeroInciso(new BigDecimal(fila));
					error.setNombreSeccion(lineaDeNegocio);
					error.setNombreCampoError("FECHA VIGENCIA FIN");
					error.setRecomendaciones("Ingrese la Fecha Vigencia Fin");
					listErrores.add(error);
					mensajeError.append("Campo: ").append(error.getNombreCampoError()).append(" - ").append(error.getRecomendaciones()).append("\n");
				}
				break;
			case CAMPO_LINEA_NEGOCIO:
				if (cell != null && !cell.toString().isEmpty()) {
					detalle.setLineaNegocio(cell.toString());
					lineaVacia = false;
				}else {
					error.setNumeroInciso(new BigDecimal(fila));
					error.setNombreSeccion(lineaDeNegocio);
					error.setNombreCampoError("LINEA DE NEGOCIO");
					error.setRecomendaciones("Seleccione una Linea de Negocio");
					listErrores.add(error);
					mensajeError.append("Campo: ").append(error.getNombreCampoError()).append(" - ").append(error.getRecomendaciones()).append("\n");
				}
				break;
			case CAMPO_ID_PAQUETE:
				if (cell != null && !cell.toString().isEmpty()) {
					detalle.setPaquete(cell.toString());
					lineaVacia = false;
				}else {
					error.setNumeroInciso(new BigDecimal(fila));
					error.setNombreSeccion(lineaDeNegocio);
					error.setNombreCampoError("PAQUETE");
					error.setRecomendaciones("Seleccione un Paquete");
					listErrores.add(error);
					mensajeError.append("Campo: ").append(error.getNombreCampoError()).append(" - ").append(error.getRecomendaciones()).append("\n");
				}
				break;
			case CAMPO_NUMERO_EMPLEADO:
				if (cell != null && !cell.toString().isEmpty()) {
					try{
						detalle.setNumeroEmpleado(new BigDecimal(cell.toString()));
						lineaVacia = false;
					}catch(Exception e){
					}
				}
				break;
			case CAMPO_TIPO_PERSONA_CLIENTE:
				if (cell != null && !cell.toString().isEmpty()) {
					if(cell.toString().equals(FISICA)){
						detalle.setTipoPersonaCliente((short) 1);
					}else{
						detalle.setTipoPersonaCliente((short) 2);
					}
					lineaVacia = false;
				}else {
					error.setNumeroInciso(new BigDecimal(fila));
					error.setNombreSeccion(lineaDeNegocio);
					error.setNombreCampoError("TIPO PERSONA CLIENTE");
					error.setRecomendaciones("Ingrese el Tipo Persona Cliente");
					listErrores.add(error);
					mensajeError.append("Campo: ").append(error.getNombreCampoError()).append(" - ").append(error.getRecomendaciones()).append("\n");
				}
				break;
			case CAMPO_RFC_CLIENTE:
				if (cell != null && !cell.toString().isEmpty()) {
					detalle.setRfcCliente(cell.toString().toUpperCase());
					lineaVacia = false;
				}else {
					error.setNumeroInciso(new BigDecimal(fila));
					error.setNombreSeccion(lineaDeNegocio);
					error.setNombreCampoError("RFC CLIENTE");
					error.setRecomendaciones("Ingrese el RFC Cliente");
					listErrores.add(error);
					mensajeError.append("Campo: ").append(error.getNombreCampoError()).append(" - ").append(error.getRecomendaciones()).append("\n");
				}
				break;
			case CAMPO_CLIENTE_VIP:
				if (cell != null && !cell.toString().isEmpty()) {
					if(cell.toString().equals(GeneraExcelCargaMasiva.VALOR_SI)){
						detalle.setClienteVIP((short) 1);
					}else{
						detalle.setClienteVIP((short) 0);
					}
					lineaVacia = false;
				}
				break;
			case CAMPO_NOMBRE_RAZON_SOCIAL:
				if (cell != null && !cell.toString().isEmpty()) {
					detalle.setNombreORazonSocial(cell.toString().toUpperCase());
					lineaVacia = false;
				}else {
					error.setNumeroInciso(new BigDecimal(fila));
					error.setNombreSeccion(lineaDeNegocio);
					error.setNombreCampoError("NOMBRE O RAZON SOCIAL");
					error.setRecomendaciones("Ingrese el Nombre o Razon Social");
					listErrores.add(error);
					mensajeError.append("Campo: ").append(error.getNombreCampoError()).append(" - ").append(error.getRecomendaciones()).append("\n");
				}
				break;
			case CAMPO_APELLIDO_PATERNO_CLIENTE:				
				if(detalle.getTipoPersonaCliente() != null && detalle.getTipoPersonaCliente().equals(uno)){
				if (cell != null && !cell.toString().isEmpty()) {
					detalle.setApellidoPaternoCliente(cell.toString().toUpperCase());
					lineaVacia = false;
				}else {
					error.setNumeroInciso(new BigDecimal(fila));
					error.setNombreSeccion(lineaDeNegocio);
					error.setNombreCampoError("APELLIDO PATERNO CLIENTE");
					error.setRecomendaciones("Ingrese el Apellido Paterno Cliente");
					listErrores.add(error);
					mensajeError.append("Campo: ").append(error.getNombreCampoError()).append(" - ").append(error.getRecomendaciones()).append("\n");
				}
				}
				break;
			case CAMPO_APELLIDO_MATERNO_CLIENTE:
				if(detalle.getTipoPersonaCliente() != null && detalle.getTipoPersonaCliente().equals(uno)){
				if (cell != null && !cell.toString().isEmpty()) {
					detalle.setApellidoMaternoCliente(cell.toString().toUpperCase());
					lineaVacia = false;
				}else {
					error.setNumeroInciso(new BigDecimal(fila));
					error.setNombreSeccion(lineaDeNegocio);
					error.setNombreCampoError("APELLIDO MATERNO CLIENTE");
					error.setRecomendaciones("Ingrese el Apellido Materno Cliente");
					listErrores.add(error);
					mensajeError.append("Campo: ").append(error.getNombreCampoError()).append(" - ").append(error.getRecomendaciones()).append("\n");
				}
				}
				break;
			case CAMPO_CODIGO_POSTAL_CLIENTE:
				if (cell != null && !cell.toString().isEmpty()) {
					String valor = cell.toString();
					if (valor.indexOf(".") != -1) {
						valor = valor.substring(0, valor.indexOf("."));
					}
					detalle.setCodigoPostalCliente(valor);
					lineaVacia = false;
				}else {
					error.setNumeroInciso(new BigDecimal(fila));
					error.setNombreSeccion(lineaDeNegocio);
					error.setNombreCampoError("CODIGO POSTAL CLIENTE");
					error.setRecomendaciones("Ingrese el Codigo Postal Cliente");
					listErrores.add(error);
					mensajeError.append("Campo: ").append(error.getNombreCampoError()).append(" - ").append(error.getRecomendaciones()).append("\n");
				}
				break;
			case CAMPO_COLONIA_CLIENTE:
				if (cell != null && !cell.toString().isEmpty()) {
					detalle.setColoniaCliente(cell.toString());
					lineaVacia = false;
				}else {
					error.setNumeroInciso(new BigDecimal(fila));
					error.setNombreSeccion(lineaDeNegocio);
					error.setNombreCampoError("COLONIA CLIENTE");
					error.setRecomendaciones("Ingrese el Colonia Cliente");
					listErrores.add(error);
					mensajeError.append("Campo: ").append(error.getNombreCampoError()).append(" - ").append(error.getRecomendaciones()).append("\n");
				}
				break;
			case CAMPO_TELEFONO_CLIENTE:
				if (cell != null && !cell.toString().isEmpty()) {
					detalle.setTelefonoCliente(cell.toString().toUpperCase());
					lineaVacia = false;
				}else {
					error.setNumeroInciso(new BigDecimal(fila));
					error.setNombreSeccion(lineaDeNegocio);
					error.setNombreCampoError("TELEFONO CLIENTE");
					error.setRecomendaciones("Ingrese el Telefono Cliente");
					listErrores.add(error);
					mensajeError.append("Campo: ").append(error.getNombreCampoError()).append(" - ").append(error.getRecomendaciones()).append("\n");
				}
				break;	
			case CAMPO_CVE_AMIS:
				if (cell != null && !cell.toString().isEmpty()) {
					String valorStr = "";
					if(cell.getCellType() == HSSFCell.CELL_TYPE_NUMERIC){
						double valor = cell.getNumericCellValue();
						BigDecimal valorB = new BigDecimal(valor);
						valorStr = valorB.toPlainString();
					}else if(cell.getCellType() == HSSFCell.CELL_TYPE_STRING){
						valorStr = cell.getStringCellValue();
					}else{
						valorStr = cell.toString();
					}
					if (valorStr.indexOf(".") != -1) {
						valorStr = valorStr.substring(0, valorStr.indexOf("."));
					}
					detalle.setClaveAMIS(valorStr);
					lineaVacia = false;
				}else {
					error.setNumeroInciso(new BigDecimal(fila));
					error.setNombreSeccion(lineaDeNegocio);
					error.setNombreCampoError("CLAVE AMIS");
					error.setRecomendaciones("Ingrese la Clave AMIS");
					listErrores.add(error);
					mensajeError.append("Campo: ").append(error.getNombreCampoError()).append(" - ").append(error.getRecomendaciones()).append("\n");
				}
				break;
			case CAMPO_MODELO:
				if (cell != null && !cell.toString().isEmpty()) {
					String valor = cell.toString();
					if (valor.indexOf(".") != -1) {
						valor = valor.substring(0, valor.indexOf("."));
					}
					try{
						detalle.setModelo(new Short(valor));
						lineaVacia = false;
					}catch(Exception e){
						error.setNumeroInciso(new BigDecimal(fila));
						error.setNombreSeccion(lineaDeNegocio);
						error.setNombreCampoError("MODELO");
						error.setRecomendaciones("Valor Invalido");
						listErrores.add(error);
						mensajeError.append("Campo: ").append(error.getNombreCampoError()).append(" - ").append(error.getRecomendaciones()).append("\r\n").append(System.getProperty("line.separator"));
					}
				}else {
					error.setNumeroInciso(new BigDecimal(fila));
					error.setNombreSeccion(lineaDeNegocio);
					error.setNombreCampoError("MODELO");
					error.setRecomendaciones("Ingrese el Modelo");
					listErrores.add(error);
					mensajeError.append("Campo: ").append(error.getNombreCampoError()).append(" - ").append(error.getRecomendaciones()).append("\n");
				}
				break;
			case CAMPO_NCI_REPUVE:
				if (cell != null && !cell.toString().isEmpty()) {
					detalle.setNciRepuve(cell.toString().toUpperCase());
					lineaVacia = false;
				}
				break;
			case CAMPO_CVE_USO:
				if (cell != null && !cell.toString().isEmpty()) {
					detalle.setClaveUso(cell.toString());
					lineaVacia = false;
				}else {
					error.setNumeroInciso(new BigDecimal(fila));
					error.setNombreSeccion(lineaDeNegocio);
					error.setNombreCampoError("CLAVE USO");
					error.setRecomendaciones("Ingrese la Clave Uso");
					listErrores.add(error);
					mensajeError.append("Campo: ").append(error.getNombreCampoError()).append(" - ").append(error.getRecomendaciones()).append("\n");
				}
				break;
			case CAMPO_PLACAS:
				if (cell != null && !cell.toString().isEmpty()) {
					detalle.setPlacas(cell.toString().toUpperCase());
					lineaVacia = false;
				}else {
					error.setNumeroInciso(new BigDecimal(fila));
					error.setNombreSeccion(lineaDeNegocio);
					error.setNombreCampoError("PLACAS");
					error.setRecomendaciones("Ingrese las Placas");
					listErrores.add(error);
					mensajeError.append("Campo: ").append(error.getNombreCampoError()).append(" - ").append(error.getRecomendaciones()).append("\n");
				}
				break;
			case CAMPO_FORMA_PAGO:
				if (cell != null && !cell.toString().isEmpty()) {
					detalle.setFormaPago(cell.toString());
					lineaVacia = false;
				}else {
					error.setNumeroInciso(new BigDecimal(fila));
					error.setNombreSeccion(lineaDeNegocio);
					error.setNombreCampoError("FORMAS DE PAGO");
					error.setRecomendaciones("Seleccione la Forma de Pago");
					listErrores.add(error);
					mensajeError.append("Campo: ").append(error.getNombreCampoError()).append(" - ").append(error.getRecomendaciones()).append("\n");
				}
				break;
			case CAMPO_NUMERO_MOTOR:
				if (cell != null && !cell.toString().isEmpty()) {
					detalle.setNumeroMotor(cell.toString().toUpperCase());
					lineaVacia = false;
				}else {
					error.setNumeroInciso(new BigDecimal(fila));
					error.setNombreSeccion(lineaDeNegocio);
					error.setNombreCampoError("NUMERO DE MOTOR");
					error.setRecomendaciones("Ingrese el Numero de Motor");
					listErrores.add(error);
					mensajeError.append("Campo: ").append(error.getNombreCampoError()).append(" - ").append(error.getRecomendaciones()).append("\n");
				}
				break;
			case CAMPO_NUMERO_SERIE:
				if (cell != null && !cell.toString().isEmpty()) {
					if(cell.getCellType() == HSSFCell.CELL_TYPE_NUMERIC){
						double valor = cell.getNumericCellValue();
						BigDecimal valorB = new BigDecimal(valor);
						detalle.setNumeroSerie(valorB.toPlainString());
					}else if(cell.getCellType() == HSSFCell.CELL_TYPE_STRING){
						detalle.setNumeroSerie(cell.toString());
					}else{
						detalle.setNumeroSerie(cell.toString());
					}
					lineaVacia = false;
				}else {
					error.setNumeroInciso(new BigDecimal(fila));
					error.setNombreSeccion(lineaDeNegocio);
					error.setNombreCampoError("NUMERO DE SERIE");
					error.setRecomendaciones("Ingrese el Numero de Serie");
					listErrores.add(error);
					mensajeError.append("Campo: ").append(error.getNombreCampoError()).append(" - ").append(error.getRecomendaciones()).append("\n");
				}
				break;
			case CAMPO_VALOR_COMERCIAL:
				if (cell != null && !cell.toString().isEmpty()) {
					try{
						detalle.setValorComercial(new BigDecimal(cell.toString()));
						lineaVacia = false;
					}catch(Exception e){
					}
				}
				break;
			case CAMPO_TOTAL_PASAJEROS:
				if (cell != null && !cell.toString().isEmpty()) {
					String valor = cell.toString();
					if (valor.indexOf(".") != -1) {
						valor = valor.substring(0, valor.indexOf("."));
					}
					try{
						detalle.setNumeroPasajeros(new Short(valor));
						lineaVacia = false;
					}catch(Exception e){
					}
				}
				break;
			case CAMPO_DESCRIPCION_VEHICULO:
				if (cell != null && !cell.toString().isEmpty()) {
					detalle.setDescripcionVehiculo(cell.toString().toUpperCase());
					lineaVacia = false;
				}
				break;
			case CAMPO_PRIMA_TOTAL:
				if (cell != null && !cell.toString().isEmpty()) {
					detalle.setPrimaTotal(new Double(cell.toString()));
					lineaVacia = false;
				}
				break;
			case CAMPO_DEDUCIBLE_DANOS_MATERIALES:
				if (cell != null && !cell.toString().isEmpty()) {
					try{
						detalle.setDeducibleDanosMateriales(new Double(cell.toString()));
						lineaVacia = false;
					}catch(Exception e){
					}
				}
				break;
			case CAMPO_DEDUCIBLE_ROBO_TOTAL:
				if (cell != null && !cell.toString().isEmpty()) {
					try{
						detalle.setDeducibleRoboTotal(new Double(cell.toString()));
						lineaVacia = false;
					}catch(Exception e){
					}
				}
				break;
			case CAMPO_LIMITE_RC_TERCEROS:
				if (cell != null && !cell.toString().isEmpty()) {
					try{
						detalle.setLimiteRcTerceros(new Double(cell.toString()));
						lineaVacia = false;
					}catch(Exception e){
					}
				}
				break;
			case CAMPO_DEDUCIBLE_RC_TERCEROS:
				if (cell != null && !cell.toString().isEmpty()) {
					try{
						detalle.setDeducibleRcTerceros(new Double(cell.toString()));
						lineaVacia = false;
					}catch(Exception e){
					}
				}
				break;
			case CAMPO_LIMITE_GASTOS_MEDICOS:
				if (cell != null && !cell.toString().isEmpty()) {
					try{
						detalle.setLimiteGastosMedicos(new Double(cell.toString()));
						lineaVacia = false;
					}catch(Exception e){
					}
				}
				break;
			case CAMPO_LIMITE_MUERTE:
				if (cell != null && !cell.toString().isEmpty()) {
					try{
						detalle.setLimiteMuerte(new Double(cell.toString()));
						lineaVacia = false;
					}catch(Exception e){
					}
				}
				break;
			case CAMPO_LIMITE_RC_VIAJERO:
				if (cell != null && !cell.toString().isEmpty()) {
					try{
						detalle.setLimiteRcViajero(new Double(cell.toString()));
						lineaVacia = false;
					}catch(Exception e){
					}
				}
				break;
			case CAMPO_ASISTENCIA_JURIDICA:
				if (cell != null && !cell.toString().isEmpty()) {
					detalle.setAsistenciaJuridica(cell.toString());
					lineaVacia = false;
				}
				break;
			case CAMPO_LIMITE_ADAPTACION_CONVERSION:
				if (cell != null && !cell.toString().isEmpty()) {
					try{
						detalle.setLimiteAdaptacionConversion(new Double(cell.toString()));
						lineaVacia = false;
					}catch(Exception e){
					}
				}
				break;
			case CAMPO_LIMITE_EQUIPO_ESPECIAL:
				if (cell != null && !cell.toString().isEmpty()) {
					try{
						detalle.setLimiteEquipoEspecial(new Double(cell.toString()));
						lineaVacia = false;
					}catch(Exception e){
					}
				}
				break;
			case CAMPO_DEDUCIBLE_EQUIPO_ESPECIAL:
				if (cell != null && !cell.toString().isEmpty()) {
					try{
						detalle.setDeducibleEquipoEspecial(new Double(cell.toString()));
						lineaVacia = false;
					}catch(Exception e){
					}
				}
				break;
			case CAMPO_IGUALACION:
				if (cell != null && !cell.toString().isEmpty()) {
					detalle.setIgualacion(cell.toString());
					lineaVacia = false;
				}
				break;
			case CAMPO_DERECHOS:
				if (cell != null && !cell.toString().isEmpty()) {
					try{
						detalle.setDerechos(new Double(cell.toString()));
						lineaVacia = false;
					}catch(Exception e){
						error.setNumeroInciso(new BigDecimal(fila));
						error.setNombreSeccion(lineaDeNegocio);
						error.setNombreCampoError("DERECHOS");
						error.setRecomendaciones("Valor Invalido");
						listErrores.add(error);
						mensajeError.append("Campo: ").append(error.getNombreCampoError()).append(" - ").append(error.getRecomendaciones()).append("\r\n").append(System.getProperty("line.separator"));
					}
				}else{
					error.setNumeroInciso(new BigDecimal(fila));
					error.setNombreSeccion(lineaDeNegocio);
					error.setNombreCampoError("DERECHOS");
					error.setRecomendaciones("Ingrese el Derecho");
					listErrores.add(error);
					mensajeError.append("Campo: ").append(error.getNombreCampoError()).append(" - ").append(error.getRecomendaciones()).append("\n");
				}
				break;	
			case CAMPO_SOLICITAR_AUTORIZACION:
				if (cell != null && !cell.toString().isEmpty()) {
					detalle.setSolicitarAutorizacion(cell.toString());
					lineaVacia = false;
				}
				break;	
			case CAMPO_CAUSA_AUTORIZACION:
				if (cell != null && !cell.toString().isEmpty()) {
					detalle.setCausaAutorizacion(cell.toString());
					lineaVacia = false;
				}
				break;
			case CAMPO_DESCRIPCION_EQUIPO_ESPECIAL:
				if (cell != null && !cell.toString().isEmpty()) {
					detalle.setDescripcionEquipoEspecial(cell.toString().toUpperCase());
					lineaVacia = false;
				}
				break;	
			case CAMPO_DESCRIPCION_ADAPTACION_CONVERSION:
				if (cell != null && !cell.toString().isEmpty()) {
					detalle.setDescripcionAdaptacionConversion(cell.toString().toUpperCase());
					lineaVacia = false;
				}
				break;
			case CAMPO_NOMBRE:
				if (cell != null && !cell.toString().isEmpty()) {
					detalle.setNombre(cell.toString().toUpperCase());
					lineaVacia = false;
				} else {
					if(datosConductorObligatorios){
					error.setNumeroInciso(new BigDecimal(fila));
					error.setNombreSeccion(lineaDeNegocio);
					error.setNombreCampoError("NOMBRE");
					error.setRecomendaciones("Ingrese un Nombre");
					listErrores.add(error);
					mensajeError.append("Campo: NOMBRE -  Ingrese un Nombre \n");
					}
				}
				break;
			case CAMPO_APELLIDO_PATERNO:
				if (cell != null && !cell.toString().isEmpty()) {
					detalle.setApellidoPaterno(cell.toString().toUpperCase());
					lineaVacia = false;
				} else {
					if(datosConductorObligatorios){
					error.setNumeroInciso(new BigDecimal(fila));
					error.setNombreSeccion(lineaDeNegocio);
					error.setNombreCampoError("APELLIDO PATERNO");
					error.setRecomendaciones("Ingrese el Apellido Paterno");
					listErrores.add(error);
					mensajeError.append("Campo: APELLIDO PATERNO -  Ingrese el Apellido Paterno \n");
					}
				}
				break;
			case CAMPO_APELLIDO_MATERNO:
				if (cell != null && !cell.toString().isEmpty()) {
					detalle.setApellidoMaterno(cell.toString().toUpperCase());
					lineaVacia = false;
				}else{
					if(datosConductorObligatorios){
					error.setNumeroInciso(new BigDecimal(fila));
					error.setNombreSeccion(lineaDeNegocio);
					error.setNombreCampoError("APELLIDO MATERNO");
					error.setRecomendaciones("Ingrese el Apellido Materno");
					listErrores.add(error);
					mensajeError.append("Campo: APELLIDO MATERNO -  Ingrese el Apellido Materno \n");
					}
				}
				break;
			case CAMPO_NUMERO_LICENCIA:
				if (cell != null && !cell.toString().isEmpty()) {
					detalle.setNumeroLicencia(cell.toString().toUpperCase());
					lineaVacia = false;
				}else{
					if(datosConductorObligatorios){
						error.setNumeroInciso(new BigDecimal(fila));
						error.setNombreSeccion(lineaDeNegocio);
						error.setNombreCampoError("NUMERO LICENCIA");
						error.setRecomendaciones("Ingrese el Numero de Licencia");
						listErrores.add(error);
						mensajeError.append("Campo: ").append(error.getNombreCampoError()).append(" - ").append(error.getRecomendaciones()).append(" \n");
					}					
				}
				break;
			case CAMPO_FECHA_NACIMIENTO:
				if (cell != null && !cell.toString().isEmpty()) {
					try {						
						detalle.setFechaNacimiento(cell.getDateCellValue());
						lineaVacia = false;
					} catch (Exception e) {
						if(datosConductorObligatorios){
							error.setNumeroInciso(new BigDecimal(fila));
							error.setNombreSeccion(lineaDeNegocio);
							error.setNombreCampoError("FECHA DE NACIMIENTO");
							error.setRecomendaciones("Fecha de Nacimiento Invalida");
							listErrores.add(error);
							mensajeError.append("Campo: FECHA DE NACIMIENTO -  Fecha de Nacimiento Invalida \n");
						}
					}
				} else {
					if(datosConductorObligatorios){
					error.setNumeroInciso(new BigDecimal(fila));
					error.setNombreSeccion(lineaDeNegocio);
					error.setNombreCampoError("FECHA DE NACIMIENTO");
					error.setRecomendaciones("Ingrese la Fecha de Nacimiento");
					listErrores.add(error);
					mensajeError.append("Campo: FECHA DE NACIMIENTO -  Ingrese la Fecha de Nacimiento \n");
					}
				}
				break;
			case CAMPO_OCUPACION:
				if (cell != null && !cell.toString().isEmpty()) {
					detalle.setOcupacion(cell.toString().toUpperCase());
					lineaVacia = false;
				}else{
					if(datosConductorObligatorios){
						error.setNumeroInciso(new BigDecimal(fila));
						error.setNombreSeccion(lineaDeNegocio);
						error.setNombreCampoError("OCUPACION");
						error.setRecomendaciones("Ingrese la Ocupacion");
						listErrores.add(error);
						mensajeError.append("Campo: ").append(error.getNombreCampoError()).append(" - ").append(error.getRecomendaciones()).append(" \n");
					}					
				}
				break;
			case CAMPO_IDCLIENTE:
				if (cell != null && !cell.toString().isEmpty()) {
					if(cell.getCellType() == HSSFCell.CELL_TYPE_NUMERIC){
						double valor = cell.getNumericCellValue();
						BigDecimal valorB = new BigDecimal(valor);
						detalle.setIdCliente(valorB);
						lineaVacia = false;
					}
				}
				break;
				case CAMPO_ENTREVISTA_PRIMA_ESTIMADA:
					if (cell != null && !cell.toString().isEmpty()) {
						entrevista.setPrimaEstimada(new BigDecimal(cell.toString()));
						;
					}
				break;
				case CAMPO_ENTREVISTA_MONEDA:
					if (cell != null && !cell.toString().isEmpty()) {
						BigDecimal moneda=new BigDecimal(cell.toString());
						entrevista.setMoneda(moneda.intValue());
						;
					}else{
						entrevista.setMoneda(484);					
					}
				break;
				case CAMPO_ENTREVISTA_PEP:
					if (cell != null && !cell.toString().isEmpty()) {
						String valPep=cell.toString();
						if(valPep.toUpperCase().contains("S")){
							entrevista.setPep(true);
						}else{
							entrevista.setPep(false);
						}
						
					}
				break;
				case CAMPO_ENTREVISTA_PARENTESCO:
					if (cell != null && !cell.toString().isEmpty()) {
						entrevista.setParentesco(cell.toString());
						
					}
					break;
				case CAMPO_ENTREVISTA_NOMBRE_PUESTO:
					if (cell != null && !cell.toString().isEmpty()) {
						entrevista.setNombrePuesto(cell.toString());
						
					}
					break;
				case CAMPO_ENTREVISTA_PERIODO_FUNCIONES:
					if (cell != null && !cell.toString().isEmpty()) {
						entrevista.setPeriodoFunciones(cell.toString());
						
					}
					break;
				case CAMPO_ENTREVISTA_CUOTA_PROPIA:
					if (cell != null && !cell.toString().isEmpty()) {
						
						
						String valPep=cell.toString();
						if(valPep.toUpperCase().contains("S")){
							entrevista.setCuentaPropia(true);
						}else{
							entrevista.setCuentaPropia(false);
						}entrevista.setCuentaPropia(Boolean.valueOf(cell.toString()));
						
					}
					break;
				case CAMPO_ENTREVISTA_NOMBRE_PARENTESCO:
					if (cell != null && !cell.toString().isEmpty()) {
						entrevista.setNombreRepresentado(cell.toString());
						
					}
				break;
				case CAMPO_ENTREVISTA_DOCUMENTACION_REPRESENTACIOM:
					if (cell != null && !cell.toString().isEmpty()) {
						
						String valPep=cell.toString();
						if(valPep.toUpperCase().contains("S")){
							entrevista.setDocumentoRepresentacion(true);
						}else{
							entrevista.setDocumentoRepresentacion(false);
						}
					}
				break;
				case CAMPO_ENTREVISTA_NUMERO_FIEL:
					if (cell != null && !cell.toString().isEmpty()) {
						entrevista.setNumeroSerieFiel(cell.toString());
						
					}
				break;
				case CAMPO_ENTREVISTA_FOLIO_MERCANTIL:
					if (cell != null && !cell.toString().isEmpty()) {
						entrevista.setFolioMercantil(cell.toString());
						
					}
				break;
				
			}
			
		}
		try {
			clienteRest.findInterViews(detalle.getIdCliente().longValue());
		} catch (InterviewException e) {
		clienteRest.createInterview(detalle.getIdCliente().longValue(), entrevista);}
		detalle.setMensajeError(mensajeError.toString());
		if (mensajeError.toString().isEmpty()) {
			detalle.setClaveEstatus(DetalleCargaMasivaAutoCot.ESTATUS_PENDIENTE);
		} else {
			detalle.setClaveEstatus(DetalleCargaMasivaAutoCot.ESTATUS_ERROR);
		}
		if(lineaVacia && !mensajeError.toString().isEmpty()){
			Iterator<LogErroresCargaMasivaDTO> it = listErrores.iterator();
			while(it.hasNext()){
				LogErroresCargaMasivaDTO error = it.next();
				if(error.getNumeroInciso().equals(new BigDecimal(fila))){
					it.remove();
				}
			}
			return null;
		}

		return detalle;
	}
	

	public void validaDetalleAutoExpedibles(
			List<AutoExpediblesDetalleAutoCot> detalleCargaMasivaList, HSSFSheet sheet) {
		StringBuilder mensajeError = new StringBuilder();
		int fila = 1;
		for (AutoExpediblesDetalleAutoCot detalle : detalleCargaMasivaList) {

			fila++;
			try{
			if (detalle.getClaveEstatus() != null && !detalle.getClaveEstatus().equals(
					DetalleCargaMasivaAutoCot.ESTATUS_ERROR)) {
				
				mensajeError.delete(0, mensajeError.length());
				
				CotizacionDTO cotizacion = new CotizacionDTO();
				
				Negocio negocio = negocioTipoPoliza.getNegocioProducto().getNegocio();
				SolicitudDTO solicitud = new SolicitudDTO();
				solicitud.setNegocio(negocio);
				
				//TODO Valida AutoExpedible
				
				//Codigo Agente				
				solicitud.setCodigoAgente(detalle.getIdAgente());
				Agente agente = cargaMasivaService.validaAgente(solicitud.getCodigoAgente().longValue(), "A", negocio.getIdToNegocio());
				if(agente == null){
					LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
					error.setNumeroInciso(new BigDecimal(fila));
					error.setNombreSeccion("");
					error.setNombreCampoError("AGENTE");
					error.setRecomendaciones("Agente Invalido");
					listErrores.add(error);
					mensajeError.append("Campo: ").append(error.getNombreCampoError()).append(" - ").append(error.getRecomendaciones()).append("\n");
				}else{
					solicitud.setCodigoAgente(new BigDecimal(agente.getId()));
				}
				
				//Moneda
				if(detalle.getMoneda().equals(DOLARES)){
					cotizacion.setIdMoneda(new BigDecimal(840));
				}
				if(detalle.getMoneda().equals(PESOS)){
					cotizacion.setIdMoneda(new BigDecimal(484));
				}
				
				
				cotizacion.setSolicitudDTO(solicitud);
				cotizacion.setNegocioTipoPoliza(getNegocioTipoPoliza());
				
				cotizacion = cargaMasivaService.crearCotizacion(cotizacion);
				
				cotizacion.setFechaInicioVigencia(detalle.getFechaVigenciaInicio());
				cotizacion.setFechaFinVigencia(detalle.getFechaVigenciaFin());
				
				FormaPagoDTO formaPago = cargaMasivaService.obtieneFormaPago(detalle.getFormaPago());
				if(formaPago != null){
					cotizacion.setIdFormaPago(new BigDecimal(formaPago.getIdFormaPago()));
				}else{
					LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
					error.setNumeroInciso(new BigDecimal(fila));
					error.setNombreSeccion("");
					error.setNombreCampoError("FORMA DE PAGO");
					error.setRecomendaciones("Forma de Pago Invalido");
					listErrores.add(error);
					mensajeError.append("Campo: ").append(error.getNombreCampoError()).append(" - ").append(error.getRecomendaciones()).append("\n");
				}
				
				MedioPagoDTO medioPago = cargaMasivaService.obtieneMedioPago("AGENTE");
				if(medioPago != null){
					cotizacion.setIdMedioPago(new BigDecimal(medioPago.getIdMedioPago()));
				}else{
					LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
					error.setNumeroInciso(new BigDecimal(fila));
					error.setNombreSeccion("");
					error.setNombreCampoError("MEDIO DE PAGO");
					error.setRecomendaciones("Medio de Pago Efectivo no definido en sistema");
					listErrores.add(error);
					mensajeError.append("Campo: ").append(error.getNombreCampoError()).append(" - ").append(error.getRecomendaciones()).append("\n");
				}
				
				NegocioDerechoPoliza negocioDerechoPoliza = cargaMasivaService.obtieneNegocioDerechoPoliza(new BigDecimal(negocio.getIdToNegocio()), detalle.getDerechos());
				if(negocioDerechoPoliza != null){
					cotizacion.setNegocioDerechoPoliza(negocioDerechoPoliza);
				}else{
					LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
					error.setNumeroInciso(new BigDecimal(fila));
					error.setNombreSeccion("");
					error.setNombreCampoError("DERECHOS");
					error.setRecomendaciones("Derechos Invalidos");
					listErrores.add(error);
					mensajeError.append("Campo: ").append(error.getNombreCampoError()).append(" - ").append(error.getRecomendaciones()).append("\n");
				}
				
				//Bono Comision
				if(cotizacion.getPorcentajebonifcomision() == null){
					cotizacion.setPorcentajebonifcomision(new Double(0));
				}
				if(cotizacion.getPorcentajePagoFraccionado() == null){
					try{
						Double pctPagoFrac = cargaMasivaService
								.getPctePagoFraccionado(cotizacion
										.getIdFormaPago().intValue(),
										cotizacion.getIdMoneda().shortValue());
						if (pctPagoFrac == null) {
							cotizacion.setPorcentajePagoFraccionado(new Double(
									0));
						} else {
							cotizacion
									.setPorcentajePagoFraccionado(pctPagoFrac);
						}
					}catch(Exception e){
						cotizacion.setPorcentajePagoFraccionado(new Double(0));
					}
				}
				
				//IVA
				Double iva = cargaMasivaService.obtieneIVAPorCodigoPostalColonia(detalle.getCodigoPostalCliente(), detalle.getColoniaCliente());
				cotizacion.setPorcentajeIva(iva);
				
				//Contratante
				ClienteGenericoDTO cliente = cargaMasivaService.validaClienteContratante(detalle);
			
				
				
				if(cliente != null){
					//se valida cliente peps
					
					if (cliente.getIdDomicilioConsulta() == null || cliente.getCodigoPostalFiscal() == null
							|| cliente.getCodigoPostalFiscal().equals("")) {
						LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
						error.setNumeroInciso(new BigDecimal(fila));
						error.setNombreSeccion("");
						error.setNombreCampoError("CLIENTE");
						error.setRecomendaciones("Favor de Verificar Codigo Postal y Domicilio");
						listErrores.add(error);
						mensajeError.append("Campo: ").append(error.getNombreCampoError()).append(" - ").append(error.getRecomendaciones()).append("\n");
					}else{
						cotizacion.setNombreAsegurado(detalle.getNombreCompleto());
						cotizacion.setIdToPersonaAsegurado(cliente.getIdCliente());
						cotizacion.setIdDomicilioContratante(BigDecimal.valueOf(cliente.getIdDomicilioConsulta()));
						cotizacion.setIdToPersonaContratante(cliente.getIdCliente());
						cotizacion.setNombreContratante(cliente.getNombreCompleto());
						cotizacion.getSolicitudDTO().setClaveTipoPersona(cliente.getClaveTipoPersona());
					}
				}else{
					LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
					error.setNumeroInciso(new BigDecimal(fila));
					error.setNombreSeccion("");
					error.setNombreCampoError("CLIENTE");
					error.setRecomendaciones("Cliente No Existe");
					listErrores.add(error);
					mensajeError.append("Campo: ").append(error.getNombreCampoError()).append(" - ").append(error.getRecomendaciones()).append("\n");
				}
				
				//TODO VALIDACION AUTOEXPEDIBLE
				if(detalle.getIdPolizaAutoExpedible() != null){
					cotizacion.setNumeroAutoexpedible(detalle.getIdPolizaAutoExpedible());
				}
				
				cotizacion = cargaMasivaService.guardarCotizacion(cotizacion);
				
				
				if(cotizacion == null){
					throw new Exception("");
				}
				detalle.setIdToCotizacion(cotizacion.getIdToCotizacion());
				
				HSSFCell cell = sheet.getRow((fila - 1)).getCell(CAMPO_NUMERO_COTIZACION);
				if(cell == null){
					cell = sheet.getRow((fila - 1)).createCell(CAMPO_NUMERO_COTIZACION);
				}
				cell.setCellValue(cotizacion.getIdToCotizacion().toPlainString());
				
				//INCISO
				boolean requiereAutorizacion = false;
				if(mensajeError.toString().isEmpty()){
					IncisoCotizacionDTO incisoCotizacion = new IncisoCotizacionDTO();
					IncisoCotizacionId incisoCotizacionId = new IncisoCotizacionId();
					incisoCotizacionId.setIdToCotizacion(cotizacion
							.getIdToCotizacion());
					incisoCotizacion.setCotizacionDTO(cotizacion);
					incisoCotizacion.setId(incisoCotizacionId);
					
					IncisoAutoCot incisoAutoCot = new IncisoAutoCot();

					NegocioSeccion negocioSeccion = null;
					negocioSeccion = cargaMasivaService
							.obtieneNegocioSeccionPorDescripcion(cotizacion,
									this.remplazaGuion(detalle.getLineaNegocio()));
					if (negocioSeccion != null) {
						incisoAutoCot.setNegocioSeccionId(negocioSeccion
								.getIdToNegSeccion().longValue());
					} else {
						LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
						error.setNumeroInciso(new BigDecimal(fila));
						error.setNombreSeccion("");
						error.setNombreCampoError("LINEA DE NEGOCIO");
						error.setRecomendaciones("Linea de Negocio Invalida");
						listErrores.add(error);
						mensajeError.append("Campo: LINEA DE NEGOCIO -  Linea de Negocio Invalida \n");
					}
					
					NegocioTipoUso negocioTipoUso = cargaMasivaService
							.obtieneNegocioTipoUsoPorDescripcion(
									negocioSeccion,
									detalle.getClaveUso());
					if (negocioTipoUso != null) {
						incisoAutoCot.setTipoUsoId(negocioTipoUso
								.getTipoUsoVehiculoDTO()
								.getIdTcTipoUsoVehiculo().longValue());
					} else {
						LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
						error.setNumeroInciso(new BigDecimal(fila));
						error.setNombreSeccion("");
						error.setNombreCampoError("TIPO DE USO");
						error.setRecomendaciones("TipoUso Invalido");
						listErrores.add(error);
						mensajeError.append("Campo: TIPO DE USO -  TipoUso Invalido \n");
					}
					
					EstiloVehiculoDTO estiloVehiculo = cargaMasivaService
							.obtieneEstiloVehiculoDTOPorClaveAMIS(cotizacion,
									negocioSeccion, detalle.getClaveAMIS());
					if (estiloVehiculo != null) {
						incisoAutoCot.setMarcaId(estiloVehiculo
								.getMarcaVehiculoDTO().getIdTcMarcaVehiculo());
						incisoAutoCot.setEstiloId(estiloVehiculo.getId()
								.getStrId());
						
						try{
							incisoAutoCot.setClaveTipoBien(estiloVehiculo.getId().getClaveTipoBien());
							incisoAutoCot.setIdVersionCarga(estiloVehiculo.getId().getIdVersionCarga());
							incisoAutoCot.setIdMoneda(cotizacion.getIdMoneda().shortValue());
						}catch(Exception e){
						}
						
						if(detalle.getDescripcionVehiculo() != null && !detalle.getDescripcionVehiculo().isEmpty()){
							incisoAutoCot.setDescripcionFinal(detalle.getDescripcionVehiculo().toUpperCase());
						}else{
							incisoAutoCot.setDescripcionFinal(estiloVehiculo.getDescripcionEstilo());
							detalle.setDescripcionVehiculo(estiloVehiculo.getDescripcionEstilo().toUpperCase());
						}
					} else {
						LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
						error.setNumeroInciso(new BigDecimal(fila));
						error.setNombreSeccion("");
						error.setNombreCampoError("CLAVE AMIS");
						error.setRecomendaciones("Clave AMIS Invalida");
						listErrores.add(error);
						mensajeError.append("Campo: CLAVE AMIS -  Clave AMIS Invalida \n");
					}
					
					if(estiloVehiculo != null){
					if(cargaMasivaService.validaModeloVehiculo(cotizacion.getIdMoneda(), estiloVehiculo.getId().getStrId(), 
							negocioSeccion.getIdToNegSeccion(), detalle.getModelo())){
						incisoAutoCot.setModeloVehiculo(detalle.getModelo());
					}else{
						LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
						error.setNumeroInciso(new BigDecimal(fila));
						error.setNombreSeccion("");
						error.setNombreCampoError("MODELO VEHICULO");
						error.setRecomendaciones("Modelo Vehiculo Invalido");
						listErrores.add(error);
						mensajeError.append("Campo: ").append(error.getNombreCampoError() ).append(" -  ").append(error.getRecomendaciones()).append(" \n");						
					}
					}
					

					NegocioPaqueteSeccion negocioPaquete = cargaMasivaService
							.obtieneNegocioPaqueteSeccionPorDescripcion(
									negocioSeccion, detalle.getPaquete());

					if (negocioPaquete != null) {
						incisoAutoCot.setNegocioPaqueteId(negocioPaquete
								.getIdToNegPaqueteSeccion());
					} else {
						LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
						error.setNumeroInciso(new BigDecimal(fila));
						error.setNombreSeccion("");
						error.setNombreCampoError("PAQUETE");
						error.setRecomendaciones("Paquete Invalido");
						listErrores.add(error);
						mensajeError.append("Campo: PAQUETE -  Paquete Invalido \n");
					}
					
					String municipioId = cargaMasivaService
							.obtieneMunicipioIdPorCodigoPostal(detalle.getCodigoPostalCliente());
					if (municipioId != null) {
						incisoAutoCot.setMunicipioId(municipioId);

						
						String estadoId = cargaMasivaService
								.obtieneEstadoIdPorMunicipio(municipioId);
						incisoAutoCot.setEstadoId(estadoId);
						
						if(!cargaMasivaService.validaEstadoMunicipio(negocio.getIdToNegocio(), estadoId, municipioId)){
							LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
							error.setNumeroInciso(new BigDecimal(fila));
							error.setNombreSeccion("");
							error.setNombreCampoError("CODIGO POSTAL");
							error.setRecomendaciones("Codigo Postal Invalido");
							listErrores.add(error);
							mensajeError.append("Campo: CODIGO POSTAL -  Codigo Postal Invalido \n");							
						}

					} else {
						LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
						error.setNumeroInciso(new BigDecimal(fila));
						error.setNombreSeccion("");
						error.setNombreCampoError("CODIGO POSTAL");
						error.setRecomendaciones("Codigo Postal Invalido");
						listErrores.add(error);
						mensajeError.append("Campo: CODIGO POSTAL -  Codigo Postal Invalido \n");
					}
					
					if (mensajeError.toString().isEmpty()) {
						incisoCotizacion.setIncisoAutoCot(incisoAutoCot);
						List<CoberturaCotizacionDTO> coberturaCotizacionList = cargaMasivaService
								.obtieneCoberturasDelInciso(incisoCotizacion,
										negocioPaquete, estiloVehiculo.getId());
						
						
					    if(coberturaCotizacionList != null && !coberturaCotizacionList.isEmpty()){
							Short claveContrato = 1;
							Short claveObligatoriedad = 0;
							Short valorSI = 1;
							Short valorNO = 0;
							for(CoberturaCotizacionDTO coberturaCotizacion : coberturaCotizacionList){
								
				    			//Valida si es obligatoria
								Boolean esObligatoria = false;
				    			if(coberturaCotizacion.getClaveContrato().equals(claveContrato) && 
				    					coberturaCotizacion.getClaveObligatoriedad().equals(claveObligatoriedad)){
				    				esObligatoria = true;
				    			}
				    			
				    			//Valida Rangos
				    			Boolean validaRangos = false;
				    			if(coberturaCotizacion.getCoberturaSeccionDTO().getCoberturaDTO().getClaveFuenteSumaAsegurada().equals("0") &&
				    					coberturaCotizacion.getValorSumaAseguradaMin() != 0 && coberturaCotizacion.getValorSumaAseguradaMax() != 0){
				    				validaRangos = true;
				    			}
				    			
				    			//Valida Deducibles
				    			Boolean validaDeducibles = false;
				    			if(!coberturaCotizacion.getCoberturaSeccionDTO().getCoberturaDTO().getClaveTipoDeducible().equals("0")){
				    				validaDeducibles = true;
				    			}
				    			
				    			System.out.println("Cobertura: " + coberturaCotizacion.getCoberturaSeccionDTO().getCoberturaDTO().getNombreComercial());
					    		if(coberturaCotizacion.getCoberturaSeccionDTO().getCoberturaDTO().getNombreComercial().toUpperCase().equals(
					    				GeneraExcelCargaMasiva.NOMBRE_DEDUCIBLE_DANOS_MATERIALES.toUpperCase())){
					    			if(detalle.getDeducibleDanosMateriales() != null){
					    				boolean valido = true;
					    				if(validaDeducibles){
					    					valido = validaDeducible(coberturaCotizacion.getDeducibles(), detalle.getDeducibleDanosMateriales());
					    				}
					    				if(valido){
					    					coberturaCotizacion.setValorDeducible(detalle.getDeducibleDanosMateriales());
					    					coberturaCotizacion.setPorcentajeDeducible(detalle.getDeducibleDanosMateriales());
					    					coberturaCotizacion.setClaveContrato(claveContrato);
					    				}else{
					    					LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
					    					error.setNumeroInciso(new BigDecimal(fila));
					    					error.setNombreSeccion("");
					    					error.setNombreCampoError("DEDUCIBLE DA\u00d1OS MATERIALES");
					    					error.setRecomendaciones("Valor no valido");
					    					listErrores.add(error);
					    					mensajeError.append("Campo: ").append(error.getNombreCampoError()).append(" - ").append(error.getRecomendaciones()).append(" \n");
					    				}
					    			}
						 		}else if(coberturaCotizacion.getCoberturaSeccionDTO().getCoberturaDTO().getNombreComercial().toUpperCase().equals(
						 				GeneraExcelCargaMasiva.NOMBRE_DEDUCIBLE_ROBO_TOTAL.toUpperCase())){
					    			if(detalle.getDeducibleRoboTotal() != null){
					    				boolean valido = true;
					    				if(validaDeducibles){
					    					valido = validaDeducible(coberturaCotizacion.getDeducibles(), detalle.getDeducibleRoboTotal());
					    				}
					    				if(valido){
					    					coberturaCotizacion.setValorDeducible(detalle.getDeducibleRoboTotal());
					    					coberturaCotizacion.setPorcentajeDeducible(detalle.getDeducibleRoboTotal());
					    					coberturaCotizacion.setClaveContrato(claveContrato);
					    				}else{
					    					LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
					    					error.setNumeroInciso(new BigDecimal(fila));
					    					error.setNombreSeccion("");
					    					error.setNombreCampoError("DEDUCIBLE ROBO TOTAL");
					    					error.setRecomendaciones("Valor no valido");
					    					listErrores.add(error);
					    					mensajeError.append("Campo: ").append(error.getNombreCampoError()).append(" - ").append(error.getRecomendaciones()).append(" \n");
					    				}
					    			}
						 		}else if(coberturaCotizacion.getCoberturaSeccionDTO().getCoberturaDTO().getNombreComercial().toUpperCase().equals(
						 				GeneraExcelCargaMasiva.NOMBRE_LIMITE_RC_TERCEROS.toUpperCase())){
							    		if(detalle.getLimiteRcTerceros() != null){
							    			boolean valido = true;
							    			if(validaRangos){
							    				valido = validaRangos(coberturaCotizacion.getValorSumaAseguradaMin(), 
							    						coberturaCotizacion.getValorSumaAseguradaMax(), detalle.getLimiteRcTerceros());
							    			}
							    			if(valido){
								    			coberturaCotizacion.setValorSumaAsegurada(detalle.getLimiteRcTerceros());
								    			coberturaCotizacion.setClaveContrato(claveContrato);						    				
							    			}else{
						    					LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
						    					error.setNumeroInciso(new BigDecimal(fila));
						    					error.setNombreSeccion("");
						    					error.setNombreCampoError("LIMITE RC TERCEROS");
						    					error.setRecomendaciones("Valor no valido");
						    					listErrores.add(error);
						    					mensajeError.append("Campo: LIMITE RC TERCEROS -  Valor debe ser entre ").append(coberturaCotizacion.getValorSumaAseguradaMin()).append(
						    					" y " ).append(coberturaCotizacion.getValorSumaAseguradaMax()).append(" ");
							    			}
							    			//
						    				if(validaDeducibles){
						    					valido = validaDeducible(coberturaCotizacion.getDeducibles(), detalle.getDeducibleRcTerceros());
						    				}
							    			if(valido){
								    			coberturaCotizacion.setValorDeducible(detalle.getDeducibleRcTerceros());
								    			coberturaCotizacion.setPorcentajeDeducible(detalle.getDeducibleRcTerceros());
								    			coberturaCotizacion.setClaveContrato(claveContrato);						    				
							    			}else{
						    					LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
						    					error.setNumeroInciso(new BigDecimal(fila));
						    					error.setNombreSeccion("");
						    					error.setNombreCampoError("DEDUCIBLE RC TERCEROS");
						    					error.setRecomendaciones("Valor no valido");
						    					listErrores.add(error);
						    					mensajeError.append("Campo: ").append(error.getNombreCampoError()).append(" -  ").append(error.getRecomendaciones());
							    			}
							    		}
						 		}else if(coberturaCotizacion.getCoberturaSeccionDTO().getCoberturaDTO().getNombreComercial().toUpperCase().equals(
						 				GeneraExcelCargaMasiva.NOMBRE_LIMITE_GASTOS_MEDICOS.toUpperCase())){
								    		if(detalle.getLimiteGastosMedicos() != null){
								    			boolean valido = true;
								    			if(validaRangos){
								    				valido = validaRangos(coberturaCotizacion.getValorSumaAseguradaMin(), 
								    						coberturaCotizacion.getValorSumaAseguradaMax(), detalle.getLimiteGastosMedicos());
								    			}
								    			if(valido){
									    			coberturaCotizacion.setValorSumaAsegurada(detalle.getLimiteGastosMedicos());
									    			coberturaCotizacion.setClaveContrato(claveContrato);					    				
								    			}else{
							    					LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
							    					error.setNumeroInciso(new BigDecimal(fila));
							    					error.setNombreSeccion("");
							    					error.setNombreCampoError("LIMITE GASTOS MEDICOS");
							    					error.setRecomendaciones("Valor no valido");
							    					listErrores.add(error);
							    					mensajeError.append("Campo: LIMITE GASTOS MEDICOS -  Valor debe ser entre ").append(coberturaCotizacion.getValorSumaAseguradaMin()).append(
							    					" y ").append(coberturaCotizacion.getValorSumaAseguradaMax()).append(" ");
								    			}
								    		}
						 		}else if(coberturaCotizacion.getCoberturaSeccionDTO().getCoberturaDTO().getNombreComercial().toUpperCase().equals(
						 				GeneraExcelCargaMasiva.NOMBRE_LIMITE_MUERTE.toUpperCase())){
								    		if(detalle.getLimiteMuerte() != null){
								    			boolean valido = true;
								    			if(validaRangos){
								    				valido = validaRangos(coberturaCotizacion.getValorSumaAseguradaMin(), 
								    						coberturaCotizacion.getValorSumaAseguradaMax(), detalle.getLimiteMuerte());
								    			}
								    			if(valido){
									    			coberturaCotizacion.setValorSumaAsegurada(detalle.getLimiteMuerte());
									    			coberturaCotizacion.setClaveContrato(claveContrato);						    				
								    			}else{
							    					LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
							    					error.setNumeroInciso(new BigDecimal(fila));
							    					error.setNombreSeccion("");
							    					error.setNombreCampoError("LIMITE MUERTE");
							    					error.setRecomendaciones("Valor no valido");
							    					listErrores.add(error);
							    					mensajeError.append("Campo: LIMITE MUERTE -  Valor debe ser entre ").append(coberturaCotizacion.getValorSumaAseguradaMin()).append(
							    					" y ").append(coberturaCotizacion.getValorSumaAseguradaMax()).append(" ");
								    			}
								    		}else{
								    			if(!esObligatoria){
									    			coberturaCotizacion.setClaveContrato(valorNO);
									    		}
									    	}
						 		}else if(coberturaCotizacion.getCoberturaSeccionDTO().getCoberturaDTO().getNombreComercial().toUpperCase().equals(
						 				GeneraExcelCargaMasiva.NOMBRE_LIMITE_RC_VIAJERO.toUpperCase())){
								    		if(detalle.getLimiteRcViajero() != null){
								    			boolean valido = true;
								    			if(validaRangos){
								    				valido = validaRangos(coberturaCotizacion.getValorSumaAseguradaMin(), 
								    						coberturaCotizacion.getValorSumaAseguradaMax(), detalle.getLimiteRcViajero());
								    			}
								    			if(valido){
									    			coberturaCotizacion.setValorSumaAsegurada(detalle.getLimiteRcViajero());
									    			coberturaCotizacion.setClaveContrato(claveContrato);						    				
								    			}else{
							    					LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
							    					error.setNumeroInciso(new BigDecimal(fila));
							    					error.setNombreSeccion("");
							    					error.setNombreCampoError("LIMITE RC VIAJERO");
							    					error.setRecomendaciones("Valor no valido");
							    					listErrores.add(error);
							    					mensajeError.append("Campo: LIMITE RC VIAJERO -  Valor debe ser entre ").append(coberturaCotizacion.getValorSumaAseguradaMin()).append(
							    					" y ").append(coberturaCotizacion.getValorSumaAseguradaMax()).append(" ");
								    			}
								    		}
						 		}else if(coberturaCotizacion.getCoberturaSeccionDTO().getCoberturaDTO().getNombreComercial().toUpperCase().equals(
						 				GeneraExcelCargaMasiva.NOMBRE_ASISTENCIA_JURIDICA.toUpperCase())){
								    		if(detalle.getAsistenciaJuridica() != null){
								    			if(detalle.getAsistenciaJuridica().equals(valorSI)){
								    				coberturaCotizacion.setClaveContrato(claveContrato);
								    			}else{
								    				if(!esObligatoria){
									    				coberturaCotizacion.setClaveContrato(valorNO);
									    			}
								    			}
								    		}
						 		}else if(coberturaCotizacion.getCoberturaSeccionDTO().getCoberturaDTO().getNombreComercial().toUpperCase().startsWith(
						 				GeneraExcelCargaMasiva.NOMBRE_ASISTENCIA_EN_VIAJES.toUpperCase())){

						 			//
						 		}else if(coberturaCotizacion.getCoberturaSeccionDTO().getCoberturaDTO().getNombreComercial().toUpperCase().equals(
						 				GeneraExcelCargaMasiva.NOMBRE_EXENCION_DEDUCIBLE_DANOS.toUpperCase())){
						 			//
						 		}else if(coberturaCotizacion.getCoberturaSeccionDTO().getCoberturaDTO().getNombreComercial().toUpperCase().equals(
						 				GeneraExcelCargaMasiva.NOMBRE_LIMITE_ACCIDENTE_CONDUCTOR.toUpperCase())){
						 			//
						 		}else if(coberturaCotizacion.getCoberturaSeccionDTO().getCoberturaDTO().getNombreComercial().toUpperCase().equals(
						 				GeneraExcelCargaMasiva.NOMBRE_EXTENCION_RC.toUpperCase())){
						 			//
						 		}else if(coberturaCotizacion.getCoberturaSeccionDTO().getCoberturaDTO().getNombreComercial().toUpperCase().equals(
						 				GeneraExcelCargaMasiva.NOMBRE_LIMITE_ADAPTACION_CONVERSION.toUpperCase())){
						    		if(detalle.getLimiteAdaptacionConversion() != null){
						    			boolean valido = true;
						    			if(validaRangos){
						    				valido = validaRangos(coberturaCotizacion.getValorSumaAseguradaMin(), 
						    						coberturaCotizacion.getValorSumaAseguradaMax(), detalle.getLimiteAdaptacionConversion());
						    			}
						    			if(valido){
							    			coberturaCotizacion.setValorSumaAsegurada(detalle.getLimiteAdaptacionConversion());
							    			coberturaCotizacion.setClaveContrato(claveContrato);					    				
						    			}else{
					    					LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
					    					error.setNumeroInciso(new BigDecimal(fila));
					    					error.setNombreSeccion("");
					    					error.setNombreCampoError("LIMITE ADAPTACION CONVERSION");
					    					error.setRecomendaciones("Valor no valido");
					    					listErrores.add(error);
					    					mensajeError.append("Campo: LIMITE ADAPTACION CONVERSION -  Valor debe ser entre ").append(coberturaCotizacion.getValorSumaAseguradaMin()).append(
					    					" y ").append(coberturaCotizacion.getValorSumaAseguradaMax()).append("").append("\r\n").append(System.getProperty("line.separator"));
						    			}
						    		}
						 		}else if(coberturaCotizacion.getCoberturaSeccionDTO().getCoberturaDTO().getNombreComercial().toUpperCase().equals(
						 				GeneraExcelCargaMasiva.NOMBRE_LIMITE_EQUIPO_ESPECIAL.toUpperCase())){
						    		if(detalle.getLimiteEquipoEspecial() != null && detalle.getDeducibleEquipoEspecial() != null){
						    			boolean valido = true;
						    			if(validaRangos){
						    				valido = validaRangos(coberturaCotizacion.getValorSumaAseguradaMin(), 
						    						coberturaCotizacion.getValorSumaAseguradaMax(), detalle.getLimiteEquipoEspecial());
						    			}
						    			if(valido){
							    			coberturaCotizacion.setValorSumaAsegurada(detalle.getLimiteEquipoEspecial());
							    			coberturaCotizacion.setClaveContrato(claveContrato);					    				
						    			}else{
					    					LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
					    					error.setNumeroInciso(new BigDecimal(fila));
					    					error.setNombreSeccion("");
					    					error.setNombreCampoError("LIMITE EQUIPO ESPECIAL");
					    					error.setRecomendaciones("Valor no valido");
					    					listErrores.add(error);
					    					mensajeError.append("Campo: LIMITE EQUIPO ESPECIAL -  Valor debe ser entre ").append(coberturaCotizacion.getValorSumaAseguradaMin()).append(
					    					" y ").append(coberturaCotizacion.getValorSumaAseguradaMax()).append("").append("\r\n").append(System.getProperty("line.separator"));
						    			}
					    				valido = true;
					    				if(validaDeducibles){
					    					valido = validaDeducible(coberturaCotizacion.getDeducibles(), detalle.getDeducibleEquipoEspecial());
					    				}
					    				if(valido){
					    					coberturaCotizacion.setValorDeducible(detalle.getDeducibleEquipoEspecial());
					    					coberturaCotizacion.setPorcentajeDeducible(detalle.getDeducibleEquipoEspecial());
					    					coberturaCotizacion.setClaveContrato(claveContrato);
					    				}else{
					    					LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
					    					error.setNumeroInciso(new BigDecimal(fila));
					    					error.setNombreSeccion("");
					    					error.setNombreCampoError("DEDUCIBLE EQUIPO ESPECIAL");
					    					error.setRecomendaciones("Valor no valido");
					    					listErrores.add(error);
					    					mensajeError.append("Campo: DEDUCIBLE EQUIPO ESPECIAL -  Valor no valido ").append("\r\n").append(System.getProperty("line.separator"));
					    				}
						    		}
						 		}else if(coberturaCotizacion.getCoberturaSeccionDTO().getCoberturaDTO().getNombreComercial().toUpperCase().equals(
						 				GeneraExcelCargaMasiva.NOMBRE_EXENCION_DEDUCIBLE_ROBO.toUpperCase())){
						 			//
						 		}else if(coberturaCotizacion.getCoberturaSeccionDTO().getCoberturaDTO().getNombreComercial().toUpperCase().equals(
						 				GeneraExcelCargaMasiva.NOMBRE_RESPONSABILIDAD_CIVIL_USA.toUpperCase())){
						 			//
						 		}
						 
						 	}
						 
						}
					  

					// Asegurado
					if (detalle.getNombreORazonSocial() != null) {
						incisoCotizacion.getIncisoAutoCot().setNombreAsegurado(
								detalle.getNombreCompleto().toUpperCase());
						incisoCotizacion.getIncisoAutoCot().setPersonaAseguradoId(cliente.getIdCliente().longValue());
					}

				
					//Datos Vehiculo
					incisoCotizacion.getIncisoAutoCot().setNumeroMotor(
							detalle.getNumeroMotor());
					
					String numeroSerieRempl = remplazarCaracteresNumSerie(detalle.getNumeroSerie());
					
					incisoCotizacion.getIncisoAutoCot().setNumeroSerie(numeroSerieRempl);
					List<String> errors = NumSerieValidador.getInstance()
							.validate(
									incisoCotizacion.getIncisoAutoCot()
											.getNumeroSerie());
					if (!errors.isEmpty()) {
						LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
						error.setNumeroInciso(new BigDecimal(fila));
						error.setNombreSeccion("");
						error.setNombreCampoError("NUMERO DE SERIE");
						error.setRecomendaciones("Error al validar el n\u00FAmero de serie:");
						listErrores.add(error);
						mensajeError.append("Campo: NUMERO DE SERIE -  ").append(errors.get(0)).append("");
					}
					incisoCotizacion.getIncisoAutoCot().setPlaca(
							detalle.getPlacas());
					incisoCotizacion.getIncisoAutoCot().setRepuve(
							detalle.getNciRepuve());
					
					
					
						if (mensajeError.toString().isEmpty()) {

							try{
								incisoCotizacion = cargaMasivaService.guardaIncisoCotizacion(incisoCotizacion,
												coberturaCotizacionList);

								cargaMasivaService.calculoInciso(incisoCotizacion);
								
								
								Boolean datosConductorObligatorios = false;
								try{
									datosConductorObligatorios = cargaMasivaService.datosDelConductorRequeridos(incisoCotizacion);
								}catch(Exception e){
									
								}
								
								//Valida datos de Riesgo
								List<CoberturaCotizacionDTO> coberturas = cargaMasivaService.obtieneCoberturasContratadas(incisoCotizacion);
								List<DatoIncisoCotAuto> datoIncisoCotAutos = new ArrayList<DatoIncisoCotAuto>();
								DatoIncisoCotAuto datoIncisoCotAutoEquipoEspecial = null;
								DatoIncisoCotAuto datoIncisoCotAutoAdaptacionConversion = null;
								for(CoberturaCotizacionDTO coberturaCotizacion : coberturas){
									if(coberturaCotizacion.getCoberturaSeccionDTO().getCoberturaDTO().getNombreComercial().toUpperCase().equals(
											GeneraExcelCargaMasiva.NOMBRE_LIMITE_EQUIPO_ESPECIAL.toUpperCase())){
										datoIncisoCotAutoEquipoEspecial = cargaMasivaService.obtieneDatoIncisoCotAuto(cotizacion, coberturaCotizacion, incisoCotizacion.getId().getNumeroInciso(), 
												detalle.getDescripcionEquipoEspecial());
										if(datoIncisoCotAutoEquipoEspecial == null){
											LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
											error.setNumeroInciso(new BigDecimal(fila));
											error.setNombreSeccion("");
											error.setNombreCampoError("DESCRIPCION EQUIPO ESPECIAL");
											error.setRecomendaciones("Error al validar Descripcion Equipo Especial");
											listErrores.add(error);
											mensajeError.append("Campo: DESCRIPCION EQUIPO ESPECIAL -  Error al validar Descripcion Equipo Especial ");
										}
										datoIncisoCotAutos.add(datoIncisoCotAutoEquipoEspecial);
									}
									if(coberturaCotizacion.getCoberturaSeccionDTO().getCoberturaDTO().getNombreComercial().toUpperCase().equals(
											GeneraExcelCargaMasiva.NOMBRE_LIMITE_ADAPTACION_CONVERSION.toUpperCase())){
										datoIncisoCotAutoAdaptacionConversion = cargaMasivaService.obtieneDatoIncisoCotAuto(cotizacion, coberturaCotizacion, incisoCotizacion.getId().getNumeroInciso(), 
												detalle.getDescripcionAdaptacionConversion());
										if(datoIncisoCotAutoAdaptacionConversion == null){
											LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
											error.setNumeroInciso(new BigDecimal(fila));
											error.setNombreSeccion("");
											error.setNombreCampoError("DESCRIPCION ADAPTACION Y CONVERSION");
											error.setRecomendaciones("Error al validar Descripcion Adaptacion y Conversion");
											listErrores.add(error);
											mensajeError.append("Campo: DESCRIPCION ADAPTACION Y CONVERSION -  Error al validar Descripcion Adaptacion y Conversion ");
										}
										datoIncisoCotAutos.add(datoIncisoCotAutoAdaptacionConversion);
									}
								}
								//Datos del COnductor
								if(datosConductorObligatorios){
									incisoCotizacion.getIncisoAutoCot().setNombreConductor(
										detalle.getNombre());
									incisoCotizacion.getIncisoAutoCot().setPaternoConductor(
										detalle.getApellidoPaterno());
									incisoCotizacion.getIncisoAutoCot().setMaternoConductor(
										detalle.getApellidoMaterno());
									incisoCotizacion.getIncisoAutoCot().setNumeroLicencia(
										detalle.getNumeroLicencia());
									incisoCotizacion.getIncisoAutoCot().setFechaNacConductor(
										detalle.getFechaNacimiento());
									incisoCotizacion.getIncisoAutoCot().setOcupacionConductor(
										detalle.getOcupacion());
									
									try{
										IncisoAutoCot incisoAutoCotComp = cargaMasivaService.obtieneIncisoAutoCot(incisoCotizacion);
										incisoAutoCotComp.setNombreConductor(detalle.getNombre());
										incisoAutoCotComp.setPaternoConductor(detalle.getApellidoPaterno());
										incisoAutoCotComp.setMaternoConductor(detalle.getApellidoMaterno());
										incisoAutoCotComp.setNumeroLicencia(detalle.getNumeroLicencia());
										incisoAutoCotComp.setFechaNacConductor(detalle.getFechaNacimiento());
										incisoAutoCotComp.setOcupacionConductor(detalle.getOcupacion());
										IncisoCotizacionDTO incisoComp = new IncisoCotizacionDTO();
										incisoComp.setIncisoAutoCot(incisoAutoCotComp);
										
										cargaMasivaService.guardarComplementarInciso(incisoComp, datoIncisoCotAutos);
									}catch(Exception e){
										e.printStackTrace();
									}
								}else{
									try{
										cargaMasivaService.guardarComplementarInciso(null, datoIncisoCotAutos);
									}catch(Exception e){
									}
								}
								
							}catch(Exception e){
								e.printStackTrace();
								incisoCotizacion = null;
							}
							if (incisoCotizacion == null) {
								LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
								error.setNumeroInciso(new BigDecimal(fila));
								error.setNombreSeccion("");
								error.setNombreCampoError("ERROR AL GUARDAR INCISO");
								error.setRecomendaciones("");
								listErrores.add(error);
								mensajeError.append("Error al guardar Inciso \n");
							}else{
								try{
									ResumenCostosDTO resumen = cargaMasivaService.calculaCotizacion(cotizacion);
									cotizacion.setValorPrimaTotal(new BigDecimal(resumen.getPrimaTotal()));
									
									if(detalle.getPrimaTotal() != null && detalle.getIgualacion() != null && detalle.getIgualacion().equals(GeneraExcelCargaMasiva.VALOR_SI)){
										cargaMasivaService.igualarPrima(cotizacion.getIdToCotizacion(), detalle.getPrimaTotal());
									}else{
										detalle.setPrimaTotal(resumen.getPrimaTotal());
									}
									HSSFCell cellPrima = sheet.getRow((fila - 1)).getCell(CAMPO_PRIMA_TOTAL);
									if(cellPrima == null){
										cellPrima = sheet.getRow((fila - 1)).createCell(CAMPO_PRIMA_TOTAL);
									}
									cellPrima.setCellValue(detalle.getPrimaTotal());
								}catch(Exception e){
									e.printStackTrace();
								}
								
								if(mensajeError.toString().isEmpty()){
									TerminarCotizacionDTO terminarCotizacionDTO = cargaMasivaService.terminaCotizacion(cotizacion);
								if(terminarCotizacionDTO != null){
									if(terminarCotizacionDTO.getEstatus() == TerminarCotizacionDTO.Estatus.ERROR_EXCEPCIONES.getEstatus().shortValue()){
										if(detalle.getSolicitarAutorizacion() != null && detalle.getSolicitarAutorizacion().equals(GeneraExcelCargaMasiva.VALOR_SI)){
											Long idSolicitud = cargaMasivaService.solicitudAutorizacion(terminarCotizacionDTO.getExcepcionesList(), cotizacion.getIdToCotizacion(), idUsuario);
											detalle.setMensajeError("Solicitud Autorizacion:" + idSolicitud);
											detalle.setClaveEstatus(DetalleCargaMasivaIndAutoCot.ESTATUS_AUTORIZACION);
											requiereAutorizacion = true;
											HSSFCell cellStat = sheet.getRow((fila - 1)).getCell(CAMPO_ESTATUS);
											if(cellStat == null){
												cellStat = sheet.getRow((fila - 1)).createCell(CAMPO_ESTATUS);
											}
											cellStat.setCellValue("SOLICITO AUTORIZACION");
										}else{
											LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
											error.setNumeroInciso(new BigDecimal(fila));
											error.setNombreSeccion("");
											error.setNombreCampoError(terminarCotizacionDTO.getMensajeError());
											error.setRecomendaciones("");
											listErrores.add(error);
											mensajeError.append(terminarCotizacionDTO.getMensajeError());
										}
									}else{
										LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
										error.setNumeroInciso(new BigDecimal(fila));
										error.setNombreSeccion("");
										error.setNombreCampoError(terminarCotizacionDTO.getMensajeError());
										error.setRecomendaciones("");
										listErrores.add(error);
										mensajeError.append(terminarCotizacionDTO.getMensajeError());
									}
								}
								}
							}
				    	}
					}
					
				}////////INCISO
				
				if(!requiereAutorizacion){
					detalle.setMensajeError(mensajeError.toString());
					if (mensajeError.toString().isEmpty()) {
						detalle.setClaveEstatus(DetalleCargaMasivaIndAutoCot.ESTATUS_TERMINADO);
						HSSFCell cellStat = sheet.getRow((fila - 1)).getCell(CAMPO_ESTATUS);
						if(cellStat == null){
							cellStat = sheet.getRow((fila - 1)).createCell(CAMPO_ESTATUS);
						}
						cellStat.setCellValue("TERMINADO");
					} else {
						detalle.setClaveEstatus(DetalleCargaMasivaIndAutoCot.ESTATUS_ERROR);
						HSSFCell cellStat = sheet.getRow((fila - 1)).getCell(CAMPO_ESTATUS);
						if(cellStat == null){
							cellStat = sheet.getRow((fila - 1)).createCell(CAMPO_ESTATUS);
						}
						cellStat.setCellValue("ERROR");
						HSSFCell cell2 = sheet.getRow((fila - 1)).getCell(CAMPO_NUMERO_COTIZACION);
						if(cell2 == null){
							cell2 = sheet.getRow((fila - 1)).createCell(CAMPO_NUMERO_COTIZACION);
						}
						cell2.setCellValue("");
						
						//Elimina Cotizacion
						if(cotizacion != null && cotizacion.getIdToCotizacion() != null){
							cargaMasivaService.eliminaCotizacionError(cotizacion);
							detalle.setIdToCotizacion(BigDecimal.ZERO);
						}
						
					}
				}
			}//ERROR
			}catch(Exception e){
				e.printStackTrace();
				detalle.setClaveEstatus(DetalleCargaMasivaIndAutoCot.ESTATUS_ERROR);
				HSSFCell cellStat = sheet.getRow((fila - 1)).getCell(CAMPO_ESTATUS);
				if(cellStat == null){
					cellStat = sheet.getRow((fila - 1)).createCell(CAMPO_ESTATUS);
				}
				cellStat.setCellValue("ERROR");
				HSSFCell cell2 = sheet.getRow((fila - 1)).getCell(CAMPO_NUMERO_COTIZACION);
				if(cell2 == null){
					cell2 = sheet.getRow((fila - 1)).createCell(CAMPO_NUMERO_COTIZACION);
				}
				cell2.setCellValue("");
			}
		}//FOR
	}
	
	public void validaEmisionAutoExpedibles(List<AutoExpediblesDetalleAutoCot> detalleCargaMasivaList, HSSFSheet sheet){
		StringBuilder mensajeError = new StringBuilder("");
		int fila = 1;
		for (AutoExpediblesDetalleAutoCot detalle : detalleCargaMasivaList) {

			fila++;
			mensajeError.delete(0,mensajeError.length());
			if (detalle.getClaveEstatus() != null && !detalle.getClaveEstatus().equals(
					DetalleCargaMasivaAutoCot.ESTATUS_ERROR)) {
				
				CotizacionDTO cotizacion = cargaMasivaService.getCotizacionByDetalleCargaMasivaIndAutoCot(detalle.getNumeroCotizacion());
				if(cotizacion != null && cotizacion.getClaveEstatus() != null && 
						cotizacion.getClaveEstatus().equals(CotizacionDTO.ESTATUS_COT_TERMINADA)){
					
					detalle.setNumeroCotizacion(cotizacion.getIdToCotizacion());
					detalle.setIdToCotizacion(cotizacion.getIdToCotizacion());
					
					
				IncisoCotizacionDTO incisoCotizacion = cargaMasivaService
						.obtieneIncisoCotizacion(cotizacion, new BigDecimal(1));

				if (incisoCotizacion == null) {
					LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
					error.setNumeroInciso(new BigDecimal(fila));
					error.setNombreSeccion("");
					error.setNombreCampoError("NUMERO DE INCISO");
					error.setRecomendaciones("No existe inciso");
					listErrores.add(error);
					mensajeError.append("Campo: ").append(error.getNombreCampoError()).append(" - ").append(error.getRecomendaciones()).append(" \n");
				} else {

					// Asegurado
					if (detalle.getNombreORazonSocial() != null) {
						incisoCotizacion.getIncisoAutoCot().setNombreAsegurado(
								detalle.getNombreCompleto().toUpperCase());
					} else {
						if(cotizacion.getIdToPersonaContratante() != null && cotizacion.getNombreContratante() != null){
							incisoCotizacion.getIncisoAutoCot().setNombreAsegurado(
									cotizacion.getNombreContratante().toUpperCase());
							incisoCotizacion.getIncisoAutoCot().setPersonaAseguradoId(
									cotizacion.getIdToPersonaContratante().longValue());
						}
					}

					//Datos del COnductor
					Boolean datosConductorObligatorios = false;
					try{
						datosConductorObligatorios = cargaMasivaService.datosDelConductorRequeridos(incisoCotizacion);
					}catch(Exception e){
						
					}
					if(datosConductorObligatorios){
						incisoCotizacion.getIncisoAutoCot().setNombreConductor(
							detalle.getNombre());
						incisoCotizacion.getIncisoAutoCot().setPaternoConductor(
							detalle.getApellidoPaterno());
						incisoCotizacion.getIncisoAutoCot().setMaternoConductor(
							detalle.getApellidoMaterno());
						incisoCotizacion.getIncisoAutoCot().setNumeroLicencia(
							detalle.getNumeroLicencia());
						incisoCotizacion.getIncisoAutoCot().setFechaNacConductor(
							detalle.getFechaNacimiento());
						incisoCotizacion.getIncisoAutoCot().setOcupacionConductor(
							detalle.getOcupacion());
					}
					//Datos Vehiculo
					incisoCotizacion.getIncisoAutoCot().setNumeroMotor(
							detalle.getNumeroMotor());
					String numeroSerieRempl = remplazarCaracteresNumSerie(detalle.getNumeroSerie());
					incisoCotizacion.getIncisoAutoCot().setNumeroSerie(numeroSerieRempl);
					
					List<String> errors = NumSerieValidador.getInstance()
							.validate(
									incisoCotizacion.getIncisoAutoCot()
											.getNumeroSerie());
					if (!errors.isEmpty()) {
						LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
						error.setNumeroInciso(new BigDecimal(fila));
						error.setNombreSeccion("");
						error.setNombreCampoError("NUMERO DE SERIE");
						error.setRecomendaciones("Error al validar el n\u00FAmero de serie:");
						listErrores.add(error);
						mensajeError.append("Campo: NUMERO DE SERIE -  ").append(errors.get(0)).append("");
					}
					incisoCotizacion.getIncisoAutoCot().setPlaca(
							detalle.getPlacas());
					incisoCotizacion.getIncisoAutoCot().setRepuve(
							detalle.getNciRepuve());
					
					
					//Valida datos de Riesgo
					List<CoberturaCotizacionDTO> coberturas = cargaMasivaService.obtieneCoberturasContratadas(incisoCotizacion);
					List<DatoIncisoCotAuto> datoIncisoCotAutos = new ArrayList<DatoIncisoCotAuto>();
					DatoIncisoCotAuto datoIncisoCotAutoEquipoEspecial = null;
					DatoIncisoCotAuto datoIncisoCotAutoAdaptacionConversion = null;
					for(CoberturaCotizacionDTO coberturaCotizacion : coberturas){
						if(coberturaCotizacion.getCoberturaSeccionDTO().getCoberturaDTO().getNombreComercial().toUpperCase().equals(
								GeneraExcelCargaMasiva.NOMBRE_LIMITE_EQUIPO_ESPECIAL.toUpperCase())){
							datoIncisoCotAutoEquipoEspecial = cargaMasivaService.obtieneDatoIncisoCotAuto(cotizacion, coberturaCotizacion, incisoCotizacion.getId().getNumeroInciso(), 
									detalle.getDescripcionEquipoEspecial());
							if(datoIncisoCotAutoEquipoEspecial == null){
								LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
								error.setNumeroInciso(new BigDecimal(fila));
								error.setNombreSeccion("");
								error.setNombreCampoError("DESCRIPCION EQUIPO ESPECIAL");
								error.setRecomendaciones("Error al validar Descripcion Equipo Especial");
								listErrores.add(error);
								mensajeError.append("Campo: DESCRIPCION EQUIPO ESPECIAL -  Error al validar Descripcion Equipo Especial ");
							}
							datoIncisoCotAutos.add(datoIncisoCotAutoEquipoEspecial);
						}
						if(coberturaCotizacion.getCoberturaSeccionDTO().getCoberturaDTO().getNombreComercial().toUpperCase().equals(
								GeneraExcelCargaMasiva.NOMBRE_LIMITE_ADAPTACION_CONVERSION.toUpperCase())){
							datoIncisoCotAutoAdaptacionConversion = cargaMasivaService.obtieneDatoIncisoCotAuto(cotizacion, coberturaCotizacion, incisoCotizacion.getId().getNumeroInciso(), 
									detalle.getDescripcionAdaptacionConversion());
							if(datoIncisoCotAutoAdaptacionConversion == null){
								LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
								error.setNumeroInciso(new BigDecimal(fila));
								error.setNombreSeccion("");
								error.setNombreCampoError("DESCRIPCION ADAPTACION Y CONVERSION");
								error.setRecomendaciones("Error al validar Descripcion Adaptacion y Conversion");
								listErrores.add(error);
								mensajeError.append("Campo: DESCRIPCION ADAPTACION Y CONVERSION -  Error al validar Descripcion Adaptacion y Conversion ");
							}
							datoIncisoCotAutos.add(datoIncisoCotAutoAdaptacionConversion);
						}
					}
					
					if (mensajeError.toString().isEmpty()) {
						cargaMasivaService.guardarComplementarInciso(incisoCotizacion, datoIncisoCotAutos);
					
					

					Map<String, String> mensajeEmision = cargaMasivaService.emitirCotizacion(cotizacion);
					String icono = mensajeEmision.get("icono");
					String mensaje = mensajeEmision.get("mensaje");
					if(icono.equals("30")){
						String idPoliza = mensajeEmision.get("idpoliza");
						PolizaDTO poliza = cargaMasivaService.getPolizaByStringId(idPoliza);
						
						
						
						if(cotizacion.getIdToPersonaContratante()!=null){
							
							clienteRest.validatePeps(cotizacion.getIdToPersonaContratante().longValue(),poliza.getNumeroPolizaFormateada());
								
						}
						if(cotizacion.getIdToPersonaAsegurado()!=null){
							
							clienteRest.validatePeps(cotizacion.getIdToPersonaAsegurado().longValue(),poliza.getNumeroPolizaFormateada());
								
						}
						HSSFCell cellStat = sheet.getRow((fila - 1)).getCell(CAMPO_NUMERO_POLIZA);
						if(cellStat == null){
							cellStat = sheet.getRow((fila - 1)).createCell(CAMPO_NUMERO_POLIZA);
						}
						cellStat.setCellValue(poliza.getNumeroPolizaFormateada());
						detalle.setMensajeError("Numero de Poliza: " + poliza.getNumeroPolizaFormateada());
						detalle.setNumeroPoliza(poliza.getNumeroPolizaFormateada());
						detalle.setIdToPoliza(poliza.getIdToPoliza());
						HSSFCell cellFechaEmision = sheet.getRow((fila - 1)).getCell(CAMPO_FECHA_EMISION);
						if(cellFechaEmision == null){
							cellFechaEmision = sheet.getRow((fila - 1)).createCell(CAMPO_FECHA_EMISION);
						}
						cellFechaEmision.setCellValue(poliza.getFechaCreacion());
					}else if(icono.equals("10")){
						LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
						error.setNumeroInciso(new BigDecimal(fila));
						error.setNombreSeccion("");
						error.setNombreCampoError("ERROR AL EMITIR COTIZACION");
						if(mensaje != null){
							mensaje = mensaje.replaceAll("\"", "");
						}
						error.setRecomendaciones(mensaje);
						listErrores.add(error);
						mensajeError.append("Campo: ").append(error.getNombreCampoError()).append(" - ").append(error.getRecomendaciones()).append(" \n");
					}
					}//NO VALIDO
					}//NO INCISO
				}else{
					LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
					error.setNumeroInciso(new BigDecimal(fila));
					error.setNombreSeccion("");
					error.setNombreCampoError("NUMERO DE COTIZACION");
					error.setRecomendaciones("Cotizacion no Terminada");
					listErrores.add(error);
					mensajeError.append("Campo: ").append(error.getNombreCampoError()).append(" - ").append(error.getRecomendaciones()).append(" \n");
				}
				
				
				if (mensajeError.toString().isEmpty()) {
					detalle.setClaveEstatus(DetalleCargaMasivaIndAutoCot.ESTATUS_EMITIDO);
					HSSFCell cellStat = sheet.getRow((fila - 1)).getCell(CAMPO_ESTATUS);
					if(cellStat == null){
						cellStat = sheet.getRow((fila - 1)).createCell(CAMPO_ESTATUS);
					}
					cellStat.setCellValue("POLIZA EMITIDA");
					
				} else {
					detalle.setMensajeError(mensajeError.toString());
					detalle.setClaveEstatus(DetalleCargaMasivaIndAutoCot.ESTATUS_ERROR);
					HSSFCell cellStat = sheet.getRow((fila - 1)).getCell(CAMPO_ESTATUS);
					if(cellStat == null){
						cellStat = sheet.getRow((fila - 1)).createCell(CAMPO_ESTATUS);
					}
					cellStat.setCellValue("ERROR");
				}
			}
		}
			
	}
	
	public void procesaAutoExpediblesPendientes(){
		//Obtiene cargas pendientes
		List<AutoExpediblesAutoCot> cargasPendientes = cargaMasivaService.obtieneAutoExpediblesPendientes();
		for(AutoExpediblesAutoCot carga: cargasPendientes){
			try {
				this.setDetalleList(new ArrayList<AutoExpediblesDetalleAutoCot>(
						1));
				setHasLogErrors(false);
				if (carga.getIdToNegTipoPoliza() != null) {
					negocioTipoPoliza = cargaMasivaService.getNegocioTipoPolizaByCarga(carga.getIdToNegTipoPoliza());
				}
				setNegocioTipoPoliza(negocioTipoPoliza);
				Usuario usuario = cargaMasivaService.getUsuarioByCarga(carga.getCodigoUsuarioCreacion());
				cargaMasivaService.setUsuarioActual(usuario);
				setIdUsuario(usuario.getId().longValue());
				validaAutoExpedibles(carga.getIdToControlArchivo(), GeneraExcelCargaMasiva.TIPO_CARGA_NORMAL, carga.getClaveTipo());
			} catch (Exception e) {
				setHasLogErrors(true);
			}
			if(!isHasLogErrors()){
				carga.setClaveEstatus(CargaMasivaIndividualAutoCot.ESTATUS_TERMINADO);
			}else{
				carga.setClaveEstatus(CargaMasivaIndividualAutoCot.ESTATUS_CON_ERROR);
			}
			BigDecimal idAutoExpedibles = cargaMasivaService.guardaAutoExpediblesAutoCot(carga);
			carga.setIdToAutoExpediblesAutoCot(idAutoExpedibles);			
			cargaMasivaService.guardaDetalleAutoExpedibles(carga, getDetalleList());
		}
	}
	
	private String remplazaEspacios(String valor) {
		return valor.replaceAll(" ", "_");
	}

	private String remplazaGuion(String valor) {
		return valor.replaceAll("_", " ");
	}
	
	private String obtenerNombreArchivo(ControlArchivoDTO controlArchivoDTO) {
		String fileName = controlArchivoDTO.getNombreArchivoOriginal();
		String extension = (fileName.lastIndexOf(".") == -1) ? "" : fileName
				.substring(fileName.lastIndexOf("."), fileName.length());
		return controlArchivoDTO.getIdToControlArchivo().toString() + extension;
	}
	
	public HSSFWorkbook cargaArchivodeCarga(String archivo) throws IOException {
		HSSFWorkbook workbook = null;
		File file = new File(archivo);
		POIFSFileSystem plantilla = new POIFSFileSystem(new FileInputStream(
				file));
		workbook = new HSSFWorkbook(plantilla);
		return workbook;
	}

	private void creaLogErrores() {
		if (!this.getListErrores().isEmpty()) {
			File file = new File(uploadFolder + "Log"
					+ controlArchivoDTO.getIdToControlArchivo() + ".txt");
			try {
				FileWriter outFile = new FileWriter(file);
				//PrintWriter out = new PrintWriter(outFile);

				BufferedWriter out = new BufferedWriter(outFile);
				//out.println("N\u00FAmero de L\u00EDnea |  Nombre de la secci\u00F3n |   Nombre de campo que no cumpli\u00F3 con la validaci\u00F3n");
				out.write("N\u00FAmero de L\u00EDnea |  Nombre de la secci\u00F3n |   Nombre de campo que no cumpli\u00F3 con la validaci\u00F3n \r\n" + System.getProperty("line.separator"));
				out.newLine();
				for (LogErroresCargaMasivaDTO error : this.getListErrores()) {
					/*
					 * out.println(error.getNumeroInciso() + " | " +
					 * error.getNombreSeccion() + " | " +
					 * error.getNombreCampoError() + "\r\n");
					 */
					out.write(" " + error.getNumeroInciso() + " | "
							+ error.getNombreSeccion() + " | "
							+ error.getNombreCampoError() + "\r\n"
							+ System.getProperty("line.separator"));
					out.newLine();
				}
				out.flush();
				out.close();
				outFile.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	
	public boolean validaDeducible(List<NegocioDeducibleCob> deducibles, Double valor){
		boolean esValido = false;
		if(deducibles != null){
			for(NegocioDeducibleCob deducible : deducibles){
				if(deducible.getValorDeducible().equals(valor)){
					esValido = true;
					break;
				}
			}
		}
		return esValido;
	}
	
	public boolean validaRangos(Double valorMinimo, Double valorMaximo, Double valor){
		boolean esValido = true;
		if(valor.doubleValue() < valorMinimo.doubleValue() || valor.doubleValue() > valorMaximo.doubleValue()){
			esValido = false;
		}
		return esValido;
	}
	
	public String remplazarCaracteresNumSerie(String numeroSerie){
		String nuevoNumeroSerie = numeroSerie.toUpperCase();
		
		nuevoNumeroSerie = nuevoNumeroSerie.replaceAll("\u00D1", "N");
		nuevoNumeroSerie = nuevoNumeroSerie.replaceAll("O", "0");
		nuevoNumeroSerie = nuevoNumeroSerie.replaceAll("Q", "0");
		nuevoNumeroSerie = nuevoNumeroSerie.replaceAll("I", "1");
		nuevoNumeroSerie = nuevoNumeroSerie.replaceAll(" ", "");
		
		return nuevoNumeroSerie;
	}
	
	
	public void setListErrores(List<LogErroresCargaMasivaDTO> listErrores) {
		this.listErrores = listErrores;
	}
	public List<LogErroresCargaMasivaDTO> getListErrores() {
		return listErrores;
	}
	public void setControlArchivoDTO(ControlArchivoDTO controlArchivoDTO) {
		this.controlArchivoDTO = controlArchivoDTO;
	}
	public ControlArchivoDTO getControlArchivoDTO() {
		return controlArchivoDTO;
	}
	public void setCargaMasivaService(
			CargaMasivaService cargaMasivaService) {
		this.cargaMasivaService = cargaMasivaService;
	}
	public CargaMasivaService getCargaMasivaService() {
		return cargaMasivaService;
	}
	public void setLogErrores(FileInputStream logErrores) {
		this.logErrores = logErrores;
	}
	public FileInputStream getLogErrores() {
		return logErrores;
	}
	public void setHasLogErrors(boolean hasLogErrors) {
		this.hasLogErrors = hasLogErrors;
	}
	public boolean isHasLogErrors() {
		return hasLogErrors;
	}
	public void setTipoCarga(Short tipoCarga) {
		this.tipoCarga = tipoCarga;
	}
	public Short getTipoCarga() {
		return tipoCarga;
	}



	public void setNegocioTipoPoliza(NegocioTipoPoliza negocioTipoPoliza) {
		this.negocioTipoPoliza = negocioTipoPoliza;
	}



	public NegocioTipoPoliza getNegocioTipoPoliza() {
		return negocioTipoPoliza;
	}



	public void setDetalleList(List<AutoExpediblesDetalleAutoCot> detalleList) {
		this.detalleList = detalleList;
	}



	public List<AutoExpediblesDetalleAutoCot> getDetalleList() {
		return detalleList;
	}



	public void setIdUsuario(Long idUsuario) {
		this.idUsuario = idUsuario;
	}



	public Long getIdUsuario() {
		return idUsuario;
	}



	public void setClaveTipo(Short claveTipo) {
		this.claveTipo = claveTipo;
	}



	public Short getClaveTipo() {
		return claveTipo;
	}



	public void setEjemplo(AutoExpediblesDetalleAutoCot ejemplo) {
		this.ejemplo = ejemplo;
	}



	public AutoExpediblesDetalleAutoCot getEjemplo() {
		return ejemplo;
	}


}
