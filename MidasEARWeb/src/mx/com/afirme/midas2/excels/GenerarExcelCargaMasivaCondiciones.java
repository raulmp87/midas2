package mx.com.afirme.midas2.excels;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.controlArchivo.ControlArchivoDN;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.BitemporalInciso;
import mx.com.afirme.midas2.domain.condicionesespeciales.CondicionEspecial;
import mx.com.afirme.midas2.domain.condicionesespeciales.CondicionEspecial.NivelAplicacion;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.cargamasivacotizacion.CargaMasivaCondicionesEsp;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.cargamasivacotizacion.CargaMasivaCondicionesEspDet;
import mx.com.afirme.midas2.dto.condicionespecial.CondicionEspecialBitemporalDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.cargamasiva.LogErroresCargaMasivaDTO;
import mx.com.afirme.midas2.service.bitemporal.endoso.condicionesEspeciales.CondicionEspecialBitemporalService;
import mx.com.afirme.midas2.service.bitemporal.poliza.inciso.IncisoViewService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.cargaMasiva.CargaMasivaService;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("prototype")
public class GenerarExcelCargaMasivaCondiciones extends GenerarExcelBase {
	
	private static final String PLANTILLA = "/mx/com/afirme/midas2/impresiones/jrxml/plantilla.xls";
	private static final short  POLIZA = 0;
	private static final short  INCISO = 1;
	private static final short  CONDICIONES_ESPECIALES = 2;
	private int                 maxColumns = 3;
	private String              numeroPolizaFormateado;
	private boolean             hasLogErrors = false;
	private long                idToNegocio;
	private short               accionEndoso;             // # 0-ALTA   | 1-BAJA   | 2-TODAS
	private short               nivelApp;                 // # 0-POLIZA | 1-INCISO | 2-TODAS 
	private Long                idCotizacionContinuity;
	private IncisoViewService   incisoViewService;
	private Date                validoEn;
	private Long                idCargaMasivaCondiciones; // # ID GENERADO DE SALVAR EN CargaMasivaCondicionesEsp.CLASS
	private long                idToCotizacion;
	private String              listaIncisos = "0";       // # CONCATENA LOS INCISOS MODIFICADOS
	private CondicionEspecialBitemporalService condicionEspecialBitemporalService;
	
	
	
	public GenerarExcelCargaMasivaCondiciones(CargaMasivaService cargaMasivaService) {
		this.subirDocumento(cargaMasivaService);
	}
	
	public GenerarExcelCargaMasivaCondiciones(CargaMasivaService cargaMasivaService, short xAccionEndoso, short xNivelAplicacion, Long xIdCotizacionContinuity, String xNumPolizaFormateado, long xIdToNegocio) {
		this.accionEndoso           = xAccionEndoso;
		this.nivelApp               = xNivelAplicacion;
		this.idCotizacionContinuity = xIdCotizacionContinuity;
		this.numeroPolizaFormateado = xNumPolizaFormateado;
		this.idToNegocio            = xIdToNegocio;
		this.subirDocumento(cargaMasivaService);
	}	
	
	
	private void subirDocumento (CargaMasivaService cargaMasivaService){
		this.setCargaMasivaService(cargaMasivaService);

		String OSName = System.getProperty("os.name");
		if (OSName.toLowerCase().indexOf("windows")!=-1)
			this.uploadFolder = Sistema.UPLOAD_FOLDER;
		else
			this.uploadFolder = Sistema.LINUX_UPLOAD_FOLDER;
		
	}
	
	
	public InputStream generaPlantillaCargaMasiva()throws IOException {
			HSSFWorkbook workbook = null;
			HSSFSheet validaciones = null,condicionesEspeciales = null;;
			ByteArrayOutputStream bos = null;
			
			try {
				// Obtiene plantilla con macros
				workbook = this.cargaPlantilla(PLANTILLA);
				validaciones = workbook.getSheetAt(0);
			
				// Escribe datos cabecera en el excel
				buildValidationsSheet(validaciones);
				
				//Escribe los incisos de la poliza en el excel
				this.escribeIncisosPoliza(validaciones);
			
				// Ajusta tamano de columnas
				for (int i = 0; i <= maxColumns; i++) {
					validaciones.setColumnWidth(i, 9000);
				}
				
				// Crea pestana de condiciones especiales
				workbook.createSheet("Condiciones Especiales");
				condicionesEspeciales = workbook.getSheetAt(2);
				
				// Valida Si las condiciones son del tipo poliza o inciso
				this.validarCondicionesMostrar(condicionesEspeciales,this.getIdToNegocio());
			
				// Oculta hoja de datos. Esta hoja tiene validaciones en caso de usar combos.
				workbook.setSheetHidden(1, true);
				// Muestra primera hoja
				workbook.setActiveSheet(0);
				short firstCell = 0;
				validaciones.showInPane(firstCell, firstCell);
				
				bos = new ByteArrayOutputStream();
			
				workbook.write(bos);
			
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				if (bos != null) {
					bos.close();
				}
			}
			
			//File file = new File(uploadFolder + "CargaMasivaCot"+ cotizacion.getIdToCotizacion() + ".xls");
			if(bos == null){
				return null;
			}
			return new ByteArrayInputStream(bos.toByteArray());
}	

	// Valida el tipo de condiciones a mostrar en el excel -todos, inciso, poliza
	private void validarCondicionesMostrar(HSSFSheet condicionesEspeciales, long idToNegocio){
		
		switch( this.getNivelAplicacion() ){
			case 0:
				this.createCondicionesSheet(condicionesEspeciales, new BigDecimal( idToNegocio ) , NivelAplicacion.POLIZA );
			break;
			case 1:
				this.createCondicionesSheet(condicionesEspeciales, new BigDecimal( idToNegocio ) , NivelAplicacion.INCISO );
			break;
			case 2:
				this.createCondicionesSheet(condicionesEspeciales, new BigDecimal( idToNegocio ) , NivelAplicacion.TODAS );
			break;
		}
		
	}
	

	
	
	public HSSFWorkbook cargaPlantillaEndozos() throws IOException {
		return this.cargaPlantilla(PLANTILLA);
	}


	@Override
	protected void buildValidationsSheet(HSSFSheet sheet) {
		HSSFRow row = sheet.createRow(0);
		HSSFCell cell = row.createCell(POLIZA);
		cell.setCellValue("POLIZA");
		cell = row.createCell(INCISO);
		cell.setCellValue("INCISO");
		
		cell = row.createCell(CONDICIONES_ESPECIALES);
		cell.setCellValue( this.getAccionEndoso()==0 ? "CONDICIONES ESPECIALES ALTA":"CONDICIONES ESPECIALES BAJA" );

		llenaLineaEjemploComp(sheet);

		this.escribeIncisosPoliza(sheet); // # ESCRIBE INCISOS DE LA POLIZA DENTRO DEL EXCEL
		
	}
	
	@SuppressWarnings("deprecation")
	private void llenaLineaEjemploComp(HSSFSheet sheet){
		
		HSSFRow row = sheet.createRow(1);
		HSSFCell cell = null; 
		
		cell = row.createCell(POLIZA,Cell.CELL_TYPE_STRING);
		cell.setCellValue(this.getNumeroPolizaFormateado());
		cell = row.createCell(INCISO, Cell.CELL_TYPE_STRING);
		cell.setCellValue("");
		cell = row.createCell(CONDICIONES_ESPECIALES, Cell.CELL_TYPE_STRING);
		cell.setCellValue("");
	}
	
	@SuppressWarnings("static-access")
	private void escribeIncisosPoliza(HSSFSheet sheet){
		
		HSSFRow  row           = null;
		HSSFCell cell          = null;
		StringBuilder sIncisos = new StringBuilder();
		
		// # SE INCIA EN 1 PARA COMENZAR A ESCRIBIR DESDE LA 2DA LINEA
		int fila = 1;
			
		// # OBTIENE LAS INCISOS VINCULADOS A LA POLIZA
		Collection<BitemporalInciso> incisos = incisoViewService.getLstIncisoByCotizacion( this.getIdCotizacionContinuity() ,this.getValidoEn());
			
		for(BitemporalInciso inc:incisos){
				
			row = sheet.createRow(fila);
				
			// # ESCRIBE EL NUMERO FORMATEADO (00-0000-00) DE LA POLIZA A PROCESAR
			sheet.autoSizeColumn(0);
			cell = row.createCell(0,Cell.CELL_TYPE_STRING);
			cell.setCellValue( this.getNumeroPolizaFormateado() );
				
			// # ESCRIBE EL NUMERO DEL INCISO SOBRE EL EXCEL
			sheet.autoSizeColumn(1);
			cell = row.createCell(1,Cell.CELL_TYPE_STRING);
			cell.setCellValue( inc.getContinuity().getNumero() );
			
			// # CONCATENA INCISOS
			sIncisos.append( inc.getContinuity().getNumero() );
			fila++;
		}
		
		this.listaIncisos = sIncisos.toString();
		
	}	

	
	public void validaCargaMasiva(BigDecimal idControlArchivo, Short tipoCarga) {

		try {
			this.tipoCarga = tipoCarga;

			ControlArchivoDN controlArchivoDN = ControlArchivoDN.getInstancia();
			controlArchivoDTO = controlArchivoDN.getPorId(idControlArchivo);
			String fileName = this.obtenerNombreArchivo(controlArchivoDTO);

			HSSFWorkbook workbook = this.cargaArchivodeCarga(uploadFolder
					+ fileName);
			HSSFSheet validaciones = workbook.getSheetAt(0);
			
			leeLineasArchivoCarga(validaciones);

		} catch (IOException e) {
			e.printStackTrace();
		} catch (SystemException e) {
			e.printStackTrace();
		}
	}
	
	@Override
	protected void leeLineasArchivoCarga(HSSFSheet sheet) {
		
		int fila                 = 0;
		boolean isFirst          = true;
		StringBuilder mensajeError = new StringBuilder("");
		long incisoIdContinuity  = 0; 
		long inciso              = 0;
		
		Map<Long,List<CondicionEspecial> > datosIncisos = new HashMap<Long,List<CondicionEspecial> >(); 
		List<Long> lIcisos = new ArrayList<Long>();
		
		Iterator<Row> rowIterator = sheet.rowIterator();
		while (rowIterator.hasNext()) {
			
			HSSFRow hssfRow = (HSSFRow) rowIterator.next();
			fila = hssfRow.getRowNum() + 1;
			
			if (!isFirst) {
				
				for (int i = 1; i <= maxColumns; i++) {
					
					HSSFCell cell = hssfRow.getCell(i);
					//LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
					switch (i) {
						case POLIZA:
							cell.setCellType(Cell.CELL_TYPE_STRING);
						break;
						case INCISO:
							cell.setCellType(Cell.CELL_TYPE_STRING);
							
							if(!this.isInt(cell.getStringCellValue())){
								listErrores.add(
										this.crearError(new BigDecimal(fila), cell.getStringCellValue(), "Revise las condiciones ingresadas sean solo datos numéricos: ", "")
								);
								mensajeError.append("Campo: INCISO - los datos ingresados: ").append(cell.getStringCellValue()).append(" NO son validos \r\n").append(System.getProperty("line.separator"));
							}else{
								// # RECUPERA ID CONTINUITY DEL INCISO
								incisoIdContinuity = this.getIdIncisoContinuity( Long.valueOf( cell.getStringCellValue() ) );
								// # ES EL ID INCISO QUE VIENE EN EL EXCEL
								inciso = Long.valueOf( cell.getStringCellValue() );
							}
						break;
						case CONDICIONES_ESPECIALES:
							// # VALIDACION ALTA
							if(this.getAccionEndoso() == 0){
								
								if (cell != null && !cell.toString().isEmpty()) {
									
									cell.setCellType(Cell.CELL_TYPE_STRING);
									String[] aAlta = cell.getStringCellValue().split(",");
									
									if(aAlta.length > 0){
										for(int sa=0; sa<=aAlta.length-1; sa++){
											
											// # VALIDAR SOLO NUMEROS ENTEROS
											if(!this.isInt( aAlta[sa] )){
												listErrores.add(
														this.crearError(new BigDecimal(fila), cell.getStringCellValue(), "Solo se permiten datos numericos: ", "")
												);
												mensajeError.append("Campo: CONDICIONES ESPECIALES - los datos ingresados: ").append(cell.getStringCellValue()).append(" NO son validos \r\n").append(System.getProperty("line.separator"));
											}
										}
										if(mensajeError.toString().isEmpty()){
											
											// # VALIDAR QUE CONDICIONES ESPECIALES SEAN PARTE DEL NEGOCIO
											mensajeError.append(this.validarCondicionesAlta(this.getIdToNegocio(), aAlta,new BigDecimal(fila) ));
											
											// # VALIDAR QUE LA CONDICION NO SE ENCUENTRE YA AGREGADA
											mensajeError.append(this.validaCondicionVinculadaAlta(aAlta, incisoIdContinuity, fila, inciso));
											
											// # SI NO HAY ERRORES SE AGREGA AL MAP UN List<CondicionEspecial>
											if(mensajeError.toString().isEmpty()){
												datosIncisos.put(incisoIdContinuity, this.convertirCondicionesEspeciales(aAlta, new BigDecimal( fila ), "CONDICIONES ESPECIALES ALTA"));
												lIcisos.add(inciso);
											}
											
										}
									}
								}
							
							// # VALIDACION BAJA
							}else if (this.getAccionEndoso() == 1){
								
								if (cell != null && !cell.toString().isEmpty()) {
									
									cell.setCellType(Cell.CELL_TYPE_STRING);
									String[] aBaja = cell.getStringCellValue().split(",");
									
									if(aBaja.length > 0){
										for(int sb=0; sb<=aBaja.length-1; sb++){
											
											// # VALIDAR SOLO NUMEROS ENTEROS
											if(!this.isInt( aBaja[sb] )){
												listErrores.add(
														this.crearError(new BigDecimal(fila), cell.getStringCellValue(), "Solo se permiten datos numericos: ", "")
												);
												mensajeError.append("Campo: CONDICIONES ESPECIALES ADICIONALES - los datos ingresados: ").append(cell.getStringCellValue()).append(" NO son validos \r\n").append(System.getProperty("line.separator"));
											}
										}
										
										if(mensajeError.toString().isEmpty()){

											// # VALIDAR QUE CONDICIONES ESPECIALES SEAN PARTE DEL NEGOCIO
											mensajeError.append(this.validarCondicionesBaja(this.getIdToNegocio(), aBaja,new BigDecimal(fila) ));
										
											if(mensajeError.toString().isEmpty()){
												datosIncisos.put(incisoIdContinuity, this.convertirCondicionesEspeciales(aBaja, new BigDecimal( fila ), "CONDICIONES ESPECIALES BAJA"));
												lIcisos.add(inciso);
											}
										
										}
									}
								}
								
							}
						break;
						
					}// # CIERRA SWITCH
					
				}// # CIERRA FOR
			
			}else{// # CIERRA IF
				isFirst = false;
			}
		
		}// # CIERRA WHILE
		
		if (mensajeError.toString().isEmpty()) {
			this.validaArchivoCargaIncisos(datosIncisos,lIcisos);
		} else {
			this.setHasLogErrors(true);
			this.creaLogErrores();
		}
		
	}

	
	/***
	 * Valida si la condicion ya fue vinculada al inciso
	 * @param condiciones
	 * @return
	 */
	private String validaCondicionVinculadaAlta(String[] condiciones, Long idIncisoContinuity,int fila, Long inciso){
	
		StringBuilder mensajeError = new StringBuilder("");
		List<CondicionEspecialBitemporalDTO> lCondBitemporalDTO = null;
			
		if(this.getNivelAplicacion() == 1 )	{
		
			// # OBTIENE TODAS LAS CONDICIONES ASOCIADAS AL INCISO
			lCondBitemporalDTO = 
				this.condicionEspecialBitemporalService.
					obtenerCondicionesIncisoAsociadas(
							idIncisoContinuity, 
							this.getValidoEn() 
				);
		
		}else{
			
			lCondBitemporalDTO = 
				this.condicionEspecialBitemporalService.
					obtenerCondicionesAsociadasCotizacion(
							this.getIdCotizacionContinuity(),
							this.getValidoEn()
				);
		}
		
		if(condiciones!= null && (condiciones.length > 0 )){
			
			for(int i = 0; i<=condiciones.length -1; i++){
				
				for( CondicionEspecialBitemporalDTO cbd:lCondBitemporalDTO){
					
					if( cbd.getCondicionEspecial().getCodigo() == Long.valueOf(condiciones[i])  ){
						mensajeError.append("La condicion especial: ").append(condiciones[i]).append(" ya fue agregada al inciso anteriormente. \r\n").append(System.getProperty("line.separator"));
						listErrores.add(
								this.crearError(new BigDecimal(fila),"","La condicion especial: "+condiciones[i]+" ya se encuentra ligada en la base de datos al inciso:"+inciso, "")
						);
					}
					
				}
			}
			
		}
		
		return mensajeError.toString();
	}
	
	
	/***
	 * Valida si la condicion ya fue vinculada al inciso
	 * @param condiciones
	 * @return
	 */
	private Long validaCondicionVinculadaBaja(Long condicion, long inciso){
		
		List<CondicionEspecialBitemporalDTO> lCondBitemporalDTO = null;
		long keyEntidad = 0; // # ALMACENARA LA LLAVE DE LA CONDICION ENCONTRADA DE RECORRER EL LIST
		
		// # POLIZA
		if(this.getNivelAplicacion() == 0){
			
			// # OBTIENE TODAS LAS CONDICIONES ASOCIADAS A POLIZA
			lCondBitemporalDTO = 
						this.condicionEspecialBitemporalService.
						obtenerCondicionesAsociadasCotizacion(
										this.getIdCotizacionContinuity(), 
										this.getValidoEn() 
							);

		}else{
		// # INCISO	
			// # OBTIENE TODAS LAS CONDICIONES ASOCIADAS AL INCISO
			lCondBitemporalDTO = 
						this.condicionEspecialBitemporalService.
							obtenerCondicionesIncisoAsociadas(
									this.getIdIncisoContinuity(inciso), 
									this.getValidoEn() 
							);

			
		}
		
		// # RECORRER LIST Y OBTENER IDCONDICIONESCONTINUITY
		
		for( CondicionEspecialBitemporalDTO cbd:lCondBitemporalDTO){
			
			if( cbd.getCondicionEspecial().getCodigo() == condicion ){
				keyEntidad = cbd.getIdContinuity();// getIdCondicionBitemporal();
			}
			
		}
		
		if(keyEntidad == 0){
			listErrores.add(
					this.crearError(new BigDecimal(0),"","La condicion especial: "+condicion+" agregada al inciso: "+inciso+" no se pude eliminar al no encontrarse asociada a la condicion en la base de datos", "")
			);
		}
		
		return keyEntidad;
	}
	
	public FileInputStream descargaLogErrores(BigDecimal idControlArchivo) {
		try {
			ControlArchivoDN controlArchivoDN = ControlArchivoDN.getInstancia();
			controlArchivoDTO = controlArchivoDN.getPorId(idControlArchivo);

			File file = new File(uploadFolder + "Log"
					+ controlArchivoDTO.getIdToControlArchivo() + ".txt");

			return new FileInputStream(file);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public InputStream descargaLogErroresInciso(String textoError) {

		InputStream is = null;
		//out.write("N\u00FAmero de L\u00EDnea |  Nombre de la secci\u00F3n |   Nombre de campo que no cumpli\u00F3 con la validaci\u00F3n \r\n" + System.getProperty("line.separator"));
        try {
            is = new ByteArrayInputStream(textoError.getBytes("UTF-8"));
        } catch (UnsupportedEncodingException e) {
            textoError = e.getMessage();
            try {
				is = new ByteArrayInputStream(textoError.getBytes("UTF-8"));
			} catch (Exception e1) {
				e1.printStackTrace();
			}
        }
		return is;
	}	
	
	/**
	 * Recorre el mapa donde se guardo el inciso y las condiciones especiales, asi como persiste 
	 * @param Map datosInciso key-Inciso Value-String[]
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private void validaArchivoCargaIncisos(Map<Long, List<CondicionEspecial>> datosIncisoCot,List<Long> lIncisos){
			
			List<CargaMasivaCondicionesEspDet> lCargaMasivaCondicionesEspDet 
										 = new ArrayList<CargaMasivaCondicionesEspDet>();
			StringBuilder condicionesEspeciales = new StringBuilder("");
			long idCondicionContinuity   = 0;
			StringBuilder sIncisos       = new StringBuilder();
			
			Iterator<?> iterator = datosIncisoCot.entrySet().iterator();
			while (iterator.hasNext()) {

				Map.Entry mapEntry = (Map.Entry) iterator.next();
				List<CondicionEspecial> datoCondicionEspecial = (List<CondicionEspecial>) mapEntry.getValue();
				
				CargaMasivaCondicionesEspDet cargaMasivaCondicionesEspDet = new CargaMasivaCondicionesEspDet();
				
				//System.out.println("llave: " + mapEntry.getKey()+ ", valor :" + mapEntry.getValue().toString() + " - " + datoCondicionEspecial.get(0).getId() );
				
				if(! datoCondicionEspecial.isEmpty() ){
					
					// # ALTA
					if(this.getAccionEndoso() == 0){
						
						for(CondicionEspecial ce: datoCondicionEspecial){
								
							CondicionEspecialBitemporalDTO condicionEspecialBitemporalDTO = new CondicionEspecialBitemporalDTO();
							condicionEspecialBitemporalDTO.setCondicionEspecial(ce);
								
							// # INCISO
							if( this.getNivelAplicacion() == 1 ){
									
								this.condicionEspecialBitemporalService.guardarCondicionInciso(
										Long.valueOf( mapEntry.getKey().toString() ), 
										condicionEspecialBitemporalDTO, 
										this.getValidoEn()
								);
								
							}else{
							// # POLIZA	
								this.condicionEspecialBitemporalService.guardarCondicionCotizacion(
										this.getIdCotizacionContinuity(),
										condicionEspecialBitemporalDTO,
										this.getValidoEn()
								);
							}
							
							condicionesEspeciales.append(ce.getCodigo()).append(","); // # CONCATENAR CONDCIONES ESPECIALES
						}
						
						cargaMasivaCondicionesEspDet.setCondicionesAlta(condicionesEspeciales.toString()); // # LLENAR OBJETO DE CARGA MASIVA
						condicionesEspeciales.delete(0,condicionesEspeciales.length()); // # LIMPIAR VARIABLE
						
					}else{
					// # BAJA
						
						// # INCISO
						if( this.getNivelAplicacion() == 1 ){
							
							for( Long lInc : lIncisos ){
							
								for(CondicionEspecial ce: datoCondicionEspecial){
									
									CondicionEspecialBitemporalDTO condicionEspecialBitemporalDTO = new CondicionEspecialBitemporalDTO();
									condicionEspecialBitemporalDTO.setCondicionEspecial(ce);
									
									
										idCondicionContinuity = this.validaCondicionVinculadaBaja(ce.getCodigo(), lInc.longValue() );
										
										if( idCondicionContinuity !=0 ){
											condicionEspecialBitemporalDTO.setIdContinuity(idCondicionContinuity);
											this.condicionEspecialBitemporalService.eliminarCondicionInciso( 
																condicionEspecialBitemporalDTO, 
																this.getValidoEn()
											);
										}
										condicionesEspeciales.append(ce.getCodigo()).append(","); // # CONCATENAR CONDCIONES ESPECIALES
								}
							}
							
						}else{
							
							// # POLIZA
							for(CondicionEspecial ce: datoCondicionEspecial){
									
								CondicionEspecialBitemporalDTO condicionEspecialBitemporalDTO = new CondicionEspecialBitemporalDTO();
								condicionEspecialBitemporalDTO.setCondicionEspecial(ce);
									
								// # POLIZA	
								if( this.getNivelAplicacion() == 0 ){
									idCondicionContinuity = this.validaCondicionVinculadaBaja(ce.getCodigo(), 0 );
									
									if( idCondicionContinuity != 0){
										condicionEspecialBitemporalDTO.setIdContinuity(idCondicionContinuity);
										this.condicionEspecialBitemporalService.eliminarCondicionCotizacion(
													condicionEspecialBitemporalDTO, 
													this.getValidoEn()
											);
									}
								}
								condicionesEspeciales.append(ce.getCodigo()).append(","); // # CONCATENAR CONDCIONES ESPECIALES
							}
						
						}
						
						cargaMasivaCondicionesEspDet.setCondicionesBaja(condicionesEspeciales.toString()); // # LLENAR OBJETO DE CARGA MASIVA
						condicionesEspeciales.delete(0,condicionesEspeciales.length()); // # LIMPIAR VARIABLE
						
					}
					
					// # LLENAR OBJETO DE CARGA MASIVA
					cargaMasivaCondicionesEspDet.setIdInciso(Long.valueOf( mapEntry.getKey().toString() ));
					cargaMasivaCondicionesEspDet.setIdToCotizacion(this.getIdToCotizacion());
					cargaMasivaCondicionesEspDet.setIdCargaMasivaCondicionesEsp(this.getIdCargaMasivaCondiciones());
					
					lCargaMasivaCondicionesEspDet.add(cargaMasivaCondicionesEspDet); // # AGREGA AL LIST
					
				}// # CIERRA IF DATOCONDICIONESPECIAL
			}// # CIERRA WHILE ITERATOR
			
			// # RECORRER INCISOS Y CONCATENAR -- APLICAN PARA MANDARLOS A LA PANTALLA DE ENDOZO
			for(Long lI:lIncisos){
				sIncisos.append( lI.longValue() ).append(",");
			}
			this.setListaIncisos( sIncisos.toString().substring(0,sIncisos.toString().length() -1) );
			
			if(listErrores.isEmpty()){
				
				// # GUARDA EL HISTORICO DE LAS CONDICIONES AGREGADAS
				this.cargaMasivaService.guardaDetalleCargaMasivaCondiciones(lCargaMasivaCondicionesEspDet);
			}else{
				this.setHasLogErrors(true);
				this.creaLogErrores();
			}
	}
	
	/***
	 * Recupera el incisoContinuytiId en base a su IdCotizacionContinuity y fecha 
	 * @param incisoABuscar
	 * @return
	 */
	private Long getIdIncisoContinuity(long incisoABuscar){
		
		Collection<BitemporalInciso> incisos = incisoViewService.getLstIncisoByCotizacion( this.getIdCotizacionContinuity() ,this.getValidoEn());
		
		long inciso = 0;
		
		for(BitemporalInciso inc:incisos){
			
			if(inc.getContinuity().getNumero() == incisoABuscar){
				inciso = inc.getContinuity().getKey();
				break;
			}
		}
		
		return inciso;
	}

	@Override
	protected void buildDataSheet(HSSFSheet sheet) {}

	

	public boolean isHasLogErrors() {
		return hasLogErrors;
	}


	public void setHasLogErrors(boolean hasLogErrors) {
		this.hasLogErrors = hasLogErrors;
	}


	public void setIncisoViewService(IncisoViewService incisoViewService) {
		this.incisoViewService = incisoViewService;
	}

	public Date getValidoEn() {
		return validoEn;
	}

	public void setValidoEn(Date validoEn) {
		this.validoEn = validoEn;
	}

	public long getIdToNegocio() {
		return idToNegocio;
	}

	public void setIdToNegocio(long idToNegocio) {
		this.idToNegocio = idToNegocio;
	}

	public short getAccionEndoso() {
		return accionEndoso;
	}

	public void setAccionEndoso(short accionEndoso) {
		this.accionEndoso = accionEndoso;
	}

	public String getNumeroPolizaFormateado() {
		return numeroPolizaFormateado;
	}

	public void setNumeroPolizaFormateado(String numeroPolizaFormateado) {
		this.numeroPolizaFormateado = numeroPolizaFormateado;
	}

	public Long getIdCotizacionContinuity() {
		return idCotizacionContinuity;
	}

	public void setIdCotizacionContinuity(Long idCotizacionContinuity) {
		this.idCotizacionContinuity = idCotizacionContinuity;
	}

	public CondicionEspecialBitemporalService getCondicionEspecialBitemporalService() {
		return condicionEspecialBitemporalService;
	}

	public void setCondicionEspecialBitemporalService(
			CondicionEspecialBitemporalService condicionEspecialBitemporalService) {
		this.condicionEspecialBitemporalService = condicionEspecialBitemporalService;
	}

	private Long getIdCargaMasivaCondiciones() {
		return idCargaMasivaCondiciones;
	}

	public void setIdCargaMasivaCondiciones(Long idCargaMasivaCondiciones) {
		this.idCargaMasivaCondiciones = idCargaMasivaCondiciones;
	}

	private long getIdToCotizacion() {
		return idToCotizacion;
	}

	public void setIdToCotizacion(long idToCotizacion) {
		this.idToCotizacion = idToCotizacion;
	}

	private short getNivelAplicacion() {
		return nivelApp;
	}

	public void setNivelAplicacion(short nivelApp) {
		this.nivelApp = nivelApp;
	}

	public String getListaIncisos() {
		return listaIncisos;
	}

	public void setListaIncisos(String listaIncisos) {
		this.listaIncisos = listaIncisos;
	}



}
