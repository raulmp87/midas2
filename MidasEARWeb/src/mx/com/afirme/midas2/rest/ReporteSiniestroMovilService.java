package mx.com.afirme.midas2.rest;

import javax.ejb.EJB;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import mx.com.afirme.midas2.domain.Coordenadas;
import mx.com.afirme.midas2.domain.siniestro.ReporteSiniestroMovil;
import mx.com.afirme.midas2.service.catalogos.EntidadService;


@Path("/reporte-siniestro-movil")
public class ReporteSiniestroMovilService {

	@EJB
	private EntidadService entidadService;
   
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public ReporteSiniestroMovil guardar(@QueryParam("numeroPoliza") String numeroPoliza, 
    		@QueryParam("numeroCelular")String numeroCelular, 
    		@QueryParam("latitud") Double latitud, @QueryParam("longitud") Double longitud) {
    	ReporteSiniestroMovil reporteSiniestroMovil = new ReporteSiniestroMovil();
    	reporteSiniestroMovil.setNumeroCelular(numeroCelular);
    	reporteSiniestroMovil.setCoordenadas(new Coordenadas(latitud, longitud));
    	reporteSiniestroMovil = entidadService.save(reporteSiniestroMovil);
    	return reporteSiniestroMovil;
    }
    
}
