package mx.com.afirme.midas2.utils;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class SystemCommonUtilsTest {
	
	private SystemCommonUtils tested;
	
	@Before
	public void setUp(){
		tested = new SystemCommonUtils();
	}

	@Test
	public void testIsValid() {
		assertTrue(SystemCommonUtils.isValid("1"));
	}
	
	@Test
	public void testIsValid_null() {
		assertFalse(SystemCommonUtils.isValid(null));
	}
	
	@Test
	public void testIsValid_empty() {
		assertFalse(SystemCommonUtils.isValid(""));
	}
	
	@Test
	public void testIsValid_spaces() {
		assertFalse(SystemCommonUtils.isValid("  "));
	}

}
