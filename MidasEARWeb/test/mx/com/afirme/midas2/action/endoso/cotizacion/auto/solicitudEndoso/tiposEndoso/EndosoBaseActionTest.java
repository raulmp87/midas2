package mx.com.afirme.midas2.action.endoso.cotizacion.auto.solicitudEndoso.tiposEndoso;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class EndosoBaseActionTest {
	EndosoBaseAction tested;

	@Before
	public void setUp() throws Exception {
		tested= new EndosoBaseAction();
		}

	@Test
	public void testObtenerIdsSeleccionados() {
		String[] continuitiesStringIds = {
				"s-d", "ejemplo2"				
		};
		assertEquals("ejemplo2,",tested.obtenerIdsSeleccionados(continuitiesStringIds,""));
	}
	@Test
	public void testObtenerIdsSeleccionados_null() {
		String[] continuitiesStringIds = {
				"", ""				
		};
		assertEquals(",,",tested.obtenerIdsSeleccionados(continuitiesStringIds,""));
	}
	@Test
	public void testObtenerIdsSeleccionados_dos() {
		String[] continuitiesStringIds = {
				"ejemplo1", "ejemplo2"				
		};
		assertEquals("ejemplo1,ejemplo2,",tested.obtenerIdsSeleccionados(continuitiesStringIds,""));
	}
}
