package mx.com.afirme.midas2.action;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;




import org.junit.Before;
import org.junit.Test;


public class BaseActionTest {

	private BaseAction tested;


	@Before
	public void setUp(){
		tested = new BaseAction();
	}
	@Test
	public void testSetMensajeListaPersonalizado() {
		List<String> param =new ArrayList<String>();
		param.add("lista 1");
		param.add("lista 2");
		param.add("lista 3");
		param.add("lista 4");
		assertEquals("Ejemplo encabezado"+
				"\n<ul>"+
				"\n<li>lista 1</li>"+
				"\n<li>lista 2</li>"+
				"\n<li>lista 3</li>"+
				"\n<li>lista 4</li>"+
				"\n</ul>", tested.obtenerMensajeListaPersonalizado("Ejemplo encabezado" , param));
	}
	@Test
	public void testSetMensajeListaPersonalizado_vacio() {
		List<String> param =new ArrayList<String>();
		assertEquals("Ejemplo encabezado"+
				"\n<ul>"+"\n</ul>", tested.obtenerMensajeListaPersonalizado("Ejemplo encabezado" , param));
	}


	@Test
	public void testSetElementosSeleccionados() {
		List<String> param =new ArrayList<String>();
		param.add("lista 1");
		param.add("lista 2");
		param.add("lista 3");
		param.add("lista 4");
		assertEquals("lista 1,lista 2,lista 3,lista 4,", tested.obtenerElementoSeleccionadosDistintos(param,",","-"));
	}
	@Test
	public void testSetElementosSeleccionados_vacio() {
		List<String> param =new ArrayList<String>();
		
		assertEquals("", tested.obtenerElementoSeleccionadosDistintos(param,",","-"));
	}

}
