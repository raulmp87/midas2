package mx.com.afirme.midas2.action.catalogos.fuerzaventa.reporteAgente;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

import java.util.LinkedHashMap;
import java.util.Map;

import mx.com.afirme.midas2.dto.reportesAgente.DatosAgenteEstatusEntregaFactura;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

public class ReporteEstatusEntregaFacturasActionTest {
	ReporteEstatusEntregaFacturasAction tested;
	@Mock
	private DatosAgenteEstatusEntregaFactura datos;

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		tested = new ReporteEstatusEntregaFacturasAction();
		tested.setDatos(datos);
	}

	@Test
	public void testGetEstatus() {
	//	datos = new DatosAgenteEstatusEntregaFactura();
		Map<String, Boolean> listEstatus = new  LinkedHashMap<String, Boolean>();
		listEstatus.put("0", true);
		listEstatus.put("1", false);
		listEstatus.put("2", true);
		when(datos.getListEstatus()).thenReturn(listEstatus);
		assertEquals("ENTREGADA','NO TIENE PAGOS", tested.getEstatus());
	}
	
	@Test
	public void testGetEstatusAllTrue() {
	//	datos = new DatosAgenteEstatusEntregaFactura();
		Map<String, Boolean> listEstatus = new  LinkedHashMap<String, Boolean>();
		listEstatus.put("0", true);
		listEstatus.put("1", true);
			listEstatus.put("2", true);
		when(datos.getListEstatus()).thenReturn(listEstatus);
		assertEquals("ENTREGADA','PENDIENTE','NO TIENE PAGOS", tested.getEstatus());
	}
	
	@Test
	public void testGetEstatusAllFalse() {
	//	datos = new DatosAgenteEstatusEntregaFactura();
		Map<String, Boolean> listEstatus = new  LinkedHashMap<String, Boolean>();
		listEstatus.put("0", false);
		listEstatus.put("1", false);
		listEstatus.put("2", false);
		when(datos.getListEstatus()).thenReturn(listEstatus);
		assertEquals("", tested.getEstatus());
	}
	@Test
	public void testGetEstatusnull() {
	//	datos = new DatosAgenteEstatusEntregaFactura();
		Map<String, Boolean> listEstatus = new  LinkedHashMap<String, Boolean>();
		when(datos.getListEstatus()).thenReturn(listEstatus);
		assertEquals("", tested.getEstatus());
	}

}
