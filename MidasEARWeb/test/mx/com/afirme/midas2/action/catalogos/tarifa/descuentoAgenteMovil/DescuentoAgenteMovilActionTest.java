package mx.com.afirme.midas2.action.catalogos.tarifa.descuentoAgenteMovil;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

public class DescuentoAgenteMovilActionTest {
	DescuentoAgenteMovilAction tested;

	@Before
	public void setUp() throws Exception {
		tested = new DescuentoAgenteMovilAction();
	}

	@Test
	public void testObtenerLogErrores() {
		List<String> lista = new ArrayList<String>();
		lista.add("ejemplo 1");
		lista.add("ejemplo 2");
		lista.add("ejemplo 3");
		assertEquals("<br>ejemplo 1", tested.obtenerLogErrores(lista));
	}
	@Test
	public void testObtenerLogErroresmasdos() {
		List<String> lista = new ArrayList<String>();
		lista.add("ejemplo 1");
		lista.add("ejemplo 2");
		lista.add("ejemplo 3");
		lista.add("ejemplo 3");
		assertEquals("<br>ejemplo 1<br>ejemplo 2", tested.obtenerLogErrores(lista));
	}
	
	@Test
	public void testObtenerLogErrores_dos() {
		List<String> lista = new ArrayList<String>();
		lista.add("ejemplo 1");
		lista.add("ejemplo 2");
		assertEquals("", tested.obtenerLogErrores(lista));
	}
	@Test
	public void testObtenerLogErrores_null() {
		List<String> lista = new ArrayList<String>();
		assertEquals("", tested.obtenerLogErrores(lista));
	}


}
