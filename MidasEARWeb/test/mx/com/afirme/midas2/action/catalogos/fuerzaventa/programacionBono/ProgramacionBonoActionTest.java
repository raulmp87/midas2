package mx.com.afirme.midas2.action.catalogos.fuerzaventa.programacionBono;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import mx.com.afirme.midas2.domain.bonos.ConfigBonos;

import org.junit.Before;
import org.junit.Test;

public class ProgramacionBonoActionTest {
	ProgramacionBonoAction tested;
	ConfigBonos configBonos;

	@Before
	public void setUp() throws Exception {
		tested = new ProgramacionBonoAction();
	}

	@Test
	public void testObtenerIdsConfigBono() {
		List<ConfigBonos> listaBonoNoVigente = new ArrayList<ConfigBonos>();
		configBonos = new ConfigBonos();
		configBonos.setId(1L);
		listaBonoNoVigente.add(configBonos);
		configBonos = new ConfigBonos();
		configBonos.setId(2L);
		listaBonoNoVigente.add(configBonos);
		assertEquals("2, 1, ", tested.obtenerIdsConfigBono(listaBonoNoVigente));
	}
	@Test
	public void testObtenerIdsConfigBono_null() {
		List<ConfigBonos> listaBonoNoVigente = new ArrayList<ConfigBonos>();
		assertEquals("", tested.obtenerIdsConfigBono(listaBonoNoVigente));
	}

}
