package mx.com.afirme.midas2.action.catalogos.tarifa.tarifaMovil;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import mx.com.afirme.midas2.domain.movil.RespuestaCargaMasiva;

import org.junit.Before;
import org.junit.Test;

public class TarifaMovilActionTest {
	TarifaMovilAction tested;
	RespuestaCargaMasiva respuesta;

	@Before
	public void setUp() throws Exception {
		tested = new TarifaMovilAction();
	}

	@Test
	public void testObtenerLogErrores() {
		respuesta = new RespuestaCargaMasiva();
		List<String> registrosAfectados = new ArrayList<String>();
		registrosAfectados.add("registro1");
		registrosAfectados.add("registro2");
		registrosAfectados.add("registro3");
		respuesta.setRegistrosAfectados(registrosAfectados);
		assertEquals("<br>registro1<br>registro2<br>registro3",tested.obtenerLogErrores(respuesta));
	}

	@Test
	public void testObtenerLogErrores_null() {
		respuesta = new RespuestaCargaMasiva();
		List<String> registrosAfectados = new ArrayList<String>();
		respuesta.setRegistrosAfectados(registrosAfectados);
		assertEquals("",tested.obtenerLogErrores(respuesta));
	}

}
