package mx.com.afirme.midas2.action.suscripcion.cotizacion.auto;

import static org.junit.Assert.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

public class CotizacionExpressActionTest {
	CotizacionExpressAction tested;

	@Before
	public void setUp() throws Exception {
		tested = new CotizacionExpressAction();
	}

	@Test
	public void testGetFormaPagoMatrizStr_tres() {
		List<BigDecimal> formaPagoMatriz = new ArrayList<BigDecimal>();
		formaPagoMatriz.add(BigDecimal.ZERO);
		formaPagoMatriz.add(BigDecimal.ONE);
		formaPagoMatriz.add(BigDecimal.TEN);
		tested.setFormaPagoMatriz(formaPagoMatriz);
		assertEquals("0,1,10", tested.getFormaPagoMatrizStr());
	}
	
	@Test
	public void testGetFormaPagoMatrizStr_dos() {
		List<BigDecimal> formaPagoMatriz = new ArrayList<BigDecimal>();
		formaPagoMatriz.add(BigDecimal.ZERO);
		formaPagoMatriz.add(BigDecimal.ONE);
		tested.setFormaPagoMatriz(formaPagoMatriz);
		assertEquals("0,1", tested.getFormaPagoMatrizStr());
	}

	@Test
	public void testGetFormaPagoMatrizStr_cero() {
		List<BigDecimal> formaPagoMatriz = new ArrayList<BigDecimal>();
		tested.setFormaPagoMatriz(formaPagoMatriz);
		assertEquals(null, tested.getFormaPagoMatrizStr());
	}
	@Test
	public void testGetPaquetesMatrizStr() {
		List<BigDecimal> paquetesMatriz = new ArrayList<BigDecimal>();
		paquetesMatriz.add(BigDecimal.ZERO);
		paquetesMatriz.add(BigDecimal.ONE);
		paquetesMatriz.add(BigDecimal.TEN);
		tested.setPaquetesMatriz(paquetesMatriz);
		assertEquals("0,1,10", tested.getPaquetesMatrizStr());
	}
	@Test
	public void testGetPaquetesMatrizStr_null() {
		List<BigDecimal> paquetesMatriz = new ArrayList<BigDecimal>();
		tested.setPaquetesMatriz(paquetesMatriz);
		assertEquals(null, tested.getPaquetesMatrizStr());
	}
}
