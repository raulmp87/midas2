package mx.com.afirme.midas.contratofacultativo;

import static org.junit.Assert.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import mx.com.afirme.midas.contratofacultativo.slip.SlipDTO;

import org.junit.Before;
import org.junit.Test;

public class ContratoFacultativoActionTest {
	
	ContratoFacultativoAction tested;
	SlipDTO slip;
	DetalleContratoFacultativoDTO detalle;

	@Before
	public void setUp() throws Exception {
		tested = new ContratoFacultativoAction();
		slip = new SlipDTO();
		detalle = new DetalleContratoFacultativoDTO();
	}


	@Test
	public void testObtenerMensaje() {
		
		List<DetalleContratoFacultativoDTO> detalleContratoFacultativoDTOList = new ArrayList<DetalleContratoFacultativoDTO>();
		detalle.setContratoFacultativoDTO(BigDecimal.valueOf(12345));
		detalleContratoFacultativoDTOList.add(detalle);
		assertEquals("_12345", tested.obtenerMensaje(detalleContratoFacultativoDTOList));

	}
	
	@Test
	public void testObtenerMensaje_null() {
		
		List<DetalleContratoFacultativoDTO> detalleContratoFacultativoDTOList = new ArrayList<DetalleContratoFacultativoDTO>();
		assertEquals("", tested.obtenerMensaje(detalleContratoFacultativoDTOList));

	}
}
