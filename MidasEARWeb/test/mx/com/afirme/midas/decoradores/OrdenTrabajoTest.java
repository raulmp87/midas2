package mx.com.afirme.midas.decoradores;

import static org.junit.Assert.*;

import java.math.BigDecimal;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;

import org.junit.Before;
import org.junit.Test;

public class OrdenTrabajoTest {
	OrdenTrabajo tested;
	CotizacionDTO cotizacion;

	@Before
	public void setUp() throws Exception {
		tested = new OrdenTrabajo();
	}

	@Test
	public void testObtenerCodigoMenorAOcho() {
		cotizacion = new CotizacionDTO();
		cotizacion.setIdToCotizacion(BigDecimal.ONE);
		assertEquals("ODT-00000001", tested.obtenerCodigo(cotizacion));
	}

	@Test
	public void testObtenerCodigoOcho() {
		cotizacion = new CotizacionDTO();
		cotizacion.setIdToCotizacion(BigDecimal.valueOf(12345678));
		assertEquals("ODT-12345678", tested.obtenerCodigo(cotizacion));
	}
	@Test
	public void testObtenerCodigoMayorAOcho() {
		cotizacion = new CotizacionDTO();
		cotizacion.setIdToCotizacion(BigDecimal.valueOf(1234567890));
		assertEquals("ODT-1234567890", tested.obtenerCodigo(cotizacion));
	}
}
