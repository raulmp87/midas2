package mx.com.afirme.midas.cotizacion.validacion;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

public class ValidarCotizacionActionTest {
	ValidarCotizacionAction validar;

	@Before
	public void setUp() throws Exception {
		validar =  new ValidarCotizacionAction();
		}
	

	@Test
	public void testObtenerError() {
		List<String> errores =new ArrayList<String>();
		errores.add("error 1");
		errores.add("error 2");
		String resul="error 1<br>error 2<br>";
		assertEquals(resul, validar.obtenerError(errores));
	}
	@Test
	public void testObtenerError_null() {
		List<String> errores =new ArrayList<String>();
		String resul="";
		assertEquals(resul, validar.obtenerError(errores));
	}
	@Test
	public void testObtenerErrorMasDeDos() {
		List<String> errores =new ArrayList<String>();
		errores.add("error 1");
		errores.add("error 2");
		errores.add("error 3");
		errores.add("error 4");
		String resul="error 1<br>error 2<br>error 3<br>error 4<br>";
		assertEquals(resul, validar.obtenerError(errores));
	}
}
