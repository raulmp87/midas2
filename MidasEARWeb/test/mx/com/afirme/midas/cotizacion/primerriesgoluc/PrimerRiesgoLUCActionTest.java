package mx.com.afirme.midas.cotizacion.primerriesgoluc;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionDTO;

import org.junit.Before;
import org.junit.Test;

public class PrimerRiesgoLUCActionTest {
	PrimerRiesgoLUCAction tested;
	SeccionDTO seccion;
	@Before
	public void setUp() throws Exception {
		tested = new PrimerRiesgoLUCAction();	
	}

	@Test
	public void testObtenerSeccionPRR() {
		seccion = new SeccionDTO();
		seccion.setNombreComercial("ejemplo nombre comercial");
		List<SeccionDTO> secciones = new ArrayList<SeccionDTO>();
		secciones.add(seccion);
		assertEquals("ejemplo nombre comercial ", tested.obtenerSeccionPRR(secciones));

	}

	@Test
	public void testObtenerSeccionPRR_null() {
		//seccion.setNombreComercial("ejemplo nombre comercial");
		List<SeccionDTO> secciones = new ArrayList<SeccionDTO>();
	//	secciones.add(seccion);
		assertEquals("", tested.obtenerSeccionPRR(secciones));

	}
	
	@Test
	public void testObtenerSeccionPRRMasDeUno() {
		List<SeccionDTO> secciones = new ArrayList<SeccionDTO>();
		seccion = new SeccionDTO();
		seccion.setNombreComercial("ejemplo nombre comercial1");
		secciones.add(seccion);
		seccion = new SeccionDTO();
		seccion.setNombreComercial("ejemplo nombre comercial2");
		secciones.add(seccion);
		seccion = new SeccionDTO();
		seccion.setNombreComercial("ejemplo nombre comercial3");
		secciones.add(seccion);
		assertEquals("ejemplo nombre comercial1 ejemplo nombre comercial2 ejemplo nombre comercial3 ", tested.obtenerSeccionPRR(secciones));

	}
}
