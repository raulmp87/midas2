package mx.com.afirme.midas.cotizacion;

import static org.junit.Assert.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.exclusion.CoberturaExcluidaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.requerida.CoberturaRequeridaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.requerida.CoberturaRequeridaId;

import org.junit.Before;
import org.junit.Test;

public class CotizacionActionTest {
	CotizacionAction tested;
	CoberturaRequeridaId coberturaRequeridaId;
	CoberturaDTO coberturaDTO;
	CoberturaRequeridaDTO coberturaRequeridaDTO;
	CoberturaExcluidaDTO coberturaExcluidaDTO;

	@Before
	public void setUp() throws Exception {
		tested = new CotizacionAction();
		
	}

	@Test
	public void testObtenerListaCoberturaRequeridas() {
		coberturaRequeridaDTO = new CoberturaRequeridaDTO();
		coberturaRequeridaId = new CoberturaRequeridaId();
		List<CoberturaRequeridaDTO> requeridas = new ArrayList<CoberturaRequeridaDTO>();
		coberturaRequeridaId.setIdToCoberturaRequerida(BigDecimal.ONE);
		coberturaRequeridaDTO.setId(coberturaRequeridaId);
		requeridas.add(coberturaRequeridaDTO);
		coberturaRequeridaDTO = new CoberturaRequeridaDTO();
		coberturaRequeridaId = new CoberturaRequeridaId();
		coberturaRequeridaId.setIdToCoberturaRequerida(BigDecimal.ZERO);
		coberturaRequeridaDTO.setId(coberturaRequeridaId);
		requeridas.add(coberturaRequeridaDTO);
		assertEquals("1_0", tested.obtenerListaCoberturaRequeridas(requeridas));

	}

	@Test
	public void testObtenerListaCoberturaRequeridas_null() {
		List<CoberturaRequeridaDTO> requeridas = new ArrayList<CoberturaRequeridaDTO>();
		assertEquals("", tested.obtenerListaCoberturaRequeridas(requeridas));

	}
	
	@Test
	public void testObtenerListaCoberturaExcluidas() {
		List<CoberturaExcluidaDTO> Excluida = new ArrayList<CoberturaExcluidaDTO>();
		coberturaExcluidaDTO = new CoberturaExcluidaDTO();
		coberturaDTO = new CoberturaDTO();
		coberturaDTO.setIdToCobertura(BigDecimal.ONE);
		coberturaExcluidaDTO.setCoberturaExcluidaDTO(coberturaDTO);
		Excluida.add(coberturaExcluidaDTO);
		coberturaExcluidaDTO = new CoberturaExcluidaDTO();
		coberturaDTO = new CoberturaDTO();
		coberturaDTO.setIdToCobertura(BigDecimal.ZERO);
		coberturaExcluidaDTO.setCoberturaExcluidaDTO(coberturaDTO);
		Excluida.add(coberturaExcluidaDTO);
		assertEquals("1_0", tested.obtenerListaCoberturaExcluidas(Excluida));

	}
	
	@Test
	public void testObtenerListaCoberturaExcluidas_null() {
		List<CoberturaExcluidaDTO> Excluida = new ArrayList<CoberturaExcluidaDTO>();
				assertEquals("", tested.obtenerListaCoberturaExcluidas(Excluida));

	}
	@Test
	public void testObtenerIdMenorACinco() {
		assertEquals("00012", tested.obtenerId("12"));

	}
	@Test
	public void testObtenerIdCinco() {
		assertEquals("12345", tested.obtenerId("12345"));

	}
	@Test
	public void testObtenerIdMayorACinco() {
		assertEquals("1234567", tested.obtenerId("1234567"));

	}

}
