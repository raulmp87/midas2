package mx.com.afirme.midas.cotizacion.cobertura;

import static org.junit.Assert.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

public class CoberturaCotizacionActionTest {
	CoberturaCotizacionAction tested;
	CoberturaCotizacionDTO coberturaCotizacionDTO;
	CoberturaCotizacionId coberturaCotizacionId;
	

	@Before
	public void setUp() throws Exception {
		tested = new CoberturaCotizacionAction();
	}

	@Test
	public void testObtenerSendRequest() {
		coberturaCotizacionDTO = new CoberturaCotizacionDTO();
		coberturaCotizacionId = new CoberturaCotizacionId();
		coberturaCotizacionId.setIdToCotizacion(BigDecimal.ZERO);
		coberturaCotizacionId.setNumeroInciso(BigDecimal.ZERO);
		coberturaCotizacionId.setIdToSeccion(BigDecimal.ZERO);
		coberturaCotizacionId.setIdToCobertura(BigDecimal.ZERO);
		coberturaCotizacionDTO.setId(coberturaCotizacionId);
		String res = "sendRequest(null,'/MidasWeb/cotizacion/cobertura/mostrarARD.do?idToCotizacion=0&numeroInciso=0&idToSeccion=0&idToCobertura=0','configuracion_detalle',null);";
		assertEquals(res, tested.obtenerSendRequest(coberturaCotizacionDTO));
	}

	
}
