package mx.com.afirme.midas.endoso;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

public class EndosoDNTest {
	EndosoDN tested;

	@Before
	public void setUp() throws Exception {
		tested = new EndosoDN();
	}

	@Test
	public void testObtenerDescError() {
		List<String> errores = new ArrayList<String>();
		errores.add("error1");
		errores.add("error2");
		assertEquals("error1\nerror2\n",tested.obtenerDescError(errores));
	}
	@Test
	public void testObtenerDescError_null() {
		List<String> errores = new ArrayList<String>();
		
		assertEquals("",tested.obtenerDescError(errores));
	}

}
