package mx.com.afirme.midas.danios.reportes;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;

import java.util.ArrayList;
import java.util.List;

import mx.com.afirme.midas.cotizacion.seccion.SeccionCotizacionDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionDTO;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

public class PlantillaCotizacionBaseTest {

	@Mock
	PlantillaCotizacionBase tested;
	
	SeccionCotizacionDTO seccionCotizacionDTO;
	SeccionDTO seccionDTO;

	@Before
	public void setUp() throws Exception {
		tested = mock(PlantillaCotizacionBase.class, Mockito.CALLS_REAL_METHODS);
	}

	@Test
	public void testConcatenarNombreSecciones() {
		List<SeccionCotizacionDTO> listaSeccionesPrimerRiesgo = new ArrayList<SeccionCotizacionDTO>();
		seccionCotizacionDTO =  new SeccionCotizacionDTO();
		seccionDTO = new SeccionDTO();
		seccionDTO.setNombreComercial("ejemplo1");
		seccionCotizacionDTO.setSeccionDTO(seccionDTO);
		listaSeccionesPrimerRiesgo.add(seccionCotizacionDTO);
		seccionCotizacionDTO =  new SeccionCotizacionDTO();
		seccionDTO = new SeccionDTO();
		seccionDTO.setNombreComercial("ejemplo2");
		seccionCotizacionDTO.setSeccionDTO(seccionDTO);
		listaSeccionesPrimerRiesgo.add(seccionCotizacionDTO);
		seccionCotizacionDTO =  new SeccionCotizacionDTO();
		seccionDTO = new SeccionDTO();
		seccionDTO.setNombreComercial("ejemplo3");
		seccionCotizacionDTO.setSeccionDTO(seccionDTO);
		listaSeccionesPrimerRiesgo.add(seccionCotizacionDTO);
		assertEquals("ejemplo1, ejemplo2 y ejemplo3", tested.concatenarNombreSecciones(listaSeccionesPrimerRiesgo));
	}
	@Test
	public void testConcatenarNombreSecciones_null() {
		List<SeccionCotizacionDTO> listaSeccionesPrimerRiesgo = new ArrayList<SeccionCotizacionDTO>();
		assertEquals("", tested.concatenarNombreSecciones(listaSeccionesPrimerRiesgo));
	}

}
