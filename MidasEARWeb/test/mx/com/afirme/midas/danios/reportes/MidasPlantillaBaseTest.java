package mx.com.afirme.midas.danios.reportes;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;

import org.mockito.Mock;
import org.mockito.Mockito;
import org.junit.Before;
import org.junit.Test;

public class MidasPlantillaBaseTest {
	@Mock
	MidasPlantillaBase tested;

	@Before
	public void setUp() throws Exception {
		tested = mock(MidasPlantillaBase.class,Mockito.CALLS_REAL_METHODS);
	}

	@Test
	public void testCalculaNumeroRomano() {
	       assertEquals("I",tested.calculaNumeroRomano(1));
	}
	
	@Test
	public void testCalculaNumeroRomanoMayorACient() {
	       assertEquals("CI",tested.calculaNumeroRomano(101));
	}
	
	@Test
	public void testCalculaNumeroRomanocero() {
	       assertEquals("",tested.calculaNumeroRomano(0));
	}

}
