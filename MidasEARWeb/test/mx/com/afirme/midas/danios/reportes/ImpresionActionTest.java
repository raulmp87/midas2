package mx.com.afirme.midas.danios.reportes;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class ImpresionActionTest {
	ImpresionAction imp;

	@Before
	public void setUp(){
		imp = new ImpresionAction();
	}

	@Test
	public void testObtenerNombreArchivo() {
		String[] data={"123456", "123456", "123456"};
		assertEquals("123456123456123456", imp.obtenerNombreArchivo(data));
	}
	
	@Test
	public void testObtenerNombreArchivo_vscio() {
		String[] data={""};
		assertEquals("", imp.obtenerNombreArchivo(data));
	}
	
	@Test
	public void testObtenerNombreArchivo_null() {
		String[] data={};
		assertEquals("", imp.obtenerNombreArchivo(data));
	}

}
