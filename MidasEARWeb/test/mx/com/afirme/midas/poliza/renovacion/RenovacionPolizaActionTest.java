package mx.com.afirme.midas.poliza.renovacion;

import static org.junit.Assert.*;

import java.util.LinkedHashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;


public class RenovacionPolizaActionTest {
	private RenovacionPolizaAction tested;

	@Before
	public void setUp() throws Exception {
		tested = new RenovacionPolizaAction();
	}

	@Test
	public void testObtenerMensaje() {
		Map<String, String> mesajesError = new LinkedHashMap<String, String>();
		mesajesError.put("1", "valor 1");
		mesajesError.put("2", "valor 2");
		mesajesError.put("3", "valor 3");
		assertEquals("P�liza: 1 Error: valor 1 </br>P�liza: 2 Error: valor 2 </br>P�liza: 3 Error: valor 3 </br>",tested.obtenerMensaje(mesajesError));
	}
	@Test
	public void testObtenerMensaje_null() {
		Map<String, String> mesajesError = new LinkedHashMap<String, String>();
		assertEquals("",tested.obtenerMensaje(mesajesError));
	}

}
