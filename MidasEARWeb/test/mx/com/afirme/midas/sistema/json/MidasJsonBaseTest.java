package mx.com.afirme.midas.sistema.json;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.ibm.u2.jdbc.stringUtil;

public class MidasJsonBaseTest {
	private MidasJsonBase tested;
	private MidasJsonRow row;
	@Before
	public void setUp(){
		tested = new MidasJsonBase();
		row = new MidasJsonRow();		
	}
	@Test
	public void testToString() {
		ArrayList<MidasJsonRow> lis=new ArrayList<MidasJsonRow>() ;
		row.setId("3");
		row.setDatos("ejemplo3","ejemplo4");
		row.getId();
		row.getDatos();
		lis.add(row);
		tested.setRows(lis);
		String res="{rows:[{id:\"3\",data:[\"ejemplo3\",\"ejemplo4\"]}]}";
		assertEquals(res, tested.toString());
	}
	@Test
	public void testToString_vacio() {
	assertEquals("{rows:[]}", tested.toString());
	}
	@Test
	public void testToString_null() {
		tested.setRows(null);
		String res="{rows:[]}";
		assertEquals(res, tested.toString());
	}

	@Test
	public void testToXMLString() {
		ArrayList<MidasJsonRow> lis=new ArrayList<MidasJsonRow>() ;
		row.setId("3");
		row.setDatos("ejemplo3","ejemplo4");
		row.getId();
		row.getDatos();
		lis.add(row);
		tested.setRows(lis);
		assertEquals("<rows><row id=\"3\"><cell>ejemplo3</cell><cell>ejemplo4</cell></row></rows>", tested.toXMLString());
	}
	@Test
	public void testToXMLString_null() {
		tested.setRows(null);
		assertEquals("<rows></rows>", tested.toXMLString());
	
	}
	@Test
	public void testToXMLString_vacio() {
		assertEquals("<rows></rows>", tested.toXMLString());
	
	}

	@Test
	public void testToCSVString_rowvacio() {
		assertEquals("", tested.toCSVString(","));
	
	}
	@Test
	public void testToCSVString() {
		ArrayList<MidasJsonRow> lis=new ArrayList<MidasJsonRow>() ;
		row.setId("3");
		row.setDatos("ejemplo3","ejemplo4");
		row.getId();
		row.getDatos();
		lis.add(row);
		tested.setRows(lis);
		assertEquals("3,ejemplo3,ejemplo4\n", tested.toCSVString(","));
	
	}
	@Test
	public void testToCSVString_null() {
		ArrayList<MidasJsonRow> lis=new ArrayList<MidasJsonRow>() ;
		tested.setRows(null);
		assertEquals("", tested.toCSVString(","));
	
	}

}
