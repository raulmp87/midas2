package mx.com.afirme.midas.sistema.json;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

public class MidasJsonRowTest {

	private MidasJsonRow tested;
	@Before
	public void setUp(){
		
		tested = new MidasJsonRow();	

	}
	@Test
	public void testToString() {
		ArrayList<MidasJsonRow> lis=new ArrayList<MidasJsonRow>() ;
		tested.setId("3");
		tested.setDatos("ejemplo3","ejemplo4");
		tested.getId();
		tested.getDatos();
		lis.add(tested);	
		String res="{id:\"3\",data:[\"ejemplo3\",\"ejemplo4\"]}";
		assertEquals(res, tested.toString());
	}

	@Test
	public void testToString_null() {
		ArrayList<MidasJsonRow> lis=new ArrayList<MidasJsonRow>() ;
		lis.add(null);	
		String res="{id:\"null\",data:[]}";
		assertEquals(res, tested.toString());
	}

	@Test
	public void testToXMLString() {
		ArrayList<MidasJsonRow> lis=new ArrayList<MidasJsonRow>() ;
		tested.setId("3");
		tested.setDatos("ejemplo3","ejemplo4");
		tested.getId();
		tested.getDatos();
		lis.add(tested);	
		String res="<row id=\"3\"><cell>ejemplo3</cell><cell>ejemplo4</cell></row>";
		assertEquals(res, tested.toXMLString());
	}
	@Test
	public void testToXMLString_null() {
		ArrayList<MidasJsonRow> lis=new ArrayList<MidasJsonRow>() ;
		lis.add(null);	
		String res="<row id=\"null\"></row>";
		assertEquals(res, tested.toXMLString());
	}

	@Test
	public void testToCSVString() {
		ArrayList<MidasJsonRow> lis=new ArrayList<MidasJsonRow>() ;
		tested.setId("3");
		tested.setDatos("ejemplo3","ejemplo4");
		tested.getId();
		tested.getDatos();
		lis.add(tested);	
		String res="3,ejemplo3,ejemplo4";
		assertEquals(res, tested.toCSVString(","));
	}


	@Test
	public void testToCSVString_null() {
		ArrayList<MidasJsonRow> lis=new ArrayList<MidasJsonRow>() ;
		lis.add(null);	
		String res="null,";
		assertEquals(res, tested.toCSVString(","));
	}
}
