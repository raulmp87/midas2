package mx.com.afirme.midas.sistema.gestionPendientes;

import static org.junit.Assert.*;

import java.math.BigDecimal;

import org.junit.Before;
import org.junit.Test;

public class GestorPendientesDanosTest {
	GestorPendientesDanos tested;

	@Before
	public void setUp() throws Exception {
		tested = new GestorPendientesDanos();
	}

	@Test
	public void testFormateaIdCotizacionMenorAOcho() {
		assertEquals("00000123", tested.formateaIdCotizacion(BigDecimal.valueOf(123)));
	}
	@Test
	public void testFormateaIdCotizacionOcho() {
		assertEquals("12345678", tested.formateaIdCotizacion(BigDecimal.valueOf(12345678)));
	}
	@Test
	public void testFormateaIdCotizacionMayorAOcho() {
		assertEquals("1234567890", tested.formateaIdCotizacion(BigDecimal.valueOf(1234567890)));
	}

}
