package mx.com.afirme.midas.sistema;

import static org.junit.Assert.*;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;

import org.junit.Before;
import org.junit.Test;


public class UtileriasWebTest {
	private UtileriasWeb utilerias;
	@Before
	public void setUp(){
	  utilerias = new UtileriasWeb();
	}
	@Test
	public void testLlenarIzquierda() {
		String coberturaCompuesta = "uno,dos,tres";
		assertEquals("-uno,dos,tres", utilerias.llenarIzquierda(coberturaCompuesta, "-", 13));
	}
	@Test
	public void testLlenarIzquierda_null() {
		String coberturaCompuesta = "";
		assertEquals("...", utilerias.llenarIzquierda(coberturaCompuesta, ".", 3));
	}
	
	
	
/*
	@Test
	public void obtenerNombreEIpDelServidor() throws IOException {
		 InetAddress addr = InetAddress.getLocalHost () ;
	        // Tomar la  IP 
	         byte [] ip = addr.getAddress () ;
		assertEquals(utilerias.obtenerIpServidor(ip), utilerias.obtenerIpServidor(ip));

	}*/
	
}
