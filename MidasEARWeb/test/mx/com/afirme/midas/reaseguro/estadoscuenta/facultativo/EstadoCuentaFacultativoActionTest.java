package mx.com.afirme.midas.reaseguro.estadoscuenta.facultativo;

import static org.junit.Assert.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import mx.com.afirme.midas.catalogos.ramo.SubRamoDTO;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.combos.etiqueta.Estado;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

import org.junit.Before;
import org.junit.Test;

public class EstadoCuentaFacultativoActionTest {

	EstadoCuentaFacultativoAction tested;
	SubRamoDTO SubRamoDTO;
	@Before
	public void setUp() throws Exception {
		tested = new EstadoCuentaFacultativoAction();
		SubRamoDTO = new SubRamoDTO();
	}

	@Test
	public void testObtenerDescripcionSubRamosDTO() {
		List<SubRamoDTO> listaSubRamosIncluidos = new ArrayList<SubRamoDTO>();
		SubRamoDTO.setDescripcionSubRamo("ejemplo");
		listaSubRamosIncluidos.add(SubRamoDTO);
		SubRamoDTO = new SubRamoDTO();
		SubRamoDTO.setDescripcionSubRamo("ejemplo 2");
		listaSubRamosIncluidos.add(SubRamoDTO);
		assertEquals("ejemplo, ejemplo 2", tested.obtenerDescripcionSubRamosDTO(listaSubRamosIncluidos ));
	}
	@Test
	public void testObtenerDescripcionSubRamosDTO_null() {
		List<SubRamoDTO> listaSubRamosIncluidos = new ArrayList<SubRamoDTO>();
		assertEquals("", tested.obtenerDescripcionSubRamosDTO(listaSubRamosIncluidos ));
	}

}
