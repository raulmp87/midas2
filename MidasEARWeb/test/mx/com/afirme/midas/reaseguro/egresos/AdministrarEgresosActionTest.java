package mx.com.afirme.midas.reaseguro.egresos;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class AdministrarEgresosActionTest {
AdministrarEgresosAction tested;
	@Before
	public void setUp() throws Exception {
		tested = new AdministrarEgresosAction();
	}

	@Test
	public void testObtenerIdsCoberturasReaseguradores() {
		String[] idsCobRea = {"ejemplo1-1","ejemplo2-2","ejemplo3-3"};
		assertEquals("(ejemplo1,1),(ejemplo2,2),(ejemplo3,3),", tested.obtenerIdsCoberturasReaseguradores(idsCobRea ));
	}

	@Test
	public void testObtenerIdsCoberturasReaseguradores_null() {
		String[] idsCobRea = {};
		assertEquals("", tested.obtenerIdsCoberturasReaseguradores(idsCobRea ));
	}

}
