package mx.com.afirme.midas.reaseguro.egresos;

import static org.junit.Assert.*;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import mx.com.afirme.midas.catalogos.ramo.SubRamoDTO;
import mx.com.afirme.midas.contratofacultativo.participacionFacultativa.ParticipacionReaseguradorContratoFacultativoDTO;

import org.junit.Before;
import org.junit.Test;

public class EgresoReaseguroDNTest {
	EgresoReaseguroDN tested;
	ParticipacionReaseguradorContratoFacultativoDTO participacion;
	SubRamoDTO subRamoDTO;

	@Before
	public void setUp() throws Exception {
	
		tested = new  EgresoReaseguroDN();
	}

	@Test
	public void testObtenerContenido() {
		List<ParticipacionReaseguradorContratoFacultativoDTO> listaParticipaciones = new ArrayList<ParticipacionReaseguradorContratoFacultativoDTO>();
		listaParticipaciones.add(obtenerparticipacion());	
		SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
		String respuesta = "<tr><td>3276169</td><td>1</td><td>2</td><td>3</td><td>seecion1</td><td>cobertura</td><td>descripcionsubramo</td><td>reasegurador1</td><td>corredor1</td><td>4</td><td>" + formato.format(new Date()) +"</td><td></td></tr>";
		assertEquals(respuesta,tested.obtenerContenido(listaParticipaciones));
	}
	@Test
	public void testObtenerContenido_classnull() {
		List<ParticipacionReaseguradorContratoFacultativoDTO> listaParticipaciones = new ArrayList<ParticipacionReaseguradorContratoFacultativoDTO>();
		participacion = new ParticipacionReaseguradorContratoFacultativoDTO();
		listaParticipaciones.add(participacion);
		String respuesta = "<tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>";
		assertEquals(respuesta,tested.obtenerContenido(listaParticipaciones));
	}
	@Test
	public void testObtenerContenido_null() {
		List<ParticipacionReaseguradorContratoFacultativoDTO> listaParticipaciones = new ArrayList<ParticipacionReaseguradorContratoFacultativoDTO>();
		assertEquals("",tested.obtenerContenido(listaParticipaciones));
	}

	private ParticipacionReaseguradorContratoFacultativoDTO obtenerparticipacion() {
		participacion = new ParticipacionReaseguradorContratoFacultativoDTO();
		participacion.setNumeroPoliza("3276169");
		participacion.setNumeroEndoso(1);
		participacion.setNumeroInciso(2);
		participacion.setNumeroSubInciso(3);
		participacion.setDescripcionSeccion("seecion1");
		participacion.setDescripcionCobertura("cobertura");
		subRamoDTO = new SubRamoDTO();
		subRamoDTO.setDescripcionSubRamo("descripcionsubramo");
		participacion.setSubRamoDTO(subRamoDTO);
		participacion.setNombreReasegurador("reasegurador1");
		participacion.setNombreCorredor("corredor1");
		participacion.setNumeroExhibicion(Short.decode("4"));
		participacion.setFechaPago(new Date());
		return participacion;
	}

}
