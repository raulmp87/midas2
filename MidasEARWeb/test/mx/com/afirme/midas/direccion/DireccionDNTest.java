package mx.com.afirme.midas.direccion;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class DireccionDNTest {
	DireccionDN tested;

	@Before
	public void setUp() throws Exception {
		tested = new DireccionDN();
	}

	@Test
	public void testObtenerIdMenorACinco() {
		assertEquals("00001", tested.obtenerId("1"));
	}
	@Test
	public void testObtenerIdCinco() {
		assertEquals("12345", tested.obtenerId("12345"));
	}@Test
	public void testObtenerIdMayorACinco() {
		assertEquals("12345678", tested.obtenerId("12345678"));
	}
}
