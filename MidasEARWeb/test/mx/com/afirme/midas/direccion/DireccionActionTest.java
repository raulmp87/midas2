package mx.com.afirme.midas.direccion;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class DireccionActionTest {
	DireccionAction tested;

	@Before
	public void setUp() throws Exception {
		tested = new DireccionAction();
	}

	@Test
	public void testObtenerIdMenorACinco() {
		assertEquals("00001", DireccionAction.obtenerId("1"));
	}
	@Test
	public void testObtenerIdCinco() {
		assertEquals("12345", DireccionAction.obtenerId("12345"));
	}
	@Test
	public void testObtenerIdMayorACinco() {
		assertEquals("123456767890", DireccionAction.obtenerId("123456767890"));
	}

}
