package mx.com.afirme.midas.contratos.linea;

import static org.junit.Assert.*;

import java.util.StringTokenizer;

import org.junit.Before;
import org.junit.Test;

public class ConfiguracionLineaDNTest {
	
	ConfiguracionLineaDN linea = new ConfiguracionLineaDN();

	@Before
	public void setUp() throws Exception {
		linea =  new ConfiguracionLineaDN();
	}

	@Test
	public void testObtenerIdLineas() {
		String idLineas ="ejemplo";
		StringTokenizer tokenizer = new StringTokenizer(idLineas,",");
		assertEquals("ejemplo", linea.obtenerIdLineas(tokenizer));
	}
	@Test
	public void testObtenerIdLineas_two() {
		String idLineas ="ejemplo1,ejemplo2";
		StringTokenizer tokenizer = new StringTokenizer(idLineas,",");
		assertEquals("ejemplo1,ejemplo2", linea.obtenerIdLineas(tokenizer));
	}
	
	@Test
	public void testObtenerIdLineas_null() {
		String idLineas ="";
		StringTokenizer tokenizer = new StringTokenizer(idLineas,",");
		assertEquals("", linea.obtenerIdLineas(tokenizer));
	}

}
