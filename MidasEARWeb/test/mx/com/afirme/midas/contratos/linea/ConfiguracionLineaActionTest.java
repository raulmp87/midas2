package mx.com.afirme.midas.contratos.linea;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import mx.com.afirme.midas.catalogos.ramo.RamoDTO;
import mx.com.afirme.midas.catalogos.ramo.SubRamoDTO;

import org.junit.Before;
import org.junit.Test;

public class ConfiguracionLineaActionTest {
	ConfiguracionLineaAction tested;
	SubRamoDTO subRamoDTO;
	RamoDTO ramoDTO;

	@Before
	public void setUp() throws Exception {
		tested = new ConfiguracionLineaAction();
	}

	@Test
	public void testObtenerIdsRamos() {
		Map<String,String> idsSubRamosMap= new LinkedHashMap<String, String>();
		idsSubRamosMap.put("123", "123");
		idsSubRamosMap.put("12", "12");
		idsSubRamosMap.put("1", "1");
		Iterator<Entry<String, String>> it = idsSubRamosMap.entrySet().iterator();
		assertEquals("123,12,1,0", tested.obtenerIdsRamos(it));
	}
	@Test
	public void testObtenerIdsRamos_null() {
		Map<String,String> idsSubRamosMap= new LinkedHashMap<String, String>();
		
		
		Iterator<Entry<String, String>> it = idsSubRamosMap.entrySet().iterator();
		assertEquals("0", tested.obtenerIdsRamos(it));
	}
	
	@Test
	public void testAgregaLineasNegociacionParaRamosRestantes() {
		List<SubRamoDTO> subramos = new  ArrayList<SubRamoDTO>();
		subRamoDTO = new SubRamoDTO();
		ramoDTO = new RamoDTO();
		ramoDTO.setDescripcion("Ramo 1");
		ramoDTO.setIdTcRamo(BigDecimal.ONE);
		subRamoDTO.setRamoDTO(ramoDTO);
		subRamoDTO.setDescripcionSubRamo("subramon1");
		subRamoDTO.setIdTcSubRamo(BigDecimal.ONE);
		subramos.add(subRamoDTO);
		subRamoDTO = new SubRamoDTO();
		ramoDTO = new RamoDTO();
		ramoDTO.setDescripcion("Ramo 1");
		ramoDTO.setIdTcRamo(BigDecimal.TEN);
		subRamoDTO.setRamoDTO(ramoDTO);
		subRamoDTO.setDescripcionSubRamo("subramon1");
		subRamoDTO.setIdTcSubRamo(BigDecimal.TEN);
		subramos.add(subRamoDTO);
		String res = "{\"ejemplo\": {\"id\": \"file\"	,{id:-1,data:[0,,\"Ramo 1\",\"subramon1\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"/MidasWeb/img/blank.gif\",\"/MidasWeb/img/blank.gif\",\"/MidasWeb/img/blank.gif\",\"/MidasWeb/img/blank.gif\",1,1]},{id:-2,data:[0,,\"Ramo 1\",\"subramon1\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"/MidasWeb/img/blank.gif\",\"/MidasWeb/img/blank.gif\",\"/MidasWeb/img/blank.gif\",\"/MidasWeb/img/blank.gif\",10,10]}]}";
		String json = "{\"ejemplo\": {\"id\": \"file\"	}}";
		assertEquals(res, tested.agregaLineasNegociacionParaRamosRestantes(json, subramos));
	}
	
	@Test
	public void testAgregaLineasNegociacionParaRamosRestantes_null() {
		List<SubRamoDTO> subramos = new  ArrayList<SubRamoDTO>();
		String res = "{\"ejemplo\": {\"id\": \"file\"	]}";
		String json = "{\"ejemplo\": {\"id\": \"file\"	}}";
		assertEquals(res, tested.agregaLineasNegociacionParaRamosRestantes(json, subramos));
	}

	
}
