package mx.com.afirme.midas.siniestro.cabina;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

public class ReporteEstadoSiniestroActionTest {
	ReporteEstadoSiniestroAction reporte;

	@Before
	public void setUp() throws Exception {
		reporte = new ReporteEstadoSiniestroAction();
	}

	@Test
	public void testObtenerMensajeError() {
		List<String> mensajesError =new ArrayList<String>();
		mensajesError.add("error 1");
		mensajesError.add("error 2");
		String resul="<br>-error 1<br>-error 2";
		assertEquals(resul, reporte.obtenerMensajeError(mensajesError));

	}
	
	@Test
	public void testObtenerMensajeError_null() {
		List<String> mensajesError =new ArrayList<String>();
		String resul="";
		assertEquals(resul, reporte.obtenerMensajeError(mensajesError));

	}

}
