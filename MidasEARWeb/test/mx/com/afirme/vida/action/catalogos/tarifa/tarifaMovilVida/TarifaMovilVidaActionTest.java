package mx.com.afirme.vida.action.catalogos.tarifa.tarifaMovilVida;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

public class TarifaMovilVidaActionTest {
	TarifaMovilVidaAction tested;

	@Before
	public void setUp() throws Exception {
		tested = new TarifaMovilVidaAction();
	}

	@Test
	public void testObtenerLogErrores() {
		List<String> list = new ArrayList<String>();
		list.add("ejemplo");
		list.add("ejemplo2");
		list.add("ejemplo3");
		list.add("ejemplo4");
		list.add("ejemplo5");
		assertEquals("<br>ejemplo<br>ejemplo2<br>ejemplo3",tested.obtenerLogErrores(list));
	}

	@Test
	public void testObtenerLogErrores_null() {
		List<String> list = new ArrayList<String>();
		tested.setLogErrores("ejemplo error");
		assertEquals("ejemplo error",tested.obtenerLogErrores(list));
	}
	@Test
	public void testObtenerLogErrores_vacios() {
		List<String> list = new ArrayList<String>();
		assertEquals("",tested.obtenerLogErrores(list));
	}
	
}
