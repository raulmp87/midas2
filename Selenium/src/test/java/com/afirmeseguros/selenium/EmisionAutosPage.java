package com.afirmeseguros.selenium;

import static org.junit.Assert.assertTrue;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.Select;

public class EmisionAutosPage {

	@FindBy(how=How.CLASS_NAME, using="titulo")
	private WebElement tituloEmisionAutos;
	
	@FindBy(how=How.ID, using="polizaDTO.numeroPoliza")
	private WebElement numeroPoliza;
	
	@FindBy(how=How.ID, using="polizaDTO.fechaCreacion")
	private WebElement fechaCreacion;
	
	@FindBy(how=How.ID, using="polizaDTO.fechaCreacionHasta")
	private WebElement fechaCreacionHasta;
	
	@FindBy(how=How.ID, using="polizaDTO.numeroPolizaSeycos")
	private WebElement numeroPolizaSeycos;
	
	@FindBy(how=How.ID, using="polizaDTO.cotizacionDTO.nombreContratante")
	private WebElement nombreContratante;
	
	@FindBy(how=How.ID, using="polizaDTO.numeroSerie")
	private WebElement numeroSerie;
	
	@FindBy(how=How.ID, using="agenteNombre")
	private WebElement agenteNombreCombo;
	
	@FindBy(how=How.ID, using="descripcionBusquedaAgente")
	private WebElement descripcionAgente;

	@FindBy(how=How.ID, using="seleccionarAgente")
	private WebElement btnAgenteSeleccionar;

	@FindBy(how=How.ID, using="negocioNombre")
	private WebElement negocioNombreCombo;
	
	@FindBy(how=How.ID, using="descripcionBusquedaNegocio")
	private WebElement descripcionNegocio;

	@FindBy(how=How.ID, using="seleccionarNegocio")
	private WebElement btnseleccionarNegocio;
	
	@FindBy(how=How.ID, using="conflictoNumeroSerie")
	private WebElement checkConflictoNumeroSerie;
	
	@FindBy(how=How.ID, using="polizaDTO.cotizacionDTO.tipoPolizaDTO.claveAplicaFlotillas")
	private WebElement selectTipo;
	
	@FindBy(how=How.ID, using="numeroFolio")
	private WebElement inputNumeroFolio;
	
	@FindBy(how=How.ID, using="polizaDTO.limiteInferiorIncisos")
	private WebElement inputLimiteInferiorIncisos;
	
	@FindBy(how=How.ID, using="polizaDTO.limiteSuperiorIncisos")
	private WebElement inputLimiteSuperiorIncisos;

	@FindBy(how=How.ID, using="submit")
	private WebElement btnBuscar;
	
	@FindBys({@FindBy(id="agregar"), @FindBy(className="btn_back w140"), @FindBy(xpath = "//a[contains(.,' Limpiar ')]")})
	private WebElement btnLimpiar; 

	@FindBy(how=How.ID, using="polizaGrid")
	private WebElement polizaGrid;
	
	@FindBy(xpath = "//tr[contains(@class, 'ev_light')]")
	private WebElement polizaGridTr;
	
	@FindBy(xpath = "//img[@src='../img/icons/ico_verdetalle.gif']")
	private WebElement verDetalle;

	@FindBy(xpath= "//img[contains(@title,'Consultar')]")
	private WebElement consultar;
	
	@FindBy(how=How.XPATH, using="//div[contains(.,'Consulta de Póliza')]")
	private WebElement tituloConsultaPoliza;
	
	
	//Getters Setters
	
	public WebElement getAgenteNombreCombo() {
		return agenteNombreCombo;
	}

	public void setAgenteNombreCombo(WebElement agenteNombreCombo) {
		this.agenteNombreCombo = agenteNombreCombo;
	}

	public WebElement getDescripcionAgente() {
		return descripcionAgente;
	}

	public void setDescripcionAgente(WebElement descripcionAgente) {
		this.descripcionAgente = descripcionAgente;
	}

	public WebElement getBtnAgenteSeleccionar() {
		return btnAgenteSeleccionar;
	}

	public void setBtnAgenteSeleccionar(WebElement btnAgenteSeleccionar) {
		this.btnAgenteSeleccionar = btnAgenteSeleccionar;
	}

	public WebElement getNegocioNombreCombo() {
		return negocioNombreCombo;
	}

	public void setNegocioNombreCombo(WebElement negocioNombreCombo) {
		this.negocioNombreCombo = negocioNombreCombo;
	}

	public WebElement getDescripcionNegocio() {
		return descripcionNegocio;
	}

	public void setDescripcionNegocio(WebElement descripcionNegocio) {
		this.descripcionNegocio = descripcionNegocio;
	}

	public WebElement getBtnseleccionarNegocio() {
		return btnseleccionarNegocio;
	}

	public void setBtnseleccionarNegocio(WebElement btnseleccionarNegocio) {
		this.btnseleccionarNegocio = btnseleccionarNegocio;
	}

	public WebElement getCheckConflictoNumeroSerie() {
		return checkConflictoNumeroSerie;
	}

	public void setCheckConflictoNumeroSerie(WebElement checkConflictoNumeroSerie) {
		this.checkConflictoNumeroSerie = checkConflictoNumeroSerie;
	}

	public WebElement getBtnBuscar() {
		return btnBuscar;
	}

	public void setBtnBuscar(WebElement btnBuscar) {
		this.btnBuscar = btnBuscar;
	}

	public WebElement getBtnLimpiar() {
		return btnLimpiar;
	}

	public void setBtnLimpiar(WebElement btnLimpiar) {
		this.btnLimpiar = btnLimpiar;
	}

	public WebElement getPolizaGridTr() {
		return polizaGridTr;
	}

	public void setPolizaGridTr(WebElement polizaGridTr) {
		this.polizaGridTr = polizaGridTr;
	}

	public WebElement getVerDetalle() {
		return verDetalle;
	}

	public void setVerDetalle(WebElement verDetalle) {
		this.verDetalle = verDetalle;
	}

	public WebElement getConsultar() {
		return consultar;
	}

	public void setConsultar(WebElement consultar) {
		this.consultar = consultar;
	}
	
	
	public void validateTitleListado(){
		assertTrue("Listado de Póliza Autos".equals(tituloEmisionAutos.getText()));
		System.out.println(tituloEmisionAutos.getText());
	}
	
	public void validateTitleConsultaPoliza(){
		assertTrue("Consulta de Póliza".equals(tituloConsultaPoliza.getText()));
		System.out.println(tituloConsultaPoliza.getText());
	}
	
	public void sendKeysNumeroPoliza(String numPol){
		numeroPoliza.sendKeys(numPol);
	}
	
	public void sendKeysfechaCreacion(String dia, String mes, String anio){
		fechaCreacion.sendKeys(dia);
		fechaCreacion.sendKeys(mes);
		fechaCreacion.sendKeys(anio);
	}
	
	public void sendKeysfechaCreacionHasta(String dia, String mes, String anio){
		fechaCreacionHasta.sendKeys(dia);
		fechaCreacionHasta.sendKeys(mes);
		fechaCreacionHasta.sendKeys(anio);
	}
	
	public void sendKeysNumeroPolizaSeycos(String numPolSey){
		numeroPolizaSeycos.sendKeys(numPolSey);
	}
	
	public void sendKeysNombreContratante(String nombreContr){
		nombreContratante.sendKeys(nombreContr);
	}
	
	public void sendKeysnumeroSerie(String numSerie){
		numeroSerie.sendKeys(numSerie);
	}
	
	public void selectTipo(String tipo){
		Select dropdown = new Select(selectTipo);
		dropdown.selectByVisibleText(tipo);
	}
	
	public void inputNumeroFolio(String folio){
		inputNumeroFolio.sendKeys(folio);
	}
	
	public void inputLimiteInferiorIncisos(String inferiorIncisos){
		inputLimiteInferiorIncisos.sendKeys(inferiorIncisos);
	}
	
	public void inputLimiteSuperiorIncisos(String superiorIncisos){
		inputLimiteSuperiorIncisos.sendKeys(superiorIncisos);
	}
	
	public void gridPolizas(){
		polizaGrid.isDisplayed();
	}
	
}
