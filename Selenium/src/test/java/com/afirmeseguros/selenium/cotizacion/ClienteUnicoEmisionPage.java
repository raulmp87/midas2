package com.afirmeseguros.selenium.cotizacion;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.How;

import com.afirmeseguros.selenium.BasicTest;

public class ClienteUnicoEmisionPage extends BasicTest {

	public WebElement getPrimaEstimadaInput() {
		return primaEstimadaInput;
	}

	public void setPrimaEstimadaInput(WebElement primaEstimadaInput) {
		this.primaEstimadaInput = primaEstimadaInput;
	}

	public WebElement getPesosMnRb() {
		return pesosMnRb;
	}

	public void setPesosMnRb(WebElement pesosMnRb) {
		this.pesosMnRb = pesosMnRb;
	}

	public WebElement getDolaresUsRb() {
		return dolaresUsRb;
	}

	public void setDolaresUsRb(WebElement dolaresUsRb) {
		this.dolaresUsRb = dolaresUsRb;
	}

	public WebElement getNumSerieFielInput() {
		return numSerieFielInput;
	}

	public void setNumSerieFielInput(WebElement numSerieFielInput) {
		this.numSerieFielInput = numSerieFielInput;
	}

	public WebElement getPepTrueRb() {
		return pepTrueRb;
	}

	public void setPepTrueRb(WebElement pepTrueRb) {
		this.pepTrueRb = pepTrueRb;
	}

	public WebElement getPepFalseRb() {
		return pepFalseRb;
	}

	public void setPepFalseRb(WebElement pepFalseRb) {
		this.pepFalseRb = pepFalseRb;
	}

	public WebElement getParentescoInput() {
		return parentescoInput;
	}

	public void setParentescoInput(WebElement parentescoInput) {
		this.parentescoInput = parentescoInput;
	}

	public WebElement getNombrePuestoInput() {
		return nombrePuestoInput;
	}

	public void setNombrePuestoInput(WebElement nombrePuestoInput) {
		this.nombrePuestoInput = nombrePuestoInput;
	}

	public WebElement getCuentaPropiaSiRb() {
		return cuentaPropiaSiRb;
	}

	public void setCuentaPropiaSiRb(WebElement cuentaPropiaSiRb) {
		this.cuentaPropiaSiRb = cuentaPropiaSiRb;
	}

	public WebElement getCuentaPropiaNoRb() {
		return cuentaPropiaNoRb;
	}

	public void setCuentaPropiaNoRb(WebElement cuentaPropiaNoRb) {
		this.cuentaPropiaNoRb = cuentaPropiaNoRb;
	}

	public WebElement getRepresentacionSiRb() {
		return representacionSiRb;
	}

	public void setRepresentacionSiRb(WebElement representacionSiRb) {
		this.representacionSiRb = representacionSiRb;
	}

	public WebElement getRepresentacionNoRb() {
		return representacionNoRb;
	}

	public void setRepresentacionNoRb(WebElement representacionNoRb) {
		this.representacionNoRb = representacionNoRb;
	}

	public WebElement getNomRepresentadoInput() {
		return nomRepresentadoInput;
	}

	public void setNomRepresentadoInput(WebElement nomRepresentadoInput) {
		this.nomRepresentadoInput = nomRepresentadoInput;
	}

	public WebElement getCancelarBtn() {
		return cancelarBtn;
	}

	public void setCancelarBtn(WebElement cancelarBtn) {
		this.cancelarBtn = cancelarBtn;
	}

	public WebElement getGuardarBtn() {
		return guardarBtn;
	}

	public void setGuardarBtn(WebElement guardarBtn) {
		this.guardarBtn = guardarBtn;
	}

	@FindBy(how = How.ID, using = "entrevista.primaEstimada")
	private WebElement primaEstimadaInput;
	
	@FindBy(how = How.ID, using = "entrevista.moneda484")
	private WebElement pesosMnRb;
	
	@FindBy(how = How.ID, using = "entrevista.moneda840")
	private WebElement dolaresUsRb;
	
	@FindBy(how = How.ID, using = "entrevista.numeroSerieFiel")
	private WebElement numSerieFielInput;
	
	@FindBy(how = How.ID, using = "entrevista.peptrue")
	private WebElement pepTrueRb;
	
	@FindBy(how = How.ID, using = "entrevista.pepfalse")
	private WebElement pepFalseRb;
	
	@FindBy(how = How.ID, using = "entrevista.parentesco")
	private WebElement parentescoInput;
	
	@FindBy(how = How.ID, using = "entrevista.nombrePuesto")
	private WebElement nombrePuestoInput;
	
	@FindBy(how = How.ID, using = "entrevista.cuentaPropiatrue")
	private WebElement cuentaPropiaSiRb;
	
	@FindBy(how = How.ID, using = "entrevista.cuentaPropiafalse")
	private WebElement cuentaPropiaNoRb;
	
	@FindBy(how = How.ID, using = "entrevista.documentoRepresentaciontrue")
	private WebElement representacionSiRb;
	
	@FindBy(how = How.ID, using = "entrevista.documentoRepresentacionfalse")
	private WebElement representacionNoRb;
	
	@FindBy(how = How.ID, using = "entrevista.nombreRepresentado")
	private WebElement nomRepresentadoInput;
	
	@FindBys({@FindBy(how = How.ID, using = "botonEmision"), @FindBy(how = How.XPATH, using = "//a[contains(.,'Cancelar ')]")})
	private WebElement cancelarBtn;
	
	@FindBys({@FindBy(how = How.ID, using = "botonEmision"), @FindBy(how = How.XPATH, using = "//a[contains(.,'Guardar ')]")})
	private WebElement guardarBtn;
	
	@FindBy(how = How.CLASS_NAME, using = "entrevistaForm")
	private WebElement entrevistaForm;

	public WebElement getEntrevistaForm() {
		return entrevistaForm;
	}

	public void setEntrevistaForm(WebElement entrevistaForm) {
		this.entrevistaForm = entrevistaForm;
	}
	
}
