package com.afirmeseguros.selenium.cotizacion;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import com.afirmeseguros.selenium.BasicTest;

public class SeleccionAgentePage extends BasicTest {

	@FindBy(how = How.ID, using = "descripcionBusquedaAgente")
	private WebElement descripcionBusquedaAgenteInput;
	
	@FindBy(how = How.ID, using = "seleccionarAgente")
	private WebElement seleccionarAgenteBtn;
	
	@FindBy(how = How.XPATH, using = "//a[contains(.,'FEDERICO DE HOYOS NAVARRO - 92714')]")
	private WebElement agenteSeleccionado;

	//----------------------------------GETTERS & SETTERS------------------------------------//

	public WebElement getAgenteSeleccionado() {
		return agenteSeleccionado;
	}

	public void setAgenteSeleccionado(WebElement agenteSeleccionado) {
		this.agenteSeleccionado = agenteSeleccionado;
	}

	public WebElement getSeleccionarAgenteBtn() {
		return seleccionarAgenteBtn;
	}

	public void setSeleccionarAgenteBtn(WebElement seleccionarAgenteBtn) {
		this.seleccionarAgenteBtn = seleccionarAgenteBtn;
	}
	
	//---------------------------------------------------------------------------------------//
	
	public void sendKeysAgenteBusquda(String agenteNombre){
		descripcionBusquedaAgenteInput.sendKeys(agenteNombre);
	}

	
	

}
