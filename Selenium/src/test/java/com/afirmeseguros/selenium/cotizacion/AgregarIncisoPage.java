package com.afirmeseguros.selenium.cotizacion;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.afirmeseguros.selenium.BasicTest;

public class AgregarIncisoPage extends BasicTest{
	
	protected static WebDriverWait waitEmi;

	@FindBy(how = How.ID, using = "incisoCotizacion.incisoAutoCot.estadoId")
	private WebElement estadoSelect;
	
	@FindBy(how = How.ID, using = "incisoCotizacion.incisoAutoCot.municipioId")
	private WebElement municipioSelect;
	
	@FindBy(how = How.ID, using = "incisoCotizacion.incisoAutoCot.negocioSeccionId")
	private WebElement lineaNegocioSelect;
	
	@FindBy(how = How.ID, using = "incisoCotizacion.incisoAutoCot.marcaId")
	private WebElement marcaSelect;
	
	@FindBy(how = How.ID, using = "incisoCotizacion.incisoAutoCot.estiloId")
	private WebElement estiloSelect;
	
	@FindBy(how = How.ID, using = "incisoCotizacion.incisoAutoCot.modeloVehiculo")
	private WebElement modeloSelect;
	
	@FindBy(how = How.ID, using = "ajustarCaracteristicas")
	private WebElement ajustarCaracteristicasBtn;
	
	@FindBy(how = How.ID, using = "incisoCotizacion.incisoAutoCot.tipoUsoId")
	private WebElement tipoUsoSelect;
	
	@FindBy(how = How.ID, using = "incisoCotizacion.incisoAutoCot.negocioPaqueteId")
	private WebElement paqueteSelect;
	
	@FindBy(how = How.ID, using = "incisoCotizacion.incisoAutoCot.numeroSerie")
	private WebElement numSerieInput;
	
	@FindBy(how = How.ID, using = "mostrarInfvehicularBtn")
	private WebElement validarBtn;
	
	@FindBy(how = How.ID, using = "btnRecalcular")
	private WebElement calcularBtn;
	
	@FindBy(how = How.ID, using = "btnGuardar")
	private WebElement guardarBtn;
	
	@FindBy(how = How.XPATH, using = "//a[contains(.,'Cancelar')]")
	private WebElement cancelarBtn;

	//----------------------------------------------------------------------------------------//
	
	public void clickElementEmitirBtn(WebElement element){
		waitEmi = new WebDriverWait(driver, 70);
		waitEmi.until(ExpectedConditions.visibilityOf(element));
		element.click();
	}
	
	
	//----------------------------------GETTERS & SETTERS------------------------------------//
	
	public WebElement getEstadoSelect() {
		return estadoSelect;
	}

	public void setEstadoSelect(WebElement estadoSelect) {
		this.estadoSelect = estadoSelect;
	}

	public WebElement getMunicipioSelect() {
		return municipioSelect;
	}

	public void setMunicipioSelect(WebElement municipioSelect) {
		this.municipioSelect = municipioSelect;
	}

	public WebElement getLineaNegocioSelect() {
		return lineaNegocioSelect;
	}

	public void setLineaNegocioSelect(WebElement lineaNegocioSelect) {
		this.lineaNegocioSelect = lineaNegocioSelect;
	}

	public WebElement getMarcaSelect() {
		return marcaSelect;
	}

	public void setMarcaSelect(WebElement marcaSelect) {
		this.marcaSelect = marcaSelect;
	}

	public WebElement getEstiloSelect() {
		return estiloSelect;
	}

	public void setEstiloSelect(WebElement estiloSelect) {
		this.estiloSelect = estiloSelect;
	}

	public WebElement getModeloSelect() {
		return modeloSelect;
	}

	public void setModeloSelect(WebElement modeloSelect) {
		this.modeloSelect = modeloSelect;
	}

	public WebElement getTipoUsoSelect() {
		return tipoUsoSelect;
	}

	public void setTipoUsoSelect(WebElement tipoUsoSelect) {
		this.tipoUsoSelect = tipoUsoSelect;
	}

	public WebElement getPaqueteSelect() {
		return paqueteSelect;
	}

	public void setPaqueteSelect(WebElement paqueteSelect) {
		this.paqueteSelect = paqueteSelect;
	}

	public WebElement getNumSerieInput() {
		return numSerieInput;
	}

	public void setNumSerieInput(WebElement numSerieInput) {
		this.numSerieInput = numSerieInput;
	}

	public WebElement getAjustarCaracteristicasBtn() {
		return ajustarCaracteristicasBtn;
	}

	public void setAjustarCaracteristicasBtn(WebElement ajustarCaracteristicasBtn) {
		this.ajustarCaracteristicasBtn = ajustarCaracteristicasBtn;
	}

	public WebElement getValidarBtn() {
		return validarBtn;
	}

	public void setValidarBtn(WebElement validarBtn) {
		this.validarBtn = validarBtn;
	}

	public WebElement getCalcularBtn() {
		return calcularBtn;
	}

	public void setCalcularBtn(WebElement calcularBtn) {
		this.calcularBtn = calcularBtn;
	}

	public WebElement getGuardarIconBtn() {
		return guardarBtn;
	}

	public void setGuardarIconBtn(WebElement guardarIconBtn) {
		this.guardarBtn = guardarIconBtn;
	}

	public WebElement getCancelarBtn() {
		return cancelarBtn;
	}

	public void setCancelarBtn(WebElement cancelarBtn) {
		this.cancelarBtn = cancelarBtn;
	}
	
}
