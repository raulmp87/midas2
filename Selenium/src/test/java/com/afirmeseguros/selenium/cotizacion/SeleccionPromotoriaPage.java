package com.afirmeseguros.selenium.cotizacion;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import com.afirmeseguros.selenium.BasicTest;

public class SeleccionPromotoriaPage extends BasicTest {

	@FindBy(how = How.ID, using = "descripcionBusquedaPromotoria")
	private WebElement descripcionBusquedaPromotoriaInput;
	
	@FindBy(how = How.ID, using = "seleccionarPromotoria")
	private WebElement seleccionarPromotoriaBtn;

	public WebElement getSeleccionarPromotoriaBtn() {
		return seleccionarPromotoriaBtn;
	}

	public void setSeleccionarPromotoriaBtn(WebElement seleccionarPromotoriaBtn) {
		this.seleccionarPromotoriaBtn = seleccionarPromotoriaBtn;
	}
	
	

}
