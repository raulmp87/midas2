package com.afirmeseguros.selenium.cotizacion;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class ComplementarDatosAseguradoPage {

	@FindBy(how = How.ID, using = "radioAsegurado1")
	private WebElement datosIgualContratanteRb;
	
	@FindBy(how = How.ID, using = "radioAsegurado2")
	private WebElement soloNombreRb;
	
	@FindBy(how = How.ID, using = "radioAsegurado3")
	private WebElement clienteExistenteRb;
	
	@FindBy(how = How.ID, using = "nombreAsegurado")
	private WebElement nombreAseguradoInput;
	
	@FindBy(how = How.ID, using = "divGuardar")
	private WebElement divGuardarBtn;
	
	@FindBy(how = How.ID, using = "divSalirBtn")
	private WebElement divSalirBtn;

	//----------------------------------GETTERS & SETTERS------------------------------------//
	
	public WebElement getDatosIgualContratanteRb() {
		return datosIgualContratanteRb;
	}

	public void setDatosIgualContratanteRb(WebElement datosIgualContratanteRb) {
		this.datosIgualContratanteRb = datosIgualContratanteRb;
	}

	public WebElement getSoloNombreRb() {
		return soloNombreRb;
	}

	public void setSoloNombreRb(WebElement soloNombreRb) {
		this.soloNombreRb = soloNombreRb;
	}

	public WebElement getClienteExistenteRb() {
		return clienteExistenteRb;
	}

	public void setClienteExistenteRb(WebElement clienteExistenteRb) {
		this.clienteExistenteRb = clienteExistenteRb;
	}

	public WebElement getDivGuardarBtn() {
		return divGuardarBtn;
	}

	public void setDivGuardarBtn(WebElement divGuardarBtn) {
		this.divGuardarBtn = divGuardarBtn;
	}

	public WebElement getDivSalirBtn() {
		return divSalirBtn;
	}

	public void setDivSalirBtn(WebElement divSalirBtn) {
		this.divSalirBtn = divSalirBtn;
	}
	
	public WebElement getNombreAseguradoInput() {
		return nombreAseguradoInput;
	}

	public void setNombreAseguradoInput(WebElement nombreAseguradoInput) {
		this.nombreAseguradoInput = nombreAseguradoInput;
	}
	
}
