package com.afirmeseguros.selenium.cotizacion;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class CobranzaPage {
	
	@FindBy(how = How.ID, using = "medioPagoDTOs")
	private WebElement medioPagoDTOsSelect;

	public WebElement getMedioPagoDTOsSelect() {
		return medioPagoDTOsSelect;
	}

	public void setMedioPagoDTOsSelect(WebElement medioPagoDTOsSelect) {
		this.medioPagoDTOsSelect = medioPagoDTOsSelect;
	}

	@FindBy(how = How.XPATH, using = "//a[@onclick='guardarCobranza();']")
	private WebElement guardarCobranzaBtn;

	public WebElement getGuardarCobranzaBtn() {
		return guardarCobranzaBtn;
	}

	public void setGuardarCobranzaBtn(WebElement guardarCobranzaBtn) {
		this.guardarCobranzaBtn = guardarCobranzaBtn;
	}
	
}
