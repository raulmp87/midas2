package com.afirmeseguros.selenium.cotizacion;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.afirmeseguros.selenium.BasicTest;

public class ComplementarCotizacionPage extends BasicTest {
	
	protected static WebDriverWait waitEmi;

	@FindBy(how = How.CLASS_NAME, using = "icon_cliente")
	private WebElement buscarCliente;
	
	@FindBy(how = How.ID, using = "tipoImpresionCliente")
	private WebElement tipoImpresionClienteSelect;
	
	@FindBy(how = How.ID, using = "previoRecibos")
	private WebElement previoRecibosBtn;
	
	@FindBy(how = How.ID, using = "enviaEmail")
	private WebElement enviaEmailBtn;
	
	@FindBy(how = How.ID, using = "imprimeCotizacion")
	private WebElement imprimeCotizacionBtn;
	
	@FindBy(how = How.XPATH, using = "//a[contains(.,'Regresar ')]")
	private WebElement regresarBtn;
	
	@FindBy(how = How.XPATH, using = "//a[contains(.,'Actualizar ')]")
	private WebElement actalizarBtn;
	
	@FindBy(how = How.XPATH, using = "//a[contains(.,' Salir ')]")
	private WebElement salirBtn;
	

	//----------------------------------------------------------------------------------------//
	
	@FindBy(how = How.XPATH, using = "//img[@src='../img/icons/ico_agregar.gif']")
	private WebElement agregarDatosIncisoIco;
	
	@FindBy(how = How.XPATH, using = "//img[@src='../img/persona.jpg']")
	private WebElement verDetalleAseguradoIco;
	
	@FindBy(how = How.XPATH, using = "//img[@src='../img/listsicon.gif']")
	private WebElement condicionesEspecialesIco; 
		
	
	//----------------------------------------------------------------------------------------//
	
	@FindBy(how = How.XPATH, using = "//span[text()='Cobranza']")
	private WebElement cobranzaTab;
	
	@FindBy(how = How.XPATH, using = "//span[text()='Complem. Emisión']")
	private WebElement complementarEmisionTab;
	
	//----------------------------------------------------------------------------------------//
	
	//@FindBy(how = How.XPATH, using ="//a[contains(.,' Emitir ')]")
	//private WebElement emitirBtn;
	
	@FindBy(how = How.ID, using ="botonEmision")
	private WebElement emitirBtn;
	
	
	//----------------------------------GETTERS & SETTERS------------------------------------//

	public WebElement getBuscarCliente() {
		return buscarCliente;
	}

	public void setBuscarCliente(WebElement buscarCliente) {
		this.buscarCliente = buscarCliente;
	}

	public WebElement getTipoImpresionClienteSelect() {
		return tipoImpresionClienteSelect;
	}

	public void setTipoImpresionClienteSelect(WebElement tipoImpresionClienteSelect) {
		this.tipoImpresionClienteSelect = tipoImpresionClienteSelect;
	}

	public WebElement getPrevioRecibosBtn() {
		return previoRecibosBtn;
	}

	public void setPrevioRecibosBtn(WebElement previoRecibosBtn) {
		this.previoRecibosBtn = previoRecibosBtn;
	}

	public WebElement getEnviaEmailBtn() {
		return enviaEmailBtn;
	}

	public void setEnviaEmailBtn(WebElement enviaEmailBtn) {
		this.enviaEmailBtn = enviaEmailBtn;
	}

	public WebElement getImprimeCotizacionBtn() {
		return imprimeCotizacionBtn;
	}

	public void setImprimeCotizacionBtn(WebElement imprimeCotizacionBtn) {
		this.imprimeCotizacionBtn = imprimeCotizacionBtn;
	}

	public WebElement getRegresarBtn() {
		return regresarBtn;
	}

	public void setRegresarBtn(WebElement regresarBtn) {
		this.regresarBtn = regresarBtn;
	}

	public WebElement getActalizarBtn() {
		return actalizarBtn;
	}

	public void setActalizarBtn(WebElement actalizarBtn) {
		this.actalizarBtn = actalizarBtn;
	}

	public WebElement getSalirBtn() {
		return salirBtn;
	}

	public void setSalirBtn(WebElement salirBtn) {
		this.salirBtn = salirBtn;
	}

	public WebElement getAgregarDatosIncisoIco() {
		return agregarDatosIncisoIco;
	}

	public void setAgregarDatosIncisoIco(WebElement agregarDatosIncisoIco) {
		this.agregarDatosIncisoIco = agregarDatosIncisoIco;
	}

	public WebElement getVerDetalleAseguradoIco() {
		return verDetalleAseguradoIco;
	}

	public void setVerDetalleAseguradoIco(WebElement verDetalleAseguradoIco) {
		this.verDetalleAseguradoIco = verDetalleAseguradoIco;
	}

	public WebElement getCondicionesEspecialesIco() {
		return condicionesEspecialesIco;
	}

	public void setCondicionesEspecialesIco(WebElement condicionesEspecialesIco) {
		this.condicionesEspecialesIco = condicionesEspecialesIco;
	}

	public WebElement getCobranzaTab() {
		return cobranzaTab;
	}

	public void setCobranzaTab(WebElement cobranzaTab) {
		this.cobranzaTab = cobranzaTab;
	}

	public WebElement getComplementarEmisionTab() {
		return complementarEmisionTab;
	}

	public void setComplementarEmisionTab(WebElement complementarEmisionTab) {
		this.complementarEmisionTab = complementarEmisionTab;
	}

	public WebElement getEmitirBtn() {
		return emitirBtn;
	}

	public void setEmitirBtn(WebElement emitirBtn) {
		this.emitirBtn = emitirBtn;
	}
	
	
	public void clickElementEmitirBtn(WebElement element){
		waitEmi = new WebDriverWait(driver, 70);
		waitEmi.until(ExpectedConditions.visibilityOf(element));
		element.click();
	}
}

	