package com.afirmeseguros.selenium.cotizacion;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import com.afirmeseguros.selenium.BasicTest;

public class ListadoCotizacionesAutosPage extends BasicTest{
	
	@FindBy(how = How.ID, using = "mostrarFiltros")
	private WebElement mostrarFiltrosBtn;
	
	@FindBy(how = How.ID, using = "icon_limpiar")
	private WebElement limpiarBtn;

	@FindBy(how = How.ID, using = "nuevoCotizadorAgentes")
	private WebElement autosIndividualBtn;
	
	@FindBy(how = How.ID, using = "cotizadorAgentes")
	private WebElement cotizadorAgentesBtn;
	
	@FindBy(how = How.XPATH, using = "//a[contains(.,' Cotización Express ')]")
	private WebElement cotizacionExpressBtn;
	
	@FindBy(how = How.XPATH, using = "//a[contains(.,' Individual y Flotillas ')]")
	private WebElement individualFlotillasBtn;
	
	@FindBy(how = How.XPATH, using = "//a[contains(.,' Carga Masiva Individual ')]")
	private WebElement cargaMasivaIndividualBtn;
	
	@FindBy(how = How.XPATH, using = "//a[contains(.,' AutoExpedibles ')]")
	private WebElement AutoExpediblesBtn;
	
	@FindBy(how = How.ID, using = "cotizadorServicioPublico")
	private WebElement cotizadorServicioPublicoBtn;

	@FindBy(how = How.ID, using = "idToCotizacion")
	private WebElement idToCotizacionInput;

	@FindBy(how = How.ID, using = "fechaCreacion")
	private WebElement fechaCreacionInput;	

	@FindBy(how = How.ID, using = "fechaFinal")
	private WebElement fechaFinalInput;	

	@FindBy(how = How.ID, using = "claveEstatus")
	private WebElement claveEstatusSelect;	

	@FindBy(how = How.ID, using = "nombreAsegurado")
	private WebElement nombreAseguradoInput;

	@FindBy(how = How.ID, using = "codigoUsuarioCotizacion")
	private WebElement codigoUsuarioCotizacionInput;

	@FindBy(how = How.ID, using = "agente")
	private WebElement descripcionVehiculoInput;

	@FindBy(how = How.ID, using = "centrosEmisores")
	private WebElement centrosEmisoresSelect;

	@FindBy(how = How.ID, using = "descripcionBusquedaPromotoria")
	private WebElement descripcionBusquedaPromotoriaOF;

	@FindBy(how = How.ID, using = "agenteNombre")
	private WebElement agenteNombreOF;

	@FindBy(how = How.ID, using = "negocios")
	private WebElement negociosSelect;

	@FindBy(how = How.ID, using = "productos")
	private WebElement productosSelect;

	@FindBy(how = How.ID, using = "descuento")
	private WebElement descuentoDesdeInput;

	@FindBy(how = How.ID, using = "descuentoFin")
	private WebElement descuentoHastaInput;

	@FindBy(how = How.ID, using = "valorPrimaTotal")
	private WebElement valorPrimaTotalDesdeInput;

	@FindBy(how = How.ID, using = "valorPrimaTotalFin")
	private WebElement valorPrimaTotalHastaInput;

	@FindBy(how = How.ID, using = "cotizacionForm_cotizacion_tipoPolizaDTO_claveAplicaFlotillas1")
	private WebElement aplicaFlotillaSiRb;

	@FindBy(how = How.ID, using = "cotizacionForm_cotizacion_tipoPolizaDTO_claveAplicaFlotillas0")
	private WebElement aplicaFlotillaNoRb;

	@FindBy(how = How.ID, using = "cotizacionForm_cotizacion_tipoPolizaDTO_claveAplicaFlotillas")
	private WebElement aplicaFlotillaTodosRb;

	@FindBy(how = How.ID, using = "fechaSeguimiento")
	private WebElement fechaSeguimientoDate;

	@FindBy(how = How.ID, using = "numeroFolio")
	private WebElement numeroFolioInput;

	@FindBy(how = How.ID, using = "submit")
	private WebElement buscarSubmitBtn;
	
	@FindBy(how = How.XPATH, using = "//img[@src='../img/icons/ico_editar.gif']")
	private WebElement editarCotizacionIco;
	
	@FindBy(how = How.XPATH, using = "//img[@src='../img/complementar.png']")
	private WebElement complementarCotizacionIco;
	
	//---------------------------- GETTERS & SETTERS-----------------------------------//
	
	public WebElement getComplementarCotizacionIco() {
		return complementarCotizacionIco;
	}

	public void setComplementarCotizacionIco(WebElement complementarCotizacionIco) {
		this.complementarCotizacionIco = complementarCotizacionIco;
	}

	public WebElement getEditarCotizacionIco() {
		return editarCotizacionIco;
	}

	public void setEditarCotizacionIco(WebElement editarCotizacionIco) {
		this.editarCotizacionIco = editarCotizacionIco;
	}

	public WebElement getMostrarFiltrosBtn() {
		return mostrarFiltrosBtn;
	}

	public void setMostrarFiltrosBtn(WebElement mostrarFiltrosBtn) {
		this.mostrarFiltrosBtn = mostrarFiltrosBtn;
	}

	public WebElement getLimpiarBtn() {
		return limpiarBtn;
	}

	public void setLimpiarBtn(WebElement limpiarBtn) {
		this.limpiarBtn = limpiarBtn;
	}

	public WebElement getAutosIndividualBtn() {
		return autosIndividualBtn;
	}

	public void setAutosIndividualBtn(WebElement autosIndividualBtn) {
		this.autosIndividualBtn = autosIndividualBtn;
	}

	public WebElement getCotizadorAgentesBtn() {
		return cotizadorAgentesBtn;
	}

	public void setCotizadorAgentesBtn(WebElement cotizadorAgentesBtn) {
		this.cotizadorAgentesBtn = cotizadorAgentesBtn;
	}

	public WebElement getCotizacionExpressBtn() {
		return cotizacionExpressBtn;
	}

	public void setCotizacionExpressBtn(WebElement cotizacionExpressBtn) {
		this.cotizacionExpressBtn = cotizacionExpressBtn;
	}

	public WebElement getIndividualFlotillasBtn() {
		return individualFlotillasBtn;
	}

	public void setIndividualFlotillasBtn(WebElement individualFlotillasBtn) {
		this.individualFlotillasBtn = individualFlotillasBtn;
	}

	public WebElement getCargaMasivaIndividualBtn() {
		return cargaMasivaIndividualBtn;
	}

	public void setCargaMasivaIndividualBtn(WebElement cargaMasivaIndividualBtn) {
		this.cargaMasivaIndividualBtn = cargaMasivaIndividualBtn;
	}

	public WebElement getAutoExpediblesBtn() {
		return AutoExpediblesBtn;
	}

	public void setAutoExpediblesBtn(WebElement autoExpediblesBtn) {
		AutoExpediblesBtn = autoExpediblesBtn;
	}

	public WebElement getCotizadorServicioPublicoBtn() {
		return cotizadorServicioPublicoBtn;
	}

	public void setCotizadorServicioPublicoBtn(WebElement cotizadorServicioPublicoBtn) {
		this.cotizadorServicioPublicoBtn = cotizadorServicioPublicoBtn;
	}

	public WebElement getAplicaFlotillaSiRb() {
		return aplicaFlotillaSiRb;
	}

	public void setAplicaFlotillaSiRb(WebElement aplicaFlotillaSiRb) {
		this.aplicaFlotillaSiRb = aplicaFlotillaSiRb;
	}

	public WebElement getAplicaFlotillaNoRb() {
		return aplicaFlotillaNoRb;
	}

	public void setAplicaFlotillaNoRb(WebElement aplicaFlotillaNoRb) {
		this.aplicaFlotillaNoRb = aplicaFlotillaNoRb;
	}

	public WebElement getAplicaFlotillaTodosRb() {
		return aplicaFlotillaTodosRb;
	}

	public void setAplicaFlotillaTodosRb(WebElement aplicaFlotillaTodosRb) {
		this.aplicaFlotillaTodosRb = aplicaFlotillaTodosRb;
	}

	public WebElement getBuscarSubmitBtn() {
		return buscarSubmitBtn;
	}

	public void setBuscarSubmitBtn(WebElement buscarSubmitBtn) {
		this.buscarSubmitBtn = buscarSubmitBtn;
	}
	
	public WebElement getIdToCotizacionInput() {
		return idToCotizacionInput;
	}

	public void setIdToCotizacionInput(WebElement idToCotizacionInput) {
		this.idToCotizacionInput = idToCotizacionInput;
	}

	public WebElement getFechaCreacionInput() {
		return fechaCreacionInput;
	}

	public void setFechaCreacionInput(WebElement fechaCreacionInput) {
		this.fechaCreacionInput = fechaCreacionInput;
	}

	public WebElement getFechaFinalInput() {
		return fechaFinalInput;
	}

	public void setFechaFinalInput(WebElement fechaFinalInput) {
		this.fechaFinalInput = fechaFinalInput;
	}

	public WebElement getClaveEstatusSelect() {
		return claveEstatusSelect;
	}

	public void setClaveEstatusSelect(WebElement claveEstatusSelect) {
		this.claveEstatusSelect = claveEstatusSelect;
	}

	public WebElement getNombreAseguradoInput() {
		return nombreAseguradoInput;
	}

	public void setNombreAseguradoInput(WebElement nombreAseguradoInput) {
		this.nombreAseguradoInput = nombreAseguradoInput;
	}

	public WebElement getCodigoUsuarioCotizacionInput() {
		return codigoUsuarioCotizacionInput;
	}

	public void setCodigoUsuarioCotizacionInput(WebElement codigoUsuarioCotizacionInput) {
		this.codigoUsuarioCotizacionInput = codigoUsuarioCotizacionInput;
	}

	public WebElement getDescripcionVehiculoInput() {
		return descripcionVehiculoInput;
	}

	public void setDescripcionVehiculoInput(WebElement descripcionVehiculoInput) {
		this.descripcionVehiculoInput = descripcionVehiculoInput;
	}

	public WebElement getCentrosEmisoresSelect() {
		return centrosEmisoresSelect;
	}

	public void setCentrosEmisoresSelect(WebElement centrosEmisoresSelect) {
		this.centrosEmisoresSelect = centrosEmisoresSelect;
	}

	public WebElement getDescripcionBusquedaPromotoriaOF() {
		return descripcionBusquedaPromotoriaOF;
	}

	public void setDescripcionBusquedaPromotoriaOF(WebElement descripcionBusquedaPromotoriaOF) {
		this.descripcionBusquedaPromotoriaOF = descripcionBusquedaPromotoriaOF;
	}

	public WebElement getAgenteNombreOF() {
		return agenteNombreOF;
	}

	public void setAgenteNombreOF(WebElement agenteNombreOF) {
		this.agenteNombreOF = agenteNombreOF;
	}

	public WebElement getNegociosSelect() {
		return negociosSelect;
	}

	public void setNegociosSelect(WebElement negociosSelect) {
		this.negociosSelect = negociosSelect;
	}

	public WebElement getProductosSelect() {
		return productosSelect;
	}

	public void setProductosSelect(WebElement productosSelect) {
		this.productosSelect = productosSelect;
	}

	public WebElement getDescuentoDesdeInput() {
		return descuentoDesdeInput;
	}

	public void setDescuentoDesdeInput(WebElement descuentoDesdeInput) {
		this.descuentoDesdeInput = descuentoDesdeInput;
	}

	public WebElement getDescuentoHastaInput() {
		return descuentoHastaInput;
	}

	public void setDescuentoHastaInput(WebElement descuentoHastaInput) {
		this.descuentoHastaInput = descuentoHastaInput;
	}

	public WebElement getValorPrimaTotalDesdeInput() {
		return valorPrimaTotalDesdeInput;
	}

	public void setValorPrimaTotalDesdeInput(WebElement valorPrimaTotalDesdeInput) {
		this.valorPrimaTotalDesdeInput = valorPrimaTotalDesdeInput;
	}

	public WebElement getValorPrimaTotalHastaInput() {
		return valorPrimaTotalHastaInput;
	}

	public void setValorPrimaTotalHastaInput(WebElement valorPrimaTotalHastaInput) {
		this.valorPrimaTotalHastaInput = valorPrimaTotalHastaInput;
	}

	public WebElement getFechaSeguimientoDate() {
		return fechaSeguimientoDate;
	}

	public void setFechaSeguimientoDate(WebElement fechaSeguimientoDate) {
		this.fechaSeguimientoDate = fechaSeguimientoDate;
	}

	public WebElement getNumeroFolioInput() {
		return numeroFolioInput;
	}

	public void setNumeroFolioInput(WebElement numeroFolioInput) {
		this.numeroFolioInput = numeroFolioInput;
	}
	
	//----------------------------------------------------------------------------------------//
	
}
