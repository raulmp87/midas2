package com.afirmeseguros.selenium.cotizacion;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class ComplementarDatosVehiculoPage {
	
	@FindBy(how = How.NAME, using = "inciso.incisoAutoCot.nombreConductor")
	private WebElement nombreConductorInput;
	
	@FindBy(how = How.NAME, using = "inciso.incisoAutoCot.paternoConductor")
	private WebElement aPaternoConductorInput;
	
	@FindBy(how = How.NAME, using = "inciso.incisoAutoCot.maternoConductor")
	private WebElement aMaternoConductorInput;
	
	@FindBy(how = How.NAME, using = "inciso.incisoAutoCot.numeroLicencia")
	private WebElement noLicenciaConductorInput;
	
	@FindBy(how = How.ID, using = "fechaNacimiento")
	private WebElement fechaNacConductor;
	
	@FindBy(how = How.XPATH, using = "//button[contains(@class,'ui-datepicker-trigger')]")
	private WebElement iconoFechaNacConductor;
	
	@FindBy(how = How.ID, using = "ocupacionConductor")
	private WebElement ocupacionConductorInput;
	
	//----------------------------------------------------------------------------------------//
	
	@FindBy(how = How.ID, using = "numMotor")
	private WebElement numMotorInput;
	
	@FindBy(how = How.ID, using = "numPlaca")
	private WebElement numPlacaInput;
	
	@FindBy(how = How.ID, using = "repuve")
	private WebElement repuveInput;
	
	@FindBy(how = How.ID, using = "rutaCirculacion")
	private WebElement rutaCirculacionInput;
	
	@FindBy(how = How.ID, using = "numSerie")
	private WebElement numSerieInput;
	
	@FindBy(how = How.ID, using = "emailContacto")
	private WebElement emailContactoInput;
	
	@FindBy(how = How.ID, using = "complementarIncisoForm_inciso_incisoAutoCot_observacionesinciso")
	private WebElement observacionesInput;
	
	@FindBy(how = How.ID, using = "btnGuardar")
	private WebElement guardarBtn;
	
	@FindBy(how = How.ID, using = "salirBtn")
	private WebElement salirBtn;
	
	//----------------------------------GETTERS & SETTERS------------------------------------//

	public WebElement getIconoFechaNacConductor() {
		return iconoFechaNacConductor;
	}

	public void setIconoFechaNacConductor(WebElement iconoFechaNacConductor) {
		this.iconoFechaNacConductor = iconoFechaNacConductor;
	}
	
	public WebElement getFechaNacConductor() {
		return fechaNacConductor;
	}

	public void setFechaNacConductor(WebElement fechaNacConductor) {
		this.fechaNacConductor = fechaNacConductor;
	}
	
	public WebElement getNombreConductorInput() {
		return nombreConductorInput;
	}

	public void setNombreConductorInput(WebElement nombreConductorInput) {
		this.nombreConductorInput = nombreConductorInput;
	}

	public WebElement getaPaternoConductorInput() {
		return aPaternoConductorInput;
	}

	public void setaPaternoConductorInput(WebElement aPaternoConductorInput) {
		this.aPaternoConductorInput = aPaternoConductorInput;
	}

	public WebElement getaMaternoConductorInput() {
		return aMaternoConductorInput;
	}

	public void setaMaternoConductorInput(WebElement aMaternoConductorInput) {
		this.aMaternoConductorInput = aMaternoConductorInput;
	}

	public WebElement getNoLicenciaConductorInput() {
		return noLicenciaConductorInput;
	}

	public void setNoLicenciaConductorInput(WebElement noLicenciaConductorInput) {
		this.noLicenciaConductorInput = noLicenciaConductorInput;
	}

	public WebElement getOcupacionConductorInput() {
		return ocupacionConductorInput;
	}

	public void setOcupacionConductorInput(WebElement ocupacionConductorInput) {
		this.ocupacionConductorInput = ocupacionConductorInput;
	}

	public WebElement getNumMotorInput() {
		return numMotorInput;
	}

	public void setNumMotorInput(WebElement numMotorInput) {
		this.numMotorInput = numMotorInput;
	}

	public WebElement getNumPlacaInput() {
		return numPlacaInput;
	}

	public void setNumPlacaInput(WebElement numPlacaInput) {
		this.numPlacaInput = numPlacaInput;
	}

	public WebElement getRepuveInput() {
		return repuveInput;
	}

	public void setRepuveInput(WebElement repuveInput) {
		this.repuveInput = repuveInput;
	}

	public WebElement getRutaCirculacionInput() {
		return rutaCirculacionInput;
	}

	public void setRutaCirculacionInput(WebElement rutaCirculacionInput) {
		this.rutaCirculacionInput = rutaCirculacionInput;
	}

	public WebElement getNumSerieInput() {
		return numSerieInput;
	}

	public void setNumSerieInput(WebElement numSerieInput) {
		this.numSerieInput = numSerieInput;
	}

	public WebElement getEmailContactoInput() {
		return emailContactoInput;
	}

	public void setEmailContactoInput(WebElement emailContactoInput) {
		this.emailContactoInput = emailContactoInput;
	}

	public WebElement getObservacionesInput() {
		return observacionesInput;
	}

	public void setObservacionesInput(WebElement observacionesInput) {
		this.observacionesInput = observacionesInput;
	}

	public WebElement getGuardarBtn() {
		return guardarBtn;
	}

	public void setGuardarBtn(WebElement guardarBtn) {
		this.guardarBtn = guardarBtn;
	}

	public WebElement getSalirBtn() {
		return salirBtn;
	}

	public void setSalirBtn(WebElement salirBtn) {
		this.salirBtn = salirBtn;
	}
	
	
	
	
}
