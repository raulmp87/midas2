package com.afirmeseguros.selenium.cotizacion;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.Select;

import com.afirmeseguros.selenium.BasicTest;

public class CrearCotizacionPage extends BasicTest {
	
	
	
	@FindBy(how = How.ID, using = "divBuscarBtn")
	private WebElement seleccionarAgenteBtn;
	
	@FindBy(how = How.ID, using = "negocio")
	private WebElement negocioSelect;
	
	@FindBy(how = How.ID, using = "producto")
	private WebElement productoSelect;
	
	@FindBy(how = How.ID, using = "tipoPoliza")
	private WebElement tipoPolizaSelect;
	
	@FindBy(how = How.ID, using = "idMoneda")
	private WebElement monedaSelect;
	
	@FindBy(how = How.XPATH, using = "//a[contains(.,' Cancelar ')]")
	private WebElement cancelarBtn;

	@FindBy(how = How.CSS, using = ".icon_guardar")
	private WebElement crearCotizacionBtn;
	
	@FindBy(how = How.ID, using = "idToCotizacion")
	private WebElement idToCotizacion;
	
	
	//----------------------------------GETTERS & SETTERS------------------------------------//
	
	
	public WebElement getCrearCotizacionBtn() {
		return crearCotizacionBtn;
	}

	public void setCrearCotizacionBtn(WebElement crearCotizacionBtn) {
		this.crearCotizacionBtn = crearCotizacionBtn;
	}

	public WebElement getSeleccionarAgenteBtn() {
		return seleccionarAgenteBtn;
	}

	public void setSeleccionarAgenteBtn(WebElement seleccionarAgenteBtn) {
		this.seleccionarAgenteBtn = seleccionarAgenteBtn;
	}

	public WebElement getCancelarBtn() {
		return cancelarBtn;
	}

	public void setCancelarBtn(WebElement cancelarBtn) {
		this.cancelarBtn = cancelarBtn;
	}


	
	//------------------------------------------------------------------------------------//
		
	public void selectNegocio(String NEGOCIO){
		Select dropdown = new Select(negocioSelect);
		dropdown.selectByVisibleText(NEGOCIO);
	}
	
	public void selectProducto(String producto){
		Select dropdown = new Select(productoSelect);
		dropdown.selectByVisibleText(producto);
	}
	
	public void selectTipoPoliza(String tipoPoliza){
		Select dropdown = new Select(tipoPolizaSelect);
		dropdown.selectByVisibleText(tipoPoliza);
	}
	
	public void selectMoneda(String moneda){
		Select dropdown = new Select(monedaSelect);
		dropdown.selectByVisibleText(moneda);
	}
	
	public void putNumCotizacion(){
		DATA.put("idToCotizacion", idToCotizacion.getAttribute("value"));
		DATA.forEach( (k,v) -> System.out.println("Key: " + k + ": Value: " + v));

	}
	
	
}
