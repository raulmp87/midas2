package com.afirmeseguros.selenium.cotizacion;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.afirmeseguros.selenium.BasicTest;

public class MensajePage extends BasicTest {
	
	protected static WebDriverWait waitEmi;
	

	@FindBy(how = How.CSS, using = "#mensajeBoton .b_aceptar a")
	private WebElement aceptarMensajeBtn;
	
	@FindBy(how = How.CSS, using = "#mensajeBoton .b_cancelar a")
	private WebElement cancelarMensajeBtn;

	public WebElement getCancelarMensajeBtn() {
		return cancelarMensajeBtn;
	}

	public void setCancelarMensajeBtn(WebElement cancelarMensajeBtn) {
		this.cancelarMensajeBtn = cancelarMensajeBtn;
	}

	public WebElement getMensajeAlertaEmision() {
		return mensajeAlertaEmision;
	}

	public void setMensajeAlertaEmision(WebElement mensajeAlertaEmision) {
		this.mensajeAlertaEmision = mensajeAlertaEmision;
	}

	@FindBy(how = How.CLASS_NAME, using ="mensaje_texto")
	private WebElement mensajeGlobalEmision;
	
	@FindBy(how = How.XPATH, using ="//div[contains(@class,'dhtmlxWindowMainContent')]")
	private WebElement mensajeAlertaEmision;
	
	public WebElement getAceptarMensajeBtn() {
		return aceptarMensajeBtn;
	}

	public void setAceptarMensajeBtn(WebElement aceptarMensajeBtn) {
		this.aceptarMensajeBtn = aceptarMensajeBtn;
	}
	
	public WebElement getMensajeGlobalEmision() {
		return mensajeGlobalEmision;
	}

	public void setMensajeGlobalEmision(WebElement mensajeGlobalEmision) {
		this.mensajeGlobalEmision = mensajeGlobalEmision;
	}

	public void getMensajeGobal(){
		waitEmi = new WebDriverWait(driver, 70);
		waitEmi.until(ExpectedConditions.visibilityOf(mensajeGlobalEmision));
		mensajeGlobalEmision.getText();
		System.out.println(mensajeGlobalEmision.getText());
	}
	
	public void waitMensajeGlobal(){
		waitEmi = new WebDriverWait(driver, 150);
		waitEmi.until(ExpectedConditions.visibilityOf(mensajeAlertaEmision));
	}
}
