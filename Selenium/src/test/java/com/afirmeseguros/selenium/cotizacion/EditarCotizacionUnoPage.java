package com.afirmeseguros.selenium.cotizacion;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.afirmeseguros.selenium.BasicTest;

public class EditarCotizacionUnoPage extends BasicTest {
	
	protected static WebDriverWait waitEsp;
	
	@FindBy(how = How.CLASS_NAME, using = "tituloEditarCotizacion")
	private WebElement tituloEditarCotizacion;
	
	@FindBy(how = How.XPATH, using = "//a[contains(.,' Ver Igualar Primas ')]")
	private WebElement verIgualarPrimasBtn;
	
	@FindBy(how = How.XPATH, using = "//a[contains(.,' Ver Esquema de Pago ')]")
	private WebElement verEsquemaPagoBtn;
	
	@FindBy(how = How.ID, using = "fecha")
	private WebElement fechaDate;
	
	@FindBy(how = How.ID, using = "formaPago")
	private WebElement formaPagoSelect;
	
	@FindBy(how = How.ID, using = "derechos")
	private WebElement derechosSelect;

	@FindBy(how = How.ID, using = "comisionCedida")
	private WebElement comisionCedidaInput;

	@FindBy(how = How.ID, using = "fechaSeguimiento")
	private WebElement fechaSeguimientoDate;
	
	public WebElement getFechaSeguimientoDate() {
		return fechaSeguimientoDate;
	}

	public void setFechaSeguimientoDate(WebElement fechaSeguimientoDate) {
		this.fechaSeguimientoDate = fechaSeguimientoDate;
	}

	@FindBy(how = How.ID, using = "fechados")
	private WebElement fechadosDate;
	
	@FindBy(how = How.ID, using = "porcentajePagoFraccionado")
	private WebElement porcentajePagoFraccionadoInput;
	
	@FindBy(how = How.ID, using = "guardarInciso")
	private WebElement guardarIconBtn;
	
	@FindBy(how = How.ID, using = "nombreContratante")
	private WebElement nombreContratanteInput;
	
	
	//----------------------------------------------------------------------------------------//
	
	@FindBy(how = How.ID, using = "agregarInciso")
	private WebElement masAgregarIconBtn;
	
	//-------------------------Aparece despues de guardar el inciso--------------------------//
	
	@FindBy(how = How.XPATH, using = "//a[contains(.,' Terminar Cotización ')]")
	private WebElement terminanCotizacionBtn;
	
	//----------------------------Informacion del inciso--------------------------------------//

	@FindBy(how = How.ID,  using = "mostrarInfvehicularBtn")
	private WebElement validarBtn;
	
	@FindBy(how = How.ID,  using = "btnDelet")
	private WebElement eliminarIncisoBtn;
	
	@FindBy(how = How.ID,  using = "btnEdit")
	private WebElement editarIncisoBtn;

	//-------------------------------------GETTERS & SETTERS----------------------------------//
	
	public WebElement getTituloEditarCotizacion() {
		return tituloEditarCotizacion;
	}

	public void setTituloEditarCotizacion(WebElement tituloEditarCotizacion) {
		this.tituloEditarCotizacion = tituloEditarCotizacion;
	}

	public WebElement getFechaDate() {
		return fechaDate;
	}

	public void setFechaDate(WebElement fechaDate) {
		this.fechaDate = fechaDate;
	}

	public WebElement getFormaPagoSelect() {
		return formaPagoSelect;
	}

	public void setFormaPagoSelect(WebElement formaPagoSelect) {
		this.formaPagoSelect = formaPagoSelect;
	}

	public WebElement getDerechosSelect() {
		return derechosSelect;
	}

	public void setDerechosSelect(WebElement derechosSelect) {
		this.derechosSelect = derechosSelect;
	}

	public WebElement getComisionCedidaInput() {
		return comisionCedidaInput;
	}

	public void setComisionCedidaInput(WebElement comisionCedidaInput) {
		this.comisionCedidaInput = comisionCedidaInput;
	}

	public WebElement getFechadosDate() {
		return fechadosDate;
	}

	public void setFechadosDate(WebElement fechadosDate) {
		this.fechadosDate = fechadosDate;
	}

	public WebElement getPorcentajePagoFraccionadoInput() {
		return porcentajePagoFraccionadoInput;
	}

	public void setPorcentajePagoFraccionadoInput(WebElement porcentajePagoFraccionadoInput) {
		this.porcentajePagoFraccionadoInput = porcentajePagoFraccionadoInput;
	}

	public WebElement getNombreContratanteInput() {
		return nombreContratanteInput;
	}

	public void setNombreContratanteInput(WebElement nombreContratanteInput) {
		this.nombreContratanteInput = nombreContratanteInput;
	}

	public WebElement getVerIgualarPrimasBtn() {
		return verIgualarPrimasBtn;
	}

	public void setVerIgualarPrimasBtn(WebElement verIgualarPrimasBtn) {
		this.verIgualarPrimasBtn = verIgualarPrimasBtn;
	}

	public WebElement getVerEsquemaPagoBtn() {
		return verEsquemaPagoBtn;
	}

	public void setVerEsquemaPagoBtn(WebElement verEsquemaPagoBtn) {
		this.verEsquemaPagoBtn = verEsquemaPagoBtn;
	}

	public WebElement getGuardarIconBtn() {
		return guardarIconBtn;
	}

	public void setGuardarIconBtn(WebElement guardarIconBtn) {
		this.guardarIconBtn = guardarIconBtn;
	}

	public WebElement getMasAgregarIconBtn() {
		return masAgregarIconBtn;
	}

	public void setMasAgregarIconBtn(WebElement masAgregarIconBtn) {
		this.masAgregarIconBtn = masAgregarIconBtn;
	}

	public WebElement getTerminanCotizacionBtn() {
		return terminanCotizacionBtn;
	}

	public void setTerminanCotizacionBtn(WebElement terminanCotizacionBtn) {
		this.terminanCotizacionBtn = terminanCotizacionBtn;
	}

	public WebElement getValidarBtn() {
		return validarBtn;
	}

	public void setValidarBtn(WebElement validarBtn) {
		this.validarBtn = validarBtn;
	}

	public WebElement getEliminarIncisoBtn() {
		return eliminarIncisoBtn;
	}

	public void setEliminarIncisoBtn(WebElement eliminarIncisoBtn) {
		this.eliminarIncisoBtn = eliminarIncisoBtn;
	}

	public WebElement getEditarIncisoBtn() {
		return editarIncisoBtn;
	}

	public void setEditarIncisoBtn(WebElement editarIncisoBtn) {
		this.editarIncisoBtn = editarIncisoBtn;
	}

	//----------------------------------------------------------------------------------------//
	
	public void waitElementPage(){
		wait.until(ExpectedConditions.visibilityOf(formaPagoSelect));
	}
	
	public void clickElementEsp(WebElement element){
		waitEsp = new WebDriverWait(driver, 70);
		waitEsp.until(ExpectedConditions.visibilityOf(element));
		element.click();
	}
	
	
}
