package com.afirmeseguros.selenium.cotizacion;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import com.afirmeseguros.selenium.BasicTest;

public class ConsultaClientesPage extends BasicTest {

	@FindBy(how = How.ID, using = "filtroCliente.claveTipoPersonaString1")
	private WebElement pFisicaRb;
	
	@FindBy(how = How.ID, using = "filtroCliente.claveTipoPersonaString2")
	private WebElement pMoralRb;
	
	@FindBy(how = How.NAME, using = "filtroCliente.nombre")
	private WebElement nombreCliente;

	@FindBy(how = How.NAME, using = "filtroCliente.apellidoPaterno")
	private WebElement aPaternoCliente;
	
	@FindBy(how = How.NAME, using = "filtroCliente.apellidoMaterno")
	private WebElement aMaternoCliente;
	
	@FindBy(how = How.NAME, using = "filtroCliente.codigoCURP")
	private WebElement curpCliente;
	
	@FindBy(how = How.NAME, using = "filtroCliente.codigoRFC")
	private WebElement rfcCliente;
	
	@FindBy(how = How.XPATH, using = "//a[contains(@class,'icon_buscar')]")
	private WebElement buscarBtn;
	
	@FindBy(how = How.XPATH, using = "//a[contains(.,'Agregar')]")
	private WebElement agregarClienteBtn;
	
	//@FindBy(how = How.CLASS_NAME, using = "ev_light")
	//@FindBys({@FindBy(how = How.ID, using = "clienteGrid"), @FindBy(how = How.XPATH, using = "//tr[2]")})
	@FindBy(how = How.XPATH, using = ".//*[@id='clienteGrid']/div[2]/table/tbody/tr[2]")
	private WebElement clienteSeleccionado;
	

	//----------------------------------GETTERS & SETTERS------------------------------------//
	
	public WebElement getClienteSeleccionado() {
		return clienteSeleccionado;
	}

	public void setClienteSeleccionado(WebElement clienteSeleccionado) {
		this.clienteSeleccionado = clienteSeleccionado;
	}

	public WebElement getpFisicaRb() {
		return pFisicaRb;
	}

	public void setpFisicaRb(WebElement pFisicaRb) {
		this.pFisicaRb = pFisicaRb;
	}

	public WebElement getpMoralRb() {
		return pMoralRb;
	}

	public void setpMoralRb(WebElement pMoralRb) {
		this.pMoralRb = pMoralRb;
	}

	public WebElement getBuscarBtn() {
		return buscarBtn;
	}

	public void setBuscarBtn(WebElement buscarBtn) {
		this.buscarBtn = buscarBtn;
	}

	public WebElement getAgregarClienteBtn() {
		return agregarClienteBtn;
	}

	public void setAgregarClienteBtn(WebElement agregarClienteBtn) {
		this.agregarClienteBtn = agregarClienteBtn;
	}
	
	public WebElement getNombreCliente() {
		return nombreCliente;
	}

	public void setNombreCliente(WebElement nombreCliente) {
		this.nombreCliente = nombreCliente;
	}

	public WebElement getaPaternoCliente() {
		return aPaternoCliente;
	}

	public void setaPaternoCliente(WebElement aPaternoCliente) {
		this.aPaternoCliente = aPaternoCliente;
	}

	public WebElement getaMaternoCliente() {
		return aMaternoCliente;
	}

	public void setaMaternoCliente(WebElement aMaternoCliente) {
		this.aMaternoCliente = aMaternoCliente;
	}

	public WebElement getCurpCliente() {
		return curpCliente;
	}

	public void setCurpCliente(WebElement curpCliente) {
		this.curpCliente = curpCliente;
	}

	public WebElement getRfcCliente() {
		return rfcCliente;
	}

	public void setRfcCliente(WebElement rfcCliente) {
		this.rfcCliente = rfcCliente;
	}
	
}
