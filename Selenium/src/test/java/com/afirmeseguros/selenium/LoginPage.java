package com.afirmeseguros.selenium;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class LoginPage {

		@FindBy(how=How.ID, using="form-username")
		private WebElement formUsername;
		
		@FindBy(how=How.ID, using="form-password")
		private WebElement formPassword;
		
		@FindBy(how=How.CSS, using="button.btn")
		private WebElement loginButton;
		
		public WebElement getLoginButton() {
			return loginButton;
		}

		public void setLoginButton(WebElement loginButton) {
			this.loginButton = loginButton;
		}

		public void EnterUserName(String usernametext){
			formUsername.clear();
			formUsername.sendKeys(usernametext);
		}
		
		public void EnterPassword(String passwordtext){
			formPassword.clear();
			formPassword.sendKeys(passwordtext);
		}
}
