package com.afirmeseguros.selenium;

import static org.junit.Assert.assertTrue;

import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.StringUtils;
import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class BasicTest {

	protected static WebDriver driver;
	protected static boolean acceptNextAlert = true;
	protected static String baseUrl;
	public final static String NEGOCIO = "NEGOCIO BASE PRODUCCION";
	protected static Actions builder;
	protected static WebDriverWait wait;
	protected static WebDriverWait waitMenu;
	public static final Map<String, String> DATA = new HashMap<String, String>();
		
	public void hacerClicMenu(String ... menus) throws Exception {
		//final WebElement logo = driver.findElement(By.id("logo"));
		//wait.until(ExpectedConditions.visibilityOf(logo));
		for (int i = 0; i < menus.length; i++) {
			WebElement menuElement = null;
			List<WebElement> menuElementList = driver.findElements(By.xpath("//div[contains(text(), \'" + menus[i] + "\')]"));
			for (WebElement menu: menuElementList) {
				String id = menu.getAttribute("id");
				if (menu.isDisplayed() && StringUtils.countMatches(id, "_") == i ) {
					menuElement = menu;
					break;
				}
			}
			
			waitMenu.until(ExpectedConditions.visibilityOf(menuElement));
			builder.moveToElement(menuElement);
			builder.build().perform();
			if( i != 0 && i != menus.length-1 ){
				builder.moveToElement(menuElement, menuElement.getSize().width, 0);
				builder.build().perform();
			}
			
			if (i == menus.length -1) {
				menuElement.click();
			}
		}
		
	}
	
	public void validateTitle(WebElement element, String titulo){
		assertTrue(titulo.equals(element.getText()));
		System.out.println(element.getText());
	}
	
	
	public void changeToFrame(){
		final WebElement frame = (new WebDriverWait(driver, 20)).until(ExpectedConditions
				.presenceOfElementLocated(By.xpath("//div[contains(@class, 'dhtmlxWindowMainContent')]//iframe")));
		driver.switchTo().frame(frame);
	}
	
	public void changeToFrame2(){
		final WebElement frame2 = (new WebDriverWait(driver, 20)).until(ExpectedConditions
				.presenceOfElementLocated(By.xpath("//iframe[contains(@src,'/MidasWeb/suscripcion/cotizacion/auto/ventanaAgentes.action')]")));
		driver.switchTo().frame(frame2);
	}
	
	public void changeToDefalut(){
		driver.switchTo().defaultContent();
	}
	
	public void clickElement(WebElement element){
		wait.until(ExpectedConditions.visibilityOf(element));
		element.click();
	}
	
	public void sendKeysElement(WebElement element, String keys){
		wait.until(ExpectedConditions.visibilityOf(element));
		element.sendKeys(keys);
	}
	
	public void chooseOptionSelect(WebElement element, String option){
		wait.until(ExpectedConditions.visibilityOf(element));
		Select dropdown = new Select(element);
		dropdown.selectByVisibleText(option);
	}
	
	
	public void doubleClick(WebElement element){
		builder.doubleClick(element).build().perform();
	}
	
	@Before
	public void setUp() throws Exception {
		
		final DesiredCapabilities capability = DesiredCapabilities.internetExplorer();
		capability.setPlatform(Platform.WINDOWS);
		driver = new RemoteWebDriver(new URL("http://10.126.115.215:4444/wd/hub"), capability);
		
		//driver = new InternetExplorerDriver();
		builder = new Actions(driver);
		wait = new WebDriverWait(driver, 100);
		waitMenu = new WebDriverWait(driver, 100);
		baseUrl = "http://cap2.afirme.com.mx";
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.manage().window().maximize();
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}

	protected boolean isElementPresent(By by) {
		try {
			driver.findElement(by);
			return true;
		} catch (NoSuchElementException e) {
			return false;
		}
	}

	protected boolean isAlertPresent() {
		try {
			driver.switchTo().alert();
			return true;
		} catch (NoAlertPresentException e) {
			return false;
		}
	}

	protected String closeAlertAndGetItsText() {
		try {
			Alert alert = driver.switchTo().alert();
			String alertText = alert.getText();
			if (acceptNextAlert) {
				alert.accept();
			} else {
				alert.dismiss();
			}
			return alertText;
		} finally {
			acceptNextAlert = true;
		}
	}
}
