package com.afirmeseguros.selenium.midas2;

import org.junit.Test;
import org.openqa.selenium.support.PageFactory;
import com.afirmeseguros.selenium.BasicTest;
import com.afirmeseguros.selenium.EmisionAutosPage;

public class EmisionAutos extends BasicTest {

	@Test
	public void testAutosEmiEmisionPF() throws Exception {
		
		new Inicio().menuEmisionAutosEmisionEmision(); 
		EmisionAutosPage emisionAutosPage = PageFactory.initElements(driver, EmisionAutosPage.class);
		emisionAutosPage.sendKeysNumeroPoliza("01091526");
		clickElement(emisionAutosPage.getBtnBuscar());
		clickElement(emisionAutosPage.getPolizaGridTr());
		clickElement(emisionAutosPage.getVerDetalle());
		changeToFrame();
		clickElement(emisionAutosPage.getConsultar());
		Thread.sleep(3000);
		//emisionAutosPage.validateTitleConsultaPoliza();
		
	}
}
