package com.afirmeseguros.selenium.midas2;

import java.util.HashMap;
import java.util.Map;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.afirmeseguros.selenium.BasicTest;

public class CotizacionAutos  extends BasicTest {

	public static final Logger LOG = LoggerFactory.getLogger(CotizacionAutos.class);

	public static final Map<String, String> DATA = new HashMap<String, String>();

	public void testAutosCotCotizacion() throws Exception {

		hacerClicMenu("Emisión", "Autos", "Cotizaciones", "Cotización");

		final WebElement btnIndFlo = driver.findElement(By.cssSelector(".btn_back.w130>a"));
		wait.until(ExpectedConditions.visibilityOf(btnIndFlo));
		btnIndFlo.click();

		final WebElement frame = (new WebDriverWait(driver, 10)).until(ExpectedConditions
				.presenceOfElementLocated(By.xpath("//div[contains(@class, 'dhtmlxWindowMainContent')]//iframe")));
		driver.switchTo().frame(frame);

		final WebElement btnSeleccionarAgente = driver.findElement(By.cssSelector(".icon_persona"));
		btnSeleccionarAgente.click();

		driver.switchTo().defaultContent();

		final WebElement frame2 = (new WebDriverWait(driver, 10)).until(ExpectedConditions.presenceOfElementLocated(
				By.xpath("//iframe[contains(@src,'/MidasWeb/suscripcion/cotizacion/auto/ventanaAgentes.action')]")));
		driver.switchTo().frame(frame2);

		final WebElement escribirAgente = driver.findElement(By.id("descripcionBusquedaAgente"));
		escribirAgente.sendKeys("Federico de Hoyos Navarro");

		driver.findElement(By.xpath("//li[contains(@class, 'ui-menu-item')]//a")).click();

		driver.switchTo().defaultContent();
		final WebElement frame3 = (new WebDriverWait(driver, 10)).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[contains(@class, 'dhtmlxWindowMainContent')]//iframe")));
		driver.switchTo().frame(frame3);

		final Select selectNegocio = new Select(driver.findElement(By.id("negocio")));
		selectNegocio.selectByVisibleText(NEGOCIO);

		final Select selectProducto = new Select(driver.findElement(By.id("producto")));
		selectProducto.selectByVisibleText("AUTOMOVILES INDIVIDUALES");

		final Select selectMoneda = new Select(driver.findElement(By.id("idMoneda")));
		selectMoneda.selectByVisibleText("NACIONAL");

		final WebElement btnCrearCotizacion = driver.findElement(By.cssSelector(".icon_guardar"));
		wait.until(ExpectedConditions.visibilityOf(btnCrearCotizacion));
		btnCrearCotizacion.click();

		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("loading")));
		driver.switchTo().defaultContent();

		final WebElement fecVigencia = driver.findElement(By.id("wwgrp_fecha"));
		wait.until(ExpectedConditions.visibilityOf(fecVigencia));

		final WebElement numCotizacion = driver.findElement(By.id("idToCotizacion"));
		System.out.println(numCotizacion.getAttribute("value"));

		DATA.put("idCotizacion", numCotizacion.getAttribute("value"));
		wait.until(ExpectedConditions.attributeContains(numCotizacion, "value", numCotizacion.getAttribute("value")));

		System.out.println("|||HashMap - Put|||");
		DATA.forEach( (k,v) -> System.out.println("Key: " + k + ": Value: " + v));
	}

	public void buscarCotizacion() throws Exception{

		hacerClicMenu("Emisión", "Autos", "Cotizaciones", "Cotización");
		wait.until(ExpectedConditions.presenceOfElementLocated(By.id("agente")));
		wait.until(ExpectedConditions.presenceOfElementLocated(By.id("fechaSeguimiento")));

		final WebElement numCotizacionInput = driver.findElement(By.id("idToCotizacion"));
		wait.until(ExpectedConditions.visibilityOf(numCotizacionInput));
		System.out.println("|||HashMap - Get|||");
		DATA.forEach( (k,v) -> System.out.println("Key: " + k + ": Value: " + v));
		numCotizacionInput.sendKeys(DATA.get("idCotizacion"));
		Thread.sleep(3000);

		final WebElement btnBuscar = driver.findElement(By.id("submit"));
		btnBuscar.click();
	}

	public void editarCotizacion() throws Exception{

		LOG.trace("--> editarCotizacion()");

		final WebElement btnEditarCot = driver.findElement(By.xpath("//div[@id='cotizacionGrid']/div[2]/table/tbody/tr[2]/td[13]/a/img"));
		btnEditarCot.click();

		final WebElement tablaInfo = driver.findElement(By.id("agregar"));
		wait.until(ExpectedConditions.visibilityOf(tablaInfo));

		final WebElement numCotizacion = driver.findElement(By.className("titulo"));
		System.out.println(numCotizacion.getText());

		final Select selectFormaPago = new Select(driver.findElement(By.id("formaPago")));
		selectFormaPago.selectByVisibleText("MENSUAL");
		
		final WebElement pctgPago = driver.findElement(By.id("porcentajePagoFraccionado"));
		pctgPago.sendKeys("10");

		final WebElement guardarIcon = driver.findElement(By.id("guardarInciso")).findElement(By.tagName("a"));
		wait.until(ExpectedConditions.visibilityOf(guardarIcon));
		builder.moveToElement(guardarIcon);
		builder.perform();
		guardarIcon.click();

		final WebElement agregarIncisoBtn = driver.findElement(By.id("agregarInciso")).findElement(By.className("icon_masAgregar"));
		wait.until(ExpectedConditions.visibilityOf(agregarIncisoBtn));
		builder.moveToElement(agregarIncisoBtn);
		builder.perform();
		agregarIncisoBtn.click();

		final WebElement divFrame = driver.findElement(By.className("dhtmlxWindowMainContent")).findElement(By.tagName("iframe"));
		driver.switchTo().frame(divFrame);

		final Select selectEstado = new Select(driver.findElement(By.id("incisoCotizacion.incisoAutoCot.estadoId")));
		selectEstado.selectByVisibleText("CHIAPAS");

		final Select selectMunicipio = new Select(driver.findElement(By.id("incisoCotizacion.incisoAutoCot.municipioId")));
		selectMunicipio.selectByVisibleText("ALTAMIRANO");

		final Select selectLn = new Select(driver.findElement(By.id("incisoCotizacion.incisoAutoCot.negocioSeccionId")));
		selectLn.selectByVisibleText("AUTOMOVILES INDIVIDUALES");

		final Select selectMarca = new Select(driver.findElement(By.id("incisoCotizacion.incisoAutoCot.marcaId")));
		selectMarca.selectByVisibleText("FORD");

		final Select selectEstilo = new Select(driver.findElement(By.id("incisoCotizacion.incisoAutoCot.estiloId")));
		selectEstilo.selectByVisibleText("03256 - FD (C1A) FUSION SEL 3.0L GPS SYNC LN V6 AUT 4P ABS VP CQ CB");

		final Select selectModelo = new Select(driver.findElement(By.id("incisoCotizacion.incisoAutoCot.modeloVehiculo")));
		selectModelo.selectByVisibleText("2010");

		final Select selectPaquete = new Select(driver.findElement(By.id("incisoCotizacion.incisoAutoCot.negocioPaqueteId")));
		selectPaquete.selectByVisibleText("AMPLIA");

		final WebElement descripcionTh = driver.findElement(By.id("t_riesgo"));
		wait.until(ExpectedConditions.visibilityOf(descripcionTh));

		final WebElement calcularBtn = driver.findElement(By.id("btnRecalcular"));
		calcularBtn.click();

		final WebElement guardarBtn = driver.findElement(By.className("icon_guardar"));
		guardarBtn.click();

		driver.switchTo().defaultContent();
		Thread.sleep(6000);

		final WebElement terminarCotBtn = driver.findElement(By.id("terminarCotizacion"));
		terminarCotBtn.click();

		WebElement btnAceptar = driver.findElement(By.className("b_aceptar"));
		builder.moveToElement(btnAceptar);
		builder.build().perform();
		btnAceptar.click();

		LOG.trace("<--  editarCotizacion()");
	}

	public void complementarCotizacion() throws Exception{

		final WebElement terminarCotIco2 = driver.findElement(By.xpath(".//*[@id='cotizacionGrid']/div[2]/table/tbody/tr[2]/td[14]/a/img"));
		wait.until(ExpectedConditions.visibilityOf(terminarCotIco2));
		terminarCotIco2.click();
		Thread.sleep(6000);

		final WebElement buscarClienteBtn = driver.findElement(By.className("icon_cliente"));
		wait.until(ExpectedConditions.visibilityOf(buscarClienteBtn));
		buscarClienteBtn.click();

		final WebElement nombreCliente = driver.findElement(By.xpath("//input[@name='filtroCliente.nombre']"));
		nombreCliente.sendKeys("GABRIELA");

		final WebElement apellidopCliente = driver.findElement(By.xpath("//input[@name='filtroCliente.apellidoPaterno']"));
		apellidopCliente.sendKeys("RAMOS");

		final WebElement apellidomCliente = driver.findElement(By.xpath("//input[@name='filtroCliente.apellidoMaterno']"));
		apellidomCliente.sendKeys("IBARRA");

		final WebElement curpCliente = driver.findElement(By.id("curp"));
		curpCliente.sendKeys("RAIG731025MNLMBB04");

		final WebElement buscarClienteNombreBtn = driver.findElement(By.className("icon_buscar"));
		buscarClienteNombreBtn.click();

		final WebElement rowGridClient = driver.findElement(By.id("clienteGrid")).findElement(By.xpath("//td[contains(text(), '913998')]"));
		builder.doubleClick(rowGridClient).build().perform();

		final WebElement btnAceptar10 = driver.findElement(By.cssSelector("#mensajeBoton .b_aceptar a"));
		builder.moveToElement(btnAceptar10);
		builder.build().perform();
		btnAceptar10.click();

		final WebElement gridDatosInciso = driver.findElement(By.id("listadoIncisos"));
		//Element is not longer valid 
		wait.until(ExpectedConditions.visibilityOf(gridDatosInciso));

		final WebElement agregarDatosInsiso = driver.findElement(By.xpath("//*[@id='listadoIncisos']/div[2]/table/tbody/tr[2]/td[8]/a/img"));
		builder.moveToElement(agregarDatosInsiso);
		builder.perform();
		agregarDatosInsiso.click();

		final WebElement frame = (new WebDriverWait(driver, 10)).until(ExpectedConditions
				.presenceOfElementLocated(By.xpath("//div[contains(@class, 'dhtmlxWindowMainContent')]//iframe")));
		driver.switchTo().frame(frame);

		final WebElement nombreConductor = driver.findElement(By.id("nombre"));
		nombreConductor.sendKeys("Tania");
		final WebElement paternoConductor = driver.findElement(By.id("paterno"));
		paternoConductor.sendKeys("Montemayor");
		final WebElement maternoConductor = driver.findElement(By.id("materno"));
		maternoConductor.sendKeys("Peralta");
		final WebElement licenciaConductor = driver.findElement(By.id("licencia"));
		licenciaConductor.sendKeys("1867482574854");
		final WebElement fecNacConductor = driver.findElement(By.id("fechaNacimiento"));
		fecNacConductor.sendKeys("07");
		fecNacConductor.sendKeys("01");
		fecNacConductor.sendKeys("1991");
		final WebElement ocupacionConductor = driver.findElement(By.id("ocupacionConductor"));
		ocupacionConductor.sendKeys("practicante");

		final WebElement numMotor = driver.findElement(By.id("numMotor"));
		numMotor.sendKeys("18945154815656");
		final WebElement numPlaca = driver.findElement(By.id("numPlaca"));
		numPlaca.sendKeys("81565698866358");
		final WebElement numSerie = driver.findElement(By.id("numSerie"));
		numSerie.sendKeys("816638763361758");

		final WebElement bntGuardarDatosInciso = driver.findElement(By.id("btnGuardar"));
		builder.moveToElement(bntGuardarDatosInciso);
		builder.perform();
		bntGuardarDatosInciso.click();

		driver.switchTo().defaultContent();

		final WebElement detalleAsegurado = driver.findElement(By.xpath("//*[@id='listadoIncisos']/div[2]/table/tbody/tr[2]/td[9]/a/img"));
		builder.moveToElement(detalleAsegurado);
		builder.perform();
		detalleAsegurado.click();

		final WebElement frameDetalleAsegurado = (new WebDriverWait(driver, 10)).until(ExpectedConditions
				.presenceOfElementLocated(By.xpath("//div[contains(@class, 'dhtmlxWindowMainContent')]//iframe")));
		driver.switchTo().frame(frameDetalleAsegurado);

		final WebElement radioSoloNombre = driver.findElement(By.id("radioAsegurado2"));
		builder.moveToElement(radioSoloNombre);
		builder.perform();
		radioSoloNombre.click();

		final WebElement radioCapturaNombre = driver.findElement(By.id("nombreAsegurado"));
		radioCapturaNombre.sendKeys("Tania Montemayor Peralta");

		final WebElement guardarNombreAsegurado = driver.findElement(By.id("divGuardar"));
		builder.moveToElement(guardarNombreAsegurado);
		builder.perform();
		guardarNombreAsegurado.click();

		Alert alert = driver.switchTo().alert();
		alert.accept(); 

		driver.switchTo().defaultContent();

		final WebElement btnAceptar1 = driver.findElement(By.cssSelector("#mensajeBoton .b_aceptar a"));
		builder.moveToElement(btnAceptar1);
		builder.build().perform();
		btnAceptar1.click();

		final WebElement tabCobranza = driver.findElement(By.xpath("//span[text()='Cobranza']"));
		tabCobranza.click();

		final WebElement selectFormaPago = driver.findElement(By.id("medioPagoDTOs"));
		selectFormaPago.click();

		final Select selectEfectivo = new Select(driver.findElement(By.id("medioPagoDTOs")));
		selectEfectivo.selectByVisibleText("EFECTIVO");

		final WebElement guardarCobranza = driver.findElement(By.xpath("//div[contains(@class, 'guardarCobranza')]"));
		guardarCobranza.click();

		final WebElement btnAceptar2 = driver.findElement(By.cssSelector("#mensajeBoton .b_aceptar a"));
		builder.moveToElement(btnAceptar2);
		builder.build().perform();
		btnAceptar2.click();

		final WebElement tabComplEmision = driver.findElement(By.xpath("//span[text()='Complem. Emisión']"));
		builder.moveToElement(tabComplEmision);
		builder.build().perform();
		tabComplEmision.click();
		System.out.println("Cambio al tab Complem. Emisión");
		Thread.sleep(3000);

		final WebElement emitirIcon = driver.findElement(By.id("botonEmision")).findElement(By.tagName("a"));
		builder.moveToElement(emitirIcon);
		builder.perform();
		emitirIcon.click();

		Alert solicitarEmision = driver.switchTo().alert();
		solicitarEmision.accept(); 

		Thread.sleep(8000);
		
		final WebElement btnAceptar3 = driver.findElement(By.xpath("//a[contains(.,'Aceptar')]"));
		builder.moveToElement(btnAceptar3);
		builder.build().perform();
		btnAceptar3.click();
	}

	public void CotizarEmitir() throws Exception {

		new Login().testLogin();
		System.out.println("LOGIN CORRECTO 1/6");
		testAutosCotCotizacion();
		System.out.println("COTIZACION CREADA 2/6");
		buscarCotizacion();
		System.out.println("COTIZACION BUSQUEDA 3/6");
		editarCotizacion();
		System.out.println("COTIZACION EDITAR 4/6");
		buscarCotizacion();
		System.out.println("COTIZACION BUSQUEDA_ 5/6");
		complementarCotizacion();
		System.out.println("COTIZACION COMPLEMENTADA 6/6");
	}
}
