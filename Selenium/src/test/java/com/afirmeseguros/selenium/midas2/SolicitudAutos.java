package com.afirmeseguros.selenium.midas2;

import org.junit.Test;
import org.openqa.selenium.support.PageFactory;
import com.afirmeseguros.selenium.BasicTest;
import com.afirmeseguros.selenium.SolicitudAutosPage;

public class SolicitudAutos extends BasicTest {

	@Test
	public void solicitudAutosTest() throws Exception{
		testAutosSolicitudesNuevoTramitePF();
		testAutosSolicitudesBusquedaPF();
	}
	
	public void testAutosSolicitudesNuevoTramitePF() throws Exception {	
		new Inicio().menuEmisionAutosSolicitudes();
		SolicitudAutosPage solicitudAutosPage = PageFactory.initElements(driver, SolicitudAutosPage.class);
		clickElement(solicitudAutosPage.getNuevoTramiteRb());
		solicitudAutosPage.sendKeysNombreCliente("Tania");
		solicitudAutosPage.sendKeysPaternoCliente("Montemayor");
		solicitudAutosPage.sendKeysMaternoCliente("Peralta");
		solicitudAutosPage.sendKeysClaveAgente("92714");
		solicitudAutosPage.selectCodigoEjecutivo("ADRIAN JOSUE NOVA ALCAZAR");
		solicitudAutosPage.selectNegocios(NEGOCIO);
		solicitudAutosPage.selectProducto("AUTOMOVILES INDIVIDUALES");
		solicitudAutosPage.selectTiposSolicitud("PÓLIZA");
		solicitudAutosPage.inputCorreo("0tania.montemayor@afirme.com");
		clickElement(solicitudAutosPage.getBtnEnviar());
		clickElement(solicitudAutosPage.getBtnAceptar());
		solicitudAutosPage.putIdSolicitud();
	}
	
	public void testAutosSolicitudesBusquedaPF() throws Exception {
		new Inicio().menuEmisionAutosSolicitudes();
		SolicitudAutosPage solicitudAutosPage = PageFactory.initElements(driver, SolicitudAutosPage.class);
		solicitudAutosPage.validateTitle();
		solicitudAutosPage.getIdSolicitud();
		clickElement(solicitudAutosPage.getBtnBuscar());
	}
	
}
