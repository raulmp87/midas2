package com.afirmeseguros.selenium.midas2;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;
import com.afirmeseguros.selenium.BasicTest;
import com.afirmeseguros.selenium.LoginPage;

public class Login extends BasicTest {
	
	public void testLogin() throws Exception {
		driver.get(baseUrl + "/MidasWeb/portal/login.action");
		driver.findElement(By.id("form-username")).clear();
		driver.findElement(By.id("form-username")).sendKeys("VMCASPAL");
		driver.findElement(By.id("form-password")).clear();
		driver.findElement(By.id("form-password")).sendKeys("VMCASPAL");
		driver.findElement(By.cssSelector("button.btn")).click();
	}	
	
	@Test
	public void testLoginPF() throws Exception {
		  LoginPage loginPage = PageFactory.initElements(driver, LoginPage.class);
		  driver.get(baseUrl + "/MidasWeb/portal/login.action");
		  loginPage.EnterUserName("VMCASPAL");
		  loginPage.EnterPassword("VMCASPAL");
		  clickElement(loginPage.getLoginButton());

	}
}
