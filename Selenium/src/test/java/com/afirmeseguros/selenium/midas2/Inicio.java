package com.afirmeseguros.selenium.midas2;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import static org.junit.Assert.assertTrue;
import com.afirmeseguros.selenium.BasicTest;

public class Inicio extends BasicTest {
	
	public void entraMenu() throws Exception {
		new Login().testLogin();
		menuEmisionAutosSolicitudes();
		driver.navigate().refresh();
		menuEmisionAutosEmisionEmision();
		driver.navigate().refresh();
		testMenuVida();
		driver.navigate().refresh();
		mEmisionAutosCotCotizacion();
		driver.navigate().refresh();
		testMenuSiniestros();
		driver.navigate().refresh();
		menuProductos();
	}

	public void menuEmisionAutosSolicitudes() throws Exception {
		new Login().testLogin();
		hacerClicMenu("Emisión", "Autos", "Solicitudes");
		//final WebElement tituloSol = driver.findElement(By.className("titulo"));
		//assertTrue("Solicitud de Trámite".equals(tituloSol.getText()));
		//System.out.println(tituloSol.getText());
	}

	public void menuEmisionAutosEmisionEmision() throws Exception {
		new Login().testLogin();
		hacerClicMenu("Emisión", "Autos", "Emisión", "Emisión");
		//final WebElement tituloEmi = driver.findElement(By.className("titulo"));
		//assertTrue("Listado de Póliza Autos".equals(tituloEmi.getText()));
		//System.out.println(tituloEmi.getText());
	}
	
	@Test
	public void testMenuVida() throws Exception {
		new Login().testLogin();
		hacerClicMenu("Emisión", "Vida", "Configuración", "Tarifas", "Tarifas Móvil");
		final WebElement tituloTarMovil = driver.findElement(By.className("titulo"));
		System.out.println(tituloTarMovil.getText());
		assertTrue("Tarifas Móvil Vida".equals(tituloTarMovil.getText()));
	}

	@Test
	public void mEmisionAutosCotCotizacion() throws Exception {
		new Login().testLogin();
		hacerClicMenu("Emisión", "Autos", "Cotizaciones", "Cotización");
		//final WebElement tituloCotizacion = driver.findElement(By.className("titulo"));
		//assertTrue("Listado de Cotizaciones Autos".equals(tituloCotizacion.getText()));
		//System.out.println(tituloCotizacion.getText());
	}

	@Test
	public void testMenuSiniestros() throws Exception {
		new Login().testLogin();
		hacerClicMenu("Siniestros", "Autos", "Cabina", "Reporte de Cabina");
		final WebElement tituloSiniestros = driver.findElement(By.className("titulo"));
		assertTrue("Servicio de Cabina de Autos".equals(tituloSiniestros.getText()));
		System.out.println(tituloSiniestros.getText());
	}

	public void menuProductos() throws Exception {
		new Login().testLogin();
		hacerClicMenu("Producto", "Negocios", "Autos");
		final WebElement formListadoNegocios = driver.findElement(By.id("NegocioForm"));
		assertTrue("Listado de Negocios Autos".equals(formListadoNegocios.getText()));
		System.out.println(formListadoNegocios.getText());
	}
	
	public void menuCatalagoAjustadores() throws Exception {
		new Login().testLogin();
		hacerClicMenu("Siniestros", "Autos", "Catalogos", "Ajustadores");
		final WebElement tituloBusquedaAjustadores = driver.findElement(By.className("titulo"));
		assertTrue("Búsqueda de Ajustadores".equals(tituloBusquedaAjustadores.getText()));
		System.out.println(tituloBusquedaAjustadores.getText());
	}

	@Test
	public void menuView() throws Exception {
		
		new Login().testLogin();
		
		hacerClicMenu("Emisión");
		Thread.sleep(100);

		hacerClicMenu("Siniestros");
		Thread.sleep(100);

		hacerClicMenu("Producto");
		Thread.sleep(100);

		hacerClicMenu("Reportes");
		Thread.sleep(100);

		hacerClicMenu("Agentes");
		Thread.sleep(100);

		hacerClicMenu("Cobranza");
		Thread.sleep(100);
		
		hacerClicMenu("SAP AMIS");
		Thread.sleep(100);

		hacerClicMenu("Enlace");
		Thread.sleep(100);

		hacerClicMenu("Jurídico");
		Thread.sleep(100);

		hacerClicMenu("Proveedores");
		Thread.sleep(100);

		hacerClicMenu("Mesa de Control");
		Thread.sleep(100);
		
		hacerClicMenu("Condiciones Impresas");
		Thread.sleep(100);

		hacerClicMenu("Solvencia");
		Thread.sleep(100);

		hacerClicMenu("Compensaciones");
		Thread.sleep(100);

		hacerClicMenu("Ayuda");
		Thread.sleep(100);
	}

}
