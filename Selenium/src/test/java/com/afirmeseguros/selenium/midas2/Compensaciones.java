package com.afirmeseguros.selenium.midas2;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.afirmeseguros.selenium.BasicTest;

public class Compensaciones extends BasicTest {

	public void addCompensacionesAutos() throws Exception {

		new Inicio().menuProductos();
		new ProductoNegocios().buscarNegociosPF();
		new ProductoNegocios().editarNegocioPF();
		
		final WebElement scrollRight = driver.findElement(By.xpath("//div[@class='dhx_tab_scroll_right']"));
		builder.moveToElement(scrollRight);
		builder.build().perform();
		scrollRight.click();
		scrollRight.click();
		
		final WebElement pestañaCompensaciones = driver.findElement(By.xpath("//span[text()='Compensaciones']"));
		builder.moveToElement(pestañaCompensaciones);
		builder.build().perform();
		pestañaCompensaciones.click();
		
		String winHandleBefore = driver.getWindowHandle();
		
		final WebElement btnAceptarCompensaciones = driver.findElement(By.id("b_agregar"));
		builder.moveToElement(btnAceptarCompensaciones);
		builder.build().perform();
		btnAceptarCompensaciones.click();
		

		String winHandle = driver.getWindowHandle();
		    driver.switchTo().window(winHandle);
		
		final WebElement checkPrimaGral = driver.findElement(By.id("checkPrimaGral"));
		wait.until(ExpectedConditions.visibilityOf(checkPrimaGral));
		builder.moveToElement(checkPrimaGral);
		builder.build().perform();
		    
		if ( !driver.findElement(By.id("checkPrimaGral")).isSelected() ){
		     driver.findElement(By.id("checkPrimaGral")).click();
		   }
		
		final WebElement porcePrimaGral = driver.findElement(By.id("textPorcePrimaGral"));
		porcePrimaGral.sendKeys("50");
		final WebElement porcePrimaAgenGral = driver.findElement(By.id("textPorcePrimaAgenGral"));
		porcePrimaAgenGral.sendKeys("25");
		final WebElement porcePrimaPromoGral = driver.findElement(By.id("textPorcePrimaPromoGral"));
		porcePrimaPromoGral.sendKeys("50");
		final WebElement guardarConfigGral = driver.findElement(By.id("btnGuardarConfigGral"));
		guardarConfigGral.click();
		
		driver.switchTo().window(winHandleBefore);
		
		
	}
}
