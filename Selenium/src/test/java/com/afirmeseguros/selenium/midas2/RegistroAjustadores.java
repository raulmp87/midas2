package com.afirmeseguros.selenium.midas2;

import org.junit.Test;
import org.openqa.selenium.support.PageFactory;

import com.afirmeseguros.selenium.BasicTest;
import com.afirmeseguros.selenium.ajustadoresSiniestros.CatalagoAjustadoresPage;
import com.afirmeseguros.selenium.ajustadoresSiniestros.RegistroAjustadoresPage;

public class RegistroAjustadores extends BasicTest {
	
	@Test
	public void registrarAjustador() throws Exception{
		new Inicio().menuCatalagoAjustadores();
		CatalagoAjustadoresPage catalagoAjustadoresPage = PageFactory.initElements(driver, CatalagoAjustadoresPage.class);
		clickElement(catalagoAjustadoresPage.getAgregarBtn());
		RegistroAjustadoresPage registroAjustadoresPage = PageFactory.initElements(driver, RegistroAjustadoresPage.class);
		chooseOptionSelect(registroAjustadoresPage.getOficinaSelect(), "OFICINA PRUEBAS SINIESTROS");
		chooseOptionSelect(registroAjustadoresPage.getEstatusSelect(), "Activo");
		sendKeysElement(registroAjustadoresPage.getNombrePersonaInput(), "PABLO");
		sendKeysElement(registroAjustadoresPage.getApellidoPaternoInput(), "ALMAGUER");
		sendKeysElement(registroAjustadoresPage.getApellidoMaternoInput(), "ALANIS");
		sendKeysElement(registroAjustadoresPage.getClaveUsuarioInput(), "PDAA");
		chooseOptionSelect(registroAjustadoresPage.getTipoAjustadorSelect(), "INTERNO");
		chooseOptionSelect(registroAjustadoresPage.getCertificacionSelect(), "Si");
		chooseOptionSelect(registroAjustadoresPage.getPaisSelect(), "MEXICO");
		chooseOptionSelect(registroAjustadoresPage.getEstadoSelect(), "NUEVO LEON");
		chooseOptionSelect(registroAjustadoresPage.getMunicipioSelect(), "JUAREZ");
		chooseOptionSelect(registroAjustadoresPage.getColoniaSelect(), "A LOS REYES");
		sendKeysElement(registroAjustadoresPage.getCalleInput(), "AMANECER 556");
		sendKeysElement(registroAjustadoresPage.getNumExtInput(), "12369");
		sendKeysElement(registroAjustadoresPage.getTelCasaLadaInput(), "81");
		sendKeysElement(registroAjustadoresPage.getTelCasaInput(), "14548788");
		sendKeysElement(registroAjustadoresPage.getCorreoPrincipalInput(), "0tania.montemayor@afirme.com");
		sendKeysElement(registroAjustadoresPage.getRfcInput(), "AAAP970308P65");
		clickElement(registroAjustadoresPage.getAgregarBtn());
		clickElement(registroAjustadoresPage.getAgregarBtn());
		
	}
	

}
