package com.afirmeseguros.selenium.midas2;

import java.util.List;
import java.util.Random;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Test;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
//import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;

import com.afirmeseguros.selenium.BasicTest;
import com.afirmeseguros.selenium.cotizacion.AgregarIncisoPage;
import com.afirmeseguros.selenium.cotizacion.ClienteUnicoEmisionPage;
//import com.afirmeseguros.selenium.cotizacion.ClienteUnicoEmisionPage;
import com.afirmeseguros.selenium.cotizacion.CobranzaPage;
import com.afirmeseguros.selenium.cotizacion.ComplementarCotizacionPage;
import com.afirmeseguros.selenium.cotizacion.ComplementarDatosAseguradoPage;
import com.afirmeseguros.selenium.cotizacion.ComplementarDatosVehiculoPage;
import com.afirmeseguros.selenium.cotizacion.ConsultaClientesPage;
import com.afirmeseguros.selenium.cotizacion.CrearCotizacionPage;
import com.afirmeseguros.selenium.cotizacion.EditarCotizacionUnoPage;
import com.afirmeseguros.selenium.cotizacion.ListadoCotizacionesAutosPage;
import com.afirmeseguros.selenium.cotizacion.MensajePage;
import com.afirmeseguros.selenium.cotizacion.SeleccionAgentePage;

public class CotizacionAutosPF extends BasicTest {
	
	protected static String numRdm;
	protected static String rand;

	@Test
	public void cotizacionCompleta() throws Exception{
		crearCotizacion();
		buscarCotizacion();
		editarCotizacion();
		agregarInciso();
		buscarCotizacion();
		complementarCotizacion();
	}
	
	public void crearCotizacion() throws Exception{
		new Inicio().mEmisionAutosCotCotizacion();
		ListadoCotizacionesAutosPage listadoCotizacionesAutosPage = PageFactory.initElements(driver, ListadoCotizacionesAutosPage.class);
		clickElement(listadoCotizacionesAutosPage.getIndividualFlotillasBtn());
		changeToFrame();
		CrearCotizacionPage crearCotizacionPage = PageFactory.initElements(driver, CrearCotizacionPage.class);
		clickElement(crearCotizacionPage.getSeleccionarAgenteBtn());
		changeToDefalut();
		SeleccionAgentePage seleccionAgentePage = PageFactory.initElements(driver, SeleccionAgentePage.class);
		changeToFrame2();
		seleccionAgentePage.sendKeysAgenteBusquda("Federico de Hoyos Navarro");
		clickElement(seleccionAgentePage.getAgenteSeleccionado());
		changeToDefalut();
		changeToFrame();
		crearCotizacionPage.selectNegocio(NEGOCIO);
		crearCotizacionPage.selectProducto("AUTOMOVILES INDIVIDUALES");
		crearCotizacionPage.selectTipoPoliza("AUTO INDIVIDUAL");
		crearCotizacionPage.selectMoneda("NACIONAL");
		clickElement(crearCotizacionPage.getCrearCotizacionBtn());
		changeToDefalut();
		EditarCotizacionUnoPage editarCotizacionUnoPage = PageFactory.initElements(driver, EditarCotizacionUnoPage.class);
		editarCotizacionUnoPage.waitElementPage();
		crearCotizacionPage.putNumCotizacion();
		
	}
	
	public void buscarCotizacion() throws Exception{
		driver.navigate().refresh();
		hacerClicMenu("Emisión", "Autos", "Cotizaciones", "Cotización"); //aqui --
		ListadoCotizacionesAutosPage listadoCotizacionesAutosPage = PageFactory.initElements(driver, ListadoCotizacionesAutosPage.class);
		sendKeysElement(listadoCotizacionesAutosPage.getIdToCotizacionInput(), DATA.get("idToCotizacion"));
		clickElement(listadoCotizacionesAutosPage.getBuscarSubmitBtn());
		
	}
	
	public void editarCotizacion() throws Exception{
		ListadoCotizacionesAutosPage listadoCotizacionesAutosPage = PageFactory.initElements(driver, ListadoCotizacionesAutosPage.class);
		clickElement(listadoCotizacionesAutosPage.getEditarCotizacionIco());
		EditarCotizacionUnoPage editarCotizacionUnoPage = PageFactory.initElements(driver, EditarCotizacionUnoPage.class);
		chooseOptionSelect(editarCotizacionUnoPage.getFormaPagoSelect(), "MENSUAL");
		sendKeysElement(editarCotizacionUnoPage.getFechaSeguimientoDate(), "12");
		sendKeysElement(editarCotizacionUnoPage.getFechaSeguimientoDate(), "06");
		sendKeysElement(editarCotizacionUnoPage.getFechaSeguimientoDate(), "2017");
		sendKeysElement(editarCotizacionUnoPage.getPorcentajePagoFraccionadoInput(), "10");
		clickElement(editarCotizacionUnoPage.getGuardarIconBtn());

	}
	
	public void agregarInciso() throws Exception{
		EditarCotizacionUnoPage editarCotizacionUnoPage = PageFactory.initElements(driver, EditarCotizacionUnoPage.class);
		clickElement(editarCotizacionUnoPage.getMasAgregarIconBtn());
		changeToFrame(); //aqui
		AgregarIncisoPage agregarIncisoPage = PageFactory.initElements(driver, AgregarIncisoPage.class);
		chooseOptionSelect(agregarIncisoPage.getEstadoSelect(), "CHIAPAS");
		chooseOptionSelect(agregarIncisoPage.getMunicipioSelect(), "ALTAMIRANO");
		chooseOptionSelect(agregarIncisoPage.getLineaNegocioSelect(), "AUTOMOVILES INDIVIDUALES");
		chooseOptionSelect(agregarIncisoPage.getMarcaSelect(), "FORD");
		Thread.sleep(800);
		
		System.out.println("Seleccionando Estilo");
		WebElement mySelectElm = driver.findElement(By.id("incisoCotizacion.incisoAutoCot.estiloId")); 
		Select mySelect= new Select(mySelectElm);
		Thread.sleep(1400);
		List<WebElement> listaEstilos = mySelect.getOptions();
		int num = listaEstilos.size();
		System.out.println("____________________________________________________________");
		System.out.println("Total Size Estilo:" + num);
		System.out.println("____________________________________________________________");
		System.out.println("Lista de Estilos:" + listaEstilos);
		System.out.println("____________________________________________________________");
		Random r = new Random();
		int randomValue = r.nextInt(listaEstilos.size());
		System.out.println("Random Size Estilo:" +randomValue);
		mySelect.selectByIndex(randomValue);
		System.out.println("Termino de seleccionar Estilo");
		
		System.out.println("Seleccionando Modelo");
		WebElement mySelectElmModel = driver.findElement(By.id("incisoCotizacion.incisoAutoCot.modeloVehiculo")); 
		Select mySelectModel= new Select(mySelectElmModel);
		Thread.sleep(800);
		List<WebElement> optionsModel = mySelectModel.getOptions();
		int randomValueModel = r.nextInt(optionsModel.size());
		mySelectModel.selectByIndex(randomValueModel);
		Thread.sleep(600);
		
		/*
		chooseOptionSelect(agregarIncisoPage.getEstiloSelect(), "03256 - FD (C1A) FUSION SEL 3.0L GPS SYNC LN V6 AUT 4P ABS VP CQ CB");
		for (WebElement option : optionsModel) {
		    System.out.println(option.getText()); 
		}
		chooseOptionSelect(agregarIncisoPage.getModeloSelect(), "2010");*/
	
		chooseOptionSelect(agregarIncisoPage.getPaqueteSelect(), "AMPLIA"); //aqui --
		clickElement(agregarIncisoPage.getCalcularBtn());
		new AgregarIncisoPage().clickElementEmitirBtn(agregarIncisoPage.getGuardarIconBtn());
		changeToDefalut();
		new EditarCotizacionUnoPage().clickElementEsp(editarCotizacionUnoPage.getTerminanCotizacionBtn());
		MensajePage mensajePage = PageFactory.initElements(driver, MensajePage.class);
		clickElement(mensajePage.getAceptarMensajeBtn());
	}
	
	public void complementarCotizacion()throws Exception{
		ListadoCotizacionesAutosPage listadoCotizacionesAutosPage = PageFactory.initElements(driver, ListadoCotizacionesAutosPage.class);
		clickElement(listadoCotizacionesAutosPage.getComplementarCotizacionIco());
		ComplementarCotizacionPage complementarCotizacionPage = PageFactory.initElements(driver, ComplementarCotizacionPage.class);
		new ComplementarCotizacionPage().clickElementEmitirBtn(complementarCotizacionPage.getBuscarCliente());
		ConsultaClientesPage consultaClientesPage = PageFactory.initElements(driver, ConsultaClientesPage.class);
		sendKeysElement(consultaClientesPage.getNombreCliente(), "GABRIELA");
		sendKeysElement(consultaClientesPage.getaPaternoCliente(), "RAMOS");
		sendKeysElement(consultaClientesPage.getaMaternoCliente(), "IBARRA");
		sendKeysElement(consultaClientesPage.getCurpCliente(), "RAIG731025MNLMBB04");
		clickElement(consultaClientesPage.getBuscarBtn());
		doubleClick(consultaClientesPage.getClienteSeleccionado());
		MensajePage mensajePage = PageFactory.initElements(driver, MensajePage.class);
		clickElement(mensajePage.getAceptarMensajeBtn());
		clickElement(complementarCotizacionPage.getAgregarDatosIncisoIco());
		changeToFrame(); 
		ComplementarDatosVehiculoPage complementarDatosVehiculo = PageFactory.initElements(driver, ComplementarDatosVehiculoPage.class);
		sendKeysElement(complementarDatosVehiculo.getNombreConductorInput(), "TANIA");
		sendKeysElement(complementarDatosVehiculo.getaPaternoConductorInput(), "MONTEMAYOR");
		sendKeysElement(complementarDatosVehiculo.getaMaternoConductorInput(), "PERALTA");
		sendKeysElement(complementarDatosVehiculo.getNoLicenciaConductorInput(), "25627548745485");
		sendKeysElement(complementarDatosVehiculo.getFechaNacConductor(), "10");
		sendKeysElement(complementarDatosVehiculo.getFechaNacConductor(), "11");
		sendKeysElement(complementarDatosVehiculo.getFechaNacConductor(), "1996");
		clickElement(complementarDatosVehiculo.getIconoFechaNacConductor());
		sendKeysElement(complementarDatosVehiculo.getOcupacionConductorInput(), "Estudiante");
		sendKeysElement(complementarDatosVehiculo.getNumMotorInput(), "156245746284");
		sendKeysElement(complementarDatosVehiculo.getNumPlacaInput(), "699331246284");
		numRdm = RandomStringUtils.random(10, false, true);
		System.out.println(numRdm);
		sendKeysElement(complementarDatosVehiculo.getNumSerieInput(), numRdm);
		clickElement(complementarDatosVehiculo.getGuardarBtn());
		Thread.sleep(3000);
		changeToDefalut();
		clickElement(complementarCotizacionPage.getVerDetalleAseguradoIco());
		changeToFrame(); 
		ComplementarDatosAseguradoPage complementarDatosAsegurado = PageFactory.initElements(driver, ComplementarDatosAseguradoPage.class);
		clickElement(complementarDatosAsegurado.getSoloNombreRb());
		sendKeysElement(complementarDatosAsegurado.getNombreAseguradoInput(), "Tania Montemayor Peralta");
		clickElement(complementarDatosAsegurado.getDivGuardarBtn());
		Alert alert = driver.switchTo().alert();
		alert.accept();
		changeToDefalut();
		clickElement(mensajePage.getAceptarMensajeBtn());
		clickElement(complementarCotizacionPage.getCobranzaTab());
		CobranzaPage cobranzaPage = PageFactory.initElements(driver, CobranzaPage.class);
		chooseOptionSelect(cobranzaPage.getMedioPagoDTOsSelect(), "EFECTIVO");
		clickElement(cobranzaPage.getGuardarCobranzaBtn());
		clickElement(mensajePage.getAceptarMensajeBtn()); 
		clickElement(complementarCotizacionPage.getComplementarEmisionTab());
		clickElement(complementarCotizacionPage.getEmitirBtn());
		Alert solicitarEmision = driver.switchTo().alert();
		solicitarEmision.accept(); 
		changeToDefalut();
		ClienteUnicoEmisionPage clienteUnicoEmisionPage = PageFactory.initElements(driver, ClienteUnicoEmisionPage.class);
		
		if(ExpectedConditions.visibilityOf(clienteUnicoEmisionPage.getEntrevistaForm()) == clienteUnicoEmisionPage.getEntrevistaForm()){
			sendKeysElement(clienteUnicoEmisionPage.getPrimaEstimadaInput(), "23000");
			clickElement(clienteUnicoEmisionPage.getPesosMnRb());
			sendKeysElement(clienteUnicoEmisionPage.getNumSerieFielInput(), "156784523655244");
			clickElement(clienteUnicoEmisionPage.getPepFalseRb());
			clickElement(clienteUnicoEmisionPage.getCuentaPropiaSiRb());
			clickElement(clienteUnicoEmisionPage.getGuardarBtn());
			Thread.sleep(300);
			changeToDefalut();
			clickElement(complementarCotizacionPage.getEmitirBtn());
			solicitarEmision.accept(); 
			changeToDefalut();
		}
		mensajePage.waitMensajeGlobal();
		clickElement(mensajePage.getCancelarMensajeBtn());
	}
	
}
