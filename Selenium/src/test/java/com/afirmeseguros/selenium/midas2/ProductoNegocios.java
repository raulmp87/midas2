package com.afirmeseguros.selenium.midas2;

import org.junit.Test;
import org.openqa.selenium.support.PageFactory;
import com.afirmeseguros.selenium.BasicTest;
import com.afirmeseguros.selenium.ProductoNegociosPage;

public class ProductoNegocios extends BasicTest{	
	
	@Test
	public void testEditarNegocioProducto() throws Exception {
		
		new Inicio().menuProductos();
		buscarNegociosPF();
 		editarNegocioPF();
		editarProductoPF();
	}
	
	public void buscarNegociosPF() throws Exception {
		
		ProductoNegociosPage productoNegociosPage = PageFactory.initElements(driver, ProductoNegociosPage.class);
		productoNegociosPage.sendKeysNombreNegocio();
		//productoNegociosPage.clickOrdenarNombres();
		clickElement(productoNegociosPage.getOrdenarNegocios());
		clickElement(productoNegociosPage.getNegocioSeleccion());
	}

	public void editarNegocioPF() throws Exception {
		ProductoNegociosPage productoNegociosPage = PageFactory.initElements(driver, ProductoNegociosPage.class);
		clickElement(productoNegociosPage.getEditarIcon());
		//productoNegociosPage.validateTitleListado();
	}
	
	public void editarProductoPF() throws Exception {	
		ProductoNegociosPage productoNegociosPage = PageFactory.initElements(driver, ProductoNegociosPage.class);
		clickElement(productoNegociosPage.getEditarIconProductoInd());
		//productoNegociosPage.validateTitleEditarProducto();
	}
		
}
