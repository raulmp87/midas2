package com.afirmeseguros.selenium;

import static org.junit.Assert.assertTrue;
import java.util.HashMap;
import java.util.Map;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;

public class SolicitudAutosPage extends BasicTest {
	
	public static final Map<String, String> DATASOL = new HashMap<String, String>();
	
	@FindBy(how=How.CLASS_NAME, using="titulo")
	private WebElement tituloSol;

	@FindBy(how=How.ID, using = "seleccion1")
	private WebElement busquedaTramiteRb;
	
	@FindBy(how=How.XPATH, using = "//input[@id='seleccion2']")
	public WebElement nuevoTramiteRb;
	
	@FindBy(how=How.ID, using = "solicitud_claveTipoPersona1")
	private WebElement personaFisicaRb;
	
	@FindBy(how=How.ID, using = "solicitud_claveTipoPersona2")
	private WebElement personaMoralRb;
	
	@FindBy(how=How.ID, using = "idToSolicitud")
	private WebElement inputSol;

	@FindBy(how=How.ID, using = "solicitud.codigoUsuarioAsignacion")
	private WebElement inputUsuarioAsigacion;

	@FindBy(how=How.ID, using = "nombrePersona")
	private WebElement inputNombreCliente;
	
	@FindBy(how=How.ID, using = "apellidoPaterno")
	private WebElement inputPaternoCliente;
	
	@FindBy(how=How.ID, using = "apellidoMaterno")
	private WebElement inputMaternoCliente;
	
	@FindBy(how=How.ID, using = "idTcAgente")
	private WebElement inputClaveAgente;
	
	@FindBy(how=How.ID, using = "codigoEjecutivo")
	private WebElement selectCodigoEjecutivo;
	
	@FindBy(how=How.ID, using = "codigoAgente")
	private WebElement selectCodigoAgente;
	
	@FindBy(how=How.ID, using = "negocios")
	private WebElement selectNegocios;
	
	@FindBy(how=How.ID, using = "idToProducto")
	private WebElement selectProducto;
	
	@FindBy(how=How.ID, using = "tiposSolicitud")
	private WebElement selectTiposSolicitud;
	
	@FindBy(how=How.ID, using = "claveEstatus")
	private WebElement selectclaveEstatus;
	
	@FindBy(how=How.ID, using = "fechaCreacion")
	private WebElement inputFechaCreacion;
	
	@FindBy(how=How.ID, using = "fechaFinal")
	private WebElement inputFechaFinal;
	
	@FindBys({@FindBy(id="wwctrl_emailContactos"), @FindBy(id="emailContactos")})
	private WebElement inputCorreo;
	
	@FindBy(how=How.ID, using = "divFiltrosBtn")
	private WebElement btnMostrarFiltros;
	
	@FindBy(how=How.ID, using = "divLimpiarBtn")
	private WebElement btnLimpiar;
	
	@FindBy(how=How.ID, using = "divBuscarBtn")
	private WebElement btnBuscar;
	
	@FindBy(how=How.ID, using = "divEnviarBtn")
	private WebElement btnEnviar;
	
	@FindBy(how=How.ID, using = "solicitudesPolizaGrid")
	private WebElement solicitudesPolizaGrid;

	@FindBy(how=How.CLASS_NAME, using = "btn_back w220")
	private WebElement exportarExcelBtn;
	
	//wait
	@FindBy(how=How.CLASS_NAME, using = "dhtmlx_window_active")
	private WebElement mensajeSolicitud;
	
	//Get value
	@FindBy(how=How.ID, using = "idToSolicitud")
	private WebElement idToSolicitud;
	
	@FindBy(how = How.CSS, using = "#mensajeBoton .b_aceptar a")
	private WebElement btnAceptar;
	
	public void setBusquedaTramiteRb(WebElement busquedaTramiteRb) {
		this.busquedaTramiteRb = busquedaTramiteRb;
	}
	
	public WebElement getBusquedaTramiteRb() {
		return busquedaTramiteRb;
	}
	public WebElement getNuevoTramiteRb() {
		return nuevoTramiteRb;
	}

	public void setNuevoTramiteRb(WebElement nuevoTramiteRb) {
		this.nuevoTramiteRb = nuevoTramiteRb;
	}

	public WebElement getPersonaFisicaRb() {
		return personaFisicaRb;
	}

	public void setPersonaFisicaRb(WebElement personaFisicaRb) {
		this.personaFisicaRb = personaFisicaRb;
	}

	public WebElement getPersonaMoralRb() {
		return personaMoralRb;
	}

	public void setPersonaMoralRb(WebElement personaMoralRb) {
		this.personaMoralRb = personaMoralRb;
	}

	public WebElement getBtnMostrarFiltros() {
		return btnMostrarFiltros;
	}

	public void setBtnMostrarFiltros(WebElement btnMostrarFiltros) {
		this.btnMostrarFiltros = btnMostrarFiltros;
	}

	public WebElement getBtnLimpiar() {
		return btnLimpiar;
	}

	public void setBtnLimpiar(WebElement btnLimpiar) {
		this.btnLimpiar = btnLimpiar;
	}

	public WebElement getBtnBuscar() {
		return btnBuscar;
	}

	public void setBtnBuscar(WebElement btnBuscar) {
		this.btnBuscar = btnBuscar;
	}

	public WebElement getBtnEnviar() {
		return btnEnviar;
	}

	public void setBtnEnviar(WebElement btnEnviar) {
		this.btnEnviar = btnEnviar;
	}
	
	public WebElement getBtnAceptar() {
		return btnAceptar;
	}

	public void setBtnAceptar(WebElement btnAceptar) {
		this.btnAceptar = btnAceptar;
	}


	
	//---------------------------------------------------------------------------------------//
	
	public void verMensajeSolicitud(){
		wait.until(ExpectedConditions.visibilityOf(mensajeSolicitud));
	}
	
	public void putIdSolicitud(){
		DATASOL.put("idToSolicitud", idToSolicitud.getAttribute("value"));
		System.out.println(idToSolicitud.getAttribute("value"));
		wait.until(ExpectedConditions.attributeContains(idToSolicitud, "value", idToSolicitud.getAttribute("value")));
	}
	
	public void getIdSolicitud(){
		inputSol.sendKeys(DATASOL.get("idToSolicitud"));
	}

	public void validateTitle(){
		assertTrue("Solicitud de Trámite".equals(tituloSol.getText()));
		System.out.println(tituloSol.getText());
	}
	
	public void clickElement(WebElement element){
		element.click();
	}
		
	public void sendKeysIdSol(String idSol){
		inputSol.sendKeys(idSol);
	}
	
	public void sendKeysUsuarioAsig(String codigoUsAs){
		inputUsuarioAsigacion.sendKeys(codigoUsAs);
	}
	
	public void sendKeysNombreCliente(String nombreCliente){
		inputNombreCliente.sendKeys(nombreCliente);
	}
	
	public void sendKeysPaternoCliente(String paternoCliente){
		inputPaternoCliente.sendKeys(paternoCliente);
	}
	
	public void sendKeysMaternoCliente(String maternoCliente){
		inputMaternoCliente.sendKeys(maternoCliente);
	}
	
	public void sendKeysClaveAgente(String claveAgente){
		inputClaveAgente.sendKeys(claveAgente);
	}
	
	public void selectCodigoEjecutivo(String ejecutivo){
		Select dropdown = new Select(selectCodigoEjecutivo);
		dropdown.selectByVisibleText(ejecutivo);
	}
	
	public void selectCodigoAgente(String agente){
		Select dropdown = new Select(selectCodigoAgente);
		dropdown.selectByVisibleText(agente);
	}
	
	public void selectNegocios(String negocio){
		Select dropdown = new Select(selectNegocios);
		dropdown.selectByVisibleText(negocio);
	}
	
	public void selectProducto(String producto){
		Select dropdown = new Select(selectProducto);
		dropdown.selectByVisibleText(producto);
	}
	
	public void selectTiposSolicitud(String tiposSolicitud){
		Select dropdown = new Select(selectTiposSolicitud);
		dropdown.selectByVisibleText(tiposSolicitud);
	}
	
	public void inputFechaCreacion(String dia, String mes, String anio){
		inputFechaCreacion.sendKeys(dia);
		inputFechaCreacion.sendKeys(mes);
		inputFechaCreacion.sendKeys(anio);
	}
	
	public void inputFechaFinal(String dia, String mes, String anio){
		inputFechaFinal.sendKeys(dia);
		inputFechaFinal.sendKeys(mes);
		inputFechaFinal.sendKeys(anio);
	}
	
	public void inputCorreo(String correoDestinatario){
		inputCorreo.sendKeys(correoDestinatario);
	}
	
}


