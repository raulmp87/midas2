package com.afirmeseguros.selenium.ajustadoresSiniestros;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import com.afirmeseguros.selenium.BasicTest;

public class CatalagoAjustadoresPage extends BasicTest {

	@FindBy(how = How.ID, using = "noAbogadoAjustadorId")
	private WebElement noAbogadoAjustadorInput;
	
	@FindBy(how = How.ID, using = "noPrestador")
	private WebElement noPrestadorInput;
	
	@FindBy(how = How.ID, using = "oficinaId")
	private WebElement oficinaSelect;
	
	@FindBy(how = How.ID, using = "nombreAbogadoAjustador")
	private WebElement nombreAbogadoAjustadorInput;
	
	@FindBy(how = How.ID, using = "nombrePrestador")
	private WebElement nombrePrestadorInput;
	
	@FindBy(how = How.ID, using = "estatus")
	private WebElement estatusSelect;
	
	@FindBy(how = How.ID, using = "tipoAbogadoAjustador")
	private WebElement tipoAbogadoAjustadorSelect;
	
	@FindBy(how = How.XPATH, using = "//a[@onclick='limpiar();']")
	private WebElement limpiarBtn;
	
	@FindBy(how = How.XPATH, using = "//a[@onclick='busquedaServicioSiniestro();']")
	private WebElement buscarBtn;
	
	@FindBy(how = How.XPATH, using = "//div[contains(.,Listado de Ajustadores')]")
	private WebElement tituloListadoAjustadores;
	
	@FindBy(how = How.XPATH, using = "//a[contains(@onclick,'muestraAlta();')]")
	private WebElement agregarBtn;
	
	@FindBy(how = How.XPATH, using = "//a[@onclick='exportarExcel();']")
	private WebElement exportarExcel;

	//---------------------------------GETTERS & SETTERS-----------------------------------//
	
	public WebElement getNoAbogadoAjustadorInput() {
		return noAbogadoAjustadorInput;
	}

	public void setNoAbogadoAjustadorInput(WebElement noAbogadoAjustadorInput) {
		this.noAbogadoAjustadorInput = noAbogadoAjustadorInput;
	}

	public WebElement getNoPrestadorInput() {
		return noPrestadorInput;
	}

	public void setNoPrestadorInput(WebElement noPrestadorInput) {
		this.noPrestadorInput = noPrestadorInput;
	}

	public WebElement getOficinaSelect() {
		return oficinaSelect;
	}

	public void setOficinaSelect(WebElement oficinaSelect) {
		this.oficinaSelect = oficinaSelect;
	}

	public WebElement getNombreAbogadoAjustadorInput() {
		return nombreAbogadoAjustadorInput;
	}

	public void setNombreAbogadoAjustadorInput(WebElement nombreAbogadoAjustadorInput) {
		this.nombreAbogadoAjustadorInput = nombreAbogadoAjustadorInput;
	}

	public WebElement getNombrePrestadorInput() {
		return nombrePrestadorInput;
	}

	public void setNombrePrestadorInput(WebElement nombrePrestadorInput) {
		this.nombrePrestadorInput = nombrePrestadorInput;
	}

	public WebElement getEstatusSelect() {
		return estatusSelect;
	}

	public void setEstatusSelect(WebElement estatusSelect) {
		this.estatusSelect = estatusSelect;
	}

	public WebElement getTipoAbogadoAjustadorSelect() {
		return tipoAbogadoAjustadorSelect;
	}

	public void setTipoAbogadoAjustadorSelect(WebElement tipoAbogadoAjustadorSelect) {
		this.tipoAbogadoAjustadorSelect = tipoAbogadoAjustadorSelect;
	}

	public WebElement getLimpiarBtn() {
		return limpiarBtn;
	}

	public void setLimpiarBtn(WebElement limpiarBtn) {
		this.limpiarBtn = limpiarBtn;
	}

	public WebElement getBuscarBtn() {
		return buscarBtn;
	}

	public void setBuscarBtn(WebElement buscarBtn) {
		this.buscarBtn = buscarBtn;
	}

	public WebElement getTituloListadoAjustadores() {
		return tituloListadoAjustadores;
	}

	public void setTituloListadoAjustadores(WebElement tituloListadoAjustadores) {
		this.tituloListadoAjustadores = tituloListadoAjustadores;
	}

	public WebElement getAgregarBtn() {
		return agregarBtn;
	}

	public void setAgregarBtn(WebElement agregarBtn) {
		this.agregarBtn = agregarBtn;
	}

	public WebElement getExportarExcel() {
		return exportarExcel;
	}

	public void setExportarExcel(WebElement exportarExcel) {
		this.exportarExcel = exportarExcel;
	}
	
}
