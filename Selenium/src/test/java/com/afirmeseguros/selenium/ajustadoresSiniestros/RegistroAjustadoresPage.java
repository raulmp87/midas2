package com.afirmeseguros.selenium.ajustadoresSiniestros;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class RegistroAjustadoresPage {
	
	//----------------------------------Datos del Ajustador------------------------------------//
	
	@FindBy(how = How.XPATH, using = "//div[contains(.,'Datos del Ajustador')]")
	private WebElement tituloDatosAjustador;
	
	@FindBy(how = How.ID, using = "id")
	private WebElement noAjustador;
	
	@FindBy(how = How.ID, using = "oficinaId")
	private WebElement oficinaSelect;
	
	@FindBy(how = How.ID, using = "r_tipoPersonaPM")
	private WebElement personaMoralRb;
	
	@FindBy(how = How.ID, using = "r_tipoPersonaPF")
	private WebElement personaFisicaRb;

	@FindBy(how = How.ID, using = "estatus")
	private WebElement estatusSelect;
	
	@FindBy(how = How.ID, using = "nombrePersona")
	private WebElement nombrePersonaInput;
	
	@FindBy(how = How.ID, using = "apellidoPaterno")
	private WebElement apellidoPaternoInput;
	
	@FindBy(how = How.ID, using = "apellidoMaterno")
	private WebElement apellidoMaternoInput;
	
	@FindBy(how = How.ID, using = "claveUsuario")
	private WebElement claveUsuarioInput;
	
	@FindBy(how = How.ID, using = "txt_nombre_de_la_empresa")
	private WebElement nombreEmpresaInput;
	
	@FindBy(how = How.ID, using = "txt_nombre_comercial")
	private WebElement nombreComercialInput;
	
	@FindBy(how = How.ID, using = "txt_administrador")
	private WebElement administradorInput;
	
	@FindBy(how = How.ID, using = "altaModificacionServcio_servicioSiniestroDto_fechaActivo")
	private WebElement fechaActivoInput;
	
	@FindBy(how = How.ID, using = "altaModificacionServcio_servicioSiniestroDto_fechaInactivo")
	private WebElement fechaInactivoInput;
	
	@FindBy(how = How.ID, using = "ambitoPrestadorServicio")
	private WebElement tipoAjustadorSelect;
	
	@FindBy(how = How.ID, using = "prestadorServicioId")
	private WebElement prestadorServicioIdInput;
	
	@FindBy(how = How.ID, using = "certificacion")
	private WebElement certificacionSelect;
	
	//----------------------------------Direccion del Ajustador------------------------------------//
	
	@FindBy(how = How.ID, using = "servicioSiniestroDto.idPaisName")
	private WebElement paisSelect;
	
	@FindBy(how = How.ID, using = "servicioSiniestroDto.idEstadoName")
	private WebElement estadoSelect;
	
	@FindBy(how = How.ID, using = "servicioSiniestroDto.idCiudadName")
	private WebElement municipioSelect;
	
	@FindBy(how = How.ID, using = "servicioSiniestroDto.idColoniaName")
	private WebElement coloniaSelect;
	
	@FindBy(how = How.ID, using = "servicioSiniestroDto.cpName")
	private WebElement cpInput;
	
	@FindBy(how = How.ID, using = "idColoniaCheck")
	private WebElement coloniaCheck;
	
	@FindBy(how = How.ID, using = "servicioSiniestroDto.nuevaColoniaName")
	private WebElement nuevaColoniaInput;
	
	@FindBy(how = How.ID, using = "servicioSiniestroDto.calleName")
	private WebElement calleInput;
	
	@FindBy(how = How.ID, using = "servicioSiniestroDto.numeroName")
	private WebElement numExtInput;
	
	@FindBy(how = How.ID, using = "servicioSiniestroDto.numeroIntName")
	private WebElement numIntInput;
	
	@FindBy(how = How.ID, using = "servicioSiniestroDto.referencia")
	private WebElement referenciaInput;
	
	@FindBy(how = How.ID, using = "telCasaLada")
	private WebElement telCasaLadaInput;
	
	@FindBy(how = How.ID, using = "telCasa")
	private WebElement telCasaInput;
	
	@FindBy(how = How.ID, using = "telCasaLada2")
	private WebElement telCasaLada2Input;
	
	@FindBy(how = How.ID, using = "telCasa2")
	private WebElement telCasa2Input;

	@FindBy(how = How.ID, using = "telOtro2Lada")
	private WebElement telOtro2LadaInput;
	
	@FindBy(how = How.ID, using = "telOtro2")
	private WebElement telOtro2Input;
	
	@FindBy(how = How.ID, using = "telCelularLada")
	private WebElement telCelularLadaInput;
	
	@FindBy(how = How.ID, using = "telCelular")
	private WebElement telCelularInput;
	
	@FindBy(how = How.ID, using = "correoPrincipal")
	private WebElement correoPrincipalInput;
	
	@FindBy(how = How.ID, using = "rfc")
	private WebElement rfcInput;
	
	@FindBy(how = How.ID, using = "altaModificacionServcio_servicioSiniestroDto_codigoUnidad")
	private WebElement codigoUnidadInput;
	
	@FindBy(how = How.ID, using = "altaModificacionServcio_servicioSiniestroDto_placas")
	private WebElement placasInput;
	
	@FindBy(how = How.ID, using = "altaModificacionServcio_servicioSiniestroDto_descripcionUnidad")
	private WebElement descripcionInput;
	
	@FindBy(how = How.XPATH, using = "//a[@onclick='agregarDetalleServicio(1)']")
	private WebElement agregarBtn;
	
	@FindBy(how = How.XPATH, using = "//a[contains(@onclick,'regresar();')]")
	private WebElement cerrarBtn;
		
	//---------------------------------GETTERS & SETTERS-----------------------------------//

	public WebElement getTituloDatosAjustador() {
		return tituloDatosAjustador;
	}

	public void setTituloDatosAjustador(WebElement tituloDatosAjustador) {
		this.tituloDatosAjustador = tituloDatosAjustador;
	}

	public WebElement getNoAjustador() {
		return noAjustador;
	}

	public void setNoAjustador(WebElement noAjustador) {
		this.noAjustador = noAjustador;
	}

	public WebElement getOficinaSelect() {
		return oficinaSelect;
	}

	public void setOficinaSelect(WebElement oficinaSelect) {
		this.oficinaSelect = oficinaSelect;
	}

	public WebElement getPersonaMoralRb() {
		return personaMoralRb;
	}

	public void setPersonaMoralRb(WebElement personaMoralRb) {
		this.personaMoralRb = personaMoralRb;
	}

	public WebElement getPersonaFisicaRb() {
		return personaFisicaRb;
	}

	public void setPersonaFisicaRb(WebElement personaFisicaRb) {
		this.personaFisicaRb = personaFisicaRb;
	}

	public WebElement getEstatusSelect() {
		return estatusSelect;
	}

	public void setEstatusSelect(WebElement estatusSelect) {
		this.estatusSelect = estatusSelect;
	}

	public WebElement getNombrePersonaInput() {
		return nombrePersonaInput;
	}

	public void setNombrePersonaInput(WebElement nombrePersonaInput) {
		this.nombrePersonaInput = nombrePersonaInput;
	}

	public WebElement getApellidoPaternoInput() {
		return apellidoPaternoInput;
	}

	public void setApellidoPaternoInput(WebElement apellidoPaternoInput) {
		this.apellidoPaternoInput = apellidoPaternoInput;
	}

	public WebElement getApellidoMaternoInput() {
		return apellidoMaternoInput;
	}

	public void setApellidoMaternoInput(WebElement apellidoMaternoInput) {
		this.apellidoMaternoInput = apellidoMaternoInput;
	}

	public WebElement getClaveUsuarioInput() {
		return claveUsuarioInput;
	}

	public void setClaveUsuarioInput(WebElement claveUsuarioInput) {
		this.claveUsuarioInput = claveUsuarioInput;
	}

	public WebElement getNombreEmpresaInput() {
		return nombreEmpresaInput;
	}

	public void setNombreEmpresaInput(WebElement nombreEmpresaInput) {
		this.nombreEmpresaInput = nombreEmpresaInput;
	}

	public WebElement getNombreComercialInput() {
		return nombreComercialInput;
	}

	public void setNombreComercialInput(WebElement nombreComercialInput) {
		this.nombreComercialInput = nombreComercialInput;
	}

	public WebElement getAdministradorInput() {
		return administradorInput;
	}

	public void setAdministradorInput(WebElement administradorInput) {
		this.administradorInput = administradorInput;
	}

	public WebElement getFechaActivoInput() {
		return fechaActivoInput;
	}

	public void setFechaActivoInput(WebElement fechaActivoInput) {
		this.fechaActivoInput = fechaActivoInput;
	}

	public WebElement getFechaInactivoInput() {
		return fechaInactivoInput;
	}

	public void setFechaInactivoInput(WebElement fechaInactivoInput) {
		this.fechaInactivoInput = fechaInactivoInput;
	}

	public WebElement getTipoAjustadorSelect() {
		return tipoAjustadorSelect;
	}

	public void setTipoAjustadorSelect(WebElement tipoAjustadorSelect) {
		this.tipoAjustadorSelect = tipoAjustadorSelect;
	}

	public WebElement getPrestadorServicioIdInput() {
		return prestadorServicioIdInput;
	}

	public void setPrestadorServicioIdInput(WebElement prestadorServicioIdInput) {
		this.prestadorServicioIdInput = prestadorServicioIdInput;
	}

	public WebElement getCertificacionSelect() {
		return certificacionSelect;
	}

	public void setCertificacionSelect(WebElement certificacionSelect) {
		this.certificacionSelect = certificacionSelect;
	}

	public WebElement getPaisSelect() {
		return paisSelect;
	}

	public void setPaisSelect(WebElement paisSelect) {
		this.paisSelect = paisSelect;
	}

	public WebElement getEstadoSelect() {
		return estadoSelect;
	}

	public void setEstadoSelect(WebElement estadoSelect) {
		this.estadoSelect = estadoSelect;
	}

	public WebElement getMunicipioSelect() {
		return municipioSelect;
	}

	public void setMunicipioSelect(WebElement municipioSelect) {
		this.municipioSelect = municipioSelect;
	}

	public WebElement getColoniaSelect() {
		return coloniaSelect;
	}

	public void setColoniaSelect(WebElement coloniaSelect) {
		this.coloniaSelect = coloniaSelect;
	}

	public WebElement getCpInput() {
		return cpInput;
	}

	public void setCpInput(WebElement cpInput) {
		this.cpInput = cpInput;
	}

	public WebElement getColoniaCheck() {
		return coloniaCheck;
	}

	public void setColoniaCheck(WebElement coloniaCheck) {
		this.coloniaCheck = coloniaCheck;
	}

	public WebElement getNuevaColoniaInput() {
		return nuevaColoniaInput;
	}

	public void setNuevaColoniaInput(WebElement nuevaColoniaInput) {
		this.nuevaColoniaInput = nuevaColoniaInput;
	}

	public WebElement getCalleInput() {
		return calleInput;
	}

	public void setCalleInput(WebElement calleInput) {
		this.calleInput = calleInput;
	}

	public WebElement getNumExtInput() {
		return numExtInput;
	}

	public void setNumExtInput(WebElement numExtInput) {
		this.numExtInput = numExtInput;
	}

	public WebElement getNumIntInput() {
		return numIntInput;
	}

	public void setNumIntInput(WebElement numIntInput) {
		this.numIntInput = numIntInput;
	}

	public WebElement getReferenciaInput() {
		return referenciaInput;
	}

	public void setReferenciaInput(WebElement referenciaInput) {
		this.referenciaInput = referenciaInput;
	}

	public WebElement getTelCasaLadaInput() {
		return telCasaLadaInput;
	}

	public void setTelCasaLadaInput(WebElement telCasaLadaInput) {
		this.telCasaLadaInput = telCasaLadaInput;
	}

	public WebElement getTelCasaInput() {
		return telCasaInput;
	}

	public void setTelCasaInput(WebElement telCasaInput) {
		this.telCasaInput = telCasaInput;
	}

	public WebElement getTelCasaLada2Input() {
		return telCasaLada2Input;
	}

	public void setTelCasaLada2Input(WebElement telCasaLada2Input) {
		this.telCasaLada2Input = telCasaLada2Input;
	}

	public WebElement getTelCasa2Input() {
		return telCasa2Input;
	}

	public void setTelCasa2Input(WebElement telCasa2Input) {
		this.telCasa2Input = telCasa2Input;
	}

	public WebElement getTelOtro2LadaInput() {
		return telOtro2LadaInput;
	}

	public void setTelOtro2LadaInput(WebElement telOtro2LadaInput) {
		this.telOtro2LadaInput = telOtro2LadaInput;
	}

	public WebElement getTelOtro2Input() {
		return telOtro2Input;
	}

	public void setTelOtro2Input(WebElement telOtro2Input) {
		this.telOtro2Input = telOtro2Input;
	}

	public WebElement getTelCelularLadaInput() {
		return telCelularLadaInput;
	}

	public void setTelCelularLadaInput(WebElement telCelularLadaInput) {
		this.telCelularLadaInput = telCelularLadaInput;
	}

	public WebElement getTelCelularInput() {
		return telCelularInput;
	}

	public void setTelCelularInput(WebElement telCelularInput) {
		this.telCelularInput = telCelularInput;
	}

	public WebElement getCorreoPrincipalInput() {
		return correoPrincipalInput;
	}

	public void setCorreoPrincipalInput(WebElement correoPrincipalInput) {
		this.correoPrincipalInput = correoPrincipalInput;
	}

	public WebElement getRfcInput() {
		return rfcInput;
	}

	public void setRfcInput(WebElement rfcInput) {
		this.rfcInput = rfcInput;
	}

	public WebElement getCodigoUnidadInput() {
		return codigoUnidadInput;
	}

	public void setCodigoUnidadInput(WebElement codigoUnidadInput) {
		this.codigoUnidadInput = codigoUnidadInput;
	}

	public WebElement getPlacasInput() {
		return placasInput;
	}

	public void setPlacasInput(WebElement placasInput) {
		this.placasInput = placasInput;
	}

	public WebElement getDescripcionInput() {
		return descripcionInput;
	}

	public void setDescripcionInput(WebElement descripcionInput) {
		this.descripcionInput = descripcionInput;
	}

	public WebElement getAgregarBtn() {
		return agregarBtn;
	}

	public void setAgregarBtn(WebElement agregarBtn) {
		this.agregarBtn = agregarBtn;
	}

	public WebElement getCerrarBtn() {
		return cerrarBtn;
	}

	public void setCerrarBtn(WebElement cerrarBtn) {
		this.cerrarBtn = cerrarBtn;
	}
	
	

	
	

}
