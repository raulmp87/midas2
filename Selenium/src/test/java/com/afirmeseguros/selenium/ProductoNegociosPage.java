package com.afirmeseguros.selenium;

import static org.junit.Assert.assertTrue;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class ProductoNegociosPage extends BasicTest{
	
	@FindBy(how = How.CSS, using= ".hdrcell.filter>input")
	private WebElement inputNombreNegocio; 
	
	@FindBy(how = How.CLASS_NAME, using="hdrcell")
	private WebElement ordenarNegocios;

	@FindBy(how = How.XPATH, using="//td[contains(.,'NEGOCIO BASE PRODUCCION')]")
	private WebElement negocioSeleccion;

	@FindBy(how =  How.CSS, using= ".ev_light>td>a")
	private WebElement editarIcon;
	
	@FindBy(how = How.CLASS_NAME, using="titulo")
	private WebElement tituloModificarNegocio;
	
	@FindBy(how = How.XPATH, using="//td[@title='AUTOMOVILES INDIVIDUALES']")
	private WebElement prodAutoInd;
	
	@FindBy(xpath = "//td[@title='AUTOMOVILES FLOTILLA']")
	private WebElement prodAutoFlo;
	
	@FindBy(css = ".ev_light>td>a")
	private WebElement editarIconProductoFlo;
	
	@FindBy(css = ".odd_light>td>a")
	private WebElement editarIconProductoInd;
	
	@FindBy(how = How.CLASS_NAME, using="subtituloIzquierdaDiv")
	private WebElement tituloEditarProducto;
	
	//GETTERS SETTERS
	public WebElement getOrdenarNegocios() {
		return ordenarNegocios;
	}

	public void setOrdenarNegocios(WebElement ordenarNegocios) {
		this.ordenarNegocios = ordenarNegocios;
	}

	public WebElement getEditarIcon() {
		return editarIcon;
	}

	public void setEditarIcon(WebElement editarIcon) {
		this.editarIcon = editarIcon;
	}

	public WebElement getProdAutoInd() {
		return prodAutoInd;
	}

	public void setProdAutoInd(WebElement prodAutoInd) {
		this.prodAutoInd = prodAutoInd;
	}

	public WebElement getProdAutoFlo() {
		return prodAutoFlo;
	}

	public void setProdAutoFlo(WebElement prodAutoFlo) {
		this.prodAutoFlo = prodAutoFlo;
	}

	public WebElement getEditarIconProducto() {
		return editarIconProductoFlo;
	}

	public void setEditarIconProducto(WebElement editarIconProducto) {
		this.editarIconProductoFlo = editarIconProducto;
	}
	
	public WebElement getNegocioSeleccion() {
		return negocioSeleccion;
	}

	public void setNegocioSeleccion(WebElement negocioSeleccion) {
		this.negocioSeleccion = negocioSeleccion;
	}
	
	public WebElement getEditarIconProductoFlo() {
		return editarIconProductoFlo;
	}

	public void setEditarIconProductoFlo(WebElement editarIconProductoFlo) {
		this.editarIconProductoFlo = editarIconProductoFlo;
	}

	public WebElement getEditarIconProductoInd() {
		return editarIconProductoInd;
	}

	public void setEditarIconProductoInd(WebElement editarIconProductoInd) {
		this.editarIconProductoInd = editarIconProductoInd;
	}
	
	
	
	public void sendKeysNombreNegocio(){
		wait.until(ExpectedConditions.visibilityOf(inputNombreNegocio));
		builder.moveToElement(inputNombreNegocio);
		builder.build().perform();
		inputNombreNegocio.sendKeys(NEGOCIO);
	}
	
	public void validateTitleListado(){
		assertTrue("Modificar Negocio".equals(tituloModificarNegocio.getText()));
		System.out.println(tituloModificarNegocio.getText());
	}
	
	public void validateTitleEditarProducto(){
		assertTrue("Información General del Producto".equals(tituloEditarProducto.getText()));
		System.out.println(tituloEditarProducto.getText());
	}
	

}
